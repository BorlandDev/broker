package io.sevenb.terminal.device.utils

import io.sevenb.terminal.data.model.auth.ErrorMessage
import org.junit.Assert.assertEquals
import org.junit.Test

class SerializerTest {

    @Test
    fun deserialize() {
        val json = "{\"message\":\"Please complete the account confirmation procedure before using this functionality\",\"error\":\"access_denied\"}"
        val message = "Please complete the account confirmation procedure before using this functionality"
        val error = "access_denied"
        val errorMessage = Serializer.deserialize(json, ErrorMessage::class.java)

        assertEquals(error, errorMessage.error)
        assertEquals(message, errorMessage.message)

        print("deserialize $errorMessage")
    }
}