package io.sevenb.terminal.domain.usecase.auth

import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

class ValidateEmailPasswordTest {

    private lateinit var validateEmailPassword: ValidateEmailPassword

    @Before
    fun init() {
        validateEmailPassword = ValidateEmailPassword()
    }

    @Test
    fun validateEmailPositive() {
        assertTrue(validateEmailPassword.validateEmail("s@d.aa"))
        assertTrue(validateEmailPassword.validateEmail("z@a.b.c.dd"))
        assertTrue(validateEmailPassword.validateEmail("go.usa.gov@gsa.gov"))
    }

    @Test
    fun validateEmailNegative() {
        assertFalse(validateEmailPassword.validateEmail("Z@a.b.c.aa"))
        assertFalse(validateEmailPassword.validateEmail("zsxdcF3"))
        assertFalse(validateEmailPassword.validateEmail("zsxdcF33zsxdcF33zsxdcF33zsxdcF33zsxdcF33zsxdcF33zsxdcF33zsxdcF33"))
        assertFalse(validateEmailPassword.validateEmail(""))
        assertFalse(validateEmailPassword.validateEmail("!@#$%&*+"))
        assertFalse(validateEmailPassword.validateEmail("=?^_{|}~/-"))
        assertFalse(validateEmailPassword.validateEmail("zsxdcf33"))
        assertFalse(validateEmailPassword.validateEmail("Zsxdcf33"))
    }

    @Test
    fun validatePasswordPositive() {
        assertTrue(validateEmailPassword.validatePassword("zsxdcF33"))
        assertTrue(validateEmailPassword.validatePassword("zsxdcF3/"))
        assertTrue(validateEmailPassword.validatePassword("@sxdcF33"))
        // 64 symbols is maximum length
        assertTrue(validateEmailPassword.validatePassword("zsxdcF33zsxdcF33zsxdcF33zsxdcF33zsxdcF33zsxdcF33zsxdcF33zsxdcF33"))
        assertTrue(validateEmailPassword.validatePassword("0aZ!@#$%&*+=?^_{|}~/-"))
    }

    @Test
    fun validatePasswordNegative() {
        assertFalse(validateEmailPassword.validatePassword("zsxdcF3"))
        // 65 symbols
        assertFalse(validateEmailPassword.validatePassword("/zsxdcF33zsxdcF33zsxdcF33zsxdcF33zsxdcF33zsxdcF33zsxdcF33zsxdcF33"))
        assertFalse(validateEmailPassword.validatePassword(""))
        assertFalse(validateEmailPassword.validatePassword("!@#$%&*+"))
        assertFalse(validateEmailPassword.validatePassword("=?^_{|}~/-"))
        assertFalse(validateEmailPassword.validatePassword("zsxdcf33"))
        assertFalse(validateEmailPassword.validatePassword("Zsxdcfvg"))
        assertFalse(validateEmailPassword.validatePassword("zsxdcfv3"))
    }
}