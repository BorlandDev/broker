package io.sevenb.terminal.device.utils

import com.google.gson.Gson
import java.lang.reflect.Type


object Serializer {

    private val gson = Gson()

    fun serialize(any: Any?, clazz: Class<*>?): String? {
        return gson.toJson(any, clazz)
    }

    fun serializeType(any: Any?, type: Type): String? {
        return gson.toJson(any, type)
    }

    fun <T> deserialize(string: String?, clazz: Class<T>?): T {
        return gson.fromJson(string, clazz)
    }

    fun <T> deserializeType(string: String?, type: Type): T {
        return gson.fromJson(string, type)
    }

}