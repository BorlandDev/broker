package io.sevenb.terminal.device.cryptography

import android.content.Context
import android.os.Build
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyPermanentlyInvalidatedException
import android.security.keystore.KeyProperties
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.biometric.BiometricManager
import java.lang.IllegalArgumentException
import java.nio.charset.Charset
import java.security.KeyStore
import java.security.UnrecoverableKeyException
import javax.crypto.AEADBadTagException
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.SecretKey
import javax.crypto.spec.GCMParameterSpec
import kotlin.jvm.Throws

/**
 * Handles encryption and decryption
 */
interface CryptographyManager {

    fun getInitializedCipherForEncryption(): Cipher

    fun getInitializedCipherForDecryption(initializationVector: ByteArray): Cipher

    /**
     * The Cipher created with [getInitializedCipherForEncryption] is used here
     */
    fun encryptData(plaintext: String, cipher: Cipher): CiphertextWrapper

    /**
     * The Cipher created with [getInitializedCipherForDecryption] is used here
     */
    fun decryptData(ciphertext: ByteArray, cipher: Cipher): String

    /**
     * Return a constant represented the ability of biometric usage
     */
    fun getBiometricAuthenticateAbility(): Int

}

fun CryptographyManager(context: Context): CryptographyManager = CryptographyManagerImpl(context)

/**
 * To get an instance of this private CryptographyManagerImpl class, use the top-level function
 * fun CryptographyManager(): CryptographyManager = CryptographyManagerImpl()
 */
@RequiresApi(Build.VERSION_CODES.M)
private class CryptographyManagerImpl(val context: Context) : CryptographyManager {

    private val KEY_SIZE = 256
    private val ANDROID_KEYSTORE = "AndroidKeyStore"
    private val SECRET_KEY_NAME = "SECRET_KEY_NAME"
    private val ENCRYPTION_BLOCK_MODE = KeyProperties.BLOCK_MODE_GCM
    private val ENCRYPTION_PADDING = KeyProperties.ENCRYPTION_PADDING_NONE
    private val ENCRYPTION_ALGORITHM = KeyProperties.KEY_ALGORITHM_AES

    override fun getBiometricAuthenticateAbility(): Int {
        val biometricManager = BiometricManager.from(context)
        return when (biometricManager.canAuthenticate()) {
            BiometricManager.BIOMETRIC_SUCCESS ->
                BiometricManager.BIOMETRIC_SUCCESS
//                    Log.d("MY_APP_TAG", "App can authenticate using biometrics.")

            BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE ->
                BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE
//                    Log.e("MY_APP_TAG", "No biometric features available on this device.")

            BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE ->
                BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE
//                    Log.e("MY_APP_TAG", "Biometric features are currently unavailable.")

            BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED ->
                BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED

            else -> BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE
        }
    }

    @Throws(java.lang.IllegalStateException::class)
    override fun getInitializedCipherForEncryption(): Cipher {
        val cipher = getCipher()
        var secretKey: SecretKey

        try {
            secretKey = getOrCreateSecretKey(SECRET_KEY_NAME)
        } catch (e: UnrecoverableKeyException) {
            val keyStore = KeyStore.getInstance(ANDROID_KEYSTORE)
            keyStore.load(null)
            keyStore.deleteEntry(SECRET_KEY_NAME)
            secretKey = getOrCreateSecretKey(SECRET_KEY_NAME)
            cipher.init(Cipher.ENCRYPT_MODE, secretKey)
        } catch (e: IllegalStateException) {
            throw IllegalStateException()
        }

        try {
            cipher.init(Cipher.ENCRYPT_MODE, secretKey)
        } catch (e: KeyPermanentlyInvalidatedException) {
            val keyStore = KeyStore.getInstance(ANDROID_KEYSTORE)
            keyStore.load(null)
            keyStore.deleteEntry(SECRET_KEY_NAME)
            getOrCreateSecretKey(SECRET_KEY_NAME)
            cipher.init(Cipher.ENCRYPT_MODE, secretKey)
            return cipher
        }
        return cipher
    }

    override fun getInitializedCipherForDecryption(
        initializationVector: ByteArray
    ): Cipher {
        val cipher = getCipher()

        var secretKey: SecretKey
        try {
            secretKey = getOrCreateSecretKey(SECRET_KEY_NAME)

        } catch (e: UnrecoverableKeyException) {
            val keyStore = KeyStore.getInstance(ANDROID_KEYSTORE)
            keyStore.load(null)
            keyStore.deleteEntry(SECRET_KEY_NAME)
            secretKey = getOrCreateSecretKey(SECRET_KEY_NAME)
            cipher.init(Cipher.DECRYPT_MODE, secretKey, GCMParameterSpec(128, initializationVector))
        }

        try {
            cipher.init(Cipher.DECRYPT_MODE, secretKey, GCMParameterSpec(128, initializationVector))
        } catch (e: KeyPermanentlyInvalidatedException) {
            val keyStore = KeyStore.getInstance(ANDROID_KEYSTORE)
            keyStore.load(null)
            keyStore.deleteEntry(SECRET_KEY_NAME)
            getOrCreateSecretKey(SECRET_KEY_NAME)
        }
//        val secretKey = getOrCreateSecretKey(SECRET_KEY_NAME)
//        cipher.init(Cipher.DECRYPT_MODE, secretKey, GCMParameterSpec(128, initializationVector))
        return cipher
    }

    override fun encryptData(plaintext: String, cipher: Cipher): CiphertextWrapper {
        val ciphertext = cipher.doFinal(plaintext.toByteArray(Charset.forName("UTF-8")))
        return CiphertextWrapper(ciphertext, cipher.iv)
    }

    override fun decryptData(ciphertext: ByteArray, cipher: Cipher): String {
        return try {
            val plaintext = cipher.doFinal(ciphertext)
            String(plaintext, Charset.forName("UTF-8"))
        } catch (e: AEADBadTagException) {
            ""
        } catch (e: Exception) {
            ""
        }
    }

    private fun getCipher(): Cipher {
        val transformation = "$ENCRYPTION_ALGORITHM/$ENCRYPTION_BLOCK_MODE/$ENCRYPTION_PADDING"
        return Cipher.getInstance(transformation)
    }

    private fun getOrCreateSecretKey(keyName: String): SecretKey {
        // If Secretkey was previously created for that keyName, then grab and return it.
        val keyStore = KeyStore.getInstance(ANDROID_KEYSTORE)
        keyStore.load(null) // Keystore must be loaded before it can be accessed
        keyStore.getKey(keyName, null)?.let { return it as SecretKey }

        // if you reach here, then a new SecretKey must be generated for that keyName
        val paramsBuilder = KeyGenParameterSpec.Builder(
            keyName,
            KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT
        )
        paramsBuilder.apply {
            setBlockModes(ENCRYPTION_BLOCK_MODE)
            setEncryptionPaddings(ENCRYPTION_PADDING)
            setKeySize(KEY_SIZE)
            setUserAuthenticationRequired(true)
        }

        val keyGenParams = paramsBuilder.build()
        val keyGenerator = KeyGenerator.getInstance(
            KeyProperties.KEY_ALGORITHM_AES,
            ANDROID_KEYSTORE
        )
        keyGenerator.init(keyGenParams)
        return keyGenerator.generateKey()
    }

}


data class CiphertextWrapper(val ciphertext: ByteArray, val initializationVector: ByteArray)