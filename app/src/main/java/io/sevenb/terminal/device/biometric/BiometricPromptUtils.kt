package io.sevenb.terminal.device.biometric

import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import io.sevenb.terminal.R
import timber.log.Timber

object BiometricPromptUtils {

    fun createBiometricPrompt(
        fragment: Fragment,
        processSuccess: (BiometricPrompt.AuthenticationResult) -> Unit,
        processError: (Int) -> Unit
    ): BiometricPrompt {
        val executor = ContextCompat.getMainExecutor(fragment.activity)

        val callback = object : BiometricPrompt.AuthenticationCallback() {

            override fun onAuthenticationError(errCode: Int, errString: CharSequence) {
                super.onAuthenticationError(errCode, errString)
                processError(errCode)
                Timber.d("errCode is $errCode and errString is: $errString")
            }

            override fun onAuthenticationFailed() {
                super.onAuthenticationFailed()
                processError(101)
                Timber.d("User biometric rejected.")
            }

            override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                super.onAuthenticationSucceeded(result)
                Timber.d("Authentication was successful")
                processSuccess(result)
            }
        }
        return BiometricPrompt(fragment, executor, callback)
    }

    fun createLoginPromptInfo(
        fragment: Fragment,
        buttonString: String = fragment.getString(R.string.prompt_info_use_app_password)
    ): BiometricPrompt.PromptInfo =
        BiometricPrompt.PromptInfo.Builder().apply {
            setTitle(fragment.getString(R.string.prompt_info_title))
            setNegativeButtonText(buttonString)
            setConfirmationRequired(false)
        }.build()
}