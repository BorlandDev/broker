package io.sevenb.terminal.device.utils

import org.threeten.bp.Instant
import org.threeten.bp.ZoneId
import org.threeten.bp.ZonedDateTime
import org.threeten.bp.format.DateTimeFormatter
import java.util.*

object DateTimeUtil {

    private val dateFormatter = DateTimeFormatter.ofPattern("dd MMM yyyy")
    private val timeFormatter = DateTimeFormatter.ofPattern("h:mm a")
    private val dateTimeFormatter = DateTimeFormatter.ofPattern("dd MMM yyyy h:mm a")
    private val secondsTimeFormatter = DateTimeFormatter.ofPattern("ss")

    fun dateFromTimestamp(timestamp: Long): String {
        val dt = ZonedDateTime.ofInstant(Instant.ofEpochSecond(timestamp), ZoneId.systemDefault())
        return dt.format(dateFormatter)
    }

    fun dateTimeFromTimestamp(timestamp: Long): String {
        val dt = ZonedDateTime.ofInstant(Instant.ofEpochSecond(timestamp), ZoneId.systemDefault())
        return dt.format(dateTimeFormatter)
    }

    fun timeFromTimestamp(timestamp: Long): String {
        val dt = ZonedDateTime.ofInstant(Instant.ofEpochSecond(timestamp), ZoneId.systemDefault())
        return dt.format(timeFormatter)
    }

    fun parseToTimestamp(stringDateTime: String): Long {
        val dateTime = ZonedDateTime.parse(stringDateTime)
        return dateTime.toEpochSecond()
    }

    fun parseToTimestampWithFormat(stringDateTime: String): Long {
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
        val dateTime = ZonedDateTime.parse(stringDateTime, formatter)
        return dateTime.toEpochSecond()
    }

    fun formatSeconds(seconds: Long): String {
        val dt = ZonedDateTime.ofInstant(Instant.ofEpochSecond(seconds), ZoneId.systemDefault())
        return dt.format(secondsTimeFormatter)
    }

    private fun getMonth(date: String): String {
        monthRU.forEach {
            if (date.contains(it))  {
                return date.replace(it, monthEN[monthRU.indexOf(it)])
            }
        }

        return ""
    }

    private val monthEN = arrayOf(
        "jan",
        "feb",
        "mar",
        "apr",
        "may",
        "jun",
        "jul",
        "aug",
        "sep",
        "oct",
        "nov",
        "dec"
    )

    private val monthRU = arrayOf(
        "янв",
        "фев",
        "мар",
        "апр",
        "май",
        "июн",
        "июл",
        "авг",
        "сен",
        "окт",
        "ноя",
        "дек"
    )

}