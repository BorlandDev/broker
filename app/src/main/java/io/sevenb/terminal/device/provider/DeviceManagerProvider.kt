package io.sevenb.terminal.device.provider

import android.content.ClipboardManager
import android.content.Context

class DeviceManagerProvider(
    private val context: Context
) {

    fun clipboardManager() : ClipboardManager {
        return context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    }
}