package io.sevenb.terminal

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.android.play.core.appupdate.AppUpdateInfo
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.model.ActivityResult
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.UpdateAvailability
import io.sevenb.terminal.data.datasource.retrofit.interceptor.AccessTokenInterceptor
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.enums.AuthState
import io.sevenb.terminal.data.model.enums.CommonState
import io.sevenb.terminal.data.model.enums.SafetyStates
import io.sevenb.terminal.data.model.enums.auth.NewAuthStates
import io.sevenb.terminal.data.model.enums.auth.TfaState
import io.sevenb.terminal.data.model.enums.pass_code.PassCodeStates
import io.sevenb.terminal.data.utils.ContextWrapperUtils
import io.sevenb.terminal.domain.usecase.auth_back_stack.AuthBackStackHandler
import io.sevenb.terminal.domain.usecase.charts.AccountData
import io.sevenb.terminal.domain.usecase.new_auth.HandleFingerprint
import io.sevenb.terminal.domain.usecase.safety.AccountSafetyStoring
import io.sevenb.terminal.ui.base.BaseActivity
import io.sevenb.terminal.ui.extension.setGoneViews
import io.sevenb.terminal.ui.extension.viewModelProvider
import io.sevenb.terminal.ui.extension.visible
import io.sevenb.terminal.ui.screens.auth.SharedAuthViewModel
import io.sevenb.terminal.ui.screens.lock_timeout.LockTimeoutModule
import io.sevenb.terminal.ui.screens.lock_timeout.SharedLockModel
import io.sevenb.terminal.ui.screens.safety.SafetyViewModel
import io.sevenb.terminal.ui.screens.safety.passcode_state.PassCodeFlow
import io.sevenb.terminal.ui.screens.safety.passcode_state.PassCodeViewModel
import io.sevenb.terminal.ui.screens.safety.tfa.TfaViewModel
import io.sevenb.terminal.ui.screens.settings.NotifySettingsViewModel
import io.sevenb.terminal.ui.screens.settings.SharedSettingToActivityViewModel
import io.sevenb.terminal.utils.ConnectionStateMonitor
import kotlinx.android.synthetic.main.activity_app.*
import kotlinx.coroutines.launch
import timber.log.Timber
import zendesk.support.guide.HelpCenterActivity
import zendesk.support.request.RequestActivity
import javax.inject.Inject

class AppActivity : BaseActivity() {

    private lateinit var navController: NavController

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var connectionStateMonitor: ConnectionStateMonitor

    @Inject
    lateinit var authBackStackHandler: AuthBackStackHandler

    @Inject
    lateinit var handleFingerprint: HandleFingerprint

    @Inject
    lateinit var passCodeFlow: PassCodeFlow
    private lateinit var passCodeViewModel: PassCodeViewModel
    private lateinit var safetyViewModel: SafetyViewModel

    @Inject
    lateinit var accessTokenInterceptor: AccessTokenInterceptor

    @Inject
    lateinit var safetyStoring: AccountSafetyStoring

    @Inject
    lateinit var lockTimeoutModule: LockTimeoutModule

    lateinit var authState: AuthState
    private lateinit var sharedSettingsToActivityViewModel: SharedSettingToActivityViewModel
    private lateinit var notifySettingsViewModel: NotifySettingsViewModel

    private lateinit var sharedAuthViewModel: SharedAuthViewModel

    private lateinit var sharedLockModel: SharedLockModel

    private lateinit var tfaViewModel: TfaViewModel

    private var navigatedToAuth = false

    var launchActivity = false
    var firstLaunch = true
    private lateinit var appUpdateManager: AppUpdateManager

    override fun onResume() {
        super.onResume()
//        checkUpdate()
        lifecycleScope.launch {
            val passCodeResult = safetyStoring.restorePassCodeEnabled()
            val isTimeoutEnabled = safetyStoring.restoreTimeoutEnabled()
            if (passCodeResult is Result.Success && launchActivity && !firstLaunch &&
                isTimeoutEnabled is Result.Success && isTimeoutEnabled.data
            ) {
                lockTimeoutModule.apply {
                    init(lifecycleScope, sharedLockModel)
                    when (restoreLockData()) {
                        null -> {
                            println()
                        }
                        R.id.splashScreenFragment -> {
                            if (findNavController(R.id.nav_host_fragment).currentDestination?.id != R.id.safetyFragment) {
                                sharedLockModel.availableMethod.value = SafetyStates.NOT_LOCKED
                                findNavController(R.id.nav_host_fragment).navigate(R.id.safetyFragment)
                            }
                        }
                        R.id.safetyFragment -> {
                            if (findNavController(R.id.nav_host_fragment).currentDestination?.id != R.id.safetyFragment) {
                                safetyStoring.storeIsLocked(true)
                                val state =
                                    when (val result = safetyStoring.restoreFingerprintEnabled()) {
                                        is Result.Success -> {
                                            if (result.data) SafetyStates.BOTH_VARIANTS_AVAILABLE
                                            else SafetyStates.PASS_CODE_AVAILABLE
                                        }
                                        is Result.Error -> SafetyStates.NOT_LOCKED
                                    }
                                sharedLockModel.availableMethod.value = state
                                findNavController(R.id.nav_host_fragment).navigate(R.id.safetyFragment)
                            }
                        }
                        else -> {
                            println()
                        }
                    }
                    removeData()
                }
            }
            firstLaunch = false
        }
        hideKeyboard()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_app)
        launchActivity = true
        sharedAuthViewModel = viewModelProvider(viewModelFactory)
        passCodeViewModel = viewModelProvider(viewModelFactory)
        safetyViewModel = viewModelProvider(viewModelFactory)
        sharedSettingsToActivityViewModel = viewModelProvider(viewModelFactory)
        sharedLockModel = viewModelProvider(viewModelFactory)
        notifySettingsViewModel = viewModelProvider(viewModelFactory)
        tfaViewModel = viewModelProvider(viewModelFactory)
        initView()
        subscribeBackStack()
        subscribeCreatePassCodeFromSettings()
        connectionStateMonitor.observe(this, {})
    }

    private fun subscribeCreatePassCodeFromSettings() {
        sharedSettingsToActivityViewModel.apply {
            createPassCodeFromSettings.observe(
                this@AppActivity, { processCreatePassCodeFromSettings(it) })
            enterPassCodeFromSettings.observe(this@AppActivity, { processEnterPassCode(it) })
            message.observe(this@AppActivity, { processShowMessage(it) })
        }

        notifySettingsViewModel.notifyActToUpdateSefetyData.observe(
            this, { processNotifySettings(it) })
        accessTokenInterceptor.observe(this, { processLogOut(it) })
    }

    private fun processLogOut(value: Boolean?) {
        if (value == false) {
            if (!navigatedToAuth) {
                navigatedToAuth = true
                navController.navigate(R.id.newAuthFragment)
            }
        } else if (value == true) {
            navigatedToAuth = false
        }
    }

    private fun processNotifySettings(value: Boolean?) {
        value?.let { notifySettingsViewModel.notifyFrToUpdateSefetyData.value = it }
    }

    private fun processEnterPassCode(pair: Pair<Boolean?, Fragment?>) {
        if (pair.first != null) {
            if (pair.first!! && pair.second != null) {
                passCodeFlow.apply {
                    initLifecycleOwner(this@AppActivity, pair.second!!, handleFingerprint)
                    initSharedSettingsToActivityViewModel(sharedSettingsToActivityViewModel)
                    initViews(
                        fragmentPassCodeHandling,
                        passCodeViewModel,
                        safetyViewModel,
                        PassCodeStates.ENTER_PASS_CODE,
                        "",
                        "",
                        true
                    )
                }
                setGoneViews(applicationContainer)
                fragmentPassCodeHandling.visible(true)
            } else {
                setGoneViews(fragmentPassCodeHandling)
                applicationContainer.visible(true)
                passCodeFlow.setNullsToLiveData()
                sharedSettingsToActivityViewModel.passCodeWasEntered.value = true
            }
        } else {
            setGoneViews(fragmentPassCodeHandling)
            applicationContainer.visible(true)
            passCodeFlow.setNullsToLiveData()
            sharedSettingsToActivityViewModel.passCodeWasEntered.value = false
        }
    }

    private fun processShowMessage(message: String?) {
        message?.let { showSnackBarMessage(it) }
    }

    private fun processCreatePassCodeFromSettings(pair: Pair<Boolean?, Fragment?>) {
        if (pair.first != null) {
            if (pair.first!! && pair.second != null) {
                passCodeFlow.apply {
                    initLifecycleOwner(this@AppActivity, pair.second!!, handleFingerprint)
                    initSharedSettingsToActivityViewModel(sharedSettingsToActivityViewModel)
                    initViews(
                        fragmentPassCodeHandling,
                        passCodeViewModel,
                        safetyViewModel,
                        PassCodeStates.CREATE_PASS_CODE,
                        "",
                        "",
                        true
                    )
                }
                setGoneViews(applicationContainer)
                fragmentPassCodeHandling.visible(true)
            } else {
                setGoneViews(fragmentPassCodeHandling)
                applicationContainer.visible(true)
                passCodeFlow.setNullsToLiveData()
                sharedSettingsToActivityViewModel.passCodeWasCreated.value = true
                sharedSettingsToActivityViewModel.passCodeWasCreated.value = null
            }
        } else {
            setGoneViews(fragmentPassCodeHandling)
            applicationContainer.visible(true)
            passCodeFlow.setNullsToLiveData()
            sharedSettingsToActivityViewModel.passCodeWasCreated.value = false
            sharedSettingsToActivityViewModel.passCodeWasCreated.value = null
        }
    }

    private fun subscribeBackStack() {
        sharedAuthViewModel.apply {
            setGlobalStateToActivity.observe(this@AppActivity, { processNewGlobalState(it) })
            setSubStateToActivity.observe(this@AppActivity, { processNewSubState(it) })
        }
        tfaViewModel.apply {
            setGlobalStateToActivity.observe(this@AppActivity, { processNewGlobalState(it) })
        }
    }

    private fun processNewSubState(state: CommonState?) {
        state?.let { authBackStackHandler.setSubAuthState(it) }
    }

    private fun processNewGlobalState(state: CommonState?) {
        state?.let { authBackStackHandler.setGlobalState(it) }
    }

    private fun processGetGlobalStateFromActivity(state: CommonState?) {
        state?.let {
            if (it is NewAuthStates) {
                sharedAuthViewModel.getGlobalStateFromActivity.value = it
            } else if (it is TfaState) {
                tfaViewModel.getGlobalStateFromActivity.value = it
            }
        }
    }

    private fun initView() {
        initNavigation()
    }

    private fun initNavigation() {
        navController = findNavController(R.id.nav_host_fragment)
        bottomNavView.setupWithNavController(navController)
        bottomNavView.setOnNavigationItemReselectedListener {
            Timber.d("BottomNavigationView item is reselected")
        }

        // open Zendesk support activity or follow default navigation
        bottomNavView.setOnNavigationItemSelectedListener {
            if (it.itemId == R.id.supportFragment) {
                openSupport()
                return@setOnNavigationItemSelectedListener false
            } else {
                navController.popBackStack()
                navController.navigate(it.itemId)
                return@setOnNavigationItemSelectedListener true
            }
        }

        bottomNavView.visibility = View.VISIBLE
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(ContextWrapperUtils.wrap(newBase, "en"))
    }

    override fun onBackPressed() {
        val navHost = R.id.nav_host_fragment
        findNavController(navHost).apply {
            if (currentDestination?.id == R.id.portfolioFragment) finish()
            if (previousBackStackEntry?.destination?.id == R.id.newAuthFragment) finish()
            if (previousBackStackEntry?.destination?.id == R.id.safetyFragment) finish()
            if (previousBackStackEntry?.destination?.id == R.id.splashScreenFragment) finish()
            if (currentDestination?.id == R.id.safetyFragment) finish()
            if (currentDestination?.id == R.id.splashScreenFragment) finish()
        }

        if (authBackStackHandler.availableBackNavigation()) {
            processGetGlobalStateFromActivity(authBackStackHandler.getPreviousEntry())
        } else {

            super.onBackPressed()
        }
    }

    private fun openSupport() {
        val requestActivityConfig = RequestActivity.builder()
            .withRequestSubject("Android ticket")
            .withTags("android", "mobile")
            .config()

        HelpCenterActivity.builder()
            .withContactUsButtonVisible(false)
            .withShowConversationsMenuButton(false)
            .show(this, requestActivityConfig)
    }

    fun isShowProgress(isShow: Boolean) {
        progress.visibility = if (isShow) View.VISIBLE else View.GONE
    }

    private fun showSnackBarMessage(message: String) {
        val bottomNavView: BottomNavigationView? = this.findViewById(R.id.bottomNavView)
        bottomNavView?.let {
            if (!message.contains(AccountData.ERROR_CODE_500))
                Snackbar.make(bottomNavView, message, Snackbar.LENGTH_SHORT)
                    .apply { anchorView = bottomNavView }
                    .show()
        }
    }

    override fun onPause() {
        hideKeyboard()
        lifecycleScope.launch {
            val isLockedResult = safetyStoring.restoreIsLocked()
            if (isLockedResult is Result.Success && !isLockedResult.data) {
                safetyStoring.storePauseTime(System.currentTimeMillis().toString())
            }
        }
        super.onPause()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == MY_REQUEST_CODE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                }
                Activity.RESULT_CANCELED -> {
                    finish()
                }
                ActivityResult.RESULT_IN_APP_UPDATE_FAILED -> {
//                    checkUpdate()
                }
            }
        }
    }

    private fun startImmediateUpdate(appUpdateInfo: AppUpdateInfo) {
        try {
            appUpdateManager.startUpdateFlowForResult(
                appUpdateInfo,
                AppUpdateType.IMMEDIATE,
                this,
                MY_REQUEST_CODE
            )
        } catch (e: IntentSender.SendIntentException) {
            e.printStackTrace()
        }
    }

    private fun checkUpdate() {
        appUpdateManager = AppUpdateManagerFactory.create(this)
        val appUpdateInfoTask = appUpdateManager.appUpdateInfo
        appUpdateInfoTask.addOnSuccessListener { appUpdateInfo ->
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS
                || appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
            ) {
                startImmediateUpdate(appUpdateInfo)
            }
        }
    }

    companion object {
        var user_email: String = ""
        private const val MY_REQUEST_CODE = 123
    }

}
