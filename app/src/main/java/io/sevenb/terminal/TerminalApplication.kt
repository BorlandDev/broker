package io.sevenb.terminal

import android.content.BroadcastReceiver
import android.content.Intent
import android.content.IntentFilter
import android.preference.PreferenceManager
import android.util.Log
import coil.Coil
import coil.ImageLoader
import coil.request.CachePolicy
import coil.util.CoilUtils
import com.jakewharton.threetenabp.AndroidThreeTen
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.di.DaggerAppComponent
import io.sevenb.terminal.domain.usecase.auth.LocalUserDataStoring
import io.sevenb.terminal.domain.usecase.safety.BootUpReceiver
import io.sevenb.terminal.utils.Logger
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.OkHttpClient
import timber.log.Timber
import zendesk.core.AnonymousIdentity
import zendesk.core.Identity
import zendesk.core.Zendesk
import zendesk.support.Support
import java.util.*
import javax.inject.Inject


class TerminalApplication : DaggerApplication() {

    private var bootReceiver: BroadcastReceiver? = null

    @Inject
    lateinit var localUserDataStoring: LocalUserDataStoring

    override fun onCreate() {
        super.onCreate()

        Timber.plant(Timber.DebugTree())
        AndroidThreeTen.init(this)

        initImageLoader()
        registerThisAppReceivers()
        initLogger()
    }

    private fun initLogger() {
        GlobalScope.launch {
            when (val localUserDataStoringResult =
                localUserDataStoring.restoreLocalAccountData()) {
                is Result.Success -> {
                    val email = localUserDataStoringResult.data.email
                    Logger.init(email)

                    val identity: Identity = AnonymousIdentity.Builder()
                        .withNameIdentifier(email)
                        .withEmailIdentifier(email)
                        .build()

                    Zendesk.INSTANCE.setIdentity(identity)

                    Zendesk.INSTANCE.init(applicationContext, "https://factory3192.zendesk.com",
                        "c4e1e420e34a874822eee5da493fd676613b7f8a22d6d18a",
                        "mobile_sdk_client_c69953852154db0ebfc5"
                    )


                    Zendesk.INSTANCE.setIdentity(identity)

                    Support.INSTANCE.init(Zendesk.INSTANCE)

                    Support.INSTANCE.helpCenterLocaleOverride = Locale("ru")

                }
            }
        }
    }

    private fun registerThisAppReceivers() {
        bootReceiver = BootUpReceiver()
        registerReceiver(bootReceiver, IntentFilter(Intent.ACTION_BOOT_COMPLETED))
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.factory().create(this)
    }

    private fun initImageLoader() {
        val imageLoader = ImageLoader.Builder(this)
            .availableMemoryPercentage(0.2)
            .okHttpClient {
                OkHttpClient.Builder()
                    .cache(CoilUtils.createDefaultCache(this))
                    .build()
            }
            .diskCachePolicy(CachePolicy.ENABLED)
            .memoryCachePolicy(CachePolicy.ENABLED)
            .networkCachePolicy(CachePolicy.ENABLED)
            .addLastModifiedToFileCacheKey(true)
            .build()
        Coil.setImageLoader(imageLoader)
    }

}
