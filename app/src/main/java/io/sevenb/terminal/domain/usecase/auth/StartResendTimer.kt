package io.sevenb.terminal.domain.usecase.auth

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch

class StartResendTimer {

    private val timerValueChannel = BroadcastChannel<Int>(Channel.CONFLATED)
    val timerValueFlow = timerValueChannel.asFlow()

    lateinit var timerJob: Job
    private lateinit var localTimerJob: Job

    suspend fun execute(
        coroutineScope: CoroutineScope
    ) {
        localTimerJob = coroutineScope.launch {
            flow {
                for (i in 59 downTo 0) {
                    delay(1000)
                    emit(i)
                }
            }.collect {
                timerValueChannel.offer(it)
            }
        }
    }

    fun cancelTimerJob() {
        if (::timerJob.isInitialized && timerJob.isActive)
            timerJob.cancel()

        if (::localTimerJob.isInitialized && localTimerJob.isActive)
            localTimerJob.cancel()
    }
}
