package io.sevenb.terminal.domain.usecase.sort

import io.sevenb.terminal.domain.usecase.charts.AccountData
import io.sevenb.terminal.ui.screens.select_currency.BaseCurrencyItem
import io.sevenb.terminal.ui.screens.select_currency.BaseSelectItem
import io.sevenb.terminal.ui.screens.select_currency.SelectCurrencyFragment

class SortCurrenciesHandler {

    fun <T : BaseCurrencyItem> sortByTicker(list: List<T>): List<T> {
        return list.sortedWith(compareBy {
            mapTickerToCmcRank.getOrElse(
                it.ticker,
                { mapTickerToCmcRank.size + 1 })
        })
    }

    fun <T : BaseSelectItem> makeSingleBTCTicker(list: List<T>): MutableList<T> {
        val tempList = mutableListOf<T>()
        list.forEach {
            if (it.ticker == AccountData.BTC) {
                if (it.network == AccountData.BTC) {
                    tempList.add(it)
                }
            } else tempList.add(it)
        }
        return tempList
    }

    fun <T : BaseSelectItem> filterBnbBscNetwork(list: List<T>): List<T> {
        val tempList = mutableListOf<T>()
        list.forEach {
            if (it.ticker == it.network) tempList.add(it)
            else if (it.network != SelectCurrencyFragment.BNB_NETWORK &&
                it.network != SelectCurrencyFragment.BSC_NETWORK
            ) tempList.add(it)
        }
        return tempList
    }

    companion object {
        private val mapTickerToCmcRank = mapOf(
            "BTC" to 1,
            "ETH" to 2,
            "USDT" to 3,
            "XRP" to 4,
            "LTC" to 5,
            "BCH" to 6,
            "ADA" to 7,
            "BNB" to 8,
            "DOT" to 9,
            "XVG" to 10,
            "LINK" to 11,
            "USDC" to 12,
            "XLM" to 13,
            "BSV" to 14,
            "WBTC" to 15,
            "XMR" to 16,
            "EOS" to 17,
            "XEM" to 18,
            "TRX" to 19,
            "XTZ" to 20,
            "ADX" to 21,
            "THETA" to 22,
            "LEO" to 23,
            "CRO" to 24,
            "DAI" to 25,
            "FIL" to 26,
            "DASH" to 27,
            "NEO" to 28,
            "REV" to 29,
            "ATOM" to 30,
            "SNX" to 31,
            "AAVE" to 32,
            "VET" to 33,
            "UNI" to 34,
            "CEL" to 35,
            "BUSD" to 36,
            "ZIL" to 37,
            "HT" to 38,
            "MIOTA" to 39,
            "ZEC" to 40,
            "YFI" to 41,
            "WAVES" to 42,
            "ETC" to 43,
            "DOGE" to 44,
            "COMP" to 45,
            "GRT" to 46,
            "MKR" to 47,
            "FTT" to 48,
            "DCR" to 49,
            "UMA" to 50,
            "SUSHI" to 51,
            "KSM" to 52,
            "OKB" to 53,
            "ALGO" to 54,
            "ONT" to 55,
            "RENBTC" to 56,
            "OMG" to 57,
            "DGB" to 58,
            "NEXO" to 59,
            "EGLD" to 60,
            "BAT" to 61,
            "CHSB" to 62,
            "BTT" to 63,
            "TUSD" to 64,
            "LUNA" to 65,
            "ZRX" to 66,
            "PAX" to 67,
            "ICX" to 68,
            "REN" to 69,
            "AVAX" to 70,
            "STX" to 71,
            "QTUM" to 72,
            "HBAR" to 73,
            "EWT" to 74,
            "AMPL" to 75,
            "NEAR" to 76,
            "ABBC" to 77,
            "LRC" to 78,
            "CELO" to 79,
            "REP" to 80,
            "HEDG" to 81,
            "UST" to 82,
            "HUSD" to 83,
            "KNC" to 84,
            "MAID" to 85,
            "RSR" to 86,
            "LSK" to 87,
            "BTG" to 88,
            "TFUEL" to 89,
            "SC" to 90,
            "QNT" to 91,
            "RUNE" to 92,
            "NANO" to 93,
            "OCEAN" to 94,
            "CVT" to 95,
            "ZB" to 96,
            "ZEN" to 97,
            "BNT" to 98,
            "MANA" to 99,
            "CHZ" to 100,
            "ANT" to 101,
            "ENJ" to 102
        )
    }
}