package io.sevenb.terminal.domain.usecase.auth

import io.sevenb.terminal.data.model.auth.FirebaseMessageToken
import io.sevenb.terminal.data.model.auth.ResponseBoolean
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.enum_model.ErrorMessageType
import io.sevenb.terminal.data.repository.auth.FirebaseAuthRepository
import io.sevenb.terminal.data.repository.auth.UserAuthRepository
import io.sevenb.terminal.exceptions.NewAuthException
import io.sevenb.terminal.utils.Logger
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext

class ProcessTokens(
    private val userAuthRepository: UserAuthRepository,
    private val firebaseAuthRepository: FirebaseAuthRepository,
    private val storeRefreshToken: StoreRefreshToken
) {

    suspend fun sendMessageToken(token: String): Result<ResponseBoolean> {
        val messageToken = FirebaseMessageToken(token)
        return withContext(IO) { userAuthRepository.sendMessageToken(messageToken) }
    }

    suspend fun processTokens(
        email: String,
        password: String,
        accessToken: String,
        refreshToken: String,
    ): Result<Unit> {

        UserAuthRepository.accessToken = accessToken
        UserAuthRepository.refreshToken = refreshToken

        return withContext(IO) {
            when (val getMessageTokenResult =
                firebaseAuthRepository.getMessageToken(email, password)) {
                is Result.Success -> {
                    when (val sendMessageTokenResult =
                        sendMessageToken(getMessageTokenResult.data)) {
                        is Result.Success -> {
                            if (sendMessageTokenResult.data.result) {
                                Logger.sendLog(
                                    "ProcessTokens",
                                    "processTokens",
                                    "sendMessageToken = true"
                                )
                                return@withContext storeRefreshToken(refreshToken)
                            } else {
                                val exception = NewAuthException(ErrorMessageType.LOGIN_ERROR)
                                Logger.sendLog(
                                    "ProcessTokens",
                                    "processTokens sendMessageToken",
                                    exception
                                )
                                return@withContext Result.Error(exception)
                            }
                        }
                        is Result.Error -> {
                            val exception = sendMessageTokenResult.exception
                            Logger.sendLog("ProcessTokens", "processTokens", exception)
                            return@withContext Result.Error(exception)
                        }
                    }
                }
                is Result.Error -> {
                    val exception = getMessageTokenResult.exception
                    Logger.sendLog("ProcessTokens", "processTokens getMessageToken", exception)
                    return@withContext Result.Error(exception)
                }
            }
        }
    }

    suspend fun storeRefreshToken(refreshToken: String): Result<Unit> {
        return withContext(IO) {
            when (storeRefreshToken.execute(refreshToken)) {
                is Result.Success -> {
                    Result.Success(Unit)
                }
                else -> {
                    Result.Error(NewAuthException(ErrorMessageType.REFRESH_TOKEN))
                }
            }
        }
    }

    suspend fun verifyToken() = userAuthRepository.verifyToken()

}
