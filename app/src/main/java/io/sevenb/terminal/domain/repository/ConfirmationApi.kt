package io.sevenb.terminal.domain.repository

import io.sevenb.terminal.data.model.broker_api.CaptchaGet
import io.sevenb.terminal.data.model.broker_api.CaptchaPost
import io.sevenb.terminal.data.model.core.Result


interface ConfirmationApi {

    suspend fun getCaptchaData(time: String): Result<CaptchaGet>
    suspend fun postCaptchaData(
        validate: String,
        secCode: String,
        challenge: String
    ): Result<CaptchaPost>

}