package io.sevenb.terminal.domain.usecase.order

import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.order.OrderItem
import io.sevenb.terminal.domain.usecase.charts.AccountData
import io.sevenb.terminal.domain.usecase.historyfilter.HistoryFilterProcessing
import io.sevenb.terminal.ui.screens.order.OrderBottomSheetFragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class OrderAvailability(
    private val accountData: AccountData
) {

    /**
     * For side BUY true if there are no balances.
     * For side SELL true if there is no selected currency balance.
     */
    suspend fun isBalanceNotAvailable(
        from: String,
        to: String,
        orderSide: OrderBottomSheetFragment.Companion.OrderSide
    ): Boolean {
        return withContext(Dispatchers.IO) {
            when (val balances = accountData.cacheFirstBalances()) {
                is Result.Success -> {
                    val listOfBalances = balances.data
                    when (orderSide) {
                        OrderBottomSheetFragment.Companion.OrderSide.BUY -> {
                            val balance = listOfBalances.find { it.ticker.equals(to, true) }
                            val available = balance?.available?.toFloatOrNull() ?: 0.0f
                            return@withContext balance == null || available == 0.0f
                        }
                        OrderBottomSheetFragment.Companion.OrderSide.SELL -> {
                            val balance = listOfBalances.find { it.ticker.equals(from, true) }
                            return@withContext balance == null
                        }
                    }
                }
                is Result.Error -> {
                    // in case balances from server not received
                    return@withContext false
                }
            }
        }
    }

    fun hasProcessingWithdrawOrders(list: List<OrderItem>): Boolean {
        if (list.isNullOrEmpty()) return false
        return list.any { it.status == HistoryFilterProcessing.pending }
    }

}
