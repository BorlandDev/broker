package io.sevenb.terminal.domain.usecase.charts

import com.github.mikephil.charting.data.Entry
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.trading.TradeChartParams
import io.sevenb.terminal.data.model.trading.TradeChartRange
import io.sevenb.terminal.data.repository.trading.HistoryChartDataRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.IOException

class ChartData(
    private val historyChartDataRepository: HistoryChartDataRepository
) {

    suspend fun requestChartData(
        ticker: String,
        baseCurrency: String,
        tradeChartParams: TradeChartParams
    ): Result<List<Entry>> {
        return withContext(Dispatchers.IO) {
            val chartDataResult = when (tradeChartParams.tradeChartRange) {
                TradeChartRange.DAY -> historyChartDataRepository.getHourlyPair(ticker, baseCurrency, ONE_DAY)
                TradeChartRange.WEEK -> historyChartDataRepository.getHistoday(ticker, baseCurrency, SEVEN_DAYS, 1, false)
                TradeChartRange.MONTH -> historyChartDataRepository.getHistoday(ticker, baseCurrency, THIRTY_DAYS, 1, false)
                TradeChartRange.THREE_MONTH -> historyChartDataRepository.getHistoday(ticker, baseCurrency, NINETY_DAYS, 1, false)
                TradeChartRange.YEAR -> historyChartDataRepository.getHistoday(ticker, baseCurrency, ONE_YEAR, 4, false)
                TradeChartRange.ALL -> historyChartDataRepository.getHistoday(ticker, baseCurrency, ALL_DATA, 1, true)
            }

            when (chartDataResult) {
                is Result.Success -> {
                    val listEntries = mutableListOf<Entry>()
                    chartDataResult.data.resultData.data?.forEachIndexed { _, cryptoCmpModel ->
                        listEntries.add(Entry(cryptoCmpModel.time.toFloat(), cryptoCmpModel.close))
                    }
                    return@withContext Result.Success(listEntries)
                }
                is Result.Error -> Result.Error(IOException("Can't get chart data"))
            }
        }
    }

    companion object {
        const val ONE_DAY = 24
        const val SEVEN_DAYS = 7
        const val THIRTY_DAYS = 30
        const val NINETY_DAYS = 90
        const val ONE_YEAR = 91
        const val ALL_DATA = 100
    }

}
