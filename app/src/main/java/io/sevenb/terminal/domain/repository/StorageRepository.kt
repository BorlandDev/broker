package io.sevenb.terminal.domain.repository

import io.sevenb.terminal.data.model.core.Result

interface StorageRepository {

    var accessToken: String?


    suspend fun clearSharedPreferences(): Result<Unit>
    suspend fun clearBiometry(): Result<Unit>


    suspend fun storeRefreshToken(refreshToken: String): Result<Unit>
    suspend fun restoreRefreshToken(): Result<String>
    suspend fun storeEncryptedPassword(encryptedPassword: String): Result<Unit>
    suspend fun restoreEncryptedPassword(): Result<String>

    suspend fun restoreListOfOrderSymbols(): Result<String>
    suspend fun storeListOfOrderSymbols(listOfOrderSymbols: String): Result<Unit>

    suspend fun storeLocalAccountData(localAccountDataString: String): Result<Unit>
    suspend fun restoreLocalAccountData(): Result<String>

    suspend fun storeFavoriteTickers(favoriteCurrencies: String): Result<Unit>
    suspend fun restoreFavoriteTickers(): Result<String>

    suspend fun storeBaseCurrency(baseCurrency: String): Result<Unit>
    suspend fun restoreBaseCurrency(): Result<String>

    suspend fun storeInfoConfirmationState(isShowDialog: Boolean): Result<Unit>
    suspend fun restoreInfoConfirmationState(): Result<Boolean>

    suspend fun restoreFirstLaunch(): Result<Boolean>
    suspend fun storeFirstLaunch(boolean: Boolean): Result<Unit>

    suspend fun restorePassCodeEnabled(): Result<Boolean>
    suspend fun storePassCodeEnabled(boolean: Boolean): Result<Unit>

    suspend fun restoreFingerprintEnabled(): Result<Boolean>
    suspend fun storeFingerprintEnabled(boolean: Boolean): Result<Unit>

    suspend fun restoreLoggedOut(): Result<Int>
    suspend fun storeLoggedOut(value: Int): Result<Unit>

    suspend fun restorePassCode(): Result<String>
    suspend fun storePassCode(passCode: String): Result<Unit>

    suspend fun restorePassword(): Result<String>
    suspend fun storePassword(password: String): Result<Unit>

    suspend fun restoreMinuteLock(): Result<Boolean>
    suspend fun storeMinuteLock(value: Boolean): Result<Unit>

    suspend fun restoreFiveMinutesLock(): Result<Boolean>
    suspend fun storeFiveMinutesLock(value: Boolean): Result<Unit>

    suspend fun restoreHalfHourLock(): Result<Boolean>
    suspend fun storeHalfHourLock(value: Boolean): Result<Unit>

    suspend fun restoreHourLock(): Result<Boolean>
    suspend fun storeHourLock(value: Boolean): Result<Unit>

    suspend fun restoreRebootLock(): Result<Boolean>
    suspend fun storeRebootLock(value: Boolean): Result<Unit>

    suspend fun restorePauseTime(): Result<String>
    suspend fun storePauseTime(value: String): Result<Unit>

    suspend fun restoreTimeoutEnabled(): Result<Boolean>
    suspend fun storeTimeoutEnabled(value: Boolean): Result<Unit>

    suspend fun restoreIsRebooted(): Result<Boolean>
    suspend fun storeIsRebooted(value: Boolean): Result<Unit>

    suspend fun restoreIsLocked(): Result<Boolean>
    suspend fun storeIsLocked(value: Boolean): Result<Unit>

    suspend fun restoreEnabledNotifications(): Result<String>
    suspend fun storeEnabledNotifications(notifications: String): Result<Unit>

    suspend fun restoreEnableNotifications(): Result<Int>
    suspend fun storeEnableNotifications(value: Int): Result<Unit>

    suspend fun restoreCountEnter(): Result<String>
    suspend fun incrementCountEnter(): Result<Unit>
    suspend fun resetCountEnter(): Result<Unit>

    suspend fun disableRateUs(): Result<Unit>
    suspend fun enableRateUs(): Result<Unit>

    suspend fun restoreCountTransactions(): Result<Boolean>
    suspend fun incrementCountTransactions(): Result<Unit>

    suspend fun setLastOpen(value: String): Result<Unit>
    suspend fun getLastOpen(): Result<String>

    suspend fun setOpenVersion(value: Int): Result<Unit>
    suspend fun getOpenVersion(): Result<Int>

    //ADDED START BY JIM IRS

    suspend fun getLastRateTime(): Result<String>
    suspend fun setLastRateTime(value: String): Result<Unit>

    suspend fun getOpenPortfolioCount(): Result<Int>
    suspend fun incrementOpenPortfolioCount(): Result<Unit>
    suspend fun getSuccessTradeCount(): Result<Int>
    suspend fun incrementSuccessTradeCount(): Result<Unit>
    suspend fun getDontAskVersion(): Result<Int>
    suspend fun setDontAskVersion(value: Int): Result<Unit>
    suspend fun getAskLaterTime(): Result<String>
    suspend fun setAskLaterTime(value: String): Result<Unit>


    //ADDED END

    suspend fun logOut(): Result<Unit>
}