package io.sevenb.terminal.domain.usecase.historyfilter

import io.sevenb.terminal.data.datasource.database.model.CurrencyEntity
import io.sevenb.terminal.data.model.broker_api.account.DepositHistoryItem
import io.sevenb.terminal.data.model.broker_api.rates.MarketRate
import io.sevenb.terminal.data.model.broker_api.rates.MarketRateParams
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.enum_model.HistoryType
import io.sevenb.terminal.data.model.order.OrderItem
import io.sevenb.terminal.data.model.withdrawal.WithdrawalRequest
import io.sevenb.terminal.data.repository.breakdown.BreakdownRepository
import io.sevenb.terminal.data.repository.database.DbRepository
import io.sevenb.terminal.domain.repository.RawDataRepository
import io.sevenb.terminal.domain.usecase.charts.AccountData
import io.sevenb.terminal.domain.usecase.deposit.RequestDepositHistory
import io.sevenb.terminal.domain.usecase.order.OrderOperations
import io.sevenb.terminal.domain.usecase.withdrawal.RequestWithdrawalHistory
import io.sevenb.terminal.ui.extension.getFloatValue
import io.sevenb.terminal.ui.screens.portfolio.PortfolioHistoryItem
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

class HistoryFilterProcessing(
    private val requestDepositHistory: RequestDepositHistory,
    private val requestWithdrawalHistory: RequestWithdrawalHistory,
    private val orderOperations: OrderOperations,
    private val accountData: AccountData,
    private val rawDataRepository: RawDataRepository,
    private val breakdownRepository: BreakdownRepository
) {

    private lateinit var historyTypes: MutableList<HistoryType>
    private lateinit var errorData: MutableList<String>

    private val pairs = mutableListOf<Pair<String?, String?>>()
    private val orderRates = mutableListOf<MarketRate?>()
    private val withdrawRates = mutableListOf<MarketRate?>()
    private val depositRates = mutableListOf<MarketRate?>()
    private var savedCurrencies = mutableMapOf<String, String>()

    private lateinit var tempResponse: Pair<List<PortfolioHistoryItem>, List<String>>
    private var lastUpdateTime: Long = 0

    suspend fun getFilteredHistory(
        historyTypes: MutableList<HistoryType>,
        list: List<CurrencyEntity>
    ): Pair<List<PortfolioHistoryItem>, List<String>> {
        if (historyTypes.isEmpty()) return Pair(emptyList(), emptyList())

        if (System.currentTimeMillis() - lastUpdateTime < 3000 && ::tempResponse.isInitialized)
            return tempResponse

        this.historyTypes = historyTypes
        list.forEach {
            savedCurrencies[it.ticker] = it.ticker
        }
        errorData = mutableListOf()

        val withdrawalHistory = GlobalScope.async { withdrawAsync() }
        val ordersHistory = GlobalScope.async { orderAsync() }
        val depositHistory = GlobalScope.async { depositAsync() }

        return Pair(
            constructList(
                withdrawalHistory.await(),
                ordersHistory.await(),
                depositHistory.await(),
                pairs,
                orderRates,
                depositRates,
                withdrawRates
            ),
            errorData.toMutableList()
        )
    }

    private suspend fun withdrawAsync(): List<WithdrawalRequest> {
        return if (containsWithdraw()) {
            val withdraws = getData(::getWithdrawalResult)
            val listOfParams = mutableListOf<MarketRateParams>()
            withdraws.forEach {
                it.ticker?.let { ticker ->
                    listOfParams.add(
                        MarketRateParams(ticker, AccountData.getBaseCurrencyTicker())
                    )
                }
            }

            when (val pricesResult = accountData.getPrices(listOfParams)) {
                is Result.Success -> withdrawRates.addAll(pricesResult.data)
            }

            withdraws
        } else emptyList()
    }

    private suspend fun depositAsync(): List<DepositHistoryItem> {
        return if (containsDeposit()) {
            val deposits = getData(::getDepositsResult)

            val listOfParams = mutableListOf<MarketRateParams>()
            deposits.forEach {
                listOfParams.add(MarketRateParams(it.ticker, AccountData.getBaseCurrencyTicker()))
            }

            when (val pricesResult = accountData.getPrices(listOfParams)) {
                is Result.Success -> depositRates.addAll(pricesResult.data)
            }

            deposits
        } else emptyList()
    }

    private suspend fun orderAsync(): List<OrderItem> {
        return if (containsOrders()) {
            val orders = getData(::getOrdersResult)

            val listOfParams = mutableListOf<MarketRateParams>()
            breakdownRepository.initSavedCurrencies(savedCurrencies)
            orders.forEach {
                val pair = breakdownRepository.breakdown(it.symbol)
                pairs.add(pair)
                pair.first?.let { firstTicker ->
                    listOfParams.add(
                        MarketRateParams(firstTicker, AccountData.getBaseCurrencyTicker())
                    )
                }
            }
            breakdownRepository.deleteSavedCurrencies()

            when (val pricesResult = accountData.getPrices(listOfParams)) {
                is Result.Success -> orderRates.addAll(pricesResult.data)
            }

            orders
        } else emptyList()
    }

    private fun constructList(
        withdrawalHistory: List<WithdrawalRequest>,
        orderHistory: List<OrderItem>,
        depositHistory: List<DepositHistoryItem>,
        listOfPairs: MutableList<Pair<String?, String?>>,
        listOfOrderRates: MutableList<MarketRate?>,
        listOfDepositRates: MutableList<MarketRate?>,
        lisOfWithdrawalRates: MutableList<MarketRate?>
    ): List<PortfolioHistoryItem> {
        val withdrawalList = constructWithdraw(withdrawalHistory, lisOfWithdrawalRates)
        val ordersList = constructOrders(orderHistory, listOfPairs, listOfOrderRates)
        val depositList = constructDeposit(depositHistory, listOfDepositRates)

        val portfolioHistoryItemList = mutableListOf<PortfolioHistoryItem>().apply {
            addAll(withdrawalList)
            addAll(ordersList)
            addAll(depositList)
            sortBy { it.createdAt }
            reverse()
        }

        tempResponse = Pair(portfolioHistoryItemList, errorData.toMutableList())
        lastUpdateTime = System.currentTimeMillis()

        return portfolioHistoryItemList
    }

    private fun constructWithdraw(
        withdrawalHistory: List<WithdrawalRequest>,
        listOfRates: MutableList<MarketRate?>
    ): List<PortfolioHistoryItem> {
        return withdrawalHistory.mapIndexed { index, withdrawalRequest ->
            val marketRate = listOfRates[index]
            val rate = marketRate?.rate

            val estimated = rate?.let {
                if (withdrawalRequest.amount != null)
                    it * withdrawalRequest.amount
                else 0.0f
            }

            val status = withdrawalRequest.status?.let { status -> getStatus(status) }
            PortfolioHistoryItem(
                withdrawalRequest.txId ?: "",
                withdrawalRequest.ticker ?: "",
                withdrawalRequest.amount?.toString() ?: "",
                withdrawalRequest.createdAt ?: "",
                type = HistoryType.WITHDRAW,
                status = status ?: "",
                cmcIconId = rawDataRepository.mapTickerToCmcIconId?.get(withdrawalRequest.ticker)
                    ?: 0,
                estimatedInBase = estimated,
                address = withdrawalRequest.address
            )
        }
    }

    private fun constructDeposit(
        depositHistory: List<DepositHistoryItem>,
        listOfRates: MutableList<MarketRate?>
    ): List<PortfolioHistoryItem> {
        return depositHistory.mapIndexed { index, depositHistoryItem ->
//            val status = getStatus(depositHistoryItem.status)

            val marketRate = listOfRates[index]
            val rate = marketRate?.rate

            val estimated = rate?.let {
                it * depositHistoryItem.amount.getFloatValue()
            }

            PortfolioHistoryItem(
                depositHistoryItem.txId,
                depositHistoryItem.ticker,
                depositHistoryItem.amount,
                depositHistoryItem.createdAt,
                type = HistoryType.DEPOSIT,
//                status = status,
                status = finishedTitle,
                cmcIconId = rawDataRepository.mapTickerToCmcIconId?.get(depositHistoryItem.ticker)
                    ?: 0,
                estimatedInBase = estimated
            )
        }
    }

    private fun constructOrders(
        orderHistory: List<OrderItem>,
        listOfPairs: MutableList<Pair<String?, String?>>,
        listOfRates: MutableList<MarketRate?>
    ): List<PortfolioHistoryItem> {
        val setOf = mutableSetOf<String>()
        val ordersList = orderHistory.mapIndexed { index, orderItem ->
            val status = getStatus(orderItem.status)
            setOf.add(orderItem.status)

            val firstTicker = listOfPairs[index].first
            val secondTicker = listOfPairs[index].second

            val marketRate = listOfRates[index]
            val rate = marketRate?.rate

            val estimated = rate?.let {
                it * orderItem.executedAmount
            }?.toFloat()

            PortfolioHistoryItem(
                orderItem.side,
                orderItem.symbol,
                orderItem.executedAmount.toString(),
                orderItem.createdAt,
                type = HistoryType.ORDERS,
                status = status,
                firstTicker = firstTicker,
                secondTicker = secondTicker,
                firstTickerIconId = rawDataRepository.mapTickerToCmcIconId?.get(firstTicker) ?: 0,
                secondTickerIconId = rawDataRepository.mapTickerToCmcIconId?.get(secondTicker) ?: 0,
                estimatedInBase = estimated,
                commisison = orderItem.binanceCommission,
                cummulativeQuoteAmount = orderItem.cummulativeQuoteAmount
            )
        }

        return if (historyTypes.contains(HistoryType.CANCELLED_ORDER)) {
            if (historyTypes.contains(HistoryType.ORDERS)) {
                ordersList
            } else ordersList.filter { it.status == cancelled }//|| it.amount == "0.0" || it.amount == "0" || it.amount == "null" }
        } else {
            ordersList.filter { it.amount != "0.0" && it.status != cancelled }
        }
    }

    private suspend fun <T> getData(call: suspend () -> Result<List<T>>): List<T> {
        var data = emptyList<T>()

        when (val result = call()) {
            is Result.Success -> data = result.data
            is Result.Error -> result.message.let { errorData.add(it) }
        }

        return data
    }

    private suspend fun getWithdrawalResult() = requestWithdrawalHistory.execute()
    private suspend fun getOrdersResult() = orderOperations.ordersHistory()
    private suspend fun getDepositsResult() = requestDepositHistory.execute()

    private fun containsWithdraw() = historyTypes.contains(HistoryType.WITHDRAW)
    private fun containsOrders() =
        historyTypes.contains(HistoryType.ORDERS) || historyTypes.contains(HistoryType.CANCELLED_ORDER)

    private fun containsDeposit() = historyTypes.contains(HistoryType.DEPOSIT)

    private fun getStatus(status: String) = when (status) {
        pending -> pendingTitle
        not_confirmed -> not_confirmedTitle
        failed -> failedTitle
        finished -> finishedTitle
        successful -> successfulTitle
        cancelled -> cancelledTitle
        processing -> processingTitle
        filled -> finishedTitle
        else -> ""
    }

    companion object {

        private const val pendingTitle = "Pending"
        private const val not_confirmedTitle = "Not confirmed"
        private const val failedTitle = "Failed"
        private const val finishedTitle = "Finished"
        private const val successfulTitle = "Successful"
        private const val cancelledTitle = "Cancelled"
        private const val processingTitle = "Processing"
        private const val filledTitle = "Filled"

        const val pending = "pending"
        private const val not_confirmed = "not_confirmed"
        private const val failed = "failed"
        private const val processing = "processing"
        private const val finished = "finished"
        private const val successful = "successful"
        private const val cancelled = "CANCELED"
        private const val filled = "FILLED"
    }
}