package io.sevenb.terminal.domain.repository

import io.sevenb.terminal.data.model.broker_api.rates.MarketRate
import io.sevenb.terminal.data.model.broker_api.rates.MarketRateParams
import io.sevenb.terminal.data.model.core.Result


interface RatesApi {

    var mapOfRates: MutableMap<String, Float>

    suspend fun batchEstimates(listMarketRateParams: List<MarketRateParams>): Result<List<MarketRate>>

    fun addNewRates(listOfMarketRates: List<MarketRate?>)

}