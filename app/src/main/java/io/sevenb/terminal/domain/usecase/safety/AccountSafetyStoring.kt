package io.sevenb.terminal.domain.usecase.safety

import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.repository.auth.PreferenceRepository

class AccountSafetyStoring(
    private val preferenceRepository: PreferenceRepository
) {

    suspend fun storeFingerprintEnabled(boolean: Boolean): Result<Unit> {
        return preferenceRepository.storeFingerprintEnabled(boolean)
    }

    suspend fun restoreFingerprintEnabled() = preferenceRepository.restoreFingerprintEnabled()

    suspend fun storePassCodeEnabled(boolean: Boolean): Result<Unit> {
        return preferenceRepository.storePassCodeEnabled(boolean)
    }

    suspend fun restorePassCodeEnabled() = preferenceRepository.restorePassCodeEnabled()

    suspend fun storeLoggedOut(value: Int): Result<Unit> {
        return preferenceRepository.storeLoggedOut(value)
    }

    suspend fun restoreLoggedOut() = preferenceRepository.restoreLoggedOut()

    suspend fun restoreMinuteLock() = preferenceRepository.restoreMinuteLock()
    suspend fun storeMinuteLock(value: Boolean) = preferenceRepository.storeMinuteLock(value)

    suspend fun restoreFiveMinutesLock() = preferenceRepository.restoreFiveMinutesLock()
    suspend fun storeFiveMinutesLock(value: Boolean) =
        preferenceRepository.storeFiveMinutesLock(value)

    suspend fun restoreHalfHourLock() = preferenceRepository.restoreHalfHourLock()
    suspend fun storeHalfHourLock(value: Boolean) = preferenceRepository.storeHalfHourLock(value)

    suspend fun restoreHourLock() = preferenceRepository.restoreHourLock()
    suspend fun storeHourLock(value: Boolean) = preferenceRepository.storeHourLock(value)

    suspend fun restoreRebootLock() = preferenceRepository.restoreRebootLock()
    suspend fun storeRebootLock(value: Boolean) = preferenceRepository.storeRebootLock(value)

    suspend fun removeAllLocks(): Result<Unit> {
        storeMinuteLock(false)
        storeFiveMinutesLock(false)
        storeHalfHourLock(false)
        storeHourLock(false)
        storeRebootLock(false)

        return Result.Success(Unit)
    }

    suspend fun storePauseTime(value: String) = preferenceRepository.storePauseTime(value)
    suspend fun restorePauseTime() = preferenceRepository.restorePauseTime()

    suspend fun storeTimeoutEnabled(value: Boolean) =
        preferenceRepository.storeTimeoutEnabled(value)
    suspend fun restoreTimeoutEnabled() = preferenceRepository.restoreTimeoutEnabled()

    suspend fun storeIsRebooted(value: Boolean) = preferenceRepository.storeIsRebooted(value)
    suspend fun restoreIsRebooted() = preferenceRepository.restoreIsRebooted()

    suspend fun storeIsLocked(value: Boolean) = preferenceRepository.storeIsLocked(value)
    suspend fun restoreIsLocked() = preferenceRepository.restoreIsLocked()

    suspend fun logOut() = preferenceRepository.logOut()

}