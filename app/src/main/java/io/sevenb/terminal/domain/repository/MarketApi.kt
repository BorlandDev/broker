package io.sevenb.terminal.domain.repository

import io.sevenb.terminal.data.model.broker_api.markets.Currency
import io.sevenb.terminal.data.model.broker_api.markets.Market
import io.sevenb.terminal.data.model.broker_api.rates.MarketRate
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.filter.Filters
import io.sevenb.terminal.data.model.order.OrderItem
import io.sevenb.terminal.data.model.order.OrderParams
import io.sevenb.terminal.data.model.symbols.SymbolsData


interface MarketApi {

    var listOfCurrencies: MutableList<Currency>
    var listOfMarkets: MutableList<Market>

    suspend fun currencies(): Result<List<Currency>>
    suspend fun markets(): Result<List<Market>>
    suspend fun orderPost(orderParams: OrderParams): Result<OrderItem>
    suspend fun marketRate(from: String, to: String): Result<MarketRate>
    suspend fun getFilters(symbol: String): Result<SymbolsData>

}