package io.sevenb.terminal.domain.usecase.deposit

import android.graphics.Bitmap
import android.graphics.Color
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.google.zxing.common.BitMatrix
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel
import io.sevenb.terminal.data.model.core.Result
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.util.*

class QrCodeBuilder {

    suspend fun execute(textString: String, width: Int = 300): Result<Bitmap> {
        return withContext(Dispatchers.IO) {
            val bitmap = bitmapFromText(textString, width)
            bitmap ?: return@withContext Result.Error(IllegalArgumentException("Can't create qr code"))
            Result.Success(bitmap)
        }
    }

    private fun bitmapFromText(textString: String?, width: Int): Bitmap? {
        val hintMap: MutableMap<EncodeHintType, Any> = EnumMap<EncodeHintType, Any>(
            EncodeHintType::class.java
        )
        hintMap[EncodeHintType.CHARACTER_SET] = "UTF-8"

        // Now with zxing version 3.2.1 you could change border size (white border size to just 1)
        hintMap[EncodeHintType.MARGIN] = 2 /* default = 4 */
        hintMap[EncodeHintType.ERROR_CORRECTION] = ErrorCorrectionLevel.L
        var bitMatrix: BitMatrix? = null
        try {
            bitMatrix = MultiFormatWriter().encode(
                textString,
                BarcodeFormat.QR_CODE,
                width, width, hintMap
            )
        } catch (ignored: IllegalArgumentException) {
            Timber.e("ignored=%s", ignored.message)
        } catch (e: WriterException) {
            Timber.e("WriterException=%s", e.message)
            e.printStackTrace()
        }

        bitMatrix ?: return null

        val bitMatrixWidth: Int = bitMatrix.width
        val bitMatrixHeight: Int = bitMatrix.height
        val pixels = IntArray(bitMatrixWidth * bitMatrixHeight)
        for (y in 0 until bitMatrixHeight) {
            val offset = y * bitMatrixWidth
            for (x in 0 until bitMatrixWidth) {
                pixels[offset + x] = if (bitMatrix.get(x, y)) Color.BLACK else Color.WHITE
            }
        }
        val bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_8888)
        bitmap.setPixels(pixels, 0, width, 0, 0, bitMatrixWidth, bitMatrixHeight)
        return bitmap
    }

}
