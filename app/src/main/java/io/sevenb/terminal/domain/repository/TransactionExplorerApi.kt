package io.sevenb.terminal.domain.repository

import io.sevenb.terminal.data.model.transactionexplorer.TransactionExplorerMask
import io.sevenb.terminal.data.model.core.Result

interface TransactionExplorerApi {

    suspend fun getTransactionMask(currency: String): Result<TransactionExplorerMask>
}