package io.sevenb.terminal.domain.usecase.auth

import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.domain.repository.StorageRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class StoreFirstLaunch(
    private val storageRepository: StorageRepository
) {
    suspend fun setFirstLaunch(boolean: Boolean = false): Result<Unit> {
        return withContext(Dispatchers.IO) {
            return@withContext storageRepository.storeFirstLaunch(boolean)
        }
    }

    suspend fun getFirstLaunch(): Result<Boolean> {
        return withContext(Dispatchers.IO) {
            return@withContext storageRepository.restoreFirstLaunch()
        }
    }
}