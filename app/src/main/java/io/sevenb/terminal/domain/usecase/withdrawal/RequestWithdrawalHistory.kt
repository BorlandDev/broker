package io.sevenb.terminal.domain.usecase.withdrawal

import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.withdrawal.WithdrawalRequest
import io.sevenb.terminal.domain.repository.AccountApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.IOException

class RequestWithdrawalHistory(
    private val accountApi: AccountApi
) {

    suspend fun execute() : Result<List<WithdrawalRequest>> {
        return withContext(Dispatchers.IO) {
            when(val withdrawalHistoryResult = accountApi.withdrawalHistory()) {
                is Result.Success -> Result.Success(withdrawalHistoryResult.data)
                is Result.Error -> Result.Error(IOException("Can't get withdrawal history"))
            }
        }
    }

}
