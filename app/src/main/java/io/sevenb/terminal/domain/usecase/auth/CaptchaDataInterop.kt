package io.sevenb.terminal.domain.usecase.auth

import io.sevenb.terminal.data.model.broker_api.CaptchaGet
import io.sevenb.terminal.data.model.broker_api.CaptchaPost
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.domain.repository.ConfirmationApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONException
import org.json.JSONObject
import timber.log.Timber

class CaptchaDataInterop(
    private val confirmationApi: ConfirmationApi
) {

    suspend fun getCaptchaData(): Result<CaptchaGet> {
        return withContext(Dispatchers.IO) {
            val timeString = System.currentTimeMillis().toString()
            confirmationApi.getCaptchaData(timeString)
        }
    }

    suspend fun postCaptchaData(result: String): Result<CaptchaPost> {
        return withContext(Dispatchers.IO) {
            try {
                val jsonObject = JSONObject(result)
                val validate = jsonObject.getString(GEETEST_VALIDATE)
                val seccode = jsonObject.getString(GEETEST_SECCODE)
                val challenge = jsonObject.getString(GEETEST_CHALLENGE)
                return@withContext confirmationApi.postCaptchaData(validate, seccode, challenge)
            } catch (e: JSONException) {
                Timber.e("postCaptchaData: error - ${e.message}")
                return@withContext Result.Error(e)
            }
        }
    }

    companion object {
        val GEETEST_CHALLENGE = "geetest_challenge"
        val GEETEST_SECCODE = "geetest_seccode"
        val GEETEST_VALIDATE = "geetest_validate"
    }
}
