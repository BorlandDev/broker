package io.sevenb.terminal.domain.usecase.auth

import io.sevenb.terminal.data.model.auth.ResponseBoolean
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.enum_model.ErrorMessageType
import io.sevenb.terminal.data.repository.auth.FirebaseAuthRepository
import io.sevenb.terminal.data.repository.auth.UserAuthRepository
import io.sevenb.terminal.exceptions.NewAuthException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class SetNewPassword(
    private val userRepositoryRepository: UserAuthRepository,
//    private val firebaseAuthRepository: FirebaseAuthRepository,

    ) {

    suspend fun execute(
        login: String,
        newPassword: String,
        token: String
    ): Result<ResponseBoolean> {
        return withContext(Dispatchers.IO) {
            val newPasswordResult = userRepositoryRepository.setNewPassword(login, newPassword, token)

            return@withContext newPasswordResult

            //TODO REMOVE
//            if (newPasswordResult is Result.Success && newPasswordResult.data.result) {
//                firebaseAuthRepository.updatePassword(oldPassword, newPassword)
//
//                return@withContext Result.Success(newPasswordResult.data)
//            } else {
//                return@withContext Result.Error(NewAuthException(ErrorMessageType.LOG_OUT))
//            }
        }
    }
}