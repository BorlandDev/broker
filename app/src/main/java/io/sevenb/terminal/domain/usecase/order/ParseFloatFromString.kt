package io.sevenb.terminal.domain.usecase.order

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ParseFloatFromString {

    suspend fun execute(amountString: String): Float {
        return withContext(Dispatchers.IO) {
            val float = amountString.toFloatOrNull()
            return@withContext float ?: 0.0f
        }
    }

}
