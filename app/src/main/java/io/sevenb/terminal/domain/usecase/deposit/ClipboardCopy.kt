package io.sevenb.terminal.domain.usecase.deposit

import android.content.ClipData
import android.content.ClipDescription.MIMETYPE_TEXT_PLAIN
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.device.provider.DeviceManagerProvider

class ClipboardCopy(
    private val deviceManagerProvider: DeviceManagerProvider
) {

    fun copyToClipboard(text: String): Result<Unit> {
        val clipData = ClipData.newPlainText("Address", text)
        deviceManagerProvider.clipboardManager().setPrimaryClip(clipData)
        return Result.Success(Unit)
    }

    fun pasteIsEnabled() = when {
        !deviceManagerProvider.clipboardManager().hasPrimaryClip() -> false
        !(deviceManagerProvider.clipboardManager()
            .primaryClipDescription!!.hasMimeType(MIMETYPE_TEXT_PLAIN)) -> false
        else -> true
    }

    fun textToPaste(): String {
        if (pasteIsEnabled()) {
            val item = deviceManagerProvider.clipboardManager().primaryClip?.getItemAt(0)

            val pasteData = item?.text
            return pasteData?.toString() ?: ""
        }
        return ""
    }
}
