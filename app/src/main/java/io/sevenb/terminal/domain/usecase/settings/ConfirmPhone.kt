package io.sevenb.terminal.domain.usecase.settings

import io.sevenb.terminal.data.model.auth.SignUpVerify
import io.sevenb.terminal.data.model.broker_api.ConfirmPhoneResponse
import io.sevenb.terminal.data.model.broker_api.RequestTokenResponse
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.domain.repository.AuthApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ConfirmPhone(
    private val authApi: AuthApi
) {

    suspend fun requestConfirmPhoneCode(): Result<ConfirmPhoneResponse> {
        return withContext(Dispatchers.IO) {
            authApi.requestConfirmPhoneCode()
        }
    }

    suspend fun confirmPhoneWithCode(
        registrationToken: String,
        confirmationCode: String
    ): Result<RequestTokenResponse> {
        return withContext(Dispatchers.IO) {
            val signUpConfirm = SignUpVerify(registrationToken, confirmationCode)
            authApi.verifyCodeRegistration(signUpConfirm)
        }
    }

}
