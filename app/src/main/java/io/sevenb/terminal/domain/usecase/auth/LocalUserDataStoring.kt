package io.sevenb.terminal.domain.usecase.auth

import io.sevenb.terminal.BuildConfig
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.portfolio.LocalAccountData
import io.sevenb.terminal.data.repository.auth.PreferenceRepository
import io.sevenb.terminal.device.utils.DateTimeUtil
import io.sevenb.terminal.device.utils.Serializer
import io.sevenb.terminal.utils.ConstUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.util.*

class LocalUserDataStoring(
    private val preferenceRepository: PreferenceRepository
) {

    var localAccountData = LocalAccountData()

    suspend fun restoreLocalAccountData(): Result<LocalAccountData> {
        return withContext(Dispatchers.IO) {
            when (val accountStringResult = preferenceRepository.restoreLocalAccountData()) {
                is Result.Success -> {
                    val string = accountStringResult.data
                    if (string.isNotEmpty()) {
                        val accountData =
                            Serializer.deserialize(string, LocalAccountData::class.java)
                        localAccountData = accountData
                        return@withContext Result.Success(accountData)
                    } else {
                        return@withContext Result.Success(LocalAccountData())//Error(IOException("Can't get local account data"))
                    }
                }
                is Result.Error -> return@withContext accountStringResult
            }
        }
    }

    suspend fun updateLocalAccountData(
        email: String? = null,
        totalHoldingBalance: String? = null,
        availableBalance: String? = null,
        onOrdersBalance: String? = null,
        verificationToken: String? = null
    ) {
        withContext(Dispatchers.IO) {
            if (!email.isNullOrEmpty()) {
                localAccountData.email = email
            }
            if (!totalHoldingBalance.isNullOrEmpty()) {
                localAccountData.totalHoldingBalance = totalHoldingBalance
            }
            if (!availableBalance.isNullOrEmpty()) {
                localAccountData.availableBalance = availableBalance
            }
            if (!onOrdersBalance.isNullOrEmpty()) {
                localAccountData.onOrdersBalance = onOrdersBalance
            }
            if (!verificationToken.isNullOrEmpty()) {
                localAccountData.verificationToken = verificationToken
            }
            val accountString = Serializer.serialize(localAccountData, LocalAccountData::class.java)
            if (!accountString.isNullOrEmpty()) {
                preferenceRepository.storeLocalAccountData(accountString)
            }
        }
    }

    suspend fun deleteAllLocalUserData(): Result<Unit> {
        return withContext(Dispatchers.IO) {
            preferenceRepository.clearSharedPreferences()
        }
    }

    suspend fun storePassword(password: String) = preferenceRepository.storePassword(password)

    suspend fun restorePassword() = preferenceRepository.restorePassword()

    suspend fun clearBiometry(): Result<Unit> {
        return withContext(Dispatchers.IO) {
            preferenceRepository.clearBiometry()
        }
    }

//    suspend fun showRateUsOnMainScreen(): Result<Boolean> {
//        return withContext(Dispatchers.IO) {
//            preferenceRepository.incrementCountEnter()
//
//            val history = preferenceRepository.restoreCountEnter() as? Result.Success<String>
//                ?: Result.Success(false)
//
//            var arr = history.data.toString().split(" ")
//
//            val lastOpen: Result.Success<String> =
//                preferenceRepository.getLastOpen() as? Result.Success<String> ?: Result.Success("0")
//            val openVersion = preferenceRepository.getOpenVersion() as? Result.Success<Int>
//
//            Timber.d("showRateUsOnMainScreen: arr.size = ${arr.size} lastOpen = $lastOpen openVersion = $openVersion")
//            when {
//                arr.size <= 50 -> {
//                    Timber.d("showRateUsOnMainScreen: STEP 1")
//                    Result.Success(false)
//                }
//                (BuildConfig.VERSION_CODE <= (openVersion?.data ?: 1))
//                        || (Date().time - ((1000 * 60 * 60 * 24) * 10)) < lastOpen.data.toLong() -> {
//                    Timber.d("showRateUsOnMainScreen: STEP 2")
//                    Result.Success(false)
//                }
//                else -> {
//                    val startData = arr[arr.size - 1].toLong() - ((1000 * 60 * 60 * 24) * 5)
//                    val endData = arr[arr.size - 5].toLong()
//
//                    if (startData < endData) {
//                        Timber.d("showRateUsOnMainScreen: STEP 3")
//                        preferenceRepository.setLastOpen(Date().time.toString())
//                        preferenceRepository.setOpenVersion(BuildConfig.VERSION_CODE)
//                        preferenceRepository.resetCountEnter()
//                        Result.Success(true)
//                    } else {
//                        Timber.d("showRateUsOnMainScreen: STEP 4")
//                        Result.Success(false)
//                    }
//                }
//            }
//        }
//    }

//    suspend fun disableRateUs(): Result<Unit> {
//        return withContext(Dispatchers.IO) {
//            preferenceRepository.disableRateUs()
//
//        }
//    }
//
//    suspend fun enableRateUs(): Result<Unit> {
//        return withContext(Dispatchers.IO) {
//            preferenceRepository.enableRateUs()
//
//        }
//    }

//    suspend fun showRateUsAfterTransaction(): Result<Boolean> {
//        return withContext(Dispatchers.IO) {
//            val lastOpen: Result.Success<String> =
//                preferenceRepository.getLastOpen() as? Result.Success<String> ?: Result.Success("0")
//            val openVersion = preferenceRepository.getOpenVersion() as? Result.Success<Int>
//            if ((BuildConfig.VERSION_CODE <= (openVersion?.data
//                    ?: 1)) || (Date().time - ((1000 * 60 * 60 * 24) * 10)) < lastOpen.data.toLong()
//            ) {
//                Result.Success(false)
//            } else {
//                preferenceRepository.incrementCountTransactions()
//                return@withContext preferenceRepository.restoreCountTransactions()
//            }
//        }
//    }

//    suspend fun resetLastRateUs(): Result<Unit> {
//        return withContext(Dispatchers.IO) {
//            preferenceRepository.setOpenVersion(1)
//            preferenceRepository.setLastOpen("0")
//        }
//    }

    suspend fun setLastRateTime(lastRateTime: String?): Result<Unit> {
        return withContext(Dispatchers.IO) {
            preferenceRepository.setLastRateTime(lastRateTime ?: Date().time.toString())
        }
    }

    suspend fun isInAppReviewEnabled(): Result<Boolean> {
        return withContext(Dispatchers.IO) {

            val lastRateTime: Result.Success<String> =
                preferenceRepository.getLastRateTime() as? Result.Success<String>
                    ?: Result.Success("0")

            return@withContext Result.Success((Date().time - lastRateTime.data.toLong()) > ConstUtils.DAYS_30)
        }
    }

    suspend fun isShowRateUsOnPortfolio(): Result<Boolean> {
        return withContext(Dispatchers.IO) {

            val dontAskVersion = preferenceRepository.getDontAskVersion()
                    as? Result.Success<Int> ?: Result.Success(0)
            val askLaterTime = preferenceRepository.getAskLaterTime()
                    as? Result.Success<String> ?: Result.Success("0")

            Timber.d(
                "showRateUsOnPortfolio: " +
                        "dontAskVersion = ${dontAskVersion.data} " +
                        "askLaterTime = ${askLaterTime.data}"
            )

            if (BuildConfig.VERSION_CODE != dontAskVersion.data) {
                if (askLaterTime.data.toLong() != 0L && (Date().time - askLaterTime.data.toLong()) < ConstUtils.DAYS_10) {
                    return@withContext Result.Success(false)
                } else {
                    preferenceRepository.incrementOpenPortfolioCount()
                    val openOnPortfolioCount = preferenceRepository.getOpenPortfolioCount()
                            as? Result.Success<Int> ?: Result.Success(0)
                    if (openOnPortfolioCount.data >= 50) {
                        return@withContext Result.Success(true)
                    }
                }
            }
            return@withContext Result.Success(false)
        }
    }

    suspend fun setDontAskVersion(version: Int): Result<Unit> {
        return withContext(Dispatchers.IO) {
            preferenceRepository.setDontAskVersion(version)
        }
    }

    suspend fun setAskLaterTime(time: String): Result<Unit> {
        return withContext(Dispatchers.IO) {
            preferenceRepository.setAskLaterTime(time)
        }
    }

}
