package io.sevenb.terminal.domain.usecase.auth

import io.sevenb.terminal.data.model.auth.ErrorMessage
import io.sevenb.terminal.data.model.auth.ErrorUnconfirmed
import io.sevenb.terminal.device.utils.Serializer

class ParseErrorMessage {

    fun execute(rawMessage: String): ErrorMessage? {
        return Serializer.deserialize(rawMessage, ErrorMessage::class.java)
    }

    fun exec(rawMessage: String): ErrorUnconfirmed? {
        return Serializer.deserialize(rawMessage, ErrorUnconfirmed::class.java)
    }
}
