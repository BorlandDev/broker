package io.sevenb.terminal.domain.usecase.auth

import androidx.biometric.BiometricManager
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.repository.auth.PreferenceRepository
import io.sevenb.terminal.device.cryptography.CiphertextWrapper
import io.sevenb.terminal.device.cryptography.CryptographyManager
import io.sevenb.terminal.device.utils.Serializer
import java.security.KeyException
import java.security.ProviderException
import java.security.UnrecoverableKeyException
import javax.crypto.Cipher

class StorePassword(
    private val preferenceRepository: PreferenceRepository,
    private val cryptographyManager: CryptographyManager
) {

    fun provideEncryptionCipher(): Pair<String?, Cipher?> {
        return try {
            Pair(null, cryptographyManager.getInitializedCipherForEncryption())
        } catch (e: KeyException) {
            Pair(null, null)
        } catch (e: UnrecoverableKeyException) {
            Pair(null, null)
        } catch (e: ProviderException) {
            Pair(null, null)
        } catch (e: IllegalStateException) {
            Pair("Please activate fingerprints feature in your device settings.", null)
        } catch (e: java.security.InvalidAlgorithmParameterException) {
            Pair("Please activate fingerprints feature in your device settings.", null)
        } catch (e: Exception) {
            Pair("Please activate fingerprints feature in your device settings.", null)
        }
    }

    suspend fun encryptPassword(password: String, cipher: Cipher): Result<Unit> {
        return try {
            val ciphertextWrapper = cryptographyManager.encryptData(password, cipher)
            val serializedCiphertext =
                Serializer.serialize(ciphertextWrapper, CiphertextWrapper::class.java)
            serializedCiphertext
                ?: return Result.Error(IllegalArgumentException("Error while creating fingerprint"))
            preferenceRepository.storeEncryptedPassword(serializedCiphertext)
        } catch (e: Exception) {
            Result.Error(Exception("Something goes wrong"))
        }
    }

    suspend fun restoreEncryptedPassword(): Result<String> {
        return preferenceRepository.restoreEncryptedPassword()
    }

    fun getBiometricAuthenticateAbility() = cryptographyManager.getBiometricAuthenticateAbility()

    fun isBiometricSuccess() =
        getBiometricAuthenticateAbility() == BiometricManager.BIOMETRIC_SUCCESS

}
