package io.sevenb.terminal.domain.usecase.channelprovider

import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.asFlow

class ChannelProvider {

    fun <T> provideConflatedChannel() = BroadcastChannel<T>(Channel.CONFLATED)

    fun <T> provideFlow(channel: BroadcastChannel<T>) = channel.asFlow()
}