package io.sevenb.terminal.domain.usecase.auth

import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.repository.auth.PreferenceRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class RestoreRefreshToken(
    private val preferenceRepository: PreferenceRepository
) {

    suspend fun execute(): Result<String> {
        return withContext(Dispatchers.IO) {
            return@withContext preferenceRepository.restoreRefreshToken()
        }
    }
}
