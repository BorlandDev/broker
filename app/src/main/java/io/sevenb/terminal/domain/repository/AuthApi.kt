package io.sevenb.terminal.domain.repository

import io.sevenb.terminal.data.model.auth.*
import io.sevenb.terminal.data.model.broker_api.ConfirmPhoneResponse
import io.sevenb.terminal.data.model.broker_api.RefreshTokenRequest
import io.sevenb.terminal.data.model.broker_api.RequestTokenResponse
import io.sevenb.terminal.data.model.broker_api.TokensResponse
import io.sevenb.terminal.data.model.core.Result


interface AuthApi {

    suspend fun requestVerificationCodeRegistration(
        userData: UserData
    ): Result<RequestTokenResponse>

    suspend fun requestVerificationCodeRestorePassword(
        login: String
    ): Result<RequestTokenResponse>

    suspend fun verifyCodeRestorePassword(
        login: String, confirmationCode: String, token: String
    ): Result<RequestTokenResponse>

    suspend fun setNewPassword(
        login: String, password: String, token: String
    ): Result<ResponseBoolean>

    suspend fun verifyCodeRegistration(
        pSignUpVerify: SignUpVerify
    ): Result<RequestTokenResponse>

    suspend fun resendCode(
        signUpResend: SignUpResend
    ): Result<RequestTokenResponse>

    suspend fun signInUser(
        signInRequest: SignInRequest
    ): Result<TokensResponse>

    suspend fun sendMessageToken(messageToken: FirebaseMessageToken): Result<ResponseBoolean>
    suspend fun verifyToken(): Result<ResponseBoolean>
    suspend fun logout(): Result<ResponseBoolean>

    suspend fun refreshAccessToken(refreshTokenRequest: RefreshTokenRequest): Result<TokensResponse>

    suspend fun requestConfirmPhoneCode(): Result<ConfirmPhoneResponse>

//    suspend fun logOutUser(): Result<Unit>
}