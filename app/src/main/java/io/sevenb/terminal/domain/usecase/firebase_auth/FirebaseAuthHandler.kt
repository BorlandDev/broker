package io.sevenb.terminal.domain.usecase.firebase_auth

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.messaging.FirebaseMessaging
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.ui.extension.login
import io.sevenb.terminal.ui.extension.register
import io.sevenb.terminal.ui.extension.requestToken
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okio.IOException
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class FirebaseAuthHandler @Inject constructor(
    private val firebaseAuth: FirebaseAuth,
    private val firebaseMessaging: FirebaseMessaging
) {

    fun isSignedIn(email: String): Boolean =
        firebaseAuth.currentUser?.let { it.email == email } ?: false

    suspend fun register(email: String, password: String) = firebaseAuth.register(email, password)
    suspend fun login(email: String, password: String) = firebaseAuth.login(email, password)

    suspend fun getUserLoginToken(user: FirebaseUser): Result<String> =
        suspendCoroutine { continuation ->
            user.getIdToken(false).addOnSuccessListener { tokenResult ->
                tokenResult.token?.let { continuation.resume(Result.Success(it)) }
                    ?: continuation.resume(Result.Error(IOException("Can't get auth token")))
            }
                .addOnFailureListener {
                    continuation.resume(Result.Error(IOException("Can't get auth token")))
                }
        }

    suspend fun isNewEmail(email: String): Result<Boolean> =
        suspendCoroutine { continuation ->
            firebaseAuth.fetchSignInMethodsForEmail(email)
                .addOnCompleteListener { methodResult ->
                    val isNew = methodResult.result?.signInMethods?.isEmpty() ?: true
                    continuation.resume(
                        Result.Success(isNew)
                    )
                }.addOnFailureListener {
                    continuation.resume(Result.Success(true))
                }
        }

    suspend fun requestToken() = withContext(Dispatchers.IO) { firebaseMessaging.requestToken() }

    suspend fun logout() = withContext(Dispatchers.IO) { firebaseAuth.signOut() }

    suspend fun updatePassword(newPassword: String) =
        withContext(Dispatchers.IO) { firebaseAuth.currentUser?.updatePassword(newPassword) }
}