package io.sevenb.terminal.domain.usecase.deposit

import io.sevenb.terminal.data.model.broker_api.account.DepositHistoryItem
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.domain.repository.AccountApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.IOException

class RequestDepositHistory(
    private val accountApi: AccountApi
) {

    suspend fun execute() : Result<List<DepositHistoryItem>> {
        return withContext(Dispatchers.IO) {
            when(val depositAddressResult = accountApi.depositHistory()) {
                is Result.Success -> Result.Success(depositAddressResult.data)
                is Result.Error -> Result.Error(IOException("Can't get deposit history"))
            }
        }
    }

}
