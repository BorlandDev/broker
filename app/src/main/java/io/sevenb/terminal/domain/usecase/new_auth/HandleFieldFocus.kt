package io.sevenb.terminal.domain.usecase.new_auth

import android.view.View
import android.widget.EditText
import androidx.lifecycle.LifecycleObserver
import com.google.android.material.snackbar.Snackbar
import io.sevenb.terminal.ui.extension.focusField
import timber.log.Timber

class HandleFieldFocus : LifecycleObserver {

    private var snackbarIsShowing = false
    private var viewToFocus: EditText? = null
    private lateinit var snackbar: Snackbar

    fun init(baseView: View) {
        snackbar = Snackbar.make(baseView, "", Snackbar.LENGTH_LONG)
        snackbar.addCallback(object : Snackbar.Callback() {
            override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                super.onDismissed(transientBottomBar, event)
                viewToFocus?.let { it.focusField() }
                snackbarIsShowing = false
            }
        })
    }

    fun showSnackBar(message: String, field: EditText? = null) {
        if (::snackbar.isInitialized && !snackbarIsShowing) {
            snackbar.apply {
                setText(message)
                viewToFocus = field
                show()
                snackbarIsShowing = true
            }
        }
    }

    fun focusField(view: EditText?) {
        Timber.d("focusField $view")
        view?.let {
            if (!snackbarIsShowing) {
                it.focusField()
            } else {
                if (it != viewToFocus) {
                    viewToFocus = it
                }
            }
        }
    }

    fun clearFocusField(view: EditText?) {
        Timber.d("clearFocusField $view")
        view?.clearFocus()
        removeFocusField()
    }

    fun removeFocusField() {
        Timber.d("removeFocusField $viewToFocus")
        viewToFocus = null
    }
}