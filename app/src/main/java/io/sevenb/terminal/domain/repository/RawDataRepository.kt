package io.sevenb.terminal.domain.repository


interface RawDataRepository {

    var mapTickerToCmcIconId: Map<String, Int>?

    suspend fun mapTickerCmcIconId() : String

}