package io.sevenb.terminal.domain.usecase.safety

import android.content.Context
import android.content.Intent
import android.widget.Toast
import dagger.android.DaggerBroadcastReceiver
import kotlinx.coroutines.runBlocking
import javax.inject.Inject


class BootUpReceiver : DaggerBroadcastReceiver() {
    @Inject
    lateinit var safetyStoring: AccountSafetyStoring

    override fun onReceive(context: Context?, intent: Intent?) {
        super.onReceive(context, intent)
        runBlocking { safetyStoring.storeIsRebooted(true) }
    }
}