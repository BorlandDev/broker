package io.sevenb.terminal.domain.usecase.auth_back_stack

import androidx.lifecycle.LifecycleObserver
import io.sevenb.terminal.data.model.enums.auth.NewAuthStates
import io.sevenb.terminal.data.model.enums.auth.PasswordRestoringStates
import io.sevenb.terminal.data.model.enums.auth.RegistrationStates
import io.sevenb.terminal.data.model.enums.auth.PasswordChangeStates
import io.sevenb.terminal.data.model.enums.CommonState
import io.sevenb.terminal.data.model.enums.auth.TfaState

class AuthBackStackHandler : LifecycleObserver {

    private lateinit var globalState: CommonState
    private lateinit var subState: CommonState

    private val registrationBackStack = arrayOf(
        RegistrationStates.REGISTRATION_DATA,
        RegistrationStates.REGISTRATION_VERIFICATION_CODE
    )

    private val passwordRestoring = arrayOf(
        NewAuthStates.LOGIN,
        PasswordRestoringStates.PASSWORD_RESTORING_DATA,
        PasswordRestoringStates.PASSWORD_RESTORING_VERIFICATION_CODE,
        PasswordRestoringStates.PASSWORD_RESTORING_NEW_PASSWORD
    )

    private val passwordChange = arrayOf(
        PasswordChangeStates.PASSWORD_CHANGE_VERIFICATION_CODE,
        PasswordChangeStates.PASSWORD_CHANGE_NEW_PASSWORD,
    )

    private val continuesRegistration = arrayOf(
        NewAuthStates.LOGIN,
        NewAuthStates.CONTINUES_REGISTRATION
    )

    private val loginTfa = arrayOf(
        TfaState.INFORMATION,
        TfaState.SECRET,
        TfaState.VERIFICATION,
        TfaState.IS_ON,
        TfaState.DONE,
    )

//    private val tfa = arrayOf(
//        TfaState.INFORMATION,
//        TfaState.SECRET,
//        TfaState.VERIFICATION,
//        TfaState.DONE,
//    )

    fun setGlobalState(state: CommonState) {
        globalState = state
        if (state is NewAuthStates) {
            setInitialSubAuthState(state)
        }
    }

    private fun setInitialSubAuthState(state: CommonState) {
        subState = when (state) {
            NewAuthStates.REGISTRATION -> RegistrationStates.REGISTRATION_DATA
            NewAuthStates.PASSWORD_RESTORING -> PasswordRestoringStates.PASSWORD_RESTORING_DATA
            else -> NewAuthStates.LOGIN
        }
    }

    fun setSubAuthState(state: CommonState) {
        subState = state
    }

    fun availableBackNavigation(): Boolean {
        return if (!::globalState.isInitialized) false
        else
            when (globalState) {
                NewAuthStates.REGISTRATION -> subState != RegistrationStates.REGISTRATION_DATA
                NewAuthStates.LOGIN -> false
                NewAuthStates.PASSWORD_RESTORING -> true
                NewAuthStates.CONTINUES_REGISTRATION -> true
                NewAuthStates.CONTINUES_PASSWORD_RESTORING -> true
                NewAuthStates.TFA_CREATE -> true
                NewAuthStates.TFA_VERIFICATION -> true

                //TODO INCLUDE IN REFACTOR
                NewAuthStates.PASSWORD_CHANGE -> true
                NewAuthStates.CONTINUES_PASSWORD_CHANGE -> true

//                NewAuthStates.VERIFICATION_CODE -> true
                NewAuthStates.DONE_AUTH -> false

                else -> globalState is TfaState
            }
    }

    fun getPreviousEntry(): CommonState? {
        return try {
            when (globalState) {
                NewAuthStates.REGISTRATION -> if (subState != registrationBackStack[0]) registrationBackStack[0] else null
                NewAuthStates.PASSWORD_RESTORING,
                NewAuthStates.CONTINUES_PASSWORD_RESTORING -> if (subState != passwordRestoring[0]) getPasswordRestoreEntry() else null
                NewAuthStates.CONTINUES_REGISTRATION -> if (subState != continuesRegistration[0]) registrationBackStack[0] else null
                NewAuthStates.TFA_CREATE -> if (subState != loginTfa[0]) getTfaEntry() else loginTfa[0]
                NewAuthStates.TFA_VERIFICATION -> continuesRegistration[0]
//                NewAuthStates.PASSWORD_CHANGE,
//                NewAuthStates.CONTINUES_PASSWORD_CHANGE -> if (subState != passwordChange[0]) getPasswordChaneEntry() else null

                else ->
                    if (globalState is TfaState) {
                        globalState
                    } else {
                        null
                    }
            }
        } catch (e: Exception) {
            null
        }
    }

    private fun getPasswordRestoreEntry(): CommonState? {
        return when (subState) {
            PasswordRestoringStates.PASSWORD_RESTORING_DATA -> NewAuthStates.LOGIN
            PasswordRestoringStates.PASSWORD_RESTORING_VERIFICATION_CODE -> PasswordRestoringStates.PASSWORD_RESTORING_DATA
            PasswordRestoringStates.PASSWORD_RESTORING_NEW_PASSWORD -> PasswordRestoringStates.PASSWORD_RESTORING_VERIFICATION_CODE
            else -> null
        }
    }

    private fun getTfaEntry(): CommonState? {
        return when (subState) {
            TfaState.SECRET -> loginTfa[0]
            TfaState.VERIFICATION -> loginTfa[1]
            else -> null
        }
    }

//    private fun getPasswordChaneEntry(): CommonAuthState? {
//        return when (subState) {
//            PasswordChangeStates.PASSWORD_CHANGE_VERIFICATION_CODE -> NewAuthStates.SETTINGS
//            PasswordChangeStates.PASSWORD_CHANGE_NEW_PASSWORD -> PasswordChangeStates.PASSWORD_CHANGE_VERIFICATION_CODE
//            else -> null
//        }
//    }
}