package io.sevenb.terminal.domain.usecase.charts

import com.github.mikephil.charting.data.CandleEntry
import com.github.mikephil.charting.data.Entry

class ChartDataHandler {

    fun getChartEntries(candlesList: List<CandleEntry?>): List<Entry> {
        return candlesList.map {
            if (it?.x != null && it.close != null) Entry(it.x, it.close)
            else Entry(0F, 0F)
        }
    }
}