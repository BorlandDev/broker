package io.sevenb.terminal.domain.repository

import io.sevenb.terminal.data.model.ProfitLoss
import io.sevenb.terminal.data.model.broker_api.account.*
import io.sevenb.terminal.data.model.broker_api.markets.AccountBalance
import io.sevenb.terminal.data.model.broker_api.verification.Verification
import io.sevenb.terminal.data.model.broker_api.verification.VerificationToken
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.portfolio.LocalAccountData
import io.sevenb.terminal.data.model.withdrawal.ConfirmWithdrawalParams
import io.sevenb.terminal.data.model.withdrawal.WithdrawalHistoryItem
import io.sevenb.terminal.data.model.withdrawal.WithdrawalRequest
import io.sevenb.terminal.ui.screens.select_currency.DepositCurrencyItem


interface AccountApi {

    var localAccountData: LocalAccountData
    var listOfBalances: List<AccountBalance>

    suspend fun accountBalances(): Result<List<AccountBalance>>
    suspend fun depositAddress(currency: DepositCurrencyItem): Result<DepositAddress>
    suspend fun depositHistory(): Result<List<DepositHistoryItem>>
    suspend fun requestWithdrawal(withdrawalRequest: WithdrawalRequest): Result<WithdrawalHistoryItem>
    suspend fun confirmWithdrawal(confirmWithdrawalParams: ConfirmWithdrawalParams): Result<Unit>
    suspend fun withdrawalHash(id: String): Result<WithdrawalHistoryItem>
    suspend fun withdrawalHistory(): Result<List<WithdrawalRequest>>
    suspend fun accountStatus(): Result<AccountStatus>
    suspend fun accountInfo(): Result<AccountInfo>
    suspend fun getVerifications(): Result<List<Verification>>
    suspend fun createVerification(): Result<VerificationToken>
    suspend fun refreshVerificationToken(): Result<VerificationToken>
    suspend fun availableWithdrawal(): Result<AvailableWithdrawal>
    suspend fun profitLoss(): Result<ProfitLoss>
}