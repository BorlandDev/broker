package io.sevenb.terminal.domain.usecase.charts

import com.github.mikephil.charting.data.CandleEntry
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.trading.CandleChartParams
import io.sevenb.terminal.data.model.trading.TradeChartParams
import io.sevenb.terminal.data.model.trading.TradeChartRange
import io.sevenb.terminal.data.repository.trading.CandlesDataRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.math.BigDecimal

class CandlesData(
    private val candlesDataRepository: CandlesDataRepository
) {

    suspend fun requestCandleData(
        candleChartParams: CandleChartParams
    ): List<CandleEntry?> {
        return withContext(Dispatchers.IO) {
            setChartParams(candleChartParams)

            when (val candlesResult = candlesDataRepository.getCandle(candleChartParams)) {
                is Result.Success -> constructCandlesList(candlesResult.data.data)
                is Result.Error -> emptyList()
            }
        }
    }

    suspend fun requestCandleData(tradeChartParams: TradeChartParams): List<CandleEntry?> {
        return withContext(Dispatchers.IO) {
            val candleChartParams = constructCandleChartParams(tradeChartParams)
            setChartParams(candleChartParams)

            when (val candlesResult = candlesDataRepository.getCandle(candleChartParams)) {
                is Result.Success -> constructCandlesList(candlesResult.data.data)
                is Result.Error -> emptyList()
            }
        }
    }

    private fun constructCandleChartParams(tradeChartParams: TradeChartParams): CandleChartParams {
        val symbol = "${tradeChartParams.ticker}${AccountData.getBaseCurrencyTicker()}"
        return CandleChartParams(
            tradeChartParams.listPosition,
            tradeChartParams.tradeChartRange,
            symbol = symbol
        )
    }

    private fun setChartParams(candleChartParams: CandleChartParams) {
        val startTime: Long
        val endTime = getTime()
        val interval: String
        val limit: Int

        when (candleChartParams.tradeChartRange) {
            TradeChartRange.DAY -> {
                interval = MIN_5
                startTime = getStartTime(endTime, DAY)
                limit = 289
            }
            TradeChartRange.WEEK -> {
                interval = MIN_30
                startTime = getStartTime(endTime, WEEK)
                limit = 337
            }
            TradeChartRange.MONTH -> {
                interval = HOUR_2
                startTime = getStartTime(endTime, MONTH)
                limit = 361
            }
            TradeChartRange.THREE_MONTH -> {
                interval = HOUR_6
                startTime = getStartTime(endTime, THREE_MOTHS)
                limit = 361
            }
            TradeChartRange.YEAR -> {
                interval = DAY_1
                startTime = getStartTime(endTime, YEAR)
                limit = 366
            }
            TradeChartRange.ALL -> {
                interval = WEEK_1
                startTime = endTime - 7 * 24 * 60 * 60 * 1000 * 1000
                limit = 1000
            }
        }

        candleChartParams.interval = interval
        candleChartParams.startTime = "$startTime"
        candleChartParams.endTime = "$endTime"
        candleChartParams.limit = limit
    }

    private fun constructCandlesList(notConstructedList: List<List<Any?>?>?): List<CandleEntry?> {
        val candleEntries = mutableListOf<CandleEntry?>()
        if (!notConstructedList.isNullOrEmpty()) {
            notConstructedList.forEach { listOfAny ->
                if (!listOfAny.isNullOrEmpty()) {
                    val candleEntry = try {
                        val endTime = longFromStringDouble(listOfAny[6].toString())
                        val shadowH = floatFromStringDouble(listOfAny[2].toString())
                        val shadowL = floatFromStringDouble(listOfAny[3].toString())
                        val open = floatFromStringDouble(listOfAny[1].toString())
                        val close = floatFromStringDouble(listOfAny[4].toString())
                        CandleEntry(endTime.toFloat() / 5_000_000, shadowH, shadowL, open, close)
                    } catch (e: IndexOutOfBoundsException) {
                        null
                    } catch (e: ClassCastException) {
                        null
                    } catch (e: NullPointerException) {
                        null
                    }
                    candleEntries.add(candleEntry)
                }
            }
        }

        return candleEntries.toList()
    }

    private fun floatFromStringDouble(stringValue: String): Float {
        return try {
            stringValue.toFloat()
        } catch (e: ClassCastException) {
            0.0F
        }
    }

    private fun longFromStringDouble(stringValue: String): Long {
        return try {
            val bigDecimal = BigDecimal(stringValue)
            val bigInteger = bigDecimal.toBigInteger()
            bigInteger.toLong()
        } catch (e: ClassCastException) {
            0
        }
    }

    private fun getTime() = System.currentTimeMillis()

    private fun getStartTime(currentTime: Long, daysCount: Long): Long {
        return currentTime - daysCount * 1_000 * 60 * 60 * 24
    }

    companion object {
        private const val MIN_5 = "5m"
        private const val MIN_30 = "30m"
        private const val HOUR_2 = "2h"
        private const val HOUR_6 = "6h"
        private const val DAY_1 = "1d"
        private const val WEEK_1 = "1w"

        private const val DAY = 1L
        private const val WEEK = 7L
        private const val MONTH = 30L
        private const val THREE_MOTHS = 90L
        private const val YEAR = 365L
    }
}