package io.sevenb.terminal.domain.usecase.auth

class ValidateEmailPassword{

    fun validateEmail(
        email: String
    ): Boolean {
        val regex = EMAIL_REGEX.toRegex()
        return regex.matches(email)
    }

    fun validatePassword(
        password: String
    ): Boolean {
        val regex = PASSWORD_REGEX.toRegex()
        return regex.matches(password)
    }

    companion object {
        private const val EMAIL_REGEX = "^([a-zA-Z0-9_\\.-\\.+]+)@([a-z0-9-_\\.-\\.+]+)\\.([a-z\\.]{2,6})\$"
        private const val PASSWORD_REGEX = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9!@#\$%&*^ˆ()+=?_{|}~\\/-]{8,64}$"
    }
}
