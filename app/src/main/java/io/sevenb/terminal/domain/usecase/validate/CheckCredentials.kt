package io.sevenb.terminal.domain.usecase.validate

import io.sevenb.terminal.data.model.enum_model.ErrorMessageType
import io.sevenb.terminal.domain.usecase.auth.ValidateEmailPassword
import javax.inject.Inject

class CheckCredentials @Inject constructor(
    private val validateEmailPassword: ValidateEmailPassword
) {

    fun checkCredentials(email: String, password: String): ErrorMessageType {
        if (email.isEmpty() && password.isEmpty()) {
            return ErrorMessageType.EMPTY_FIELDS
        } else if (email.isEmpty()) {
            return ErrorMessageType.EMPTY_EMAIL
        } else if (password.isEmpty()) {
            return ErrorMessageType.EMPTY_PASSWORD
        }

        if (!validateEmailPassword.validateEmail(email)) {
            return ErrorMessageType.EMAIL_INCORRECT
        }

        if (!validateEmailPassword.validatePassword(password)) {
            return ErrorMessageType.PASSWORD_INCORRECT
        }

        return ErrorMessageType.CHECK_PASSED
    }

    fun checkCredentials(email: String): ErrorMessageType {
        if (email.isEmpty()) {
            return ErrorMessageType.EMPTY_EMAIL
        }

        if (!validateEmailPassword.validateEmail(email)) {
            return ErrorMessageType.EMAIL_INCORRECT
        }
        return ErrorMessageType.CHECK_PASSED
    }

    fun checkVerificationCode(code: String, errorMessageType: ErrorMessageType): ErrorMessageType {
        if (code.isEmpty()) {
            return errorMessageType
        }
        return ErrorMessageType.CHECK_PASSED
    }
}