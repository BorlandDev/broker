package io.sevenb.terminal.domain.usecase.auth

import io.sevenb.terminal.data.model.auth.SignInRequest
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.repository.auth.UserAuthRepository
import io.sevenb.terminal.utils.Logger
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import java.io.IOException

class SignInUser(
    private val userAuthRepository: UserAuthRepository,
    private val processTokens: ProcessTokens,
) {

    suspend fun execute(
        email: String,
        password: String
    ): Result<Boolean> {
        return withContext(IO) {
            val signInRequest = SignInRequest(email, password)

            when (val signInResult = userAuthRepository.signInUser(signInRequest)) {
                is Result.Success -> {

                    Logger.sendLog("SignInUser", "execute", "signInResult = true")

                    //TODO REFACTOR ADD BASE RESPONSE WITH ERROR HANDLING AND EXTRAS
                    if (signInResult.data.error?.isNotEmpty() == true && signInResult.data.extra?.accessToken?.isNotEmpty() == true) {
                        return@withContext Result.Error(
                            IOException(signInResult.data.error),
                            signInResult.data.extra!!.accessToken
                        )
                    }

                    if (signInResult.data.userSettings?.tfa == true) {
                        return@withContext Result.Success(true)
                    } else {
                        when (val processResult = processTokens.processTokens(
                            email,
                            password,
                            signInResult.data.accessToken,
                            signInResult.data.refreshToken
                        )) {
                            is Result.Success -> {
                                return@withContext Result.Success(false)
                            }
                            is Result.Error -> {
                                return@withContext Result.Error(processResult.exception)
                            }
                        }
                    }
                }
                is Result.Error -> {
                    val exception = IOException(signInResult.message)
                    Logger.sendLog("SignInUser", "execute", exception)
                    return@withContext Result.Error(exception)
                }
            }
        }
    }

}
