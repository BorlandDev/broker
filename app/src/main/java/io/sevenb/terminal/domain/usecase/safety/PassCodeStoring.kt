package io.sevenb.terminal.domain.usecase.safety

import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.repository.auth.PreferenceRepository

class PassCodeStoring(
    private val preferenceRepository: PreferenceRepository
) {

    suspend fun storePassCode(passCode: String): Result<Unit> {
        return preferenceRepository.storePassCode(passCode)
    }

    suspend fun restorePassCode() = preferenceRepository.restorePassCode()
}