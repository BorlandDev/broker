package io.sevenb.terminal.domain.usecase.withdrawal

import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.withdrawal.WithdrawalHistoryItem
import io.sevenb.terminal.data.model.withdrawal.WithdrawalRequest
import io.sevenb.terminal.domain.repository.AccountApi
import io.sevenb.terminal.domain.usecase.auth.ParseErrorMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.IOException

class RequestWithdrawal(
    private val accountApi: AccountApi,
    private val parseErrorMessage: ParseErrorMessage
) {

    suspend fun execute(withdrawalRequest: WithdrawalRequest) : Result<WithdrawalHistoryItem> {
        return withContext(Dispatchers.IO) {
            when(val requestWithdrawalResult = accountApi.requestWithdrawal(withdrawalRequest)) {
                is Result.Success -> Result.Success(requestWithdrawalResult.data)
                is Result.Error -> {
                    val errorMessage = parseErrorMessage.execute(requestWithdrawalResult.exception.message?:"")
                    if (errorMessage != null) {
                        Result.Error(IOException("Can't create withdrawal request"), errorMessage.message)
                    } else {
                        Result.Error(IOException("Can't create withdrawal request"), "Withdrawal error: code 0")
                    }
                }
            }
        }
    }

}
