package io.sevenb.terminal.domain.repository

import io.sevenb.terminal.data.model.auth.ResponseBoolean
import io.sevenb.terminal.data.model.broker_api.TokensResponse
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.tfa.TfaResponse
import io.sevenb.terminal.data.model.tfa.TfaVerification

interface TfaApi {
    suspend fun twoFaIsEnabled(): Result<ResponseBoolean>
    suspend fun toggleTfa(): Result<TfaResponse>
    suspend fun verify(tfaVerification: TfaVerification): Result<TokensResponse>
}