package io.sevenb.terminal.domain.usecase.auth

import io.sevenb.terminal.data.model.auth.PasswordCheckResponse
import java.util.regex.Pattern

class CreatingPasswordCheck {

    fun checkPassword(password: String) =
        password.let {
            PasswordCheckResponse(
                checkLength(it),
                stringContainsNumber(it),
                stringContainsLowercase(it),
                stringContainsUppercase(it)
            )
        }

    private fun checkLength(s: String) = s.length >= 8

    private fun stringContainsNumber(s: String) = Pattern.compile("[0-9]").matcher(s).find()

    private fun stringContainsLowercase(s: String) =
        Pattern.compile("(?=.*[a-z])").matcher(s).find()

    private fun stringContainsUppercase(s: String) =
        Pattern.compile("(?=.*[A-Z])").matcher(s).find()
}