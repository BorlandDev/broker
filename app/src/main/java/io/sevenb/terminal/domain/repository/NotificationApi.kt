package io.sevenb.terminal.domain.repository

import io.sevenb.terminal.data.model.auth.ResponseBoolean
import io.sevenb.terminal.data.model.broker_api.CreatePriceAlert
import io.sevenb.terminal.data.model.broker_api.PriceAlert
import io.sevenb.terminal.data.model.broker_api.PriceAlertGet
import io.sevenb.terminal.data.model.broker_api.PriceAlertGetApi
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.enums.notifications.NotificationsType
import io.sevenb.terminal.data.model.messages.*
import io.sevenb.terminal.data.model.settings.SettingsPOJO

interface NotificationApi {
    suspend fun storeEnableNotifications(value: Boolean): Result<ResponseBoolean>
    suspend fun restoreEnableNotifications(): Result<ResponseBoolean>
    suspend fun storeEnabledNotifications(vararg notificationType: NotificationsType): Result<ResponseBoolean>
    suspend fun restoreEnabledNotifications(): Result<List<NotificationsType>>

    suspend fun testTextTypes(): Result<TextTypesPOJO>
//    suspend fun testTemplate(): Result<TemplatePOJO>
//    suspend fun getUserSettings(): Result<UserSettingsPOJO>
//    suspend fun <T : Parcelable> getUserSettings(): Result<BaseResponse<T>>
    suspend fun  getUserSettings(): Result<SettingsPOJO>
    suspend fun  deletePriceAlert(id: String): Result<PriceAlert>
    suspend fun  createPriceAlert(bodyAlert: PriceAlert): Result<CreatePriceAlert>
    suspend fun  getPriceAlert(): Result<PriceAlertGetApi>
    suspend fun put(request: List<NotificationsSettingsRequest>): Result<ResponseBoolean>

    suspend fun sendMessage(sendMessageRequest: SendMessageRequest): Result<FirebaseMessageResponse>
}