package io.sevenb.terminal.domain.repository

import androidx.lifecycle.LiveData
import io.sevenb.terminal.data.datasource.database.model.CurrencyEntity
import io.sevenb.terminal.data.model.core.Result
import kotlinx.coroutines.flow.Flow

interface DbApi {
    fun getAll(): LiveData<List<CurrencyEntity>>
    fun getAllFlow(): Flow<List<CurrencyEntity>>
    fun getAllResult(): List<CurrencyEntity>
    fun getCurrency(ticker: String): Flow<CurrencyEntity>
    suspend fun deleteAll()
    suspend fun insertAll(vararg currencies: CurrencyEntity)
    suspend fun delete(currency: CurrencyEntity)
    suspend fun update(currency: CurrencyEntity)
}