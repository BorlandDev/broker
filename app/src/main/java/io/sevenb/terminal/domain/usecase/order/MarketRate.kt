package io.sevenb.terminal.domain.usecase.order

import io.sevenb.terminal.data.model.broker_api.rates.MarketRate
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.domain.repository.MarketApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MarketRate(
    private val marketApi: MarketApi
) {

    suspend fun execute(from: String, to: String): Result<MarketRate> {
        return withContext(Dispatchers.IO) {
            return@withContext marketApi.marketRate(from, to)
        }
    }
}
