package io.sevenb.terminal.domain.usecase.charts

import com.github.mikephil.charting.data.CandleEntry
import com.github.mikephil.charting.data.Entry
import io.sevenb.terminal.data.datasource.database.model.CurrencyEntity
import io.sevenb.terminal.data.model.broker_api.markets.AccountBalance
import io.sevenb.terminal.data.model.broker_api.markets.Currency
import io.sevenb.terminal.data.model.broker_api.rates.MarketRate
import io.sevenb.terminal.data.model.broker_api.rates.MarketRateParams
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.enums.TradingWalletItemType
import io.sevenb.terminal.data.model.portfolio.LocalAccountData
import io.sevenb.terminal.data.model.trading.TradeChartRange
import io.sevenb.terminal.data.model.withdrawal.WithdrawItem
import io.sevenb.terminal.data.repository.auth.PreferenceRepository
import io.sevenb.terminal.data.repository.database.DbRepository
import io.sevenb.terminal.domain.repository.AccountApi
import io.sevenb.terminal.domain.repository.MarketApi
import io.sevenb.terminal.domain.repository.RatesApi
import io.sevenb.terminal.domain.repository.RawDataRepository
import io.sevenb.terminal.domain.usecase.auth.ParseErrorMessage
import io.sevenb.terminal.domain.usecase.sort.SortCurrenciesHandler
import io.sevenb.terminal.ui.screens.order.OrderViewModel
import io.sevenb.terminal.ui.screens.portfolio.AssetItem
import io.sevenb.terminal.ui.screens.portfolio.PortfolioHistoryItem
import io.sevenb.terminal.ui.screens.settings.SettingsFragment
import io.sevenb.terminal.ui.screens.settings.SettingsFragment.Companion.hasUpdatedCheck
import io.sevenb.terminal.ui.screens.trading.TradingListAdapter.Companion.formatCryptoAmount
import io.sevenb.terminal.ui.screens.trading.TradingWalletItem
import io.sevenb.terminal.utils.Logger
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.channels.*
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.consumeAsFlow
import timber.log.Timber
import java.io.IOException

//@ExperimentalCoroutinesApi
//@FlowPreview
class AccountData(
    val accountApi: AccountApi,
    private val ratesApi: RatesApi,
    private val marketApi: MarketApi,
    private val rawDataRepository: RawDataRepository,
    private val parseErrorMessage: ParseErrorMessage,
    private val preferenceRepository: PreferenceRepository,
    private val sortCurrenciesHandler: SortCurrenciesHandler,
    val dbRepository: DbRepository
) {

    var totalBalanceChannel = BroadcastChannel<LocalAccountData>(Channel.CONFLATED)
    var totalBalanceFlow = totalBalanceChannel.asFlow()

    var balancesListChannel = BroadcastChannel<List<AssetItem>>(Channel.CONFLATED)
    var balancesListFlow = balancesListChannel.asFlow()

    var dailyWithdrawalLimitChannel = BroadcastChannel<String>(Channel.CONFLATED)
    var dailyWithdrawalLimitFlow = dailyWithdrawalLimitChannel.asFlow()

    var accountInfoChannel = BroadcastChannel<LocalAccountData>(Channel.CONFLATED)
    var accountInfoFlow = accountInfoChannel.asFlow()

    private var currenciesListChannel =
        BroadcastChannel<MutableList<TradingWalletItem>>(Channel.CONFLATED)
    var currenciesListFlow = currenciesListChannel.asFlow()

    var historyListChannel = BroadcastChannel<MutableList<PortfolioHistoryItem>>(Channel.CONFLATED)
    var historyListFlow = historyListChannel.asFlow()

    private var updateAssetsChannel = BroadcastChannel<Long>(Channel.CONFLATED)
    var updateAssetsFlow = updateAssetsChannel.asFlow()

    var tradingItems: MutableList<TradingWalletItem> = mutableListOf()
    private var lastTradeListUpdate = 0L

    var updateChannel = BroadcastChannel<List<AssetItem>>(Channel.CONFLATED)
    var updateFlow = updateChannel.asFlow()

    private val currencyMap = mutableMapOf<String, Pair<Int, String>>()//ticker, (position, ticker)

    var sortedCurrencies: List<Currency> = mutableListOf()
//    var emptyBalancesAfterLogOut = false

    lateinit var tradingTimerJob: Job
    lateinit var currenciesListFlowJob: Job
    lateinit var balancesListFlowJob: Job
    lateinit var portfolioTotalBalanceFlowJob: Job
    lateinit var tradingTotalBalanceFlowJob: Job
    lateinit var historyFlowJob: Job
    lateinit var updateAssetsFlowJob: Job

    private val updateTimeChannel = ConflatedBroadcastChannel<Pair<String, Long>>()
    private val updateRangeChannel = ConflatedBroadcastChannel<Pair<String, TradeChartRange>>()
    private val updateExpandedStateChannel = Channel<Pair<String, Boolean>>(1)

    init {
//        emptyBalancesAfterLogOut = false

        GlobalScope.launch {
            updateTimeChannel.consumeEach {
                updateChartTimeByTicker(it.first, it.second)
            }
        }

        GlobalScope.launch {
            updateRangeChannel.consumeEach {
                updateExpandSelectedRangeByTicker(it.first, it.second)
            }
        }

        GlobalScope.launch {
            updateExpandedStateChannel.consumeAsFlow().collect {
                updateExpandedStateByTicker(it.first, it.second)
            }
        }

        updateAssetList(System.currentTimeMillis() - 5000)
    }

    private fun updateExpandedStateByTicker(ticker: String, stateType: Boolean) {
        val position = currencyMap[ticker]?.first
        position?.let {
            if (!containsInList(it)) return@let
            val item = tradingItems[it]
            item.isExpanded = stateType
            tradingItems[it] = item
        }
    }

    private suspend fun networkAccountBalances(): Result<List<AccountBalance>> {
        return withContext(IO) {
            when (val balancesResult = accountApi.accountBalances()) {
                is Result.Success -> {
                    Timber.d("SPEEDTEST networkAccountBalances: " + System.currentTimeMillis())

                    val balances = balancesResult.data
                        .filter { it.available.toFloatOrNull() ?: 0.0f > 0.0f }
                    accountApi.listOfBalances = balances

                    Logger.sendLog(
                        "AccountData",
                        "networkAccountBalances",
                        "balancesResult.data is list empty ${balances.isEmpty()}"
                    )

                    Result.Success(balances)
                }
                is Result.Error -> {
                    val errorMessage = parseErrorMessage.execute(balancesResult.message)
                    val exception = IOException("Can't get account data")
                    Logger.sendLog(
                        "AccountData",
                        "networkAccountBalances",
                        exception
                    )
                    if (errorMessage?.error == "access_denied") {
                        Result.Error(
                            exception,
                            "Please confirm your phone number"
                        )
                    } else {
                        Result.Error(exception)
                    }
                }
            }
        }
    }

    private suspend fun networkCurrencies(): Result<List<Currency>> {
        return withContext(IO) {
            when (val currenciesResult = marketApi.currencies()) {
                is Result.Success -> {
                    Timber.d("SPEEDTEST networkCurrencies: " + System.currentTimeMillis())

                    val list = currenciesResult.data.toMutableList()

                    val filteredBnbBsc = sortCurrenciesHandler.filterBnbBscNetwork(list)
                    val sortedList = sortCurrenciesHandler.sortByTicker(filteredBnbBsc)

                    val listOfCurrencyEntity = sortedList.map {
                        CurrencyEntity(it.ticker, it.name)
                    }

                    dbRepository.insertAll(*listOfCurrencyEntity.toTypedArray())
                    marketApi.listOfCurrencies = list

                    Logger.sendLog(
                        "AccountData",
                        "networkCurrencies",
                        "currenciesResult is list empty = ${list.isEmpty()}"
                    )
                    Result.Success(list)
                }
                is Result.Error -> {
                    val errorMessage = parseErrorMessage.execute(currenciesResult.message)
                    val exception = IOException("Can't get account data")
                    Logger.sendLog(
                        "AccountData",
                        "networkCurrencies",
                        exception
                    )
                    if (errorMessage?.error == "access_denied") {
                        Result.Error(
                            exception,
                            "Please confirm your phone number"
                        )
                    } else {
                        Result.Error(exception)
                    }
                }
            }
        }
    }

    private suspend fun cacheFirstCurrencies(loadFromDB: Boolean? = null): Result<List<Currency>> {
        return withContext(IO) {
            val list = marketApi.listOfCurrencies
            if (loadFromDB == true) {
                val allCurrencies = dbRepository.getAllResult()
                if (allCurrencies.isNotEmpty()) {
                    Result.Success(allCurrencies.map {
                        Currency(it.ticker, it.currencyName)
                    })
                } else networkCurrencies()
            } else {
                if (list.isNotEmpty() && !hasUpdatedCheck) {
                    Result.Success(list)
                } else {
                    hasUpdatedCheck = true
                    networkCurrencies()
                }
            }
        }
    }

    /**
     * ORDER VM + TRADING VM
     */
    suspend fun cacheFirstBalances(): Result<List<AccountBalance>> {
        return withContext(IO) {
            return@withContext networkAccountBalances()
        }
    }

    /**
     * SPLASH PRELOAD + PORTFOLIO + AUTH (PASS RESTORING/LOGIN/REG)
     */
    suspend fun mapAccountAssets(loadFromDB: Boolean? = null): Result<List<AssetItem>> {
        return withContext(IO) {
            val balancesResultAsync = async { networkAccountBalances() }
            val mapOfCurrenciesAsync = async { getMapOfCurrencies(loadFromDB) }
            when (val awaited =
                balancesResultAsync.await()) {//val balancesResult = networkAccountBalances()) {
                is Result.Success -> {
                    mapOfCurrenciesAsync.await()
//                    getMapOfCurrencies(loadFromDB)
                    val totalHoldingAsync = async { totalHolding() }

                    val balancesList = awaited.data
                        .map {

                            val position = currencyMap[it.ticker]?.first
                            val charts = position?.let {
                                if (!containsInList(position)) return@let null
                                else tradingItems[position].chartEntries
                            }

                            AssetItem(
                                currencyMap[it.ticker]?.second ?: "",
                                it.ticker,
                                rawDataRepository.mapTickerToCmcIconId?.get(it.ticker) ?: 0,
                                balance = formatCryptoAmount(it.available.toFloatOrNull()),
                                estimated = formatCryptoAmount(it.availableEstimated.toFloatOrNull()),
                                currentRangeAsset = TradeChartRange.DAY,
                                updateChartRangeChannel = updateRangeChannel,
                                updateTimeChannel = updateTimeChannel,
                                chartEntries = charts,
                                expandedChartEntries = charts,
                                updateExpandedStateChannel = updateExpandedStateChannel
                            )
                        }

                    val baseCurrenciesRates = getBaseCurrenciesRates(balancesList)

                    baseCurrenciesRates.forEachIndexed { index, marketRate ->
                        marketRate?.let {
                            balancesList[index].apply {
                                balanceInDefaultCurrency = marketRate.rate
                                currentRange = TradeChartRange.DAY
                                tradingWalletItemType = TradingWalletItemType.CHART
                            }
                        }
                    }

                    updateAssetCharts(balancesList)
                    balancesListChannel.offer(balancesList)
                    totalHoldingAsync.await()

                    Result.Success(balancesList)
                }
                is Result.Error -> awaited
            }
        }
    }

    /**
     * ORDER + WITHDRAWAL
     */
    suspend fun mapWithdrawalList(withdraw: Boolean = false): Result<List<WithdrawItem>> {
        return withContext(IO) {
            val listOfCurrenciesResult = cacheFirstCurrencies()
            var listOfCurrencies: List<Currency>? = null
            if (listOfCurrenciesResult is Result.Success) {
                listOfCurrencies = listOfCurrenciesResult.data
            }

            when (val balances = networkAccountBalances()) {
                is Result.Success -> {
                    val withdrawalList = mutableListOf<WithdrawItem>()
                    balances.data
                        .forEach {
                            val currency =
                                listOfCurrencies?.firstOrNull { currency -> it.ticker == currency.ticker }

                            currency?.networks?.forEach network@{ currencyNetwork ->
                                if (withdraw)
                                    if (!currencyNetwork.withdraw) return@network

                                withdrawalList.add(
                                    WithdrawItem(
                                        it.ticker,
                                        currency.name,
                                        it.available,
                                        it.availableEstimated,
                                        currencyNetwork.network,
                                        currencyNetwork.withdrawFee,
                                        currencyNetwork.withdrawMin,
                                        currencyNetwork.hasMemo,
                                        rawDataRepository.mapTickerToCmcIconId?.get(it.ticker) ?: 0,
                                        updateExpandedStateChannel = updateExpandedStateChannel,
                                        isExpanded = false,
                                        currentRange = TradeChartRange.DAY
                                    )
                                )
                            }
                        }
                    Result.Success(withdrawalList)
                }
                is Result.Error -> Result.Error(IOException("Can't get withdrawal list"))
            }
        }
    }

    /**
     * ORDER
     */
    suspend fun balanceByTicker(ticker: String): Result<String> {
        return withContext(IO) {
            when (val balances = networkAccountBalances()) {
                is Result.Success -> {
                    val tickerAsset = balances.data.firstOrNull { it.ticker == ticker }
                    val formattedBalance =
                        formatCryptoAmount(tickerAsset?.available?.toFloatOrNull())
                    Result.Success(formattedBalance)
                }
                is Result.Error -> Result.Error(IOException("Can't get balance by ticker"))
            }
        }
    }

    fun updateCandleChartData(position: Int, candleEntries: List<CandleEntry?>) {
        if (position < tradingItems.size)
            if (!candleEntries.isNullOrEmpty()) {
                tradingItems[position].candleChartData = candleEntries
            }
    }

    fun updateCandleChartDataByTicker(ticker: String, candleEntries: List<CandleEntry?>) {
        val currency = tradingItems.firstOrNull { it.ticker == ticker }
        currency?.let {
            if (!candleEntries.isNullOrEmpty())
                it.candleChartData = candleEntries
        }
    }

    suspend fun mapTradingList(test: Boolean = false): Result<List<TradingWalletItem>> {
        return withContext(IO) {
            lastTradeListUpdate = if (
                System.currentTimeMillis() - lastTradeListUpdate < TRADE_LIST_CACHE_TIME
                || !SettingsFragment.hasUpdated || test
            ) {
                if (sortedCurrencies.isEmpty() || SettingsFragment.hasUpdated || test || tradingItems.isEmpty())
                    System.currentTimeMillis()
                else
                    return@withContext Result.Success(tradingItems)
            } else {
                System.currentTimeMillis()
            }

            val asyncBalances = async { cacheFirstBalances() }
            val asyncFirstCurrencies = async { cacheFirstCurrencies() }

            return@withContext tradingListResult(
                asyncBalances.await(),
                asyncFirstCurrencies.await()
            )
        }
    }

    private suspend fun tradingListResult(
        accountBalances: Result<List<AccountBalance>>,
        currencies: Result<List<Currency>>
    ): Result<List<TradingWalletItem>> {
        var balancesTickers = listOf<String>()
        if (accountBalances is Result.Success) {
            balancesTickers = accountBalances.data.map { it.ticker }
        }
        return withContext(IO) {
            when (currencies) {
                is Result.Success -> {
                    SettingsFragment.hasUpdated = false
                    val list = currencies.data
                    val favoriteCurrenciesAsync =
                        async { preferenceRepository.restoreFavoriteTickers() }
                    val listPricesAsync = async { getPricesMap(list) }

                    return@withContext getSortedList(
                        favoriteCurrenciesAsync.await(),
                        listPricesAsync.await(),
                        list,
                        balancesTickers
                    )
                }
                is Result.Error -> currencies
            }
        }
    }

    private fun getSortedList(
        favorites: Result<String>,
        prices: Map<String, Float?>,
        list: List<Currency>,
        balancesTickers: List<String>
    ): Result<List<TradingWalletItem>> {

        val lss = mutableListOf<TradingWalletItem>()
        val asdasd = mutableListOf<Currency>()

        list.forEachIndexed { index, currency ->
            val isSellAvailable = balancesTickers.contains(currency.ticker)
            val price: Float? = if (prices.size > index) prices[currency.ticker]
            else null

            val favorite = favorites is Result.Success && favorites.data.contains(currency.ticker)

            if (price != null && price != 0.0f) {
                val item = TradingWalletItem(
                    currency.name,
                    currency.ticker,
                    rawDataRepository.mapTickerToCmcIconId?.get(currency.ticker) ?: 0,
                    isSellAvailable = isSellAvailable,
                    price = price,
                    isFavorite = favorite,
                    currentRange = TradeChartRange.DAY,
                    tradingWalletItemType = TradingWalletItemType.CHART,
                    updateChartRangeChannel = updateRangeChannel,
                    updateTimeChannel = updateTimeChannel,
                    updateExpandedStateChannel = updateExpandedStateChannel
                )
                lss.add(item)
                currency.apply {
                    asdasd.add(
                        Currency(
                            ticker,
                            name,
                            networks,
                            network ?: "",
                            isExpanded,
                            currentRange,
                            updateExpandedStateChannel,
                            estimated ?: "",
                            amount ?: ""
                        )
                    )
                }
            }
        }

//        val mappedList = list.mapIndexed { index, currency ->
//            val isSellAvailable = balancesTickers.contains(currency.ticker)
//            val price: Float? = if (prices.size > index) prices[currency.ticker]
//            else null
//
//            val favorite = favorites is Result.Success && favorites.data.contains(currency.ticker)
//
//            TradingWalletItem(
//                currency.name,
//                currency.ticker,
//                rawDataRepository.mapTickerToCmcIconId?.get(currency.ticker) ?: 0,
//                isSellAvailable = isSellAvailable,
//                price = price,
//                isFavorite = favorite,
//                currentRange = TradeChartRange.DAY,
//                tradingWalletItemType = TradingWalletItemType.CHART,
//                updateChartRangeChannel = updateRangeChannel,
//                updateTimeChannel = updateTimeChannel,
//                updateExpandedStateChannel = updateExpandedStateChannel
//            )
//        }
        val sortedList = sortCurrenciesHandler.sortByTicker(lss)

        sortedCurrencies = sortCurrenciesHandler.sortByTicker(asdasd)

        tradingItems = sortedList.toMutableList()
        return Result.Success(sortedList)
    }

    /**
     * ACCOUNT DATA / TRADING
     */
    suspend fun totalHolding() {
        Timber.d("SPEEDTEST totalHolding: " + System.currentTimeMillis())

        withContext(IO) {
            when {
                accountApi.listOfBalances.isEmpty() -> {
                    if (networkAccountBalances() is Result.Success) {
                        updateTotalHolding()
                    }
                }
                accountApi.listOfBalances.isNotEmpty() -> updateTotalHolding()
                else -> computeTotalHolding()
            }
        }
    }

    private suspend fun computeTotalHolding() {
        Timber.d("SPEEDTEST computeTotalHolding1: " + System.currentTimeMillis())

        val totalEstimated = accountApi.listOfBalances
            .map { it.availableEstimated.toFloatOrNull() ?: 0.0f }
            .sum()
        val lockedEstimated = accountApi.listOfBalances
            .map { it.lockedEstimated.toFloatOrNull() ?: 0.0f }
            .sum()
        val availableEstimated = totalEstimated - lockedEstimated

        val totalHolding =
            if (totalEstimated == 0.0f) {
                if (accountApi.listOfBalances.isNotEmpty())
                    accountApi.localAccountData.totalHoldingBalance
                else "0.0"
            } else formatCryptoAmount(totalEstimated)

        accountApi.localAccountData.totalHoldingBalance = totalHolding
        accountApi.localAccountData.availableBalance = formatCryptoAmount(availableEstimated)
        accountApi.localAccountData.onOrdersBalance = formatCryptoAmount(lockedEstimated)

        val rateResult = marketApi.marketRate(BTC, getBaseCurrencyTicker())
        if (rateResult is Result.Success) {
            val rate = rateResult.data?.rate ?: 0.0f
            val convertedValue = (totalEstimated * rate)
            accountApi.localAccountData.totalInBaseCurrency = OrderViewModel.roundDouble(
                convertedValue.toDouble(), 2, true
            ).replace(",", ".")
        }

        Timber.d("SPEEDTEST computeTotalHolding2: " + System.currentTimeMillis())

        totalBalanceChannel.offer(accountApi.localAccountData)
    }

    /**
     * 1. compute estimates from local rate repository
     * 2. collect ticker with no local rate
     * 3. call estimated for tickers from p. 2
     * 4. compute estimated amounts for remaining balances
     */
    private suspend fun updateTotalHolding() {
        Timber.d("SPEEDTEST updateTotalHolding: " + System.currentTimeMillis())

        withContext(IO) {
            val currencyMapAsync = async { getMapOfCurrencies() }
            val tickersForRatesRequest = mutableListOf<String>()
            accountApi.listOfBalances.forEach {
                if (it.available.toFloatOrNull() == 0.0f) return@forEach

                val ticker = it.ticker
                val rate = ratesApi.mapOfRates[ticker]
                rate ?: tickersForRatesRequest.add(ticker)

                it.updateEstimated(rate)
            }

            val listOfMarketRatesParams =
                tickersForRatesRequest.map {
                    if (it == USDT) MarketRateParams(BTC, it)
                    else MarketRateParams(it, BTC)
                }

            //TODO UP HERE
            //                    emptyBalancesAfterLogOut = false

            if (listOfMarketRatesParams.isEmpty()) {
                computeTotalHolding()
                return@withContext
            }

            when (val rates = ratesApi.batchEstimates(listOfMarketRatesParams)) {
                is Result.Success -> {
                    val ratesList: MutableList<MarketRate> = mutableListOf()

                    for ((pos, rate) in rates.data.withIndex()) {
                        if (listOfMarketRatesParams[pos].from == BTC && listOfMarketRatesParams[pos].to == USDT) {
                            rate?.let {
                                if (rate.rate != null) {
                                    val newRate = 1 / rate.rate!!
                                    rate.rate = newRate
                                    ratesList.add(rate)
                                }
                            }
                        } else ratesList.add(rate)
                    }

                    ratesApi.addNewRates(ratesList)
                    accountApi.listOfBalances.forEach {
                        if (it.available.toFloatOrNull() == 0.0f) return@forEach
                        val symbol = if (it.ticker == USDT) "%s%s".format(
                            BTC,
                            it.ticker
                        ) else "%s%s".format(it.ticker, BTC)
                        val rate = ratesApi.mapOfRates[symbol]
                        if (rate != null) {
                            tickersForRatesRequest.add(symbol)
                            it.updateEstimated(rate)
                        }
                    }

                    currencyMapAsync.await()

                    val listAssets = accountApi.listOfBalances
                        .map {
                            val currencyName = currencyMap[it.ticker]?.second ?: ""

                            val position = getPositionByTicker(it.ticker, tradingItems)
                            val charts = position?.let {
                                if (!containsInList(position)) return@let null
                                else tradingItems[position].chartEntries
                            }

                            AssetItem(
                                currencyName,
                                it.ticker,
                                rawDataRepository.mapTickerToCmcIconId?.get(it.ticker) ?: 0,
                                balance = formatCryptoAmount(it.available.toFloatOrNull()),
                                estimated = formatCryptoAmount(it.availableEstimated.toFloatOrNull()),
                                updateChartRangeChannel = updateRangeChannel,
                                updateTimeChannel = updateTimeChannel,
                                chartEntries = charts,
                                expandedChartEntries = charts,
                                updateExpandedStateChannel = updateExpandedStateChannel
                            )
                        }

                    val baseCurrenciesRates = getBaseCurrenciesRates(listAssets)

                    baseCurrenciesRates.forEachIndexed { index, marketRate ->
                        marketRate?.let {
                            listAssets[index].balanceInDefaultCurrency = marketRate.rate
                        }
                    }

//                    emptyBalancesAfterLogOut = false
                    balancesListChannel.offer(listAssets)

                    computeTotalHolding()
                }
                is Result.Error -> Result.Error(IOException("Can't get batch estimates"))
            }
        }
    }

    private suspend fun getBaseCurrenciesRates(listAssets: List<AssetItem>): List<MarketRate?> {
        val params: MutableList<MarketRateParams> = mutableListOf()
        listAssets.forEach {
            params.add(MarketRateParams(it.ticker, getBaseCurrencyTicker()))
        }

        return when (val pricesResult = getPrices(params)) {
            is Result.Success -> pricesResult.data
            is Result.Error -> listOf()
        }
    }

    private suspend fun getMapOfCurrencies(loadFromDB: Boolean? = null) {
        Timber.d("SPEEDTEST getMapOfCurrencies: " + System.currentTimeMillis())

        if (currencyMap.isEmpty()) {
            val currencies = cacheFirstCurrencies(loadFromDB)

            if (currencies is Result.Success) {
                val sortedList = sortCurrenciesHandler.sortByTicker(currencies.data)
                sortedList.forEachIndexed { index, currency ->
                    currency?.let {
                        currencyMap[currency.ticker] = Pair(index, currency.name)
                    }
                }
            }
        }
    }

    fun updateChartParamsByTicker(ticker: String, entries: List<Entry>) {
        getPositionInSortedCurrencyList(ticker)?.let {
            if (!containsInList(it)) return@let
            if (!entries.isNullOrEmpty()) {
                val item = tradingItems[it]
                item.chartEntries = entries
                tradingItems[it] = item
            }
        }
    }

    private fun getPositionInSortedCurrencyList(ticker: String) = currencyMap[ticker]?.first

    fun updateExpandedChartParamsByTicker(ticker: String, entries: List<Entry>) {
        val position = currencyMap[ticker]?.first
        position?.let { pos ->
            if (!containsInList(pos)) return@let
            if (entries.isNotEmpty()) {
                val item = tradingItems[pos]
                item.expandedChartEntries = entries
                tradingItems[pos] = item
            }
        }
    }

    private fun updateExpandSelectedRangeByTicker(ticker: String, selectedRange: TradeChartRange) {
        val position = currencyMap[ticker]?.first
        position?.let {
            if (!containsInList(it)) return@let
            val item = tradingItems[it]
            item.currentRange = selectedRange
            tradingItems[it] = item
        }
    }

    private suspend fun getPricesMap(list: List<Currency>): Map<String, Float?> {
        val listOfParams = constructRateParams(list)
        var mapPrices = mutableMapOf<String, Float?>()

        when (val data = getPrices(listOfParams.toList())) {
            is Result.Success -> {
                data.data.forEachIndexed { index, item ->
                    mapPrices[list[index].ticker] = item?.rate
                }
            }
            is Result.Error -> mapPrices = mutableMapOf()
        }

        return mapPrices
    }

    fun constructRateParams(list: List<Currency>): List<MarketRateParams> {
        val listOfParams: MutableList<MarketRateParams> = mutableListOf()
        list.forEach {
            listOfParams.add(MarketRateParams(it.ticker, getBaseCurrencyTicker()))
        }

        return listOfParams.toList()
    }

    suspend fun getPrices(listParams: List<MarketRateParams>): Result<List<MarketRate?>> {
        return ratesApi.batchEstimates(listParams)
    }

    fun updateCurrencyList(list: List<MarketRate?>) {
        if (!tradingItems.isNullOrEmpty()) {
            list.forEachIndexed { index, item ->
                item?.let {
                    if (tradingItems.size > index) {
                        tradingItems[index].price = it.rate
                    }
                }
            }

            currenciesListChannel.offer(tradingItems)
        }
    }

    suspend fun primaryLoadFromDB() {
        val localCurrencyList = dbRepository.getAllFlow()
        localCurrencyList.collect {
            if (tradingItems.isNullOrEmpty()) {
                it.forEach { currency ->
                    tradingItems.add(
                        TradingWalletItem(
                            currency.currencyName,
                            currency.ticker,
                            rawDataRepository.mapTickerToCmcIconId?.get(currency.ticker) ?: 0,
                            updateChartRangeChannel = updateRangeChannel,
                            updateTimeChannel = updateTimeChannel,
                            updateExpandedStateChannel = updateExpandedStateChannel
                        )
                    )
                }
            }
        }
    }

    fun updateHistoryList(list: MutableList<PortfolioHistoryItem>) {
        historyListChannel.offer(list)
    }

    fun updateAssetList(long: Long) {
        updateAssetsChannel.offer(long)
    }

    private fun updateChartTimeByTicker(ticker: String, time: Long) {
        val position = currencyMap[ticker]?.first
        position?.let {
            if (!containsInList(it)) return
            val item = tradingItems[it]
            item.lastChartUpdate = time
            tradingItems[it] = item
        }
    }

    private fun containsInList(position: Int) =
        tradingItems.isNotEmpty() && tradingItems.size > position

    fun cancelTimerJob() {
        if (::tradingTimerJob.isInitialized && tradingTimerJob.isActive)
            tradingTimerJob.cancel()
    }

    fun cancelCurrenciesListJob() {
        if (::currenciesListFlowJob.isInitialized && currenciesListFlowJob.isActive)
            currenciesListFlowJob.cancel()
    }

    fun cancelTradingTotalBalanceJob() {
        if (::tradingTotalBalanceFlowJob.isInitialized && tradingTotalBalanceFlowJob.isActive)
            tradingTotalBalanceFlowJob.cancel()
    }

    fun cancelPortfolioTotalBalanceJob() {
        if (::portfolioTotalBalanceFlowJob.isInitialized && portfolioTotalBalanceFlowJob.isActive)
            portfolioTotalBalanceFlowJob.cancel()
    }

    fun cancelBalancesListFlowJobJob() {
        if (::balancesListFlowJob.isInitialized && balancesListFlowJob.isActive)
            balancesListFlowJob.cancel()
    }

    fun cancelHistoryFlowJobJob() {
        if (::historyFlowJob.isInitialized && historyFlowJob.isActive)
            historyFlowJob.cancel()
    }

    fun cancelUpdateAssetsFlowJobJob() {
        if (::updateAssetsFlowJob.isInitialized && updateAssetsFlowJob.isActive)
            updateAssetsFlowJob.cancel()
    }

    fun getPositionByTicker(ticker: String, list: List<TradingWalletItem>): Int? {
        val item = list.firstOrNull { it.ticker == ticker }
        return item?.let { list.indexOf(it) }
    }

    fun updateAssetCharts(assetList: List<AssetItem>) {
        assetList.forEach {
            val position = currencyMap[it.ticker]?.first
            position?.let { pos ->
                if (!containsInList(pos)) return@let
                val item = tradingItems[pos]
                it.chartEntries = item.chartEntries
                it.expandedChartEntries = item.expandedChartEntries
            }
        }
    }

    suspend fun clearDb() {
        dbRepository.deleteAll()
    }

    companion object {
        private const val TRADE_LIST_CACHE_TIME = 1_800_000
        const val BTC = "BTC"
        const val USDT = "USDT"
        var isLoggedOut = false
        const val ERROR_CODE_500 = "code=500"

        fun getBaseCurrencyTicker(): String =
            if (PreferenceRepository.baseCurrencyTicker == "USD" || PreferenceRepository.baseCurrencyTicker == "ZAR") {
                if (PreferenceRepository.baseCurrencyTicker == "ZAR")
                    PreferenceRepository.baseCurrencyTicker = "USD"
                USDT
            } else PreferenceRepository.baseCurrencyTicker

        fun getBaseCurrencyCharacter(): String =
            when (getBaseCurrencyTicker()) {
                USDT -> "$"
                "EUR" -> "€"
                "RUB" -> "₽"
                "GBP" -> "£"
                "AUD" -> "AU$"
                "BRL" -> "R$"
                "NGN" -> "₦"
                "UAH" -> "₴"
                else -> ""
            }

    }
}
