package io.sevenb.terminal.domain.usecase.auth

import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.enum_model.ErrorMessageType
import io.sevenb.terminal.data.repository.auth.PreferenceRepository
import io.sevenb.terminal.device.cryptography.CiphertextWrapper
import io.sevenb.terminal.device.cryptography.CryptographyManager
import io.sevenb.terminal.device.utils.Serializer
import io.sevenb.terminal.exceptions.NewAuthException
import timber.log.Timber
import javax.crypto.Cipher

class FingerPrintRestore(
    private val preferenceRepository: PreferenceRepository,
    private val cryptographyManager: CryptographyManager
) {

    suspend fun provideDecryptionCipher(): Cipher? {
        when (val encryptedPassword = preferenceRepository.restoreEncryptedPassword()) {
            is Result.Success -> {
                val data = encryptedPassword.data
                if (data.isNotEmpty()) {
                    val ciphertextWrapper =
                        Serializer.deserialize(data, CiphertextWrapper::class.java)
                    return cryptographyManager.getInitializedCipherForDecryption(ciphertextWrapper.initializationVector)
                } else {
                  //  throw NewAuthException(ErrorMessageType.NO_FINGERPRINT)
                    Timber.e("encryptedPassword is empty error")
                }
            }
            else -> Timber.e("encryptedPassword error")
        }

        return null
    }

    suspend fun decryptPassword(cipher: Cipher): String? {
        when (val encryptedPassword = preferenceRepository.restoreEncryptedPassword()) {
            is Result.Success -> {
                val data = encryptedPassword.data
                if (data.isNotEmpty()) {
                    val ciphertextWrapper =
                        Serializer.deserialize(data, CiphertextWrapper::class.java)
                    return cryptographyManager.decryptData(ciphertextWrapper.ciphertext, cipher)
                } else
                    Timber.e("encryptedPassword is empty error")
            }
            else -> Timber.e("encryptedPassword error")
        }
        return null
    }

    suspend fun deleteStoredPassword() {
        when (val encryptedPassword = preferenceRepository.storeEncryptedPassword("")) {
            is Result.Success -> {

            }
        }
    }

}
