package io.sevenb.terminal.domain.usecase.trading

import io.sevenb.terminal.data.model.broker_api.markets.Currency
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.trading.TradeChartRange
import io.sevenb.terminal.domain.repository.MarketApi
import io.sevenb.terminal.domain.repository.RawDataRepository
import io.sevenb.terminal.ui.screens.select_currency.DepositCurrencyItem
import io.sevenb.terminal.ui.screens.trading.TradingWalletItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.IOException

class TradingCurrencies(
    private val marketApi: MarketApi,
    private val rawDataRepository: RawDataRepository
) {

    private suspend fun networkCurrencies(): Result<List<Currency>> {
        return withContext(Dispatchers.IO) {
            when (val currenciesResult = marketApi.currencies()) {
                is Result.Success -> {
                    val list = currenciesResult.data.toMutableList()
                    marketApi.listOfCurrencies = list

                    return@withContext Result.Success(list)
                }
                is Result.Error -> Result.Error(IOException("Can't get currencies list"))
            }
        }
    }

    private suspend fun cacheFirstCurrencies(): Result<List<Currency>> {
        return withContext(Dispatchers.IO) {
            val list = marketApi.listOfCurrencies
            if (list.isNotEmpty()) {
                Result.Success(list)
            } else {
                networkCurrencies()
            }
        }
    }

    suspend fun mappedDepositList(): Result<List<DepositCurrencyItem>> {
        return withContext(Dispatchers.IO) {
            when (val currenciesResult = cacheFirstCurrencies()) {
                is Result.Success -> {
                    val depositList = mutableListOf<DepositCurrencyItem>()
                    val list = currenciesResult.data.toMutableList()
                    list.forEach {
                        it.networks.forEach network@{ currencyNetwork ->
                            if (!currencyNetwork.deposit) return@network

                            depositList.add(
                                DepositCurrencyItem(
                                    it.name,
                                    it.ticker,
                                    network = currencyNetwork.network,
                                    cmcIconId = rawDataRepository.mapTickerToCmcIconId?.get(it.ticker) ?: 0,
                                    isExpanded = false,
                                    updateExpandedStateChannel = null,
                                    currentRange = TradeChartRange.DAY
                                )
                            )
                        }
                    }
                    return@withContext Result.Success(depositList)
                }
                is Result.Error -> Result.Error(IOException("Can't currencies list"))
            }
        }
    }
}
