package io.sevenb.terminal.domain.usecase.withdrawal

import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.withdrawal.WithdrawalHistoryItem
import io.sevenb.terminal.domain.repository.AccountApi

class RequestWithdrawalHash(
    private val accountApi: AccountApi
) {
    suspend fun execute(id: String) : Result<WithdrawalHistoryItem> =  accountApi.withdrawalHash(id)
//        return withContext(Dispatchers.IO) {
//            when(val withdrawalHistoryResult =) {
//                is Result.Success -> Result.Success(withdrawalHistoryResult.data)
////                is Result.Error -> Result.Error(IOException("Can't get withdrawal history"))
//            }
//        }
//    }
}