package io.sevenb.terminal.domain.usecase.tfa

import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.enum_model.ErrorMessageType
import io.sevenb.terminal.data.model.tfa.TfaVerification
import io.sevenb.terminal.data.repository.two_fa.TfaRepository
import io.sevenb.terminal.domain.usecase.auth.ProcessTokens
import io.sevenb.terminal.exceptions.NewAuthException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class TfaHandler @Inject constructor(
    private val twoFaRepository: TfaRepository,
    private val processTokens: ProcessTokens,
) {

    //    suspend fun twoFaIsEnabled() = twoFaRepository.twoFaIsEnabled()
    suspend fun toggleTfa() = twoFaRepository.toggleTfa()
    suspend fun verify(code: String, email: String, password: String): Result<Unit> {

        val tfaVerificationCode = TfaVerification(email, code)

        return withContext(Dispatchers.IO) {
            val tfaVerificationResult = twoFaRepository.verify(tfaVerificationCode)
//            if (tfaVerificationResult is Result.Success && tfaVerificationResult.data.token == true) {
            if (tfaVerificationResult is Result.Success && tfaVerificationResult.data.token != false) {
                when (val processResult = processTokens.processTokens(
                    email,
                    password,
                    tfaVerificationResult.data.access ?: tfaVerificationResult.data.accessToken,
                    tfaVerificationResult.data.refresh ?: tfaVerificationResult.data.refreshToken
                )) {
                    is Result.Success -> {
                        Result.Success(Unit)
                    }
                    is Result.Error -> {
                        Result.Error(processResult.exception)
                    }
                }
            } else {
                Result.Error(NewAuthException(ErrorMessageType.TFA_VERIFICATION))
            }
        }
    }

    companion object {
        const val GOOGLE_AUTHENTICATOR_FORMAT = "otpauth://totp/%s?secret=%s&issuer=%s"
    }
}
