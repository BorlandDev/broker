package io.sevenb.terminal.domain.repository

import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.cryptocompare.CryptocompareResult


interface ChartApi {

    suspend fun getHistoday(from: String, to: String, limit: Int, aggregate: Int, allData: Boolean) : Result<CryptocompareResult>
    suspend fun getHourlyPair(from: String, to: String, limit: Int) : Result<CryptocompareResult>

}