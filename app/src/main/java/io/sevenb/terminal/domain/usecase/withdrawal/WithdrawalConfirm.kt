package io.sevenb.terminal.domain.usecase.withdrawal

import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.withdrawal.ConfirmWithdrawalParams
import io.sevenb.terminal.domain.repository.AccountApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.IOException

class WithdrawalConfirm(
    private val accountApi: AccountApi
) {

    suspend fun execute(confirmWithdrawalParams: ConfirmWithdrawalParams): Result<Unit> {
        return withContext(Dispatchers.IO) {
            when (accountApi.confirmWithdrawal(confirmWithdrawalParams)) {
                is Result.Success -> Result.Success(Unit)
                is Result.Error -> Result.Error(IOException("Incorrect code"))
            }
        }
    }

}
