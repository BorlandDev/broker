package io.sevenb.terminal.domain.usecase.auth

import io.sevenb.terminal.data.model.auth.SignUpResend
import io.sevenb.terminal.data.model.broker_api.RequestTokenResponse
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.repository.auth.UserAuthRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ResendConfirmationCode(
    private val userAuthRepository: UserAuthRepository
) {

    suspend fun execute(
        registrationToken: String,
        email: String = ""
    ): Result<RequestTokenResponse> {
        return withContext(Dispatchers.IO) {
            val signUpResend = SignUpResend(registrationToken, email)
            userAuthRepository.resendCode(signUpResend)
        }
    }
}
