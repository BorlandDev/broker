package io.sevenb.terminal.domain.usecase.auth

import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.repository.auth.PreferenceRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class StoreRefreshToken(
    private val preferenceRepository: PreferenceRepository
) {

    suspend fun execute(refreshToken: String): Result<Unit> {
        return withContext(Dispatchers.IO) {
            return@withContext preferenceRepository.storeRefreshToken(refreshToken)
        }
    }
}
