package io.sevenb.terminal.domain.usecase.order

import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.order.OrderItem
import io.sevenb.terminal.data.model.order.OrderParams
import io.sevenb.terminal.domain.repository.MarketApi
import io.sevenb.terminal.domain.repository.OrdersApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class OrderOperations(
    private val marketApi: MarketApi,
    private val ordersApi: OrdersApi
) {

    suspend fun orderPost(orderParams: OrderParams): Result<OrderItem> {
        return withContext(Dispatchers.IO) {
            return@withContext marketApi.orderPost(orderParams)
        }
    }

    suspend fun orderOpened(): Result<List<OrderItem>> {
        return withContext(Dispatchers.IO) {
            return@withContext ordersApi.orderOpened()
        }
    }

    suspend fun cancelOrder(orderItem: OrderItem): Result<OrderItem> {
        return withContext(Dispatchers.IO) {
            return@withContext ordersApi.cancelOrder(orderItem.symbol, orderItem.orderId)
        }
    }

    suspend fun ordersHistory(): Result<List<OrderItem>> {
        return withContext(Dispatchers.IO) {
            ordersApi.ordersHistory()
        }
    }
}
