package io.sevenb.terminal.domain.repository

import io.sevenb.terminal.data.model.candles.CandlesData
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.trading.CandleChartParams

interface CandlesApi {

    suspend fun getCandle(candleChartParams: CandleChartParams): Result<CandlesData>
}