package io.sevenb.terminal.domain.usecase.new_auth

import androidx.biometric.BiometricPrompt
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleObserver
import io.sevenb.terminal.R
import io.sevenb.terminal.device.biometric.BiometricPromptUtils
import io.sevenb.terminal.domain.usecase.auth.StorePassword
import javax.crypto.Cipher
import javax.inject.Inject

class HandleFingerprint @Inject constructor(
    private var storePassword: StorePassword
): LifecycleObserver {

    lateinit var fragment: Fragment

    fun init(fragment: Fragment) {
        this.fragment = fragment
    }

    fun createFingerprintDialog(
        cipher: Cipher?,
        biometricUnavailable: () -> Unit,
        biometricSuccess: (BiometricPrompt.AuthenticationResult) -> Unit,
        biometricError: (Int) -> Unit,
        buttonString: String = fragment.getString(R.string.prompt_info_use_app_password)
    ) {
        if (storePassword.isBiometricSuccess()) {
            cipher?.let {
                val biometricPrompt =
                    BiometricPromptUtils.createBiometricPrompt(
                        fragment,
                        biometricSuccess,
                        biometricError
                    )
                val promptInfo = BiometricPromptUtils.createLoginPromptInfo(fragment, buttonString)
                biometricPrompt.authenticate(promptInfo, BiometricPrompt.CryptoObject(it))
            }
        } else {
            biometricUnavailable()
        }
    }

    fun decryptFingerprintDialog(
        cipher: Cipher?,
        biometricUnavailable: () -> Unit,
        biometricSuccess: (BiometricPrompt.AuthenticationResult) -> Unit,
        biometricError: (Int) -> Unit,
        buttonString: String = fragment.getString(R.string.prompt_info_use_app_password)
    ) {
        if (storePassword.isBiometricSuccess()) {
            cipher?.let {
                val biometricPrompt =
                    BiometricPromptUtils.createBiometricPrompt(
                        fragment,
                        biometricSuccess,
                        biometricError
                    )
                val promptInfo = BiometricPromptUtils.createLoginPromptInfo(fragment, buttonString)
                biometricPrompt.authenticate(promptInfo, BiometricPrompt.CryptoObject(cipher))
            }
        } else {
            biometricUnavailable()
        }
    }

    fun isBiometricSuccess() = storePassword.isBiometricSuccess()

    fun getBiometricAuthenticateAbility() = storePassword.getBiometricAuthenticateAbility()
}