package io.sevenb.terminal.domain.usecase.order

import io.sevenb.terminal.data.model.broker_api.markets.Market
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.domain.repository.MarketApi
import io.sevenb.terminal.utils.ConstUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.io.IOException

class MarketsData(
    private val marketApi: MarketApi
) {

    private suspend fun networkMarkets(): Result<List<Market>> {
        return withContext(Dispatchers.IO) {
            when (val currenciesResult = marketApi.markets()) {
                is Result.Success -> {
                    Timber.d("SPEEDTEST networkMarkets: " + System.currentTimeMillis())
                    val list = currenciesResult.data.toMutableList()
                    marketApi.listOfMarkets = list

                    return@withContext Result.Success(list)
                }
                is Result.Error -> Result.Error(IOException(ConstUtils.CANT_GET_MARKET_LIST))
            }
        }
    }

    suspend fun cacheFirstMarkets(): Result<List<Market>> {
        Timber.d("SPEEDTEST cacheFirstMarkets: " + System.currentTimeMillis())

        return withContext(Dispatchers.IO) {
            val list = marketApi.listOfMarkets
            if (list.isNotEmpty()) {
                Result.Success(list)
            } else {
                networkMarkets()
            }
        }
    }

//    suspend fun updateMarkets() {
//        networkMarkets()
//    }

    suspend fun getFilters(symbol: String) = marketApi.getFilters(symbol)
}
