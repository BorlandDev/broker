package io.sevenb.terminal.domain.usecase.auth

import io.sevenb.terminal.data.model.broker_api.RefreshTokenRequest
import io.sevenb.terminal.data.model.broker_api.TokensResponse
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.domain.repository.AuthApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class UpdateRefreshToken(
    private val authApi: AuthApi
) {

    suspend fun execute(
        refreshToken: String
    ): Result<TokensResponse> {
        return withContext(Dispatchers.IO) {
            val refreshTokenRequest = RefreshTokenRequest(refreshToken)
            authApi.refreshAccessToken(refreshTokenRequest)
        }
    }

}
