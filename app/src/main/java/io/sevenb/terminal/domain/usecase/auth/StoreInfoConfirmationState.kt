package io.sevenb.terminal.domain.usecase.auth

import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.domain.repository.StorageRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class StoreInfoConfirmationState(
    private val storageRepository: StorageRepository
) {

    suspend fun execute(): Result<Unit> {
        return withContext(Dispatchers.IO) {
            return@withContext storageRepository.storeInfoConfirmationState(false)
        }
    }

    suspend fun restore(): Result<Boolean> {
        return withContext(Dispatchers.IO) {
            return@withContext storageRepository.restoreInfoConfirmationState()
        }
    }

}
