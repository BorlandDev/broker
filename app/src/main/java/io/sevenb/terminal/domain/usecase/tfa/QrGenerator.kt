package io.sevenb.terminal.domain.usecase.tfa

import android.graphics.Bitmap
import android.graphics.Color
import com.google.zxing.BarcodeFormat
import com.google.zxing.qrcode.QRCodeWriter
import javax.inject.Inject

class QrGenerator @Inject constructor(
    private val qrCodeWriter: QRCodeWriter
) {

    fun getQrCodeBitmap(secret: String): Bitmap {
        val bitMatrix = qrCodeWriter.encode(secret, BarcodeFormat.QR_CODE, 512, 512)
        val width = bitMatrix.width
        val height = bitMatrix.height

        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)
        for (x in 0 until width) {
            for (y in 0 until height) {
                bitmap.setPixel(x, y, if (bitMatrix.get(x, y)) Color.BLACK else Color.WHITE)
            }
        }

        return bitmap
    }
}