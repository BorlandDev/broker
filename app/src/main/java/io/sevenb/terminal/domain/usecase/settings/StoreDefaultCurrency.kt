package io.sevenb.terminal.domain.usecase.settings

import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.repository.auth.PreferenceRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class StoreDefaultCurrency(
    private val preferenceRepository: PreferenceRepository
) {

    suspend fun execute(ticker: String): Result<Unit> {
        return withContext(Dispatchers.IO) {
            preferenceRepository.storeBaseCurrency(ticker)
        }
    }

}
