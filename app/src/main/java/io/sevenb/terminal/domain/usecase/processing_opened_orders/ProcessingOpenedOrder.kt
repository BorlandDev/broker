package io.sevenb.terminal.domain.usecase.processing_opened_orders

import io.sevenb.terminal.data.datasource.database.model.CurrencyEntity
import io.sevenb.terminal.data.model.broker_api.rates.MarketRateParams
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.order.OrderItem
import io.sevenb.terminal.data.repository.breakdown.BreakdownRepository
import io.sevenb.terminal.domain.repository.RawDataRepository
import io.sevenb.terminal.domain.usecase.charts.AccountData

class ProcessingOpenedOrder(
    private val breakdownRepository: BreakdownRepository,
    private val accountData: AccountData,
    private val rawDataRepository: RawDataRepository
) {

    suspend fun processOrders(
        orderItems: List<OrderItem>,
        currencies: List<CurrencyEntity>
    ): Result<List<OrderItem>> {
        val savedCurrencies = mutableMapOf<String, String>()

        currencies.forEach {
            savedCurrencies[it.ticker] = it.ticker
        }

        val listOfParams = mutableListOf<MarketRateParams>()
        breakdownRepository.initSavedCurrencies(savedCurrencies)
        orderItems.forEach {
            val pair = breakdownRepository.breakdown(it.symbol)
            pair.first?.let { firstTicker ->
                it.apply {
                    baseAsset = firstTicker
                    baseAssetIconId = rawDataRepository.mapTickerToCmcIconId?.get(firstTicker)
                }
                listOfParams.add(
                    MarketRateParams(firstTicker, AccountData.getBaseCurrencyTicker())
                )
            }
            pair.second?.let { secondTicker ->
                it.apply {
                    quoteAsset = secondTicker
                    quoteAssetIconId = rawDataRepository.mapTickerToCmcIconId?.get(secondTicker)
                }
                listOfParams.add(
                    MarketRateParams(secondTicker, AccountData.getBaseCurrencyTicker())
                )
            }
        }
        breakdownRepository.deleteSavedCurrencies()

        return when (val pricesResult = accountData.getPrices(listOfParams)) {
            is Result.Success -> {
                orderItems.forEachIndexed { index, orderItem ->
                    val firstIndex = index * 2
                    val secondIndex = firstIndex + 1
                    orderItem.baseAssetRate = pricesResult.data[firstIndex]?.rate
                    orderItem.quoteAssetRate = pricesResult.data[secondIndex]?.rate
                }
                Result.Success(orderItems)
            }
            is Result.Error -> pricesResult
        }
    }
}