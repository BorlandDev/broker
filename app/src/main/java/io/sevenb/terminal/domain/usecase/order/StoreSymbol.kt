package io.sevenb.terminal.domain.usecase.order

import com.google.gson.reflect.TypeToken
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.device.utils.Serializer
import io.sevenb.terminal.domain.repository.StorageRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.io.IOException
import java.lang.reflect.Type

class StoreSymbol(
    private val storageRepository: StorageRepository
) {

    suspend fun updateOrderSymbolList(symbol: String) {
        return withContext(Dispatchers.IO) {
            when (val stringOfListResult = storageRepository.restoreListOfOrderSymbols()) {
                is Result.Success -> {
                    var stringOfList = stringOfListResult.data
                    var listOfSymbols = mutableListOf<String>()

                    val type: Type = object : TypeToken<MutableList<String?>?>() {}.type

                    if (stringOfList.isNotEmpty()) {
                        listOfSymbols = Serializer.deserializeType(stringOfList, type)
                        if (listOfSymbols.contains(symbol)) return@withContext
                    }

                    stringOfList = Serializer.serializeType(listOfSymbols, type) ?: ""
                    storageRepository.storeListOfOrderSymbols(stringOfList)
                }
                is Result.Error -> Timber.d("storeOrderSymbol error")
            }
        }
    }

    suspend fun orderSymbolList() : Result<List<String>> {
        return withContext(Dispatchers.IO) {
            when (val stringOfListResult = storageRepository.restoreListOfOrderSymbols()) {
                is Result.Success -> {
                    val stringOfList = stringOfListResult.data
                    var listOfSymbols = mutableListOf<String>()

                    val type: Type = object : TypeToken<MutableList<String?>?>() {}.type

                    if (stringOfList.isNotEmpty()) {
                        listOfSymbols = Serializer.deserializeType(stringOfList, type)
                    }

                    Result.Success(listOfSymbols)
                }
                is Result.Error -> Result.Error(IOException("Can't get storage symbol"))
            }
        }
    }
}
