package io.sevenb.terminal.domain.usecase.deposit

import io.sevenb.terminal.data.model.broker_api.account.DepositAddress
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.domain.repository.AccountApi
import io.sevenb.terminal.ui.screens.select_currency.DepositCurrencyItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.IOException

class RequestDepositAddress(
    private val accountApi: AccountApi
) {

    suspend fun execute(currency: DepositCurrencyItem) : Result<DepositAddress> {
        return withContext(Dispatchers.IO) {
            when(val depositAddressResult = accountApi.depositAddress(currency)) {
                is Result.Success -> Result.Success(depositAddressResult.data)
                is Result.Error -> Result.Error(IOException("Can't get currencies list"))
            }
        }
    }

}
