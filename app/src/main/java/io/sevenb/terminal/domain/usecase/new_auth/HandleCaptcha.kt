package io.sevenb.terminal.domain.usecase.new_auth

import android.app.Activity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.geetest.sdk.GT3ConfigBean
import com.geetest.sdk.GT3ErrorBean
import com.geetest.sdk.GT3GeetestUtils
import com.geetest.sdk.GT3Listener
import com.geetest.sdk.utils.GT3ServiceNode
import io.sevenb.terminal.data.model.enum_model.ErrorMessageType
import io.sevenb.terminal.data.model.enums.GeeTestResult
import io.sevenb.terminal.ui.screens.auth.new_auth.NewAuthSharedViewModel
import io.sevenb.terminal.utils.Logger
import org.json.JSONObject
import java.io.IOException

//error codes - https://docs.geetest.com/captcha/apirefer/errorcode/android
class HandleCaptcha : LifecycleObserver {

    private lateinit var gT3GeetestUtils: GT3GeetestUtils
    private lateinit var gt3ConfigBean: GT3ConfigBean
    private lateinit var sharedAuthViewModel: NewAuthSharedViewModel

    fun initCaptchaConfig(activity: Activity?, sharedAuthViewModel: NewAuthSharedViewModel) {
        this.sharedAuthViewModel = sharedAuthViewModel
        gT3GeetestUtils = GT3GeetestUtils(activity)
        gt3ConfigBean = GT3ConfigBean()
        gt3ConfigBean.apply {
            pattern = 1
            isUnCanceledOnTouchKeyCodeBack = true
            isCanceledOnTouchOutside = true
            lang = null
            timeout = 10000
            webviewTimeout = 10000
            gt3ServiceNode = GT3ServiceNode.NODE_CHINA
            isCanceledOnTouchOutside = false
            listener = object : GT3Listener() {

                override fun onSuccess(p0: String?) {
                    Logger.sendLog(
                        "HandleCaptcha",
                        "onSuccess",
                        "$p0"
                    )
                    sharedAuthViewModel.onCaptchaSuccess()
                }

                override fun onFailed(errorBean: GT3ErrorBean?) {
                    Logger.sendLog(
                        "HandleCaptcha",
                        "onFailed",
                        IOException(errorBean?.errorCode)
                    )
                    sharedAuthViewModel.showErrorMessage(ErrorMessageType.CAPTCHA_TIMEOUT)
                    sharedAuthViewModel.onCaptchaFailed()
                }

                override fun onReceiveCaptchaCode(p0: Int) {
                }

                override fun onStatistics(p0: String?) {

                }

                //0 is failure. 1 is normal and 2 closed by user
                override fun onClosed(p0: Int) {
                    Logger.sendLog(
                        "HandleCaptcha",
                        "onClosed",
                        "$p0"
                    )
                    sharedAuthViewModel.closeCaptcha()
//                    when (p0) {
//                        0 -> sharedAuthViewModel.onCaptchaFailed()
//                        1, 2 -> sharedAuthViewModel.closeCaptcha()
//                    }
                }

                override fun onButtonClick() {
                    sharedAuthViewModel.showCaptcha()
                }

                override fun onDialogReady(p0: String?) {
                    super.onDialogReady(p0)
                }

                override fun onDialogResult(p0: String?) {
                    Logger.sendLog("HandleCaptcha", "onDialogResult", p0)
                    p0 ?: return
                    sharedAuthViewModel.onCaptchaResult(p0)
                }
            }
        }

        gT3GeetestUtils.init(gt3ConfigBean)
    }

    fun geeTestResult(geeTestResult: GeeTestResult) {
        when (geeTestResult) {
            GeeTestResult.SUCCESS -> gT3GeetestUtils.showSuccessDialog()
            GeeTestResult.FAIL -> gT3GeetestUtils.showFailedDialog()
            GeeTestResult.USER_EXISTS,
            GeeTestResult.PARSE_JSON_FAIL,
            GeeTestResult.BAD_PARAMS,
            GeeTestResult.TOO_MANY_ATTEMPTS,
            GeeTestResult.RESEND_FAIL,
            GeeTestResult.CONFIRM_FAIL -> gT3GeetestUtils.dismissGeetestDialog()
        }
    }

    fun startCaptchaFlow() {
        gT3GeetestUtils.startCustomFlow()
    }

    fun showCaptcha(jsonObj: JSONObject?) {
        gt3ConfigBean.api1Json = jsonObj
        gT3GeetestUtils.getGeetest()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun destroyCaptcha() {
        gT3GeetestUtils.destory()
    }

}