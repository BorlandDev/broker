package io.sevenb.terminal.domain.usecase.trading

import com.google.gson.reflect.TypeToken
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.repository.auth.PreferenceRepository
import io.sevenb.terminal.device.utils.Serializer
import io.sevenb.terminal.ui.screens.trading.FavoriteTradeItem
import io.sevenb.terminal.ui.screens.trading.TradingWalletItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.IOException
import java.lang.reflect.Type

class FavoriteCurrencies(
    private val preferenceRepository: PreferenceRepository
) {

    suspend fun favoriteTickers(tradeList: List<TradingWalletItem>): Result<List<FavoriteTradeItem>> {
        return withContext(Dispatchers.IO) {
            when (val favoriteTickersResult = preferenceRepository.restoreFavoriteTickers()) {
                is Result.Success -> {
                    val stringTickers = favoriteTickersResult.data

                    if (stringTickers.isNotEmpty()) {
                        val type: Type = object : TypeToken<MutableList<String?>?>() {}.type
                        val listOfFavoritesTickers: MutableList<String> =
                            Serializer.deserializeType(stringTickers, type)
                        if (listOfFavoritesTickers.isNotEmpty()) {
                            val favoritesList = tradeList
                                .filter { listOfFavoritesTickers.contains(it.ticker) }
                                .map {
                                    FavoriteTradeItem(
                                        it.name,
                                        it.ticker,
                                        it.cmcIconId,
                                        it.price,
                                        isSellAvailable = it.isSellAvailable,
                                        chartEntries = it.chartEntries,
                                        expandedChartEntries = it.expandedChartEntries,
                                        lastChartUpdate = it.lastChartUpdate,
                                        updateChartRangeChannel = it.updateChartRangeChannel,
                                        updateTimeChannel = it.updateTimeChannel,
                                        currentRange = it.currentRange,
                                        isExpanded = it.isExpanded,
                                        updateExpandedStateChannel = it.updateExpandedStateChannel
                                    )
                                }
                            return@withContext Result.Success(favoritesList)
                        }
                    }

                    return@withContext Result.Success(listOf())
                }
                is Result.Error -> Result.Error(IOException("Can't get favorites list"))
            }
        }
    }

    suspend fun processFavorite(ticker: String, isFavorite: Boolean): Result<Unit> {
        return withContext(Dispatchers.IO) {
            when (val favoriteTickersResult = preferenceRepository.restoreFavoriteTickers()) {
                is Result.Success -> {
                    val stringTickers = favoriteTickersResult.data
                    val type: Type = object : TypeToken<MutableList<String?>?>() {}.type
                    var listOfFavoritesTickers = mutableListOf<String>()

                    if (stringTickers.isNotEmpty()) {
                        listOfFavoritesTickers = Serializer.deserializeType(stringTickers, type)
                    }

                    if (isFavorite) listOfFavoritesTickers.remove(ticker)
                    else listOfFavoritesTickers.add(ticker)

                    val listString = Serializer.serializeType(listOfFavoritesTickers, type) ?: ""
                    preferenceRepository.storeFavoriteTickers(listString)
                    Result.Success(Unit)
                }
                is Result.Error -> Result.Error(IOException("Can't add to favorites list"))

            }
        }
    }

}
