package io.sevenb.terminal.domain.usecase.auth

import io.sevenb.terminal.data.model.auth.UserData
import io.sevenb.terminal.data.model.broker_api.RequestTokenResponse
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.repository.auth.UserAuthRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class RequestVerificationCode(
    private val userRepositoryRepository: UserAuthRepository
) {

    suspend fun execute(
        userData: UserData
    ): Result<RequestTokenResponse> {
        return withContext(Dispatchers.IO) {
            userRepositoryRepository.requestVerificationCodeRegistration(userData)
        }
    }

    suspend fun executeRestorePassword(
        login: String
    ): Result<RequestTokenResponse> {
        return withContext(Dispatchers.IO) {
            userRepositoryRepository.requestVerificationCodeRestorePassword(login)
        }
    }
}
