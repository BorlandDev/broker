package io.sevenb.terminal.domain.usecase.safety

import android.util.Base64
import java.security.Key
import javax.crypto.Cipher
import javax.crypto.spec.SecretKeySpec

class AESCrypt {
    private val ALGORITHM = "AES"
    private val KEY = "1Hbfh667adfDEJ78"

    @Throws(Exception::class)
    fun encrypt(value: String): String? {
        val key: Key = generateKey()
        val cipher: Cipher =
            Cipher.getInstance(ALGORITHM)
        cipher.init(Cipher.ENCRYPT_MODE, key)
        return try {
            val encryptedByteValue: ByteArray =
                cipher.doFinal(value.toByteArray(charset("utf-8")))
            Base64.encodeToString(encryptedByteValue, Base64.DEFAULT)
        } catch (e: Exception) {
            null
        }
    }

    @Throws(Exception::class)
    fun decrypt(value: String?): String {
        val key: Key = generateKey()
        val cipher: Cipher =
            Cipher.getInstance(ALGORITHM)
        cipher.init(Cipher.DECRYPT_MODE, key)
        val decryptedValue64: ByteArray = Base64.decode(value, Base64.DEFAULT)
        return try {
            val decryptedByteValue: ByteArray = cipher.doFinal(decryptedValue64)
            String(decryptedByteValue, charset("utf-8"))
        } catch (e: java.lang.Exception) {
            ""
        }
    }

    @Throws(Exception::class)
    private fun generateKey(): Key {
        return SecretKeySpec(KEY.toByteArray(), ALGORITHM)
    }
}