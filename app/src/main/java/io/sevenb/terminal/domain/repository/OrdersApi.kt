package io.sevenb.terminal.domain.repository

import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.order.OrderItem


interface OrdersApi {

    suspend fun orderOpened(): Result<List<OrderItem>>
    suspend fun cancelOrder(
        symbol: String, orderId: String
    ): Result<OrderItem>
    suspend fun ordersHistory(): Result<List<OrderItem>>

}