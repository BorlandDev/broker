package io.sevenb.terminal.domain.usecase.verification

import io.sevenb.terminal.data.model.broker_api.account.AccountInfo
import io.sevenb.terminal.data.model.broker_api.account.AccountStatus
import io.sevenb.terminal.data.model.broker_api.verification.Verification
import io.sevenb.terminal.data.model.broker_api.verification.VerificationToken
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.domain.repository.AccountApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class VerificationData(
    private val accountApi: AccountApi
) {

    suspend fun accountStatus(): Result<AccountStatus> {
        return withContext(Dispatchers.IO) {
            accountApi.accountStatus()
        }
    }

    suspend fun accountInfo(): Result<AccountInfo> {
        return withContext(Dispatchers.IO) {
            accountApi.accountInfo()
        }
    }

    suspend fun gerVerifications(): Result<List<Verification>> {
        return withContext(Dispatchers.IO) {
            accountApi.getVerifications()
        }
    }

    suspend fun createVerification(): Result<VerificationToken> {
        return withContext(Dispatchers.IO) {
            accountApi.createVerification()
        }
    }

    suspend fun refreshToken(): Result<VerificationToken> {
        return withContext(Dispatchers.IO) {
            accountApi.refreshVerificationToken()
        }
    }

    companion object {
        const val ACCOUNT_STATUS_CONFIRMED = "confirmed"
        const val ACCOUNT_STATUS_VERIFIED = "verified"
        const val ACCOUNT_STATUS_NEW = "new"
    }
}
