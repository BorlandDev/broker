package io.sevenb.terminal.domain.usecase.auth

import io.sevenb.terminal.data.model.auth.SignUpVerify
import io.sevenb.terminal.data.model.broker_api.RequestTokenResponse
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.repository.auth.UserAuthRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class SendConfirmationCode(
    private val userRepositoryRepository: UserAuthRepository
) {

    suspend fun execute(
        registrationToken: String,
        confirmationCode: String
    ): Result<RequestTokenResponse> {
        return withContext(Dispatchers.IO) {
            val signUpConfirm = SignUpVerify(registrationToken, confirmationCode)
            userRepositoryRepository.verifyCodeRegistration(signUpConfirm)
        }
    }

    suspend fun executeVerifyCode(
        login: String,
        confirmationCode: String,
        token: String,
    ): Result<RequestTokenResponse> {
        return withContext(Dispatchers.IO) {
            userRepositoryRepository.verifyCodeRestorePassword(login, confirmationCode, token)
        }
    }
}
