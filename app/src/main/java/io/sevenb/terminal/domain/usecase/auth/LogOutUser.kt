package io.sevenb.terminal.domain.usecase.auth

import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.enum_model.ErrorMessageType
import io.sevenb.terminal.data.repository.auth.FirebaseAuthRepository
import io.sevenb.terminal.data.repository.auth.UserAuthRepository
import io.sevenb.terminal.domain.usecase.safety.AccountSafetyStoring
import io.sevenb.terminal.exceptions.NewAuthException
import io.sevenb.terminal.utils.Logger
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class LogOutUser(
    private val userRepositoryRepository: UserAuthRepository,
    private val firebaseAuthRepository: FirebaseAuthRepository,
    private val safetyStoring: AccountSafetyStoring
) {
    suspend fun execute(): Result<Unit> {
        return withContext(Dispatchers.IO) {
            UserAuthRepository.accessToken = ""
            UserAuthRepository.refreshToken = ""
            val logOutResult = userRepositoryRepository.logout()
            if (logOutResult is Result.Success && logOutResult.data.result) {
                firebaseAuthRepository.logoutFirebase()
                safetyStoring.logOut()
                return@withContext Result.Success(Unit)
            } else {
                val exception = NewAuthException(ErrorMessageType.LOG_OUT)
                Logger.sendLog("LogOutUser", "execute", exception)
                return@withContext Result.Error(exception)
            }
        }
    }

}
