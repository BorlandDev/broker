package io.sevenb.terminal.domain.usecase.notification_handler

import io.sevenb.terminal.data.model.broker_api.PriceAlert
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.enums.notifications.NotificationsType
import io.sevenb.terminal.data.model.messages.NotificationsSettingsRequest
import io.sevenb.terminal.data.model.messages.SendMessageRequest
import io.sevenb.terminal.data.repository.notification_repository.NotificationsRepository
import java.io.IOException
import javax.inject.Inject

class NotificationHandler @Inject constructor(
    private val notificationsRepository: NotificationsRepository
) {

    suspend fun restoreEnableNotifications() =
        when (val enabledNotifications = restoreEnabledNotifications()) {
            is Result.Success -> {
                if (enabledNotifications.data.isNotEmpty()) Result.Success(true)
                else Result.Success(false)
            }
            is Result.Error -> Result.Error(IOException("Can't get enabled notifications"))
        }

    suspend fun storeEnableNotifications(value: Boolean) = when (val enableNotificationsResult =
        notificationsRepository.storeEnableNotifications(value)) {
        is Result.Success -> Result.Success(enableNotificationsResult.data.result)
        is Result.Error -> Result.Error(IOException("Can't enable notifications"))
    }

    suspend fun restoreEnabledNotifications() = when (val restoredNotificationsResult =
        notificationsRepository.restoreEnabledNotifications()) {
        is Result.Success -> Result.Success(restoredNotificationsResult.data)
        is Result.Error -> Result.Error(IOException("Can't restore enabled notifications"))
    }

    suspend fun storeEnabledNotifications(notificationType: NotificationsType) =
        when (val enabledNotificationsResult =
            notificationsRepository.storeEnabledNotifications(notificationType)) {
            is Result.Success -> Result.Success(enabledNotificationsResult.data)
            is Result.Error -> Result.Error(IOException("Can't save this notification"))
        }

    suspend fun createPriceAlert(bodyAlert: PriceAlert) =
        when (val enabledNotificationsResult =
            notificationsRepository.createPriceAlert(bodyAlert)) {
            is Result.Success -> Result.Success(enabledNotificationsResult.data)
            is Result.Error -> Result.Error(IOException(enabledNotificationsResult.error))
        }

    suspend fun getTextTypes() = notificationsRepository.testTextTypes()
//    suspend fun getTemplate() = notificationRepository.testTemplate()
//    suspend fun <T: Parcelable> getUserSettings() = notificationsRepository.getUserSettings<T>()
    suspend fun  getUserSettings() = notificationsRepository.getUserSettings()
    suspend fun  deletePriceAlert(id: String) = notificationsRepository.deletePriceAlert(id)
//    suspend fun  createPriceAlert(bodyAlert: PriceAlert) = notificationsRepository.createPriceAlert(bodyAlert)
    suspend fun  getPriceAlert() = notificationsRepository.getPriceAlert()

    suspend fun put(request: List<NotificationsSettingsRequest>) = notificationsRepository.put(request)

    suspend fun sendMessage() = notificationsRepository.sendMessage(SendMessageRequest())
}