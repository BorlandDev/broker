package io.sevenb.terminal.domain.usecase.withdrawal

import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.domain.repository.AccountApi
import io.sevenb.terminal.domain.repository.MarketApi
import io.sevenb.terminal.ui.screens.trading.TradingListAdapter.Companion.formatCryptoAmount
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class EstimatedDailyLimit(
    private val accountApi: AccountApi,
    val marketApi: MarketApi
) {

    companion object {
        private const val DAILY_LIMIT_BASE_CURRENCY = "BTC"
    }

    suspend fun execute(ticker: String): Result<String> {
        return withContext(Dispatchers.IO) {
            when (val availableWithdrawalResult = accountApi.availableWithdrawal()) {
                is Result.Success -> {
                    val remainingBtc = availableWithdrawalResult.data.remainingBtc

                    estimated(remainingBtc, ticker)
                }
                is Result.Error -> availableWithdrawalResult
            }
        }
    }

    private suspend fun estimated(dailyLimit: Float, ticker: String) : Result<String> {
        return when (val rateResult = marketApi.marketRate(DAILY_LIMIT_BASE_CURRENCY, ticker)) {
            is Result.Success -> {
                val marketRateResult = rateResult.data
                val rate = marketRateResult.rate
                val estimated = dailyLimit.times(rate ?: 1F)

                Result.Success("%s %s".format(formatCryptoAmount(estimated), ticker))
            }
            is Result.Error -> rateResult
        }
    }

}
