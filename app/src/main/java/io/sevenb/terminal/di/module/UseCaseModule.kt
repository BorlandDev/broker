package io.sevenb.terminal.di.module

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging
import com.google.zxing.qrcode.QRCodeWriter
import dagger.Module
import dagger.Provides
import io.sevenb.terminal.data.repository.auth.FirebaseAuthRepository
import io.sevenb.terminal.data.repository.auth.PreferenceRepository
import io.sevenb.terminal.data.repository.auth.UserAuthRepository
import io.sevenb.terminal.data.repository.breakdown.BreakdownRepository
import io.sevenb.terminal.data.repository.database.DbRepository
import io.sevenb.terminal.data.repository.notification_repository.NotificationsRepository
import io.sevenb.terminal.data.repository.trading.CandlesDataRepository
import io.sevenb.terminal.data.repository.trading.HistoryChartDataRepository
import io.sevenb.terminal.data.repository.two_fa.TfaRepository
import io.sevenb.terminal.device.cryptography.CryptographyManager
import io.sevenb.terminal.device.provider.DeviceManagerProvider
import io.sevenb.terminal.domain.repository.*
import io.sevenb.terminal.domain.usecase.auth.*
import io.sevenb.terminal.domain.usecase.auth_back_stack.AuthBackStackHandler
import io.sevenb.terminal.domain.usecase.charts.AccountData
import io.sevenb.terminal.domain.usecase.charts.CandlesData
import io.sevenb.terminal.domain.usecase.charts.ChartData
import io.sevenb.terminal.domain.usecase.charts.ChartDataHandler
import io.sevenb.terminal.domain.usecase.deposit.ClipboardCopy
import io.sevenb.terminal.domain.usecase.deposit.QrCodeBuilder
import io.sevenb.terminal.domain.usecase.deposit.RequestDepositAddress
import io.sevenb.terminal.domain.usecase.deposit.RequestDepositHistory
import io.sevenb.terminal.domain.usecase.firebase_auth.FirebaseAuthHandler
import io.sevenb.terminal.domain.usecase.historyfilter.HistoryFilterProcessing
import io.sevenb.terminal.domain.usecase.notification_handler.NotificationHandler
import io.sevenb.terminal.domain.usecase.order.*
import io.sevenb.terminal.domain.usecase.processing_opened_orders.ProcessingOpenedOrder
import io.sevenb.terminal.domain.usecase.safety.AESCrypt
import io.sevenb.terminal.domain.usecase.safety.AccountSafetyStoring
import io.sevenb.terminal.domain.usecase.safety.PassCodeStoring
import io.sevenb.terminal.domain.usecase.tfa.QrGenerator
import io.sevenb.terminal.domain.usecase.settings.ConfirmPhone
import io.sevenb.terminal.domain.usecase.settings.StoreDefaultCurrency
import io.sevenb.terminal.domain.usecase.sort.SortCurrenciesHandler
import io.sevenb.terminal.domain.usecase.trading.*
import io.sevenb.terminal.domain.usecase.tfa.TfaHandler
import io.sevenb.terminal.domain.usecase.verification.VerificationData
import io.sevenb.terminal.domain.usecase.withdrawal.*
import javax.inject.Singleton

/**
 * Dagger module to provide core data functionality.
 */
@Module
class UseCaseModule {

    /**
     * Auth use cases
     */
    @Provides
    @Singleton
    fun provideCaptchaDataInterop(
        confirmationApi: ConfirmationApi
    ): CaptchaDataInterop = CaptchaDataInterop(confirmationApi)

    @Provides
    @Singleton
    fun provideFingerPrintRestore(
        preferenceRepository: PreferenceRepository,
        cryptographyManager: CryptographyManager
    ): FingerPrintRestore = FingerPrintRestore(preferenceRepository, cryptographyManager)

    @Provides
    @Singleton
    fun provideRequestSmsCode(userRepositoryRepository: UserAuthRepository)
            : RequestVerificationCode = RequestVerificationCode(userRepositoryRepository)

    @Provides
    @Singleton
    fun provideSetNewPassword(userRepositoryRepository: UserAuthRepository)
            : SetNewPassword = SetNewPassword(userRepositoryRepository)

    @Provides
    @Singleton
    fun provideRestoreRefreshToken(preferenceRepository: PreferenceRepository)
            : RestoreRefreshToken = RestoreRefreshToken(preferenceRepository)

    @Provides
    @Singleton
    fun provideSendConfirmationCode(userRepositoryRepository: UserAuthRepository)
            : SendConfirmationCode = SendConfirmationCode(userRepositoryRepository)

    @Provides
    @Singleton
    fun provideSignInUser(
        userRepositoryRepository: UserAuthRepository,
        processTokens: ProcessTokens
//        firebaseAuthRepository: FirebaseAuthRepository,
//        storeRefreshToken : StoreRefreshToken
    )
//            : SignInUser = SignInUser(userRepositoryRepository, firebaseAuthRepository, storeRefreshToken )
            : SignInUser = SignInUser(userRepositoryRepository, processTokens)

    @Provides
    @Singleton
    fun provideProcessTokens(
        userRepositoryRepository: UserAuthRepository,
        firebaseAuthRepository: FirebaseAuthRepository,
        storeRefreshToken: StoreRefreshToken,
    )
            : ProcessTokens =
        ProcessTokens(userRepositoryRepository, firebaseAuthRepository, storeRefreshToken)


    @Provides
    @Singleton
    fun provideStorePassword(
        preferenceRepository: PreferenceRepository,
        cryptographyManager: CryptographyManager
    ): StorePassword = StorePassword(preferenceRepository, cryptographyManager)

    @Provides
    @Singleton
    fun provideStoreRefreshToken(preferenceRepository: PreferenceRepository)
            : StoreRefreshToken = StoreRefreshToken(preferenceRepository)

    @Provides
    @Singleton
    fun provideLogOutUser(
        userAuthRepository: UserAuthRepository,
        firebaseAuthRepository: FirebaseAuthRepository,
        safetyStoring: AccountSafetyStoring
    ): LogOutUser =
        LogOutUser(userAuthRepository, firebaseAuthRepository, safetyStoring)

    @Provides
    @Singleton
    fun provideLocalUserDataStoring(preferenceRepository: PreferenceRepository)
            : LocalUserDataStoring = LocalUserDataStoring(preferenceRepository)

    @Provides
    fun provideStartResendTimer(): StartResendTimer = StartResendTimer()

    @Provides
    fun provideResendConfirmationCode(userAuthRepository: UserAuthRepository): ResendConfirmationCode =
        ResendConfirmationCode(userAuthRepository)

    @Provides
    fun provideParseErrorMessage(): ParseErrorMessage = ParseErrorMessage()

    @Provides
    fun provideValidateEmailPassword(): ValidateEmailPassword = ValidateEmailPassword()

    @Provides
    fun provideStoreInfoConfirmationState(preferenceRepository: PreferenceRepository)
            : StoreInfoConfirmationState = StoreInfoConfirmationState(preferenceRepository)

    @Provides
    fun provideStoreFirstLaunch(preferenceRepository: PreferenceRepository)
            : StoreFirstLaunch = StoreFirstLaunch((preferenceRepository))

    /**
     * Deposit use cases
     */
    @Provides
    @Singleton
    fun provideClipboardCopy(
        deviceManagerProvider: DeviceManagerProvider
    ): ClipboardCopy = ClipboardCopy(deviceManagerProvider)

    @Provides
    @Singleton
    fun provideQrCodeBuilder(): QrCodeBuilder = QrCodeBuilder()

    @Provides
    @Singleton
    fun provideRequestDepositAddress(
        accountApi: AccountApi
    ): RequestDepositAddress = RequestDepositAddress(accountApi)

    @Provides
    @Singleton
    fun provideRequestDepositHistory(
        accountApi: AccountApi
    ): RequestDepositHistory = RequestDepositHistory(accountApi)

    /**
     * Order use cases
     */
    @Provides
    @Singleton
    fun provideOrderOperations(
        marketApi: MarketApi,
        ordersApi: OrdersApi
    ): OrderOperations = OrderOperations(marketApi, ordersApi)

    @Provides
    @Singleton
    fun provideMarketEstimate(
        marketApi: MarketApi
    ): MarketRate = MarketRate(marketApi)

    @Provides
    @Singleton
    fun provideParseFloatFromString(): ParseFloatFromString = ParseFloatFromString()

    @Provides
    @Singleton
    fun provideStoreSymbol(
        preferenceRepository: PreferenceRepository
    ): StoreSymbol = StoreSymbol(preferenceRepository)

    @Provides
    @Singleton
    fun provideMarketsData(
        marketApi: MarketApi
    ): MarketsData = MarketsData(marketApi)

    @Provides
    @Singleton
    fun provideBalanceAvailability(
        accountData: AccountData
    ): OrderAvailability = OrderAvailability(accountData)

    /**
     * Trading use cases
     */
    @Provides
    @Singleton
    fun provideAccountData(
        accountApi: AccountApi,
        ratesApi: RatesApi,
        marketApi: MarketApi,
        rawDataRepository: RawDataRepository,
        parseErrorMessage: ParseErrorMessage,
        preferenceRepository: PreferenceRepository,
        sortCurrenciesHandler: SortCurrenciesHandler,
        dbRepository: DbRepository
    ): AccountData = AccountData(
        accountApi,
        ratesApi,
        marketApi,
        rawDataRepository,
        parseErrorMessage,
        preferenceRepository,
        sortCurrenciesHandler,
        dbRepository
    )

    @Provides
    @Singleton
    fun provideTradingCurrencies(
        marketApi: MarketApi,
        rawDataRepository: RawDataRepository
    ): TradingCurrencies = TradingCurrencies(marketApi, rawDataRepository)

    @Provides
    @Singleton
    fun provideChartData(
        historyChartDataRepository: HistoryChartDataRepository
    ): ChartData = ChartData(historyChartDataRepository)

    @Provides
    @Singleton
    fun provideCandleChartData(
        candlesDataRepository: CandlesDataRepository
    ) = CandlesData(candlesDataRepository)

    @Provides
    @Singleton
    fun provideChartHandler() = ChartDataHandler()

    @Provides
    @Singleton
    fun provideFavoriteCurrencies(
        preferenceRepository: PreferenceRepository
    ): FavoriteCurrencies = FavoriteCurrencies(preferenceRepository)

    /**
     * Portfolio use cases
     */
    @Provides
    @Singleton
    fun provideMultipleFilterProcessing(
        requestDepositHistory: RequestDepositHistory,
        requestWithdrawalHistory: RequestWithdrawalHistory,
        orderOperations: OrderOperations,
        accountData: AccountData,
        rawDataRepository: RawDataRepository,
        breakdownRepository: BreakdownRepository
    ) = HistoryFilterProcessing(
        requestDepositHistory,
        requestWithdrawalHistory,
        orderOperations,
        accountData,
        rawDataRepository,
        breakdownRepository
    )

    @Provides
    @Singleton
    fun provideOpenedOrdersProcessing(
        breakdownRepository: BreakdownRepository,
        accountData: AccountData,
        rawDataRepository: RawDataRepository
    ) = ProcessingOpenedOrder(breakdownRepository, accountData, rawDataRepository)

    /**
     * Withdrawal use cases
     */
    @Provides
    @Singleton
    fun provideRequestWithdrawal(
        accountApi: AccountApi,
        parseErrorMessage: ParseErrorMessage
    ): RequestWithdrawal = RequestWithdrawal(accountApi, parseErrorMessage)

    @Provides
    @Singleton
    fun provideRequestWithdrawalHistory(
        accountApi: AccountApi
    ): RequestWithdrawalHistory = RequestWithdrawalHistory(accountApi)


    /**
     * Verification use cases
     */
    @Provides
    @Singleton
    fun provideVerificationData(
        accountApi: AccountApi
    ): VerificationData = VerificationData(accountApi)

    @Provides
    @Singleton
    fun provideWithdrawalConfirm(
        accountApi: AccountApi
    ): WithdrawalConfirm = WithdrawalConfirm(accountApi)

    @Provides
    @Singleton
    fun provideWithdrawalHash(
        accountApi: AccountApi
    ): RequestWithdrawalHash = RequestWithdrawalHash(accountApi)

    @Provides
    @Singleton
    fun provideEstimatedDailyLimit(
        accountApi: AccountApi,
        marketApi: MarketApi,
    ): EstimatedDailyLimit = EstimatedDailyLimit(accountApi, marketApi)

    /**
     * Settings use cases
     */
    @Provides
    @Singleton
    fun provideStoreDefaultCurrency(
        preferenceRepository: PreferenceRepository
    ): StoreDefaultCurrency = StoreDefaultCurrency(preferenceRepository)

    @Provides
    @Singleton
    fun provideConfirmPhone(
        authApi: AuthApi
    ): ConfirmPhone = ConfirmPhone(authApi)

    @Provides
    @Singleton
    fun provideUpdateRefreshToken(
        authApi: AuthApi
    ): UpdateRefreshToken = UpdateRefreshToken(authApi)

    /**
     * Sort use case
     */
    @Provides
    @Singleton
    fun provideSortCurrencies() = SortCurrenciesHandler()

    /**
     * Creating password check use case
     */
    @Provides
    @Singleton
    fun providePasswordCheck() = CreatingPasswordCheck()

    /**
     * Providing data about fingerprint/passCode existence
     */
    @Provides
    @Singleton
    fun provideAccountSafetyStoring(preferenceRepository: PreferenceRepository) =
        AccountSafetyStoring(preferenceRepository)

    /**
     * Providing passCode functionality
     */
    @Provides
    @Singleton
    fun providePassCodeStoring(preferenceRepository: PreferenceRepository) =
        PassCodeStoring(preferenceRepository)

    /**
     * Providing backStack functionality
     */
    @Provides
    @Singleton
    fun provideBackStackHandler() = AuthBackStackHandler()

    @Provides
    @Singleton
    fun provideAESCrypt() = AESCrypt()

    @Provides
    @Singleton
    fun provideFirebaseAuth() = Firebase.auth

    @Provides
    @Singleton
    fun provideFirebaseMessaging() = FirebaseMessaging.getInstance()

    @Provides
    @Singleton
    fun provideFirebaseAuthHandler(
        firebaseAuth: FirebaseAuth,
        firebaseMessaging: FirebaseMessaging
    ) = FirebaseAuthHandler(firebaseAuth, firebaseMessaging)

    @Provides
    @Singleton
    fun provideNotificationHandler(
        pNotificationsRepository: NotificationsRepository
    ) = NotificationHandler(pNotificationsRepository)

    @Provides
    @Singleton
    fun provideTfaHandler(
        twoFaRepository: TfaRepository,
        processTokens: ProcessTokens
    ) = TfaHandler(twoFaRepository, processTokens)

    @Provides
    @Singleton
    fun provideQRCodeWriter(
    ) = QRCodeWriter()

    @Provides
    @Singleton
    fun provideQrGenerator(
        qrCodeWriter: QRCodeWriter
    ) = QrGenerator(qrCodeWriter)


}
