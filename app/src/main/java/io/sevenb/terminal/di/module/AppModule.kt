package io.sevenb.terminal.di.module

import android.content.Context
import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import io.sevenb.terminal.TerminalApplication
import io.sevenb.terminal.di.ViewModelKey
import io.sevenb.terminal.ui.screens.trading.SharedViewModel
import io.sevenb.terminal.utils.ConnectionStateMonitor

@Module(includes = [
    ViewModelFactoryModule::class,
    UseCaseModule::class,
    RepositoryModule::class,
    RetrofitApiModule::class,
    DbModule::class
])
class AppModule {

    @Provides
    fun provideContext(application: TerminalApplication): Context {
        return application.applicationContext
    }

    @Provides
    fun provideConnectivityManager(context: Context): ConnectionStateMonitor = ConnectionStateMonitor(context)
}
