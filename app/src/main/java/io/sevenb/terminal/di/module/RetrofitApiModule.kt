package io.sevenb.terminal.di.module

import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import io.sevenb.terminal.BuildConfig
import io.sevenb.terminal.data.datasource.retrofit.BrokerApi
import io.sevenb.terminal.data.datasource.retrofit.BrokerApi.Companion.PROD_BROKER_API_URL
import io.sevenb.terminal.data.datasource.retrofit.BrokerApi.Companion.STAGE_BROKER_API_URL
import io.sevenb.terminal.data.datasource.retrofit.BrokerApi.Companion.V1
import io.sevenb.terminal.data.datasource.retrofit.BrokerApi.Companion.V2
import io.sevenb.terminal.data.datasource.retrofit.ChangeNowApi
import io.sevenb.terminal.data.datasource.retrofit.ChangeNowApi.Companion.CHANGE_NOW_API_URL
import io.sevenb.terminal.data.datasource.retrofit.CryptocompareService
import io.sevenb.terminal.data.datasource.retrofit.TestCaptchaApi
import io.sevenb.terminal.data.datasource.retrofit.TestCaptchaApi.Companion.PROD_GEETEST_API_URL
import io.sevenb.terminal.data.datasource.retrofit.TestCaptchaApi.Companion.STAGE_GEETEST_API_URL
import io.sevenb.terminal.data.datasource.retrofit.interceptor.AccessTokenInterceptor
import io.sevenb.terminal.data.repository.trading.HistoryChartDataRepository.Companion.CRYPTOCOMPARE_API_URL
import io.sevenb.terminal.di.qualifiers.ApiV1
import io.sevenb.terminal.di.qualifiers.ApiV2
import io.sevenb.terminal.domain.usecase.auth.RestoreRefreshToken
import io.sevenb.terminal.domain.usecase.auth.StoreRefreshToken
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

/**
 * Dagger module to provide Retrofit related functionality
 */
@Module
class RetrofitApiModule {

    private fun getUrlV1() =
        if (BuildConfig.FLAVOR == "prod") PROD_BROKER_API_URL + V1 else STAGE_BROKER_API_URL + V1

   private fun getUrlV2() =
        if (BuildConfig.FLAVOR == "prod") PROD_BROKER_API_URL + V2 else STAGE_BROKER_API_URL + V2

    /**
     * APIs
     */
    @ApiV1
    @Singleton
    @Provides
    fun provideBrokerApiV1(
        @Named(OKHTTP_AUTH_INTERCEPTOR) okhttpClient: OkHttpClient,
        converterFactory: GsonConverterFactory
    ): BrokerApi {
        val brokerApiUrl = getUrlV1()
        return provideRetrofit(
            okhttpClient, converterFactory,
            brokerApiUrl, BrokerApi::class.java
        )
    }

    @ApiV2
    @Singleton
    @Provides
    fun provideBrokerApiV2(
        @Named(OKHTTP_AUTH_INTERCEPTOR) okhttpClient: OkHttpClient,
        converterFactory: GsonConverterFactory
    ): BrokerApi {
        val brokerApiUrl = getUrlV2()
        return provideRetrofit(
            okhttpClient, converterFactory,
            brokerApiUrl, BrokerApi::class.java
        )
    }

    @Singleton
    @Provides
    fun provideCryptocompareService(
        @Named(OKHTTP_DEFAULT_LOGGING) okhttpClient: OkHttpClient,
        converterFactory: GsonConverterFactory
    ): CryptocompareService {
        return provideRetrofit(
            okhttpClient, converterFactory,
            CRYPTOCOMPARE_API_URL, CryptocompareService::class.java
        )
    }

    @Singleton
    @Provides
    fun provideTestCaptchaApi(
        @Named(OKHTTP_DEFAULT_LOGGING) okhttpClient: OkHttpClient,
        converterFactory: GsonConverterFactory
    ): TestCaptchaApi {
        val brokerApiUrl =
            if (BuildConfig.FLAVOR == "prod") PROD_GEETEST_API_URL else STAGE_GEETEST_API_URL
        return provideRetrofit(
            okhttpClient, converterFactory,
            brokerApiUrl, TestCaptchaApi::class.java
        )
    }

    @Singleton
    @Provides
    fun provideChangeNowApi(
        @Named(OKHTTP_DEFAULT_LOGGING) okhttpClient: OkHttpClient,
        converterFactory: GsonConverterFactory
    ): ChangeNowApi {
        val changeNowApiUrl = CHANGE_NOW_API_URL
        return provideRetrofit(
            okhttpClient, converterFactory,
            changeNowApiUrl, ChangeNowApi::class.java
        )
    }

    /**
     * Okhttp client and interceptors
     */
    @Singleton
    @Provides
    fun provideAccessTokenInterceptor(
        storeRefreshToken: StoreRefreshToken,
        restoreRefreshToken: RestoreRefreshToken
    ): AccessTokenInterceptor = AccessTokenInterceptor(storeRefreshToken, restoreRefreshToken)

    @Named(OKHTTP_DEFAULT_LOGGING)
    @Provides
    fun provideLoggingOkHttpClient(
        loggingInterceptor: HttpLoggingInterceptor
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .build()
    }

    @Named(OKHTTP_AUTH_INTERCEPTOR)
    @Provides
    fun provideAuthOkHttpClient(
        accessTokenInterceptor: AccessTokenInterceptor,
        loggingInterceptor: HttpLoggingInterceptor,
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(accessTokenInterceptor)
            .addInterceptor(loggingInterceptor)
            .build()
    }

    @Provides
    fun provideLoggingInterceptor() =
        HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
//                if (BuildConfig.FLAVOR == "prod") HttpLoggingInterceptor.Level.NONE
//                else HttpLoggingInterceptor.Level.BODY
        }

    /**
     * Creating retrofit
     */
    private fun buildRetrofit(
        okhttpClient: OkHttpClient,
        converterFactory: GsonConverterFactory,
        baseUrl: String
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(okhttpClient)
            .addConverterFactory(converterFactory)
            .build()
    }

    /**
     * Providing Retrofit instance
     */
    private fun <T> provideRetrofit(
        okhttpClient: OkHttpClient,
        converterFactory: GsonConverterFactory,
        baseUrl: String,
        clazz: Class<T>
    ): T {
        return buildRetrofit(okhttpClient, converterFactory, baseUrl).create(clazz)
    }

    /**
     * Gson instance and its converter factory
     */
    @Provides
    @Singleton
    fun provideGson(): Gson = Gson()

    @Provides
    @Singleton
    fun provideGsonConverterFactory(gson: Gson): GsonConverterFactory =
        GsonConverterFactory.create(gson)

    companion object {
        private const val OKHTTP_DEFAULT_LOGGING = "OKHTTP_DEFAULT_LOGGING"
        private const val OKHTTP_AUTH_INTERCEPTOR = "OKHTTP_AUTH_INTERCEPTOR"
    }

}
