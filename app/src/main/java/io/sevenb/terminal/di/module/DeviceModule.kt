package io.sevenb.terminal.di.module

import android.content.Context
import dagger.Module
import dagger.Provides
import io.sevenb.terminal.data.datasource.PreferenceStorage
import io.sevenb.terminal.data.datasource.SharedPreferenceStorage
import io.sevenb.terminal.device.cryptography.CryptographyManager
import io.sevenb.terminal.device.provider.DeviceManagerProvider
import io.sevenb.terminal.device.provider.FileProvider
import javax.inject.Singleton

/**
 * Dagger module to provide core data functionality.
 */
@Module
class DeviceModule {

    @Provides
    @Singleton
    fun provideCryptographyManager(context: Context): CryptographyManager = CryptographyManager(context)

    @Provides
    @Singleton
    fun providesPreferenceStorage(context: Context): PreferenceStorage =
        SharedPreferenceStorage(context)

    @Provides
    @Singleton
    fun providesDeviceManagerProvider(context: Context): DeviceManagerProvider =
        DeviceManagerProvider(context)

    @Provides
    @Singleton
    fun providesFileProvider(context: Context): FileProvider =
        FileProvider(context)

}
