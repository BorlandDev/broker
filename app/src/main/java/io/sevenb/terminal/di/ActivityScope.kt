package io.sevenb.terminal.di

@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScope()
