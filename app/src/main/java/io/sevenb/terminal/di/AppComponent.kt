package io.sevenb.terminal.di

import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import io.sevenb.terminal.TerminalApplication
import io.sevenb.terminal.di.module.*
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        ViewModelFactoryModule::class,
        ActivityModule::class,
        DeviceModule::class,
        AppModule::class,
        BroadcastReceiverModule::class
    ]
)
interface AppComponent : AndroidInjector<TerminalApplication> {
    @Component.Factory
    interface Factory : AndroidInjector.Factory<TerminalApplication>
}
