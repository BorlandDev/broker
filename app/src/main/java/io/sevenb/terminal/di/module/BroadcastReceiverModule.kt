package io.sevenb.terminal.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import io.sevenb.terminal.domain.usecase.safety.BootUpReceiver

@Module
abstract class BroadcastReceiverModule {
    @ContributesAndroidInjector
    internal abstract fun contributeBroadCastReceiver(): BootUpReceiver
}