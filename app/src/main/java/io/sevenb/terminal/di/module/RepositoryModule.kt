package io.sevenb.terminal.di.module

import dagger.Module
import dagger.Provides
import io.sevenb.terminal.data.datasource.PreferenceStorage
import io.sevenb.terminal.data.datasource.database.dao.CurrencyDao
import io.sevenb.terminal.data.datasource.retrofit.BrokerApi
import io.sevenb.terminal.data.datasource.retrofit.ChangeNowApi
import io.sevenb.terminal.data.datasource.retrofit.CryptocompareService
import io.sevenb.terminal.data.repository.auth.*
import io.sevenb.terminal.data.repository.breakdown.BreakdownRepository
import io.sevenb.terminal.data.repository.database.DbRepository
import io.sevenb.terminal.data.repository.notification_repository.NotificationsRepository
import io.sevenb.terminal.data.repository.trading.CandlesDataRepository
import io.sevenb.terminal.data.repository.trading.HistoryChartDataRepository
import io.sevenb.terminal.data.repository.trading.OrderDataRepository
import io.sevenb.terminal.data.repository.trading.ResourceRepository
import io.sevenb.terminal.data.repository.transactionmask.TransactionExplorerMaskRepository
import io.sevenb.terminal.data.repository.two_fa.TfaRepository
import io.sevenb.terminal.device.provider.FileProvider
import io.sevenb.terminal.di.qualifiers.ApiV1
import io.sevenb.terminal.di.qualifiers.ApiV2
import io.sevenb.terminal.domain.repository.*
import io.sevenb.terminal.domain.usecase.auth.StorePassword
import io.sevenb.terminal.domain.usecase.auth.ValidateEmailPassword
import io.sevenb.terminal.domain.usecase.validate.CheckCredentials
import io.sevenb.terminal.domain.usecase.new_auth.HandleCaptcha
import io.sevenb.terminal.domain.usecase.new_auth.HandleFieldFocus
import io.sevenb.terminal.domain.usecase.new_auth.HandleFingerprint
import io.sevenb.terminal.domain.usecase.safety.AccountSafetyStoring
import io.sevenb.terminal.ui.screens.auth.login_state.LoginFlow
import io.sevenb.terminal.ui.screens.auth.password_resore_state.PasswordRestoringFlow
import io.sevenb.terminal.ui.screens.auth.registration_state.RegistrationFlow
import io.sevenb.terminal.ui.screens.lock_timeout.LockTimeoutModule
import io.sevenb.terminal.ui.screens.safety.passcode_state.PassCodeFlow
import io.sevenb.terminal.ui.screens.safety.tfa.TfaFlow
import javax.inject.Singleton

/**
 * Dagger module to provide core data functionality.
 */
@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun provideAuthApi(
        @ApiV1 brokerApi: BrokerApi,
        safetyStoring: AccountSafetyStoring
    ): AuthApi =
//        UserAuthRepository(brokerApi, safetyStoring)
        UserAuthRepository(brokerApi)

    @Provides
    @Singleton
    fun provideConfirmationApi(
        @ApiV1 brokerApi: BrokerApi
    ): ConfirmationApi =
        CaptchaConfirmationRepository(brokerApi)

    @Provides
    @Singleton
    fun provideAccountApi(
        @ApiV1 brokerApi: BrokerApi
    ): AccountApi =
        AccountRepository(brokerApi)

    @Provides
    @Singleton
    fun provideMarketApi(
        @ApiV1 brokerApi: BrokerApi
    ): MarketApi =
        TradingDataRepository(brokerApi)

    @Provides
    @Singleton
    fun provideTfaApi(
        @ApiV1 brokerApi: BrokerApi
    ): TfaApi =
        TfaRepository(brokerApi)

    @Provides
    @Singleton
    fun provideRatesApi(
        @ApiV1 brokerApi: BrokerApi
    ): RatesApi =
        RatesRepository(brokerApi)

    @Provides
    @Singleton
    fun provideCandlesApi(
        @ApiV1 brokerApi: BrokerApi
    ): CandlesApi = CandlesDataRepository(brokerApi)

    @Provides
    @Singleton
    fun provideOrdersApi(
        @ApiV1 brokerApi: BrokerApi
    ): OrdersApi =
        OrderDataRepository(brokerApi)

    @Provides
    @Singleton
    fun providePreferenceRepository(
        preferenceStorage: PreferenceStorage
    ): PreferenceRepository =
        PreferenceRepository(preferenceStorage)

    @Provides
    @Singleton
    fun provideHistoryChartDataRepository(
        cryptocompareService: CryptocompareService
    ): HistoryChartDataRepository =
        HistoryChartDataRepository(cryptocompareService)

    @Provides
    @Singleton
    fun provideRawDataRepository(
        fileProvider: FileProvider
    ): RawDataRepository =
        ResourceRepository(fileProvider)

    @Provides
    @Singleton
    fun provideBreakdownRepository() = BreakdownRepository()

    @Provides
    @Singleton
    fun provideTransactionExplorerMaskRepository(
        changeNowApi: ChangeNowApi
    ): TransactionExplorerMaskRepository =
        TransactionExplorerMaskRepository(changeNowApi)

    @Provides
    @Singleton
    fun provideDbRepository(
        currencyDao: CurrencyDao
    ) = DbRepository(currencyDao)

    @Provides
    @Singleton
    fun provideLoginFlow() = LoginFlow()

    @Provides
    @Singleton
    fun providePassCodeHandler() = PassCodeFlow()

    @Provides
    @Singleton
    fun provideRegistrationFlow() = RegistrationFlow()

    @Provides
    @Singleton
    fun provideTfaFlow() = TfaFlow()

//    @Provides
//    @Singleton
//    fun providePasswordChangeFlow() = ChangePasswordFragment()

    @Provides
    @Singleton
    fun providePasswordRestoringFlow() = PasswordRestoringFlow()

    @Provides
    @Singleton
    fun provideHandleCaptcha() = HandleCaptcha()

    @Provides
    @Singleton
    fun provideLockTimeoutModule(
        accountSafetyStoring: AccountSafetyStoring
    ) = LockTimeoutModule(accountSafetyStoring)

    @Provides
    @Singleton
    fun provideCheckCredentials(
        validateEmailPassword: ValidateEmailPassword
    ) = CheckCredentials(validateEmailPassword)

    @Provides
    @Singleton
    fun provideHandleFieldFocus() = HandleFieldFocus()

    @Provides
    @Singleton
    fun provideHandleFingerprint(
        storePassword: StorePassword
    ) = HandleFingerprint(storePassword)

    @Provides
    @Singleton
    fun provideNotificationRepository(
        @ApiV1 brokerApiV1: BrokerApi,
        @ApiV2 brokerApiV2: BrokerApi,
    ) = NotificationsRepository(brokerApiV1, brokerApiV2)

    @Provides
    @Singleton
    fun provideTwoFaRepository(
        @ApiV1 brokerApi: BrokerApi
    ) = TfaRepository(brokerApi)
}
