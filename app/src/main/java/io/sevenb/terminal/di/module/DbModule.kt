package io.sevenb.terminal.di.module

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import io.sevenb.terminal.data.datasource.database.db.AppDatabase
import javax.inject.Singleton

@Module
class DbModule {

    @Singleton
    @Provides
    fun provideCurrencyDatabase(
        context: Context
    ) = Room
        .databaseBuilder(context, AppDatabase::class.java, "currencies-database")
        .build()

    @Singleton
    @Provides
    fun provideCurrencyDao(appDatabase: AppDatabase) = appDatabase.currencyDao()
}