package io.sevenb.terminal.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import io.sevenb.terminal.AppActivity

@Module
abstract class ActivityModule {
    @ContributesAndroidInjector(modules = [FragmentBuilderModule::class])
    abstract fun appActivity(): AppActivity
}
