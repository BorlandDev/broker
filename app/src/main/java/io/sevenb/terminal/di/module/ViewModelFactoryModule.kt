package io.sevenb.terminal.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import io.sevenb.terminal.di.NowBrokerViewModelFactory
import io.sevenb.terminal.di.ViewModelKey
import io.sevenb.terminal.ui.delete_pass_code.DeletePassCodeViewModel
import io.sevenb.terminal.ui.screens.alert.viewModel.PercentViewModel
import io.sevenb.terminal.ui.screens.alert.viewModel.PriceViewModel
import io.sevenb.terminal.ui.screens.auth.SharedAuthViewModel
import io.sevenb.terminal.ui.screens.auth.login_state.LoginViewModel
import io.sevenb.terminal.ui.screens.auth.new_auth.NewAuthSharedViewModel
import io.sevenb.terminal.ui.screens.auth.password_resore_state.PasswordRestoringViewModel
import io.sevenb.terminal.ui.screens.auth.registration_state.RegistrationViewModel
import io.sevenb.terminal.ui.screens.auth.change_password.ChangePasswordViewModel
import io.sevenb.terminal.ui.screens.confirm_phone.ConfirmPhoneViewModel
import io.sevenb.terminal.ui.screens.create_fingerprint.CreateFingerprintViewModel
import io.sevenb.terminal.ui.screens.deposit.DepositViewModel
import io.sevenb.terminal.ui.screens.history_filter.HistoryFilterViewModel
import io.sevenb.terminal.ui.screens.lock_timeout.LockTimeoutViewModel
import io.sevenb.terminal.ui.screens.lock_timeout.SharedLockModel
import io.sevenb.terminal.ui.screens.notification_settings.NotificationSettingsViewModel
import io.sevenb.terminal.ui.screens.notification_settings.SharedNotificationsToSettingsViewModel
import io.sevenb.terminal.ui.screens.order.OrderViewModel
import io.sevenb.terminal.ui.screens.portfolio.PortfolioViewModel
import io.sevenb.terminal.ui.screens.safety.passcode_state.PassCodeViewModel
import io.sevenb.terminal.ui.screens.safety.SafetyViewModel
import io.sevenb.terminal.ui.screens.safety.tfa.TfaViewModel
import io.sevenb.terminal.ui.screens.secutiry.SecurityViewModel
import io.sevenb.terminal.ui.screens.select_currency.SelectCurrencyViewModel
import io.sevenb.terminal.ui.screens.settings.NotifySettingsViewModel
import io.sevenb.terminal.ui.screens.settings.SettingsViewModel
import io.sevenb.terminal.ui.screens.settings.SharedSettingToActivityViewModel
import io.sevenb.terminal.ui.screens.splash.SplashScreenViewModel
import io.sevenb.terminal.ui.screens.trading.AuthStateViewModel
import io.sevenb.terminal.ui.screens.trading.SharedViewModel
import io.sevenb.terminal.ui.screens.trading.TradingViewModel
import io.sevenb.terminal.ui.screens.withdraw.WithdrawViewModel
import javax.inject.Singleton


@Module
abstract class ViewModelFactoryModule {

//    @Binds
//    @IntoMap
//    @ViewModelKey(AuthViewModel::class)
//    abstract fun bindAuthViewModel(authViewModel: AuthViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TradingViewModel::class)
    abstract fun bindTradingViewModel(tradingViewModel: TradingViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PortfolioViewModel::class)
    abstract fun bindPortfolioViewModel(portfolioViewModel: PortfolioViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HistoryFilterViewModel::class)
    abstract fun bindHistoryFilterViewModel(historyFilterViewModel: HistoryFilterViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OrderViewModel::class)
    abstract fun bindOrderViewModel(orderViewModel: OrderViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DepositViewModel::class)
    abstract fun bindDepositViewModel(depositViewModel: DepositViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(WithdrawViewModel::class)
    abstract fun bindWithdrawViewModel(withdrawViewModel: WithdrawViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SharedViewModel::class)
    abstract fun bindSharedViewModel(sharedViewModel: SharedViewModel): ViewModel

    @Singleton
    @Binds
    @IntoMap
    @ViewModelKey(AuthStateViewModel::class)
    abstract fun bindAuthStateViewModel(authStateViewModel: AuthStateViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SettingsViewModel::class)
    abstract fun bindSettingsViewModel(settingsViewModel: SettingsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PercentViewModel::class)
    abstract fun bindPercentViewModel(percentViewModel: PercentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PriceViewModel::class)
    abstract fun bindPriceViewModel(percentViewModel: PriceViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CreateFingerprintViewModel::class)
    abstract fun bindCreateFingerprintViewModel(createFingerprintViewModel: CreateFingerprintViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ConfirmPhoneViewModel::class)
    abstract fun bindConfirmPhoneViewModel(confirmPhoneViewModel: ConfirmPhoneViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SelectCurrencyViewModel::class)
    abstract fun bindSelectCurrencyViewModel(selectCurrencyViewModel: SelectCurrencyViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ChangePasswordViewModel::class)
    abstract fun bindChangePasswordViewModel(changePasswordViewModel: ChangePasswordViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginViewModel(loginViewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NewAuthSharedViewModel::class)
    abstract fun bindNewAuthSharedViewModel(newAuthSharedViewModel: NewAuthSharedViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RegistrationViewModel::class)
    abstract fun bindRegistrationViewModel(registrationViewModel: RegistrationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PasswordRestoringViewModel::class)
    abstract fun bindPasswordRestoringViewModel(passwordRestoringViewModel: PasswordRestoringViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SafetyViewModel::class)
    abstract fun bindSafetyViewModel(safetyViewModel: SafetyViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SplashScreenViewModel::class)
    abstract fun bindSplashScreenViewModel(splashScreenViewModel: SplashScreenViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PassCodeViewModel::class)
    abstract fun bindPassCodeViewModel(passCodeViewModel: PassCodeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SharedAuthViewModel::class)
    abstract fun bindSharedAuthViewModel(sharedAuthViewModel: SharedAuthViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DeletePassCodeViewModel::class)
    abstract fun bindDeletePassCodeViewModel(deletePassCodeViewModel: DeletePassCodeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SharedSettingToActivityViewModel::class)
    abstract fun bindSharedSettingToActivityViewModel(sharedSettingsToActivityViewModel: SharedSettingToActivityViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NotifySettingsViewModel::class)
    abstract fun bindNotifySettingsViewModel(notifySettingsViewModel: NotifySettingsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SecurityViewModel::class)
    abstract fun bindSecurityViewModel(securityViewModel: SecurityViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LockTimeoutViewModel::class)
    abstract fun bindLockTimeoutViewModel(lockTimeoutViewModel: LockTimeoutViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SharedLockModel::class)
    abstract fun bindSharedLockViewModel(sharedLockModel: SharedLockModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NotificationSettingsViewModel::class)
    abstract fun bindNotificationSettingsViewModel(notificationsSettingsViewModel: NotificationSettingsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SharedNotificationsToSettingsViewModel::class)
    abstract fun bindNotificationToSettingsViewModel(notificationsToSettingsViewModel: SharedNotificationsToSettingsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TfaViewModel::class)
    abstract fun bindTfaViewModel(tfaViewModel: TfaViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: NowBrokerViewModelFactory): ViewModelProvider.Factory

}
