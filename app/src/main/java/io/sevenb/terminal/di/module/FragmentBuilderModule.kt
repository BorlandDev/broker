package io.sevenb.terminal.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import io.sevenb.terminal.ui.delete_pass_code.DeletePassCodeFragment
import io.sevenb.terminal.ui.screens.auth.new_auth.NewAuthFragment
import io.sevenb.terminal.ui.screens.auth.change_password.ChangePasswordFragment
import io.sevenb.terminal.ui.screens.confirm_phone.ConfirmPhoneFragment
import io.sevenb.terminal.ui.screens.create_fingerprint.CreateFingerprintFragment
import io.sevenb.terminal.ui.screens.deposit.DepositBottomSheetFragment
import io.sevenb.terminal.ui.screens.deposit.DepositViewPagerFragment
import io.sevenb.terminal.ui.screens.deposit.FiatFragment
import io.sevenb.terminal.ui.screens.history_filter.HistoryFilterBottomSheetFragment
import io.sevenb.terminal.ui.screens.lock_timeout.LockTimeoutFragment
import io.sevenb.terminal.ui.screens.notification_settings.NotificationsSettingsFragment
import io.sevenb.terminal.ui.screens.order.NewOrderBottomSheetFragment
import io.sevenb.terminal.ui.screens.order.OrderBottomSheetFragment
import io.sevenb.terminal.ui.screens.portfolio.PortfolioFragment
import io.sevenb.terminal.ui.screens.safety.SafetyFragment
import io.sevenb.terminal.ui.screens.secutiry.SecurityFragment
import io.sevenb.terminal.ui.screens.select_currency.SelectCurrencyDepositFragment
import io.sevenb.terminal.ui.screens.select_currency.SelectCurrencyFiatFragment
import io.sevenb.terminal.ui.screens.select_currency.SelectCurrencyFragment
import io.sevenb.terminal.ui.screens.settings.SettingsFragment
import io.sevenb.terminal.ui.screens.show_status.ShowStatusDepositeBottomSheetFragment
import io.sevenb.terminal.ui.screens.show_status.ShowStatusOrderBottomSheetFragment
import io.sevenb.terminal.ui.screens.show_status.ShowStatusWithdrawBottomSheetFragment
import io.sevenb.terminal.ui.screens.splash.SplashScreenFragment
import io.sevenb.terminal.ui.screens.support.SupportFragment
import io.sevenb.terminal.ui.screens.trading.TradingFragment
import io.sevenb.terminal.ui.screens.withdraw.WithdrawBottomSheetFragment
import io.sevenb.terminal.ui.screens.alert.fragment.PercentFragment
import io.sevenb.terminal.ui.screens.alert.fragment.PriceFragment
import io.sevenb.terminal.ui.screens.alert.fragment.PriceAlertFragment

@Module
abstract class FragmentBuilderModule {

    @ContributesAndroidInjector
    internal abstract fun contributeSplashScreenFragment(): SplashScreenFragment

    @ContributesAndroidInjector
    internal abstract fun contributeTradingFragment(): TradingFragment

    @ContributesAndroidInjector
    internal abstract fun contributeOrderBottomSheetFragment(): OrderBottomSheetFragment

    @ContributesAndroidInjector
    internal abstract fun contributePortfolioFragment(): PortfolioFragment

    @ContributesAndroidInjector
    internal abstract fun contributeDepositBottomSheetFragment(): DepositBottomSheetFragment

    @ContributesAndroidInjector
    internal abstract fun contributeFiatFragment(): FiatFragment

    @ContributesAndroidInjector
    internal abstract fun contributeWithdrawBottomSheetFragment(): WithdrawBottomSheetFragment

    @ContributesAndroidInjector
    internal abstract fun contributeHistoryFilterBottomSheetFragment(): HistoryFilterBottomSheetFragment

    @ContributesAndroidInjector
    internal abstract fun contributeSelectCurrencyFragment(): SelectCurrencyFragment

    @ContributesAndroidInjector
    internal abstract fun contributeSelectCurrencyFiatFragment(): SelectCurrencyFiatFragment

    @ContributesAndroidInjector
    internal abstract fun contributeSelectCurrencyDepositFragment(): SelectCurrencyDepositFragment

    @ContributesAndroidInjector
    internal abstract fun contributeSupportFragment(): SupportFragment

    @ContributesAndroidInjector
    internal abstract fun contributeSettingsFragment(): SettingsFragment

    @ContributesAndroidInjector
    internal abstract fun contributeCreateFingerprintFragment(): CreateFingerprintFragment

    @ContributesAndroidInjector
    internal abstract fun contributeConfirmPhoneFragment(): ConfirmPhoneFragment

    @ContributesAndroidInjector
    internal abstract fun contributeChangePasswordFragment(): ChangePasswordFragment

    @ContributesAndroidInjector
    internal abstract fun contributeNewOrderBottomSheetFragment(): NewOrderBottomSheetFragment

    @ContributesAndroidInjector
    internal abstract fun contributeShowStatusOrderSheetFragment(): ShowStatusOrderBottomSheetFragment

    @ContributesAndroidInjector
    internal abstract fun contributeShowStatusDepositSheetFragment(): ShowStatusDepositeBottomSheetFragment

    @ContributesAndroidInjector
    internal abstract fun contributeShowWithdrawOrderSheetFragment(): ShowStatusWithdrawBottomSheetFragment

    @ContributesAndroidInjector
    internal abstract fun contributeDepositViewPagerFragment(): DepositViewPagerFragment

    @ContributesAndroidInjector
    internal abstract fun contributeNewAuthFragment(): NewAuthFragment

    @ContributesAndroidInjector
    internal abstract fun contributeDeletePassCodeFragment(): DeletePassCodeFragment

    @ContributesAndroidInjector
    internal abstract fun contributeSecurityFragment(): SecurityFragment

    @ContributesAndroidInjector
    internal abstract fun contributeLockTimeoutFragment(): LockTimeoutFragment

    @ContributesAndroidInjector
    internal abstract fun contributeNotificationSettingsFragment(): NotificationsSettingsFragment

    @ContributesAndroidInjector
    internal abstract fun contributeSafetyFragment(): SafetyFragment

    @ContributesAndroidInjector
    internal abstract fun contributePriceAlertFragment(): PriceAlertFragment

    @ContributesAndroidInjector
    internal abstract fun contributePriceFragment(): PriceFragment

    @ContributesAndroidInjector
    internal abstract fun contributePercentFragment(): PercentFragment

//    @ContributesAndroidInjector
//    internal abstract fun contributeTfaFragment(): TfaFragment

}
