package io.sevenb.terminal.data.repository.auth

import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.enum_model.ErrorMessageType
import io.sevenb.terminal.domain.usecase.firebase_auth.FirebaseAuthHandler
import io.sevenb.terminal.exceptions.NewAuthException
import io.sevenb.terminal.utils.Logger
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject


class FirebaseAuthRepository @Inject constructor(
    private val firebaseAuthHandler: FirebaseAuthHandler,
) {

    suspend fun getMessageToken(
        email: String,
        password: String
    ): Result<String> {
        return withContext(Dispatchers.IO) {
            if (firebaseAuthHandler.isSignedIn(email)) {
                Logger.sendLog("FirebaseAuthRepository", "getMessageToken", "isSignedIn = true")
                return@withContext firebaseAuthHandler.requestToken()
            } else {
                Logger.sendLog("FirebaseAuthRepository", "getMessageToken", "isSignedIn = false")
                logoutFirebase()

                val newEmailResult = firebaseAuthHandler.isNewEmail(email)
                if (newEmailResult is Result.Success && newEmailResult.data) {
                    Logger.sendLog("FirebaseAuthRepository", "getMessageToken", "isNewEmail = true")
                    firebaseAuthHandler.register(email, password)
                }

                val loginResult = firebaseAuthHandler.login(email, password)

                if (loginResult is Result.Success && loginResult.data) {
                    Logger.sendLog("FirebaseAuthRepository", "getMessageToken", "login = true")
                    return@withContext firebaseAuthHandler.requestToken()
                }

                val exception = NewAuthException(ErrorMessageType.FIREBASE_TOKEN)
                Logger.sendLog("FirebaseAuthRepository", "getMessageToken", exception)
                return@withContext Result.Error(exception)
            }
        }
    }

    suspend fun logoutFirebase() = withContext(Dispatchers.IO) {
        Logger.sendLog("FirebaseAuthRepository", "logoutFirebase")
        firebaseAuthHandler.logout()
    }
}


