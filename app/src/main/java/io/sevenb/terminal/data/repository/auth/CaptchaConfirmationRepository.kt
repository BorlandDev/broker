package io.sevenb.terminal.data.repository.auth

import io.sevenb.terminal.data.datasource.retrofit.BrokerApi
import io.sevenb.terminal.data.model.broker_api.CaptchaGet
import io.sevenb.terminal.data.model.broker_api.CaptchaPost
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.repository.BaseRepository
import io.sevenb.terminal.di.qualifiers.ApiV1
import io.sevenb.terminal.domain.repository.ConfirmationApi
import javax.inject.Inject


class CaptchaConfirmationRepository @Inject constructor(
    @ApiV1 private val brokerApi: BrokerApi
) : BaseRepository(), ConfirmationApi {

    /**
     * Captcha data API1 and API2
     */
    override suspend fun getCaptchaData(time: String): Result<CaptchaGet> {
        return safeApiCall({ brokerApi.captchaGet() })
    }

    override suspend fun postCaptchaData(
        validate: String,
        secCode: String,
        challenge: String
    ): Result<CaptchaPost> {
        return safeApiCall({
            brokerApi.captchaPost(
                validate, secCode, challenge
            )
        })
    }

}