package io.sevenb.terminal.data.model.broker_api.account

data class AvailableWithdrawal (
    val remainingBtc: Float
)