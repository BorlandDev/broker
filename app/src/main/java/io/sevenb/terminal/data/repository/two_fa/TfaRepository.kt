package io.sevenb.terminal.data.repository.two_fa

import io.sevenb.terminal.data.datasource.retrofit.BrokerApi
import io.sevenb.terminal.data.model.auth.ResponseBoolean
import io.sevenb.terminal.data.model.broker_api.TokensResponse
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.tfa.TfaResponse
import io.sevenb.terminal.data.model.tfa.TfaVerification
import io.sevenb.terminal.data.repository.BaseRepository
import io.sevenb.terminal.di.qualifiers.ApiV1
import io.sevenb.terminal.domain.repository.TfaApi
import javax.inject.Inject


class TfaRepository @Inject constructor(
   @ApiV1 private val brokerApi: BrokerApi
) : BaseRepository(), TfaApi {

    override suspend fun twoFaIsEnabled(): Result<ResponseBoolean> {
        return Result.Success(ResponseBoolean(false))//safeApiCall({  })
    }

    override suspend fun toggleTfa(): Result<TfaResponse> {
        return safeApiCall({ brokerApi.toggleTfa() })
    }

    override suspend fun verify(tfaVerification: TfaVerification): Result<TokensResponse> {
        return safeApiCall({ brokerApi.verify(tfaVerification) })
    }

}