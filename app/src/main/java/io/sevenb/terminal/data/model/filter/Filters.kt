package io.sevenb.terminal.data.model.filter

import io.sevenb.terminal.data.model.enum_model.BinanceFilter

data class Filters(
    val filterType: BinanceFilter?,
    val minPrice: String?,
    val maxPrice: String?,
    val tickSize: String?,
    val multiplierUp: String?,
    val multiplierDown: String?,
    val avgPriceMins: Int?,
    val minQty: String?,
    val maxQty: String?,
    val stepSize: String?,
    val minNotional: String?,
    val applyToMarket: Boolean?,
    val limit: Int?,
    val maxNumOrders: Int?,
    val maxNumAlgoOrders: Int?
)