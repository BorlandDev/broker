package io.sevenb.terminal.data.repository

import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.utils.Logger
import retrofit2.Response
import java.io.IOException

abstract class BaseRepository {

    suspend fun <T : Any> safeApiCall(
        call: suspend () -> Response<T>,
        errorMessage: String = ""
    ): Result<T> {
        return safeApiResult(call, errorMessage)
    }

    private suspend fun <T : Any> safeApiResult(
        call: suspend () -> Response<T>,
        errorMessage: String
    ): Result<T> {
        return try {
            val response = call.invoke()
            val code = response.code()
            val url = response.raw().request.url

            if (response.isSuccessful) {
                val body = response.body()!!
//                Logger.sendLog(
//                    "BaseRepository",
//                    "safeApiResult",
//                    "url = $url code = $code"
//                )
                Result.Success(body)
            } else {
                val body = response.errorBody()
//                Logger.sendLog(
//                    "BaseRepository",
//                    "safeApiResult",
//                    "url = $url code = $code",
//                    body?.toString()
//                )
//                val s = body?.string()
                Result.Error(IOException(body?.string() ?: "Unknown error"), body?.string() ?: "")
            }
        } catch (e: IOException) {
            Logger.sendLog("BaseRepository", "safeApiResult", e)
            Result.Error(IOException("Error IOException - ${e.message}"))
        }
    }

}
