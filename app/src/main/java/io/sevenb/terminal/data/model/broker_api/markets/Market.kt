package io.sevenb.terminal.data.model.broker_api.markets

data class Market(
    val symbol: String,
    val status: String,
    val baseAsset: String,
    val baseAssetPrecision: Float,
    val quoteAsset: String,
    val quotePrecision: Float,
    val quoteAssetPrecision: Float,
    val baseCommissionPrecision: Float,
    val quoteCommissionPrecision: Float,
    val spot: Boolean,
    val stepSize: String
)
