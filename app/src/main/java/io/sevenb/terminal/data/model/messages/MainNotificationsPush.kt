package io.sevenb.terminal.data.model.messages

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MainNotificationsPush(
    var id: Int,
    var user_id: String,
    var is_enabled: Boolean,
    var template: TemplateResponse,
    var text_type: TextTypesResponse
) : Parcelable
