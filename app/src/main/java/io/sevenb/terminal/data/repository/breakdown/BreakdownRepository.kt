package io.sevenb.terminal.data.repository.breakdown

class BreakdownRepository {

    private var savedCurrencies: MutableMap<String, String>? = null

    fun initSavedCurrencies(savedCurrencies: MutableMap<String, String>) {
        this.savedCurrencies = savedCurrencies
    }

    fun breakdown(
        symbol: String
    ): Pair<String?, String?> {
        var firstTicker = ""
        var secondTicker = ""
        var firstItem: String? = null
        var secondItem: String? = null
        var updateFirst = false

        loop@ for (char in symbol)
            if (firstItem == null || updateFirst) {
                firstTicker += char
                firstItem =
                    savedCurrencies?.get(firstTicker)?.apply { updateFirst = false }
            } else {
                secondTicker = symbol.split(firstTicker)[1]
                secondItem = savedCurrencies?.get(secondTicker)

                updateFirst = if (secondItem == null) {
                    true
                } else {
                    if (firstTicker + secondTicker == symbol)
                        break@loop
                    else
                        true
                }
            }

        return Pair(firstTicker, secondTicker)
    }

    fun deleteSavedCurrencies() {
        savedCurrencies = null
    }
}