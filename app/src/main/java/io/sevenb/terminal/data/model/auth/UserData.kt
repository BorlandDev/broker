package io.sevenb.terminal.data.model.auth

data class UserData (
    var email: String = "",
    var password: String = "",
    var captcha: UserCaptcha? = null
)

data class UserCaptcha (
    var geetest_challenge: String = "",
    var geetest_seccode: String = "",
    var geetest_validate: String = ""
)