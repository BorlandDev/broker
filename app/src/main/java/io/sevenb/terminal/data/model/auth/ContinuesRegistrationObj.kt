package io.sevenb.terminal.data.model.auth

data class ContinuesRegistrationObj(
    val email: String,
    val token: String,
    val password: String
)