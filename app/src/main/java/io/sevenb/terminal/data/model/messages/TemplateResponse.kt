package io.sevenb.terminal.data.model.messages

import android.os.Parcelable
import io.sevenb.terminal.data.model.enums.notifications.NotificationsTypeFromBack
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TemplateResponse(
    var id: Int,
    var template_type: NotificationsTypeFromBack,
    val is_enabled: Boolean
) : Parcelable
