package io.sevenb.terminal.data.model.tfa

data class TfaVerification(
    var email: String,
    var code: String,
)