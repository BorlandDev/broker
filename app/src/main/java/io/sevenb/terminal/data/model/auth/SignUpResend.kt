package io.sevenb.terminal.data.model.auth

data class SignUpResend (
    var token: String = "",
    var email: String = ""
)