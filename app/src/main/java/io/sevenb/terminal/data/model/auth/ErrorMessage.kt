package io.sevenb.terminal.data.model.auth

import io.sevenb.terminal.data.model.broker_api.RequestTokenResponse

data class ErrorMessage (
    var message: String = "",
    var error: String = "",
    var extra: RequestTokenResponse?
)

data class ErrorUnconfirmed(
    var message: String = "",
    var error: String = "",
    var extra: Token?
)

data class Token(
    var accessToken: String = ""
)