package io.sevenb.terminal.data.model.enums.notifications

enum class NotificationsType {
    WITHDRAW, DEPOSIT, ORDER
}