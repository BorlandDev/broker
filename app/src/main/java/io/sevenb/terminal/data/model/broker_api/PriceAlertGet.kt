package io.sevenb.terminal.data.model.broker_api


import com.google.gson.annotations.SerializedName

data class PriceAlertGet(
    @SerializedName("base_asset")
    val baseAsset: String,
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("fiat_currency")
    val fiatCurrency: String,
    @SerializedName("goal_type")
    val goalType: String,
    @SerializedName("goal_value")
    val goalValue: String,
    @SerializedName("goal_change")
    val goalChange: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("initial_price")
    val initialPrice: String,
    @SerializedName("notify_by_email")
    val notifyByEmail: Boolean,
    @SerializedName("notify_by_push")
    val notifyByPush: Boolean,
    @SerializedName("quote_asset")
    val quoteAsset: String,
    @SerializedName("side")
    val side: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("symbol")
    val symbol: String,
    @SerializedName("updated_at")
    val updatedAt: String,
    @SerializedName("user_id")
    val userId: String
)