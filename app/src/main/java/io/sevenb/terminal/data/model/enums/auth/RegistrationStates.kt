package io.sevenb.terminal.data.model.enums.auth

import io.sevenb.terminal.data.model.enums.CommonState

enum class RegistrationStates : CommonState {
    REGISTRATION_DATA,
    REGISTRATION_VERIFICATION_CODE
}