package io.sevenb.terminal.data.model.broker_api.account

data class DepositHistoryItem(
    val amount: String,
    val ticker: String,
    val network: String,
    val status: String,
    val address: String,
    val extraId: String?,
    val txId: String,
    val createdAt: String,
    val confirmations: String,
    val confirmationsRequired: String
)
