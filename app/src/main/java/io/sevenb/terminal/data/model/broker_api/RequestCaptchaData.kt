package io.sevenb.terminal.data.model.broker_api

data class RequestCaptchaData (
    val status: String,
    val info: String
)