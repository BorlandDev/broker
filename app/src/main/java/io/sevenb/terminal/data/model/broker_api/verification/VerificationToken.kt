package io.sevenb.terminal.data.model.broker_api.verification

data class VerificationToken (
    val token: String,
    val validUntil: String
)