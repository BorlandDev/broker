package io.sevenb.terminal.data.model.enums.pass_code

import io.sevenb.terminal.data.model.enums.CommonState

enum class PassCodeStates : CommonState {
    CREATE_PASS_CODE,
    CONFIRM_PASS_CODE,
    ENTER_PASS_CODE,
    ENTER_PASS_CODE_BOTH_METHODS,
}