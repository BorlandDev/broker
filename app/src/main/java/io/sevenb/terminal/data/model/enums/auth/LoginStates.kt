package io.sevenb.terminal.data.model.enums.auth

import io.sevenb.terminal.data.model.enums.CommonState

enum class LoginStates : CommonState {
    LOGIN_DATA,
    LOGIN_TFA,
    LOGIN_TFA_CREATE,
}