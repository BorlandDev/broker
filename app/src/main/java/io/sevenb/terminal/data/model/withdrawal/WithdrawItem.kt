package io.sevenb.terminal.data.model.withdrawal

import android.view.View
import coil.load
import io.sevenb.terminal.R
import io.sevenb.terminal.data.model.trading.TradeChartRange
import io.sevenb.terminal.ui.screens.select_currency.BaseSelectItem
import io.sevenb.terminal.ui.screens.trading.TradingWalletItem.Companion.CMC_ICON_URL
import kotlinx.android.synthetic.main.item_select_list.view.*
import kotlinx.android.synthetic.main.item_trading_list_main.view.img_icon
import kotlinx.android.synthetic.main.item_trading_list_main.view.tv_amount
import kotlinx.android.synthetic.main.item_trading_list_main.view.tv_amount_ticker
import kotlinx.android.synthetic.main.item_trading_list_main.view.tv_name
import kotlinx.android.synthetic.main.item_trading_list_main.view.tv_ticker
import kotlinx.coroutines.channels.Channel

data class WithdrawItem(
    override var ticker: String = "",
    var name: String,
    override var amount: String?,
    override var estimated: String,
    override var network: String? = "",
    val withdrawFee: Float,
    val withdrawMin: Float,
    val hasMemo: Boolean,
    val cmcIconId: Int = 0,
    override var isExpanded: Boolean?,
    override var updateExpandedStateChannel: Channel<Pair<String, Boolean>>?,
    override var currentRange: TradeChartRange?
) : BaseSelectItem {

    /**
     * Bind method by common search list adapter
     * @see io.sevenb.terminal.ui.screens.select_currency.SelectCurrencyAdapter
     */
    override fun bind(
        itemView: View,
        baseCurrency: String,
        itemSelected: (BaseSelectItem) -> Unit
    ) {
        if (cmcIconId == 7686) {
            itemView.img_icon.load(R.drawable.ic_e_cash_logo) {
                placeholder(R.drawable.ic_coin_placeholder)
                error(R.drawable.ic_coin_placeholder)
            }
        } else itemView.img_icon.load(CMC_ICON_URL.format(cmcIconId)) {
            placeholder(R.drawable.ic_coin_placeholder)
            error(R.drawable.ic_coin_placeholder)
        }

        ticker = if (ticker == "BCHA") "XEC" else ticker
        name = if (name == "Bitcoin Cash ABC") "eCash" else name
        network = if (network == "BCHA") "XEC" else network

        itemView.tv_ticker.text = ticker
        itemView.tv_name.text = when {
            ticker == network -> name
            network?.isEmpty() == true -> "%s".format(name)
            else -> "%s (%s %s)".format(name, network, "network")
        }
        itemView.tv_amount.text = amount
        itemView.tv_amount_ticker.text = ticker
        itemView.tv_estimated.text = "%s %s".format(estimated, baseCurrency)
        itemView.setOnClickListener { itemSelected(this) }
    }

}