package io.sevenb.terminal.data.model.enums

enum class OrderFieldPosition {
    FROM_FIRST,
    FROM_LAST
}