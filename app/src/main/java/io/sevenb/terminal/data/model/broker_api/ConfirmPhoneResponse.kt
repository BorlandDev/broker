package io.sevenb.terminal.data.model.broker_api

data class ConfirmPhoneResponse (
    var token: String = ""
)