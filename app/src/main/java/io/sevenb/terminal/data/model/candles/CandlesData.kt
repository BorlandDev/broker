package io.sevenb.terminal.data.model.candles

data class CandlesData(
    val fields: List<String>,
    val data: List<List<Any>>
)