package io.sevenb.terminal.data.model.order

data class OrderParams (
    val symbol: String,
    val side: String,
    val type: String,
    val timeLimitType: String? = null,
    var amount: String? = null,
    var quoteAmount: String? = null,
    val price: String? = null,
    val stopPrice: String? = null,
    val externalOrderId: String ? = null
)