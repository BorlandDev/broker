package io.sevenb.terminal.data.model.settings

import android.view.View
import androidx.core.content.ContextCompat
import io.sevenb.terminal.R
import io.sevenb.terminal.data.model.trading.TradeChartRange
import io.sevenb.terminal.ui.extension.load
import io.sevenb.terminal.ui.screens.select_currency.BaseSelectItem
import io.sevenb.terminal.ui.screens.trading.TradingWalletItem.Companion.CMC_ICON_URL
import kotlinx.android.synthetic.main.item_select_list.view.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ConflatedBroadcastChannel

data class DefaultCurrencyItem(
    override var ticker: String = "",
    val name: String,
    val cmcIconId: Int = 0,
    override var network: String? = "",
    override var isExpanded: Boolean?,
    override var updateExpandedStateChannel: Channel<Pair<String, Boolean>>?,
    override var currentRange: TradeChartRange?,
    override var estimated: String = "",
    override var amount: String? = ""
) : BaseSelectItem {

    /**
     * Bind method by common search list adapter
     * @see io.sevenb.terminal.ui.screens.select_currency.SelectCurrencyAdapter
     */
    override fun bind(
        itemView: View,
        baseCurrency: String,
        itemSelected: (BaseSelectItem) -> Unit
    ) {
        val url: Any? = if (ticker == tickerUSD) {
            ContextCompat.getDrawable(itemView.context, R.drawable.ic_dollar_symbol)
        } else CMC_ICON_URL.format(cmcIconId)
        itemView.img_icon.load(url) {
            placeholder(R.drawable.ic_coin_placeholder)
            error(R.drawable.ic_coin_placeholder)
        }
        itemView.tv_ticker.text = "%s".format(ticker)
        itemView.tv_name.text = name
    }

    companion object {
        private const val tickerUSD = "USD"
    }
}