package io.sevenb.terminal.data.model.order

import io.sevenb.terminal.data.model.enums.OrderFieldPosition

data class ReceivedRate (
    var rate: Float,
    val firstToDefault: Float,
    val lastToDefault: Float,
    val fromField: OrderFieldPosition
)