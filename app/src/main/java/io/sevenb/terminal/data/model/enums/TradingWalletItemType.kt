package io.sevenb.terminal.data.model.enums

enum class TradingWalletItemType {
    CHART, CANDLE, NONE
}