package io.sevenb.terminal.data.model

data class ProfitLoss(
    private val tags: String,
    private val description: String,
    private val security: Array<String>
)