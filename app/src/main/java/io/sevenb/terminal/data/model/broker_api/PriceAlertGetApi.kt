package io.sevenb.terminal.data.model.broker_api


import com.google.gson.annotations.SerializedName

data class PriceAlertGetApi(
    @SerializedName("response")
    val responseList: List<PriceAlertGet>
)