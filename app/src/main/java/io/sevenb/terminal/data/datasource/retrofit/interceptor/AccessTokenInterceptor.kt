package io.sevenb.terminal.data.datasource.retrofit.interceptor

import androidx.lifecycle.LiveData
import com.google.gson.Gson
import io.sevenb.terminal.BuildConfig
import io.sevenb.terminal.data.datasource.retrofit.BrokerApi
import io.sevenb.terminal.data.model.broker_api.RefreshTokenRequest
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.repository.auth.UserAuthRepository
import io.sevenb.terminal.domain.usecase.auth.RestoreRefreshToken
import io.sevenb.terminal.domain.usecase.auth.StoreRefreshToken
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.io.IOException
import java.net.HttpURLConnection.HTTP_UNAUTHORIZED
import javax.inject.Inject

class AccessTokenInterceptor @Inject constructor(
    private val storeStoreRefreshToken: StoreRefreshToken,
    private val restoreRefreshToken: RestoreRefreshToken
) : LiveData<Boolean?>(), Interceptor {

    private val brokerApi: BrokerApi

    init {
        val loggingInterceptor = HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
//                if (BuildConfig.FLAVOR == "prod") HttpLoggingInterceptor.Level.NONE
//                else HttpLoggingInterceptor.Level.BODY
        }

        val okhttpClient = OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .build()

        val converterFactory = GsonConverterFactory.create(Gson())

        val brokerApiUrl =
            if (BuildConfig.FLAVOR == "prod")
                BrokerApi.PROD_BROKER_API_URL + BrokerApi.V1
            else
                BrokerApi.STAGE_BROKER_API_URL + BrokerApi.V1

        brokerApi = provideRetrofit(
            okhttpClient, converterFactory,
            brokerApiUrl, BrokerApi::class.java
        )
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val accessToken: String = UserAuthRepository.accessToken
        if (accessToken.isEmpty()) {
            Timber.e("accessToken.isEmpty()")
        }
        val originalRequest = chain.request()
        val requestWithHeader: Request = newRequestWithAccessToken(originalRequest, accessToken)
        val response = chain.proceed(requestWithHeader)

        return if (response.code == HTTP_UNAUTHORIZED) {
            return synchronized(this) {
                val newAccessToken: String = UserAuthRepository.accessToken
                // Access token is refreshed in another thread.
//                if (newAccessToken.isNotEmpty())
//                    response.body?.close()
                if (accessToken != newAccessToken) {
                    response.body?.close()
                    return chain.proceed(newRequestWithAccessToken(originalRequest, newAccessToken))
                } else {
                    // Need to refresh an access token
                    Timber.d("intercept before runBlocking thread=${Thread.currentThread()}")
                    return@synchronized runBlocking {
                        return@runBlocking withContext(Dispatchers.IO) {
                            return@withContext withRefreshedToken(
                                chain, originalRequest, response
                            )
                        }
                    }
                }
            } ?: response
        } else {
            response
        }
    }

    private suspend fun withRefreshedToken(
        chain: Interceptor.Chain,
        request: Request,
        response: Response
    ): Response? {
        Timber.d("intercept after runBlocking thread=${Thread.currentThread()}")
        return if (UserAuthRepository.refreshToken.isEmpty()) {
            return when (val result = restoreRefreshToken.execute()) {
                is Result.Success -> {
                    val refreshToken = result.data
                    if (refreshToken.isNotEmpty()) {
                        try {
                            updateAccessToken(chain, request, refreshToken, response)
                        } catch (e: Exception) {
                            e.printStackTrace()
                            null
                        }
                    } else {
                        null
                    }
                }
                else -> {
                    Timber.e("withRefreshedToken restoreRefreshToken.execute() error")
                    null
                }
            }
        } else {
            try {
                updateAccessToken(chain, request, UserAuthRepository.refreshToken, response)
            } catch (e: Exception) {
                e.printStackTrace()
                null
            }
        }
    }

    private suspend fun updateAccessToken(
        chain: Interceptor.Chain,
        request: Request,
        refreshToken: String,
        response: Response
    ): Response? {
        val refreshTokenRequest = RefreshTokenRequest(refreshToken)

        return try {
            when (val updatedAccessToken =
                safeApiCall({ brokerApi.authRefresh(refreshTokenRequest) })) {
                is Result.Success -> {
                    response.body?.close()
                    UserAuthRepository.accessToken = updatedAccessToken.data.accessToken
                    UserAuthRepository.refreshToken = updatedAccessToken.data.refreshToken
                    storeStoreRefreshToken.execute(UserAuthRepository.refreshToken)

                    try {
                        this.postValue(true)
                        chain.proceed(
                            newRequestWithAccessToken(
                                request,
                                UserAuthRepository.accessToken
                            )
                        )
                    } catch (e: Exception) {
                        null
                    }
                }
                is Result.Error -> {
                    this.postValue(false)
                    try {
                        Timber.e("authApi.refreshAccessToken() error=${updatedAccessToken.exception.message}")
                        null
                    } catch (e: Exception) {
                        null
                    }
                }
            }
        } catch (e: Exception) {
            null
        }
    }

    private fun newRequestWithAccessToken(request: Request, accessToken: String): Request {
        return request.newBuilder()
            .addHeader("Authorization", "Bearer $accessToken")
            .build()
    }

    private fun buildRetrofit(
        okhttpClient: OkHttpClient,
        converterFactory: GsonConverterFactory,
        baseUrl: String
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(okhttpClient)
            .addConverterFactory(converterFactory)
            .build()
    }

    /**
     * Providing Retrofit instance
     */
    private fun <T> provideRetrofit(
        okhttpClient: OkHttpClient,
        converterFactory: GsonConverterFactory,
        baseUrl: String,
        clazz: Class<T>
    ): T {
        return buildRetrofit(okhttpClient, converterFactory, baseUrl).create(clazz)
    }

    suspend fun <T : Any> safeApiCall(
        call: suspend () -> retrofit2.Response<T>,
        errorMessage: String = ""
    ): Result<T> {
        return safeApiResult(call, errorMessage)
    }

    private suspend fun <T : Any> safeApiResult(
        call: suspend () -> retrofit2.Response<T>,
        errorMessage: String
    ): Result<T> {
        try {
            val response = call.invoke()
            if (response.isSuccessful) {
                return Result.Success(response.body()!!)
            }
        } catch (e: IOException) {
            return Result.Error(IOException("Error Occurred during getting safe Api result, Custom ERROR - $errorMessage"))
        }

        return Result.Error(IOException("Error Occurred during getting safe Api result, Custom ERROR - $errorMessage"))
    }

}