package io.sevenb.terminal.data.model.auth

data class PasswordCheckResponse(
    val lengthPasses: Boolean = false,
    val numbersPasses: Boolean = false,
    val lowercasePasses: Boolean = false,
    val uppercasePasses: Boolean = false
)