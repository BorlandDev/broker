package io.sevenb.terminal.data.model.enum_model

enum class HistoryType {
    DEPOSIT, WITHDRAW, ORDERS, CANCELLED_ORDER
}