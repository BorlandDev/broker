package io.sevenb.terminal.data.model.broker_api

data class CaptchaGet (
    val success: Int,
    val challenge: String,
    val gt: String,
    val new_captcha: Boolean
)