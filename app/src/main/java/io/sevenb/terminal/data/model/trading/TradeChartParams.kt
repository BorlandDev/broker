package io.sevenb.terminal.data.model.trading

data class TradeChartParams (
    val listPosition: Int,
    val tradeChartRange: TradeChartRange
) : BaseTrade()

open class BaseTrade {
    var ticker: String = ""
}

enum class TradeChartRange {
    DAY, WEEK, MONTH, THREE_MONTH, YEAR, ALL
}