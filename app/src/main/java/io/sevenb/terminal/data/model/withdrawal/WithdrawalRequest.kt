package io.sevenb.terminal.data.model.withdrawal

data class WithdrawalRequest(
    val requestId: String? = null,
    val ticker: String? = null,
    val network: String? = null,
    val address: String? = null,
    val extraId: String? = null,
    val amount: Float? = null,
    val txId: String? = null,
    val status: String? = null,
    val createdAt: String? = null,
    val withdrawalId: String? = null
)