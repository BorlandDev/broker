package io.sevenb.terminal.data.model.enums.auth

import io.sevenb.terminal.data.model.enums.CommonState

enum class PasswordRestoringStates : CommonState {
    PASSWORD_RESTORING_DATA,
    PASSWORD_RESTORING_VERIFICATION_CODE,
    PASSWORD_RESTORING_NEW_PASSWORD
}