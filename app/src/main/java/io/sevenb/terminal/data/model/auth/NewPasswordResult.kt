package io.sevenb.terminal.data.model.auth

data class NewPasswordResult (
    val status: Boolean
)