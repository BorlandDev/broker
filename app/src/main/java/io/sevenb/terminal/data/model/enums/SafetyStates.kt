package io.sevenb.terminal.data.model.enums

enum class SafetyStates : CommonState {
    NOT_LOCKED,
    PASS_CODE_AVAILABLE,
    FINGERPRINT_AVAILABLE,
    BOTH_VARIANTS_AVAILABLE,
    LOGGED_OUT
}