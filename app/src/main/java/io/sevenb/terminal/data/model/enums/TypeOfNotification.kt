package io.sevenb.terminal.data.model.enums

enum class TypeOfNotification {
    NOTIFICATION,
    DATA
}