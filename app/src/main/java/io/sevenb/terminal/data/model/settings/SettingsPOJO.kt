package io.sevenb.terminal.data.model.settings

import android.os.Parcelable
import io.sevenb.terminal.data.model.broker_api.account.UserSettings
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SettingsPOJO(
    var result: Boolean,
    var response: UserSettings?,
) : Parcelable
