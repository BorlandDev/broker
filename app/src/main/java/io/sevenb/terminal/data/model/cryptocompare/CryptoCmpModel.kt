package io.sevenb.terminal.data.model.cryptocompare

import com.google.gson.annotations.SerializedName

data class CryptoCmpModel(
    @SerializedName("time")
    val time: Long,
    @SerializedName("close")
    val close: Float
)