package io.sevenb.terminal.data.model.broker_api

data class RefreshTokenRequest (
    var refreshToken: String = ""
)