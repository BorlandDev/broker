package io.sevenb.terminal.data.model.broker_api

import com.google.gson.annotations.SerializedName
import io.sevenb.terminal.data.model.auth.Token
import io.sevenb.terminal.data.model.broker_api.account.UserSettings

data class TokensResponse(

    //TODO REMADE FOR BASE RESPONSE
    val error: String?,
    val message: String?,
    var extra: Token?,

    val token: Boolean?,
    val accessToken: String,
    val refreshToken: String,
    val access: String?,//need backend change
    val refresh: String?,//need backend change
    @SerializedName("user_settings") val userSettings: UserSettings?
)