package io.sevenb.terminal.data.repository.auth

import io.sevenb.terminal.data.datasource.retrofit.BrokerApi
import io.sevenb.terminal.data.model.auth.*
import io.sevenb.terminal.data.model.broker_api.ConfirmPhoneResponse
import io.sevenb.terminal.data.model.broker_api.RefreshTokenRequest
import io.sevenb.terminal.data.model.broker_api.RequestTokenResponse
import io.sevenb.terminal.data.model.broker_api.TokensResponse
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.repository.BaseRepository
import io.sevenb.terminal.di.qualifiers.ApiV1
import io.sevenb.terminal.domain.repository.AuthApi
import javax.inject.Inject


class UserAuthRepository @Inject constructor(
    @ApiV1 private val brokerApi: BrokerApi
//    private val safetyStoring: AccountSafetyStoring
) : BaseRepository(), AuthApi {

    companion object {
        var accessToken: String = ""
        var refreshToken: String = ""
    }

    /**
     * User auth / registration flow
     */
    override suspend fun requestVerificationCodeRegistration(
        userData: UserData
    ): Result<RequestTokenResponse> {
        return safeApiCall({ brokerApi.requestVerificationCodeRegistration(userData) })
    }

    /**
     * User auth / restore password flow
     */
    override suspend fun requestVerificationCodeRestorePassword(
        login: String
    ): Result<RequestTokenResponse> {
        return safeApiCall({ brokerApi.requestVerificationCodeRestorePassword(login) })
    }

    override suspend fun verifyCodeRegistration(
        signUpVerify: SignUpVerify
    ): Result<RequestTokenResponse> {
        return safeApiCall({ brokerApi.verifyCodeRegistration(signUpVerify) })
    }

    override suspend fun verifyCodeRestorePassword(
        login: String, confirmationCode: String, token: String
    ): Result<RequestTokenResponse> {
        return safeApiCall({ brokerApi.verifyCodeRestorePassword(login, confirmationCode, token) })
    }

    override suspend fun setNewPassword(
        login: String, password: String, token: String
    ): Result<ResponseBoolean> {
        return safeApiCall({ brokerApi.setNewPassword(login, password, token) })
    }

    override suspend fun resendCode(signUpResend: SignUpResend): Result<RequestTokenResponse> {
        return safeApiCall({ brokerApi.resendCode(signUpResend) })
    }

    /**
     * User signing in
     */
    override suspend fun signInUser(signInRequest: SignInRequest): Result<TokensResponse> {
        return safeApiCall({ brokerApi.signIn(signInRequest) })
    }

    override suspend fun sendMessageToken(messageToken: FirebaseMessageToken): Result<ResponseBoolean> {
        return safeApiCall({ brokerApi.sendMessageToken(messageToken) })
    }

    override suspend fun verifyToken(): Result<ResponseBoolean> {
        return safeApiCall({ brokerApi.verifyToken() })
    }

    override suspend fun logout(): Result<ResponseBoolean> {
        return safeApiCall({ brokerApi.logout() })
    }

    /**
     * Refreshing access token
     */
    override suspend fun refreshAccessToken(refreshTokenRequest: RefreshTokenRequest): Result<TokensResponse> {
        return safeApiCall({ brokerApi.authRefresh(refreshTokenRequest) })
    }

    /**
     * Confirm phone number on new account
     */
    override suspend fun requestConfirmPhoneCode(): Result<ConfirmPhoneResponse> {
        return safeApiCall({ brokerApi.phoneConfirmationRequest() })
    }

}