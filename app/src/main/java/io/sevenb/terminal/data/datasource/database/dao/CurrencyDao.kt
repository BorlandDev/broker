package io.sevenb.terminal.data.datasource.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import io.sevenb.terminal.data.datasource.database.model.CurrencyEntity
import io.sevenb.terminal.data.model.core.Result
import kotlinx.coroutines.flow.Flow
import java.util.*

@Dao
interface CurrencyDao {
    @Query("SELECT * FROM currency_table")
    fun getAll(): LiveData<List<CurrencyEntity>>

    @Query("SELECT * FROM currency_table")
    fun getAllFlow(): Flow<List<CurrencyEntity>>

    @Query("SELECT * FROM currency_table")
    fun getAllResult(): List<CurrencyEntity>

    @Query("SELECT * FROM currency_table WHERE ticker LIKE :ticker")
    fun getCurrency(ticker: String): Flow<CurrencyEntity>

    @Query("DELETE FROM currency_table")
    suspend fun deleteAll()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(vararg records: CurrencyEntity)

    @Delete
    suspend fun delete(record: CurrencyEntity)

    @Update
    suspend fun update(record: CurrencyEntity)
}