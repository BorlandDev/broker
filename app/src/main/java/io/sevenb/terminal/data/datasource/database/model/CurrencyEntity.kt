package io.sevenb.terminal.data.datasource.database.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "currency_table")
data class CurrencyEntity(
    @PrimaryKey
    @ColumnInfo(name = "ticker") val ticker: String = "BTC",
    @ColumnInfo(name = "currencyName") val currencyName: String = "Bitcoin",
//    @ColumnInfo(name = "cmcId") var cmcId: Int?,
//    @PrimaryKey(autoGenerate = true) var uid: Int = 0
)