package io.sevenb.terminal.data.model.order

import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import io.sevenb.terminal.R
import io.sevenb.terminal.device.utils.DateTimeUtil
import io.sevenb.terminal.domain.usecase.charts.AccountData.Companion.getBaseCurrencyCharacter
import io.sevenb.terminal.ui.extension.newBackground
import io.sevenb.terminal.ui.extension.newTextColor
import io.sevenb.terminal.ui.extension.toPlainString
import io.sevenb.terminal.ui.screens.order.OrderBottomSheetFragment
import io.sevenb.terminal.ui.screens.order.OrderViewModel.Companion.roundFloat
import io.sevenb.terminal.ui.screens.portfolio.BaseOrderListItem
import io.sevenb.terminal.ui.screens.trading.TradingWalletItem
import kotlinx.android.synthetic.main.item_open_order_list.view.*
import java.math.BigDecimal

class OrderItem(
    val symbol: String,
    val orderId: String,
    val externalOrderId: String,
    val createdAt: String,
    val updatedAt: String,
    val price: Float,
    val originalAmount: Float,
    var executedAmount: Double,
    val cummulativeQuoteAmount: Double,
    val status: String,
    val timeLimitType: String,
    val type: String,
    val side: String,
    val fills: List<Fills>,
    val binanceCommission: Double?,
    val commissionAsset: String,
    var baseAsset: String = "",
    var quoteAsset: String = "",
    var baseAssetRate: Float? = null,
    var quoteAssetRate: Float? = null,
    var baseAssetIconId: Int? = null,
    var quoteAssetIconId: Int? = null
) : BaseOrderListItem {

    override fun bind(itemView: View) {
        itemView.apply {
            loadImage(this, quoteAssetIconId, ivSecondTickerOrder)
            loadImage(this, baseAssetIconId, ivFirstTickerOrder)

            baseAsset = if (baseAsset == "BCHA") "XEC" else baseAsset
            quoteAsset = if (quoteAsset == "BCHA") "XEC" else quoteAsset

            val textOrder = "$baseAsset/$quoteAsset"
            tvSymbolOrder.text = textOrder

            if (createdAt.isNotEmpty()) {
                try {
                    tvTransactionDateOrder.text = "%s".format(formatDate())
                } catch (e: Exception) {
                }
            }

//            tvTickerPnlOrder.newTextColor(getTextColorPnl())
            val originalAmountString = "${getCharacter()} $originalAmount $baseAsset"
            tvTickerPnlOrder.text = originalAmountString
            baseAssetRate?.let {
                val estimatedOriginalAmount = roundFloat((originalAmount * it), 2, true)
                val estimatedOriginalAmountString =
                    "≈ ${getBaseCurrencyCharacter()}$estimatedOriginalAmount"
                tvEstimatedMoney.text = estimatedOriginalAmountString
            }

            val executedAmountString = executedAmount.toString()
            tvBuySellPrice.text =
                if (executedAmountString != "0.0") executedAmountString
                else (originalAmount * price).toPlainString()
            quoteAssetRate?.let {
                val estimatedExecutedAmount =
                    if (executedAmountString != "0.0") roundFloat((executedAmount * it).toFloat(), 2, true)
                    else roundFloat(((originalAmount * price) * it), 2, true)
                val estimatedExecutedAmountString =
                    "≈ ${getBaseCurrencyCharacter()}$estimatedExecutedAmount"
                tvBuySellEstimation.text = estimatedExecutedAmountString
            }
        }
    }

    private fun loadImage(itemView: View, cmcIconId: Int? = 0, target: ImageView) {
        val iconId = cmcIconId ?: 0
        val requestOptions = RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)
        if (cmcIconId == 7686) {
            Glide.with(itemView.context)
                .load(R.drawable.ic_e_cash_logo)
                .apply(requestOptions)
                .into(target)
        } else Glide.with(itemView.context)
            .load(TradingWalletItem.CMC_ICON_URL.format(iconId))
            .apply(requestOptions)
            .into(target)
    }

    private fun formatDate(): String {
        val timestamp = DateTimeUtil.parseToTimestamp(createdAt)
        val date = DateTimeUtil.dateFromTimestamp(timestamp)
        val dateArray = date.split(" ")
        return "${dateArray[1]} ${dateArray[0]}, ${dateArray[2]}"
    }

//    private fun getTextColorPnl() = when (side) {
//        "BUY" -> R.color.colorGreen
//        "SELL" -> R.color.colorRed
//        else -> android.R.color.black
//    }

    private fun getCharacter() = when (side) {
        "BUY" -> "+"
        "SELL" -> "-"
        else -> ""
    }

}