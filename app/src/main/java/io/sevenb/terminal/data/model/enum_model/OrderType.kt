package io.sevenb.terminal.data.model.enum_model

enum class OrderType {
    MARKET, LIMIT, TAKE_PROFIT_LIMIT, STOP_LOSS_LIMIT
}