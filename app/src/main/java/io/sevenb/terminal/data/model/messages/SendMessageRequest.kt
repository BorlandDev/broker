package io.sevenb.terminal.data.model.messages

import io.sevenb.terminal.data.model.enums.notifications.NotificationSpecialise
import io.sevenb.terminal.data.model.enums.TypeOfNotification

data class SendMessageRequest(
    private var data: MessageDataObj = MessageDataObj(),
    private var template_type: NotificationSpecialise = NotificationSpecialise.ORDER_COMPLETE,
    private var data_type: TypeOfNotification = TypeOfNotification.NOTIFICATION
)