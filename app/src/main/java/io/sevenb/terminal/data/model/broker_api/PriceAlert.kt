package io.sevenb.terminal.data.model.broker_api


import com.google.gson.annotations.SerializedName

data class PriceAlert(
    @SerializedName("base_asset")
    val baseAsset: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("fiat_currency")
    val fiatCurrency: String,
    @SerializedName("goal_change")
    val goalChange: String,
    @SerializedName("goal_type")
    val goalType: GoalType,
    @SerializedName("notify_by_email")
    val notifyByEmail: Boolean,
    @SerializedName("notify_by_push")
    val notifyByPush: Boolean,
    @SerializedName("side")
    val side: Side
)

enum class GoalType{
    VALUE_REACH, VALUE_CHANGE, PERCENTAGE_CHANGE
}

enum class Side{
    RISE, FALL
}