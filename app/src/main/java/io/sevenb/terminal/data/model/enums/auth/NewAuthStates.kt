package io.sevenb.terminal.data.model.enums.auth

import io.sevenb.terminal.data.model.enums.CommonState

enum class NewAuthStates : CommonState {

    LOGIN,
    TFA_VERIFICATION,
    TFA_CREATE,

    REGISTRATION,
    CONTINUES_REGISTRATION,

    PASSWORD_RESTORING,
    CONTINUES_PASSWORD_RESTORING,

    PASSWORD_CHANGE,
    CONTINUES_PASSWORD_CHANGE,

    CHANGE_PASSWORD_DONE,
    DONE_AUTH,
    SETTINGS,
}