package io.sevenb.terminal.data.datasource

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import androidx.annotation.WorkerThread
import androidx.core.content.edit
import io.sevenb.terminal.data.model.core.Result
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

interface PreferenceStorage {
    fun clearPrefs()

    fun clearBiometry()

    fun logOut()

    var countSuccessOrders: Int
    var refreshToken: String?
    var encryptedPassword: String?
    var listOfOrderSymbols: String?
    var localAccountData: String?
    var favoriteTickers: String?
    var baseCurrencyTicker: String?
    var infoConfirmationState: Boolean
    var firstLaunch: Boolean
    var isFingerPrintEnabled: Boolean
    var isPassCodeEnabled: Boolean
    var isLoggedOut: Int //0 - logged in // 1 - logged out
    var passCode: String?
    var password: String?
    var lockMinute: Boolean
    var lockFiveMinutes: Boolean
    var lockHalfHour: Boolean
    var lockHour: Boolean
    var lockReboot: Boolean
    var pauseTime: String?
    var timeoutEnabled: Boolean
    var isRebooted: Boolean
    var isLocked: Boolean
    var enabledNotifications: String?
    var enableNotifications: Int
    var countTransactions: Int
    var countEnters: String?
    var turnOnRateUs: Boolean
    var lastOpen: String?
    var openVersion: Int

    var lastRateTime: String? //VERIFIED
    //ADDED BY JIM IRS
    var dontAskVersion: Int
    var askLaterTime: String?
    var openPortfolioCount: Int
    var successTradeCount: Int
}

@Singleton
class SharedPreferenceStorage @Inject constructor(context: Context) : PreferenceStorage {

    private val prefs: Lazy<SharedPreferences> = lazy { // Lazy to prevent IO access to main thread.
        context.applicationContext.getSharedPreferences(
            PREFS_NAME, MODE_PRIVATE
        )
    }

    override fun clearPrefs() {
        prefs.value.edit().clear().apply()
    }

    override fun clearBiometry() {
        prefs.value.edit().apply {
            remove(PREF_ENCRYPTED_PASSWORD)
        }.apply()
    }

    override fun logOut() {
        clearBiometry()
        clearLockTimeout()
        prefs.value.edit().apply {
            remove(PREF_IS_FIRST_LAUNCH)
            remove(PREF_IS_FINGERPRINT_ENABLED)
            remove(PREF_IS_PASS_CODE_ENABLED)
            remove(PREF_IS_LOGGED_OUT)
            remove(PREF_PASS_CODE)
            remove(PREF_REFRESH_TOKEN)
            remove(PREF_IS_TIMEOUT_ENABLED)
        }.apply()
    }

    private fun clearLockTimeout() {
        prefs.value.edit().apply {
            remove(PREF_LOCK_MINUTE)
            remove(PREF_LOCK_FIVE_MINUTES)
            remove(PREF_LOCK_HALF_HOUR)
            remove(PREF_LOCK_HOUR)
            remove(PREF_LOCK_REBOOT)
            remove(PREF_IS_REBOOTED)
            remove(PREF_IS_LOCKED)
        }.apply()
    }

    override var countSuccessOrders by IntPreference(prefs, PREF_COUNT_SUCCESS_ORDER, 0)
    override var refreshToken by StringPreference(prefs, PREF_REFRESH_TOKEN, "")
    override var encryptedPassword by StringPreference(prefs, PREF_ENCRYPTED_PASSWORD, "")
    override var listOfOrderSymbols by StringPreference(prefs, PREF_LIST_OF_ORDER_SYMBOLS, "")
    override var localAccountData by StringPreference(prefs, PREF_LOCAL_ACCOUNT_DATA, "")
    override var favoriteTickers by StringPreference(prefs, PREF_FAVORITE_TICKERS, "")
    override var baseCurrencyTicker by StringPreference(prefs, PREF_BASE_CURRENCY_TICKER, "")
    override var infoConfirmationState by BooleanPreference(
        prefs,
        PREF_SHOW_INFO_CONFIRMATION,
        true
    )
    override var firstLaunch by BooleanPreference(prefs, PREF_IS_FIRST_LAUNCH, true)
    override var isFingerPrintEnabled: Boolean by BooleanPreference(
        prefs,
        PREF_IS_FINGERPRINT_ENABLED,
        false
    )
    override var isPassCodeEnabled: Boolean by BooleanPreference(
        prefs,
        PREF_IS_PASS_CODE_ENABLED,
        false
    )
    override var isLoggedOut by IntPreference(prefs, PREF_IS_LOGGED_OUT, -1)
    override var passCode by StringPreference(prefs, PREF_PASS_CODE, "")
    override var password by StringPreference(prefs, PREF_PASSWORD, "")

    override var lockMinute by BooleanPreference(prefs, PREF_LOCK_MINUTE, false)
    override var lockFiveMinutes by BooleanPreference(prefs, PREF_LOCK_FIVE_MINUTES, false)
    override var lockHalfHour by BooleanPreference(prefs, PREF_LOCK_HALF_HOUR, false)
    override var lockHour by BooleanPreference(prefs, PREF_LOCK_HOUR, false)
    override var lockReboot by BooleanPreference(prefs, PREF_LOCK_REBOOT, false)
    override var pauseTime by StringPreference(prefs, PREF_PAUSE_TIME, "")
    override var enabledNotifications by StringPreference(
        prefs, PREF_ENABLED_NOTIFICATIONS, "[WITHDRAW, DEPOSIT, ORDER]"
    )
    override var enableNotifications by IntPreference(prefs, PREF_ENABLED_NOTIFICATIONS, -1)
    override var countTransactions by IntPreference(prefs, PREF_COUNT_TRANSACTIONS, 0)
    override var countEnters by StringPreference(prefs, PREF_COUNT_ENTER, "")
    override var turnOnRateUs by BooleanPreference(prefs, PREF_TURN_ON_RATE_US, true)
    override var lastOpen by StringPreference(prefs, PREF_LAST_OPEN, "0")
    override var openVersion by IntPreference(prefs, PREF_OPEN_VERSION, 1)
    override var lastRateTime by StringPreference(prefs, PREF_LAST_RATE_TIME, "0")
    override var timeoutEnabled by BooleanPreference(prefs, PREF_IS_TIMEOUT_ENABLED, false)
    override var isRebooted by BooleanPreference(prefs, PREF_IS_REBOOTED, false)
    override var isLocked by BooleanPreference(prefs, PREF_IS_LOCKED, false)


    override var dontAskVersion by IntPreference(prefs, PREF_DONT_ASK_VERSION, 0)
    override var askLaterTime by StringPreference(prefs, PREF_ASK_LATER_TIME, "0")
    override var openPortfolioCount by IntPreference(prefs, PREF_OPEN_PORTFOLIO_COUNT, 0)
    override var successTradeCount by IntPreference(prefs, PREF_SUCCESS_TRADE_COUNT, 0)

    companion object {
        const val PREF_COUNT_SUCCESS_ORDER = "PREF_COUNT_SUCCESS_ORDER"
        const val PREFS_NAME = "terminal"
        const val PREF_REFRESH_TOKEN = "PREF_REFRESH_TOKEN"
        const val PREF_ENCRYPTED_PASSWORD = "PREF_ENCRYPTED_PASSWORD"
        const val PREF_LIST_OF_ORDER_SYMBOLS = "PREF_LIST_OF_ORDER_SYMBOLS"
        const val PREF_LOCAL_ACCOUNT_DATA = "PREF_LOCAL_ACCOUNT_DATA"
        const val PREF_FAVORITE_TICKERS = "PREF_FAVORITE_TICKERS"
        const val PREF_BASE_CURRENCY_TICKER = "PREF_BASE_CURRENCY_TICKER"
        const val PREF_SHOW_INFO_CONFIRMATION = "PREF_SHOW_INFO_CONFIRMATION"
        const val PREF_IS_FIRST_LAUNCH = "PREF_IS_FIRST_LAUNCH"
        const val PREF_IS_FINGERPRINT_ENABLED = "PREF_IS_FINGERPRINT_ENABLED"
        const val PREF_IS_PASS_CODE_ENABLED = "PREF_IS_PASS_CODE_ENABLED"
        const val PREF_IS_LOGGED_OUT = "PREF_IS_LOGGED_OUT"
        const val PREF_PASS_CODE = "PREF_PASS_CODE"
        const val PREF_PASSWORD = "PREF_PASSWORD"
        const val PREF_LOCK_MINUTE = "PREF_LOCK_MINUTE"
        const val PREF_LOCK_FIVE_MINUTES = "PREF_LOCK_FIVE_MINUTES"
        const val PREF_LOCK_HALF_HOUR = "PREF_LOCK_HALF_HOUR"
        const val PREF_LOCK_HOUR = "PREF_LOCK_HOUR"
        const val PREF_LOCK_REBOOT = "PREF_LOCK_REBOOT"
        const val PREF_PAUSE_TIME = "PREF_PAUSE_TIME"
        const val PREF_IS_TIMEOUT_ENABLED = "PREF_IS_TIMEOUT_ENABLED"
        const val PREF_IS_REBOOTED = "PREF_IS_REBOOTED"
        const val PREF_IS_LOCKED = "PREF_IS_LOCKED"
        const val PREF_ENABLED_NOTIFICATIONS = "PREF_ENABLED_NOTIFICATIONS"
        const val PREF_COUNT_TRANSACTIONS = "PREF_COUNT_TRANSACTIONS"
        const val PREF_COUNT_ENTER = "PREF_COUNT_ENTER"
        const val PREF_TURN_ON_RATE_US = "PREF_TURN_ON_RATE_US"
        const val PREF_LAST_OPEN = "PREF_LAST_OPEN"
        const val PREF_OPEN_VERSION = "PREF_OPEN_VERSION"
        const val PREF_LAST_RATE_TIME = "PREF_LAST_RATE_TIME"

        const val PREF_DONT_ASK_VERSION = "PREF_DONT_ASK_VERSION"
        const val PREF_ASK_LATER_TIME = "PREF_ASK_LATER_TIME"
        const val PREF_OPEN_PORTFOLIO_COUNT = "PREF_OPEN_PORTFOLIO_COUNT"
        const val PREF_SUCCESS_TRADE_COUNT = "PREF_SUCCESS_TRADE_COUNT"
    }

}

class BooleanPreference(
    private val preferences: Lazy<SharedPreferences>,
    private val name: String,
    private val defaultValue: Boolean
) : ReadWriteProperty<Any, Boolean> {

    @WorkerThread
    override fun getValue(thisRef: Any, property: KProperty<*>): Boolean {
        return preferences.value.getBoolean(name, defaultValue)
    }

    override fun setValue(thisRef: Any, property: KProperty<*>, value: Boolean) {
        preferences.value.edit { putBoolean(name, value) }
    }
}

class StringPreference(
    private val preferences: Lazy<SharedPreferences>,
    private val name: String,
    private val defaultValue: String?
) : ReadWriteProperty<Any, String?> {

    @WorkerThread
    override fun getValue(thisRef: Any, property: KProperty<*>): String? {
        return preferences.value.getString(name, defaultValue)
    }

    override fun setValue(thisRef: Any, property: KProperty<*>, value: String?) {
        preferences.value.edit { putString(name, value) }
    }
}

class IntPreference(
    private val preferences: Lazy<SharedPreferences>,
    private val name: String,
    private val defaultValue: Int
) : ReadWriteProperty<Any, Int> {

    @WorkerThread
    override fun getValue(thisRef: Any, property: KProperty<*>): Int {
        return preferences.value.getInt(name, defaultValue)
    }

    override fun setValue(thisRef: Any, property: KProperty<*>, value: Int) {
        preferences.value.edit { putInt(name, value) }
    }
}
