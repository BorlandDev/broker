package io.sevenb.terminal.data.model.order

import com.google.gson.annotations.SerializedName
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class BidAskResponse(
    @SerializedName("event") val event: String?,
    @SerializedName("data") val data: BidAskEventData?,
)
