package io.sevenb.terminal.data.model.messages

data class MessageResponse(
    private val template_type: String,
    private val data_type: String,
    private val status: String,
    private val data: List<Any>,
    private val user_id: Long,
    private val id: String,
    private val created_at: String,
    private val updated_at: String
)