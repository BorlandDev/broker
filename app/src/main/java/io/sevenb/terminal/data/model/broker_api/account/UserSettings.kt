package io.sevenb.terminal.data.model.broker_api.account

import android.os.Parcelable
import io.sevenb.terminal.data.model.messages.MainNotificationsPush
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserSettings(
    val tfa: Boolean,
    var push: MutableList<MainNotificationsPush>,
) : Parcelable