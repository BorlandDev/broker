package io.sevenb.terminal.data.repository.auth

import io.sevenb.terminal.data.datasource.PreferenceStorage
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.repository.BaseRepository
import io.sevenb.terminal.domain.repository.StorageRepository
import kotlinx.coroutines.runBlocking
import java.util.*
import javax.inject.Inject


class PreferenceRepository @Inject constructor(
    private val preferenceStorage: PreferenceStorage
) : BaseRepository(), StorageRepository {

    override var accessToken: String? = null

    companion object {
        private const val defaultBaseCurrency = "USD"
        var baseCurrencyTicker: String = defaultBaseCurrency
    }

    init {
        baseCurrencyTicker = runBlocking {
            when (val result = restoreBaseCurrency()) {
                is Result.Success -> {
                    val ticker = result.data
                    if (ticker.isNotEmpty()) {
                        ticker
                    } else {
                        defaultBaseCurrency
                    }
                }
                is Result.Error -> "USD"
            }
        }
    }

    /**
     * Clear all local data from SharedPreferences
     */
    override suspend fun clearSharedPreferences(): Result<Unit> {
        preferenceStorage.clearPrefs()
        return Result.Success(Unit)
    }

    override suspend fun clearBiometry(): Result<Unit> {
        preferenceStorage.clearBiometry()
        return Result.Success(Unit)
    }


    override suspend fun storeRefreshToken(refreshToken: String): Result<Unit> {
        preferenceStorage.refreshToken = refreshToken
        return Result.Success(Unit)
    }

    override suspend fun restoreRefreshToken(): Result<String> {
        return Result.Success(preferenceStorage.refreshToken ?: "")
    }

    override suspend fun storeEncryptedPassword(encryptedPassword: String): Result<Unit> {
        preferenceStorage.encryptedPassword = encryptedPassword
        return Result.Success(Unit)
    }

    override suspend fun restoreEncryptedPassword(): Result<String> {
        return Result.Success(preferenceStorage.encryptedPassword ?: "")
    }

    override suspend fun restoreListOfOrderSymbols(): Result<String> {
        return Result.Success(preferenceStorage.listOfOrderSymbols ?: "")
    }

    override suspend fun storeListOfOrderSymbols(listOfOrderSymbols: String): Result<Unit> {
        preferenceStorage.listOfOrderSymbols = listOfOrderSymbols
        return Result.Success(Unit)
    }

    override suspend fun storeLocalAccountData(localAccountDataString: String): Result<Unit> {
        preferenceStorage.localAccountData = localAccountDataString
        return Result.Success(Unit)
    }

    override suspend fun restoreLocalAccountData(): Result<String> {
        return Result.Success(preferenceStorage.localAccountData ?: "")
    }

    override suspend fun storeBaseCurrency(ticker: String): Result<Unit> {
        baseCurrencyTicker = ticker
        preferenceStorage.baseCurrencyTicker = ticker
        return Result.Success(Unit)
    }

    override suspend fun restoreBaseCurrency(): Result<String> {
        return Result.Success(preferenceStorage.baseCurrencyTicker ?: "")
    }

    override suspend fun storeFavoriteTickers(favoriteCurrencies: String): Result<Unit> {
        preferenceStorage.favoriteTickers = favoriteCurrencies
        return Result.Success(Unit)
    }

    override suspend fun restoreFavoriteTickers(): Result<String> {
        return Result.Success(preferenceStorage.favoriteTickers ?: "")
    }

    override suspend fun storeInfoConfirmationState(isShowDialog: Boolean): Result<Unit> {
        preferenceStorage.infoConfirmationState = isShowDialog
        return Result.Success(Unit)
    }

    override suspend fun restoreInfoConfirmationState(): Result<Boolean> {
        return Result.Success(preferenceStorage.infoConfirmationState)
    }

    override suspend fun restoreFirstLaunch(): Result<Boolean> {
        return Result.Success(preferenceStorage.firstLaunch)
    }

    override suspend fun storeFirstLaunch(boolean: Boolean): Result<Unit> {
        preferenceStorage.firstLaunch = boolean
        return Result.Success(Unit)
    }

    override suspend fun restorePassCodeEnabled(): Result<Boolean> {
        return Result.Success(preferenceStorage.isPassCodeEnabled)
    }

    override suspend fun storePassCodeEnabled(boolean: Boolean): Result<Unit> {
        preferenceStorage.isPassCodeEnabled = boolean
        return Result.Success(Unit)
    }

    override suspend fun restoreFingerprintEnabled(): Result<Boolean> {
        return Result.Success(preferenceStorage.isFingerPrintEnabled)
    }

    override suspend fun storeFingerprintEnabled(boolean: Boolean): Result<Unit> {
        preferenceStorage.isFingerPrintEnabled = boolean
        return Result.Success(Unit)
    }

    override suspend fun restoreLoggedOut(): Result<Int> {
        return Result.Success(preferenceStorage.isLoggedOut)
    }

    override suspend fun storeLoggedOut(value: Int): Result<Unit> {
        preferenceStorage.isLoggedOut = value
        return Result.Success(Unit)
    }

    override suspend fun restorePassCode(): Result<String> {
        return Result.Success(preferenceStorage.passCode ?: "")
    }

    override suspend fun storePassCode(passCode: String): Result<Unit> {
        preferenceStorage.passCode = passCode
        return Result.Success(Unit)
    }

    override suspend fun restorePassword(): Result<String> {
        return Result.Success(preferenceStorage.password ?: "")
    }

    override suspend fun storePassword(password: String): Result<Unit> {
        preferenceStorage.password = password
        return Result.Success(Unit)
    }

    override suspend fun restoreMinuteLock(): Result<Boolean> {
        return Result.Success(preferenceStorage.lockMinute)
    }

    override suspend fun storeMinuteLock(value: Boolean): Result<Unit> {
        preferenceStorage.lockMinute = value
        return Result.Success(Unit)
    }

    override suspend fun restoreFiveMinutesLock(): Result<Boolean> {
        return Result.Success(preferenceStorage.lockFiveMinutes)
    }

    override suspend fun storeFiveMinutesLock(value: Boolean): Result<Unit> {
        preferenceStorage.lockFiveMinutes = value
        return Result.Success(Unit)
    }

    override suspend fun restoreHalfHourLock(): Result<Boolean> {
        return Result.Success(preferenceStorage.lockHalfHour)
    }

    override suspend fun storeHalfHourLock(value: Boolean): Result<Unit> {
        preferenceStorage.lockHalfHour = value
        return Result.Success(Unit)
    }

    override suspend fun restoreHourLock(): Result<Boolean> {
        return Result.Success(preferenceStorage.lockHour)
    }

    override suspend fun storeHourLock(value: Boolean): Result<Unit> {
        preferenceStorage.lockHour = value
        return Result.Success(Unit)
    }

    override suspend fun restoreRebootLock(): Result<Boolean> {
        return Result.Success(preferenceStorage.lockReboot)
    }

    override suspend fun storeRebootLock(value: Boolean): Result<Unit> {
        preferenceStorage.lockReboot = value
        return Result.Success(Unit)
    }

    override suspend fun restorePauseTime(): Result<String> {
        return Result.Success(preferenceStorage.pauseTime ?: "")
    }

    override suspend fun storePauseTime(value: String): Result<Unit> {
        preferenceStorage.pauseTime = value
        return Result.Success(Unit)
    }

    override suspend fun restoreTimeoutEnabled(): Result<Boolean> {
        return Result.Success(preferenceStorage.timeoutEnabled)
    }

    override suspend fun storeTimeoutEnabled(value: Boolean): Result<Unit> {
        preferenceStorage.timeoutEnabled = value
        return Result.Success(Unit)
    }

    override suspend fun restoreIsRebooted(): Result<Boolean> {
        return Result.Success(preferenceStorage.isRebooted)
    }

    override suspend fun storeIsRebooted(value: Boolean): Result<Unit> {
        preferenceStorage.isRebooted = value
        return Result.Success(Unit)
    }

    override suspend fun restoreIsLocked(): Result<Boolean> {
        return Result.Success(preferenceStorage.isLocked)
    }

    override suspend fun storeIsLocked(value: Boolean): Result<Unit> {
        preferenceStorage.isLocked = value
        return Result.Success(Unit)
    }

    override suspend fun restoreEnabledNotifications(): Result<String> {
        return Result.Success(preferenceStorage.enabledNotifications ?: "")
    }

    override suspend fun storeEnabledNotifications(notifications: String): Result<Unit> {
        preferenceStorage.enabledNotifications = notifications
        return Result.Success(Unit)
    }

    override suspend fun restoreEnableNotifications(): Result<Int> {
        return Result.Success(preferenceStorage.enableNotifications)
    }

    override suspend fun storeEnableNotifications(value: Int): Result<Unit> {
        preferenceStorage.enableNotifications = value
        return Result.Success(Unit)
    }

    override suspend fun restoreCountEnter(): Result<String> {
        return if (!preferenceStorage.turnOnRateUs) {
            Result.Success("")
        } else {
            Result.Success(preferenceStorage.countEnters ?: "")
        }
    }

    override suspend fun incrementCountEnter(): Result<Unit> {
        preferenceStorage.countEnters = preferenceStorage.countEnters + " " + Date().time.toString()
        return Result.Success(Unit)
    }

    override suspend fun resetCountEnter(): Result<Unit> {
        preferenceStorage.countEnters = ""
        return Result.Success(Unit)
    }

    override suspend fun disableRateUs(): Result<Unit> {
        preferenceStorage.turnOnRateUs = false
        return Result.Success(Unit)
    }

    override suspend fun enableRateUs(): Result<Unit> {
        preferenceStorage.turnOnRateUs = true
        return Result.Success(Unit)
    }

    override suspend fun restoreCountTransactions(): Result<Boolean> {
        return if (!preferenceStorage.turnOnRateUs) {
            Result.Success(false)
        } else {
            Result.Success(preferenceStorage.countTransactions >= 2)
        }
    }

    override suspend fun incrementCountTransactions(): Result<Unit> {
        preferenceStorage.countTransactions = preferenceStorage.countTransactions + 1
        return Result.Success(Unit)
    }

    override suspend fun setLastOpen(value: String): Result<Unit> {
        preferenceStorage.lastOpen = value
        return Result.Success(Unit)
    }

    override suspend fun getLastOpen(): Result<String> {
        return Result.Success(preferenceStorage.lastOpen.toString())
    }

    override suspend fun setOpenVersion(value: Int): Result<Unit> {
        preferenceStorage.openVersion = value
        return Result.Success(Unit)
    }

    override suspend fun getOpenVersion(): Result<Int> {
        return Result.Success(preferenceStorage.openVersion)
    }

    //ADDED START BY JIM IRS

    override suspend fun getLastRateTime(): Result<String> {
        return Result.Success(preferenceStorage.lastRateTime.toString())
    }

    override suspend fun setLastRateTime(value: String): Result<Unit> {
        preferenceStorage.lastRateTime = value
        return Result.Success(Unit)
    }

    override suspend fun getOpenPortfolioCount(): Result<Int> {
        return Result.Success(preferenceStorage.openPortfolioCount)
    }

    override suspend fun incrementOpenPortfolioCount(): Result<Unit> {
        preferenceStorage.openPortfolioCount = preferenceStorage.openPortfolioCount + 1
        return Result.Success(Unit)
    }

    override suspend fun getSuccessTradeCount(): Result<Int> {
        return Result.Success(preferenceStorage.successTradeCount)
    }

    override suspend fun incrementSuccessTradeCount(): Result<Unit> {
        preferenceStorage.successTradeCount = preferenceStorage.successTradeCount + 1
        return Result.Success(Unit)
    }

    override suspend fun getDontAskVersion(): Result<Int> {
        return Result.Success(preferenceStorage.dontAskVersion)
    }

    override suspend fun setDontAskVersion(value: Int): Result<Unit> {
        preferenceStorage.dontAskVersion = value
        return Result.Success(Unit)
    }

    override suspend fun getAskLaterTime(): Result<String> {
        return Result.Success(preferenceStorage.askLaterTime.toString())
    }

    override suspend fun setAskLaterTime(value: String): Result<Unit> {
        preferenceStorage.askLaterTime = value
        return Result.Success(Unit)
    }

    //ADDED END

    override suspend fun logOut(): Result<Unit> {
        return Result.Success(preferenceStorage.logOut())
    }

}