package io.sevenb.terminal.data.model.broker_api.markets

data class AccountBalance(
    val ticker: String = "",
    val name: String = "",
    var available: String = "",
    var locked: String = "",
    var availableEstimated: String = "",
    var lockedEstimated: String = ""
) {

    fun updateEstimated(rate: Float?) {
        rate ?: return

        val availableFloat = available.toFloatOrNull() ?: 0f
        val lockedFloat = locked.toFloatOrNull() ?: 0f

        availableEstimated = availableFloat.times(rate).toString()
        lockedEstimated = lockedFloat.times(rate).toString()
    }

}