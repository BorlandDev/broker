package io.sevenb.terminal.data.model.cryptocompare

import com.google.gson.annotations.SerializedName

data class CryptocompareData(
    @SerializedName("Data")
    val data: List<CryptoCmpModel>?
)