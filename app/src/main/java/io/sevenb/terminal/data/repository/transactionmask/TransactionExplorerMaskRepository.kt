package io.sevenb.terminal.data.repository.transactionmask

import io.sevenb.terminal.data.datasource.retrofit.ChangeNowApi
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.transactionexplorer.TransactionExplorerMask
import io.sevenb.terminal.data.repository.BaseRepository
import io.sevenb.terminal.domain.repository.TransactionExplorerApi
import javax.inject.Inject

class TransactionExplorerMaskRepository @Inject constructor(
    private val changeNowApi: ChangeNowApi
) : BaseRepository(), TransactionExplorerApi {

    override suspend fun getTransactionMask(currency: String): Result<TransactionExplorerMask> =
        safeApiCall({ changeNowApi.getTransactionMask(currency) })
}