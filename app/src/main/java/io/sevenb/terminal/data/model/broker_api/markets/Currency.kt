package io.sevenb.terminal.data.model.broker_api.markets

import android.view.View
import io.sevenb.terminal.data.model.trading.TradeChartRange
import io.sevenb.terminal.ui.screens.select_currency.BaseSelectItem
import kotlinx.coroutines.channels.Channel

data class Currency(
    override var ticker: String,
    val name: String,
    val networks: List<CurrencyNetwork> = emptyList(),
    override var network: String? = "",
    override var isExpanded: Boolean? = false,
    override var currentRange: TradeChartRange? = TradeChartRange.DAY,
    override var updateExpandedStateChannel: Channel<Pair<String, Boolean>>? = null,
    override var estimated: String = "",
    override var amount: String? = ""
) : BaseSelectItem {

    override fun bind(
        itemView: View,
        baseCurrency: String,
        itemSelected: (BaseSelectItem) -> Unit
    ) {
        TODO("Not yet implemented")
    }
}

data class CurrencyNetwork(
    val ticker: String,
    val name: String,
    val deposit: Boolean,
    val hasMemo: Boolean = false,
    val minConfirm: Float,
    val network: String,
    val withdraw: Boolean,
    val withdrawFee: Float,
    val withdrawMin: Float
)