package io.sevenb.terminal.data.model.logs

import com.google.firebase.database.Exclude
import com.google.firebase.database.IgnoreExtraProperties
import com.google.firebase.database.PropertyName

@IgnoreExtraProperties
data class Profile(
    @get:PropertyName("last_log_since")
    @set:PropertyName("last_log_since")
    @PropertyName("last_log_since")
    var lastLogSince: Long = 0L,
    @get:PropertyName("email")
    @set:PropertyName("email")
    @PropertyName("email")
    var email: String = "",
    @get:PropertyName("manufacturer")
    @set:PropertyName("manufacturer")
    @PropertyName("manufacturer")
    var manufacturer: String? = "",
    @get:PropertyName("model")
    @set:PropertyName("model")
    @PropertyName("model")
    var model: String? = "",
) {
    @Exclude
    fun toMap(): Map<String, Any?> {
        return mapOf(
            "last_log_since" to lastLogSince,
            "email" to email,
            "manufacturer" to manufacturer,
            "model" to model
        )
    }
}