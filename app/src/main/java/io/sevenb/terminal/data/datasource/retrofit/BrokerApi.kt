package io.sevenb.terminal.data.datasource.retrofit

import io.sevenb.terminal.data.model.ProfitLoss
import io.sevenb.terminal.data.model.auth.*
import io.sevenb.terminal.data.model.broker_api.*
import io.sevenb.terminal.data.model.broker_api.account.*
import io.sevenb.terminal.data.model.broker_api.markets.AccountBalance
import io.sevenb.terminal.data.model.broker_api.markets.Currency
import io.sevenb.terminal.data.model.broker_api.markets.Market
import io.sevenb.terminal.data.model.broker_api.rates.MarketRate
import io.sevenb.terminal.data.model.broker_api.rates.MarketRateParams
import io.sevenb.terminal.data.model.broker_api.verification.Verification
import io.sevenb.terminal.data.model.broker_api.verification.VerificationToken
import io.sevenb.terminal.data.model.candles.CandlesData
import io.sevenb.terminal.data.model.messages.*
import io.sevenb.terminal.data.model.order.OrderItem
import io.sevenb.terminal.data.model.order.OrderParams
import io.sevenb.terminal.data.model.settings.SettingsPOJO
import io.sevenb.terminal.data.model.symbols.SymbolsData
import io.sevenb.terminal.data.model.tfa.TfaResponse
import io.sevenb.terminal.data.model.tfa.TfaVerification
import io.sevenb.terminal.data.model.withdrawal.ConfirmWithdrawalParams
import io.sevenb.terminal.data.model.withdrawal.WithdrawalHistoryItem
import io.sevenb.terminal.data.model.withdrawal.WithdrawalRequest
import retrofit2.Response
import retrofit2.http.*


interface BrokerApi {

    /**
     * User sign-up
     */
    @POST("sign-up")
    suspend fun requestVerificationCodeRegistration(
        @Body userData: UserData
    ): Response<RequestTokenResponse>

    @FormUrlEncoded
    @POST("reset-password/send-code")
    suspend fun requestVerificationCodeRestorePassword(
        @Field("login") login: String
    ): Response<RequestTokenResponse>

    @POST("sign-up/confirm")
    suspend fun verifyCodeRegistration(
        @Body signUpVerify: SignUpVerify
    ): Response<RequestTokenResponse>

    @FormUrlEncoded
    @POST("reset-password/verify-code")
    suspend fun verifyCodeRestorePassword(
        @Field("login") login: String,
        @Field("code") code: String,
        @Field("token") token: String
    ): Response<RequestTokenResponse>

    @FormUrlEncoded
    @POST("reset-password/set-new-password")
    suspend fun setNewPassword(
        @Field("login") login: String,
        @Field("password") password: String,
        @Field("token") token: String
    ): Response<ResponseBoolean>

    //TODO DEPRECATE AND REUSE sign-up
    @POST("sign-up/resend")
    suspend fun resendCode(
        @Body signUpResend: SignUpResend
    ): Response<RequestTokenResponse>

    @POST("account/confirmation/request")
    suspend fun phoneConfirmationRequest(): Response<ConfirmPhoneResponse>

    /**
     * User sign-in
     */
    @POST("sign-in")
    suspend fun signIn(
        @Body signInRequest: SignInRequest
    ): Response<TokensResponse>

    @POST("auth/refresh")
    suspend fun authRefresh(
        @Body refreshTokenRequest: RefreshTokenRequest
    ): Response<TokensResponse>

    /**
     * Captcha data API
     */
    @GET("captcha")
    suspend fun captchaGet(): Response<CaptchaGet>

    @POST("captcha")
    suspend fun captchaPost(
        @Query("geetest_validate") validate: String,
        @Query("geetest_seccode") secCode: String,
        @Query("geetest_challenge") challenge: String
    ): Response<CaptchaPost>

    /**
     * Account data
     */
    @GET("account/balances")
    suspend fun accountBalances(): Response<List<AccountBalance>>

    @GET("account/deposit/address")
    suspend fun depositAddress(
        @Query("ticker") ticker: String,
        @Query("network") network: String? = ""
    ): Response<DepositAddress>

    @GET("account/deposit/history")
    suspend fun depositHistory(
        @Query("limit") limit: Int = 20,
        @Query("offset") offset: Int = 1
    ): Response<List<DepositHistoryItem>>

    @GET("account/withdrawal")
    suspend fun withdrawalHistory(
        @Query("limit") limit: Int = 20,
        @Query("offset") offset: Int = 0
    ): Response<List<WithdrawalRequest>>

    @GET("account/status")
    suspend fun accountStatus(): Response<AccountStatus>

    @GET("account/info")
    suspend fun accountInfo(): Response<AccountInfo>

    /**
     * Trading data
     */
    @GET("currencies")
    suspend fun currencies(): Response<List<Currency>>

    @GET("markets")
    suspend fun markets(): Response<List<Market>>

    @GET("markets/rate")
    suspend fun marketsRate(
        @Query("from") from: String,
        @Query("to") to: String
    ): Response<MarketRate>

    @POST("batch/markets/rate")
    suspend fun batchMarketsRate(
        @Body listMarketRateParams: List<MarketRateParams>
    ): Response<List<MarketRate>>

    @GET("graphs/candle")
    suspend fun graphsCandle(): Response<List<Currency>>

    /**
     * Order flow
     */
    @POST("order")
    suspend fun orderPost(
        @Body orderParams: OrderParams
    ): Response<OrderItem>

    @GET("order/opened")
    suspend fun orderOpened(
        @Query("limit") limit: Int = 20,
        @Query("offset") offset: Int = 0
    ): Response<List<OrderItem>>

    @DELETE("order")
    suspend fun cancelOrder(
        @Query("symbol") symbol: String,
        @Query("orderId") orderId: String,
        @Query("externalOrderId") externalOrderId: String? = null
    ): Response<OrderItem>

    @GET("order/history")
    suspend fun orderHistory(
        @Query("startTime") startTime: String? = null,
        @Query("endTime") endTime: String? = null,
        @Query("limit") limit: Int = Int.MAX_VALUE
    ): Response<List<OrderItem>>

    /**
     * Withdrawal
     */
    @POST("account/withdrawal")
    suspend fun withdrawalRequest(
        @Body withdrawalRequest: WithdrawalRequest
    ): Response<WithdrawalHistoryItem>

    @POST("account/withdrawal/confirm")
    suspend fun withdrawalConfirm(
        @Body confirmWithdrawalParams: ConfirmWithdrawalParams
    ): Response<Unit>

    @POST("account/withdrawal/{id}")
    suspend fun withdrawalHash(
        @Path("id") id: String
    ): Response<WithdrawalHistoryItem>

    /**
     * Verification
     */
    @GET("account/verification")
    suspend fun getVerifications(): Response<List<Verification>>

    @POST("account/verification")
    suspend fun verification(): Response<VerificationToken>

    @POST("account/verification/refresh-token")
    suspend fun refreshVerificationToken(): Response<VerificationToken>

    @GET("account/withdrawal/available-withdrawal")
    suspend fun availableWithdrawal(): Response<AvailableWithdrawal>

    @GET("account/profit-and-loss")
    suspend fun profitLoss(): Response<ProfitLoss>

    /**
     * Candles
     */
    @GET("graphs/candle")
    suspend fun getCandles(
        @Query("symbol") symbol: String,
        @Query("interval") interval: String,
        @Query("startTime") startTime: String,
        @Query("endTime") endTime: String,
        @Query("limit") limit: Int = 500
    ): Response<CandlesData>


    /**
     * Filters
     */
    @GET("symbols")
    suspend fun getFilters(
        @Query("symbol") symbol: String
    ): Response<SymbolsData>

    @GET("currencies-info")
    suspend fun getCurrenciesInfo()

    /**
     * Firebase messaging
     */
    @GET("account/verify-token")
    suspend fun verifyToken(): Response<ResponseBoolean>

    @POST("misc/create-push")
    suspend fun sendNotification(
        @Body sendMessageRequest: SendMessageRequest
    ): Response<FirebaseMessageResponse>

    @GET("setting/text-types")
    suspend fun getTextTypes(): Response<TextTypesPOJO>

    //TODO UNUSED
//    @GET("setting/template")
//    suspend fun getTemplate(): Response<TemplatePOJO>

//    @GET("settings")
//    suspend fun getUserSettings(): Response<UserSettingsPOJO>

    @GET("settings")
    suspend fun  getUserSettings(): Response<SettingsPOJO>
//    suspend fun <T : Parcelable> getUserSettings(): Response<SettingsPOJO>

    //    @PUT("setting/main")
    @PUT("settings")
    suspend fun putSettings(
        @Body listNotificationsSettingsRequest: List<NotificationsSettingsRequest>
    ): Response<ResponseBoolean>


    /**
     * 2F
     */

    @GET("2fa/toggle")
    suspend fun toggleTfa(): Response<TfaResponse>

    @POST("2fa/verify")
    suspend fun verify(@Body tfaVerification: TfaVerification): Response<TokensResponse>


    @DELETE("account/price-request/{id}")
    suspend fun deleteAlert(@Path("id") id: String): Response<PriceAlert>

    @POST("account/price-request")
    suspend fun postAlert(@Body bodyAlert: PriceAlert): Response<CreatePriceAlert>

    @GET("account/price-request")
    suspend fun getAlert(): Response<PriceAlertGetApi>

    /**
     * Send message token
     */
    @POST("misc/update-message-token")
    suspend fun sendMessageToken(
        @Body firebaseMessageToken: FirebaseMessageToken
    ): Response<ResponseBoolean>

    /**
     * Logout
     */
    @POST("logout")
    suspend fun logout(): Response<ResponseBoolean>

    companion object {
        const val STAGE_BROKER_API_URL = "https://public-api.staging.sevenb.io/api/"
        const val PROD_BROKER_API_URL = "https://public-api.sevenb.io/api/"

        const val V1 = "v1/"
        const val V2 = "v2/"
    }
}
