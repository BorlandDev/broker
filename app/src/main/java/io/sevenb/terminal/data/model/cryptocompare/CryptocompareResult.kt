package io.sevenb.terminal.data.model.cryptocompare

import com.google.gson.annotations.SerializedName

data class CryptocompareResult(
    @SerializedName("Data")
    val resultData: CryptocompareData
)