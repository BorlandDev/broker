package io.sevenb.terminal.data.model.withdrawal

data class ConfirmWithdrawalParams(
    val withdrawalId: String,
    val code: String
)