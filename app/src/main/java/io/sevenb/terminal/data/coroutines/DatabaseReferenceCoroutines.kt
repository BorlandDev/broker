package io.sevenb.terminal.data.coroutines

import com.google.firebase.FirebaseException
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

@ExperimentalCoroutinesApi
suspend fun DatabaseReference.await(): DataSnapshot? =
//    suspendCancellableCoroutine { continuation ->
//        val listener = object : ValueEventListener {
//            override fun onCancelled(error: DatabaseError) {
//                val exception = when (error.toException()) {
//                    is FirebaseException -> error.toException()
//                    else -> Exception("The Firebase call for reference $this was cancelled")
//                }
//                continuation.resumeWithException(exception)
//            }
//
//            override fun onDataChange(snapshot: DataSnapshot) {
//                continuation.resume(snapshot) { continuation.cancel() }
//            }
//        }
//        continuation.invokeOnCancellation { this.removeEventListener(listener) }
//        this.addListenerForSingleValueEvent(listener)
//    }
    suspendCoroutine { continuation ->
        val listener = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                val exception = when (error.toException()) {
                    is FirebaseException -> error.toException()
                    else -> Exception("The Firebase call for reference $this was cancelled")
                }
                continuation.resumeWithException(exception)
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                continuation.resume(snapshot)
            }
        }
//        continuation.invokeOnCancellation { this.removeEventListener(listener) }
        this.addListenerForSingleValueEvent(listener)
    }