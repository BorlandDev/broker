package io.sevenb.terminal.data.repository.trading

import io.sevenb.terminal.data.datasource.retrofit.BrokerApi
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.order.OrderItem
import io.sevenb.terminal.data.repository.BaseRepository
import io.sevenb.terminal.di.qualifiers.ApiV1
import io.sevenb.terminal.domain.repository.OrdersApi
import javax.inject.Inject

class OrderDataRepository @Inject constructor(
   @ApiV1 private val brokerApi: BrokerApi
) : BaseRepository(), OrdersApi {

    override suspend fun orderOpened(): Result<List<OrderItem>> {
        return safeApiCall({ brokerApi.orderOpened() })
    }

    override suspend fun cancelOrder(symbol: String, orderId: String): Result<OrderItem> {
        return safeApiCall({ brokerApi.cancelOrder(symbol, orderId) })
    }

    override suspend fun ordersHistory(): Result<List<OrderItem>> {
        return safeApiCall({ brokerApi.orderHistory() })
    }
}
