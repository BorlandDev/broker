package io.sevenb.terminal.data.model.broker_api

data class CaptchaPost (
    val status: String,
    val info: String
)