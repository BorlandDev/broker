package io.sevenb.terminal.data.model.messages

data class TemplatePOJO(
    var result: Boolean,
    var response: MutableList<TemplateResponse>
)
