package io.sevenb.terminal.data.model.auth

data class SignUpVerify (
    var token: String = "",
    var code: String = ""
)