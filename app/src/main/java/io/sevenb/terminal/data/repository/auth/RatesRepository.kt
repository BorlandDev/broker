package io.sevenb.terminal.data.repository.auth

import io.sevenb.terminal.data.datasource.retrofit.BrokerApi
import io.sevenb.terminal.data.model.broker_api.rates.MarketRate
import io.sevenb.terminal.data.model.broker_api.rates.MarketRateParams
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.repository.BaseRepository
import io.sevenb.terminal.di.qualifiers.ApiV1
import io.sevenb.terminal.domain.repository.RatesApi
import javax.inject.Inject


class RatesRepository @Inject constructor(
    @ApiV1 private val brokerApi: BrokerApi
) : BaseRepository(), RatesApi {

    override var mapOfRates = mutableMapOf<String, Float>()

    override suspend fun batchEstimates(listMarketRateParams: List<MarketRateParams>): Result<List<MarketRate>> {
        return safeApiCall({ brokerApi.batchMarketsRate(listMarketRateParams) })
    }

    override fun addNewRates(listOfMarketRates: List<MarketRate?>) {
        val localMapOfRates = mutableMapOf<String, Float>()
        if (listOfMarketRates.isNullOrEmpty()) return
        listOfMarketRates.forEach {
            it?.let {
                localMapOfRates["%s%s".format(it.path.first(), it.path.last())] = it.rate ?: 0.0F
            }
        }
        mapOfRates.putAll(localMapOfRates)
    }

}