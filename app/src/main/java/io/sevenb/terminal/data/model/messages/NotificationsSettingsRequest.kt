package io.sevenb.terminal.data.model.messages

data class NotificationsSettingsRequest(
    var template_id: Int? = null,
    var text_type_id: Int? = null,
    var is_enabled: Boolean? = null
)
