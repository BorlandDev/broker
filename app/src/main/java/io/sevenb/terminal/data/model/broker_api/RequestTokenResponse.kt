package io.sevenb.terminal.data.model.broker_api

data class RequestTokenResponse (
    var token: String = "",
    var validUntil: String = ""
)