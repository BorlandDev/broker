package io.sevenb.terminal.data.model.broker_api.account

data class AccountInfo (
    val phone: String,
    val email: String,
    val status: String,
    val userSettings: UserSettings?
)