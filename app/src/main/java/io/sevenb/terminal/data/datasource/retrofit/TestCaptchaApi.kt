package io.sevenb.terminal.data.datasource.retrofit

import io.sevenb.terminal.data.model.broker_api.CaptchaPost
import org.json.JSONObject
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query


interface TestCaptchaApi {

    /**
     * Captcha data API
     */
    @GET("register-slide")
    suspend fun captchaGet(
        @Query("t") time: String
    ): Response<JSONObject>

    @POST("validate-slide")
    suspend fun captchaPost(
        @Query("geetest_validate") validate: String,
        @Query("geetest_seccode") secCode: String,
        @Query("geetest_challenge") challenge: String
    ): Response<CaptchaPost>

    companion object {
        const val STAGE_GEETEST_API_URL = "http://www.geetest.com/demo/gt/"
        const val PROD_GEETEST_API_URL = "https://api.now-broker.com/"
    }
}
