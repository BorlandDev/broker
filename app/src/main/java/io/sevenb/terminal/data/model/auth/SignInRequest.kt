package io.sevenb.terminal.data.model.auth

data class SignInRequest (
    var email: String = "",
    var password: String = ""
)