package io.sevenb.terminal.data.model.broker_api.rates

data class MarketRate(
    var rate: Float?,
    val path: List<String>,
    val symbols: List<String>
)