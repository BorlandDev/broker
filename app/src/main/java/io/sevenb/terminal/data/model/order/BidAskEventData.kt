package io.sevenb.terminal.data.model.order

import com.google.gson.annotations.SerializedName

data class BidAskEventData(
    @SerializedName("stream") val stream: String?,
    @SerializedName("data") val data: BidAskTickerData?
)