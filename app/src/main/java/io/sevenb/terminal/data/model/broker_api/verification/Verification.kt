package io.sevenb.terminal.data.model.broker_api.verification

data class Verification (
    val status: String,
    val createdAt: String,
    val updatedAt: String
)