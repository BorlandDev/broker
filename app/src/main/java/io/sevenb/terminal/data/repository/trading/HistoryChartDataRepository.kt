package io.sevenb.terminal.data.repository.trading

import io.sevenb.terminal.data.datasource.retrofit.CryptocompareService
import io.sevenb.terminal.data.repository.BaseRepository
import io.sevenb.terminal.domain.repository.ChartApi
import javax.inject.Inject

class HistoryChartDataRepository @Inject constructor(
    private val cryptocompareService: CryptocompareService,
) : BaseRepository(), ChartApi {

    override suspend fun getHistoday(from: String, to: String, limit: Int, aggregate: Int, allData: Boolean) =
        safeApiCall({ cryptocompareService.getHistoday(from, to, limit, aggregate, allData) })


    override suspend fun getHourlyPair(from: String, to: String, limit: Int) =
        safeApiCall({ cryptocompareService.getHourlyPair(from, to, limit) })


    companion object {
        const val CRYPTOCOMPARE_API_URL = "https://min-api.cryptocompare.com/"
        private const val API_KEY = "731b02fca406ea8036d02c64fba64c4918d0a141cf878a762adc7e348aeef52b"
    }
}
