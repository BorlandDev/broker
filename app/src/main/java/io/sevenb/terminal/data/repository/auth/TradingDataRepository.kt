package io.sevenb.terminal.data.repository.auth

import io.sevenb.terminal.data.datasource.retrofit.BrokerApi
import io.sevenb.terminal.data.model.broker_api.markets.Currency
import io.sevenb.terminal.data.model.broker_api.markets.Market
import io.sevenb.terminal.data.model.broker_api.rates.MarketRate
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.order.OrderItem
import io.sevenb.terminal.data.model.order.OrderParams
import io.sevenb.terminal.data.model.symbols.SymbolsData
import io.sevenb.terminal.data.repository.BaseRepository
import io.sevenb.terminal.di.qualifiers.ApiV1
import io.sevenb.terminal.domain.repository.MarketApi
import javax.inject.Inject


class TradingDataRepository @Inject constructor(
    @ApiV1 private val brokerApi: BrokerApi
) : BaseRepository(), MarketApi {

    override var listOfCurrencies = mutableListOf<Currency>()
    override var listOfMarkets = mutableListOf<Market>()

    override suspend fun currencies(): Result<List<Currency>> {
        return safeApiCall({ brokerApi.currencies() })
    }

    override suspend fun markets(): Result<List<Market>> {
        return safeApiCall({ brokerApi.markets() })
    }

    override suspend fun orderPost(orderParams: OrderParams): Result<OrderItem> {
        return safeApiCall({ brokerApi.orderPost(orderParams) })
    }

    override suspend fun marketRate(from: String, to: String): Result<MarketRate> {
        return safeApiCall({ brokerApi.marketsRate(from, to) })
    }

    override suspend fun getFilters(symbol: String): Result<SymbolsData> {
        return safeApiCall({ brokerApi.getFilters(symbol) })
    }
}