package io.sevenb.terminal.data.model.enums

enum class LockTimeout {
    MINUTE,
    FIVE_MINUTES,
    HALF_HOUR,
    HOUR,
    REBOOT,
    NOTHING
}