package io.sevenb.terminal.data.model.trading

data class TradingListItem (
    val timestamp: Long,
    val hash: String,
    val confirmations: String,
    val amount: Float,
    val from: String,
    val to: String
)