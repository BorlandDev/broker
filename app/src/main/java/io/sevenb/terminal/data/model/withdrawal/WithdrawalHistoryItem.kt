package io.sevenb.terminal.data.model.withdrawal

data class WithdrawalHistoryItem(
    val withdrawalId: String,
    val ticker: String,
    val network: String,
    val address: String,
    val extraId: String,
    val amount: Float,
    val fee: Float,
    val status: String,
    var txId: String?,
    val createdAt: String,
    val updatedAt: String,
    val submittedAt: String
)
