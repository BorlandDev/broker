package io.sevenb.terminal.data.model.enums.auth

import io.sevenb.terminal.data.model.enums.CommonState

enum class PasswordChangeStates : CommonState {
    PASSWORD_CHANGE_VERIFICATION_CODE,
    PASSWORD_CHANGE_NEW_PASSWORD
}