package io.sevenb.terminal.data.repository.database

import androidx.lifecycle.LiveData
import io.sevenb.terminal.data.datasource.database.dao.CurrencyDao
import io.sevenb.terminal.data.datasource.database.model.CurrencyEntity
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.domain.repository.DbApi
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class DbRepository @Inject constructor(
    private val currencyDao: CurrencyDao
) : DbApi {
    override fun getAll(): LiveData<List<CurrencyEntity>> {
        return currencyDao.getAll()
    }

    override fun getAllFlow(): Flow<List<CurrencyEntity>> {
        return currencyDao.getAllFlow()
    }

    override fun getAllResult(): List<CurrencyEntity> {
        return currencyDao.getAllResult()
    }

    override fun getCurrency(ticker: String): Flow<CurrencyEntity> {
        return currencyDao.getCurrency(ticker)
    }

    override suspend fun deleteAll() {
        currencyDao.deleteAll()
    }

    override suspend fun insertAll(vararg currencies: CurrencyEntity) {
        currencyDao.insertAll(*currencies)
    }

    override suspend fun delete(currency: CurrencyEntity) {
        currencyDao.delete(currency)
    }

    override suspend fun update(currency: CurrencyEntity) {
        currencyDao.update(currency)
    }

}