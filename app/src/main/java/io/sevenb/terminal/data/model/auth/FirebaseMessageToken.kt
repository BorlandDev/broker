package io.sevenb.terminal.data.model.auth

data class FirebaseMessageToken(
    var registration_token: String = ""
)