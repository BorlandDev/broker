package io.sevenb.terminal.data.model.candles

data class CandleFields(
    val openTime: Double?,
    val open: String?,
    val high: String?,
    val low: String?,
    val close: String?,
    val volume: String?,
    val closeTime: Double?,
    val quoteAssetVolume: String?,
    val numberOfTrades: Int?,
    val takerBuyBaseAssetVolume: String?,
    val takerBuyQuoteAssetVolume: String?
)