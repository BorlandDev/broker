package io.sevenb.terminal.data.datasource.retrofit

import io.sevenb.terminal.data.model.cryptocompare.CryptocompareResult
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface CryptocompareService {

    @GET("data/v2/histoday")
    suspend fun getHistoday(
        @Query("fsym") from : String,
        @Query("tsym") to : String = "USD",
        @Query("limit") limit : Int = 7,
        @Query("aggregate") aggregate : Int = 1,
        @Query("allData") allData : Boolean = false
    ): Response<CryptocompareResult>

    @GET("data/v2/histohour")
    suspend fun getHourlyPair(
        @Query("fsym") from : String,
        @Query("tsym") to : String = "USD",
        @Query("limit") limit : Int = 7,
        @Query("aggregate") aggregate : Int = 1
    ): Response<CryptocompareResult>

    //https://min-api.cryptocompare.com/data/histoday?fsym=BCH&limit=7&tsym=USD

}
