package io.sevenb.terminal.data.model.enums

enum class AuthState {
    NEW,
    SMS_CODE,
    SIGN_IN,
    CREATING_ACCOUNT,
    ACCOUNT_DETAILS,
    CREATING_PASSWORD,
    SMS_CODE_RESTORE_PASSWORD,
    ENTERED,
    REGISTRATION_STATE,
    PASSWORD_RESTORE_STATE,
    LOGIN_STATE
}