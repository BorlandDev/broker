package io.sevenb.terminal.data.model.broker_api.account

data class AccountStatus (
    val status: String
)