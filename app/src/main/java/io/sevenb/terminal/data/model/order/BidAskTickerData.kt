package io.sevenb.terminal.data.model.order

import com.google.gson.annotations.SerializedName

data class BidAskTickerData(
    @SerializedName("u") val updateId: String?,
    @SerializedName("s") val symbol: String?,
    @SerializedName("b") val bidPrice: String?,
    @SerializedName("B") val bidQty: String?,
    @SerializedName("a") val askPrice: String?,
    @SerializedName("A") val askQty: String?
)