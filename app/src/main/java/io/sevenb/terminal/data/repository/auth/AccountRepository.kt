package io.sevenb.terminal.data.repository.auth

import io.sevenb.terminal.data.datasource.retrofit.BrokerApi
import io.sevenb.terminal.data.model.ProfitLoss
import io.sevenb.terminal.data.model.broker_api.account.*
import io.sevenb.terminal.data.model.broker_api.markets.AccountBalance
import io.sevenb.terminal.data.model.broker_api.verification.Verification
import io.sevenb.terminal.data.model.broker_api.verification.VerificationToken
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.portfolio.LocalAccountData
import io.sevenb.terminal.data.model.withdrawal.ConfirmWithdrawalParams
import io.sevenb.terminal.data.model.withdrawal.WithdrawalHistoryItem
import io.sevenb.terminal.data.model.withdrawal.WithdrawalRequest
import io.sevenb.terminal.data.repository.BaseRepository
import io.sevenb.terminal.di.qualifiers.ApiV1
import io.sevenb.terminal.domain.repository.AccountApi
import io.sevenb.terminal.ui.screens.select_currency.DepositCurrencyItem
import javax.inject.Inject


class AccountRepository @Inject constructor(
    @ApiV1 private val brokerApi: BrokerApi
) : BaseRepository(), AccountApi {

    override var localAccountData = LocalAccountData()
    override var listOfBalances = listOf<AccountBalance>()

    override suspend fun accountBalances(): Result<List<AccountBalance>> {
        return safeApiCall({ brokerApi.accountBalances() })
    }

    override suspend fun depositAddress(currency: DepositCurrencyItem): Result<DepositAddress> {
        return safeApiCall({ brokerApi.depositAddress(currency.ticker, currency.network) })
    }

    override suspend fun depositHistory(): Result<List<DepositHistoryItem>> {
        return safeApiCall({ brokerApi.depositHistory() })
    }

    override suspend fun requestWithdrawal(withdrawalRequest: WithdrawalRequest): Result<WithdrawalHistoryItem> {
        return safeApiCall({ brokerApi.withdrawalRequest(withdrawalRequest) })
    }

    override suspend fun confirmWithdrawal(confirmWithdrawalParams: ConfirmWithdrawalParams): Result<Unit> {
        return safeApiCall({ brokerApi.withdrawalConfirm(confirmWithdrawalParams) })
    }

    override suspend fun withdrawalHash(id: String): Result<WithdrawalHistoryItem> {
        return safeApiCall({ brokerApi.withdrawalHash(id) })
    }

    override suspend fun withdrawalHistory(): Result<List<WithdrawalRequest>> {
        return safeApiCall({ brokerApi.withdrawalHistory() })
    }

    override suspend fun accountStatus(): Result<AccountStatus> {
        return safeApiCall({ brokerApi.accountStatus() })
    }

    override suspend fun accountInfo(): Result<AccountInfo> {
        return safeApiCall({ brokerApi.accountInfo() })
    }

    override suspend fun getVerifications(): Result<List<Verification>> {
        return safeApiCall({ brokerApi.getVerifications() })
    }

    override suspend fun createVerification(): Result<VerificationToken> {
        return safeApiCall({ brokerApi.verification() })
    }

    override suspend fun refreshVerificationToken(): Result<VerificationToken> {
        return safeApiCall({ brokerApi.refreshVerificationToken() })
    }

    override suspend fun availableWithdrawal(): Result<AvailableWithdrawal> {
        return safeApiCall({ brokerApi.availableWithdrawal() })
    }

    override suspend fun profitLoss(): Result<ProfitLoss> {
        return safeApiCall({ brokerApi.profitLoss() })
    }

}