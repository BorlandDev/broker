package io.sevenb.terminal.data.model.enums

enum class PortfolioTabs {
    HISTORY,
    ORDERS,
    ASSETS
}