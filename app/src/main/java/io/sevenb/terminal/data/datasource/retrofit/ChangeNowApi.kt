package io.sevenb.terminal.data.datasource.retrofit

import io.sevenb.terminal.data.model.transactionexplorer.TransactionExplorerMask
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface ChangeNowApi {

    @GET("{currency}")
    suspend fun getTransactionMask(@Path("currency") currency: String): Response<TransactionExplorerMask>

    companion object {
        const val CHANGE_NOW_API_URL = "https://api.changenow.io/v1/currencies/"
    }
}