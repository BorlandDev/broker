package io.sevenb.terminal.data.model.enum_model

enum class OrderFieldError {
    FIRST_FIELD,
    THIRD_FIELD
}