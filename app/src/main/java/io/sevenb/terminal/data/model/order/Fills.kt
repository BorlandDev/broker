package io.sevenb.terminal.data.model.order

data class Fills(
    val price: String,
    val qty: String,
    val commission: String,
    val commissionAsset: String,
    val tradeId: Int
)
