package io.sevenb.terminal.data.model.enums.pass_code

import io.sevenb.terminal.data.model.enums.CommonState

enum class PassCodeSubStates : CommonState {
    PASS_CODE_ENTERING_PASSED,
    PASS_CODE_ENTERING_ERROR,
    PASS_CODE_CONFIRMING_ERROR,
    PASS_CODE_CONFIRMING_PASSED,
    PASS_CODE_ENTERING_FAILED,
    PASS_CODE_CONFIRMING_FAILED,
    SKIPPED,
    LOG_OUT
}