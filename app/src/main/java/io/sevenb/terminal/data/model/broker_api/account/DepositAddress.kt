package io.sevenb.terminal.data.model.broker_api.account

data class DepositAddress (
    val ticker: String,
    val address: String,
    val extraId: String
)