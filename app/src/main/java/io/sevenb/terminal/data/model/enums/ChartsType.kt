package io.sevenb.terminal.data.model.enums

enum class ChartsType {
    PORTFOLIO,
    MARKETS
}