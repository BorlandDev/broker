package io.sevenb.terminal.data.model.enums.notifications

enum class NotificationsTypeFromBack {
    ORDER_COMPLETE,
    DEPOSIT_COMPLETE,
    WITHDRAW_COMPLETE
}