package io.sevenb.terminal.data.datasource.database.db

import androidx.room.Database
import androidx.room.RoomDatabase
import io.sevenb.terminal.data.datasource.database.model.CurrencyEntity
import io.sevenb.terminal.data.datasource.database.dao.CurrencyDao

@Database(entities = [CurrencyEntity::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun currencyDao(): CurrencyDao
}