package io.sevenb.terminal.data.model.enum_model

enum class TimeInForceType {
    GTC, IOC, FOK
}
