package io.sevenb.terminal.data.model.enums

enum class GeeTestResult {
    SUCCESS,
    FAIL,
    USER_EXISTS,
    PARSE_JSON_FAIL,
    BAD_PARAMS,
    TOO_MANY_ATTEMPTS,
    RESEND_FAIL,
    CONFIRM_FAIL
}