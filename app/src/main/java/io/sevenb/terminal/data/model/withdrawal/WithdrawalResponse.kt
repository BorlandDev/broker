package io.sevenb.terminal.data.model.withdrawal

data class WithdrawalResponse(
    val requestId: String
)