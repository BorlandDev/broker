package io.sevenb.terminal.data.model.core

//TODO REFACTOR
sealed class Result<out T : Any> {
    data class Success<out T : Any>(val data: T) : Result<T>()
    data class Error(
        val exception: Exception,
        val message: String = "",
        val error: String = ""
    ) : Result<Nothing>()
}