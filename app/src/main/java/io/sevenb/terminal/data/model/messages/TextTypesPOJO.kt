package io.sevenb.terminal.data.model.messages

data class TextTypesPOJO(
    var result: Boolean,
    var response: MutableList<TextTypesResponse>
)