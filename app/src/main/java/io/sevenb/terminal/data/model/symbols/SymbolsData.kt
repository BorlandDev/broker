package io.sevenb.terminal.data.model.symbols

import io.sevenb.terminal.data.model.filter.Filters

data class SymbolsData(
    val symbol: String?,
    val status: String?,
    val baseAsset: String?,
    val baseAssetPrecision: Int?,
    val quoteAsset: String?,
    val quotePrecision: Int?,
    val quoteAssetPrecision: Int?,
    val baseCommissionPrecision: Int?,
    val quoteCommissionPrecision: Int?,
    val icebergAllowed: Boolean?,
    val ocoAllowed: Boolean?,
    val quoteOrderQtyMarketAllowed: Boolean?,
    val isSpotTradingAllowed: Boolean?,
    val isMarginTradingAllowed: Boolean?,
    val filters: List<Filters>?,
    val orderTypes: List<String>?,
    val permissions: List<String>?,
    val ask: String?,
    val bid: String?,
)