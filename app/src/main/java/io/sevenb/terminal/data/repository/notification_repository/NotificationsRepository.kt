package io.sevenb.terminal.data.repository.notification_repository

import io.sevenb.terminal.data.datasource.retrofit.BrokerApi
import io.sevenb.terminal.data.model.auth.ResponseBoolean
import io.sevenb.terminal.data.model.broker_api.CreatePriceAlert
import io.sevenb.terminal.data.model.broker_api.PriceAlert
import io.sevenb.terminal.data.model.broker_api.PriceAlertGet
import io.sevenb.terminal.data.model.broker_api.PriceAlertGetApi
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.enums.notifications.NotificationsType
import io.sevenb.terminal.data.model.messages.*
import io.sevenb.terminal.data.model.settings.SettingsPOJO
import io.sevenb.terminal.data.repository.BaseRepository
import io.sevenb.terminal.di.qualifiers.ApiV1
import io.sevenb.terminal.di.qualifiers.ApiV2
import io.sevenb.terminal.domain.repository.NotificationApi
import javax.inject.Inject

class NotificationsRepository @Inject constructor(
    @ApiV1 private val brokerApiV1: BrokerApi,
    @ApiV2 private val brokerApiV2: BrokerApi,
) : BaseRepository(), NotificationApi {
    override suspend fun storeEnableNotifications(value: Boolean): Result<ResponseBoolean> {
        return Result.Success(ResponseBoolean(true))//safeApiCall({  })
    }

    override suspend fun restoreEnableNotifications(): Result<ResponseBoolean> {
        return Result.Success(ResponseBoolean(true))//safeApiCall({  })
    }

    override suspend fun storeEnabledNotifications(vararg notificationType: NotificationsType): Result<ResponseBoolean> {
        return Result.Success(ResponseBoolean(true))//safeApiCall({  })
    }

    override suspend fun restoreEnabledNotifications(): Result<List<NotificationsType>> {
        return Result.Success(
            listOf(
                NotificationsType.WITHDRAW, NotificationsType.DEPOSIT, NotificationsType.WITHDRAW
            )
        )
    }

    override suspend fun testTextTypes(): Result<TextTypesPOJO> {
        return safeApiCall({ brokerApiV1.getTextTypes() })
    }

        override suspend fun getUserSettings(): Result<SettingsPOJO> {
//    override suspend fun <T : Parcelable> getUserSettings(): Result<BaseResponse<T>> {
        return safeApiCall({ brokerApiV2.getUserSettings() })
    }

    override suspend fun getPriceAlert(): Result<PriceAlertGetApi> {
//    override suspend fun <T : Parcelable> getUserSettings(): Result<BaseResponse<T>> {
        return safeApiCall({ brokerApiV1.getAlert() })
    }

    override suspend fun createPriceAlert(bodyAlert: PriceAlert): Result<CreatePriceAlert> {
//    override suspend fun <T : Parcelable> getUserSettings(): Result<BaseResponse<T>> {
        return safeApiCall({ brokerApiV1.postAlert(bodyAlert) })
    }

    override suspend fun deletePriceAlert(id: String): Result<PriceAlert> {
//    override suspend fun <T : Parcelable> getUserSettings(): Result<BaseResponse<T>> {
        var res = brokerApiV1.deleteAlert(id)
        return safeApiCall({ res })
    }

    override suspend fun put(request: List<NotificationsSettingsRequest>): Result<ResponseBoolean> {
        return safeApiCall({ brokerApiV2.putSettings(request) })
    }

    override suspend fun sendMessage(sendMessageRequest: SendMessageRequest): Result<FirebaseMessageResponse> {
        return safeApiCall({ brokerApiV1.sendNotification(sendMessageRequest) })
    }
}