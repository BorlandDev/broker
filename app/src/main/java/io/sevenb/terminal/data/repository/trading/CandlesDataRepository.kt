package io.sevenb.terminal.data.repository.trading

import io.sevenb.terminal.data.datasource.retrofit.BrokerApi
import io.sevenb.terminal.data.model.candles.CandlesData
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.trading.CandleChartParams
import io.sevenb.terminal.data.repository.BaseRepository
import io.sevenb.terminal.di.qualifiers.ApiV1
import io.sevenb.terminal.domain.repository.CandlesApi
import javax.inject.Inject

class CandlesDataRepository @Inject constructor(
   @ApiV1 private val brokerApi: BrokerApi
) : BaseRepository(), CandlesApi {

    override suspend fun getCandle(candleChartParams: CandleChartParams): Result<CandlesData> {
        return safeApiCall({
            brokerApi.getCandles(
                candleChartParams.symbol,
                candleChartParams.interval,
                candleChartParams.startTime,
                candleChartParams.endTime,
                candleChartParams.limit
            )
        })
    }
}