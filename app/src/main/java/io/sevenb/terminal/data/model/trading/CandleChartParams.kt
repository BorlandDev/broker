package io.sevenb.terminal.data.model.trading

data class CandleChartParams(
    val listPosition: Int,
    val tradeChartRange: TradeChartRange,
    var limit: Int = 500,
    var interval: String = "",
    var symbol: String = "",
    var startTime: String = "",
    var endTime: String = ""
) : BaseTrade()