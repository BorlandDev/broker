package io.sevenb.terminal.data.model.enums.auth

import io.sevenb.terminal.data.model.enums.CommonState

enum class TfaState : CommonState {
    INFORMATION,
    SECRET,
    VERIFICATION,
    VERIFICATION_ONLY,
    IS_ON,
    DONE,
}