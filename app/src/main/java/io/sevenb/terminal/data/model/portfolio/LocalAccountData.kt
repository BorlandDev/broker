package io.sevenb.terminal.data.model.portfolio

data class LocalAccountData(
    var phone: String = "",
    var email: String = "",
    var totalHoldingBalance: String = "",
    var availableBalance: String = "",
    var onOrdersBalance: String = "",
    var balance: Float? = null,
    var updateTime: Long = 0L,
    var verificationToken: String = "",
    var totalInBaseCurrency: String = "",
    var status: String = "",
    var tfa: Boolean? = false
)