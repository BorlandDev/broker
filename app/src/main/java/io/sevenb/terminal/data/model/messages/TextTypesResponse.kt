package io.sevenb.terminal.data.model.messages

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TextTypesResponse(
    var id: Int,
    var text_type: String
) : Parcelable
