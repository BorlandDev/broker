package io.sevenb.terminal.data.model.logs

import com.google.firebase.firestore.PropertyName

data class Log(
    @get:PropertyName("class_name")
    @set:PropertyName("class_name")
    @PropertyName("class_name")
    var className: String,
    @get:PropertyName("method_name")
    @set:PropertyName("method_name")
    @PropertyName("method_name")
    var methodName: String?,
    @get:PropertyName("data")
    @set:PropertyName("data")
    @PropertyName("data")
    var data: String?,
    @get:PropertyName("created")
    @set:PropertyName("created")
    @PropertyName("created")
    var created: Long,
    @get:PropertyName("error")
    @set:PropertyName("error")
    @PropertyName("error")
    var error: String?,
)
