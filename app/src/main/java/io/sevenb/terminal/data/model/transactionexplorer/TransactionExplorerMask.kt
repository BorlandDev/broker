package io.sevenb.terminal.data.model.transactionexplorer

data class TransactionExplorerMask(
    val transactionExplorerMask: String
)