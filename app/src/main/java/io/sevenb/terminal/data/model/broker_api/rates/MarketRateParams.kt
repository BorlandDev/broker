package io.sevenb.terminal.data.model.broker_api.rates

data class MarketRateParams(
    val from: String,
    val to: String
)