package io.sevenb.terminal.data.model.enums

enum class RateUsStates {
    LEAVE_IT,
    ASK_LATER,
    NO,
    YES,
    YES_GOOGLE_PLAY,
}