package io.sevenb.terminal.data.model.messages

import io.sevenb.terminal.data.model.broker_api.account.UserSettings

data class UserSettingsPOJO(
    var user_settings: UserSettings
)
