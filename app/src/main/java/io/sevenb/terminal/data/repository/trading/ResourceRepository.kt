package io.sevenb.terminal.data.repository.trading

import com.google.gson.reflect.TypeToken
import io.sevenb.terminal.R
import io.sevenb.terminal.device.provider.FileProvider
import io.sevenb.terminal.device.utils.Serializer
import io.sevenb.terminal.domain.repository.RawDataRepository
import kotlinx.coroutines.runBlocking
import java.lang.reflect.Type


class ResourceRepository(
    private val fileProvider: FileProvider
) : RawDataRepository {

    override var mapTickerToCmcIconId: Map<String, Int>? = null

    init {
        initMapTickerToCmcIconId()
    }

    private fun initMapTickerToCmcIconId() {
        runBlocking {
            val fileString = mapTickerCmcIconId()
            val type: Type = object : TypeToken<Map<String?, Int?>?>() {}.type
            mapTickerToCmcIconId = Serializer.deserializeType(fileString, type)
        }
    }

    override suspend fun mapTickerCmcIconId(): String = fileProvider.getFileStringFromRawRes(R.raw.tickerid)

}