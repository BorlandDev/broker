package io.sevenb.terminal.data.model.tfa

data class TfaResponse(
    var id: String,
    var secret: String,
    val created_at: String,
    val updated_at: String,
    var status: Boolean
)