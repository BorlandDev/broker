package io.sevenb.terminal.exceptions

import android.provider.Settings
import io.sevenb.terminal.data.model.core.Result;
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

sealed class Try<T> {

    companion object {
       operator fun <T : Any> invoke(func: () -> T): Result<T> =
            try {
                Result.Success(func.invoke())
            } catch (error: Exception) {
                Result.Error(error, error.message.toString())
            }
    }

}
