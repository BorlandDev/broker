package io.sevenb.terminal.exceptions

import io.sevenb.terminal.data.model.enum_model.ErrorMessageType
import java.lang.Exception

//TODO ADD CUSTOM EXCEPTIONS
class NewAuthException(var messageType: ErrorMessageType) : Exception() {

    override val message: String
        get() =
            when (messageType) {
                ErrorMessageType.LOG_OUT -> "Log out error"
                ErrorMessageType.FIREBASE_TOKEN -> "Messaging token error"
                ErrorMessageType.TFA_VERIFICATION -> "Wrong verification code"
//                ErrorMessageType.LOGIN_BAD_PARAMS -> "Messaging token error"
                else -> "Unknown authentication error"
            }

}