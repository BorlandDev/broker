package io.sevenb.terminal.utils

import io.sevenb.terminal.BuildConfig

abstract class ConstUtils {
    companion object {
        const val GOOGLE_PLAY_LINK = "market://details?id=${BuildConfig.APPLICATION_ID}"
        const val GOOGLE_PLAY_LINK_WEB =
            "https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}"
        const val CANT_GET_MARKET_LIST = "Can't get markets list";

        const val DAYS_10 = (1000 * 60 * 60 * 24) * 10L
        const val DAYS_30 = (1000 * 60 * 60 * 24) * 30L

    }
}