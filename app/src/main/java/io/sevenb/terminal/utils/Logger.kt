package io.sevenb.terminal.utils

//import com.google.firebase.database.DatabaseReference
//import com.google.firebase.database.FirebaseDatabase
//import com.google.firebase.database.ktx.database
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import io.sevenb.terminal.data.coroutines.await
import io.sevenb.terminal.data.model.logs.Log
import io.sevenb.terminal.data.model.logs.Profile
import kotlinx.coroutines.*
import timber.log.Timber
import java.lang.Exception
import java.util.*
import kotlin.reflect.KClass

/**
 * 1) init with user email
 * 2) send log of Log data model
 * 3) check if user's profile exists on Firestore
 * 4) set if not exists and add log
 * 5) if exists add log and update last log time in user's profile
 */
//@ExperimentalCoroutinesApi
class Logger {

//    @ExperimentalCoroutinesApi
    companion object {

        private const val COLLECTION_PROFILES = "profiles"
        private const val COLLECTION_LOGS = "logs"
        private const val FIELD_LAST_LOG_SINCE = "last_log_since"

        private const val NO_DATA = "NO_DATA"
        private const val NO_ERROR = "NO_ERROR"
        private const val UNKNOWN_ERROR_MESSAGE = "Unknown error message"

        private var userId: String? = null
        private var isUserExists: Boolean? = null
        private var sendToFirestore: Boolean = true

        fun init(userId: String? = null) {
            userId?.let {
                this.userId = userId
            }
        }

        @JvmStatic
        fun sendLog(
            className: String,
            methodName: String,
            data: String?
        ) {
            sendLog(className, methodName, data, NO_ERROR)
        }

        @JvmStatic
        fun sendLog(
            className: String,
            methodName: String,
            exception: Exception?
        ) {
            sendLog(className, methodName, NO_DATA, exception?.message ?: UNKNOWN_ERROR_MESSAGE)
        }

//        @JvmStatic
//        fun sendLog(
//            className: String,
//            methodName: String,
//            data: String? = null,
//            error: String? = null
//        ) {
//            GlobalScope.launch {
//                if (!userId.isNullOrEmpty()) {
//                    val log = getFormattedLog(className, methodName, data, error)
//
//                    if (log.error == NO_ERROR) {
//                        Timber.d(log.toString())
//                    } else {
//                        Timber.e(log.toString())
//                    }
//
//                    if (sendToFirestore) {
////                        val userExists = isUserExists ?: isUserExists()
//                        val profile = Firebase.database.reference
//                            .child(COLLECTION_PROFILES)
//                            .child("test@gmailcom")
//                            .get()
//                            .await()
//                            .getValue(Profile::class.java)
//
//
////                            FirebaseDatabase
////                                .getInstance()
////                                .getReference(COLLECTION_PROFILES + userId!!)
////                                .await()
////                                .getValue(Profile::class.java)
//
//                        if (profile != null) {
//                            Timber.d(log.toString())
////                            updateLastLogSince(log.created)
////                            addLog(log)
//                        } else {
//                            Timber.d(log.toString())
//                            val profile = getFormattedProfile(log.created)
//                            setProfile(profile)
////                            addLog(log)
//                        }
//                }
//                }
//            }
//        }

        //TODO MIGRATE TO REALTIME / REFACTOR
        @JvmStatic
        fun sendLog(
            className: String,
            methodName: String,
            data: String? = null,
            error: String? = null
        ) {
            try {
                if (!userId.isNullOrEmpty()) {
                    val log = getFormattedLog(className, methodName, data, error)

                    Timber.d(log.toString())

                    val firebaseFirestore = FirebaseFirestore.getInstance()

                    firebaseFirestore
                        .collection("profiles")
                        .document(userId!!)
                        .get()
                        .addOnSuccessListener {
                            if (it.exists()) {
                                firebaseFirestore
                                    .collection("profiles")
                                    .document(userId!!)
                                    .update("last_log_since", System.currentTimeMillis())
                                    .addOnSuccessListener {
                                        firebaseFirestore
                                            .collection("profiles")
                                            .document(userId!!)
                                            .collection("logs")
                                            .add(log)
                                    }
                            } else {
                                firebaseFirestore
                                    .collection("profiles")
                                    .document(userId!!)
                                    .set(
                                        mapOf(
                                            "last_log_since" to System.currentTimeMillis(),
                                            "email" to userId!!,
                                            "manufacturer" to android.os.Build.MANUFACTURER,
                                            "model" to android.os.Build.MODEL,
                                        )
                                    ).addOnSuccessListener {
                                        firebaseFirestore
                                            .collection("profiles")
                                            .document(userId!!)
                                            .collection("logs")
                                            .add(log)
                                    }
                            }
                        }
                }
            } catch (ex: Exception) {

            }
        }


//        private suspend fun isUserExists(): Boolean {
//            val profile = FirebaseFirestore
//                .getInstance()
//                .collection(COLLECTION_PROFILES)
//                .document(userId!!)
//                .get()
//                .await()
//                .toObject(Profile::class.java)
//            return profile != null && userId == profile.email
//        }

//        private suspend fun isUserExists(): Boolean =
//            withContext(Dispatchers.IO) {
//                val profile =
//                    FirebaseDatabase
//                        .getInstance()
//                        .getReference(COLLECTION_PROFILES + userId!!)
//                        .await()
//                        ?.getValue(Profile::class.java)
//                return@withContext profile != null && userId == profile.email
//            }

        private suspend fun updateLastLogSince(lastLogSins: Long) {
            FirebaseFirestore
                .getInstance()
                .collection(COLLECTION_PROFILES)
                .document(userId!!)
                .update(FIELD_LAST_LOG_SINCE, lastLogSins)
                .await()
        }

//        private suspend fun setProfile(profile: Profile) {
//            FirebaseFirestore
//                .getInstance()
//                .collection(COLLECTION_PROFILES)
//                .document(userId!!)
//                .set(profile)
//                .await()
//            isUserExists = true
//        }

//        private suspend fun setProfile(profile: Profile) {
//            FirebaseDatabase
//                .getInstance()
//                .getReference(COLLECTION_PROFILES + userId!!)
//                .setValue(profile)
//                .await()
//
////            FirebaseFirestore
////                .getInstance()
////                .collection(COLLECTION_PROFILES)
////                .document(userId!!)
////                .set(profile)
////                .await()
//            isUserExists = true
//        }

        private suspend fun addLog(log: Log) {
            FirebaseFirestore
                .getInstance()
                .collection(COLLECTION_PROFILES)
                .document(userId!!)
                .collection(COLLECTION_LOGS)
                .add(log)
                .await()
        }

        private fun getFormattedLog(
            className: String,
            methodName: String,
            data: String?,
            error: String?
        ) = Log(
            className = className,
            methodName = methodName,
            data = data ?: NO_DATA,
            error = error ?: NO_ERROR,
            created = System.currentTimeMillis()
        )

        private fun getFormattedProfile(
            lastLogSins: Long,
        ) = Profile(
            lastLogSince = lastLogSins,
            email = userId!!,
            manufacturer = android.os.Build.MANUFACTURER,
            model = android.os.Build.MODEL
        )


        @JvmStatic
        fun sendLogFeedback(
            pData: String?
        ) {

            val firebaseFirestore = FirebaseFirestore.getInstance()

            val currentData = Date().time.toString()


            firebaseFirestore
                .collection("bad_feedback")
                .document(currentData)
                .set(
                    mapOf(
                        "last_log_since" to System.currentTimeMillis(),
                        "feedback" to pData,
                        "manufacturer" to android.os.Build.MANUFACTURER,
                        "model" to android.os.Build.MODEL,
                    )
                )

        }


        @JvmStatic
        fun sendLogRateUs(
            pClass: KClass<*>,
            pMethod: String,
            pData: String?,
        ) {

            val log = Log(
                className = pClass.simpleName!!,
                methodName = pMethod,
                data = pData,
                created = System.currentTimeMillis(),
                error = NO_ERROR
            )
            Timber.d(log.toString())

            val firebaseFirestore = FirebaseFirestore.getInstance()
            val currentData = Date().time.toString()

            firebaseFirestore
                .collection("rate_us")
                .document(currentData)
                .set(
                    mapOf(
                        "last_log_since" to System.currentTimeMillis(),
                        "isSuccess" to pData,
                        "manufacturer" to android.os.Build.MANUFACTURER,
                        "model" to android.os.Build.MODEL,
                    )
                )

        }


    }
}