package io.sevenb.terminal.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Build
import androidx.lifecycle.LiveData

class ConnectionStateMonitor(context: Context) : LiveData<Boolean>() {

    private var networkCallback: ConnectivityManager.NetworkCallback
    private var connectivityManager: ConnectivityManager =
        context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    init {
        networkCallback = NetworkCallback()
    }

    override fun onActive() {
        super.onActive()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            connectivityManager.registerDefaultNetworkCallback(networkCallback)
        }

        val networkRequest: NetworkRequest = NetworkRequest.Builder()
            .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
            .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
            .build()

        connectivityManager.registerNetworkCallback(networkRequest, networkCallback)
    }

    override fun onInactive() {
        super.onInactive()
        connectivityManager.unregisterNetworkCallback(networkCallback)
    }

    companion object {
        var connectionEnabled = true
        const val noInternetError = "No internet connection"
    }

    inner class NetworkCallback :
        ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            connectionEnabled = true
        }

        override fun onLost(network: Network) {
            connectionEnabled = false
        }

        override fun onLosing(network: Network, maxMsToLive: Int) {
            connectionEnabled = false
        }

        override fun onUnavailable() {
            connectionEnabled = false
        }
    }
}