package io.sevenb.terminal.ui.screens.portfolio

import android.view.View
import androidx.recyclerview.widget.RecyclerView

class HistoryItemViewHolder(
    itemView: View,
) : RecyclerView.ViewHolder(itemView) {

    fun bind(baseHistoryItem: BaseHistoryItem, copyHash: (String) -> Unit, onClickHistory: (PortfolioHistoryItem) -> Unit) {

        when(baseHistoryItem) {
            is PortfolioHistoryItem -> {
                baseHistoryItem.bind(itemView, copyHash, onClickHistory)
            }
        }

    }

}
