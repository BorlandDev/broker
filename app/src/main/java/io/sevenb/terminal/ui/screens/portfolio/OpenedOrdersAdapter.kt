package io.sevenb.terminal.ui.screens.portfolio

import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import androidx.recyclerview.widget.RecyclerView
import io.sevenb.terminal.R
import io.sevenb.terminal.data.model.order.OrderItem
import kotlinx.android.synthetic.main.item_open_order_list.view.*

class OpenedOrdersAdapter(
    private val deleteItem: (BaseOrderListItem) -> Unit
) :
    RecyclerView.Adapter<OpenOrderItemViewHolder>() {

    var baseItemsList: MutableList<BaseOrderListItem> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OpenOrderItemViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_open_order_list, parent, false)
        return OpenOrderItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: OpenOrderItemViewHolder, position: Int) {
        val item = baseItemsList[position]
        holder.bind(item)
        holder.itemView.bottom_wrapper.setOnClickListener {
            val anim: Animation = AlphaAnimation(0.0f, 1.0f)
            anim.duration = 400

            anim.startOffset = 20
            anim.repeatMode = Animation.REVERSE
            anim.repeatCount = Animation.INFINITE
            holder.itemView.tv_cancel_order.startAnimation(anim)

            deleteItem(item)
        }
    }

    fun submitList(list: List<OrderItem>) {
        baseItemsList = list.toMutableList()
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = baseItemsList.size

}
