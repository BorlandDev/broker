package io.sevenb.terminal.ui.screens.portfolio

import android.view.View

interface BaseAssetListItem {

    fun bind(itemView: View)

}