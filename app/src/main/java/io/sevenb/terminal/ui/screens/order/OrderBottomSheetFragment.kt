package io.sevenb.terminal.ui.screens.order

import android.content.Intent
import android.graphics.Paint
import android.graphics.Point
import android.net.Uri
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.Display
import android.view.View
import android.widget.TextView
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import androidx.core.widget.doOnTextChanged
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.google.android.play.core.review.ReviewManagerFactory
import io.sevenb.terminal.AppActivity
import io.sevenb.terminal.R
import io.sevenb.terminal.data.datasource.PreferenceStorage
import io.sevenb.terminal.data.datasource.SharedPreferenceStorage
import io.sevenb.terminal.data.model.broker_api.markets.Market
import io.sevenb.terminal.data.model.broker_api.rates.MarketRate
import io.sevenb.terminal.data.model.enum_model.ErrorMessageType
import io.sevenb.terminal.data.model.enum_model.OrderFieldError
import io.sevenb.terminal.data.model.enum_model.OrderType
import io.sevenb.terminal.data.model.enums.OrderFieldPosition
import io.sevenb.terminal.data.model.order.OrderItem
import io.sevenb.terminal.data.model.order.ReceivedRate
import io.sevenb.terminal.data.model.withdrawal.WithdrawItem
import io.sevenb.terminal.data.repository.auth.PreferenceRepository
import io.sevenb.terminal.device.utils.DateTimeUtil.dateFromTimestamp
import io.sevenb.terminal.device.utils.DateTimeUtil.parseToTimestamp
import io.sevenb.terminal.device.utils.DateTimeUtil.timeFromTimestamp
import io.sevenb.terminal.domain.repository.StorageRepository
import io.sevenb.terminal.domain.usecase.charts.AccountData
import io.sevenb.terminal.domain.usecase.charts.AccountData.Companion.getBaseCurrencyCharacter
import io.sevenb.terminal.ui.base.BaseBottomSheetFragment
import io.sevenb.terminal.ui.extension.*
import io.sevenb.terminal.ui.screens.dialog_rate_us.DialogThankYou
import io.sevenb.terminal.ui.screens.dialog_rate_us.DialogWhatCanWe
import io.sevenb.terminal.ui.screens.order.OrderViewModel.Companion.roundDouble
import io.sevenb.terminal.ui.screens.portfolio.PortfolioFragmentDirections
import io.sevenb.terminal.ui.screens.select_currency.BaseSelectItem
import io.sevenb.terminal.ui.screens.select_currency.DepositCurrencyItem
import io.sevenb.terminal.ui.screens.trading.SharedViewModel
import io.sevenb.terminal.ui.screens.trading.TradingListAdapter.Companion.formatCryptoAmount
import kotlinx.android.synthetic.main.activity_app.*
import kotlinx.android.synthetic.main.fragment_order.*
import kotlinx.android.synthetic.main.fragment_test_market.*
import kotlinx.android.synthetic.main.fragment_withdraw_main.bg_swipe


/**
 * Created by samosudovd on 24/01/2020.
 */
class OrderBottomSheetFragment : BaseBottomSheetFragment() {

    private lateinit var orderViewModel: OrderViewModel
    private lateinit var sharedViewModel: SharedViewModel

    private var localOrderSide: OrderSide = OrderSide.BUY
    private var success = false
    private var localOrderType: OrderType = OrderType.MARKET

    private var localBuyCoins: List<DepositCurrencyItem> = mutableListOf()
    private var accountCoins: List<WithdrawItem> = mutableListOf()

    override fun layoutId() = R.layout.fragment_order
    override fun containerId() = R.id.container_order

    private var firstInitAmount = true
    private var onAvailableClick = true

    private var firstLaunch = true
    private var defaultCurrency = false
    private var emptyAccountCoins: Boolean? = null

    private var availableBalance: String = ""

    private var localDepositList: List<DepositCurrencyItem> = mutableListOf()

    private var percentButtonClicked = false
    private var percentButtonSelected = false
    private var shouldUpdateEstimate = true

    private var percentEstimatedBuy: String? = null
    private var percentEstimatedSell: String? = null

    private var marketPriceReady = false

    private var shouldUpdatePrice = true


    private var marketIsReady = false
    private var estimateFromLastToFirstIsRequired = false
    private var estimateFromFirstToLastIsRequired = false
    private var firstFieldRate: Float? = null
    private var lastFieldRate: Float? = null
    private var priceFieldRate: Float? = null
    private var firstLimitOpen: Boolean? = null
    private var priceWasChanged = false
    private var percentForFirstPressed = false
    private var percentForLastPressed = false
    private var tickerWasChanged = false
    private var tickerWasChangedToMarketLimit = false
    private var priceOnMarket: Float? = null
    private var priceOnLimit: Float? = null

    private var savedMarketAmountBUY: String? = null
    private var savedMarketAmountSELL: String? = null
    private var savedMarketSecondAmountBUY: String? = null
    private var savedMarketSecondAmountSELL: String? = null

    private var savedLimitAmountBUY: String? = null
    private var savedLimitAmountSELL: String? = null
    private var savedLimitSecondAmountSELL: String? = null

    private var insertIsNeeded: Boolean = false

    private var test: Boolean? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        orderViewModel = viewModelProvider(viewModelFactory)
        sharedViewModel = activity?.run {
            viewModelProvider(viewModelFactory)
        } ?: throw Exception("Invalid Activity")
//        container_order_market.visible(false)
//        container_order_success.visible(true)
        initView()
        subscribeUi()
    }

    private fun initView() {
        setFocusListeners(et_input_amount, et_by_price, et_receiver)
        shimmer_frame_layout.startShimmerAnimation()
        container_order_success.visible(false)
        close_screen.setOnClickListener { dismiss() }
        market_tab.underlineText(true)

        market_tab.setOnClickListener {
            insertIsNeeded = true
            clearAllFocus()
            val p = Paint()
            p.color = ContextCompat.getColor(requireContext(), R.color.colorBlue)
            p.flags = Paint.UNDERLINE_TEXT_FLAG
            market_tab.paintFlags = p.flags
            market_tab.apply {
                newTextColor(R.color.colorTextMain)
                underlineText(true)
            }

            ticker_to.newTextColor(R.color.colorTextMain)
            limit_tab.apply {
                newTextColor(R.color.colorTextSecond)
                underlineText(false)
            }
            et_by_price.newTextColor(R.color.colorTextSecond)
            localOrderType = OrderType.MARKET
            cl_by_price_input.newBackground(null)
            cl_by_price_container.newBackground(R.drawable.bg_white_stroke_eight)
            cl_by_price_input.newBackground(null)
            cl_receiver_input.newBackground(R.drawable.bg_white_round_gray_stroke_eight_left)
            cl_receiver.newBackground(R.drawable.bg_white_round_gray_stroke_eight_right)
            cl_receiver_container.newBackground(null)
            ticker_by_price.newTextColor(R.color.colorTextSecond)

            setInvisibleViews(ll_bid_ask)

            if (!tickerWasChangedToMarketLimit) {
                priceOnMarket?.let {
                    et_by_price.tryText(it.toPlainString())
                }
            } else {
                priceOnMarket = priceOnLimit
            }

            et_by_price.doOnTextChanged { _, _, _, _ -> }

            setEnabledViews(false, cl_by_price_container, et_by_price, cl_receive_ticker)
            if (emptyAccountCoins == true) {
                setClickableElements(false)
            } else
                setEnabledViews(true, et_receiver, cl_receiver, cl_receiver_container)

            cl_by_price_container.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.bg_white_stroke_eight)
        }

        cl_input_amount.setOnClickListener {
//            et_input_amount.focusField(requireContext())
        }
        cl_by_price_input.setOnClickListener {
//            if (localOrderType == OrderType.LIMIT)
//                et_by_price.focusField(requireContext())
        }
        cl_receiver_input.setOnClickListener {
//            et_receiver.focusField(requireContext())
        }

        order_ratting_title.setOnClickListener {
            orderViewModel.apply { showRateUs(true) }
        }

        llRateContainer.setOnClickListener {
            orderViewModel.apply { showRateUs(true) }
        }

        limit_tab.setOnClickListener {
            insertIsNeeded = true
            clearAllFocus()
            limit_tab.apply {
                newTextColor(R.color.colorTextMain)
                underlineText(true)
            }

            et_by_price.newTextColor(R.color.colorTextMain)
            market_tab.apply {
                newTextColor(R.color.colorTextSecond)
                underlineText(false)
            }

            localOrderType = OrderType.LIMIT

            if (!tickerWasChangedToMarketLimit) {
                priceOnLimit?.let {
                    et_by_price.tryText(it.toPlainString())
                }
            } else {
                priceOnLimit = priceOnMarket
            }

            firstLimitOpen = firstLimitOpen == null

            if (emptyAccountCoins == true) {
                et_by_price.newTextColor(R.color.colorTextSecond)
                setClickableElements(false)
            } else {
                setEnabledViews(true, cl_by_price_container, et_by_price)
            }

            setVisibleViews(cl_receive_ticker, ll_bid_ask)

            setEnabledViews(false, cl_receive_ticker)
        }

        et_input_amount.doOnTextChanged { text, _, _, _ ->
            if (marketIsReady) {
                if (localOrderType == OrderType.LIMIT)
                    estimateInBaseFirstField(text.toString())

                if (orderViewModel.errorFieldIsShowing) {
                    makeDefaultAmountBackground()
                    orderViewModel.errorFieldIsShowing = false
                }
                if ((text ?: "").contains(","))
                    et_input_amount.setText(text.toString().replace(",", ""))
                val bigDecimalString = try {
                    text.toString().replace(",", "")
                        .getPlainDecimalString(et_input_amount.isFocused) ?: ""
                } catch (e: NumberFormatException) {
                    ""
                }

                if (localOrderSide == OrderSide.SELL &&
                    !orderViewModel.checkIsInPercents(bigDecimalString)
                ) {
                    defaultPercents()
                    percentButtonSelected = false
                }

                if (et_input_amount.isFocused && localOrderType == OrderType.MARKET)
                    estimateFromFirstToLastIsRequired = true

                when (localOrderSide) {
                    OrderSide.BUY -> {
                        savedMarketSecondAmountBUY = if (localOrderType == OrderType.MARKET)
                            bigDecimalString
                        else
                            bigDecimalString
                    }
                    OrderSide.SELL -> {
                        savedMarketAmountSELL = if (localOrderType == OrderType.MARKET)
                            bigDecimalString
                        else
                            bigDecimalString
                    }
                }

                doOnAmountChanged(bigDecimalString)
            }
        }

        et_receiver.doOnTextChanged { text, _, _, _ ->
            if (marketIsReady) {
                if (localOrderType == OrderType.LIMIT)
                    estimateInBaseLastField(text.toString())

                if (orderViewModel.errorFieldIsShowing) {
                    makeDefaultAmountBackground()
                    orderViewModel.errorFieldIsShowing = false
                }

                val bigDecimalString = try {
                    text.getPlainDecimalString(et_receiver.isFocused) ?: ""
                } catch (e: NumberFormatException) {
                    ""
                }

                if (localOrderSide == OrderSide.BUY &&
                    !orderViewModel.checkIsInPercents(bigDecimalString)
                ) {
                    defaultPercents()
                    percentButtonSelected = false
                }

                if (et_receiver.isFocused && localOrderType == OrderType.MARKET)
                    estimateFromLastToFirstIsRequired = true

                doOnReceiverChanged(bigDecimalString)

                when (localOrderSide) {
                    OrderSide.BUY -> {
                        if (localOrderType == OrderType.MARKET)
                            savedMarketAmountBUY = bigDecimalString
                        else
                            savedLimitAmountBUY = bigDecimalString
                    }
                    OrderSide.SELL -> {
                        if (localOrderType == OrderType.MARKET)
                            savedMarketSecondAmountSELL = bigDecimalString
                        else
                            savedLimitSecondAmountSELL = bigDecimalString
                    }
                }

                test = false
                tickerWasChanged = false
            }
        }

        ticker_to.doOnTextChanged { text, _, _, _ ->
            ticker_by_price.text = if (text.toString() == "BCHA") "XEC" else text.toString()
            val ticker = emptyAccountCoins?.let {
                if (it) AccountData.getBaseCurrencyTicker()
                else text.toString()
            } ?: text.toString()
            val tickerFromText = ticker_from.getString()
            orderViewModel.findMarkets(
                if (tickerFromText == "XEC") "BCHA" else tickerFromText,
                ticker
            )
            tickerWasChanged = true
            tickerWasChangedToMarketLimit = true
        }

        et_by_price.doOnTextChanged { text, _, _, _ ->
            cl_by_price_container.setDefaultBackground()
            estimateInBasePriceField(text.toString())
            if (localOrderType == OrderType.LIMIT) {
                priceOnLimit = text.getFloatValue()
                estimateFromFirstToLastIsRequired = false
                estimateFromLastToFirstIsRequired = false

                if (et_by_price.isFocused) {
                    priceWasChanged = true
                }

                if (tickerWasChanged && marketPriceReady && localOrderType == OrderType.LIMIT && localOrderSide == OrderSide.SELL) {
                    test = true
                }

                tryFirstField(et_input_amount.getString())
            } else {
                priceOnMarket = text.getFloatValue()
            }

            if (insertIsNeeded) {
                val rate = text.getFloatValue()
                when (localOrderType) {
                    OrderType.MARKET -> {
                        val asd = ReceivedRate(
                            if (orderViewModel.isDirectOrder) rate else 1 / rate,
                            0f,
                            0f,
                            OrderFieldPosition.FROM_FIRST
                        )
                        if (localOrderSide == OrderSide.BUY) {
                            tryFirstField(savedMarketSecondAmountBUY)
                            estimateLastField(asd)
                        } else {
                            tryFirstField(savedMarketAmountSELL)
                            estimateLastField(asd)
                        }
                        insertIsNeeded = false
                    }
                    OrderType.LIMIT -> {
                        val asd = ReceivedRate(
                            rate,
                            0f,
                            0f,
                            OrderFieldPosition.FROM_FIRST
                        )
                        if (localOrderSide == OrderSide.BUY) {
                            tryFirstField(savedMarketSecondAmountBUY)
                            estimateLastField(asd)
                        } else {
                            tryFirstField(savedLimitAmountSELL)
                            estimateLastField(asd)
                        }
                        insertIsNeeded = false
                    }
                    else -> {
                    }
                }
            }
        }

        cl_receiver.setOnClickListener {
            clearAllFocus()
            sharedViewModel.selectCurrencyList.value = when (localOrderSide) {
                OrderSide.BUY -> {
                    val selectCurrency = mutableListOf<WithdrawItem>()
                    val tempMap = mutableMapOf<String, WithdrawItem>()

                    accountCoins.forEach {
                        it.network = ""
                        tempMap[it.ticker] = it
                    }

                    tempMap.values.forEach {
                        val tickerFromText = ticker_from.getString()
                        if (it.ticker != (if (tickerFromText == "XEC") "BCHA" else tickerFromText) &&
                            localDepositList.firstOrNull { depositCurrencyItem ->
                                depositCurrencyItem.ticker == it.ticker
                            } != null
                        )
                            selectCurrency.add(it)
                    }
                    selectCurrency
                }
                OrderSide.SELL -> localDepositList
            }
            currencySelection()
        }

        val display: Display? = activity?.windowManager?.defaultDisplay
        val size = Point()
        display?.getSize(size)
        val screenHeight: Int = size.y

        bg_swipe.updateLayoutParams { height = screenHeight }
//        bg_swipe_order_success.updateLayoutParams { height = screenHeight }

//        bg_swipe.layoutParams = ConstraintLayout.LayoutParams(bg_swipe.width,screenHeight)// { height = screenHeight }
//        bg_swipe_order_success.layoutParams = ConstraintLayout.LayoutParams(bg_swipe_order_success.width,screenHeight) //.updateLayoutParams { height = screenHeight }

        container_order_market.setTransitionListener(object :
            MotionLayout.TransitionListener {
            override fun onTransitionStarted(p0: MotionLayout?, p1: Int, p2: Int) {}

            override fun onTransitionChange(p0: MotionLayout?, p1: Int, p2: Int, p3: Float) {}

            override fun onTransitionCompleted(p0: MotionLayout?, p1: Int) {
                if (p0?.endState == p0?.currentState) {
                    if (success) {
                        success = false
                        if (p0?.endState == p0?.currentState) {
                            sharedViewModel.updateAssetsAfterTransaction.value = true
                            dismiss()
                        }
                    } else {
                        val tickerFromText = ticker_from.getString()
                        val tickerToText = ticker_to.getString()
                        orderViewModel.orderPost(
                            if (tickerFromText == "XEC") "BCHA" else tickerFromText,
                            if (tickerToText == "XEC") "BCHA" else tickerToText,
                            localOrderSide,
                            localOrderType,
                            et_input_amount.text.toString(),
                            et_by_price.text.toString(),
                            et_receiver.text.toString()
                        )
                    }

                } else swipeAnimationToStart()
            }

            override fun onTransitionTrigger(p0: MotionLayout?, p1: Int, p2: Boolean, p3: Float) {}
        })

        ticker_from.doOnTextChanged { text, _, _, _ ->
            val tickerFromText = ticker_from.getString()
            orderViewModel.fromCurrency = if (tickerFromText == "XEC") "BCHA" else tickerFromText
        }

        label_twenty_five_percent_order.setOnClickListener {
            if (marketPriceReady) {
                percentButtons(25)
                selectPercents(it)
            }
        }
        label_fifty_percent_percent_order.setOnClickListener {
            if (marketPriceReady) {
                percentButtons(50)
                selectPercents(it)
            }
        }
        label_hundred_percent_order.setOnClickListener {
            if (marketPriceReady) {
                percentButtons(100)
                selectPercents(it)
            }
        }

        orderViewModel.isFirstStart = true
        setEnabledViews(
            false,
            cl_receive_ticker,
            et_by_price,
            cl_by_price_container,
            ticker_by_price
        )
    }

    private fun validatePrice(): Float {
        val tempPrice = et_by_price.getString()
        return if (localOrderType == OrderType.LIMIT) {
            if (tempPrice.containsDot()) {
                val integers = try {
                    tempPrice.getIntDigits()
//                    tempPrice.split(".")[0]
                } catch (e: Exception) {
                    "0"
                }

                var onlyZero = true
                integers.forEachIndexed { index, c ->
                    if (c.toInt() != 48) onlyZero = false
                }

                if (onlyZero) {
                    tempPrice.getFloatValue()
                } else {
                    val firstChar = tempPrice[0]
                    if (firstChar == '0') {
                        0.0f
                    } else tempPrice.getFloatValue()
                }
            } else tempPrice.getFloatValue()
        } else tempPrice.getFloatValue()
    }

    private fun doOnAmountChanged(bigDecimalString: String) {
        if (marketIsReady && orderViewModel.isDirectOrder) {
            val step = orderViewModel.getPrecision()

            val checkedIsInt = checkFirstFieldIsInt(step, bigDecimalString)
            if (step != null && step != 0 &&
                bigDecimalString.isNotEmpty() && bigDecimalString.containsDot()//.contains(".")
            ) {
                val wholeDigits = bigDecimalString.getIntDigits()//.split(".")[0]
                val decimalDigits = bigDecimalString.getDecimalDigits()//.split(".")[1]

                if (decimalDigits.length > step) { // removing unnecessary decimal digits
                    var inputAmountText = ""
                    loop@ for ((pos, char) in decimalDigits.withIndex()) {
                        if (pos != step) inputAmountText += char
                        else break@loop
                    }
                    et_input_amount.tryText("$wholeDigits.$inputAmountText")
                } else { // when step size is reached
                    if (bigDecimalString.isNotEmpty()) {
                        if (!checkedIsInt) return

                        if (localOrderType == OrderType.MARKET) {
                            makeEstimateFromFirstToLast()
                        } else {
                            if (nothingIsFocused()) {
                                if (!percentForFirstPressed) {
                                    if (test == null || !test!!)
                                        makeEstimateFromFirstToLast()
                                    else {
                                        percentForFirstPressed = false
                                        val receivedRate =
                                            getCurrentRate(OrderFieldPosition.FROM_FIRST)

                                        estimateLastField(receivedRate)
                                    }
                                } else {
                                    percentForFirstPressed = false
                                    val receivedRate = getCurrentRate(OrderFieldPosition.FROM_FIRST)

                                    estimateLastField(receivedRate)
                                }
                            } else {
                                if (et_input_amount.isFocused || et_by_price.isFocused) {
                                    val receivedRate = getCurrentRate(OrderFieldPosition.FROM_FIRST)
                                    estimateLastField(receivedRate)
                                }
                            }
                        }
                    }
                }
            } else { // if step size 0
                if (!checkedIsInt) return

                if (bigDecimalString.isNotEmpty()) {
                    if (localOrderType == OrderType.MARKET) {
                        makeEstimateFromFirstToLast()
                    } else {
                        if (nothingIsFocused()) {
                            if (!percentForFirstPressed) {
                                if (test == null || !test!!)
                                    makeEstimateFromFirstToLast()
                                else {
                                    percentForFirstPressed = false
                                    val receivedRate = getCurrentRate(OrderFieldPosition.FROM_FIRST)

                                    estimateLastField(receivedRate)
                                }
                            } else {
                                percentForFirstPressed = false
                                val receivedRate = getCurrentRate(OrderFieldPosition.FROM_FIRST)

                                estimateLastField(receivedRate)
                            }
                        } else {
                            if (et_input_amount.isFocused || et_by_price.isFocused) {
                                val receivedRate = getCurrentRate(OrderFieldPosition.FROM_FIRST)
                                estimateLastField(receivedRate)
                            }
                        }
                    }
                }
            }
        } else { // if not direct order
            if (bigDecimalString.isNotEmpty()) {
                if (localOrderType == OrderType.MARKET) {
                    makeEstimateFromFirstToLast()
                } else {
                    if (nothingIsFocused()) {
                        if (!percentForFirstPressed) {
                            if (test == null || !test!!)
                                makeEstimateFromFirstToLast()
                            else {
                                percentForFirstPressed = false
                                val receivedRate = getCurrentRate(OrderFieldPosition.FROM_FIRST)

                                estimateLastField(receivedRate)
                            }
                        } else {
                            percentForFirstPressed = false
                            val receivedRate = getCurrentRate(OrderFieldPosition.FROM_FIRST)

                            estimateLastField(receivedRate)
                        }
                    } else if (localOrderType == OrderType.LIMIT) {
                        if (et_input_amount.isFocused || et_by_price.isFocused) {
                            val receivedRate = getCurrentRate(OrderFieldPosition.FROM_FIRST)
                            estimateLastField(receivedRate)
                        }
                    }
                }
            }
        }
    }

    private fun getCurrentRate(from: OrderFieldPosition) = ReceivedRate(
        validatePrice(),
//                et_by_price . getFloatValue (),
        0f,
        0f,
        from
    )

    private fun rateUs(isInAppReviewEnabled: Boolean) {
        val intent = Intent(Intent.ACTION_VIEW).apply {
        data = Uri.parse(
            "https://play.google.com/store/apps/details?id=io.sevenb.terminal")
        setPackage("com.android.vending")
    }
        startActivity(intent)
    }

    private fun rateUsAfterThreeSuccessfulOrders() {
        val manager = ReviewManagerFactory.create(requireActivity().baseContext)
        val request = manager.requestReviewFlow()
        request.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val reviewInfo = task.result
                manager.launchReviewFlow(requireActivity(), reviewInfo)
            }
        }
    }

    private fun dialogWhatCanWe() {
        val dialog = DialogWhatCanWe.getInstance()
        dialog.show(parentFragmentManager, "")
        dialog.initListenerConfirm {
            if (it) {
                dialogThankYou()
            } else {
                orderViewModel.apply {
                    showRateUs(true)
                }
            }
        }
    }

    private fun dialogThankYou() {
        val dialog = DialogThankYou.getInstance()
        dialog.show(parentFragmentManager, "")
    }

    private fun doOnReceiverChanged(bigDecimalString: String) {
        if (marketIsReady && !orderViewModel.isDirectOrder) {
            val step = orderViewModel.getPrecision()

            val checkedIsInt = checkLastFieldIsInt(step, bigDecimalString)

            if (step != null && step != 0 &&
                bigDecimalString.isNotEmpty() && bigDecimalString.containsDot()//.contains(".")
            ) {
                val wholeDigits = bigDecimalString.getIntDigits()//.split(".")[0]
                val decimalDigits = bigDecimalString.getDecimalDigits()//.split(".")[1]

                if (decimalDigits.length > step) { // removing unnecessary decimal digits
                    var inputAmountText = ""
                    loop@ for ((pos, char) in decimalDigits.withIndex()) {
                        if (pos != step) inputAmountText += char
                        else break@loop
                    }

                    et_receiver.tryText("$wholeDigits.$inputAmountText")
                } else { // when step size is reached
                    if (bigDecimalString.isNotEmpty()) {
                        if (!checkedIsInt) return

                        if (localOrderType == OrderType.MARKET) {
                            makeEstimateFromLastToFirst()
                        } else {
                            if (nothingIsFocused()) {
                                if (!percentForLastPressed) {
                                    makeEstimateFromLastToFirst()
                                } else {
                                    percentForLastPressed = false
                                    val receivedRate = getCurrentRate(OrderFieldPosition.FROM_LAST)

                                    estimateFirstField(receivedRate)
                                }
                            } else {
                                if (et_receiver.isFocused) {
                                    val receivedRate = getCurrentRate(OrderFieldPosition.FROM_LAST)
                                    priceWasChanged = false
                                    estimateFirstField(receivedRate)
                                }
                            }
                        }
                    }
                }
            } else { // if step size 0
                if (!checkedIsInt) return

                if (bigDecimalString.isNotEmpty()) {
                    if (localOrderType == OrderType.MARKET) {
                        makeEstimateFromLastToFirst()
                    } else {
                        if (nothingIsFocused()) {
                            if (!percentForLastPressed) {
                                makeEstimateFromLastToFirst()
                            } else {
                                percentForLastPressed = false

                                if (et_receiver.isFocused) {
                                    val receivedRate = getCurrentRate(OrderFieldPosition.FROM_LAST)
                                    priceWasChanged = false
                                    estimateFirstField(receivedRate)
                                }
                            }
                        } else {
                            if (et_receiver.isFocused) {
                                val receivedRate = getCurrentRate(OrderFieldPosition.FROM_LAST)
                                priceWasChanged = false
                                estimateFirstField(receivedRate)
                            }
                        }
                    }
                }
            }
        } else { // if direct order
            if (bigDecimalString.isNotEmpty()) {
                if (localOrderType == OrderType.MARKET) {
                    makeEstimateFromLastToFirst()
                } else {
                    if (nothingIsFocused()) {
                        if (!percentForLastPressed) {
                            makeEstimateFromLastToFirst()
                        } else {
                            percentForLastPressed = false
                            val receivedRate = getCurrentRate(OrderFieldPosition.FROM_LAST)

                            if (et_receiver.isFocused) {
                                priceWasChanged = false
                                estimateFirstField(receivedRate)
                            }

                            if (localOrderSide == OrderSide.BUY) {
                                priceWasChanged = false
                                estimateFirstField(receivedRate)
                            }
                        }
                    } else {
                        if (et_receiver.isFocused) {
                            val receivedRate = getCurrentRate(OrderFieldPosition.FROM_LAST)
                            priceWasChanged = false
                            estimateFirstField(receivedRate)
                        }
                    }
                }
            }
        }
    }

    private fun processRatesReceived(ratesReceived: ReceivedRate) {
        constraint_order_container.isVisible = true
        shimmer_frame_layout.stopShimmerAnimation()
        shimmer_frame_layout.isVisible = false
        firstFieldRate = ratesReceived.firstToDefault
        lastFieldRate = ratesReceived.lastToDefault
        priceFieldRate = ratesReceived.rate

        when (ratesReceived.fromField) {
            OrderFieldPosition.FROM_FIRST -> {
                if (availableBalance.isNotEmpty())
                    estimateLastField(ratesReceived, true)

                if (availableBalance.isEmpty() && emptyAccountCoins == true) {
                    estimateLastField(ratesReceived, true)
                }
            }
            OrderFieldPosition.FROM_LAST -> {
                if (availableBalance.isNotEmpty())
                    estimateFirstField(ratesReceived, true)
            }
        }

        updateEstimatesInBase()
    }

    private fun checkStringPrecision(string: String?): String {
        if (string.isNullOrEmpty()) return ""
        val step = orderViewModel.secondTickerPrecision

        val wholeDigits = string.getIntDigits()//.split(".")[0]

        return if (step > 0) {
            val decimalDigits = try {
                string.getDecimalDigits()//.split(".")[1]
            } catch (e: Exception) {
                "0"
            }

            if (decimalDigits.length > step) {
                var inputAmountText = ""
                loop@ for ((pos, char) in decimalDigits.withIndex()) {
                    if (pos != step) inputAmountText += char
                    else break@loop
                }

                "$wholeDigits.$inputAmountText"
            } else
                "$wholeDigits.$decimalDigits"
        } else wholeDigits
    }

    private fun tryFirstField(string: String?) {
        if (string == null) return

        if (orderViewModel.isDirectOrder) {
            et_input_amount.tryText(string)
        }
        if (!orderViewModel.isDirectOrder) {
            et_input_amount.tryText(checkStringPrecision(string))
        }
    }

    private fun tryLastField(string: String?) {
        if (string == null) return

        if (orderViewModel.isDirectOrder) {
            et_receiver.tryText(checkStringPrecision(string))
        }

        if (!orderViewModel.isDirectOrder) {
            et_receiver.tryText(string)
        }
    }

    private fun updateEstimatesInBase() {
        estimateInBaseFirstField(et_input_amount.getString())
        estimateInBaseLastField(et_receiver.getString())
        estimateInBasePriceField(et_by_price.getString())
    }

    private fun estimateFirstField(ratesReceived: ReceivedRate, fromBack: Boolean = false) {
        try {
            val estimate = if (!orderViewModel.isDirectOrder) {
                if (localOrderType == OrderType.MARKET)
                    et_receiver.getFloatValue() * ratesReceived.rate
                else {
                    if (fromBack && localOrderSide == OrderSide.BUY) {
                        et_receiver.getFloatValue() * ratesReceived.rate
                    } else
                        et_receiver.getFloatValue() / ratesReceived.rate
                }
            } else et_receiver.getFloatValue() / ratesReceived.rate

            val estimatedString = estimate.toPlainString()

            tryFirstField(orderViewModel.checkIsAllZeros(estimatedString))
        } catch (e: Exception) {

        }
    }

    private fun estimateInBaseFirstField(firstField: String) {
        try {
            val baseCurrencyCharacter = getBaseCurrencyCharacter()
            firstFieldRate?.let {
                val estimatedString =
                    "≈ $baseCurrencyCharacter${
                        roundDouble(firstField.getDoubleValue() * it, 2, true)
                    }"
                tv_estimated_input.text = estimatedString
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun estimateLastField(ratesReceived: ReceivedRate, fromBack: Boolean = false) {
        try {
            val estimate = if (!orderViewModel.isDirectOrder) {
                if (localOrderType == OrderType.MARKET)
                    et_input_amount.getFloatValue() / ratesReceived.rate
                else {
                    if (fromBack && localOrderSide == OrderSide.SELL) {
                        et_input_amount.getFloatValue() * (1 / ratesReceived.rate)
                    } else
                        et_input_amount.getFloatValue() * ratesReceived.rate
                }
            } else et_input_amount.getFloatValue() * ratesReceived.rate

            val estimatedString = estimate.toPlainString()
            estimateInBaseLastField(estimatedString)
            tryLastField(orderViewModel.checkIsAllZeros(estimatedString))
        } catch (e: Exception) {

        }
    }

    private fun estimateInBaseLastField(lastField: String) {
        try {
            val baseCurrencyCharacter = getBaseCurrencyCharacter()
            lastFieldRate?.let {
                val estimatedStringReceive =
                    "≈ $baseCurrencyCharacter${
                        roundDouble((lastField.getDoubleValue() * it), 2, true)
                    }"
                tv_estimated_et_receiver.text = estimatedStringReceive
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun makeNewEstimate(fromField: OrderFieldPosition) {
        val tickerFromText = ticker_from.getString()
        val tickerToText = ticker_to.getString()
        orderViewModel.makeEstimate(
            if (tickerFromText == "XEC") "BCHA" else tickerFromText,
            if (tickerToText == "XEC") "BCHA" else tickerToText,
            fromField
        )
    }

    private fun estimateInBasePriceField(priceField: String) {
        try {
            val baseCurrencyCharacter = getBaseCurrencyCharacter()
            lastFieldRate?.let {
                val estimatedStringReceive =
                    "≈ $baseCurrencyCharacter${
                        roundDouble((priceField.getDoubleValue() * it), 2, true)
                    }"
                tv_estimated_by_price.text = estimatedStringReceive
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun makeEstimateFromFirstToLast() {
        if (estimateFromFirstToLastIsRequired) {
            makeNewEstimate(OrderFieldPosition.FROM_FIRST)
            estimateFromFirstToLastIsRequired = false
        }
    }

    private fun makeEstimateFromLastToFirst() {
        if (estimateFromLastToFirstIsRequired) {
            makeNewEstimate(OrderFieldPosition.FROM_LAST)
            estimateFromLastToFirstIsRequired = false
        }
    }

    private fun checkFirstFieldIsInt(step: Int?, bigDecimalString: String): Boolean {
        step?.let {
            return if (it == 0 && bigDecimalString.containsDot()) {
                val wholeDigits = bigDecimalString.getIntDigits()//.split(".")[0]
                et_input_amount.tryText(wholeDigits)
                false
            } else true
        }

        return false
    }

    private fun checkLastFieldIsInt(step: Int?, bigDecimalString: String): Boolean {
        step?.let {
            return if (it == 0 && bigDecimalString.containsDot()) {
                val wholeDigits = bigDecimalString.getIntDigits()//.split(".")[0]
                et_receiver.tryText(wholeDigits)
                false
            } else true
        }

        return false
    }

    private fun nothingIsFocused() =
        !et_input_amount.isFocused && !et_by_price.isFocused && !et_receiver.isFocused

    private fun clearAllFocus() {
        et_input_amount.clearFocus()
        et_by_price.clearFocus()
        et_receiver.clearFocus()
    }

    private fun makeEstimate(valueToEstimate: String) {
        val tickerFromText = ticker_from.getString()
        val tickerToText = ticker_to.getString()

        val from = if (tickerFromText == "XEC") "BCHA" else tickerFromText
        val to = if (tickerToText == "XEC") "BCHA" else tickerToText//ticker_to.text.toString()

        orderViewModel.marketEstimate(from, to, valueToEstimate)
    }

    private fun makeReverseEstimate(valueToEstimate: String) {
        val tickerFromText = ticker_from.getString()
        val tickerToText = ticker_to.getString()

        val from = if (tickerFromText == "XEC") "BCHA" else tickerFromText
        val to = if (tickerToText == "XEC") "BCHA" else tickerToText//ticker_to.text.toString()

        orderViewModel.reverseMarketEstimate(from, to, valueToEstimate)
    }

    private fun selectPercents(view: View) {
        defaultPercents()
        view.newBackground(R.drawable.bg_blue_stroke_four)
        (view as TextView).newTextColor(R.color.colorSelectPercent)
    }

    private fun defaultPercents() {
        setDefaultPercent(label_twenty_five_percent_order)
        setDefaultPercent(label_fifty_percent_percent_order)
        setDefaultPercent(label_hundred_percent_order)
    }

    private fun setDefaultPercent(textView: TextView) {
        textView.newBackground(R.drawable.bg_white_four)
        textView.newTextColor(R.color.colorTextMain)
    }

    private fun swipeAnimationToStart() {
        container_order_market.transitionToStart()
    }

    private fun makeDefaultAmountBackground() {
        cl_buy_input.newBackground(R.drawable.bg_white_stroke_eight)
        cl_receiver_input.newBackground(R.drawable.bg_white_round_gray_stroke_eight_left)
    }

    private fun subscribeUi() {
        orderViewModel.orderResult.observe(viewLifecycleOwner, { orderResult(it) })
        orderViewModel.marketEstimateResult.observe(viewLifecycleOwner, { marketEstimate(it) })
        orderViewModel.rateResultString.observe(viewLifecycleOwner, { marketPrice(it) })
        orderViewModel.orderProcessing.observe(viewLifecycleOwner, { orderProcessing(it) })
        orderViewModel.marketUnavailableMessage.observe(
            viewLifecycleOwner,
            { marketUnavailableMessage(it) })
        orderViewModel.emptyBalanceResult.observe(viewLifecycleOwner, { noAccountBalance() })
        orderViewModel.snackbarMessage.observe(viewLifecycleOwner, { showMessage(it) })
        orderViewModel.customErrorMessage.observe(viewLifecycleOwner, { showSnackbar(it) })
        orderViewModel.listOfBuyCoins.observe(viewLifecycleOwner, { listOfBuyCoins(it) })
        orderViewModel.accountCoins.observe(viewLifecycleOwner, { accountCoins(it) })
        orderViewModel.availableBalance.observe(viewLifecycleOwner, { availableBalance(it) })
        orderViewModel.showRateUs.observe(viewLifecycleOwner, { rateUs(it) })
        orderViewModel.takeProfitValues.observe(viewLifecycleOwner, { takeProfitValues(it) })
        orderViewModel.stopLossValues.observe(viewLifecycleOwner, { stopLossValues(it) })
        orderViewModel.reverseMarketEstimateResult.observe(
            viewLifecycleOwner,
            { reverseMarketEstimate(it) })
        orderViewModel.market.observe(viewLifecycleOwner, { processPrecision(it) })
        orderViewModel.listDepositCoins.observe(viewLifecycleOwner, { depositList(it) })
        orderViewModel.limitOrderMessage.observe(viewLifecycleOwner, { processLimitOrder(it) })
        orderViewModel.byBuySellEstimate.observe(viewLifecycleOwner, { processBuySellRate(it) })
        orderViewModel.fieldMinimal.observe(viewLifecycleOwner, { processMinNotional(it) })
        orderViewModel.marketDataIsReady.observe(viewLifecycleOwner, { processStepSize(it) })
        orderViewModel.receivedRates.observe(viewLifecycleOwner, { processRatesReceived(it) })
        orderViewModel.bid.observe(viewLifecycleOwner, { processBid(it) })
        orderViewModel.ask.observe(viewLifecycleOwner, { processAsk(it) })
        orderViewModel.showPercentPriceError.observe(
            viewLifecycleOwner,
            { processPercentPrice(it) })
        orderViewModel.showInsufficientBalanceError.observe(
            viewLifecycleOwner,
            { processInsufficientBalance(it) })
        orderViewModel.pricePrecision.observe(viewLifecycleOwner, { processPricePrecision(it) })
        orderViewModel.ratesList.observe(viewLifecycleOwner, { processBuyRates(it) })

        sharedViewModel.orderTicker.observe(viewLifecycleOwner, { selectType(it) })
        sharedViewModel.selectedCurrency.observe(viewLifecycleOwner, { tickerSelection(it) })
    }

    private fun processBuyRates(ratesList: MutableMap<String, MarketRate?>?) {
        if (!ratesList.isNullOrEmpty()) {
            sharedViewModel.ratesList = ratesList
        }
    }

    private fun processPricePrecision(precision: Int?) {
        println()
        precision?.let {
            et_by_price.filters =
                arrayOf(DecimalDigitsInputFilter(15, if (it == 0) 1 else it))
        }
    }

    private fun processInsufficientBalance(balanceError: String?) {
        balanceError?.let {
            if (it.isNotEmpty()) {
                showSnackbar(it)
                available_amount_order.newTextColor(R.color.colorRed)
            } else available_amount_order.newTextColor(android.R.color.black)
        }
    }

    private fun processPercentPrice(percentPrice: String?) {
        percentPrice?.let {
            if (isVisible)
                container_order_market.visibility = View.GONE
            else
                container_order_market.visibility = View.VISIBLE
            pbv_loader.visible(!isVisible)
            tv_message.visible(!isVisible)
            showNotCancelableSnackbar(it)
            cl_by_price_container.setErrorBackground()
        }
    }

    private fun processAsk(ask: String?) {
        if (localOrderType == OrderType.LIMIT) {
            if (ask != null && orderViewModel.localPricePrecision != null) {
                val neededAsk = orderViewModel.formatBidAskPrecision(ask)
                shimmer_frame_layout.stopShimmerAnimation()
                tv_ask.text = neededAsk
            }
        }
    }

    private fun processBid(bid: String?) {
        if (localOrderType == OrderType.LIMIT) {
            if (bid != null && orderViewModel.localPricePrecision != null) {
                val neededBid = orderViewModel.formatBidAskPrecision(bid)
                shimmer_frame_layout.stopShimmerAnimation()
                tv_bid.text = neededBid
            }
        }
    }

    private fun processStepSize(stepSize: Int?) {
        stepSize?.let {
            val secondTickerPrecision = orderViewModel.secondTickerPrecision
            if (orderViewModel.firstMarket != null && orderViewModel.isDirectOrder) {
                val step = orderViewModel.getPrecision()
                step?.let {
                    et_input_amount.filters =
                        arrayOf(DecimalDigitsInputFilter(15, if (it == 0) 1 else it))
                    et_receiver.filters =
//                        arrayOf(DecimalDigitsInputFilter(15, secondTickerPrecision))
                        arrayOf(
                            DecimalDigitsInputFilter(
                                15,
                                if (secondTickerPrecision == 0) 1 else secondTickerPrecision
                            )
                        )
                }
            }
            if (orderViewModel.firstMarket != null && !orderViewModel.isDirectOrder) {
                val step = orderViewModel.getPrecision()
                step?.let {
                    et_receiver.filters =
                        arrayOf(DecimalDigitsInputFilter(15, if (it == 0) 1 else it))
                    et_input_amount.filters =
                            //                        arrayOf(DecimalDigitsInputFilter(15, secondTickerPrecision))
                        arrayOf(
                            DecimalDigitsInputFilter(
                                15,
                                if (secondTickerPrecision == 0) 1 else secondTickerPrecision
                            )
                        )
                }
            }
        }
    }

    private fun processMinNotional(fieldError: OrderFieldError?) {
        swipeAnimationToStart()
        when (fieldError) {
            OrderFieldError.FIRST_FIELD -> {
                cl_buy_input.newBackground(R.drawable.bg_red_stroke_eight)
            }
            OrderFieldError.THIRD_FIELD -> {
                cl_receiver_input.newBackground(R.drawable.bg_white_round_red_stroke_eight_red)
            }
        }
    }

    private fun processBuySellRate(rate: Float?) {
        try {
            val inputAmount = et_input_amount.getDoubleValue()
            val baseCurrencyCharacter = getBaseCurrencyCharacter()
            rate?.let {
                val estimatedString =
                    "≈ $baseCurrencyCharacter${roundDouble(inputAmount * it, 2, true)}"
                tv_estimated_input.text = estimatedString
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun processLimitOrder(message: String?) {
        message?.let {
            if (it.contains("Success")) {
                showSnackbar(it)
            } else {
                val snackbar = Snackbar.make(
                    dialog?.window?.decorView ?: return,
                    message,
                    Snackbar.LENGTH_LONG
                )

                snackbar.show()
            }
        }
    }

    private fun depositList(list: List<DepositCurrencyItem>) {
        localDepositList = list

        when (localOrderSide) {
            OrderSide.BUY -> {
                orderViewModel.requestAccountCoins()
            }
            OrderSide.SELL -> {
                val tickerFromText = ticker_from.getString()

                val selectedItem = list.firstOrNull {
                    it.ticker != if (tickerFromText == "XEC") "BCHA" else tickerFromText
                } ?: list.firstOrNull()
                sellTickerSelection(selectedItem ?: return)
            }
        }
    }

    private fun processPrecision(market: Pair<Market?, Boolean>?) {
        if (market != null) {
            marketIsReady = true
            val precision = orderViewModel.getPrecision()

            precision?.let {
                if (it == 0) {
                    et_input_amount.inputType = InputType.TYPE_CLASS_NUMBER
                } else {
                    et_input_amount.inputType =
                        InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
                }
            }

            when (localOrderSide) {
                OrderSide.BUY -> {
                    estimateFromLastToFirstIsRequired = true
                    if (emptyAccountCoins == true) {
                        estimateFromFirstToLastIsRequired = true
                        et_input_amount.tryText("1")
                    } else
                        tryLastField(et_receiver.getString())
                }
                OrderSide.SELL -> {
                    estimateFromFirstToLastIsRequired = true
                    tryFirstField(et_input_amount.getString())
                }
            }
        }
    }

    private fun selectType(tickerAndType: Pair<String, OrderSide>) {
        localOrderSide = tickerAndType.second
        orderViewModel.depositList()
        when (localOrderSide) {
            OrderSide.BUY -> {
                buy_input_title.text = getString(R.string.buy_input_title)
                order_type_title.text = getString(R.string.title_buy)
                order_type_title.newTextColor(R.color.colorGreen)
                tv_receive_title.text = getString(R.string.receive_title)
            }
            OrderSide.SELL -> {
                buy_input_title.text = getString(R.string.sell_input_title)
                orderViewModel.requestSellBalance(tickerAndType.first)
                order_type_title.text = getString(R.string.title_sell)
                order_type_title.newTextColor(R.color.colorRed)
                tv_receive_title.text = getString(R.string.label_receive)
            }
        }
        ticker_from.text = if (tickerAndType.first == "BCHA") "XEC" else tickerAndType.first
    }

    private fun marketEstimate(estimate: String) {
        val estimatedCost = estimate.replace(",", ".")

        val estimated = try {
            estimatedCost.getPlainDecimalString() ?: ""
        } catch (e: NumberFormatException) {
            ""
        }

        when {
            firstInitAmount -> {
                if (localOrderSide == OrderSide.SELL)
                    reverseMarketEstimate(et_input_amount.text.toString())
                firstInitAmount = false
                tryLastField(estimated)
            }
            percentButtonClicked -> {
                tryLastField(estimated)
                et_receiver.cursorToLastIfSelected()
            }
            et_input_amount.isFocused -> {
                val check = orderViewModel.checkIsAllZeros(estimated.toBigDecimal().toPlainString())
                tryLastField(check)
            }
        }
    }

    private fun setEstimatedBuySellInDefCurrency(estimate: String) {
        try {
            val baseCurrencyCharacter = getBaseCurrencyCharacter()
            val estimatedValue = estimate.getDoubleValue()
            val estimatedStringReceive =
                "≈ $baseCurrencyCharacter${
                    roundDouble((estimatedValue * orderViewModel.buySellRate), 2, true)
                }"
            tv_estimated_input.text = estimatedStringReceive
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun marketPrice(price: String) {
        if (localOrderType != OrderType.LIMIT) {
            if (price == "0") {
                makeEstimate(et_input_amount.text.toString())
                makeReverseEstimate(et_receiver.text.toString())
            } else
                et_by_price.tryText(price.replace(",", ""))
        }

        if (localOrderType == OrderType.LIMIT && shouldUpdatePrice) {
            if (!et_by_price.isFocused && !et_receiver.isFocused)
                et_by_price.tryText(price.replace(",", ""))
            shouldUpdatePrice = false
        }

        marketPriceReady = true
        tickerWasChanged = false
        tickerWasChangedToMarketLimit = false
    }

    private fun reverseMarketEstimate(estimate: String) {
        val fixedEstimate = estimate.replace(",", ".")
        val check = orderViewModel.checkIsAllZeros(fixedEstimate)

        when {
            firstInitAmount -> {
                firstInitAmount = false
                tryFirstField(check)
                et_input_amount.cursorToLastIfSelected()
            }
            percentButtonClicked -> {
                tryFirstField(check)
                et_input_amount.cursorToLastIfSelected()
            }
            et_receiver.isFocused -> {
                tryFirstField(check)
                et_input_amount.cursorToLastIfSelected()
                setEstimatedBuySellInDefCurrency(fixedEstimate)
            }
        }
    }

    private fun orderProcessing(isVisible: Boolean) {
        swipeAnimationToStart()
        if (isVisible)
            container_order_market.visibility = View.GONE
        else
            container_order_market.visibility = View.VISIBLE
        pbv_loader.visible(isVisible)
        tv_message.visible(isVisible)
        tv_message.text = getString(R.string.message_order_processing)
    }

    private fun marketUnavailableMessage(isVisible: Boolean) {
        message_market_unavailable.visible(isVisible)
        val tickerFromText = ticker_from.getString()
        val tickerToText = ticker_to.getString()

        val from = if (tickerFromText == "XEC") "BCHA" else tickerFromText
        val to = if (tickerToText == "XEC") "BCHA" else tickerToText//ticker_to.text.toString()
        message_market_unavailable.text =
            getString(R.string.message_market_unavailable).format(from, to)
    }

    private fun noAccountBalance() {
        findNavController().popBackStack(R.id.tradingFragment, false)
        (activity as AppActivity).bottomNavView.selectedItemId = R.id.portfolioFragment
        try {
            val direction =
                PortfolioFragmentDirections.actionPortfolioFragmentToDepositBottomSheetFragment()
            findNavController().navigate(direction)
        } catch (e: Exception) {
        }
    }

    private fun buyTickerSelection(baseSelectItem: BaseSelectItem) {
        if (baseSelectItem !is DepositCurrencyItem) return
        ticker_to.text = if (baseSelectItem.ticker == "BCHA") "XEC" else baseSelectItem.ticker
        firstInitAmount = true
        firstLaunch = false
    }

    private fun sellTickerSelection(baseSelectItem: BaseSelectItem) {
        if (baseSelectItem !is WithdrawItem && baseSelectItem !is DepositCurrencyItem) return
        if (baseSelectItem is WithdrawItem) {
            try {
                ticker_to.text =
                    if (baseSelectItem.ticker == "BCHA") "XEC" else baseSelectItem.ticker
                availableBalance(formatCryptoAmount(baseSelectItem.amount?.toFloatOrNull()))
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else if (baseSelectItem is DepositCurrencyItem) {
            ticker_to.text = if (baseSelectItem.ticker == "BCHA") "XEC" else baseSelectItem.ticker
        }
        firstInitAmount = true
        firstLaunch = false
    }

    private fun availableBalance(balance: String) {
        val textBalance = if (balance == "") "0.0" else balance
        availableBalance = balance
        val ticker = when (localOrderSide) {
            OrderSide.BUY -> {
                ticker_to.text
            }
            OrderSide.SELL -> {
                ticker_from.text
            }
        }
        val sum = "$textBalance $ticker"
        if (accountCoins.isEmpty() && localOrderSide == OrderSide.BUY) {
            if (ticker_to.text.toString() != PreferenceRepository.baseCurrencyTicker)
                ticker_to.text = PreferenceRepository.baseCurrencyTicker
            sellCoinsIsEmpty()
        } else {
            inputAvailable(sum, textBalance)
        }
    }

    private fun inputAvailable(availableAmount: String, textBalance: String) {
        onAvailableClick = true
        when (localOrderSide) {
            OrderSide.BUY -> {
                available_amount_order.text = availableAmount
                if (textBalance != "0.0") {
                    et_receiver.tryText(textBalance)
                    et_receiver.cursorToLastIfSelected()
                } else {
                    et_input_amount.tryText("1")
                    et_input_amount.cursorToLastIfSelected()
                }
            }
            OrderSide.SELL -> {
                available_amount_order.text = availableAmount
                if (textBalance != "0.0") {
                    et_input_amount.tryText(textBalance)
                    et_input_amount.cursorToLastIfSelected()
                } else {
                    val tickerFromText = ticker_from.getString()
                    val tickerToText = ticker_to.getString()
                    orderViewModel.reverseMarketEstimate(
                        if (tickerFromText == "XEC") "BCHA" else tickerFromText,
                        if (tickerToText == "XEC") "BCHA" else tickerToText,
                        et_receiver.text.toString()
                    )
                }
            }
        }
    }

    private fun percentButtons(percent: Int) {
        try {
            if (marketPriceReady) {
                clearAllFocus()

                percentButtonClicked = true
                percentButtonSelected = true
                onAvailableClick = true

                et_input_amount.newTextColor(android.R.color.black)
                et_receiver.newTextColor(android.R.color.black)

                val amount = availableBalance.replace(",", "").toFloatOrNull() ?: 0.0f
                val percented = amount.toBigDecimal() * percent.toBigDecimal() / 100.toBigDecimal()
                val formattedValue = formatCryptoAmount(percented.toFloat())
                when (localOrderSide) {
                    OrderSide.BUY -> {
                        percentForLastPressed = true
                        estimateFromLastToFirstIsRequired = true
                        percentEstimatedBuy = formattedValue
                        if (orderViewModel.firstMarket != null && !orderViewModel.isDirectOrder) {
                            val step = orderViewModel.getPrecision()
                            step?.let {
                                tryLastField(roundDouble(percented.toDouble(), it))
                            }
                        } else
                            tryLastField(formattedValue)

                        et_receiver.cursorToLastIfSelected()
                    }
                    OrderSide.SELL -> {
                        percentForFirstPressed = true
                        estimateFromFirstToLastIsRequired = true

                        percentEstimatedSell = formattedValue
                        if (orderViewModel.firstMarket != null && orderViewModel.isDirectOrder) {
                            val step = orderViewModel.getPrecision()
                            step?.let {
                                tryFirstField(roundDouble(percented.toDouble(), it))
                            }
                        } else
                            tryFirstField(formattedValue)

                        et_input_amount.cursorToLastIfSelected()
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun listOfBuyCoins(list: List<DepositCurrencyItem>) {
        localBuyCoins = list

        val tickerFromText = ticker_from.getString()

        val selectedItem = list.firstOrNull {
            it.ticker != if (tickerFromText == "XEC") "BCHA" else tickerFromText
        } ?: list.firstOrNull()
        buyTickerSelection(selectedItem ?: return)
    }

    private fun sellCoinsIsEmpty() {
        constraint_order_container.isVisible = true
        shimmer_frame_layout.stopShimmerAnimation()
        shimmer_frame_layout.isVisible = false

        label_available_amount_order_new.visible(false)
        available_amount_order.text = getString(R.string.title_order_empty_balances)
        swipe_to_confirm_title_order.text = getString(R.string.action_order_swipe_to_deposit)
        defaultCurrency = true
        setClickableElements(false)
    }

    private fun accountCoins(list: List<WithdrawItem>) {
        accountCoins = list
        if (list.isEmpty()) {
            emptyAccountCoins = true
            if (ticker_to.text.toString() != PreferenceRepository.baseCurrencyTicker)
                ticker_to.text = PreferenceRepository.baseCurrencyTicker
            sellCoinsIsEmpty()
        } else {
            setClickableElements(true)

            if (localDepositList.isNotEmpty()) {
                val tickerFromText = ticker_from.getString()
                val selectedItem = list.firstOrNull {
                    it.ticker != (if (tickerFromText == "XEC") "BCHA" else tickerFromText) &&
                            localDepositList.firstOrNull { depositCurrencyItem ->
                                depositCurrencyItem.ticker == it.ticker
                            } != null
                }
                val finalItem = selectedItem ?: localDepositList.firstOrNull()
                sellTickerSelection(finalItem ?: return)
            }
        }
    }

    private fun setClickableElements(value: Boolean) {
        setEnabledViews(
            value,
            cl_receive_ticker,
            et_receiver,
            et_input_amount,
            cl_input_amount,
            cl_by_price_input,
            cl_receiver_input,
            cl_receiver,
            ticker_to,
            picker_ticker,
        )
        setViewsVisibility(
            value,
            label_twenty_five_percent_order,
            label_fifty_percent_percent_order,
            label_hundred_percent_order
        )

        if (!value) {
            et_receiver.newTextColor(R.color.colorTextSecond)
            ticker_to.newTextColor(R.color.colorTextSecond)
        }

        when (localOrderType) {
            OrderType.MARKET -> setEnabledViews(false, cl_receive_ticker)
            OrderType.LIMIT -> setEnabledViews(true, cl_receive_ticker)
        }
    }

    private fun takeProfitValues(takeProfitValues: Pair<String, String>) {
//        tv_take_profit_percent.text = "%s%s".format(takeProfitValues.first, "%")
//        tv_take_profit_amount.text = "+%s".format(takeProfitValues.second)
    }

    private fun stopLossValues(stopLossValues: Pair<String, String>) {
//        tv_stop_loss_percent.text = "%s%s".format(stopLossValues.first, "%")
//        tv_stop_loss_amount.text = "-%s".format(stopLossValues.second)
    }

    private fun tickerSelection(baseSelectItem: BaseSelectItem?) {
        baseSelectItem?.let {
            sharedViewModel.selectedCurrency.value = null
            marketIsReady = false
            shouldUpdateEstimate = true
            shouldUpdatePrice = true
            orderViewModel.firstMarketBoolean = true
            if (localOrderType != OrderType.LIMIT) {
                marketPriceReady = false
            }
            when (baseSelectItem) {
                is DepositCurrencyItem -> {
                    if (!firstLaunch)
                        buyTickerSelection(baseSelectItem)
                }
                is WithdrawItem -> {
                    if (!firstLaunch)
                        sellTickerSelection(baseSelectItem)
                }
                else -> {
                    return
                }
            }
        }
    }

    private fun currencySelection() {
        if (findNavController().currentDestination?.id != R.id.orderBottomSheetFragment) return

        val direction =
            OrderBottomSheetFragmentDirections.actionOrderBottomSheetFragmentToSelectCurrencyFragment()
        findNavController().navigate(direction)
    }

    private fun orderResult(orderItem: OrderItem) {
        market_tab.visible(false)
        limit_tab.visible(false)
        constraint_order_container.visible(false)
        container_order_success.visible(true)
        success = true
        swipe_to_confirm_title_order.text = getString(R.string.title_swipe_to_close)

        orderViewModel.setCountSuccessOrder()

        if (orderViewModel.getCountSuccessOrder() == 3) {
            rateUsAfterThreeSuccessfulOrders()
        }
        
        if (orderItem.createdAt.isNotEmpty()) {
            val timestamp = parseToTimestamp(orderItem.createdAt)
            val date = dateFromTimestamp(timestamp)
            val time = timeFromTimestamp(timestamp)
            market_date_created.text = "Created at %s %s".format(date, time)
        }

        if (orderItem.fills.isNotEmpty()) {
            val commissionAmount = try {
                orderItem.binanceCommission.toString().getPlainDecimalString()
            } catch (e: NumberFormatException) {
                ""
            }
            order_commission.text = commissionAmount
        }

        val buyTitle: String = getString(R.string.order_buy_title)
        val sellTitle: String = getString(R.string.order_sell_title)

        first_amount_ticker.text = ticker_from.text
        second_amount_ticker.text = ticker_to.text
        try {
            order_commission_ticker.text = orderItem.fills[0].commissionAsset
        } catch (e: IndexOutOfBoundsException) {
            e.printStackTrace()
            order_commission_ticker.visible(false)
        }

        setAmount(
            orderItem.executedAmount.toPlainString(),
            orderItem.cummulativeQuoteAmount.toPlainString()
        )
        when (localOrderSide) {
            OrderSide.BUY -> {
                setAmountTitle(buyTitle, sellTitle)
            }
            OrderSide.SELL -> {
                setAmountTitle(sellTitle, buyTitle)
            }
        }

        if (localOrderType == OrderType.LIMIT) {
            order_commission.visible(false)
            order_commission_title.visible(false)
            order_commission_ticker.visible(false)
            order_commission.text = ""
            order_commission_title.text = ""
            order_commission_ticker.text = ""
            market_order_title.text = getString(R.string.label_limit_order)
            second_amount_title.text = getString(R.string.title_price)

            try {
                if (orderViewModel.localPricePrecision != null) {
                    val roundedPrice =
                        roundDouble(
                            et_by_price.getDoubleValue(), orderViewModel.localPricePrecision!!
                        )
                    second_amount.text = roundedPrice
                } else second_amount.text = et_by_price.getFloatValue().toString()

                second_amount_ticker.text =
                    if (ticker_by_price.text == "BCHA") "XEC" else ticker_by_price.text
//                setInvisibleViews(swipe_to_close_order_success)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    }

    private fun setAmount(firstAmount: String, secondAmount: String) {
        first_amount.text = firstAmount
        second_amount.text = secondAmount
    }

    private fun setAmountTitle(firstTitle: String, secondTitle: String) {
        first_amount_title.text = firstTitle
        second_amount_title.text = secondTitle
    }

    private fun showMessage(errorMessageType: ErrorMessageType) {
        when (errorMessageType) {
            ErrorMessageType.EMPTY_FIELDS -> showSnackbar(getString(R.string.error_empty_field))
            ErrorMessageType.MARKET_NOT_AVAILABLE -> showSnackbar(getString(R.string.error_no_market))
        }
        swipeAnimationToStart()
    }

    private fun showSnackbar(message: String) {
        if (!defaultCurrency)
            if (!message.contains(AccountData.ERROR_CODE_500))
                Snackbar.make(dialog?.window?.decorView ?: return, message, Snackbar.LENGTH_LONG)
                    .show()
    }

    private fun showNotCancelableSnackbar(message: String) {
        if (!message.contains(AccountData.ERROR_CODE_500)) {
            val snackbar = Snackbar.make(
                dialog?.window?.decorView ?: return,
                message,
                Snackbar.LENGTH_INDEFINITE
            )
            snackbar.setAction("OK") { snackbar.dismiss() }
                .show()
        }
    }

    override fun onPause() {
        shimmer_frame_layout.stopShimmerAnimation()
        shimmer_frame_layout.isVisible = false
        orderViewModel.unsubscribe()

        super.onPause()
    }

    override fun onDestroy() {
        sharedViewModel.ratesList = mutableMapOf()
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()
        orderViewModel.subscribe()
    }

    companion object {
        enum class OrderSide {
            BUY, SELL
        }
    }
}

