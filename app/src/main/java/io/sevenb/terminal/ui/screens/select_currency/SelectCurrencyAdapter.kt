package io.sevenb.terminal.ui.screens.select_currency

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import io.sevenb.terminal.R
import io.sevenb.terminal.data.model.settings.DefaultCurrencyItem
import io.sevenb.terminal.data.model.withdrawal.WithdrawItem
import io.sevenb.terminal.data.repository.auth.PreferenceRepository
import io.sevenb.terminal.domain.usecase.charts.AccountData
import io.sevenb.terminal.ui.extension.observer
import io.sevenb.terminal.ui.extension.visible
import io.sevenb.terminal.ui.screens.portfolio.AssetItem
import io.sevenb.terminal.ui.screens.trading.FavoriteTradeItem
import io.sevenb.terminal.ui.screens.trading.TradingWalletItem
import kotlinx.android.synthetic.main.item_select_list.view.*

internal class SelectCurrencyAdapter(
    private val itemSelected: (BaseSelectItem) -> Unit,
    private val showPlaceholder: (Boolean) -> Unit
) : RecyclerView.Adapter<SelectCurrencyViewHolder>(), Filterable {

    var baseItemsList: List<Any> by observer(listOf()) {
        baseItemsListFiltered = it
        notifyDataSetChanged()
    }

    private var baseItemsListFiltered: List<Any> by observer(listOf()) {
        notifyDataSetChanged()
    }

    var selectedFiatTicker: String by observer(PreferenceRepository.baseCurrencyTicker) {
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SelectCurrencyViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_select_list, parent, false)
        return SelectCurrencyViewHolder(view)
    }

    override fun onBindViewHolder(holder: SelectCurrencyViewHolder, position: Int) {
        val item = baseItemsListFiltered[position]
        holder.bind(item, selectedFiatTicker, itemSelected)

        when (item) {
            is DefaultCurrencyItem -> {
                holder.itemView.selected_default.isVisible = selectedFiatTicker == item.ticker
                holder.itemView.setOnClickListener {
                    notifyDataSetChanged()
                    selectedFiatTicker = item.ticker
                    itemSelected(item)
                }
            }
            else -> {
                holder.itemView.selected_default.isVisible = false
                if (item is WithdrawItem && item.estimated.isNotEmpty()) {
                    val estimatedString = "≈ ${AccountData.getBaseCurrencyCharacter()}${item.estimated}"
                    holder.itemView.tv_estimated.text = estimatedString
                    holder.itemView.tv_estimated.visible(true)
                }
            }
        }
    }

    fun submitList(list: List<Any>) {
        baseItemsList = list
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString().toLowerCase()

                val filterResults = FilterResults()

                filterResults.values = if (charString.isEmpty())
                    baseItemsList
                else
                    baseItemsList.filter {
                        when (it) {
                            is DepositCurrencyItem -> {
                                return@filter it.name.toLowerCase().contains(charString) ||
                                        it.ticker.toLowerCase().contains(charString)
                            }
                            is WithdrawItem -> {
                                return@filter it.name.toLowerCase().contains(charString) ||
                                        it.ticker.toLowerCase().contains(charString)
                            }
                            is TradingWalletItem -> {
                                return@filter it.name.toLowerCase().contains(charString) ||
                                        it.ticker.toLowerCase().contains(charString)
                            }
                            is FavoriteTradeItem -> {
                                return@filter it.name.toLowerCase().contains(charString) ||
                                        it.ticker.toLowerCase().contains(charString)
                            }
                            is AssetItem -> {
                                return@filter it.name.toLowerCase().contains(charString) ||
                                        it.ticker.toLowerCase().contains(charString)
                            }
                            is DefaultCurrencyItem -> {
                                return@filter it.name.toLowerCase().contains(charString) ||
                                        it.ticker.toLowerCase().contains(charString)
                            }
                            else -> return@filter false
                        }
                    }

                return filterResults
            }

            override fun publishResults(
                charSequence: CharSequence,
                filterResults: FilterResults
            ) {
                val filteredItems = filterResults.values as List<BaseSelectItem>
                baseItemsListFiltered = filteredItems

                showPlaceholder(filteredItems.isEmpty())

                notifyDataSetChanged()
            }
        }
    }

    override fun getItemCount(): Int = baseItemsListFiltered.size

}
