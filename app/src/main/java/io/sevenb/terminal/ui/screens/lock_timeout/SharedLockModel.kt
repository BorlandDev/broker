package io.sevenb.terminal.ui.screens.lock_timeout

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.sevenb.terminal.data.model.enums.SafetyStates
import javax.inject.Inject

class SharedLockModel @Inject constructor(): ViewModel() {
    val availableMethod = MutableLiveData<SafetyStates?>()
}