package io.sevenb.terminal.ui.screens.auth.password_resore_state

import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import io.sevenb.terminal.R
import io.sevenb.terminal.data.model.auth.UserCaptcha
import io.sevenb.terminal.data.model.auth.UserData
import io.sevenb.terminal.data.model.enum_model.ErrorMessageType
import io.sevenb.terminal.data.model.enums.FocusableFields
import io.sevenb.terminal.data.model.enums.GeeTestResult
import io.sevenb.terminal.data.model.enums.auth.NewAuthStates
import io.sevenb.terminal.data.model.enums.auth.PasswordRestoringStates
import io.sevenb.terminal.domain.usecase.new_auth.HandleFingerprint
import io.sevenb.terminal.ui.extension.*
import io.sevenb.terminal.ui.screens.auth.SharedAuthViewModel
import io.sevenb.terminal.ui.screens.auth.new_auth.NewAuthSharedViewModel
import io.sevenb.terminal.utils.Logger
import kotlinx.android.synthetic.main.fragment_auth_restore_password.view.*
import kotlinx.android.synthetic.main.fragment_auth_verification_code.view.*
import timber.log.Timber

class PasswordRestoringFlow : LifecycleObserver {

    private lateinit var newAuthSharedViewModel: NewAuthSharedViewModel
    private lateinit var passwordRestoringViewModel: PasswordRestoringViewModel
    private lateinit var authSharedViewModel: SharedAuthViewModel

    private lateinit var view: View
    private lateinit var containerSmsCode: View
    private lateinit var lifecycleOwner: LifecycleOwner
    private lateinit var fragment: Fragment

    private var passVisibility = false
    private var isFirstFocus = true
    private var isSubscribed = false


    private lateinit var handleFingerprint: HandleFingerprint

    fun initLifecycleOwner(
        lifecycleOwner: LifecycleOwner,
        fragment: Fragment,
        handleFingerprint: HandleFingerprint
    ) {
        this.lifecycleOwner = lifecycleOwner
        this.fragment = fragment
        this.handleFingerprint = handleFingerprint

        passVisibility = false
        isFirstFocus = true

        handleFingerprint.init(fragment)
    }

    fun initViews(
        view: View,
        newAuthSharedViewModel: NewAuthSharedViewModel,
        passwordRestoringViewModel: PasswordRestoringViewModel,
        containerSmsCode: View,
        authSharedViewModel: SharedAuthViewModel,
        state: PasswordRestoringStates?
    ) {
        this.newAuthSharedViewModel = newAuthSharedViewModel
        this.passwordRestoringViewModel = passwordRestoringViewModel
        this.authSharedViewModel = authSharedViewModel
        this.view = view
        this.containerSmsCode = containerSmsCode

        passwordRestoringViewModel.apply {
            showResendButton.value = false
            startCaptchaFlow.value = false
            if (state != PasswordRestoringStates.PASSWORD_RESTORING_VERIFICATION_CODE)
                processShowVerificationCode(false)
            state?.let { passwordRestoringViewModel.changeCurrentState(it) }
                //?: changeCurrentState(PasswordRestoringStates.PASSWORD_RESTORING_DATA)
            subscribeUI()
        }

        if (!isSubscribed)
            subscribeUI()

        view.apply {

            input_email_restore.setOnFocusChangeListener { _, hasFocus ->
                Timber.d("FOCUS input_email_restore : setOnFocusChangeListener $hasFocus")
            }

            input_password_restore.setOnFocusChangeListener { _, hasFocus ->
                Timber.d("FOCUS input_password_restore : setOnFocusChangeListener $hasFocus")
            }

            input_email_restore.doOnTextChanged { _, _, _, _ -> showEmailError(false) }
            input_password_restore.doOnTextChanged { _, _, _, _ -> showPasswordError(false) }

            input_email_restore.onActionDoneListener { restorePassword() }
            input_password_restore.onActionDoneListener { validatePassword() }

            setPasswordVisibilityChanger(
                input_password_restore,
                img_pass_visibility_restore_password
            )
            setPasswordChangeListener(input_password_restore)
            setPasswordHints(input_password_restore)
        }

        containerSmsCode.apply { input_verification_code.tryText("") }
    }

    private fun sendConfirmCode() {
        val code = containerSmsCode.input_verification_code.getString()
        passwordRestoringViewModel.confirmCodeRestorePassword(code)
    }

    private fun validatePassword() {
        passwordRestoringViewModel.validatePassword(view.input_password_restore.getString())
    }

    private fun restorePassword() {
        val email = view.input_email_restore.getString()

        val messageType = passwordRestoringViewModel.validateEmail(email)
        if (messageType == ErrorMessageType.CHECK_PASSED) {
            Logger.init(email)
            Logger.sendLog("PasswordRestoringFlow", "restorePassword")
            newAuthSharedViewModel.showProgress(true)
            newAuthSharedViewModel.startCaptchaFlow(UserData(email))
        } else {
            processErrorMessage(messageType)
        }
    }

    private fun subscribeUI() {
        isSubscribed = true
        lifecycleOwner.let { owner ->
            passwordRestoringViewModel.apply {
                geeTestResult.observe(owner, { processGeeTestResult(it) })
                startCaptchaFlow.observe(owner, { processStartCaptchaFlow(it) })
                errorMessage.observe(owner, { processErrorMessage(it) })
                errorString.observe(owner, { processErrorString(it) })
                timerValue.observe(owner, { timerValue(it) })
                showResendButton.observe(owner, { showResendButton(it) })
                showSmsCode.observe(owner, { processShowVerificationCode(it) })
                currentState.observe(owner, { processCurrentState(it) })
//                creatingBiometricCipher.observe(owner, { processFingerprint(it) })
                passCodeCreationRequired.observe(owner, { processPassCodeCreation(it) })
            }

            newAuthSharedViewModel.apply {
                userCaptcha.observe(owner, { processUserCaptcha(it) })
                newState.observe(owner, { processNewState(it) })
            }
        }
    }

    private fun processPassCodeCreation(value: Boolean?) {
        passwordRestoringViewModel.apply {
            newAuthSharedViewModel.preloadAccountAssets()
            value?.let {
                newAuthSharedViewModel.requirePassCodeCreation(
                    value, userData.email, userData.password
                )
            }
        }
    }

    private fun processCurrentState(state: PasswordRestoringStates?) {
        state?.let { currentState ->
            authSharedViewModel.setSubStateToActivity.value = currentState
            when (currentState) {
                PasswordRestoringStates.PASSWORD_RESTORING_DATA -> {
                    processShowVerificationCode(false)
                    containerSmsCode.input_verification_code.tryText("")

                    view.apply {
                        fragment.setVisibleViews(label_input_email_restore, input_email_restore)
                        setGoneViews(
                            container_restore_password_input_password,
                            container_restore_hint
                        )
                        button_send_code_restore.setOnClickListener { restorePassword() }
                        newAuthSharedViewModel.focusField(FocusableFields.RESTORE_EMAIL)
                    }
                }
                PasswordRestoringStates.PASSWORD_RESTORING_VERIFICATION_CODE -> {
                    processShowVerificationCode(true)
                    newAuthSharedViewModel.focusField(FocusableFields.RESTORE_VERIFICATION_CODE)
                    newAuthSharedViewModel.userCaptcha.value = null

                    containerSmsCode.apply {
                        input_verification_code.onActionDoneListener { sendConfirmCode() }
                        button_verify_code.setOnClickListener { sendConfirmCode() }
                        ll_resend_code.setOnClickListener { passwordRestoringViewModel.requestOrResendRestorePasswordCode() }
                        action_change_email.setOnClickListener {
                            passwordRestoringViewModel.changeCurrentState(PasswordRestoringStates.PASSWORD_RESTORING_DATA)
                        }
                        email_code_sent_to.text = passwordRestoringViewModel.userData.email
                        sms_code_back_button.visible(true)
                        sms_code_back_button.setOnClickListener {
                            passwordRestoringViewModel.changeCurrentState(PasswordRestoringStates.PASSWORD_RESTORING_DATA)
                        }
                    }
                }
                PasswordRestoringStates.PASSWORD_RESTORING_NEW_PASSWORD -> {
                    processShowVerificationCode(false)
                    newAuthSharedViewModel.focusField(FocusableFields.RESTORE_PASSWORD)

                    view.apply {
                        setGoneViews(label_input_email_restore, input_email_restore)
                        fragment.setVisibleViews(
                            container_restore_password_input_password,
                            container_restore_hint
                        )
                        button_send_code_restore.setOnClickListener { validatePassword() }
                    }
                }
            }
        }
    }

    private fun processShowVerificationCode(value: Boolean?) {
        value?.let { newAuthSharedViewModel.showVerificationCode(it) }
    }

    private fun processUserCaptcha(userCaptcha: UserCaptcha?) {
        userCaptcha?.let {
            passwordRestoringViewModel.apply {
                if (newAuthSharedViewModel.state == NewAuthStates.PASSWORD_RESTORING ||
                    newAuthSharedViewModel.state == NewAuthStates.CONTINUES_PASSWORD_RESTORING
                ) {
                    userData = newAuthSharedViewModel.userData
                    requestOrResendRestorePasswordCode(it)
                }
            }
        }
    }


    private fun processGeeTestResult(geeTestResult: GeeTestResult?) {
        geeTestResult?.let {
            newAuthSharedViewModel.apply {
                if (geeTestResult != GeeTestResult.SUCCESS) {
                    showProgress(false)
                }
                setGeeTestResult(it)
                passwordRestoringViewModel.userData = userData
            }
        }
    }

    private fun processNewState(state: NewAuthStates?) {
//        if (state == NewAuthStates.CONTINUES_REGISTRATION || state == NewAuthStates.REGISTRATION || state == NewAuthStates.VERIFICATION_CODE) {
        if (state == NewAuthStates.PASSWORD_RESTORING || state == NewAuthStates.CONTINUES_PASSWORD_RESTORING) {
//            passwordRestoringViewModel.sta = state

            if (state == NewAuthStates.CONTINUES_PASSWORD_RESTORING) {
                passwordRestoringViewModel.changeCurrentState(PasswordRestoringStates.PASSWORD_RESTORING_VERIFICATION_CODE)
            } else if (state == NewAuthStates.PASSWORD_RESTORING) {
                passwordRestoringViewModel.changeCurrentState(PasswordRestoringStates.PASSWORD_RESTORING_DATA)
            }
        }
    }

    private fun processErrorString(message: String?) {
        message?.let { newAuthSharedViewModel.errorString.value = it }
    }

    private fun processErrorMessage(messageType: ErrorMessageType?) {
        messageType?.let {
            when (it) {
                ErrorMessageType.EMAIL_INCORRECT,
                ErrorMessageType.EMPTY_EMAIL ->
                    showEmailError(true)
                ErrorMessageType.PASSWORD_INCORRECT ->
                    showPasswordError(true)
                else -> TODO()
            }
            newAuthSharedViewModel.showErrorMessage(messageType)
        }
    }

    private fun processStartCaptchaFlow(value: Boolean?) {
        value?.let {
            if (it)
                newAuthSharedViewModel.apply { startCaptchaFlow(userData) }
        }
    }

    private fun showPasswordError(value: Boolean) {
        view.container_restore_password_input_password.apply {
            if (value) setErrorBackground()
            else setDefaultBackground()
        }
    }

    private fun showEmailError(value: Boolean) {
        view.input_email_restore.apply {
            if (value) setErrorBackground()
            else setDefaultBackground()
        }
    }

    private fun changeEyeImage(imageView: ImageView) {
        imageView.newBackground(if (passVisibility) R.drawable.ic_password else R.drawable.ic_password_hidden)
    }

    private fun setImage(imageView: ImageView, value: Boolean) =
        if (value) setEnteredImage(imageView) else setNotEnteredImage(imageView)

    private fun setEnteredImage(imageView: ImageView) =
        imageView.newBackground(R.drawable.ic_entered)

    private fun setNotEnteredImage(imageView: ImageView) =
        imageView.newBackground(R.drawable.ic_not_entered)

    private fun setPasswordHints(view: EditText) {
        view.doOnTextChanged { text, _, _, _ ->
            text?.let {
                val response = passwordRestoringViewModel.checkPassword(it.toString())

                setImage(this.view.img_pass_length_restore, response.lengthPasses)
                setImage(this.view.img_numbers_contain_restore, response.numbersPasses)
                setImage(this.view.img_uppercase_restore, response.lowercasePasses)
                setImage(this.view.img_lowercase_restore, response.uppercasePasses)
            }
        }
    }

    private fun setPasswordChangeListener(view: EditText) {
        view.onFocusChangeListener =
            View.OnFocusChangeListener { _, hasFocus ->
                if (isFirstFocus) {
                    isFirstFocus = false
                    if (hasFocus) {
                        setNotEnteredImage(this.view.img_pass_length_restore)
                        setNotEnteredImage(this.view.img_numbers_contain_restore)
                        setNotEnteredImage(this.view.img_uppercase_restore)
                        setNotEnteredImage(this.view.img_lowercase_restore)
                    }
                }
            }
    }

    private fun setPasswordVisibilityChanger(view: EditText, image: ImageView) {
        image.setOnClickListener {
            passVisibility = !passVisibility
            view.transformationMethod =
                if (passVisibility) HideReturnsTransformationMethod.getInstance() else
                    PasswordTransformationMethod.getInstance()
            view.setSelection(view.text.length)
            changeEyeImage(image)
        }
    }

    private fun timerValue(value: String) {
        containerSmsCode.timer_resend_code.text = "0:%s".format(value)
    }

    private fun showResendButton(isShow: Boolean) {
        containerSmsCode.apply {
            setViewsVisibility(!isShow, ll_resend_timer)
            setViewsVisibility(isShow, ll_resend_code)
        }
    }
}