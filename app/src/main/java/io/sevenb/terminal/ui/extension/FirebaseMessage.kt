package io.sevenb.terminal.ui.extension

import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.enum_model.ErrorMessageType
import io.sevenb.terminal.exceptions.NewAuthException
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

suspend fun FirebaseMessaging.requestToken(): Result<String> =
    suspendCoroutine { continuation ->
        this.token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                continuation.resume(Result.Error(NewAuthException(ErrorMessageType.FIREBASE_TOKEN)))
                return@OnCompleteListener
            }

            task.result?.let { continuation.resume(Result.Success(it)) }
                ?: continuation.resume(Result.Error(NewAuthException(ErrorMessageType.FIREBASE_TOKEN)))
        })

    }