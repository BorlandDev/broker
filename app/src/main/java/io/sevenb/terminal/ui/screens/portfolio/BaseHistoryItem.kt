package io.sevenb.terminal.ui.screens.portfolio

import android.view.View

interface BaseHistoryItem {

    val createdAt: String
    fun bind(itemView: View, copyHash: (String) -> Unit, onClickHistory: (PortfolioHistoryItem) -> Unit)

}