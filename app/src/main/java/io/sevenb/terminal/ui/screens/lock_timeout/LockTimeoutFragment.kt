package io.sevenb.terminal.ui.screens.lock_timeout

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import io.sevenb.terminal.R
import io.sevenb.terminal.ui.base.BaseFragment
import io.sevenb.terminal.ui.extension.viewModelProvider
import kotlinx.android.synthetic.main.fragment_lock_timeout.*

class LockTimeoutFragment : BaseFragment() {

    private lateinit var lockTimeoutViewModel: LockTimeoutViewModel

    override fun layoutId() = R.layout.fragment_lock_timeout
    override fun onSearchTapListener() {}

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lockTimeoutViewModel = viewModelProvider(viewModelFactory)

        subscribeUI()
        initViews()
    }

    private fun subscribeUI() {
        lockTimeoutViewModel.apply {
            minuteLock.observe(viewLifecycleOwner, { processMinuteLock(it) })
            fiveMinutesLock.observe(viewLifecycleOwner, { processFiveMinutesLock(it) })
            halfHourLock.observe(viewLifecycleOwner, { processHalfHourLock(it) })
            hourLock.observe(viewLifecycleOwner, { processHourLock(it) })
            rebootLock.observe(viewLifecycleOwner, { processRebootLock(it) })
        }
    }

    private fun processRebootLock(value: Boolean?) {
        value?.let { smLockTimeoutReboot.isChecked = it }
    }

    private fun processHourLock(value: Boolean?) {
        value?.let { smLockTimeoutHour.isChecked = it }
    }

    private fun processHalfHourLock(value: Boolean?) {
        value?.let { smLockTimeoutHalfHour.isChecked = it }
    }

    private fun processFiveMinutesLock(value: Boolean?) {
        value?.let { smLockTimeoutFiveMinutes.isChecked = it }
    }

    private fun processMinuteLock(value: Boolean?) {
        value?.let { smLockTimeoutMinute.isChecked = it }
    }

    private fun initViews() {
        clLockTimeoutMinute.setOnClickListener { lockTimeoutViewModel.setMinuteLock(!smLockTimeoutMinute.isChecked) }
        vLockTimeoutMinute.setOnClickListener { lockTimeoutViewModel.setMinuteLock(!smLockTimeoutMinute.isChecked) }

        clLockTimeoutFiveMinutes.setOnClickListener { lockTimeoutViewModel.setFiveMinutesLock(!smLockTimeoutFiveMinutes.isChecked) }
        vLockTimeoutFiveMinutes.setOnClickListener { lockTimeoutViewModel.setFiveMinutesLock(!smLockTimeoutFiveMinutes.isChecked) }

        clLockTimeoutHalfHour.setOnClickListener { lockTimeoutViewModel.setHalfHourLock(!smLockTimeoutHalfHour.isChecked) }
        vLockTimeoutHalfHour.setOnClickListener { lockTimeoutViewModel.setHalfHourLock(!smLockTimeoutHalfHour.isChecked) }

        clLockTimeoutHour.setOnClickListener { lockTimeoutViewModel.setHourLock(!smLockTimeoutHour.isChecked) }
        vLockTimeoutHour.setOnClickListener { lockTimeoutViewModel.setHourLock(!smLockTimeoutHour.isChecked) }

        clLockTimeoutReboot.setOnClickListener { lockTimeoutViewModel.setRebootLock(!smLockTimeoutReboot.isChecked) }
        vLockTimeoutReboot.setOnClickListener { lockTimeoutViewModel.setRebootLock(!smLockTimeoutReboot.isChecked) }

        ivLockTimeoutBack.setOnClickListener { findNavController().popBackStack() }
    }
}