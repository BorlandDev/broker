package io.sevenb.terminal.ui.screens.portfolio

import android.view.View
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import coil.load
import com.github.mikephil.charting.data.Entry
import io.sevenb.terminal.R
import io.sevenb.terminal.data.model.trading.TradeChartParams
import io.sevenb.terminal.data.model.trading.TradeChartRange
import io.sevenb.terminal.domain.usecase.charts.AccountData.Companion.getBaseCurrencyCharacter
import io.sevenb.terminal.ui.extension.getDoubleValue
import io.sevenb.terminal.ui.screens.order.OrderBottomSheetFragment
import io.sevenb.terminal.ui.screens.order.OrderViewModel
import io.sevenb.terminal.ui.screens.select_currency.BaseSelectItem
import io.sevenb.terminal.ui.screens.trading.TradingWalletItem
import kotlinx.android.synthetic.main.item_asset_list.view.*
import kotlinx.android.synthetic.main.item_asset_list.view.img_icon
import kotlinx.android.synthetic.main.item_asset_list.view.tv_name
import kotlinx.android.synthetic.main.item_asset_list.view.tv_ticker
import kotlinx.android.synthetic.main.item_trading_list_main.view.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ConflatedBroadcastChannel


/**
 * Created by samosudovd on 07/06/2018.
 */

class AssetItem(
    name: String = "",
    ticker: String = "",
    cmcIconId: Int = 0,
    price: Float? = 0.0f,
    change: Float = 0.0f,
    isSellAvailable: Boolean = true,
    chartEntries: List<Entry>? = null,
    expandedChartEntries: List<Entry>? = null,
    val balance: String = "",
    override var estimated: String = "",
    var balanceInDefaultCurrency: Float? = null,
    var currentRangeAsset: TradeChartRange? = null,
    override var network: String? = "",
    override val updateTimeChannel: ConflatedBroadcastChannel<Pair<String, Long>>,
    override val updateChartRangeChannel: ConflatedBroadcastChannel<Pair<String, TradeChartRange>>,
    override var updateExpandedStateChannel: Channel<Pair<String, Boolean>>?,
    override var amount: String? = ""
) : TradingWalletItem(
    name,
    ticker,
    cmcIconId,
    price,
    change,
    isSellAvailable,
    chartEntries,
    expandedChartEntries,
    currentRange = currentRangeAsset,
    updateTimeChannel = updateTimeChannel,
    updateChartRangeChannel = updateChartRangeChannel,
    updateExpandedStateChannel = updateExpandedStateChannel
) {

    override fun bind(
        itemView: View,
        openTrade: (Pair<String, OrderBottomSheetFragment.Companion.OrderSide>) -> Unit,
        baseCurrencyTicker: String,
        updateItemChartByPosition: (TradeChartParams) -> Unit,
        position: Int,
        updateExpandSelectedRange: (Int, TradeChartRange) -> Unit,
        updateCandleChartData: (TradeChartParams) -> Unit,
        updateExpandedCandleChartData: (TradeChartParams) -> Unit
    ) {
        super.bind(
            itemView,
            openTrade,
            baseCurrencyTicker,
            updateItemChartByPosition,
            position,
            updateExpandSelectedRange,
            updateCandleChartData,
            updateExpandedCandleChartData
        )

        itemView.tv_amount.text = balance
        itemView.tv_amount_ticker.text = baseCurrencyTicker

        itemView.tv_asset_balance.isVisible = true
        val availableBalance = "${
            OrderViewModel.roundDouble(
                balance.replace(",", "").getDoubleValue().times(balanceInDefaultCurrency), 2, true
            )
        } ${getBaseCurrencyCharacter()}"
        itemView.tv_asset_balance.text = availableBalance

        itemView.tv_change.isInvisible = true
    }

    /**
     * Bind method by common search list adapter
     * @see io.sevenb.terminal.ui.screens.select_currency.SelectCurrencyAdapter
     */
    override fun bind(
        itemView: View,
        baseCurrency: String,
        itemSelected: (BaseSelectItem) -> Unit
    ) {
        if (cmcIconId == 7686) {
            itemView.img_icon.load(R.drawable.ic_e_cash_logo) {
                placeholder(R.drawable.ic_coin_placeholder)
                error(R.drawable.ic_coin_placeholder)
            }
        } else itemView.img_icon.load(CMC_ICON_URL.format(cmcIconId)) {
            placeholder(R.drawable.ic_coin_placeholder)
            error(R.drawable.ic_coin_placeholder)
        }

        ticker = if (ticker == "BCHA") "XEC" else ticker
        name = if (name == "Bitcoin Cash ABC") "eCash" else name
        network = if (network == "BCHA") "XEC" else network

        itemView.tv_ticker.text = "%s".format(ticker)
        itemView.tv_name.text = name
        itemView.tv_estimated.text = "%s %s".format(estimated, baseCurrency)
        itemView.setOnClickListener { itemSelected(this) }
    }

}

private fun Double.times(other: Float?): Double {
    return other?.let { this * it } ?: this * 1
}
