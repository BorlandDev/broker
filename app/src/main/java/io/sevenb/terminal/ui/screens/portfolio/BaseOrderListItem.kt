package io.sevenb.terminal.ui.screens.portfolio

import android.view.View

interface BaseOrderListItem {

    fun bind(itemView: View)

}