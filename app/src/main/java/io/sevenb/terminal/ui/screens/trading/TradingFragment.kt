package io.sevenb.terminal.ui.screens.trading

import android.opengl.Visibility
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.core.view.size
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.mikephil.charting.data.Entry
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.snackbar.Snackbar
import io.sevenb.terminal.R
import io.sevenb.terminal.data.model.broker_api.PriceAlertGet
import io.sevenb.terminal.data.model.trading.TradeChartParams
import io.sevenb.terminal.data.model.trading.TradeChartRange
import io.sevenb.terminal.data.repository.auth.PreferenceRepository
import io.sevenb.terminal.domain.usecase.charts.AccountData
import io.sevenb.terminal.domain.usecase.charts.AccountData.Companion.ERROR_CODE_500
import io.sevenb.terminal.ui.base.BaseFragment
import io.sevenb.terminal.ui.extension.*
import io.sevenb.terminal.ui.screens.alert.AlertAdapter
import io.sevenb.terminal.ui.screens.order.OrderBottomSheetFragment
import io.sevenb.terminal.ui.screens.order.OrderViewModel
import io.sevenb.terminal.ui.screens.portfolio.PortfolioViewModel
import io.sevenb.terminal.ui.screens.select_currency.BaseSelectItem
import io.sevenb.terminal.ui.screens.settings.SettingsFragment
import io.sevenb.terminal.ui.view.SpeedyLinearLayoutManager
import kotlinx.android.synthetic.main.fragment_portfolio.*
import kotlinx.android.synthetic.main.fragment_trading.*
import kotlinx.android.synthetic.main.fragment_trading.ticker_total_sum
import kotlinx.android.synthetic.main.fragment_trading.total_sum
import kotlinx.android.synthetic.main.fragment_trading.total_sum_base_currency
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class TradingFragment : BaseFragment() {

    private lateinit var tradingViewModel: TradingViewModel
    private lateinit var sharedViewModel: SharedViewModel

    private var priceAlertAdapter: AlertAdapter? = null
    private var tradingLoadListAdapter: TradingLoadListAdapter? = null

    private var localCurrencies: MutableList<TradingWalletItem> = mutableListOf()
    private var favoriteCurrencies: MutableList<FavoriteTradeItem> = mutableListOf()
    private var alertList: MutableList<PriceAlertGet> = mutableListOf()
    private var selectedTab = TradeTabType.ALL

    override fun layoutId() = R.layout.fragment_trading
    override fun hasBottomNavigation() = true
    override fun toolbarTitleRes() = R.string.title_trading_tab
    override fun hasToolbarSearch() = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tradingViewModel = viewModelProvider(viewModelFactory)
        sharedViewModel = activity?.run {
            viewModelProvider(viewModelFactory)
        } ?: throw Exception("Invalid Activity")

        priceAlertAdapter =
            AlertAdapter(tradingViewModel.listAlerts.value!!.toMutableList(), localCurrencies, object :
                AlertAdapter.Callback {
                override fun onItemClicked(orders: PriceAlertGet) {
                    tradingViewModel.priceAlertDelete(orders.id)
                }

            })

        tradingViewModel.listAlerts.observe(viewLifecycleOwner){
            alertList = it.toMutableList()
            priceAlertAdapter!!.setMovieListItems(it.filter { item -> item.status ==  "ACTIVE"}.toMutableList(), localCurrencies)
        }

        val tradingListAdapter = TradingListAdapter(
            ::orderScreen,
            ::updateItemChartByPosition,
            ::tradeListScrollEnabling,
            ::processFavorites,
            ::processAlert,
            ::scrollToListPosition,
            updateExpandSelectedRange = ::updateExpandSelectedRange,
            updateCandleChartData = ::updateCandleChartData,
            updateExpandedCandleChartData = ::updateCandleChartData
        )

        if (tradingViewModel.testAdapter == null) tradingViewModel.testAdapter = tradingListAdapter
        if (tradingViewModel.priceAlertAdapter == null) tradingViewModel.priceAlertAdapter =
            priceAlertAdapter!!
        initView()
        subscribeUi()
    }

    private fun initView() {
        all_tab_title.setOnClickListener {
            resetTabTitlesView()
            selectTabTitleView(it)
            selectTab(TradeTabType.ALL)
        }
        favorites_tab_title.setOnClickListener {
            resetTabTitlesView()
            selectTabTitleView(it)
            selectTab(TradeTabType.FAVORITE)
        }

        alert_tab_title.setOnClickListener {
            resetTabTitlesView()
            selectTabTitleView(it)
            selectTab(TradeTabType.PRICE_ALERT)
        }

        open_search.setOnClickListener {
            sharedViewModel.selectCurrencyList.value = when (selectedTab) {
                TradeTabType.ALL -> localCurrencies
                TradeTabType.FAVORITE -> favoriteCurrencies
                TradeTabType.PRICE_ALERT -> favoriteCurrencies
            }
            currencySelection()
        }

        initTradingList()
        if (SettingsFragment.hasUpdated) tradingViewModel.marketRate()
        startShimmer(true)
    }

    private fun resetTabTitlesView() {
        context?.let {
            all_tab_title.setBackgroundResource(0)
            alert_tab_title.setBackgroundResource(0)
            favorites_tab_title.setBackgroundResource(0)
            all_tab_title.setTextColor(ContextCompat.getColor(it, R.color.colorTextSecond))
            alert_tab_title.setTextColor(ContextCompat.getColor(it, R.color.colorTextSecond))
            favorites_tab_title.setTextColor(ContextCompat.getColor(it, R.color.colorTextSecond))
        }
    }

    private fun updateExpandSelectedRange(position: Int, chartRange: TradeChartRange) {
        if (selectedTab == TradeTabType.FAVORITE && position < favoriteCurrencies.size) {
            favoriteCurrencies[position].currentRange = chartRange
        }
    }

    private fun selectTabTitleView(view: View) {
        view as TextView
        context?.let {
            view.newBackground(R.drawable.bg_tab_title_selected)
            view.newTextColor(R.color.colorTextMain)
        }
    }

    private fun selectTab(tradeTabType: TradeTabType) {
        selectedTab = tradeTabType
        when (tradeTabType) {
            TradeTabType.ALL -> {
                trading_list?.adapter = tradingViewModel.testAdapter
                tradingViewModel.testAdapter?.submitList(localCurrencies)
            }
            TradeTabType.FAVORITE -> {
                trading_list?.adapter = tradingViewModel.testAdapter
                tradingViewModel.testAdapter?.submitList(favoriteCurrencies)
            }
            TradeTabType.PRICE_ALERT -> {
                trading_list?.adapter = tradingViewModel.priceAlertAdapter
                tradingViewModel.priceAlertInfo()
            }
        }
    }

    private fun subscribeUi() {
        tradingViewModel.snackbarMessage.observe(viewLifecycleOwner) { showSnackBarMessage(it) }
        tradingViewModel.totalHolding.observe(viewLifecycleOwner) { totalHolding(it) }
        tradingViewModel.chartDataResult.observe(viewLifecycleOwner) { chartDataResult(it) }
        tradingViewModel.priceResult.observe(viewLifecycleOwner) { priceResult(it) }
        tradingViewModel.currencyList.observe(viewLifecycleOwner) { tradingList(it) }
        tradingViewModel.favoriteList.observe(viewLifecycleOwner) { favoriteList(it) }
        tradingViewModel.baseCurrency.observe(viewLifecycleOwner) { baseCurrency(it) }
        tradingViewModel.updateTradingWalletItem.observe(viewLifecycleOwner) { updateWalletItem() }
        tradingViewModel.rate.observe(viewLifecycleOwner) { baseCurrencyAvailableSum(it) }
        tradingViewModel.totalInBaseCurrency.observe(viewLifecycleOwner) { baseCurrencyAmount(it) }
        tradingViewModel.notifyRecyclerView.observe(viewLifecycleOwner) { notifyRecyclerView() }
        tradingViewModel.updateCandleChartData.observe(
            viewLifecycleOwner
        ) { processCandleUpdate(it) }

        sharedViewModel.selectedCurrency.observe(viewLifecycleOwner) { selectedSearchItem(it) }
    }

    private fun processCandleUpdate(position: Int) {
//        tradingListAdapter.notifyItemChanged(position)
    }

    private fun notifyRecyclerView() {
        trading_list.adapter?.notifyDataSetChanged()
    }

    private fun updateCandleChartData(tradeChartParams: TradeChartParams) {
        tradingViewModel.updateCandleChartData(tradeChartParams)
    }

    private fun baseCurrencyAmount(amount: String) {
        val amountWithCharacter = "${AccountData.getBaseCurrencyCharacter()} $amount"
        total_sum_base_currency.text = amountWithCharacter
    }

    private fun baseCurrencyAvailableSum(rate: Float) {
        val holding = tradingViewModel.totalHolding.value?.toFloatOrNull()
        if (holding != null) {
            val convertedValue = (holding * rate)
            tradingViewModel.updateTotalInBaseCurrency(
                OrderViewModel.roundDouble(convertedValue.toDouble(), 2, true).replace(",", ".")
            )
        }
    }

    private fun initTradingList() {
        trading_list.apply {
            itemAnimator = null
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(
                context,
                LinearLayoutManager.VERTICAL, false
            )
//            layoutManager = SpeedyLinearLayoutManager(
//                context,
//                LinearLayoutManager.VERTICAL, false
//            )
            adapter = tradingViewModel.testAdapter
        }
        tradingLoadListAdapter = TradingLoadListAdapter(MutableList(10){0})
        trading_list_load.apply {
            layoutManager = LinearLayoutManager(
                context,
                LinearLayoutManager.VERTICAL, false
            )

            adapter = tradingLoadListAdapter
        }
    }

    private fun updateWalletItem() {
        tradingViewModel.testAdapter?.notifyDataSetChanged() //TODO optimize this
    }

    private fun totalHolding(sumString: String) {
        total_sum.text = sumString
    }

    private fun tradingList(currencies: List<TradingWalletItem>) {
        showProgressBar(false)
        startShimmer(false)
        when (selectedTab) {
            TradeTabType.ALL -> {
                tradingViewModel.testAdapter?.submitList(currencies)
            }
            TradeTabType.FAVORITE -> {
                tradingViewModel.testAdapter?.submitList(favoriteCurrencies)
            }
            TradeTabType.PRICE_ALERT -> {
                tradingViewModel.priceAlertAdapter?.setMovieListItems(alertList.filter { item -> item.status ==  "ACTIVE"}.toMutableList(), currencies)
            }
        }
        localCurrencies = currencies.toMutableList()
        sharedViewModel.selectCurrencyList.value = localCurrencies
    }

    private fun favoriteList(currencies: List<FavoriteTradeItem>) {
        favoriteCurrencies = currencies.toMutableList()
        sharedViewModel.selectCurrencyList.value = favoriteCurrencies
    }

    private fun baseCurrency(ticker: String) {
        tradingViewModel.testAdapter?.baseCurrencyTicker = AccountData.getBaseCurrencyCharacter()
        ticker_total_sum.text = AccountData.BTC
    }

    private fun orderScreen(tickerAndType: Pair<String, OrderBottomSheetFragment.Companion.OrderSide>) {
        if (findNavController().currentDestination?.id != R.id.tradingFragment) return

        sharedViewModel.orderTicker.value = tickerAndType
        val direction = TradingFragmentDirections.actionTradingFragmentToOrderBottomSheetFragment()
        findNavController().navigate(direction)
    }

    private fun updateItemChartByPosition(tradeChartParams: TradeChartParams) {
        val list = when (selectedTab) {
            TradeTabType.ALL -> localCurrencies
            TradeTabType.FAVORITE -> favoriteCurrencies
            TradeTabType.PRICE_ALERT -> favoriteCurrencies
        }
        if (tradeChartParams.listPosition < list.size) {
            val item = list[tradeChartParams.listPosition]
            tradingViewModel.updateItemChartData(item.ticker, tradeChartParams)
//            tradingViewModel.updateChartData(tradeChartParams)
        }
    }

    private fun tradeListScrollEnabling(isEnabled: Boolean) {
        try {
            trading_list.suppressLayout(!isEnabled)
        } catch (e: IllegalStateException) {
        }
    }

    private fun processFavorites(tradingWalletItem: TradingWalletItem) {
        tradingViewModel.processFavorite(tradingWalletItem)
    }

    private fun processPriceAlert() {
        tradingViewModel.priceAlertInfo()
    }

    private fun processAlert(tradingWalletItem: TradingWalletItem) {
        if (findNavController().currentDestination?.id != R.id.tradingFragment) return

        val action = TradingFragmentDirections.actionTradingFragmentToPriceAlertFragment(
            tradingWalletItem.ticker,
            tradingWalletItem.price.toString()
        )
        findNavController().navigate(action)
    }

    private fun showProgressBar(value: Boolean) {
//        progress_trade.visible(value)
        trading_list.visible(!value)
    }

    private fun chartDataResult(pairResult: Pair<TradeChartParams, List<Entry>>) {
        val tradeChartParams = pairResult.first
        val listEntries = pairResult.second

        updateList(localCurrencies, tradeChartParams, listEntries, TradeTabType.ALL)
        updateList(favoriteCurrencies, tradeChartParams, listEntries, TradeTabType.FAVORITE)

        val payload = mutableListOf<Any>()
        payload.add(listEntries)
        payload.add(tradeChartParams)

        tradingViewModel.testAdapter?.notifyItemChanged(tradeChartParams.listPosition, payload)
    }

    private fun <T : TradingWalletItem> updateList(
        listToUpdate: MutableList<T>,
        tradeChartParams: TradeChartParams,
        listEntries: List<Entry>,
        type: TradeTabType
    ) {
        tradingViewModel.getPositionByTicker(tradeChartParams.ticker, listToUpdate)?.let {
            if (tradeChartParams.tradeChartRange == TradeChartRange.DAY) {
                listToUpdate[it].chartEntries = listEntries
                listToUpdate[it].expandedChartEntries = listEntries
            } else {
                if (type == TradeTabType.FAVORITE) {
                    val item =
                        favoriteCurrencies.firstOrNull { cur -> cur.ticker == tradeChartParams.ticker }
                    val position = item?.let { favoriteCurrencies.indexOf(item) }
                    position?.let {
                        favoriteCurrencies[position].currentRange = tradeChartParams.tradeChartRange
                    }
                }
                listToUpdate[it].expandedChartEntries = listEntries
            }
        }
    }

    private fun priceResult(pairResult: Pair<Int, String>) {
        val position = pairResult.first
        val price = pairResult.second

        localCurrencies[position].price = price.replace(",", "").toFloatOrNull()

        val payload = mutableListOf<Any>()
        payload.add(price)

        tradingViewModel.testAdapter?.notifyItemChanged(position, payload)
    }

    private fun selectedSearchItem(baseSelectItem: BaseSelectItem?) {
        if (baseSelectItem !is TradingWalletItem) return
        sharedViewModel.selectedCurrency.value = null

        val index = localCurrencies.indexOf(baseSelectItem)
        scrollToListPosition(index)
    }

    private fun scrollToListPosition(position: Int) {
        trading_list?.scrollToPosition(position)
    }

    override fun onSearchTapListener() {}

    private fun currencySelection() {
        if (findNavController().currentDestination?.id != R.id.tradingFragment) return

        val direction =
            TradingFragmentDirections.actionTradingFragmentToSelectCurrencyFragment()
        findNavController().navigate(direction)
    }

    override fun onDestroyView() {
        if (trading_list.size > 0) sharedViewModel.selectedCurrency.value = localCurrencies[0]
        super.onDestroyView()
    }

    private fun showSnackBarMessage(message: String) {
        showProgressBar(false)
        val bottomNavView: BottomNavigationView? = activity?.findViewById(R.id.bottomNavView)
        bottomNavView?.let {
            if (!message.contains(ERROR_CODE_500))
                Snackbar.make(bottomNavView, message, Snackbar.LENGTH_SHORT).apply {
                    anchorView = bottomNavView
                }.show()
        }
    }

    private fun startShimmer(value: Boolean) {
//        sfl_markets.visible(value)
        trading_list.visible(!value)
        if (value) {
            tradingLoadListAdapter?.let { it.list =  MutableList(10){0}}
            trading_list_load.visibility = View.VISIBLE
        }
        else {
            trading_list_load.visibility = View.GONE
            tradingLoadListAdapter?.let { it.list =  MutableList(0){0}}
        }
    }

    override fun onPause() {
        startShimmer(false)
        tradingViewModel.shouldUpdateCurrencies = false
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        tradingViewModel.shouldUpdateCurrencies = true
//        lifecycleScope.launch {
//            delay(300)
//            hideKeyboard(requireActivity())
//        }
    }

    private enum class TradeTabType {
        ALL, FAVORITE, PRICE_ALERT
    }
}
