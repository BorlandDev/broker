package io.sevenb.terminal.ui.screens.scan_qr

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.PointF
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.OnRequestPermissionsResultCallback
import com.dlazaro66.qrcodereaderview.QRCodeReaderView
import com.dlazaro66.qrcodereaderview.QRCodeReaderView.OnQRCodeReadListener
import com.google.android.material.snackbar.Snackbar
import io.sevenb.terminal.R
import kotlinx.android.synthetic.main.activity_scan_qr.*

/**
 * Created by samosudovd on 11/07/2018.
 */
class DecoderActivity : AppCompatActivity(), OnRequestPermissionsResultCallback,
    OnQRCodeReadListener {

    private lateinit var qrDecoderView: QRCodeReaderView
    private lateinit var closeScannerImageView: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scan_qr)
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
            == PackageManager.PERMISSION_GRANTED
        ) {
            initQRCodeReaderView()
        } else {
            requestCameraPermission()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode != MY_PERMISSION_REQUEST_CAMERA) {
            return
        }
        if (grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Snackbar.make(
                main_layout,
                R.string.message_camera_permission_granted,
                Snackbar.LENGTH_SHORT
            ).show()
            initQRCodeReaderView()
        } else {
            Snackbar.make(
                main_layout,
                R.string.message_camera_permission_denied,
                Snackbar.LENGTH_SHORT
            ).show()
        }
    }

    // Called when a QR is decoded
    // "text" : the text encoded in QR
    // "points" : points where QR control points are placed in View
    override fun onQRCodeRead(text: String, points: Array<PointF>) {
        val returnIntent = Intent()
        returnIntent.putExtra(QR_CODE_RESULT, text)
        setResult(RESULT_OK, returnIntent)
        finish()
    }

    override fun onResume() {
        super.onResume()
        if (::qrDecoderView.isInitialized) qrDecoderView.startCamera()
    }

    override fun onPause() {
        super.onPause()
        if (::qrDecoderView.isInitialized) qrDecoderView.stopCamera()
    }

    private fun initQRCodeReaderView() {
        val content: View = layoutInflater.inflate(R.layout.content_scanner_qr, main_layout, true)

        closeScannerImageView = content.findViewById(R.id.iv_close_qr)
        closeScannerImageView.setOnClickListener { finish() }

        qrDecoderView = content.findViewById(R.id.qrdecoderview)
        qrDecoderView.setAutofocusInterval(2000L)
        qrDecoderView.setOnQRCodeReadListener(this)
        qrDecoderView.setBackCamera()
        qrDecoderView.startCamera()
    }

    private fun requestCameraPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
            Snackbar.make(
                main_layout, getString(R.string.message_camera_access),
                Snackbar.LENGTH_INDEFINITE
            ).setAction("OK") {
                ActivityCompat.requestPermissions(
                    this@DecoderActivity, arrayOf(
                        Manifest.permission.CAMERA
                    ), MY_PERMISSION_REQUEST_CAMERA
                )
            }.show()
        } else {
            Snackbar.make(
                main_layout, getString(R.string.message_request_camera_permission),
                Snackbar.LENGTH_SHORT
            ).show()
            ActivityCompat.requestPermissions(
                this, arrayOf(
                    Manifest.permission.CAMERA
                ), MY_PERMISSION_REQUEST_CAMERA
            )
        }
    }

    companion object {
        private const val MY_PERMISSION_REQUEST_CAMERA = 0
        const val QR_CODE_RESULT = "QR_CODE_RESULT"
    }
}