package io.sevenb.terminal.ui.screens.confirm_phone

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import io.sevenb.terminal.R
import io.sevenb.terminal.ui.base.BaseFragment
import io.sevenb.terminal.ui.extension.viewModelProvider
import io.sevenb.terminal.ui.screens.trading.SharedViewModel
import kotlinx.android.synthetic.main.fragment_auth_verification_code.*
import kotlinx.android.synthetic.main.fragment_confirm_phone.*
import timber.log.Timber

class ConfirmPhoneFragment : BaseFragment() {

    private lateinit var confirmPhoneViewModel: ConfirmPhoneViewModel
    private lateinit var sharedViewModel: SharedViewModel

    override fun layoutId() = R.layout.fragment_confirm_phone
    override fun hasToolbarSearch() = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        confirmPhoneViewModel = viewModelProvider(viewModelFactory)
        sharedViewModel = activity?.run {
            viewModelProvider(viewModelFactory)
        } ?: throw Exception("Invalid Activity")

        initView()
        subscribeUi()

        confirmPhoneViewModel.requestConfirmPhoneCode()
    }

    private fun initView() {
        close_screen.isVisible = true
        close_screen.setOnClickListener { closeScreen() }

        action_change_email.isVisible = false

        button_verify_code.setOnClickListener {
            val confirmationCode = input_verification_code.text.toString()
            confirmPhoneViewModel.confirmPhone(confirmationCode)
        }

        ll_resend_code.setOnClickListener {
            confirmPhoneViewModel.resendConfirmationCode()
        }
    }

    override fun onSearchTapListener() {
        Timber.d("onSearchTapListener ")
    }

    private fun subscribeUi() {
        confirmPhoneViewModel.phoneConfirmed.observe(viewLifecycleOwner, { phoneConfirmed() })
        confirmPhoneViewModel.timerValue.observe(viewLifecycleOwner, { timerValue(it) })
        confirmPhoneViewModel.showResendButton.observe(viewLifecycleOwner, { showResendButton(it) })
        confirmPhoneViewModel.snackbarMessage.observe(viewLifecycleOwner, { showSnackbar(it) })

        sharedViewModel.confirmPhoneNumber.observe(viewLifecycleOwner, { phoneNumberToConfirm(it) })
    }

    private fun phoneConfirmed() {
        showSnackbar(getString(R.string.message_phone_confirmed))
        closeScreen()
    }

    private fun closeScreen() {
        findNavController().popBackStack(R.id.settingsFragment, false)
    }

    private fun timerValue(value: String) {
        timer_resend_code.text = "0:%s".format(value)
    }

    private fun showResendButton(isShow: Boolean) {
        if (isShow) {
            ll_resend_timer.visibility = View.GONE
            ll_resend_code.visibility = View.VISIBLE
        } else {
            ll_resend_timer.visibility = View.VISIBLE
            ll_resend_code.visibility = View.GONE
        }
    }

    private fun phoneNumberToConfirm(phone: String) {
        email_code_sent_to.text = phone
    }

    private fun showSnackbar(message: String) {
        Snackbar.make(container_confirm_phone, message, Snackbar.LENGTH_SHORT).show()
    }

}