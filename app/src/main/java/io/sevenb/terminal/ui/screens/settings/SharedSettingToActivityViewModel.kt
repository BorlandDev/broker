package io.sevenb.terminal.ui.screens.settings

import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import javax.inject.Inject

class SharedSettingToActivityViewModel @Inject constructor() : ViewModel() {

    val createPassCodeFromSettings = MutableLiveData<Pair<Boolean?, Fragment?>>()
    val enterPassCodeFromSettings = MutableLiveData<Pair<Boolean?, Fragment?>>()

    val passCodeWasCreated = MutableLiveData<Boolean?>()
    val passCodeWasEntered = MutableLiveData<Boolean?>()

    val tfaWasChanged = MutableLiveData<Boolean?>()

    val message = MutableLiveData<String>()
}