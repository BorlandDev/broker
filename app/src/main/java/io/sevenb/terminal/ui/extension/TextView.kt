package io.sevenb.terminal.ui.extension

import android.graphics.Paint
import android.widget.TextView
import androidx.core.content.ContextCompat
import io.sevenb.terminal.R
import java.text.DecimalFormat


fun TextView.newTextColor(colorResource: Int) {
    val context = this.context
    context?.let { this.setTextColor(ContextCompat.getColor(this.context, colorResource)) }
}

fun TextView.underlineText(value: Boolean) {
    if (this.text.isNullOrEmpty()) return

    if (value) {
        val p = Paint()
        p.color = ContextCompat.getColor(this.context, R.color.colorBlue)
        p.flags = Paint.UNDERLINE_TEXT_FLAG
        this.paintFlags = p.flags
    } else {
        this.paintFlags = 0
    }
}

fun TextView.getString(): String {
    return try {
        this.text.toString()
    } catch (e: Exception) {
        ""
    }
}
