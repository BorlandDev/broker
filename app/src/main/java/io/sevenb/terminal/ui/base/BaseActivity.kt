package io.sevenb.terminal.ui.base

import android.content.Context
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import dagger.android.support.DaggerAppCompatActivity
import io.sevenb.terminal.R
import io.sevenb.terminal.ui.view.AppToolbar

abstract class BaseActivity : DaggerAppCompatActivity() {

    private lateinit var toolbarLayout: AppToolbar

    fun initToolbar(titleResId: Int?, hasBackArrow: Boolean?, hasSearch: Boolean) {
        toolbarLayout = findViewById(R.id.appToolbar) ?: return
        setupToolbar(toolbarLayout, titleResId, hasBackArrow, hasSearch)
    }

    private fun setupToolbar(
        toolbar: AppToolbar?,
        titleRes: Int?,
        hasBackArrow: Boolean?,
        hasSearch: Boolean
    ) {
        if (toolbar == null) {
            return
        }

        setSupportActionBar(toolbar.toolbar)

        supportActionBar?.title = ""
        if (titleRes != null) {
            toolbar.toolbar.title = getString(titleRes)
        }

        if (hasBackArrow != null) {
            supportActionBar?.setDisplayHomeAsUpEnabled(hasBackArrow)
            supportActionBar?.setDisplayShowHomeEnabled(hasBackArrow)

            if (hasBackArrow) {
                toolbar.toolbar.setNavigationOnClickListener {
                    onBackPressed()
                }
            }
        }

        toolbar.ivOpenSearch.visibility = if (hasSearch) View.VISIBLE else View.GONE
    }

//    fun showKeyboard() {
//        (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
//            .toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
//    }
//
//    fun showKeyboard(editText: EditText?) {
//        (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
//            .showSoftInput(editText, InputMethodManager.SHOW_FORCED)
//    }

    fun hideKeyboard() {
        val imm = (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
        val view = this.currentFocus
        imm.hideSoftInputFromWindow(view?.windowToken, 0)
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        // hide keyboard on touch outside
        if (currentFocus != null) {
            hideKeyboard()
        }
        return super.dispatchTouchEvent(ev)
    }

}
