package io.sevenb.terminal.ui.extension

import java.math.BigDecimal
import java.math.RoundingMode

operator fun BigDecimal.plus(a: BigDecimal): BigDecimal = this.add(a)

operator fun BigDecimal.minus(a: BigDecimal): BigDecimal = this.subtract(a)

operator fun BigDecimal.times(a: BigDecimal): BigDecimal = this.multiply(a)

operator fun BigDecimal.div(a: BigDecimal): BigDecimal {
    return try {
        this.divide(a, 10, RoundingMode.HALF_UP)
    } catch (e: ArithmeticException) {
        e.printStackTrace()
        this.divide(1.toBigDecimal(), 2, RoundingMode.HALF_UP)
    }
}
fun BigDecimal.div(a: BigDecimal, precision: Int): BigDecimal {
    return try {
        this.divide(a, precision, RoundingMode.HALF_UP)
    } catch (e: ArithmeticException) {
        e.printStackTrace()
        this.divide(1.toBigDecimal(), 2, RoundingMode.HALF_UP)
    }
}