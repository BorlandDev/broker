package io.sevenb.terminal.ui.extension



fun Float.round(signs: Int = 9) = String.format("%.${signs}f", this)

operator fun Float?.times(value: Float?): Float {
    return if (this != null && value != null) {
        this * value
    } else 0.0F
}

fun Float.toPlainString(): String {
    return try {
        this.toBigDecimal().toPlainString()
    } catch (e: Exception) {
        ""
    }
}

fun Double.toPlainString(): String {
    return try {
        this.toBigDecimal().toPlainString()
    } catch (e: Exception) {
        ""
    }
}