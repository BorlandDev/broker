package io.sevenb.terminal.ui.screens.auth.change_password

import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.widget.ImageView
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.geetest.sdk.GT3ConfigBean
import com.geetest.sdk.GT3ErrorBean
import com.geetest.sdk.GT3GeetestUtils
import com.geetest.sdk.GT3Listener
import com.geetest.sdk.utils.GT3ServiceNode
import com.google.android.material.snackbar.Snackbar
import io.sevenb.terminal.R
import io.sevenb.terminal.data.model.enum_model.ErrorMessageType
import io.sevenb.terminal.data.model.enums.AuthState
import io.sevenb.terminal.data.model.enums.GeeTestResult
import io.sevenb.terminal.data.model.enums.pass_code.PassCodeStates
import io.sevenb.terminal.domain.usecase.new_auth.HandleFingerprint
import io.sevenb.terminal.ui.base.BaseFragment
import io.sevenb.terminal.ui.extension.*
import io.sevenb.terminal.ui.screens.safety.passcode_state.PassCodeFlow
import io.sevenb.terminal.ui.screens.safety.passcode_state.PassCodeViewModel
import io.sevenb.terminal.ui.screens.safety.SafetyViewModel
import io.sevenb.terminal.ui.screens.settings.NotifySettingsViewModel
import kotlinx.android.synthetic.main.view_auth_password_hint.*
import kotlinx.android.synthetic.main.fragment_auth_verification_code.*
import kotlinx.android.synthetic.main.fragment_change_password.*
import kotlinx.android.synthetic.main.fragment_change_password_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.json.JSONObject
import timber.log.Timber
import javax.inject.Inject

class ChangePasswordFragment : BaseFragment() {
    private lateinit var changePasswordViewModel: ChangePasswordViewModel
    private val notifySettingsViewModel: NotifySettingsViewModel by activityViewModels { viewModelFactory }

    private lateinit var gT3GeetestUtils: GT3GeetestUtils
    private lateinit var gt3ConfigBean: GT3ConfigBean

    var snackbarCurrentlyShowing = false
    private var passVisibility = false
    private var isFirstFocus = true

    @Inject
    lateinit var handleFingerprint: HandleFingerprint

    @Inject
    lateinit var passCodeFlow: PassCodeFlow
    private lateinit var passCodeViewModel: PassCodeViewModel
    private lateinit var safetyViewModel: SafetyViewModel

    override fun layoutId() = R.layout.fragment_change_password_main
    override fun onSearchTapListener() {}

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        changePasswordViewModel = viewModelProvider(viewModelFactory)
        passCodeViewModel = viewModelProvider(viewModelFactory)
        safetyViewModel = viewModelProvider(viewModelFactory)
//        notifySettingsViewModel = viewModelProvider(viewModelFactory)

        initViews()
        subscribeUI()
        startCaptchaFlow()
    }

    private fun subscribeUI() {
        changePasswordViewModel.apply {
            showCaptcha.observe(viewLifecycleOwner, { showCaptcha(it) })
            geeTestResult.observe(viewLifecycleOwner, { catchGeeTestResult(it) })
            state.observe(viewLifecycleOwner, { processState(it) })
            timerValue.observe(viewLifecycleOwner, { timerValue(it) })
            showResendButton.observe(viewLifecycleOwner, { showResendButton(it) })
            finishListener.observe(viewLifecycleOwner, {
                showSnackbar("Password was changed")
                notifySettingsViewModel.notifyActToUpdateSefetyData.value = true
                findNavController().popBackStack()
            })
            errorString.observe(viewLifecycleOwner, { showSnackbar(it) })
            createPassCode.observe(viewLifecycleOwner, { processCreatePassCode(it) })
            errorMessage.observe(viewLifecycleOwner, { processErrorMessage(it) })
            subscribeUI()
        }

        passCodeViewModel.authDone.observe(
            viewLifecycleOwner, { changePasswordViewModel.finishListener.value = Unit })
    }

    //TODO REFACTOR and SPLIT ERRORS
    private fun processErrorMessage(errorMessage: ErrorMessageType?) {
        when (errorMessage) {
            ErrorMessageType.CAPTCHA_TIMEOUT -> {
                showSnackbar(getString(R.string.error_captcha_timeout))
            }
            ErrorMessageType.CAPTCHA_LOST_CONNECTION -> {
                showSnackbar(getString(R.string.error_captcha_connection))
            }
            ErrorMessageType.CAPTCHA_GET_PARSE_JSON -> {
                showSnackbar(getString(R.string.error_captcha_common, errorMessage.ordinal))
            }
            ErrorMessageType.CAPTCHA_POST_PARSE_JSON -> {
                showSnackbar(getString(R.string.error_captcha_common, errorMessage.ordinal))
            }
            ErrorMessageType.CAPTCHA_TOKEN -> {
                showSnackbar(getString(R.string.error_captcha_common, errorMessage.ordinal))
            }
            ErrorMessageType.CAPTCHA_BAD_PARAMS -> {
                showSnackbar(getString(R.string.error_captcha_common, errorMessage.ordinal))
            }
            ErrorMessageType.CAPTCHA_TOO_MANY_ATTEMPTS -> {
                showSnackbar(getString(R.string.error_captcha_common, errorMessage.ordinal))
            }
            ErrorMessageType.CAPTCHA_RESEND -> {
                showSnackbar(getString(R.string.error_captcha_common, errorMessage.ordinal))
            }
            ErrorMessageType.CAPTCHA_CONFIRM -> {
                showSnackbar(getString(R.string.error_captcha_common, errorMessage.ordinal))
            }
            else -> {
                showSnackbar(getString(R.string.error_auth_common, errorMessage?.ordinal ?: 0))
            }
        }
    }

    private fun processCreatePassCode(value: Boolean?) {
        value?.let {

            if (it) initPassCode()
        }
    }

    private fun initPassCode() {
        containerPassCode.visible(true)
        passCodeFlow.initLifecycleOwner(viewLifecycleOwner, this, handleFingerprint)
        passCodeFlow.initViews(
            containerPassCode,
            passCodeViewModel,
            safetyViewModel,
            PassCodeStates.CREATE_PASS_CODE,
            changePasswordViewModel.userData.email,
            changePasswordViewModel.userData.password
        )
    }

    private fun processState(state: AuthState?) {
        when (state) {
            AuthState.SMS_CODE -> {
                setGoneViews(progress_change_password, container_change_password)
                container_verification_code.visible(true)
                setInvisibleViews(action_change_email)
                changePasswordViewModel.userData.email?.let { email_code_sent_to.text = it }
            }
            AuthState.CREATING_PASSWORD -> {
                input_password_change.onActionDoneListener { validatePasswordOnRestore() }
                input_password_change.focusField(requireContext())
                setGoneViews(progress_change_password, container_verification_code)
                container_change_password.visible(true)
                button_send_code_change_password.setOnClickListener {
                    hideKeyboard(requireActivity())
                    validatePasswordOnRestore()
                }
            }
        }
    }

    private fun showResendButton(isShow: Boolean) {
        setViewsVisibility(!isShow, ll_resend_timer)
        setViewsVisibility(isShow, ll_resend_code)
    }

    private fun initViews() {
        initCaptchaConfig()

        button_verify_code.setOnClickListener { confirmCode() }
        input_verification_code.onActionDoneListener { confirmCode() }

        img_pass_visibility_change_password.setOnClickListener {
            passVisibility = !passVisibility
            input_password_change.transformationMethod =
                if (passVisibility) HideReturnsTransformationMethod.getInstance() else
                    PasswordTransformationMethod.getInstance()
            input_password_change.cursorToLastIfSelected()

            changeEyeImage(it)
        }

        input_password_change.onFocusChangeListener =
            View.OnFocusChangeListener { _, hasFocus ->
                if (isFirstFocus) {
                    isFirstFocus = false
                    if (hasFocus) {
                        setNotEnteredImage(img_pass_length)
                        setNotEnteredImage(img_numbers_contain)
                        setNotEnteredImage(img_uppercase)
                        setNotEnteredImage(img_lowercase)
                    }
                }
            }

        input_password_change.doOnTextChanged { text, _, _, _ ->
            text?.let {
                val response = changePasswordViewModel.checkPassword(it.toString())

                setImage(img_pass_length, response.lengthPasses)
                setImage(img_numbers_contain, response.numbersPasses)
                setImage(img_lowercase, response.lowercasePasses)
                setImage(img_uppercase, response.uppercasePasses)
            }
        }

        ll_resend_code.setOnClickListener {
            changePasswordViewModel.smsCodeRestorePassword()
        }
    }

    private fun setImage(imageView: ImageView, value: Boolean) =
        if (value) setEnteredImage(imageView) else setNotEnteredImage(imageView)

    private fun setEnteredImage(imageView: ImageView) =
        imageView.newBackground(R.drawable.ic_entered)

    private fun setNotEnteredImage(imageView: ImageView) =
        imageView.newBackground(R.drawable.ic_not_entered)

    private fun timerValue(value: String) {
        timer_resend_code.text = "0:%s".format(value)
    }

    private fun confirmCode() {
        changePasswordViewModel.restoreConfirmationCode(input_verification_code.getString())
    }

    private fun startCaptchaFlow() {
        gT3GeetestUtils.startCustomFlow()
    }

    private fun catchGeeTestResult(geeTestResult: GeeTestResult) {
        when (geeTestResult) {
            GeeTestResult.SUCCESS -> gT3GeetestUtils.showSuccessDialog()
            GeeTestResult.FAIL -> gT3GeetestUtils.showFailedDialog()
            GeeTestResult.USER_EXISTS -> gT3GeetestUtils.dismissGeetestDialog()
        }
    }

    private fun changeEyeImage(imageView: View) {
        imageView.newBackground(if (passVisibility) R.drawable.ic_password else R.drawable.ic_password_hidden)
    }

    private fun initCaptchaConfig() {
        gT3GeetestUtils = GT3GeetestUtils(activity)
        gt3ConfigBean = GT3ConfigBean()
        gt3ConfigBean.pattern = 1
        gt3ConfigBean.isUnCanceledOnTouchKeyCodeBack = true
        gt3ConfigBean.isCanceledOnTouchOutside = true
        gt3ConfigBean.lang = null
        gt3ConfigBean.timeout = 10000
        gt3ConfigBean.webviewTimeout = 10000
        gt3ConfigBean.gt3ServiceNode = GT3ServiceNode.NODE_CHINA
        gt3ConfigBean.isCanceledOnTouchOutside = false

        gt3ConfigBean.listener = object : GT3Listener() {
            override fun onSuccess(p0: String?) {
                input_verification_code.focusField(requireContext())
                Timber.d("gt3ConfigBean onSuccess $p0")
            }

            override fun onFailed(errorBean: GT3ErrorBean?) {
                Timber.d("gt3ConfigBean onFailed ${errorBean.toString()}")
                findNavController().popBackStack()
            }

            override fun onReceiveCaptchaCode(p0: Int) {
                Timber.d("gt3ConfigBean onReceiveCaptchaCode $p0")
            }

            override fun onStatistics(p0: String?) {
                Timber.d("gt3ConfigBean onStatistics $p0")
            }

            override fun onClosed(p0: Int) {
                findNavController().popBackStack()
            }

            override fun onButtonClick() {
                changePasswordViewModel.captcha(0, "")
            }

            override fun onDialogReady(p0: String?) {
                Timber.d("gt3ConfigBean onDialogReady $p0")
                super.onDialogReady(p0)
            }

            override fun onDialogResult(result: String?) {
                Timber.d("gt3ConfigBean onDialogResult $result")
                result ?: return

                changePasswordViewModel.captcha(1, result)
            }
        }

        gT3GeetestUtils.init(gt3ConfigBean)
    }

    private fun showCaptcha(jsonobj: JSONObject?) {
        gt3ConfigBean.api1Json = jsonobj
        gT3GeetestUtils.getGeetest()
    }

    override fun onDestroy() {
        super.onDestroy()
        hideKeyboard(requireActivity())
        if (::gT3GeetestUtils.isInitialized) gT3GeetestUtils.destory()
    }

    private fun validatePasswordOnRestore() {
        changePasswordViewModel.validatePassword(input_password_change.getString())
    }

    private fun showSnackbar(message: String) {
        snackbarCurrentlyShowing = true
        if (message != "Fingerprint created")
            Snackbar.make(cl_change_password_container, message, Snackbar.LENGTH_LONG).show()
        showProgressBarSignUp(false)
        CoroutineScope(Dispatchers.Main).launch {

            delay(3000)
            snackbarCurrentlyShowing = false

            input_password_change?.let {
                it.focusField(requireContext())
            }
        }
    }

    private fun showProgressBarSignUp(value: Boolean) {
        progress_change_password.visible(value)
    }
}