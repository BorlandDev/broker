package io.sevenb.terminal.ui.view

import android.content.Context
import android.util.DisplayMetrics
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.utils.MPPointF
import io.sevenb.terminal.AppActivity
import io.sevenb.terminal.ui.screens.trading.TradingListAdapter.Companion.formatCryptoAmount
import kotlinx.android.synthetic.main.view_custom_marker.view.*

class CustomMarker(context: Context, layoutResource: Int) : MarkerView(context, layoutResource) {
    override fun refreshContent(entry: Entry?, highlight: Highlight?) {
        val value = entry?.y?.toDouble() ?: 0.0
        val resText = formatCryptoAmount(value.toFloat())

        val displayMetrics = DisplayMetrics()
        (context as AppActivity).windowManager
            .defaultDisplay
            .getMetrics(displayMetrics)
        val width = displayMetrics.widthPixels
        viewWidth = width.toFloat()
        tvPrice.text = resText
        super.refreshContent(entry, highlight)
    }

    override fun getOffsetForDrawingAtPoint(xpos: Float, ypos: Float): MPPointF {
        val x = if (viewWidth != null) {
            viewWidth!! - 120 - xpos
        } else 0F
        return MPPointF(x, -height.toFloat() / 2)
    }

    companion object {
        var viewWidth: Float? = null
    }
}