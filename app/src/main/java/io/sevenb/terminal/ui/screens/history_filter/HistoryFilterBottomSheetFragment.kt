package io.sevenb.terminal.ui.screens.history_filter

import android.graphics.Point
import android.os.Bundle
import android.view.Display
import android.view.View
import android.widget.TextView
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.core.view.updateLayoutParams
import io.sevenb.terminal.R
import io.sevenb.terminal.data.model.enum_model.HistoryType
import io.sevenb.terminal.ui.base.BaseBottomSheetFragment
import io.sevenb.terminal.ui.extension.*
import io.sevenb.terminal.ui.screens.trading.SharedViewModel
import kotlinx.android.synthetic.main.fragment_history_filter.*

/**
 * Created by samosudovd on 24/01/2020.
 */
class HistoryFilterBottomSheetFragment : BaseBottomSheetFragment() {

    private lateinit var historyFilterViewModel: HistoryFilterViewModel
    private lateinit var sharedViewModel: SharedViewModel

    override fun layoutId() = R.layout.fragment_history_filter
    override fun containerId() = R.id.container_history_filter_sized

    private var historyTypes: MutableList<HistoryType> =
        mutableListOf(HistoryType.DEPOSIT, HistoryType.WITHDRAW, HistoryType.ORDERS)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        historyFilterViewModel = viewModelProvider(viewModelFactory)
        sharedViewModel = activity?.run {
            viewModelProvider(viewModelFactory)
        } ?: throw Exception("Invalid Activity")
        initView()
        subscribeUi()
    }

    private fun initView() {
        initSelectors()
        processFilter(historyTypes)

        close_screen.setOnClickListener { dismiss() }

        val display: Display? = activity?.windowManager?.defaultDisplay
        val size = Point()
        display?.getSize(size)
        val screenHeight: Int = size.y

        bg_swipe_history_filter_apply.updateLayoutParams { height = screenHeight }

        container_history_filter.setTransitionListener(object :
            MotionLayout.TransitionListener {
            override fun onTransitionStarted(p0: MotionLayout?, p1: Int, p2: Int) {}

            override fun onTransitionChange(p0: MotionLayout?, p1: Int, p2: Int, p3: Float) {}

            override fun onTransitionCompleted(p0: MotionLayout?, p1: Int) {
                if (p0?.endState == p0?.currentState) {
                    sharedViewModel.historyFilter.value = historyTypes
                    dismiss()
                }
            }

            override fun onTransitionTrigger(p0: MotionLayout?, p1: Int, p2: Boolean, p3: Float) {}
        })
    }

    private fun subscribeUi() {
        sharedViewModel.historyFilter.observe(viewLifecycleOwner, { processFilter(it) })
    }

    private fun processFilter(types: MutableList<HistoryType>?) {
        types?.let {
            markSelectedFilters(it)
        }
    }

    private fun markSelectedFilters(selectedFilters: MutableList<HistoryType>) {
        historyTypes = selectedFilters.toMutableList()
        resetSelectors()

        selectedFilters.forEach {
            selectItem(getView(it))
        }
    }

    private fun initSelectors() {
        selector_deposit.setOnClickListener {
            processFilterClick(HistoryType.DEPOSIT)
        }
        selector_withdrawal.setOnClickListener {
            processFilterClick(HistoryType.WITHDRAW)
        }
        selector_trade.setOnClickListener {
            processFilterClick(HistoryType.ORDERS)
        }
        selector_cancelled_orders.setOnClickListener {
            processFilterClick(HistoryType.CANCELLED_ORDER)
        }
    }

    private fun processFilterClick(historyType: HistoryType) {
        if (historyTypes.contains(historyType)) {
            historyTypes.remove(historyType)
            resetSelection(historyType)
        } else {
            historyTypes.add(historyType)
            selectItem(getView(historyType))
        }
    }

    private fun resetSelectors() {
        batchSameBackgrounds(
            R.drawable.bg_selector_grey_default,
            selector_deposit,
            selector_withdrawal,
            selector_trade
        )

        selector_deposit.newTextColor(R.color.colorTextMain)
        selector_withdrawal.newTextColor(R.color.colorTextMain)
        selector_trade.newTextColor(R.color.colorTextMain)
        selector_cancelled_orders.newTextColor(R.color.colorTextMain)
    }

    private fun resetSelection(historyType: HistoryType) {
        when (historyType) {
            HistoryType.ORDERS ->
                resetOrdersSelection()
            HistoryType.DEPOSIT ->
                resetDepositSelection()
            HistoryType.WITHDRAW ->
                resetWithdrawSelection()
            HistoryType.CANCELLED_ORDER ->
                resetCancelledOrdersSelection()
        }
    }

    private fun resetDepositSelection() {
        resetView(selector_deposit)
    }

    private fun resetWithdrawSelection() {
        resetView(selector_withdrawal)
    }

    private fun resetOrdersSelection() {
        resetView(selector_trade)
    }

    private fun resetCancelledOrdersSelection() {
        resetView(selector_cancelled_orders)
    }

    private fun resetView(viewId: TextView?) {
        viewId?.let {
            it.newBackground(R.drawable.bg_selector_grey_default)
            it.newTextColor(R.color.colorTextMain)
        }
    }

    private fun selectItem(view: View) {
        view.newBackground(R.drawable.bg_selector_blue_selected)
        (view as TextView).newTextColor(android.R.color.white)
    }

    private fun getView(historyType: HistoryType) =
        when (historyType) {
            HistoryType.ORDERS -> selector_trade
            HistoryType.WITHDRAW -> selector_withdrawal
            HistoryType.DEPOSIT -> selector_deposit
            HistoryType.CANCELLED_ORDER -> selector_cancelled_orders
        }

}