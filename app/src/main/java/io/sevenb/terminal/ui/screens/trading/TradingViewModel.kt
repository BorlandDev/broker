package io.sevenb.terminal.ui.screens.trading

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.binance.api.client.BinanceApiWebSocketClient
import com.github.mikephil.charting.data.Entry
import io.sevenb.terminal.data.model.broker_api.PriceAlertGet
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.trading.CandleChartParams
import io.sevenb.terminal.data.model.trading.TradeChartParams
import io.sevenb.terminal.data.model.trading.TradeChartRange
import io.sevenb.terminal.data.repository.auth.PreferenceRepository
import io.sevenb.terminal.data.repository.auth.UserAuthRepository
import io.sevenb.terminal.domain.usecase.auth.LocalUserDataStoring
import io.sevenb.terminal.domain.usecase.charts.AccountData
import io.sevenb.terminal.domain.usecase.charts.AccountData.Companion.getBaseCurrencyTicker
import io.sevenb.terminal.domain.usecase.charts.CandlesData
import io.sevenb.terminal.domain.usecase.charts.ChartData
import io.sevenb.terminal.domain.usecase.charts.ChartDataHandler
import io.sevenb.terminal.domain.usecase.notification_handler.NotificationHandler
import io.sevenb.terminal.domain.usecase.order.MarketRate
import io.sevenb.terminal.domain.usecase.trading.FavoriteCurrencies
import io.sevenb.terminal.ui.screens.alert.AlertAdapter
import io.sevenb.terminal.utils.ConnectionStateMonitor
import kotlinx.coroutines.channels.ticker
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class TradingViewModel @Inject constructor(
    private val accountData: AccountData,
    private val chartData: ChartData,
    private val localUserDataStoring: LocalUserDataStoring,
    private val favoriteCurrencies: FavoriteCurrencies,
    private val notificationHandler: NotificationHandler,
    private val marketRate: MarketRate,
    private val candlesData: CandlesData,
    private val chartDataHandler: ChartDataHandler
) : ViewModel() {

    val totalHolding = MutableLiveData<String>()
    val currencyList = MutableLiveData<List<TradingWalletItem>>()
    val favoriteList = MutableLiveData<List<FavoriteTradeItem>>()
    val chartDataResult = MutableLiveData<Pair<TradeChartParams, List<Entry>>>()
    val priceResult = MutableLiveData<Pair<Int, String>>()
    val baseCurrency = MutableLiveData<String>()

    val snackbarMessage = MutableLiveData<String>()
    val updateTradingWalletItem = MutableLiveData<TradingWalletItem>()
    val rate = MutableLiveData<Float>()
    val totalInBaseCurrency = MutableLiveData<String>()
    val notifyRecyclerView = MutableLiveData<Unit>()
    val listAlerts = MutableLiveData<List<PriceAlertGet>>(mutableListOf())

    val updateCandleChartData = MutableLiveData<Int>()

    private var localCurrencyList = listOf<TradingWalletItem>()

    private var isFirstLaunch = true

    var testAdapter: TradingListAdapter? = null
    var priceAlertAdapter: AlertAdapter? = null

    private var showTimeError = 0L

    var shouldUpdateCurrencies = false

    init {
        subscribeUi()

        tradingList()
        totalHolding()
        initBaseCurrency()

        launchTimer()
    }

    fun updateCandleChartData(tradeChartParams: TradeChartParams) {
        viewModelScope.launch {
            val symbol = "${tradeChartParams.ticker}${getBaseCurrencyTicker()}"
            val candleChartParams =
                CandleChartParams(
                    tradeChartParams.listPosition,
                    tradeChartParams.tradeChartRange,
                    symbol = symbol
                )

            val listCandles = candlesData.requestCandleData(candleChartParams)

            accountData.updateCandleChartData(tradeChartParams.listPosition, listCandles)
            updateCandleChartData.value = tradeChartParams.listPosition
        }
    }

    private fun launchTimer() {
        accountData.cancelTimerJob()
        accountData.tradingTimerJob = viewModelScope.launch {
            val ticker = ticker(3_000, 0)

            for (event in ticker) {
                if (UserAuthRepository.accessToken.isNotEmpty()) {
                    if (!isFirstLaunch && shouldUpdateCurrencies) {
                        val result =
                            accountData.getPrices(accountData.constructRateParams(accountData.sortedCurrencies))

                        when (result) {
                            is Result.Success -> {
                                accountData.updateCurrencyList(result.data)
                            }
                            is Result.Error -> {
                                when {
                                    !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                                    else -> snackbarMessage.value = result.exception.message
                                }
                            }
                        }
                    }
                    isFirstLaunch = false
                }
            }
        }
    }

    private fun subscribeUi() {
        accountData.cancelTradingTotalBalanceJob()
        accountData.tradingTotalBalanceFlowJob = viewModelScope.launch {
            accountData.totalBalanceFlow.collect {
                totalHolding.value = it.totalHoldingBalance
                totalInBaseCurrency.value = it.totalInBaseCurrency
                marketRate()
                localUserDataStoring.updateLocalAccountData(
                    totalHoldingBalance = it.totalHoldingBalance,
                    availableBalance = it.availableBalance,
                    onOrdersBalance = it.onOrdersBalance
                )
            }
        }

        accountData.cancelCurrenciesListJob()
        accountData.currenciesListFlowJob = viewModelScope.launch {
            accountData.currenciesListFlow.collect {
                currencyList.value = it
                notifyRecyclerView.value = Unit
            }
        }
    }

    fun updateTotalInBaseCurrency(amount: String) {
        viewModelScope.launch {
            accountData.totalBalanceFlow.collect {
                it.totalInBaseCurrency = amount
                totalInBaseCurrency.value = amount
            }
        }
    }

    private fun tradingList() {
        viewModelScope.launch {
            when (val currenciesResult = accountData.mapTradingList()) {
                is Result.Success -> {
                    val currenciesList = currenciesResult.data
                    currencyList.value = currenciesList
                    localCurrencyList = currenciesList
                    buildFavoriteList(currenciesList)
                    notifyRecyclerView.value = Unit
                }
                is Result.Error -> {
                    when {
                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                        else -> snackbarMessage.value =
                            if (currenciesResult.message.isEmpty()) currenciesResult.exception.message
                            else currenciesResult.message
                    }
                }
            }
        }
    }

    fun marketRate() {
        viewModelScope.launch {
            when (val rateResult =
                marketRate.execute(AccountData.BTC, getBaseCurrencyTicker())) {
                is Result.Success -> {
                    val marketRateResult = rateResult.data
                    rate.value = marketRateResult.rate ?: 0F
                }
                is Result.Error -> {
                    when {
                        !ConnectionStateMonitor.connectionEnabled -> {
                        }
                        else -> snackbarMessage.value = rateResult.exception.message
                    }
                }
            }
        }
    }

    private fun showNoInternetConnection() {
        val currentTime = System.currentTimeMillis()
        if (currentTime - showTimeError >= 1500) {
            snackbarMessage.value = ConnectionStateMonitor.noInternetError
            showTimeError = currentTime
        }
    }

    private suspend fun buildFavoriteList(currenciesList: List<TradingWalletItem>) {
        when (val favoritesResult = favoriteCurrencies.favoriteTickers(currenciesList)) {
            is Result.Success -> {
                val favorites = favoritesResult.data
                favoriteList.value = favorites
            }
            is Result.Error -> Timber.e("favoriteCurrencies.favoriteTickers() error")
        }
    }

    private fun totalHolding() {
        viewModelScope.launch {
            accountData.totalHolding()
        }
    }

    private fun initBaseCurrency() {
        baseCurrency.value = PreferenceRepository.baseCurrencyTicker
    }

    fun updateItemChartData(ticker: String, tradeChartParams: TradeChartParams) {
        viewModelScope.launch {
            if (ConnectionStateMonitor.connectionEnabled)
                when (val currenciesResult =
                    chartData.requestChartData(
                        ticker,
                        PreferenceRepository.baseCurrencyTicker,
                        tradeChartParams
                    )) {
                    is Result.Success -> {
                        val entriesList = currenciesResult.data
                        if (tradeChartParams.tradeChartRange == TradeChartRange.DAY) {
                            accountData.updateChartParamsByTicker(
                                tradeChartParams.ticker, entriesList
                            )
                            accountData.updateExpandedChartParamsByTicker(
                                tradeChartParams.ticker, entriesList
                            )
                        } else {
                            accountData.updateExpandedChartParamsByTicker(
                                tradeChartParams.ticker, entriesList
                            )
                        }

                        chartDataResult.value = Pair(tradeChartParams, currenciesResult.data)
                    }
                    is Result.Error -> {
                        when {
                            !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                            else -> snackbarMessage.value = currenciesResult.exception.message
                        }
                    }
                }
        }
    }

    fun getPositionByTicker(ticker: String, list: List<TradingWalletItem>) =
        accountData.getPositionByTicker(ticker, list)

    fun priceAlertInfo() {
        viewModelScope.launch {
            when (val priceAlertInfoResult = notificationHandler.getPriceAlert()) {
                is Result.Success -> {
                    listAlerts.value = priceAlertInfoResult.data.responseList
//                    lvPriceAlertInfo.value = priceAlertInfoResult.data
                }
                is Result.Error -> {
                    when {
                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                        else -> snackbarMessage.value = priceAlertInfoResult.exception.message
                    }
                }
            }
        }

    }

    fun priceAlertDelete(id: String) {
        viewModelScope.launch {
            when (val priceAlertDeleteResult = notificationHandler.deletePriceAlert(id)) {
                is Result.Success -> {
                    priceAlertInfo()
//                    lvPriceAlertInfo.value = priceAlertInfoResult.data
                }
                is Result.Error -> {
                    when {
                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                        else -> snackbarMessage.value = priceAlertDeleteResult.exception.message
                    }
                }
            }
        }

    }

    fun processFavorite(tradingWalletItem: TradingWalletItem) {
        viewModelScope.launch {
            when (val result = favoriteCurrencies.processFavorite(
                tradingWalletItem.ticker,
                tradingWalletItem.isFavorite
            )) {
                is Result.Success -> {
                    tradingWalletItem.isFavorite = !tradingWalletItem.isFavorite
                    buildFavoriteList(localCurrencyList)
                    updateTradingWalletItem.value = tradingWalletItem
                }
                is Result.Error -> {
                    when {
                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                        else -> snackbarMessage.value = result.exception.message
                    }
                }
            }
        }
    }

}
