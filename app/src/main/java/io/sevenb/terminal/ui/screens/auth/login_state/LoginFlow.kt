package io.sevenb.terminal.ui.screens.auth.login_state

import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.widget.ImageView
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import io.sevenb.terminal.R
import io.sevenb.terminal.data.model.auth.ContinuesRegistrationObj
import io.sevenb.terminal.data.model.auth.UserData
import io.sevenb.terminal.data.model.enum_model.ErrorMessageType
import io.sevenb.terminal.data.model.enums.FocusableFields
import io.sevenb.terminal.data.model.enums.auth.NewAuthStates
import io.sevenb.terminal.domain.usecase.new_auth.HandleFingerprint
import io.sevenb.terminal.ui.extension.*
import io.sevenb.terminal.ui.screens.auth.SharedAuthViewModel
import io.sevenb.terminal.ui.screens.auth.new_auth.NewAuthSharedViewModel
import io.sevenb.terminal.ui.screens.settings.SettingsFragment
import io.sevenb.terminal.utils.Logger
import kotlinx.android.synthetic.main.fragment_auth_login.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.crypto.Cipher

class LoginFlow : LifecycleObserver {
    private lateinit var newAuthSharedViewModel: NewAuthSharedViewModel
    private lateinit var loginViewModel: LoginViewModel
    private lateinit var authSharedViewModel: SharedAuthViewModel

    private lateinit var view: View
    private lateinit var lifecycleOwner: LifecycleOwner
    private lateinit var fragment: Fragment

    private var passVisibility = false
//    private var isSubscribed = false

    private lateinit var handleFingerprint: HandleFingerprint

    fun initLifecycleOwner(
        lifecycleOwner: LifecycleOwner,
        fragment: Fragment,
        handleFingerprint: HandleFingerprint
    ) {
        this.lifecycleOwner = lifecycleOwner
        this.fragment = fragment
        this.handleFingerprint = handleFingerprint
        handleFingerprint.init(fragment)
    }

    fun initViews(
        view: View,
        newAuthSharedViewModel: NewAuthSharedViewModel,
        loginViewModel: LoginViewModel,
        authSharedViewModel: SharedAuthViewModel
    ) {
        this.newAuthSharedViewModel = newAuthSharedViewModel
        this.loginViewModel = loginViewModel
        this.authSharedViewModel = authSharedViewModel
        this.view = view

//        if (!isSubscribed)
        subscribeUI()

        view.apply {

            input_email_sign_in.doOnTextChanged { _, _, _, _ -> showEmailError(false) }
            input_password_sign_in.doOnTextChanged { _, _, _, _ -> showPasswordError(false) }
            input_password_sign_in.onActionDoneListener { signInUser() }

            ll_action_not_account.setOnClickListener {
                processLoginState(NewAuthStates.REGISTRATION)
            }

            img_pass_visibility_sign_in.setOnClickListener {
                passVisibility = !passVisibility
                input_password_sign_in.transformationMethod =
                    if (passVisibility) HideReturnsTransformationMethod.getInstance() else
                        PasswordTransformationMethod.getInstance()
                input_password_sign_in.setSelection(input_password_sign_in.text.length)
                changeEyeImage(img_pass_visibility_sign_in)
            }

            ll_action_forgot_password.setOnClickListener {
                processLoginState(NewAuthStates.PASSWORD_RESTORING)
            }

            button_next_sign_in.setOnClickListener {
                signInUser()
            }
        }

        processSavedEmail(newAuthSharedViewModel.userData.email)
    }

    private fun subscribeUI() {
//        isSubscribed = true
        loginViewModel.apply {
            loginState.observe(lifecycleOwner, { processLoginState(it) })
            decryptBiometricCipher.observe(lifecycleOwner, { processFingerprintExistence(it) })
            errorMessage.observe(lifecycleOwner, { processErrorMessage(it) })
            errorString.observe(lifecycleOwner, { processErrorMessage(it) })
            continuesRegistrationObj.observe(lifecycleOwner, { processContinueRegistration(it) })
            encryptBiometricCipher.observe(lifecycleOwner, { })
            popupLocalAccountExists.observe(lifecycleOwner, { popupRemoveLocalAccount() })
            allLocalAccountDataRemoved.observe(lifecycleOwner, { processLocalAccountDataRemoved() })
        }

        newAuthSharedViewModel.savedEmail.observe(lifecycleOwner, { processSavedEmail(it) })
    }

    private fun processContinueRegistration(obj: ContinuesRegistrationObj?) {
        authSharedViewModel.setSubStateToActivity.value = NewAuthStates.CONTINUES_REGISTRATION
        obj?.let {
            newAuthSharedViewModel.continueRegistration(obj)
            loginViewModel.continuesRegistrationObj.value = null
        }
    }

    private fun popupRemoveLocalAccount() {
        MaterialAlertDialogBuilder(fragment.requireContext()).apply {
            setMessage(fragment.getString(R.string.message_clear_local_account_data))
            setNegativeButton(android.R.string.cancel) { dialog, _ ->
                dialog.dismiss()
                newAuthSharedViewModel.showProgress(false)
            }
            setPositiveButton(android.R.string.ok) { dialog, _ ->
                loginViewModel.clearAllLocalAccountData(
                    fragment.requireContext().cacheDir.path +
                            SettingsFragment.AVATAR_FILE_PATH
                )
                dialog.dismiss()
            }
        }.create().show()
    }

    private fun processFingerprintExistence(cipher: Cipher?) {
        cipher?.let {
            handleFingerprint.decryptFingerprintDialog(
                it,
                {
                    newAuthSharedViewModel.focusField(FocusableFields.LOGIN_EMAIL)
                },
                ::decryptPasswordFromStorage,
                ::biometricErrorSignInWithFingerprint
            )
        }
    }

    private fun decryptPasswordFromStorage(authResult: BiometricPrompt.AuthenticationResult) {
        if (handleFingerprint.isBiometricSuccess()) {
            authResult.cryptoObject?.cipher.let {
                newAuthSharedViewModel.showProgress(true)
                loginViewModel.decryptPassword(it)
            }
        } else validateShowBiometricError()
    }

    private fun biometricErrorSignInWithFingerprint(errorCode: Int) {
        CoroutineScope(Main).launch {
            if (errorCode == 7 || errorCode == 10 || errorCode == 13) {
                delay(500)
                newAuthSharedViewModel.focusField(FocusableFields.LOGIN_EMAIL)
            }
        }
    }

    private fun validateShowBiometricError() {
        when (handleFingerprint.getBiometricAuthenticateAbility()) {
            BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE ->
                processErrorMessage("No biometric features available on this device.")
            BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE ->
                processErrorMessage("Biometric features are currently unavailable.")
        }
    }

    private fun processErrorMessage(message: String) {
        newAuthSharedViewModel.apply {
            showError(message)
            showProgress(false)
        }
    }

    private fun processErrorMessage(errorMessageType: ErrorMessageType) {
        newAuthSharedViewModel.apply {
            showErrorMessage(errorMessageType)
        }
    }

    private fun processSavedEmail(email: String?) {
        if (!email.isNullOrEmpty() && loginViewModel.localState == NewAuthStates.LOGIN) {
            loginViewModel.savedEmail = email
            view.input_email_sign_in.setText(email)
            loginViewModel.checkFingerprintExistence()
        }
//        else if (email.isNullOrEmpty() && loginViewModel.localState == NewAuthStates.LOGIN) {
//            newAuthSharedViewModel.focusField(FocusableFields.LOGIN_EMAIL)
//        }
        newAuthSharedViewModel.focusField(FocusableFields.LOGIN_EMAIL)
    }

    private fun processLoginState(loginState: NewAuthStates?) {
        if (loginState == NewAuthStates.TFA_CREATE || loginState == NewAuthStates.TFA_VERIFICATION) {
            loginViewModel.loginState.value = null
//            isSubscribed = true
        }
        loginState?.let { newAuthSharedViewModel.setNewState(it) }
    }

    private fun signInUser() {
        newAuthSharedViewModel.showProgress(true)
        val email = view.input_email_sign_in.getString()
        val password = view.input_password_sign_in.getString()

        val messageType = loginViewModel.validateFields(email, password)
        if (messageType == ErrorMessageType.CHECK_PASSED) {
            Logger.init(email)
            Logger.sendLog("LoginFlow", "signInUser")
            newAuthSharedViewModel.saveUserData(UserData(email = email, password))
            loginViewModel.signInUser(email, password)
        } else {
            processLoginError(messageType)
            processErrorMessage(messageType)
        }
    }

    private fun processLoginError(messageType: ErrorMessageType) {
        newAuthSharedViewModel.showProgress(false)
        when (messageType) {
            ErrorMessageType.EMPTY_FIELDS -> {
                showEmailError(true)
                showPasswordError(true)
            }
            ErrorMessageType.EMPTY_EMAIL, ErrorMessageType.EMAIL_INCORRECT ->
                showEmailError(true)
            ErrorMessageType.EMPTY_PASSWORD, ErrorMessageType.PASSWORD_INCORRECT ->
                showPasswordError(true)
            else -> {
            }
        }
    }

    private fun showPasswordError(value: Boolean) {
        view.container_password_sign_in.apply {
            if (value) setErrorBackground()
            else setDefaultBackground()
        }
    }

    private fun showEmailError(value: Boolean) {
        view.input_email_sign_in.apply {
            if (value) setErrorBackground()
            else setDefaultBackground()
        }
    }

    private fun processLocalAccountDataRemoved() = signInUser()

    private fun changeEyeImage(imageView: ImageView) {
        imageView.newBackground(if (passVisibility) R.drawable.ic_password else R.drawable.ic_password_hidden)
    }
}