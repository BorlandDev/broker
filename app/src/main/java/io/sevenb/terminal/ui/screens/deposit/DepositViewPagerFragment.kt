package io.sevenb.terminal.ui.screens.deposit

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.webkit.*
import android.widget.FrameLayout
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import io.sevenb.terminal.AppActivity
import io.sevenb.terminal.R
import io.sevenb.terminal.ui.base.BaseBottomSheetFragment
import io.sevenb.terminal.ui.base.adapter.TabFragmentAdapter
import kotlinx.android.synthetic.main.activity_app.*
import kotlinx.android.synthetic.main.fragment_deposit_view_pager.*
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetBehavior


class DepositViewPagerFragment : BaseBottomSheetFragment(),
    ActivityCompat.OnRequestPermissionsResultCallback {
    override fun layoutId() = R.layout.fragment_deposit_view_pager
    override fun containerId() = R.id.container_new_order
    private var mUploadMessage: ValueCallback<Array<Uri?>?>? = null

    var openWeb = false
    var urlLocal = ""

    override fun onActivityResult(
        requestCode: Int, resultCode: Int,
        intent: Intent?
    ) {
        if (requestCode == 1) {
            if (null == mUploadMessage) return
            mUploadMessage!!.onReceiveValue(
                WebChromeClient.FileChooserParams.parseResult(
                    resultCode,
                    intent
                )
            )
            mUploadMessage = null
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            3 -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED
                ) {
                    if ((ContextCompat.checkSelfPermission(
                            requireActivity(),
                            Manifest.permission.CAMERA
                        ) ===
                                PackageManager.PERMISSION_GRANTED) &&
                        (ContextCompat.checkSelfPermission(
                            requireActivity(),
                            Manifest.permission.READ_EXTERNAL_STORAGE
                        ) ===
                                PackageManager.PERMISSION_GRANTED) && urlLocal != ""
                    ) {
                        webView.loadUrl(urlLocal)
                    }
                }
                return
            }
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = TabFragmentAdapter(childFragmentManager)

        adapter.addFragment(
            DepositBottomSheetFragment.newInstance(),
            getString(R.string.crypto_name)
        )
        adapter.addFragment(FiatFragment.newInstance(), getString(R.string.fiat_name))

        vp_main_deposit.adapter = adapter
        webView.visibility = View.GONE
        openWeb = false
        close_screen.setOnClickListener {
            dismiss()
        }

        tl_test.setupWithViewPager(vp_main_deposit)

        dialog!!.setOnKeyListener { _, keyCode, _ ->
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                if (openWeb) {
                    webView.visibility = View.GONE
                    openWeb = false
                }
                true
            } else false
        }
    }

    @SuppressLint("SetJavaScriptEnabled", "UseRequireInsteadOfGet")
    fun openWebView(url: String) {
        urlLocal = url
        openWeb = true
        webView.visibility = View.VISIBLE
        close_screen.setOnClickListener {
            val alertBuilder: AlertDialog.Builder = AlertDialog.Builder(requireContext())
            alertBuilder.setTitle("Close this form?")
            alertBuilder.setPositiveButton(
                "Yes"
            ) { _, _ -> (this as BottomSheetDialogFragment).dismiss() }
            alertBuilder.setNegativeButton("No") { dialog, _ -> dialog.dismiss() }

            val dialog: AlertDialog = alertBuilder.create()
            dialog.show()
        }
        webView.settings.javaScriptEnabled = true
        webView.settings.domStorageEnabled = true
        webView.settings.pluginState = WebSettings.PluginState.ON
        webView.settings.mediaPlaybackRequiresUserGesture = true
        webView.settings.allowContentAccess = true
        webView.settings.domStorageEnabled = true
        webView.settings.useWideViewPort = true
        webView.settings.javaScriptCanOpenWindowsAutomatically = true
        webView.settings.allowFileAccess = true
        webView.settings.allowFileAccessFromFileURLs = true
        webView.settings.allowUniversalAccessFromFileURLs = true
        webView.webChromeClient = MyWebChromeClient()
        webView.webViewClient = WebViewClient()

        if (ContextCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.CAMERA
            ) !==
            PackageManager.PERMISSION_GRANTED ||
            ContextCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) !==
            PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE), 3
            )
        } else {
            webView.loadUrl(url)
        }
    }

    internal inner class MyWebChromeClient : WebChromeClient() {
        override fun onShowFileChooser(
            mWebView: WebView?,
            filePathCallback: ValueCallback<Array<Uri?>?>,
            fileChooserParams: FileChooserParams
        ): Boolean {

            if (mUploadMessage != null) {
                mUploadMessage!!.onReceiveValue(null);
                mUploadMessage = null
            }

            mUploadMessage = filePathCallback
            var intent: Intent? = null
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                intent = fileChooserParams.createIntent()
            }
            try {
                startActivityForResult(intent, 1)
            } catch (e: ActivityNotFoundException) {
                Toast.makeText(
                    context,
                    "Cannot Open File Chooser",
                    Toast.LENGTH_LONG
                ).show()
                return false
            }
            return true
        }

        override fun onPermissionRequest(request: PermissionRequest) {
            request.grant(request.resources)
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog

        dialog.setOnShowListener {
            val bottomSheet =
                (it as BottomSheetDialog).findViewById<View>(R.id.design_bottom_sheet) as FrameLayout?
            val behavior = BottomSheetBehavior.from(bottomSheet!!)
            behavior.state = BottomSheetBehavior.STATE_EXPANDED

            behavior.addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
                override fun onStateChanged(bottomSheet: View, newState: Int) {
                    if (newState == BottomSheetBehavior.STATE_DRAGGING && openWeb) {
                        behavior.state = BottomSheetBehavior.STATE_EXPANDED
                    }
                }

                override fun onSlide(bottomSheet: View, slideOffset: Float) {}
            })
        }

        return dialog
    }

    override fun onDestroy() {
        super.onDestroy()
        (activity as AppActivity).appToolbar.visibility = View.VISIBLE

        (activity as AppActivity).initToolbar(
            R.string.title_portfolio_tab,
            false,
            false
        )

        (activity as AppActivity).bottomNavView.visibility = View.VISIBLE
    }
}