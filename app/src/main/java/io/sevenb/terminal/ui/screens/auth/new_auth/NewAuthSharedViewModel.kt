package io.sevenb.terminal.ui.screens.auth.new_auth

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import io.sevenb.terminal.data.model.auth.ContinuesRegistrationObj
import io.sevenb.terminal.data.model.auth.UserCaptcha
import io.sevenb.terminal.data.model.auth.UserData
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.enum_model.ErrorMessageType
import io.sevenb.terminal.data.model.enums.FocusableFields
import io.sevenb.terminal.data.model.enums.GeeTestResult
import io.sevenb.terminal.data.model.enums.auth.NewAuthStates
import io.sevenb.terminal.data.model.portfolio.LocalAccountData
import io.sevenb.terminal.domain.usecase.auth.CaptchaDataInterop
import io.sevenb.terminal.domain.usecase.auth.LocalUserDataStoring
import io.sevenb.terminal.domain.usecase.auth.RestoreRefreshToken
import io.sevenb.terminal.domain.usecase.auth.StoreRefreshToken
import io.sevenb.terminal.domain.usecase.charts.AccountData
import io.sevenb.terminal.domain.usecase.safety.AESCrypt
import io.sevenb.terminal.domain.usecase.safety.AccountSafetyStoring
import io.sevenb.terminal.exceptions.Try
import io.sevenb.terminal.utils.Logger
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.json.JSONObject
import javax.inject.Inject

class NewAuthSharedViewModel @Inject constructor(
    private val localUserDataStoring: LocalUserDataStoring,
    private val captchaDataInterop: CaptchaDataInterop,
    private val restoreRefreshToken: RestoreRefreshToken,
    private val accountData: AccountData,
    private val aesCrypt: AESCrypt,
    private val storeRefreshToken: StoreRefreshToken,
    private val safetyStoring: AccountSafetyStoring,

    ) : ViewModel() {

    val newState = MutableLiveData<NewAuthStates>()
    val savedEmail = MutableLiveData<String>()
    val showProgress = MutableLiveData<Boolean>()
    val errorString = MutableLiveData<String>()
    val errorMessage = MutableLiveData<ErrorMessageType>()
    val focusField = MutableLiveData<FocusableFields?>()
    val showCaptcha = MutableLiveData<JSONObject?>()
    val startCaptchaFlow = MutableLiveData<Unit>()
    val geeTestResult = MutableLiveData<GeeTestResult>()
    val userCaptcha = MutableLiveData<UserCaptcha?>()
    val focusSmsCodeField = MutableLiveData<Unit>()
    val continueRegistration = MutableLiveData<ContinuesRegistrationObj>()
    val verificationCodeVisibility = MutableLiveData<Boolean>()
    val tfaVisibility = MutableLiveData<Boolean>()
    val showLoader = MutableLiveData<Pair<Boolean, String>>()
    val emailFromStorage = MutableLiveData<String>()
    val creatingPassCodeRequired = MutableLiveData<Pair<String, String>>()
    val creatingTfaRequired = MutableLiveData<Pair<String, String>>()
    val closeCaptcha = MutableLiveData<Boolean?>()

    var state: NewAuthStates = NewAuthStates.REGISTRATION

    //    var stateWithoutSMS = NewAuthStates.REGISTRATION
    var userData = UserData()

    init {
        restoreLocalAccountData()
    }

    fun setNewState(newAuthState: NewAuthStates) {
        state = newAuthState
//        if (newState != NewAuthStates.VERIFICATION_CODE) stateWithoutSMS = newState
        this.newState.value = newAuthState
    }

    fun onCaptchaResult(result: String) {
        when (val jsonObject: Result<JSONObject> = Try { JSONObject(result) }) {
            is Result.Success -> {
                val challenge = jsonObject.data.getString(CaptchaDataInterop.GEETEST_CHALLENGE)
                val seccode = jsonObject.data.getString(CaptchaDataInterop.GEETEST_SECCODE)
                val validate = jsonObject.data.getString(CaptchaDataInterop.GEETEST_VALIDATE)
                userCaptcha.value = UserCaptcha(challenge, seccode, validate)
            }
            is Result.Error -> {
                showErrorMessage(ErrorMessageType.CAPTCHA_POST_PARSE_JSON)// e.message)
            }
        }
    }

    fun onCaptchaSuccess() {
        when (state) {
            NewAuthStates.REGISTRATION -> setNewState(NewAuthStates.CONTINUES_REGISTRATION)
            else -> setNewState(NewAuthStates.CONTINUES_PASSWORD_RESTORING)
        }
    }

    /**
     * Back to current state
     */
    fun onCaptchaFailed() {
        setNewState(state)
//        when (state) {
//            NewAuthStates.REGISTRATION -> setNewState(NewAuthStates.CONTINUES_REGISTRATION)
//            else -> setNewState(NewAuthStates.CONTINUES_PASSWORD_RESTORING)
//        }
    }

    fun showCaptcha() {
        viewModelScope.launch {
            when (val resultJsonObject = captchaDataInterop.getCaptchaData()) {
                is Result.Success -> {
                    when (val jsonObject: Result<JSONObject> =
                        Try { JSONObject(Gson().toJson(resultJsonObject.data)) }
                    ) {
                        is Result.Success -> {
                            showCaptcha.value = jsonObject.data
                        }
                        is Result.Error -> {
                            showCaptcha.value = JSONObject("{}")
                            showErrorMessage(ErrorMessageType.CAPTCHA_GET_PARSE_JSON)
                        }
                    }
                }
                is Result.Error -> {
                    showErrorMessage(ErrorMessageType.CAPTCHA_LOST_CONNECTION)
                }
            }
        }
    }

    fun closeCaptcha() {
        viewModelScope.launch {
            withContext(Main) {
                closeCaptcha.value = true
            }
        }
    }

    fun startCaptchaFlow(userData: UserData) {
        this.userData = userData
        startCaptchaFlow.value = Unit
    }

    fun saveUserData(userData: UserData) {
        this.userData = userData
    }

    fun showErrorMessage(messageType: ErrorMessageType) {
        errorMessage.value = messageType
    }

    fun showError(message: String) {
        errorString.value = message
    }

    fun showProgress(value: Boolean) {
        showProgress.value = value
    }

    private fun restoreLocalAccountData() {
        viewModelScope.launch {
            val localAccountData = async { localUserDataStoring.restoreLocalAccountData() }
            val refreshToken = async { restoreRefreshToken.execute() }

            processRestoringData(localAccountData.await(), refreshToken.await())
        }
    }

    private fun processRestoringData(
        localAccountData: Result<LocalAccountData>,
        refreshToken: Result<String>
    ) {
        if (localAccountData !is Result.Success) return
        if (refreshToken !is Result.Success) return

        val localData = localAccountData.data
        val savedRefreshToken = refreshToken.data

        if (savedRefreshToken.isEmpty()) {
            if (localData.email.isEmpty()) setNewState(NewAuthStates.REGISTRATION)
            else {
                saveEmail(localData.email)
                setNewState(NewAuthStates.LOGIN)
            }
        } else {
            if (localData.email.isEmpty()) setNewState(NewAuthStates.REGISTRATION)
            else {
                saveEmail(localData.email)
                setNewState(NewAuthStates.LOGIN)
            }
        }
    }

    fun focusField(focusableField: FocusableFields?) {
        focusField.value = focusableField
    }

    private fun saveEmail(email: String) {
        emailFromStorage.value = email
        userData.email = email
        savedEmail.value = email
    }

    fun setGeeTestResult(geeTestResult: GeeTestResult) {
        this.geeTestResult.value = geeTestResult
    }

    fun continueRegistration(continuesRegistrationObj: ContinuesRegistrationObj) {
        continueRegistration.value = continuesRegistrationObj
    }

    fun showVerificationCode(value: Boolean) {
        verificationCodeVisibility.value = value
    }

    fun showTfa(value: Boolean) {
        tfaVisibility.value = value
    }

    fun showLoader(value: Boolean, title: String = "") {
        showLoader.value = Pair(value, title)
    }

    fun requirePassCodeCreation(value: Boolean, email: String = "", password: String = "") {
        if (email.isNotEmpty() && password.isNotEmpty()) {
            if (value) creatingPassCodeRequired.value = Pair(email, password)
        } else {
            if (value) creatingPassCodeRequired.value = Pair(userData.email, userData.password)
        }
    }

    fun preloadAccountAssets() {
//        viewModelScope.launch {
//            accountData.mapAccountAssets()
//        }
    }

    fun dismissTfa(refreshToken: String) {
        viewModelScope.launch {
            localUserDataStoring.apply {
                updateLocalAccountData(userData.email)
                val encryptedPassword = aesCrypt.encrypt(userData.password)
                encryptedPassword?.let { storePassword(it) }
            }
            storeRefreshToken(refreshToken)
        }
    }

    private fun storeRefreshToken(refreshToken: String) {
        viewModelScope.launch {
            when (val storeResult = storeRefreshToken.execute(refreshToken)) {
                is Result.Success -> {
                    authDone()
                }
                is Result.Error -> {
                    errorString.value = storeResult.exception.message
                }
            }
        }
    }

    fun authDone() {
        Logger.sendLog(
            "NewAuthSharedViewModel",
            "authDone",
        )
        viewModelScope.launch {
            val storeLogOutAsync = async { safetyStoring.storeLoggedOut(0) }
            val restoreLocalDataAsync = async { localUserDataStoring.restoreLocalAccountData() }

            if (storeLogOutAsync.await() is Result.Success && restoreLocalDataAsync.await() is Result.Success) {
                setNewState(NewAuthStates.DONE_AUTH)
            }
        }
    }
}