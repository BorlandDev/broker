package io.sevenb.terminal.ui.screens.portfolio

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import io.sevenb.terminal.R

internal class PortfolioHistoryAdapter(
    private val copyHash: (String) -> Unit = { },
    private val onClickHistory: (PortfolioHistoryItem) -> Unit = { }
) : RecyclerView.Adapter<HistoryItemViewHolder>() {

    var baseItemsList: MutableList<BaseHistoryItem> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryItemViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_history, parent, false)
        return HistoryItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: HistoryItemViewHolder, position: Int) {
        val item = baseItemsList[position]
        holder.bind(item, copyHash, onClickHistory)
    }

    fun submitList(list: List<BaseHistoryItem>) {
        baseItemsList.clear()
        baseItemsList = list.toMutableList()
        notifyDataSetChanged()
    }

    fun clearList() {
        baseItemsList.clear()
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = baseItemsList.size

}
