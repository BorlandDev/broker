package io.sevenb.terminal.ui.screens.secutiry

import android.os.Bundle
import android.view.View
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import io.sevenb.terminal.R
import io.sevenb.terminal.data.model.broker_api.account.UserSettings
import io.sevenb.terminal.data.model.enums.auth.TfaState
import io.sevenb.terminal.data.model.tfa.TfaResponse
import io.sevenb.terminal.domain.usecase.new_auth.HandleFieldFocus
import io.sevenb.terminal.domain.usecase.new_auth.HandleFingerprint
import io.sevenb.terminal.ui.base.BaseFragment
import io.sevenb.terminal.ui.extension.viewModelProvider
import io.sevenb.terminal.ui.extension.visible
import io.sevenb.terminal.ui.screens.safety.tfa.TfaFlow
import io.sevenb.terminal.ui.screens.safety.tfa.TfaViewModel
import io.sevenb.terminal.ui.screens.settings.NotifySettingsViewModel
import io.sevenb.terminal.ui.screens.settings.SharedSettingToActivityViewModel
import kotlinx.android.synthetic.main.fragment_security.*
import kotlinx.android.synthetic.main.fragment_security_settings.*
import javax.crypto.Cipher
import javax.inject.Inject

class SecurityFragment : BaseFragment() {

    @Inject
    lateinit var handleFingerprint: HandleFingerprint

    @Inject
    lateinit var handleFieldFocus: HandleFieldFocus

    private lateinit var securityViewModel: SecurityViewModel
    private val sharedSettingToActivityViewModel: SharedSettingToActivityViewModel
            by activityViewModels { viewModelFactory }
    private val notifySettingsViewModel: NotifySettingsViewModel by activityViewModels { viewModelFactory }
    private val navArgs: SecurityFragmentArgs by navArgs()

    override fun layoutId() = R.layout.fragment_security
    override fun onSearchTapListener() {}

    @Inject
    lateinit var tfaFlow: TfaFlow
    private lateinit var tfaViewModel: TfaViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        securityViewModel = viewModelProvider(viewModelFactory)
        tfaViewModel = viewModelProvider(viewModelFactory)

        processNavArgs()
        subscribeUI()
        initViews()
    }

    private fun processNavArgs() {
        navArgs.apply {
            email?.let { securityViewModel.email = it }
            tfa.let {
                securityViewModel.tfa = it
                smTfaEnabling.isChecked = it
            }
        }
    }

    private fun initViews() {
        handleFingerprint.init(this)
        clSecurityBiometric.visible(checkBiometricHardwareExistence())

        vSecurityBiometricEnabling.setOnClickListener {
            securityViewModel.processEnablingFingerprint(!smSecurityBiometricEnabling.isChecked)
        }
        clSecurityBiometric.setOnClickListener {
            securityViewModel.processEnablingFingerprint(!smSecurityBiometricEnabling.isChecked)
        }

        vSecurityPassCodeEnabling.setOnClickListener {
            securityViewModel.processPassCodeEnabling(!smSecurityPassCodeEnabling.isChecked)
        }
        clSecurityPin.setOnClickListener {
            securityViewModel.processPassCodeEnabling(!smSecurityPassCodeEnabling.isChecked)
        }

        clBlockTimer.setOnClickListener { navigateLockTimeout() }

        clSecurityChangePassword.setOnClickListener { changePassword() }

        ivSecurityBack.setOnClickListener { findNavController().popBackStack() }

        cl_tfa_container.setOnClickListener {
            processToggleTfa()
        }
        vTfaEnabling.setOnClickListener {
            processToggleTfa()
        }
    }

    private fun processToggleTfa() {
        if (!securityViewModel.tfa) {
            showTfa()
        } else {
            showTfaOffDialog()
        }
    }

    private fun showTfaOffDialog() {
        MaterialAlertDialogBuilder(requireContext()).apply {
            setMessage(getString(R.string.alert_tfa_off))
            setPositiveButton(R.string.label_yes) { dialog, _ ->
                securityViewModel.toggleTfa()
                dialog.dismiss()
            }
            setNegativeButton(R.string.label_no) { dialog, _ ->
                dialog.dismiss()
            }
        }.create().show()
    }

    private fun navigateLockTimeout() {
        if (findNavController().currentDestination?.id != R.id.securityFragment) return

        val direction = SecurityFragmentDirections.actionSecurityFragmentToLockTimeoutFragment()
        findNavController().navigate(direction)
    }

    private fun subscribeUI() {
        securityViewModel.apply {
            fingerprintCreatingRequired.observe(
                viewLifecycleOwner, { processFingerprintCreating(it) })
            requestPinApprove.observe(viewLifecycleOwner, { processPassCodeApprove(it) })
            showPopup.observe(viewLifecycleOwner, { popupEnablePin(it) })
            navigateToRemovePassCode.observe(
                viewLifecycleOwner, { processNavigationToDeletePin(it) })
            passCodeEnabled.observe(viewLifecycleOwner, { processPassCodeEnabled(it) })
            passCodeCreatingRequired.observe(viewLifecycleOwner, { processPassCodeCreating(it) })
            createFingerprint.observe(viewLifecycleOwner, { processCreateFingerprint(it) })
            snackbarMessage.observe(viewLifecycleOwner, { showSnackBarMessage(it) })
            biometricEnabled.observe(viewLifecycleOwner, { processBiometricEnabled(it) })
            tfaEnabled.observe(viewLifecycleOwner, { processTfaEnabled(it) })
        }

        sharedSettingToActivityViewModel.apply {
            passCodeWasCreated.observe(viewLifecycleOwner, { securityViewModel.pinValue(it) })
            passCodeWasEntered.observe(viewLifecycleOwner, { processPassCodeEntered(it) })
        }

        notifySettingsViewModel.notifyFrToUpdateSefetyData.observe(
            viewLifecycleOwner, { processNotifySettings(it) })
    }

    private fun changePassword() {
        if (findNavController().currentDestination?.id != R.id.securityFragment) return

        val direction = SecurityFragmentDirections.actionSecurityFragmentToChangePasswordFragment()
        findNavController().navigate(direction)
    }

    private fun processFingerprintCreating(value: Boolean?) {
        value?.let {
            if (it)
                securityViewModel.createFingerprint()
            else sharedSettingToActivityViewModel.enterPassCodeFromSettings.value =
                Pair(false, this)

            securityViewModel.fingerprintCreatingRequired.value = null
        }
    }

    private fun processPassCodeEntered(value: Boolean?) {
        value?.let { securityViewModel.removeFingerprint() }
    }

    private fun processBiometricEnabled(value: Boolean?) {
        value?.let {
            smSecurityBiometricEnabling.isChecked = it
            securityViewModel.biometricEnabled.value = null
        }
    }

    private fun processTfaEnabled(tfaResponse: TfaResponse?) {
        tfaResponse?.let {
            securityViewModel.tfaEnabled.value = null
            smTfaEnabling.isChecked = it.status
            sharedSettingToActivityViewModel.tfaWasChanged.value = true
//            if (it.status)
        }
    }

    private fun processCreateFingerprint(cipher: Cipher?) {
        cipher?.let {
            handleFingerprint.createFingerprintDialog(
                it,
                { unavailableToCreateFingerprint() },
                ::encryptPassword,
                ::biometricErrorCreateFingerprint
            )
            securityViewModel.createFingerprint.value = null
        }
    }

    private fun unavailableToCreateFingerprint() {
        showSnackBarMessage("Cannot create fingerprint")
        securityViewModel.biometricValue(false)
    }

    private fun encryptPassword(authResult: BiometricPrompt.AuthenticationResult) {
        if (handleFingerprint.isBiometricSuccess()) {
            authResult.cryptoObject?.cipher.let {
                securityViewModel.encryptPassword(it)
            }
        } else validateShowBiometricError()
    }

    private fun biometricErrorCreateFingerprint(errorCode: Int) {
        if (errorCode == 10 || errorCode == 7 || errorCode == 13) {
            unavailableToCreateFingerprint()
        }
    }

    private fun validateShowBiometricError() {
        when (handleFingerprint.getBiometricAuthenticateAbility()) {
            BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE ->
                showSnackBarMessage("No biometric features available on this device.")
            BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE ->
                showSnackBarMessage("Biometric features are currently unavailable.")
        }
    }

    private fun showSnackBarMessage(message: String) {
        val bottomNavView: BottomNavigationView? = activity?.findViewById(R.id.bottomNavView)
        bottomNavView?.let {
            Snackbar.make(bottomNavView, message, Snackbar.LENGTH_SHORT).apply {
                anchorView = bottomNavView
            }.show()
        }
    }

    private fun processPassCodeCreating(value: Boolean?) {
        value?.let {
            if (it)
                sharedSettingToActivityViewModel.createPassCodeFromSettings.value = Pair(true, this)
            else sharedSettingToActivityViewModel.createPassCodeFromSettings.value =
                Pair(false, this)
            securityViewModel.passCodeCreatingRequired.value = null
        }
    }

    private fun processPassCodeEnabled(value: Boolean?) {
        value?.let {
            smSecurityPassCodeEnabling.isChecked = it
            clBlockTimer.isVisible = it
            if (!it) smSecurityBiometricEnabling.isChecked = false
        }
    }

    private fun processNavigationToDeletePin(value: Boolean?) {
        value?.let {
            if (it) {
                navigateToDeletePin()
                securityViewModel.navigateToRemovePassCode.value = null
            }
        }
    }

    private fun processPassCodeApprove(value: Boolean?) {
        value?.let {
            if (it) sharedSettingToActivityViewModel.enterPassCodeFromSettings.value =
                Pair(true, this)
            securityViewModel.requestPinApprove.value = null
        }
    }

    private fun navigateToDeletePin() {
        if (findNavController().currentDestination?.id != R.id.securityFragment) return

        val direction = SecurityFragmentDirections.actionSecurityFragmentToDeletePassCodeFragment(
            securityViewModel.email
        )
        findNavController().navigate(direction)
    }

//    private fun navigateToTfaFragment(secretKet: String) {
//        if (findNavController().currentDestination?.id != R.id.securityFragment) return
//
//        val direction =
//            SecurityFragmentDirections.actionSecurityFragmentToTfaFragment(secretKet)
//        findNavController().navigate(direction)
//    }


    private fun popupEnablePin(value: Boolean?) {
        value?.let {
            MaterialAlertDialogBuilder(requireContext()).apply {
                setMessage(getString(R.string.message_activate_pin))
                setPositiveButton(android.R.string.ok) { dialog, _ -> dialog.dismiss() }
            }.create().show()
            securityViewModel.showPopup.value = null
        }
    }

    private fun processNotifySettings(value: Boolean?) {
        value?.let {
            if (it) {
                securityViewModel.apply {
                    loadEmail()
                    loadPinEnabled()
                    loadBiometricEnabled()
                }
            }
            notifySettingsViewModel.notifyFrToUpdateSefetyData.value = null
        }
    }

    private fun checkBiometricHardwareExistence(): Boolean {
        return when (handleFingerprint.getBiometricAuthenticateAbility()) {
            BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE -> false
            BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> false
            BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> false
            BiometricManager.BIOMETRIC_SUCCESS -> true
            else -> false
        }
    }

    private fun processTfa(userSettings: UserSettings?) {
        userSettings?.let {
//            sm2faEnabling.isChecked = it.tfa == true
//            settingsViewModel.enableTwoFa.value = null
        }
    }

//    private fun toggleTfa() =
//        securityViewModel.toggleTfa()

    private fun showSettings() {
        tfa.visible(false)
        settings.visible(true)
    }

    private fun showTfa() {
        tfa.visible(true)
        settings.visible(false)

        tfaFlow.initLifecycleOwner(viewLifecycleOwner, this)
        tfaFlow.initViews(
            tfa,
            tfaViewModel,
            null,
            sharedSettingToActivityViewModel,
            handleFieldFocus,
            TfaState.INFORMATION,
            true,
        )
    }

    override fun onDestroy() {
        sharedSettingToActivityViewModel.apply {
            passCodeWasCreated.value = null
            passCodeWasEntered.value = null
            tfaWasChanged.value = null
        }
        super.onDestroy()
    }
}