package io.sevenb.terminal.ui.screens.notification_settings

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.sevenb.terminal.data.model.enums.notifications.NotificationsTypeFromBack
import io.sevenb.terminal.data.model.messages.MainNotificationsPush
import javax.inject.Inject

class SharedNotificationsToSettingsViewModel @Inject constructor(): ViewModel() {
    val enableNotifications = MutableLiveData<List<NotificationsTypeFromBack>?>()
    val mainNotificationsList = MutableLiveData<List<MainNotificationsPush>>()
//    val enabledNotificationsOnBack = MutableLiveData<MutableList<TemplateResponse>>()
}