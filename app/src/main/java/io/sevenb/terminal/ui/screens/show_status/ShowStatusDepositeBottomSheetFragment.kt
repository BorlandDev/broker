package io.sevenb.terminal.ui.screens.show_status

import android.os.Bundle
import android.util.Log
import android.view.Display
import android.view.View
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.core.view.updateLayoutParams
import io.sevenb.terminal.R
import io.sevenb.terminal.device.utils.DateTimeUtil
import io.sevenb.terminal.domain.usecase.charts.AccountData
import io.sevenb.terminal.ui.base.BaseBottomSheetFragment
import io.sevenb.terminal.ui.screens.order.OrderViewModel
import io.sevenb.terminal.ui.screens.portfolio.PortfolioHistoryItem
import kotlinx.android.synthetic.main.fragment_order_success.*
import kotlinx.android.synthetic.main.fragment_show_status_deposite.*
import kotlinx.android.synthetic.main.fragment_show_status_deposite.first_amount
import kotlinx.android.synthetic.main.fragment_show_status_deposite.first_amount_ticker
import kotlinx.android.synthetic.main.fragment_show_status_deposite.iv_success
import kotlinx.android.synthetic.main.fragment_show_status_deposite.market_date_created
import kotlinx.android.synthetic.main.fragment_show_status_deposite.second_amount
import kotlinx.android.synthetic.main.fragment_show_status_deposite.success_title

class ShowStatusDepositeBottomSheetFragment : BaseBottomSheetFragment() {
    override fun layoutId() = R.layout.fragment_show_status_deposite
    override fun containerId() = R.id.container_show_status

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val data = arguments?.getParcelable<PortfolioHistoryItem>("content")
        Log.e("!!!", "data - $data")

        when(data?.status.toString()) {
            "Not confirmed" -> {
                iv_success.setImageResource(R.drawable.ic_failed)
            }
            "Pending" -> {
                iv_success.setImageResource(R.drawable.ic_in_proccess)

            }
            "Processing" -> {
                iv_success.setImageResource(R.drawable.ic_in_proccess)


            }
            "Finished" -> {
                iv_success.setImageResource(R.drawable.ic_success_blue)

            }
            "Failed" -> {
                iv_success.setImageResource(R.drawable.ic_failed)

            }
            "Refunded" -> {
                iv_success.setImageResource(R.drawable.ic_in_proccess)
            }
            else -> {
                iv_success.setImageResource(R.drawable.ic_in_proccess)
                success_title.text = "Processing"
            }
        }



        success_title.text = data?.status
        val timestamp = DateTimeUtil.parseToTimestamp(data?.createdAt ?: "")
        val date = DateTimeUtil.dateFromTimestamp(timestamp)
        val time = DateTimeUtil.timeFromTimestamp(timestamp)
        market_date_created.text = "Created at %s %s".format(date, time)

        val estimatedString =
            "${AccountData.getBaseCurrencyCharacter()}${
                data?.estimatedInBase?.let { OrderViewModel.roundDouble(it.toDouble(), 2, true) }
            }"
        second_amount.text = estimatedString

        first_amount.text = data?.amount
        first_amount_ticker.text = data?.symbol

        inner_container_deposit.setTransitionListener(object :
            MotionLayout.TransitionListener {
            override fun onTransitionStarted(p0: MotionLayout?, p1: Int, p2: Int) {}

            override fun onTransitionChange(p0: MotionLayout?, p1: Int, p2: Int, p3: Float) {}

            override fun onTransitionCompleted(p0: MotionLayout?, p1: Int) {
                dismiss()
            }

            override fun onTransitionTrigger(p0: MotionLayout?, p1: Int, p2: Boolean, p3: Float) {}
        })

        close_screen.setOnClickListener {
            dismiss()
        }
    }

    private fun formatDate(createdAt: String): String {
        val timestamp = DateTimeUtil.parseToTimestamp(createdAt)
        val date = DateTimeUtil.dateFromTimestamp(timestamp)
        val dateArray = date.split(" ")
        return "${dateArray[1]} ${dateArray[0]}, ${dateArray[2]}"
    }
}