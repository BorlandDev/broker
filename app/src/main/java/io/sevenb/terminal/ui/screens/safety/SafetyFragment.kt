package io.sevenb.terminal.ui.screens.safety

import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import io.sevenb.terminal.R
import io.sevenb.terminal.data.model.enums.auth.NewAuthStates
import io.sevenb.terminal.data.model.enums.pass_code.PassCodeStates
import io.sevenb.terminal.data.model.enums.SafetyStates
import io.sevenb.terminal.data.model.enums.auth.TfaState
import io.sevenb.terminal.domain.usecase.new_auth.HandleFieldFocus
import io.sevenb.terminal.domain.usecase.new_auth.HandleFingerprint
import io.sevenb.terminal.domain.usecase.safety.AccountSafetyStoring
import io.sevenb.terminal.ui.base.BaseFragment
import io.sevenb.terminal.ui.extension.viewModelProvider
import io.sevenb.terminal.ui.extension.visible
import io.sevenb.terminal.ui.screens.lock_timeout.LockTimeoutModule
import io.sevenb.terminal.ui.screens.lock_timeout.SharedLockModel
import io.sevenb.terminal.ui.screens.safety.passcode_state.PassCodeFlow
import io.sevenb.terminal.ui.screens.safety.passcode_state.PassCodeViewModel
import io.sevenb.terminal.ui.screens.safety.tfa.TfaFlow
import io.sevenb.terminal.ui.screens.safety.tfa.TfaViewModel
import kotlinx.android.synthetic.main.fragment_loader.*
import kotlinx.android.synthetic.main.fragment_safety.*
import kotlinx.android.synthetic.main.fragment_tfa.*
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

class SafetyFragment : BaseFragment() {
    private lateinit var safetyViewModel: SafetyViewModel
    private val navigationArgs: SafetyFragmentArgs by navArgs()
    private val sharedLockModel: SharedLockModel by activityViewModels { viewModelFactory }

    override fun layoutId() = R.layout.fragment_safety
    override fun onSearchTapListener() {}

    @Inject
    lateinit var handleFingerprint: HandleFingerprint

    @Inject
    lateinit var passCodeFlow: PassCodeFlow
    private lateinit var passCodeViewModel: PassCodeViewModel

    @Inject
    lateinit var lockTimeoutModule: LockTimeoutModule

    @Inject
    lateinit var safetyStoring: AccountSafetyStoring

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        safetyViewModel = viewModelProvider(viewModelFactory)
        passCodeViewModel = viewModelProvider(viewModelFactory)

        subscribeUI()
        processNavArgs()
    }

    private fun processNavArgs() {
        try {
            safetyViewModel.setNewAuthMethod(navigationArgs.safetyVariants)
        } catch (e: Exception) {

        }
    }

    private fun subscribeUI() {
        safetyViewModel.apply {
            authMethod.observe(viewLifecycleOwner, { processAuthMethod(it) })
            authConfirmed.observe(viewLifecycleOwner, { processAuthConfirmed(it) })
            processNavigation.observe(viewLifecycleOwner, { processNavigation(it) })
            showLoader.observe(viewLifecycleOwner, { processShowLoader(it) })
            logOut.observe(viewLifecycleOwner, { processLogOut(it) })
        }

        passCodeViewModel.authDone.observe(viewLifecycleOwner, { processAuthDone() })
        sharedLockModel.availableMethod.observe(viewLifecycleOwner, { processMethod(it) })
    }

    private fun processMethod(state: SafetyStates?) {
        state?.let {
            safetyViewModel.fromLocked = true
            sharedLockModel.availableMethod.value = null
            safetyViewModel.setNewAuthMethod(it)
        }
    }

    private fun processLogOut(value: Boolean?) {
        value?.let { if (it) navigateToAuth() }
    }

    private fun processAuthDone() {
        restoreRefreshToken()
    }

    private fun processShowLoader(message: String?) {
        loader.visible(true)
        message?.let { tv_message.text = message }
    }

    private fun processNavigation(state: NewAuthStates?) {
        state?.let {
            when (state) {
                NewAuthStates.DONE_AUTH -> navigateToPortfolio()
                NewAuthStates.LOGIN -> navigateToAuth()
            }
        }
    }

    private fun processAuthConfirmed(value: Boolean?) {
        lifecycleScope.launch { safetyStoring.storeIsLocked(false) }
        if (safetyViewModel.fromLocked == true) findNavController().popBackStack()
        else value?.let { if (it) navigateToPortfolio() }
    }

    private fun processAuthMethod(state: SafetyStates?) {
        state?.let {
            when (it) {
                SafetyStates.BOTH_VARIANTS_AVAILABLE -> bothVariantsEnabled()
                SafetyStates.FINGERPRINT_AVAILABLE -> processFingerprintEnabled()
                SafetyStates.PASS_CODE_AVAILABLE -> showPasscode()
                SafetyStates.NOT_LOCKED -> restoreRefreshToken()
            }
        }
    }

    private fun restoreRefreshToken() {
        safetyViewModel.restoreRefreshToken()
    }

    private fun processFingerprintEnabled() {
        //TODO
    }

    private fun navigateToPortfolio() {
        if (findNavController().currentDestination?.id != R.id.safetyFragment) return

        val direction = SafetyFragmentDirections.actionSafetyFragmentToPortfolioFragment()
        findNavController().navigate(direction)
    }

    private fun navigateToAuth() {
        if (findNavController().currentDestination?.id != R.id.safetyFragment) return

        val direction = SafetyFragmentDirections.actionSafetyFragmentToNewAuthFragment()
        findNavController().navigate(direction)
    }

    private fun showPasscode() {
        loadBlockData()
//        passCodeVisibility()
    }

    private fun loadBlockData() {
        lifecycleScope.launch {
            lockTimeoutModule.apply {
                init(this@SafetyFragment.lifecycleScope, sharedLockModel)
                when (restoreLockData()) {
                    null -> passCodeVisibility()
                    R.id.splashScreenFragment -> restoreRefreshToken()
                    R.id.safetyFragment -> passCodeVisibility()
                }
                removeData()
            }
        }
    }

    private fun bothVariantsEnabled() {
        loadBlockData()
//        passCodeVisibility()
    }

    private fun passCodeVisibility() {
        pbSafety.visible(false)
        passCode.visible(true)
        val state =
            if (safetyViewModel.availableVariant == SafetyStates.BOTH_VARIANTS_AVAILABLE)
                PassCodeStates.ENTER_PASS_CODE_BOTH_METHODS
            else PassCodeStates.ENTER_PASS_CODE
        passCodeFlow.initLifecycleOwner(viewLifecycleOwner, this, handleFingerprint)
        passCodeFlow.initViews(passCode, passCodeViewModel, safetyViewModel, state, "", "")
    }

}