package io.sevenb.terminal.ui.screens.trading

import android.view.View
import com.github.mikephil.charting.data.Entry
import io.sevenb.terminal.data.model.trading.TradeChartParams
import io.sevenb.terminal.data.model.trading.TradeChartRange
import io.sevenb.terminal.ui.screens.order.OrderBottomSheetFragment
import io.sevenb.terminal.ui.screens.select_currency.BaseSelectItem

interface BaseTradingListItem: BaseSelectItem {

    fun bind(
        itemView: View,
        openTrade: (Pair<String, OrderBottomSheetFragment.Companion.OrderSide>) -> Unit,
        baseCurrencyTicker: String,
        updateItemChartByPosition: (TradeChartParams) -> Unit,
        position: Int,
        updateExpandSelectedRange: (Int, TradeChartRange) -> Unit,
        updateCandleChartData: (TradeChartParams) -> Unit,
        updateExpandedCandleChartData: (TradeChartParams) -> Unit
    )

    fun bindChartData(
        itemView: View,
        listEntries: List<Entry>,
        tradeChartRange: TradeChartRange,
        update: Boolean = true
    )

    fun bindPrice(
        itemView: View,
        price: String
    )

}