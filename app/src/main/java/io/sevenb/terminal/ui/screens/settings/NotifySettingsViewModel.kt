package io.sevenb.terminal.ui.screens.settings

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import javax.inject.Inject

class NotifySettingsViewModel @Inject constructor() : ViewModel() {
    val notifyActToUpdateSefetyData = MutableLiveData<Boolean?>()
    val notifyFrToUpdateSefetyData = MutableLiveData<Boolean?>()
}