package io.sevenb.terminal.ui.screens.order

import android.os.Bundle
import android.view.View
import io.sevenb.terminal.R
import io.sevenb.terminal.ui.base.BaseBottomSheetFragment
import kotlinx.android.synthetic.main.fragment_new_order.*

class NewOrderBottomSheetFragment : BaseBottomSheetFragment() {
    override fun layoutId() = R.layout.fragment_new_order
    override fun containerId() = R.id.container_new_order

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        tl_test.addTab(tl_test.newTab().setText("Market"))
//        tl_test.addTab(tl_test.newTab().setText("Limit"))

//        vp_main_order.adapter = OrderPagerAdapter()
    }
}