package io.sevenb.terminal.ui.screens.settings

import android.Manifest
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.ImageView
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import coil.load
import coil.transform.CircleCropTransformation
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import io.sevenb.terminal.BuildConfig
import io.sevenb.terminal.R
import io.sevenb.terminal.data.model.broker_api.account.UserSettings
import io.sevenb.terminal.data.model.enums.notifications.NotificationsTypeFromBack
import io.sevenb.terminal.data.model.portfolio.LocalAccountData
import io.sevenb.terminal.data.model.settings.DefaultCurrencyItem
import io.sevenb.terminal.domain.usecase.new_auth.HandleFingerprint
import io.sevenb.terminal.domain.usecase.verification.VerificationData.Companion.ACCOUNT_STATUS_CONFIRMED
import io.sevenb.terminal.domain.usecase.verification.VerificationData.Companion.ACCOUNT_STATUS_VERIFIED
import io.sevenb.terminal.exceptions.Try
import io.sevenb.terminal.ui.base.BaseFragment
import io.sevenb.terminal.ui.extension.hideKeyboard
import io.sevenb.terminal.ui.extension.viewModelProvider
import io.sevenb.terminal.ui.extension.visible
import io.sevenb.terminal.ui.screens.dialog_rate_us.DialogThankYou
import io.sevenb.terminal.ui.screens.dialog_rate_us.DialogWhatCanWe
import io.sevenb.terminal.ui.screens.notification_settings.SharedNotificationsToSettingsViewModel
import io.sevenb.terminal.ui.screens.select_currency.BaseSelectItem
import io.sevenb.terminal.ui.screens.trading.SharedViewModel
import kotlinx.android.synthetic.main.fragment_settings.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.File
import java.io.FileOutputStream
import javax.crypto.Cipher
import javax.inject.Inject


class SettingsFragment : BaseFragment() {

    private lateinit var settingsViewModel: SettingsViewModel
    private lateinit var sharedViewModel: SharedViewModel
    private val sharedNotificationsToSettings: SharedNotificationsToSettingsViewModel by activityViewModels { viewModelFactory }
    private val notifySettingsViewModel: NotifySettingsViewModel by activityViewModels { viewModelFactory }
    private val sharedSettingToActivityViewModel: SharedSettingToActivityViewModel
            by activityViewModels { viewModelFactory }

    override fun layoutId() = R.layout.fragment_settings
    override fun hasBottomNavigation() = true
    override fun hasToolbarSearch() = false
    override fun toolbarTitleRes() = R.string.title_account_tab

    private var emailVisibility = false

    @Inject
    lateinit var handleFingerprint: HandleFingerprint

//    private var listOfNotifications = mutableListOf<NotificationsTypeFromBack>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        settingsViewModel = viewModelProvider(viewModelFactory)
        sharedViewModel = activity?.run {
            viewModelProvider(viewModelFactory)
        } ?: throw Exception("Invalid Activity")

        initView()
        subscribeUI()
        initializeData()
    }

    override fun onResume() {
        super.onResume()
        settingsViewModel.apply {
            accountInfo()
            getUserSettings()
        }
        lifecycleScope.launch {
            delay(300)
            hideKeyboard(requireActivity())
        }
    }

    private fun initView() {
        handleFingerprint.init(this)
        iv_account_icon.setOnClickListener {
//            dialogAvatarSetting()
        }

        status_get_verified.setOnClickListener {
//            settingsViewModel.getVerifications()
        }

        cl_default_currency.setOnClickListener {
            selectDefaultCurrency()
        }

        cl_security_container.setOnClickListener { launchSecurityFragment() }

        rate_us.setOnClickListener {
            rateUs()
        }

        rate_us_on_trustPilot.setOnClickListener {
            Try {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://www.trustpilot.com/evaluate/sevenb.io")
                    )
                )
            }
        }

        log_out.setOnClickListener {
            showLogoutDialog()
        }

        val applicationVersion = "v: ${BuildConfig.VERSION_NAME}"
        tv_version_code.text = applicationVersion

        img_email_visibility.setOnClickListener {
            emailVisibility = !emailVisibility
            changeEyeImage(img_email_visibility)
            changeEmailVisibility()
        }

        cl_notifications_container.setOnClickListener { toggleAllNotifications() }
        vNotificationsEnabling.setOnClickListener { toggleAllNotifications() }
        cl_notifications_settings_container.setOnClickListener { launchNotificationsSettingsFragment() }
    }


    private fun dialogWhatCanWe() {
        val dialog = DialogWhatCanWe.getInstance()
        dialog.show(parentFragmentManager, "")

        dialog.initListenerConfirm {
            if (it) {
//                settingsViewModel.resetRateUs()
                dialogThankYou()
            } else {
                settingsViewModel.showRateUs()
            }
        }
    }

    private fun dialogThankYou() {
        val dialog = DialogThankYou.getInstance()
        dialog.show(parentFragmentManager, "")
    }

    private fun showLogoutDialog() {
        MaterialAlertDialogBuilder(requireContext()).apply {
            setMessage(getString(R.string.alert_logout))
            setPositiveButton(R.string.label_yes) { dialog, _ ->
                settingsViewModel.accountLogOut()
                sharedViewModel.updateAssets = true
                dialog.dismiss()
            }
            setNegativeButton(R.string.label_no) { dialog, _ ->
                dialog.dismiss()
            }
        }.create().show()
    }

    override fun onSearchTapListener() {
        Timber.d("onSearchTapListener ")
    }

    private fun subscribeUI() {
        viewLifecycleOwner.let { owner ->
            settingsViewModel.apply {
                accountInfo.observe(owner, { accountInfo(it) })
                localAccountDataResult.observe(owner, { localAccountData(it) })
                dailyLimit.observe(owner, { dailyLimitEstimated(it) })
                verificationToken.observe(owner, { launchSumSubSdk(it) })
                userLogOutDone.observe(owner, { signInScreen() })
                userAvatarBitmap.observe(owner, { setUserAvatarBitmap(it) })
                snackbarMessage.observe(owner, { showSnackBarMessage(it) })
                biometricEnabled.observe(owner, { processBiometricEnabled(it) })
                passCodeEnabled.observe(owner, { processPassCodeEnabled(it) })
                passCodeCreatingRequired.observe(owner, { processPassCodeCreating(it) })
                fingerprintCreatingRequired.observe(owner, { processFingerprintCreating(it) })
                createFingerprint.observe(owner, { processCreateFingerprint(it) })
                navigateToRemovePassCode.observe(owner, { processNavigationToDeletePin(it) })
                requestPinApprove.observe(owner, { processPassCodeApprove(it) })
                showPopup.observe(owner, { popupEnablePin(it) })
                notificationsIsEnabled.observe(owner, { processNotificationsIsEnabled(it) })
                userSettings.observe(owner, {
                    processNotifications(it)
                })
//                showRateUs.observe(viewLifecycleOwner, { showGooglePlay() })
            }

            sharedViewModel.selectedCurrency.observe(owner, { selectedDefaultCurrency(it) })

            sharedSettingToActivityViewModel.apply {
                passCodeWasCreated.observe(owner, { settingsViewModel.pinValue(it) })
                passCodeWasEntered.observe(owner, { processPassCodeEntered(it) })
                tfaWasChanged.observe(owner, { processNotifySettings(it) })
            }

            notifySettingsViewModel.notifyFrToUpdateSefetyData.observe(
                owner, { processNotifySettings(it) })

            sharedNotificationsToSettings.enableNotifications.observe(
                owner, { processEnabledNotifications(it) })
        }
    }

    private fun rateUs() {
        val intent = Intent(Intent.ACTION_VIEW).apply {
            data = Uri.parse(
                "https://play.google.com/store/apps/details?id=io.sevenb.terminal")
            setPackage("com.android.vending")
        }
        startActivity(intent)
    }

    private fun processNotifications(userSettings: UserSettings?) {
        userSettings?.let {
            sharedNotificationsToSettings.mainNotificationsList.value = it.push
        }
    }

    private fun launchNotificationsSettingsFragment() {
        if (findNavController().currentDestination?.id != R.id.settingsFragment) return

        val direction =
            SettingsFragmentDirections.actionSettingsFragmentToNotificationsSettingsFragment()

        findNavController().navigate(direction)
    }

    private fun toggleAllNotifications() =
        settingsViewModel.toggleAllNotifications(!smNotificationsEnabling.isChecked)

    private fun launchSecurityFragment() {
        if (findNavController().currentDestination?.id != R.id.settingsFragment) return

        val directions =
            SettingsFragmentDirections.actionSettingsFragmentToSecurityFragment(
                settingsViewModel.email,
                settingsViewModel.tfa
            )
        findNavController().navigate(directions)
    }

    private fun processEnabledNotifications(list: List<NotificationsTypeFromBack>?) {
        list?.let {
            if (list.isEmpty()) {
                smNotificationsEnabling.isChecked = false
                cl_notifications_settings_container.isVisible = false
                sharedNotificationsToSettings.enableNotifications.value = null
            }
        }
    }

    private fun processNotificationsIsEnabled(value: Boolean?) {
        value?.let {
            smNotificationsEnabling.isChecked = it
            cl_notifications_settings_container.visible(it)
        }
    }

    private fun processNotifySettings(value: Boolean?) {
        value?.let {
            if (it) {
                settingsViewModel.apply {
                    loadEmail()
                    loadPinEnabled()
                    loadBiometricEnabled()
                    getUserSettings()
                }
            }
            notifySettingsViewModel.notifyFrToUpdateSefetyData.value = null
        }
    }

    private fun processPassCodeApprove(value: Boolean?) {
        value?.let {
            if (it) sharedSettingToActivityViewModel.enterPassCodeFromSettings.value =
                Pair(true, this)
            settingsViewModel.requestPinApprove.value = null
        }
    }

    private fun processNavigationToDeletePin(value: Boolean?) {
        value?.let {
            if (it) {
                navigateToDeletePin()
                settingsViewModel.navigateToRemovePassCode.value = null
            }
        }
    }

    private fun processPassCodeEntered(value: Boolean?) {
        value?.let {
            settingsViewModel.removeFingerprint()
        }
    }

    private fun processFingerprintCreating(value: Boolean?) {
        value?.let {
            if (it)
                settingsViewModel.createFingerprint()
            else sharedSettingToActivityViewModel.enterPassCodeFromSettings.value =
                Pair(false, this)

            settingsViewModel.fingerprintCreatingRequired.value = null
        }
    }

    private fun processPassCodeCreating(value: Boolean?) {
        value?.let {
            if (it)
                sharedSettingToActivityViewModel.createPassCodeFromSettings.value = Pair(true, this)
            else sharedSettingToActivityViewModel.createPassCodeFromSettings.value =
                Pair(false, this)
            settingsViewModel.passCodeCreatingRequired.value = null
        }
    }

    private fun processPassCodeEnabled(value: Boolean?) {
        value?.let {
//            smPassCodeEnabling.isChecked = it
//            if (!it) smBiometricEnabling.isChecked = false
        }
    }

    private fun processBiometricEnabled(value: Boolean?) {
        value?.let {
//            smBiometricEnabling.isChecked = it
//            settingsViewModel.biometricEnabled.value = null
        }
    }

    private fun initializeData() {
        val filePath = requireContext().cacheDir.path + AVATAR_FILE_PATH
        iv_account_icon.load(
            ContextCompat.getDrawable(
                requireContext(),
                R.drawable.ic_setting_avatar_placeholder
            )
        ) {//File(filePath)) {
            error(R.drawable.ic_account_placeholder)
            transformations(CircleCropTransformation())
        }
    }

    private fun setUserAvatarBitmap(bitmap: Bitmap) {
        iv_account_icon.load(
            ContextCompat.getDrawable(
                requireContext(),
                R.drawable.ic_setting_avatar_placeholder
            )
        ) {
            error(R.drawable.ic_account_placeholder)
            transformations(CircleCropTransformation())
        }
    }

    private fun accountInfo(accountInfo: LocalAccountData) {
        val email = shortEmail(accountInfo.email)
        account_email.text = email
        tv_first_letter.text = accountInfo.email.first().toString().toUpperCase()

        when (accountInfo.status) {
            ACCOUNT_STATUS_CONFIRMED -> {
                status_get_verified.isVisible = true
                status_verified.isVisible = false
            }
            ACCOUNT_STATUS_VERIFIED -> {
                status_get_verified.isVisible = false
                status_verified.isVisible = true
            }
        }
    }

    private fun dailyLimitEstimated(estimated: String) {
        daily_withdrawal_limit.text = "%s %s".format(
            getString(R.string.title_daily_withdrawal_limit),
            estimated
        )
    }

    private fun localAccountData(localAccountData: LocalAccountData) {
        val email = shortEmail(localAccountData.email)
        account_email.text = email
        tv_first_letter.text = email.first().toString().toUpperCase()
    }

    private fun updateEmailField(email: String?) {
        email?.let {
            val shortEmail = shortEmail(email)
            account_email.text = shortEmail
            tv_first_letter.text = shortEmail.first().toString().toUpperCase()
        }
    }

    private fun launchSumSubSdk(token: String) {
        // SumSub SDK calling was here
    }

    private fun dialogAvatarSetting() {
        val dialogItems = arrayOf("Take a picture", "Choose a file")

        MaterialAlertDialogBuilder(requireContext())
            .setItems(dialogItems) { _, which ->
                when (which) {
                    0 -> takePictureAvatar()
                    1 -> chooseFileAvatar()
                }
            }
            .setNegativeButton(android.R.string.cancel) { dialog, _ -> dialog.dismiss() }
            .show()
    }

    private fun takePictureAvatar() {
        if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA) ==
            PackageManager.PERMISSION_GRANTED
        ) {
            takeCameraPicture()
        } else {
            requestCameraPermission()
        }
    }

    private fun requestCameraPermission() {
        ActivityCompat.requestPermissions(
            requireActivity(),
            arrayOf(Manifest.permission.CAMERA),
            PERMISSION_REQUEST_CAMERA
        )
    }

    private fun takeCameraPicture() {
        startActivityForResult(
            Intent().apply {
                action = MediaStore.ACTION_IMAGE_CAPTURE
            },
            TAKE_PHOTO_AVATAR_REQUEST_CODE
        )
    }

    private fun chooseFileAvatar() {
        startActivityForResult(
            Intent().apply {
                action = Intent.ACTION_PICK
                data = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            },
            TAKE_AVATAR_FILE_REQUEST_CODE
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == PERMISSION_REQUEST_CAMERA) {
            if (grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                takePictureAvatar()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode != RESULT_OK) return
        when (requestCode) {
            TAKE_AVATAR_FILE_REQUEST_CODE -> takePictureAvatarResult(data)
            TAKE_PHOTO_AVATAR_REQUEST_CODE -> takePhotoAvatarResult(data)
        }
    }

    private fun takePictureAvatarResult(data: Intent?) {
        Timber.d("takePictureAvatar data = ${data?.data}")
        (data?.data)?.let {
            val bitmap = MediaStore.Images.Media.getBitmap(context?.contentResolver, it)
            storeAvatarBitmap(bitmap)
            setUserAvatarBitmap(bitmap)
        }
    }

    private fun takePhotoAvatarResult(data: Intent?) {
        Timber.d("takePhotoAvatar data = ${data?.data}")
        val bitmap = data?.extras?.get("data") as Bitmap
        storeAvatarBitmap(bitmap)
        setUserAvatarBitmap(bitmap)
    }

    private fun storeAvatarBitmap(bitmap: Bitmap) {
        val avatarFile = File(requireContext().cacheDir.path + AVATAR_FILE_PATH)
        val outputStream = FileOutputStream(avatarFile)
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream)
        outputStream.flush()
        outputStream.close()
    }

    private fun selectedDefaultCurrency(baseSelectItem: BaseSelectItem?) {
        if (baseSelectItem !is DefaultCurrencyItem) return
        sharedViewModel.selectedCurrency.value = null

        settingsViewModel.storeDefaultCurrency(baseSelectItem.ticker)
        if (baseSelectItem.ticker != selectedItem) {
            hasUpdated = true
            hasUpdatedCheck = true
            selectedItem = baseSelectItem.ticker
        }
    }

    private fun signInScreen() {
        if (findNavController().currentDestination?.id != R.id.settingsFragment) return

        val direction = SettingsFragmentDirections.actionSettingsFragmentToNewAuthFragment()
        findNavController().navigate(direction)
    }

    private fun selectDefaultCurrency() {
        sharedViewModel.selectCurrencyList.value = presetDefaultCurrencies
        openCurrencyPicker()
    }

    private fun openCurrencyPicker() {
        if (findNavController().currentDestination?.id != R.id.settingsFragment) return

        val direction =
            SettingsFragmentDirections.actionSettingsFragmentToSelectCurrencyFragment(true)
        findNavController().navigate(direction)
    }

    private fun openConfirmPhone() {
        if (findNavController().currentDestination?.id != R.id.settingsFragment) return

        val direction = SettingsFragmentDirections.actionSettingsFragmentToConfirmPhoneFragment()
        findNavController().navigate(direction)
    }

    private fun showSnackBarMessage(message: String) {
        val bottomNavView: BottomNavigationView? = activity?.findViewById(R.id.bottomNavView)
        bottomNavView?.let {
            Snackbar.make(bottomNavView, message, Snackbar.LENGTH_SHORT).apply {
                anchorView = bottomNavView
            }.show()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        super.onDestroy()
    }

    private fun changeEyeImage(imageView: ImageView) {
        imageView.background =
            if (emailVisibility) ContextCompat.getDrawable(requireContext(), R.drawable.ic_password)
            else ContextCompat.getDrawable(requireContext(), R.drawable.ic_password_hidden)
    }

    private fun changeEmailVisibility() {
        val email = settingsViewModel.accountDataInfo?.email
        if (emailVisibility) email?.let {
            account_email.text = it
        } else {
            email?.let { account_email.text = shortEmail(it) }
        }
    }

    private fun shortEmail(email: String, length: Int = 6): String {
        if (email.length <= length) return email
        val starsCount = email.length - length
        var starsString = ""
        for (i in 0 until starsCount) {
            starsString += "*"
        }
        return "${email.take(length / 2)}$starsString${email.takeLast(length / 2)}"
    }

    companion object {
        private const val TAKE_AVATAR_FILE_REQUEST_CODE = 0
        private const val TAKE_PHOTO_AVATAR_REQUEST_CODE = 1
        private const val PERMISSION_REQUEST_CAMERA = 0
        private const val SETTINGS_SUBFOLDER = "settings"
        private const val AVATAR_FILE_NAME = "7b-user-avatar.png"
        val AVATAR_FILE_PATH by lazy {
            File.pathSeparator + SETTINGS_SUBFOLDER + File.pathSeparator + AVATAR_FILE_NAME
        }
        private val presetDefaultCurrencies by lazy {
            listOf(
                DefaultCurrencyItem(
                    "EUR",
                    "Euro",
                    2790,
                    isExpanded = false,
                    updateExpandedStateChannel = null,
                    currentRange = null
                ),
                DefaultCurrencyItem(
                    "RUB",
                    "Ruble",
                    2806,
                    isExpanded = false,
                    updateExpandedStateChannel = null,
                    currentRange = null
                ),
                DefaultCurrencyItem(
                    "GBP",
                    "Pound sterling",
                    2791,
                    isExpanded = false,
                    updateExpandedStateChannel = null,
                    currentRange = null
                ),
                DefaultCurrencyItem(
                    "USD",
                    "US Dollar",
                    2781,
                    isExpanded = false,
                    updateExpandedStateChannel = null,
                    currentRange = null
                ),
                DefaultCurrencyItem(
                    "AUD",
                    "Australian Dollar",
                    2782,
                    isExpanded = false,
                    updateExpandedStateChannel = null,
                    currentRange = null
                ),
                DefaultCurrencyItem(
                    "BRL",
                    "Brazilian Real",
                    2783,
                    isExpanded = false,
                    updateExpandedStateChannel = null,
                    currentRange = null
                ),
                DefaultCurrencyItem(
                    "NGN",
                    "Nigerian Naira",
                    2819,
                    isExpanded = false,
                    updateExpandedStateChannel = null,
                    currentRange = null
                ),
                DefaultCurrencyItem(
                    "UAH",
                    "Ukrainian Hryvnia",
                    2824,
                    isExpanded = false,
                    updateExpandedStateChannel = null,
                    currentRange = null
                )
            )
        }
        var hasUpdated = false
        var hasUpdatedCheck = false
        private var selectedItem = ""
    }

    private fun tradingFragment() {
        if (findNavController().currentDestination?.id != R.id.settingsFragment) return

        val direction = SettingsFragmentDirections.actionSettingsFragmentToChangePasswordFragment()
        findNavController().navigate(direction)
    }

    private fun processCreateFingerprint(cipher: Cipher?) {
        cipher?.let {
            handleFingerprint.createFingerprintDialog(
                it,
                { unavailableToCreateFingerprint() },
                ::encryptPassword,
                ::biometricErrorCreateFingerprint
            )
            settingsViewModel.createFingerprint.value = null
        }
    }

    private fun unavailableToCreateFingerprint() {
        showSnackBarMessage("Cannot create fingerprint")
        settingsViewModel.biometricValue(false)
    }

    private fun encryptPassword(authResult: BiometricPrompt.AuthenticationResult) {
        if (handleFingerprint.isBiometricSuccess()) {
            authResult.cryptoObject?.cipher.let {
                settingsViewModel.encryptPassword(it)
            }
        } else validateShowBiometricError()
    }

    private fun biometricErrorCreateFingerprint(errorCode: Int) {
        if (errorCode == 10 || errorCode == 7 || errorCode == 13) {
            unavailableToCreateFingerprint()
//            settingsViewModel.fingerprintCancelled()
//            if (errorCode == 7) settingsViewModel.biometricEnabled = false
        }
    }

    private fun validateShowBiometricError() {
        when (handleFingerprint.getBiometricAuthenticateAbility()) {
            BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE ->
                showSnackBarMessage("No biometric features available on this device.")
            BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE ->
                showSnackBarMessage("Biometric features are currently unavailable.")
        }
    }

    private fun navigateToDeletePin() {
        if (findNavController().currentDestination?.id != R.id.settingsFragment) return

        val direction = SettingsFragmentDirections.actionSettingsFragmentToDeletePassCodeFragment(
            settingsViewModel.email
        )
        findNavController().navigate(direction)
    }

    override fun onDestroy() {
        sharedSettingToActivityViewModel.apply {
            passCodeWasCreated.value = null
            passCodeWasEntered.value = null
        }
        super.onDestroy()
    }

    private fun popupEnablePin(value: Boolean?) {
        value?.let {
            MaterialAlertDialogBuilder(requireContext()).apply {
                setMessage(getString(R.string.message_activate_pin))
                setPositiveButton(android.R.string.ok) { dialog, _ -> dialog.dismiss() }
            }.create().show()
            settingsViewModel.showPopup.value = null
        }
    }


    private fun checkBiometricHardwareExistence(): Boolean {
        return when (handleFingerprint.getBiometricAuthenticateAbility()) {
            BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE -> false
            BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> false
            BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> false
            BiometricManager.BIOMETRIC_SUCCESS -> true
            else -> false
        }
    }
}
