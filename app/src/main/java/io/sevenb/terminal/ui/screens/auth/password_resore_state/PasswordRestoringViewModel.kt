package io.sevenb.terminal.ui.screens.auth.password_resore_state

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.sevenb.terminal.data.model.auth.UserCaptcha
import io.sevenb.terminal.data.model.auth.UserData
import io.sevenb.terminal.data.model.broker_api.RequestTokenResponse
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.enum_model.ErrorMessageType
import io.sevenb.terminal.data.model.enums.GeeTestResult
import io.sevenb.terminal.data.model.enums.auth.NewAuthStates
import io.sevenb.terminal.data.model.enums.auth.PasswordRestoringStates
import io.sevenb.terminal.device.utils.DateTimeUtil
import io.sevenb.terminal.domain.usecase.auth.*
import io.sevenb.terminal.domain.usecase.safety.AESCrypt
import io.sevenb.terminal.domain.usecase.validate.CheckCredentials
import io.sevenb.terminal.utils.Logger
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class PasswordRestoringViewModel @Inject constructor(
    private val pRequestVerificationCode: RequestVerificationCode,
    private val sendConfirmationCode: SendConfirmationCode,
    private val storeRefreshToken: StoreRefreshToken,
    private val validateEmailPassword: ValidateEmailPassword,
    private val startResendTimer: StartResendTimer,
    private val parseErrorMessage: ParseErrorMessage,

    private val localUserDataStoring: LocalUserDataStoring,
//    private val storePassword: StorePassword,
    private val setNewPassword: SetNewPassword,
    private val creatingPassword: CreatingPasswordCheck,
    private val signInUser: SignInUser,
    private val aesCrypt: AESCrypt,
    private val checkCredentials: CheckCredentials
) : ViewModel() {

    val errorString = MutableLiveData<String>()
    val errorMessage = MutableLiveData<ErrorMessageType>()

    val geeTestResult = MutableLiveData<GeeTestResult>()
    val showResendButton = MutableLiveData<Boolean>()
    val userExistsResult = MutableLiveData<Boolean>()
    val authState = MutableLiveData<NewAuthStates>()

    //    val creatingBiometricCipher = MutableLiveData<Cipher?>()
    val startCaptchaFlow = MutableLiveData<Boolean>()
    val timerValue = MutableLiveData<String>()
    val showSmsCode = MutableLiveData<Boolean>()
    val currentState = MutableLiveData<PasswordRestoringStates>()
    val showLoader = MutableLiveData<Pair<Boolean, String>>()
    val passCodeCreationRequired = MutableLiveData<Boolean>()

    private lateinit var restorePasswordToken: RequestTokenResponse

    var userData = UserData()
    private var showTimeError = 0L
    private var localAuthState = NewAuthStates.PASSWORD_RESTORING


    fun requestOrResendRestorePasswordCode(userCaptcha: UserCaptcha? = null) {
        if (localAuthState == NewAuthStates.CONTINUES_PASSWORD_RESTORING) {
            requestRestorePasswordCode()
        } else {
            userData.captcha = userCaptcha
            requestRestorePasswordCode()
        }
    }

    private fun requestRestorePasswordCode() {
        viewModelScope.launch {
            startResendTimer.execute(this)
            showResendButton.value = false

            when (val result = pRequestVerificationCode.executeRestorePassword(userData.email)) {
                is Result.Success -> {
                    geeTestResult.value = GeeTestResult.SUCCESS
                    restorePasswordToken = result.data
                }
                is Result.Error -> {
                    Logger.sendLog(
                        "PasswordRestoringViewModel",
                        "requestRestorePasswordCode",
                        result.exception
                    )
                    result.exception.message?.let{
                        when {
                            it.contains("user_exists") -> {
                                geeTestResult.value = GeeTestResult.USER_EXISTS
                                errorMessage.value = ErrorMessageType.USER_ALREADY_EXISTS
                                return@launch
                            }
                            it.contains("bad_params") -> {
                                geeTestResult.value = GeeTestResult.USER_EXISTS
                                val errorMessage = parseErrorMessage.execute(result.message)
                                errorString.value = errorMessage?.message
                                return@launch
                            }
                            else -> {
                                geeTestResult.value = GeeTestResult.FAIL
                                errorString.value = "Server error: code 2"
                            }
                        }
                    }
                }
            }
        }
    }

    fun confirmCodeRestorePassword(confirmationCode: String) {
        viewModelScope.launch {
            when (val responseToken = sendConfirmationCode.executeVerifyCode(
                userData.email,
                confirmationCode,
                restorePasswordToken.token
            )) {
                is Result.Success -> {
                    localUserDataStoring.updateLocalAccountData(userData.email)
                    restorePasswordToken.token = responseToken.data.token
                    changeCurrentState(PasswordRestoringStates.PASSWORD_RESTORING_NEW_PASSWORD)
                }
                is Result.Error -> {
                    Logger.sendLog(
                        "PasswordRestoringViewModel",
                        "confirmCodeRestorePassword",
                        responseToken.exception
                    )
                    errorString.value = "Incorrect confirmation code"
                }
            }
        }
    }

    private fun createNewPassword() {
        viewModelScope.launch {
            when (val setNewPassword =
                setNewPassword.execute(
                    userData.email, userData.password, restorePasswordToken.token
                )) {
                is Result.Success -> {
                    if (setNewPassword.data.result) {
                        passCodeCreationRequired.value = true
                    } else {
                        errorString.value = "Server error: code 0"
                    }
                }
                is Result.Error -> {
                    errorString.value = "Server error: code 1"
                }
            }
        }
    }

    fun checkPassword(password: String) = creatingPassword.checkPassword(password)

    fun setNewState(state: NewAuthStates) {
        localAuthState = state
        authState.value = state
    }

    fun validateEmail(email: String) =
        checkCredentials.checkCredentials(email)

    fun validatePassword(password: String) {
        if (!validateEmailPassword.validatePassword(password)) {
            errorMessage.value = ErrorMessageType.PASSWORD_INCORRECT
            return
        }

        userData.password = password
        createNewPassword()
    }

    fun subscribeUI() {
        cancelTimerJob()
        startResendTimer.timerJob = viewModelScope.launch {
            startResendTimer.timerValueFlow.collect {
                timerValue.value = DateTimeUtil.formatSeconds(it.toLong())
                if (it == 0) showResendButton.value = true
            }
        }
    }

    fun changeCurrentState(state: PasswordRestoringStates) {
        currentState.value = state
    }

//    fun encryptPassword(password: String?, cipher: Cipher?) {
//        viewModelScope.launch {
//            if (password.isNullOrEmpty()) return@launch
//
//            val encryptionResult = storePassword.encryptPassword(
//                password, cipher ?: return@launch
//            )
//            when (encryptionResult) {
//                is Result.Success -> {
//                    signInAfterAccountCreated()
//                    errorString.value = "Fingerprint created"
//                }
//                is Result.Error -> errorString.value = "Fingerprint creating error"
//            }
//        }
//    }


//    private fun signInUser(email: String, password: String) {
//        viewModelScope.launch {
//            when (val signInResult = signInUser.execute(email, password)) {
//                is Result.Success -> {
//                    UserAuthRepository.accessToken = signInResult.data.accessToken
//                    UserAuthRepository.refreshToken = signInResult.data.refreshToken
//
//                    localUserDataStoring.apply {
//                        updateLocalAccountData(email)
//                        val encryptedPassword = aesCrypt.encrypt(password)
//                        encryptedPassword?.let { storePassword(it) }
//                    }
//
//                    storeRefreshToken(signInResult.data.refreshToken)
//                }
//                is Result.Error -> {
//                    when {
//                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
//                        signInResult.message.contains("Incorrect login params") -> {
//                            errorMessage.value = ErrorMessageType.LOGIN_ERROR
//                        }
//                        signInResult.message.contains("unconfirmed_user") -> {
//                            val errorMessage = parseErrorMessage.exec(signInResult.message)
//                            errorMessage?.extra?.let {
//                                restorePasswordToken = RequestTokenResponse(it.accessToken)
//                            }
//                            this@PasswordRestoringViewModel.errorMessage.value =
//                                ErrorMessageType.CONFIRMATION_REQUIRED
//                        }
//                        else -> errorString.value = "Server error: code 0"
//                    }
////                    errorString.value = "Server error: code 0"
//                }
//            }
//        }
//    }

//    private fun storeRefreshToken(refreshToken: String) {
//        viewModelScope.launch {
//            when (val storeResult = storeRefreshToken.execute(refreshToken)) {
//                is Result.Success -> {
//                    setNewState(NewAuthStates.DONE_AUTH)
//                }
//                is Result.Error -> {
//                    errorString.value = storeResult.exception.message
//                }
//            }
//        }
//    }

    private fun cancelTimerJob() = startResendTimer.cancelTimerJob()

//    private fun showNoInternetConnection() {
//        val currentTime = System.currentTimeMillis()
//        if (currentTime - showTimeError >= 1500) {
//            errorString.value = ConnectionStateMonitor.noInternetError
//            showTimeError = currentTime
//        }
//    }
}