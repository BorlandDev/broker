package io.sevenb.terminal.ui.screens.select_currency

import android.content.Context
import android.os.Bundle
import android.text.InputFilter
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.appcompat.widget.SearchView
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import io.sevenb.terminal.R
import io.sevenb.terminal.data.model.settings.DefaultCurrencyItem
import io.sevenb.terminal.data.model.withdrawal.WithdrawItem
import io.sevenb.terminal.data.repository.auth.PreferenceRepository
import io.sevenb.terminal.ui.base.BaseDialogFragment
import io.sevenb.terminal.ui.extension.*
import io.sevenb.terminal.ui.screens.order.OrderViewModel.Companion.roundDouble
import io.sevenb.terminal.ui.screens.order.OrderViewModel.Companion.roundFloat
import io.sevenb.terminal.ui.screens.trading.SharedViewModel
import kotlinx.android.synthetic.main.fragment_select_currency.*

class SelectCurrencyFragment : BaseDialogFragment() {

    private lateinit var sharedViewModel: SharedViewModel
    private val selectCurrencyAdapter = SelectCurrencyAdapter(::tickerSelection, ::showPlaceholder)
    private val navigationArgs: SelectCurrencyFragmentArgs by navArgs()

    private lateinit var selectCurrencyViewModel: SelectCurrencyViewModel

    override fun layoutId() = R.layout.fragment_select_currency
    override fun toolbarHasBackArrow() = true
    override fun hasToolbarSearch() = true

    private var isShowing = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.run {
            sharedViewModel = viewModelProvider(viewModelFactory)
            selectCurrencyViewModel = viewModelProvider(viewModelFactory)
        } ?: throw Exception("Invalid Activity")
        initView()
        subscribeUi()
    }

    private fun initView() {
        searchView.findViewById<TextView>(R.id.search_src_text).filters =
            arrayOf(InputFilter.LengthFilter(15))

        iv_back_button.setOnClickListener {
            hideKeyboardInAndroidFragment()
            dismiss()
        }

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                selectCurrencyAdapter.filter.filter(query)
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                selectCurrencyAdapter.filter.filter(newText)
                return false
            }
        })

        initSelectList()
    }

    private fun subscribeUi() {
        sharedViewModel.selectCurrencyList.observe(viewLifecycleOwner, {
            if (!isShowing && it.isNotEmpty()) {
                try {
                    val mutableList = it.toMutableList().map { item ->
                        item.apply {
                            ticker =
                                if (ticker == "BCHA")
                                    "XEC"
                                else ticker
                        }
                        item
                    }
                    setAdapterList(mutableList)
                    isShowing = true
                } catch (e: Exception) {

                }
            }
        })
    }

    private fun initSelectList() {
        select_currency_list.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(
                context, LinearLayoutManager.VERTICAL, false
            )
            adapter = selectCurrencyAdapter
        }
    }

    private fun hideKeyboardInAndroidFragment() {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm?.hideSoftInputFromWindow(view?.windowToken, 0)
    }

    private fun setAdapterList(list: List<BaseSelectItem>) {
        var submittedList: List<BaseSelectItem>
        if (navigationArgs.isSettings) {
            val item = list.first {
                (it as DefaultCurrencyItem).ticker == PreferenceRepository.baseCurrencyTicker
            }

            val filteredDefaultCurrencies = list.toMutableList().apply {
                remove(item)
                add(0, item)
            }

            val unselectedItems = filteredDefaultCurrencies.slice(1..list.lastIndex) as ArrayList

            val sortedList =
                unselectedItems.sortedWith(compareBy { (it as DefaultCurrencyItem).ticker })

            filteredDefaultCurrencies.apply {
                removeAll { unselectedItems.contains(it) }
                addAll(sortedList)
            }
            submittedList = filteredDefaultCurrencies
        } else {
            submittedList = selectCurrencyViewModel.sortCurrency(list)
            if (list.isNotEmpty()) {
                if (list.typeIs<DepositCurrencyItem>() || list.typeIs<WithdrawItem>()) {
                    submittedList = filterCurrencies(submittedList)
                }

                val rates = sharedViewModel.ratesList

                if (list.typeIs<WithdrawItem>() && !rates.isNullOrEmpty()) {
                    val filteredList = filterCurrencies(submittedList)

                    filteredList.forEach {
                        val marketRate = rates[it.ticker]
                        val amount = it.amount.getFloatValue()
                        marketRate?.let { rate ->
                            val estimatedValue = amount * rate.rate
                            it.estimated = roundDouble(estimatedValue.toDouble(), 2, true)
                        }
                    }
                    submittedList = filteredList
                }
            }
        }
        selectCurrencyAdapter.submitList(submittedList)
        searchView.focusField(requireContext())
    }

    private fun filterCurrencies(list: List<BaseSelectItem>): List<BaseSelectItem> {
        val tempList = selectCurrencyViewModel.filterBnbBscNetwork(list)

        return selectCurrencyViewModel.makeSingleBTCTicker(tempList)
    }

    private fun tickerSelection(selectedCurrency: BaseSelectItem) {
        sharedViewModel.selectedCurrency.value = selectedCurrency.apply {
            ticker = if (ticker == "XEC") "BCHA" else ticker
        }
        hideKeyboardInAndroidFragment()
        dismiss()
    }

    private fun showPlaceholder(showPlaceholder: Boolean) =
        tv_search_placeholder.visible(showPlaceholder)

    companion object {
        const val BNB_NETWORK = "BNB"
        const val BSC_NETWORK = "BSC"
    }
}