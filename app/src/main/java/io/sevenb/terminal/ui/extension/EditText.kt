package io.sevenb.terminal.ui.extension

import android.content.Context
import android.text.InputFilter
import android.text.Spanned
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import timber.log.Timber
import java.util.regex.Matcher
import java.util.regex.Pattern

fun EditText.onActionDoneListener(callback: () -> Unit) {
    this.setOnEditorActionListener { _, actionId, _ ->
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            callback()
        }

        false
    }
}

fun EditText?.focusField(context: Context?) {
    context?.let {
        val inputMethodManager =
            it.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

        inputMethodManager.toggleSoftInput(
            InputMethodManager.SHOW_FORCED,
            InputMethodManager.HIDE_IMPLICIT_ONLY
        )
        this?.let { et ->
            et.requestFocus()
            if (text.isNotEmpty())
                et.setSelection(et.text.length)
        }
    }
}

fun EditText?.focusField() {
    Timber.d("focusField ${this?.isInvisible} ${this?.id}")

    this?.isInvisible ?: return

    this.postDelayed({
        val inputMethodManager =
            this.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.toggleSoftInput(
            InputMethodManager.SHOW_FORCED,
            0
        )
        this.requestFocus()
        this.cursorToLastIfSelected()
    }, 300)
}

fun EditText.getFloatValue() = this.text.toString().toFloatOrNull() ?: 1f
fun EditText.getDoubleValue() = this.text.toString().toDoubleOrNull() ?: 1.0

fun EditText.cursorToLastIfSelected(view: EditText? = null) {
    try {
        if (view == null) {
            val length = this.text.toString().length
            if (this.isFocused && length >= 0) {
                this.setSelection(length)
            }
        } else {
            val length = view.text.toString().length
            if (view.isFocused && length > 0) {
                view.setSelection(length)
            }
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

fun EditText.tryText(value: String?) {
    try {
        value?.let {
            setAndSelect(it)
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

fun EditText.getString(): String {
    return try {
        this.text.toString()
    } catch (e: Exception) {
        e.printStackTrace()
        ""
    }
}

fun EditText.setAndSelect(text: String) {
    try {
        this.setText(text)
        this.cursorToLastIfSelected()
    } catch (e: Exception) {

    }
}

class DecimalDigitsInputFilter(val digitsBeforeZero: Int, val digitsAfterZero: Int) :
    InputFilter {
    var mPatternInt: Pattern =
        Pattern.compile("[0-9]{0," + (digitsBeforeZero - 1) + "}")

    //
    val mPattern =
        Pattern.compile("[0-9]{0," + (digitsBeforeZero - 1) + "}+((\\.[0-9]{0," + (digitsAfterZero - 1) + "})?)||(\\.)?")

    var mPatternDecimal: Pattern =
        Pattern.compile("((\\.[0-9]{0," + (digitsAfterZero - 1) + "})?)||(\\.)?")

    override fun filter(
        source: CharSequence?,
        start: Int,
        end: Int,
        dest: Spanned?,
        dstart: Int,
        dend: Int
    ): CharSequence? {
        val string = dest.getPlainDecimalString()

        if (string.isNullOrEmpty()) return null

        var pos = -1

        string.forEachIndexed { index, c ->
            if (c.toInt() == 46) pos = index
        }

        if (pos < dstart) {
            val matcher: Matcher = mPattern.matcher(dest)
            val asdasd = matcher.matches()
            return if (!asdasd) "" else null
        }

        return null
    }

}