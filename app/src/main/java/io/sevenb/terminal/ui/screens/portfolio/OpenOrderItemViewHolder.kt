package io.sevenb.terminal.ui.screens.portfolio

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import io.sevenb.terminal.data.model.order.OrderItem

class OpenOrderItemViewHolder(
    itemView: View,
) : RecyclerView.ViewHolder(itemView) {

    fun bind(baseOrderListItem: BaseOrderListItem) {

        when(baseOrderListItem) {
            is OrderItem -> {
                baseOrderListItem.bind(itemView)
            }
        }

    }

}
