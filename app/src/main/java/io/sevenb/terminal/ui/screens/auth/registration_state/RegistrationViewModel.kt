package io.sevenb.terminal.ui.screens.auth.registration_state

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.sevenb.terminal.data.model.auth.UserCaptcha
import io.sevenb.terminal.data.model.auth.UserData
import io.sevenb.terminal.data.model.broker_api.RequestTokenResponse
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.enum_model.ErrorMessageType
import io.sevenb.terminal.data.model.enums.GeeTestResult
import io.sevenb.terminal.data.model.enums.auth.NewAuthStates
import io.sevenb.terminal.data.model.enums.auth.RegistrationStates
import io.sevenb.terminal.device.utils.DateTimeUtil
import io.sevenb.terminal.domain.usecase.auth.*
import io.sevenb.terminal.domain.usecase.safety.AESCrypt
import io.sevenb.terminal.domain.usecase.validate.CheckCredentials
import io.sevenb.terminal.utils.Logger
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.io.File
import java.io.IOException
import javax.inject.Inject

class RegistrationViewModel @Inject constructor(
    private val creatingPassword: CreatingPasswordCheck,
    private val checkCredentials: CheckCredentials,
    private val pRequestVerificationCode: RequestVerificationCode,
    private val parseErrorMessage: ParseErrorMessage,
    private val startResendTimer: StartResendTimer,
    private val resendConfirmationCode: ResendConfirmationCode,
    private val sendConfirmationCode: SendConfirmationCode,
    private val localUserDataStoring: LocalUserDataStoring,
    private val storePassword: StorePassword,
    private val logOutUser: LogOutUser,
    private val aesCrypt: AESCrypt,
    private val signInUser: SignInUser,

    ) : ViewModel() {

    val geeTestResult = MutableLiveData<GeeTestResult>()
    val showResendButton = MutableLiveData<Boolean>()
    val userExistsResult = MutableLiveData<Boolean>()
    val errorMessage = MutableLiveData<ErrorMessageType>()
    val errorString = MutableLiveData<String>()

    //    val authState = MutableLiveData<NewAuthStates>()
//    val creatingBiometricCipher = MutableLiveData<Cipher?>()
    val timerValue = MutableLiveData<String>()

    //    val showSmsCode = MutableLiveData<Boolean>()
    val currentState = MutableLiveData<RegistrationStates>()
    val allLocalAccountDataRemoved = MutableLiveData<Unit>()
    val popupLocalAccountExists = MutableLiveData<Boolean>()
    val tfaRequired = MutableLiveData<Boolean>()
    val showLoader = MutableLiveData<String>()

    private lateinit var registrationToken: RequestTokenResponse

    var userData = UserData()
    var localAuthState = NewAuthStates.REGISTRATION

    var emailFromStorage = ""

    fun checkPassword(password: String) =
        creatingPassword.checkPassword(password)

    fun validateFields(email: String, password: String) =
        checkCredentials.checkCredentials(email, password)

    private fun requestRegistrationCode() {
        viewModelScope.launch {
            startResendTimer.execute(this)

            when (val result = pRequestVerificationCode.execute(userData)) {
                is Result.Success -> {
                    geeTestResult.value = GeeTestResult.SUCCESS
                    registrationToken = result.data
                }
                is Result.Error -> {
                    Logger.sendLog(
                        "RegistrationViewModel",
                        "requestRegistrationCode",
                        result.exception
                    )
                    result.exception.message?.let {
                        when {
                            it.contains("user_exists") -> {
                                geeTestResult.value = GeeTestResult.USER_EXISTS
                                errorMessage.value = ErrorMessageType.USER_ALREADY_EXISTS
                                userExistsResult.value = true
                            }
                            it.contains("bad_params") -> {
                                geeTestResult.value = GeeTestResult.BAD_PARAMS
                                errorMessage.value = ErrorMessageType.CAPTCHA_BAD_PARAMS
//                            val errorMessage = parseErrorMessage.execute(result.message)
//                            errorString.value = errorMessage?.error
                            }
                            it.contains("unconfirmed_user") -> {
                                geeTestResult.value = GeeTestResult.USER_EXISTS
                                errorMessage.value = ErrorMessageType.USER_ALREADY_EXISTS
                                userExistsResult.value = true
                            }
                            else -> {
                                geeTestResult.value = GeeTestResult.FAIL
                                errorMessage.value = ErrorMessageType.CAPTCHA_TOKEN
                            }
                        }
                    }
                }
            }
        }
    }

    fun requestOrResendRegistrationCode(userCaptcha: UserCaptcha? = null) {
        if (localAuthState == NewAuthStates.CONTINUES_REGISTRATION) {
            resendRegistrationCode()
        } else {
            userData.captcha = userCaptcha
            requestRegistrationCode()
        }
    }

    fun setNewState(state: NewAuthStates) {
        localAuthState = state
//        authState.value = state
    }

    fun resendRegistrationCode() {
        viewModelScope.launch {
            when (val result =
                resendConfirmationCode.execute(registrationToken.token, userData.email)) {
                is Result.Success -> {
                    registrationToken = result.data
                    startResendTimer.execute(this)
                    showResendButton.value = false
                }
                is Result.Error -> {
                    when {
                        result.message.contains("The limit for resending confirmation code for this token has") -> {
                            setNewState(NewAuthStates.REGISTRATION)
                            geeTestResult.value = GeeTestResult.TOO_MANY_ATTEMPTS
                            errorMessage.value = ErrorMessageType.CAPTCHA_TOO_MANY_ATTEMPTS
//                            errorString.value = "Too many attempts"
                        }
                        else -> {
                            geeTestResult.value = GeeTestResult.RESEND_FAIL
                            errorMessage.value = ErrorMessageType.CAPTCHA_RESEND
//                        errorString.value = "Server error: code 4"
                        }
                    }
                }
            }
        }
    }

    fun confirmCodeRegistration(confirmationCode: String) {
        Logger.sendLog(
            "RegistrationViewModel",
            "confirmCodeRegistration",
        )
        viewModelScope.launch {
            when (sendConfirmationCode.execute(registrationToken.token, confirmationCode)) {
                is Result.Success -> {
                    signInUser()
                }
                is Result.Error -> {
                    Logger.sendLog(
                        "RegistrationViewModel",
                        "confirmCodeRegistration",
                        IOException("Incorrect code")
                    )
                    errorString.value = "Incorrect code"
                }
            }
        }
    }

    fun signInUser() {
        showLoader.value = "Processing verification code..."
        viewModelScope.launch {
            delay(2 * 1000L)
            when (val signInResult = signInUser.execute(userData.email, userData.password)) {
                is Result.Success -> {
                    localUserDataStoring.apply {
                        updateLocalAccountData(userData.email)
                        val encryptedPassword = aesCrypt.encrypt(userData.password)
                        encryptedPassword?.let { storePassword(it) }
                    }
//                    localUserDataStoring.updateLocalAccountData(userData.email)
//                    aesCrypt.encrypt(userData.password)
                    createTfa()
                }
                is Result.Error -> {
                    signInResult.exception.message?.let {
                        when {
                            it.contains("INVALID_REQUEST_PARAMS") -> {
                                errorMessage.value = ErrorMessageType.LOGIN_ERROR
                            }
                            else -> errorString.value = "Server error: code 0"
                        }
                    }
                }
            }
        }
    }

    private fun createTfa() {
        tfaRequired.value = true
    }

//    fun createFingerprint() {
//        val cipherPair = storePassword.provideEncryptionCipher()
//
//        if (cipherPair.first != null && !storePassword.isBiometricSuccess()) {
//            errorString.value = cipherPair.first
//        }
//        creatingBiometricCipher.value = cipherPair.second
//    }

    fun subscribeUI() {
        startResendTimer.cancelTimerJob()
        startResendTimer.timerJob = viewModelScope.launch {
            startResendTimer.timerValueFlow.collect {
                timerValue.value = DateTimeUtil.formatSeconds(it.toLong())
                if (it == 0) showResendButton.value = true
            }
        }
    }

    fun setNewRegistrationToken(token: String) {
        registrationToken = RequestTokenResponse(token)
    }

    fun changeCurrentState(state: RegistrationStates) {
        currentState.value = state
    }

    fun isItAnotherAccount(signInEmail: String): Boolean {
        if (emailFromStorage.isEmpty()) return false

        return if (emailFromStorage != signInEmail) {
            popupLocalAccountExists.value = true
            true
        } else false
    }

    fun clearAllLocalAccountData(avatarFilePath: String) {
        viewModelScope.launch {
//            val logOutUserResult = logOutUser.execute()
            logOutUser.execute() //TODO CHECK IF NECESSARY
            val localDataResult = localUserDataStoring.deleteAllLocalUserData()

            val avatarFile: File? = File(avatarFilePath)
            avatarFile?.delete()

            emailFromStorage = ""

//            if (logOutUserResult is Result.Success && localDataResult is Result.Success) {
            if (localDataResult is Result.Success) {
                allLocalAccountDataRemoved.value = Unit
            } else {
                errorString.value = "Clear data error: 0"
            }
        }
    }


}