package io.sevenb.terminal.ui.screens.auth.change_password

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import io.sevenb.terminal.data.model.auth.UserCaptcha
import io.sevenb.terminal.data.model.auth.UserData
import io.sevenb.terminal.data.model.broker_api.RequestTokenResponse
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.enum_model.ErrorMessageType
import io.sevenb.terminal.data.model.enums.AuthState
import io.sevenb.terminal.data.model.enums.GeeTestResult
import io.sevenb.terminal.data.repository.auth.UserAuthRepository
import io.sevenb.terminal.device.utils.DateTimeUtil
import io.sevenb.terminal.domain.usecase.auth.*
import io.sevenb.terminal.domain.usecase.safety.AESCrypt
import io.sevenb.terminal.exceptions.Try
import io.sevenb.terminal.utils.ConnectionStateMonitor
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.json.JSONException
import org.json.JSONObject
import timber.log.Timber
import javax.crypto.Cipher
import javax.inject.Inject

class ChangePasswordViewModel @Inject constructor(
    private val pRequestVerificationCode: RequestVerificationCode,
    private val sendConfirmationCode: SendConfirmationCode,
    private val storeRefreshToken: StoreRefreshToken,
    private val validateEmailPassword: ValidateEmailPassword,
    private val startResendTimer: StartResendTimer,
    private val parseErrorMessage: ParseErrorMessage,

    private val localUserDataStoring: LocalUserDataStoring,
    private val setNewPassword: SetNewPassword,
//    private val storePassword: StorePassword,

    private val captchaDataInterop: CaptchaDataInterop,
    private val signInUser: SignInUser,
    private val resendConfirmationCode: ResendConfirmationCode,
    private val creatingPasswordCheck: CreatingPasswordCheck,
    private val storeFirstLaunch: StoreFirstLaunch,
    private val aesCrypt: AESCrypt,
) : ViewModel() {

    val errorString = MutableLiveData<String>()
    val errorMessage = MutableLiveData<ErrorMessageType>()

    val state = MutableLiveData<AuthState>().apply { value = AuthState.NEW }
    val showCaptcha = MutableLiveData<JSONObject?>()
    val geeTestResult = MutableLiveData<GeeTestResult>()
    val creatingBiometricCipher = MutableLiveData<Cipher?>()
    val showResendButton = MutableLiveData<Boolean>()
    val timerValue = MutableLiveData<String>()

    var userData = UserData()
    private lateinit var requestToken: RequestTokenResponse
    private lateinit var restorePasswordToken: RequestTokenResponse
    private lateinit var restoreConfirmedToken: String

    private var showTimeError = 0L

    val finishListener = MutableLiveData<Unit>()
    val createPassCode = MutableLiveData<Boolean>()

//    var userEmail: String? = null

    init {
        //TODO PASSWORD NOT REQUIRED
        runBlocking {
            val localAccountData = localUserDataStoring.restoreLocalAccountData()
//            val passRes = localUserDataStoring.restorePassword()

            if (localAccountData is Result.Success) {
//            if (localAccountData is Result.Success && passRes is Result.Success) {
//                userData = UserData(
//                    email = localAccountData.data.email,
//                    password = aesCrypt.decrypt(passRes.data)
//                )
                userData =
                    UserData(email = localAccountData.data.email)
            }
        }

    }

    fun validatePassword(password: String) {
        if (!validateEmailPassword.validatePassword(password)) {
            errorMessage.value = ErrorMessageType.PASSWORD_INCORRECT
            return
        }

        userData.email.let {
            userData.password = password
            userData.email = it
            createNewPassword()
        }
    }

    private fun createNewPassword() {
        viewModelScope.launch {
            if (::restoreConfirmedToken.isInitialized)
                when (val setNewPassword =
                    setNewPassword.execute(
                        userData.email,
                        userData.password,
                        restoreConfirmedToken
                    )) {
                    is Result.Success -> {
                        if (setNewPassword.data.result) {
                            createPassCode.value = true
                        } else {
                            errorString.value = "Server error: code 0"
                        }
                    }
                    is Result.Error -> {
                        when {
                            !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                            else -> errorString.value = "Server error: code 1"
                        }
                    }
                }
        }
    }

    private fun showNoInternetConnection() {
        val currentTime = System.currentTimeMillis()
        if (currentTime - showTimeError >= 1500) {
            errorString.value = ConnectionStateMonitor.noInternetError
            showTimeError = currentTime
        }
    }

    fun captcha(param: Int, result: String) {
        if (param == 0) {
            viewModelScope.launch {
                when (val resultJsonObject = captchaDataInterop.getCaptchaData()) {
                    is Result.Success -> {
                        when (val jsonObject: Result<JSONObject> =
                            Try { JSONObject(Gson().toJson(resultJsonObject.data)) }
                        ) {
                            is Result.Success -> {
                                showCaptcha.value = jsonObject.data
                            }
                            is Result.Error -> {
                                showCaptcha.value = JSONObject("{}")
                                errorMessage.value = ErrorMessageType.CAPTCHA_GET_PARSE_JSON
                            }
                        }
                    }
                    is Result.Error -> {
                        when {
                            !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                            else -> errorString.value = "Server error: code 3"
                        }
                    }
                }
            }
        } else if (param == 1) {
            viewModelScope.launch {
                try {
                    val jsonObject = JSONObject(result)
                    val challenge = jsonObject.getString(CaptchaDataInterop.GEETEST_CHALLENGE)
                    val seccode = jsonObject.getString(CaptchaDataInterop.GEETEST_SECCODE)
                    val validate = jsonObject.getString(CaptchaDataInterop.GEETEST_VALIDATE)

                    userData.captcha = UserCaptcha(challenge, seccode, validate)
                    geeTestResult.value = GeeTestResult.SUCCESS
                    smsCodeRestorePassword()
                } catch (e: JSONException) {
                    errorString.value = e.message
                    Timber.e("postCaptchaData: error - ${e.message}")
                }
            }
        }
    }

    fun smsCodeRestorePassword() {
        viewModelScope.launch {
            startResendTimer.execute(this)
            showResendButton.value = false
            userData.email?.let {
                when (val result = pRequestVerificationCode.executeRestorePassword(it)) {
                    is Result.Success -> {
                        restorePasswordToken = result.data
                        if (state.value != AuthState.SMS_CODE)
                            state.value = AuthState.SMS_CODE
                    }
                    is Result.Error -> {
                        when {
                            !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                            result.message.contains("user_exists") -> {
                                geeTestResult.value = GeeTestResult.USER_EXISTS
                                errorMessage.value = ErrorMessageType.USER_ALREADY_EXISTS
                            }
                            result.message.contains("bad_params") -> {
                                geeTestResult.value = GeeTestResult.USER_EXISTS
                                val errorMessage = parseErrorMessage.execute(result.message)
                                errorString.value = errorMessage?.message
                            }
                            else -> {
                                geeTestResult.value = GeeTestResult.FAIL
                                errorString.value = "Server error: code 2"
                            }
                        }
                    }
                }
            }
        }
    }

    fun restoreConfirmationCode(confirmationCode: String) {
        viewModelScope.launch {
            userData.email?.let {
                when (val responseToken = sendConfirmationCode.executeVerifyCode(
                    it, confirmationCode, restorePasswordToken.token
                )) {
                    is Result.Success -> {
                        localUserDataStoring.updateLocalAccountData()
                        restoreConfirmedToken = responseToken.data.token
                        state.value = AuthState.CREATING_PASSWORD
                    }
                    is Result.Error -> {
                        when {
                            !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                            else -> errorString.value = "Incorrect confirmation code"
                        }
                    }
                }
            }
        }
    }

    fun subscribeUI() {
        cancelTimerJob()
        startResendTimer.timerJob = viewModelScope.launch {
            startResendTimer.timerValueFlow.collect {
                timerValue.value = DateTimeUtil.formatSeconds(it.toLong())
                if (it == 0) showResendButton.value = true
            }
        }
    }

    fun resendConfirmationCode() {
        viewModelScope.launch {
            userData.email?.let {
                when (val result =
                    resendConfirmationCode.execute(restorePasswordToken.token, it)) {
                    is Result.Success -> {
                        requestToken = result.data
                        startResendTimer.execute(this)
                        showResendButton.value = false
                    }
                    is Result.Error -> {
                        when {
                            !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                            else -> errorString.value = "Server error: code 4"
                        }
                    }
                }
            }
        }
    }

//    fun signInUser(email: String, password: String) {
//        viewModelScope.launch {
//            if (email.isEmpty() || password.isEmpty()) {
//                errorMessage.value = ErrorMessageType.EMPTY_FIELDS
//                return@launch
//            }
//
//            if (!validateEmailPassword.validateEmail(email)) {
//                errorMessage.value = ErrorMessageType.EMAIL_INCORRECT
//                return@launch
//            }
//
//            if (!validateEmailPassword.validatePassword(password)) {
//                errorMessage.value = ErrorMessageType.PASSWORD_INCORRECT
//                return@launch
//            }
//
//            when (val signInResult = signInUser.execute(email, password)) {
//                is Result.Success -> {
//                    UserAuthRepository.accessToken = signInResult.data.accessToken
//                    UserAuthRepository.refreshToken = signInResult.data.refreshToken
//
//                    localUserDataStoring.updateLocalAccountData(email)
//                    storeRefreshToken(signInResult.data.refreshToken)
//                }
//                is Result.Error -> {
//                    when {
//                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
//                        signInResult.message.contains("Incorrect login params") -> {
//                            errorMessage.value = ErrorMessageType.LOGIN_ERROR
//                        }
//                        signInResult.message.contains("unconfirmed_user") -> {
//                            val errorMessage = parseErrorMessage.exec(signInResult.message)
//                            errorMessage?.extra?.let {
//                                requestToken = RequestTokenResponse(it.accessToken)
//                            }
//                            this@ChangePasswordViewModel.errorMessage.value = ErrorMessageType.CONFIRMATION_REQUIRED
//                        }
//                        else -> errorString.value = "Server error: code 0"
//                    }
//                }
//            }
//        }
//    }

    fun checkPassword(password: String) = creatingPasswordCheck.checkPassword(password)

    private fun storeRefreshToken(refreshToken: String) {
        viewModelScope.launch {
            when (val storeResult = storeRefreshToken.execute(refreshToken)) {
                is Result.Success -> {
                    finishListener.value = Unit
                }
                is Result.Error -> {
                    when {
                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                        else -> errorString.value = storeResult.exception.message
                    }
                }
            }
        }
    }

    private fun cancelTimerJob() = startResendTimer.cancelTimerJob()


}