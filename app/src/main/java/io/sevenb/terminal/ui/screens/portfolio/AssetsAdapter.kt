package io.sevenb.terminal.ui.screens.portfolio

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import io.sevenb.terminal.R

internal class AssetsAdapter : RecyclerView.Adapter<AssetItemViewHolder>() {

    private var baseItemsList: MutableList<BaseAssetListItem> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AssetItemViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_asset_list, parent, false)
        return AssetItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: AssetItemViewHolder, position: Int) {
        val item = baseItemsList[position]
        holder.bind(item)
    }

//    fun submitList(list: List<AssetItem>) {
//        baseItemsList = list.toMutableList()
//        notifyDataSetChanged()
//    }

    fun listIndexOf(item: BaseAssetListItem): Int {
        return baseItemsList.indexOf(item)
    }

    override fun getItemCount(): Int = baseItemsList.size

}
