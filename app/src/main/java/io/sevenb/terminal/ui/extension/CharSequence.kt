package io.sevenb.terminal.ui.extension

fun CharSequence?.getFloatValue() = this?.let { it.toString().toFloatOrNull() ?: 1f } ?: 1f

fun CharSequence?.getDoubleValue() = this?.let { it.toString().toDoubleOrNull() ?: 1.0 } ?: 1.0

fun CharSequence?.getPlainDecimalString(isFocused: Boolean = false) = this?.let {
    it.toString().toFloatOrNull()?.let { floatValue ->
        val a = floatValue.toInt().toBigDecimal()
        val b = floatValue.toBigDecimal()
        val isInteger = (a - b) == (0.0.toBigDecimal())

        var formattedString = ""
        if (!isInteger) {
            val plainString = floatValue.toBigDecimal().toPlainString().reversed()
            var lastNumPosition = 0
            var firstCheckNumber = true


            for ((index, char) in plainString.withIndex())
                if (char != '0' && firstCheckNumber) {
                    lastNumPosition = index
                    firstCheckNumber = false
                }

            for ((i, char) in plainString.withIndex()) {
                if (i >= lastNumPosition) formattedString += char
            }

            if (lastNumPosition == 0) formattedString = plainString

            try {
                while (formattedString.first() == '0' || formattedString.first() == '.') {
                    formattedString = formattedString.substring(1, formattedString.length)
                }
            } catch (e: NoSuchElementException) {
                e.printStackTrace()
            }
        } else if (floatValue == 0.0f) {
            formattedString = it.toString().reversed()
        } else formattedString = a.toString().reversed()

        if (isFocused) it.toString() else formattedString.reversed()
    }
}
