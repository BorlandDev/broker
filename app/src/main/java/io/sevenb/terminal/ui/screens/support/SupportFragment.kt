package io.sevenb.terminal.ui.screens.support

import android.os.Bundle
import android.view.View
import io.sevenb.terminal.R
import io.sevenb.terminal.ui.base.BaseFragment
import io.sevenb.terminal.ui.extension.hideKeyboard
import timber.log.Timber

class SupportFragment : BaseFragment() {

//    private lateinit var aboutViewModel: AboutViewModel
//    private val aboutAdapter = AboutAdapter()

    override fun layoutId() = R.layout.fragment_support

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        aboutViewModel = viewModelProvider(viewModelFactory)
        initView()
        subscribeUi()
    }

    private fun initView() {

    }

    override fun onSearchTapListener() {
        Timber.d("onSearchTapListener ")
    }

    private fun subscribeUi() {

    }

    override fun onPause() {
        super.onPause()
        hideKeyboard(requireActivity())
    }

    override fun onDestroyView() {
        super.onDestroyView()
        hideKeyboard(requireActivity())
    }
}