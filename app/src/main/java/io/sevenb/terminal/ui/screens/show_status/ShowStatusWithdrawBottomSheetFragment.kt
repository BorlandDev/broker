package io.sevenb.terminal.ui.screens.show_status

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.util.Log
import android.view.View
import androidx.constraintlayout.motion.widget.MotionLayout
import com.google.android.material.snackbar.Snackbar
import io.sevenb.terminal.R
import io.sevenb.terminal.device.utils.DateTimeUtil
import io.sevenb.terminal.domain.usecase.charts.AccountData
import io.sevenb.terminal.ui.base.BaseBottomSheetFragment
import io.sevenb.terminal.ui.screens.order.OrderViewModel
import io.sevenb.terminal.ui.screens.portfolio.PortfolioHistoryItem
import kotlinx.android.synthetic.main.fragment_deposit_main.*
import kotlinx.android.synthetic.main.fragment_show_status_withdraw.*
import kotlinx.android.synthetic.main.fragment_show_status_withdraw.close_screen
import kotlinx.android.synthetic.main.fragment_show_status_withdraw.first_amount
import kotlinx.android.synthetic.main.fragment_show_status_withdraw.first_amount_ticker
import kotlinx.android.synthetic.main.fragment_show_status_withdraw.iv_success
import kotlinx.android.synthetic.main.fragment_show_status_withdraw.market_date_created
import kotlinx.android.synthetic.main.fragment_show_status_withdraw.second_amount
import kotlinx.android.synthetic.main.fragment_show_status_withdraw.success_title

class ShowStatusWithdrawBottomSheetFragment : BaseBottomSheetFragment() {
    override fun layoutId() = R.layout.fragment_show_status_withdraw
    override fun containerId() = R.id.container_show_status_withdraw

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val data = arguments?.getParcelable<PortfolioHistoryItem>("content")
        Log.e("!!!", "data - $data")

        success_title.text = data?.status

        when(data?.symbol){
            "ETH" -> {
                val content = SpannableString("https://eth-explorer.nownodes.io/")
                content.setSpan(UnderlineSpan(), 0, content.length, 0)
                txid_content.text = content
                txid_content.setOnClickListener {
                    val browserIntent =
                        Intent(Intent.ACTION_VIEW, Uri.parse("https://eth-explorer.nownodes.io/"))
                    startActivity(browserIntent)
                }
            }
            "BTC" -> {
                val content = SpannableString("http://btc-explorer.nownodes.io/")
                content.setSpan(UnderlineSpan(), 0, content.length, 0)
                txid_content.text = content
                txid_content.setOnClickListener {
                    val browserIntent =
                        Intent(Intent.ACTION_VIEW, Uri.parse("http://btc-explorer.nownodes.io/"))
                    startActivity(browserIntent)
                }
            }
            "DASH" -> {
                val content = SpannableString("https://dash-explorer.nownodes.io/")
                content.setSpan(UnderlineSpan(), 0, content.length, 0)
                txid_content.text = content
                txid_content.setOnClickListener {
                    val browserIntent =
                        Intent(Intent.ACTION_VIEW, Uri.parse("https://dash-explorer.nownodes.io/"))
                    startActivity(browserIntent)
                }
            }
            "DOGE" -> {
                val content = SpannableString("https://doge-explorer.nownodes.io/")
                content.setSpan(UnderlineSpan(), 0, content.length, 0)
                txid_content.text = content
                txid_content.setOnClickListener {
                    val browserIntent =
                        Intent(Intent.ACTION_VIEW, Uri.parse("https://doge-explorer.nownodes.io/"))
                    startActivity(browserIntent)
                }
            }
            "DGB" -> {
                val content = SpannableString("https://dgb-explorer.nownodes.io/")
                content.setSpan(UnderlineSpan(), 0, content.length, 0)
                txid_content.text = content
                txid_content.setOnClickListener {
                    val browserIntent =
                        Intent(Intent.ACTION_VIEW, Uri.parse("https://dgb-explorer.nownodes.io/"))
                    startActivity(browserIntent)
                }
            }
            "GRS" -> {
                val content = SpannableString("https://grs-explorer.nownodes.io/")
                content.setSpan(UnderlineSpan(), 0, content.length, 0)
                txid_content.text = content
                txid_content.setOnClickListener {
                    val browserIntent =
                        Intent(Intent.ACTION_VIEW, Uri.parse("https://grs-explorer.nownodes.io/"))
                    startActivity(browserIntent)
                }
            }
            "BCH" -> {
                val content = SpannableString("https://bch-explorer.nownodes.io/")
                content.setSpan(UnderlineSpan(), 0, content.length, 0)
                txid_content.text = content
                txid_content.setOnClickListener {
                    val browserIntent =
                        Intent(Intent.ACTION_VIEW, Uri.parse("https://bch-explorer.nownodes.io/"))
                    startActivity(browserIntent)
                }
            }
            "LTC" -> {
                val content = SpannableString("https://ltc-explorer.nownodes.io/")
                content.setSpan(UnderlineSpan(), 0, content.length, 0)
                txid_content.text = content
                txid_content.setOnClickListener {
                    val browserIntent =
                        Intent(Intent.ACTION_VIEW, Uri.parse("https://ltc-explorer.nownodes.io/"))
                    startActivity(browserIntent)
                }
            }
        }

        when(data?.status.toString()) {
            "Not confirmed" -> {
                iv_success.setImageResource(R.drawable.ic_failed)
            }
            "Pending" -> {
                iv_success.setImageResource(R.drawable.ic_in_proccess)

            }
            "Processing" -> {
                iv_success.setImageResource(R.drawable.ic_in_proccess)


            }
            "Finished" -> {
                iv_success.setImageResource(R.drawable.ic_success_blue)

            }
            "Failed" -> {
                iv_success.setImageResource(R.drawable.ic_failed)

            }
            "Refunded" -> {
                iv_success.setImageResource(R.drawable.ic_in_proccess)

            }
            else -> {
                iv_success.setImageResource(R.drawable.ic_in_proccess)
                success_title.text = "Processing"
            }
        }


        val estimatedString =
            "${AccountData.getBaseCurrencyCharacter()}${
                data?.estimatedInBase?.let { OrderViewModel.roundDouble(it.toDouble(), 2, true) }
            }"
        second_amount.text = estimatedString

        first_amount.text = data?.amount
        first_amount_ticker.text = data?.symbol

        val timestamp = DateTimeUtil.parseToTimestamp(data?.createdAt ?: "")
        val date = DateTimeUtil.dateFromTimestamp(timestamp)
        val time = DateTimeUtil.timeFromTimestamp(timestamp)
        market_date_created.text = "Created at %s %s".format(date, time)

        inner_container_withdraw.setTransitionListener(object :
            MotionLayout.TransitionListener {
            override fun onTransitionStarted(p0: MotionLayout?, p1: Int, p2: Int) {}

            override fun onTransitionChange(p0: MotionLayout?, p1: Int, p2: Int, p3: Float) {}

            override fun onTransitionCompleted(p0: MotionLayout?, p1: Int) {
                dismiss()
            }

            override fun onTransitionTrigger(p0: MotionLayout?, p1: Int, p2: Boolean, p3: Float) {}
        })
        if(data!!.address!!.length > 30) {
            try {
                sent_to_content.text = "${data!!.address!!.subSequence(0, 11)}..${data!!.address!!.subSequence(data!!.id!!.toString().length - 14, data!!.id!!.toString().length - 1)}"
            } catch (e: Exception) {
                sent_to_content.text = "-"
            }
        } else {
            sent_to_content.text = data.address
        }
        try {
            val content = SpannableString("${data!!.id!!.subSequence(0, 11)}..${data!!.id!!.subSequence(data!!.id!!.toString().length - 14, data!!.id!!.toString().length - 1)}")
            content.setSpan(UnderlineSpan(), 0, content.length, 0)
            txid_content.text = content
        } catch (e: Exception) {
//            txid_content.text = "-"
        }
        ivCopy.setOnClickListener {
            val clipboard: ClipboardManager =
                requireActivity().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clip: ClipData = ClipData.newPlainText("Send to", data?.address)
            clipboard.setPrimaryClip(clip)

            showSnackbar()
        }

        ivCopy_txid.setOnClickListener {
            val clipboard: ClipboardManager =
                requireActivity().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clip: ClipData = ClipData.newPlainText("txid", data?.id)
            clipboard.setPrimaryClip(clip)

            showSnackbar()
        }

        close_screen.setOnClickListener {
            dismiss()
        }
    }

    private fun formatDate(createdAt: String): String {
        val timestamp = DateTimeUtil.parseToTimestamp(createdAt)
        val date = DateTimeUtil.dateFromTimestamp(timestamp)
        val dateArray = date.split(" ")
        return "${dateArray[1]} ${dateArray[0]}, ${dateArray[2]}"
    }

    private fun showSnackbar() {
        Snackbar.make(dialog!!.window!!.decorView, "Copy to clipboard", Snackbar.LENGTH_SHORT).show()
    }
}