package io.sevenb.terminal.ui.screens.create_fingerprint

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.sevenb.terminal.R
import io.sevenb.terminal.data.model.auth.UserData
import io.sevenb.terminal.data.model.broker_api.account.AccountInfo
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.enum_model.ErrorMessageType
import io.sevenb.terminal.data.model.portfolio.LocalAccountData
import io.sevenb.terminal.data.repository.auth.UserAuthRepository
import io.sevenb.terminal.domain.usecase.auth.LocalUserDataStoring
import io.sevenb.terminal.domain.usecase.auth.SignInUser
import io.sevenb.terminal.domain.usecase.auth.StorePassword
import io.sevenb.terminal.domain.usecase.auth.ValidateEmailPassword
import io.sevenb.terminal.domain.usecase.firebase_auth.FirebaseAuthHandler
import io.sevenb.terminal.domain.usecase.verification.VerificationData
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.crypto.Cipher
import javax.inject.Inject

class CreateFingerprintViewModel @Inject constructor(
    private val verificationData: VerificationData,
    private val signInUser: SignInUser,
    private val storePassword: StorePassword,
    private val validateEmailPassword: ValidateEmailPassword,
    private val localUserDataStoring: LocalUserDataStoring,
    private val firebaseAuthHandler: FirebaseAuthHandler
) : ViewModel() {

    val fingerPrintCreated = MutableLiveData<Boolean>()
    val createFingerCipher = MutableLiveData<Cipher>()
    val snackbarMessage = MutableLiveData<ErrorMessageType>()
    val customMessage = MutableLiveData<String>()
    val passwordExist = MutableLiveData<Unit>()

    val userData = UserData()

    val accountInfo = MutableLiveData<AccountInfo>()
    val localAccountDataResult = MutableLiveData<LocalAccountData>()

    init {
        restoreEncryptedPassword()
    }

    fun accountInfo() {
        viewModelScope.launch {
            when (val accountInfoResult = verificationData.accountInfo()) {
                is Result.Success -> {
                    val status = accountInfoResult.data
                    accountInfo.value = status
                }
                is Result.Error -> {
                    restoreLocalAccountData()
                    Timber.e("getting accountInfo error")
                }
            }
        }
    }

    private fun restoreLocalAccountData() {
        viewModelScope.launch {
            when (val accountResult = localUserDataStoring.restoreLocalAccountData()) {
                is Result.Success -> {
                    val accountData = accountResult.data
                    localAccountDataResult.value = accountData
                }
                is Result.Error -> Timber.e("localUserDataStoring.restoreLocalAccountData() error=${accountResult.exception}")
            }
        }
    }

    private fun restoreEncryptedPassword() {
        viewModelScope.launch {
            when (val createdPassword = storePassword.restoreEncryptedPassword()) {
                is Result.Success -> {
                    if (createdPassword.data.isNotEmpty())
                        passwordExist.value = Unit
                }
            }
        }
    }

//    fun signInUser(email: String, password: String) {
//        viewModelScope.launch {
//            if (email.isEmpty() || password.isEmpty()) {
//                snackbarMessage.value = ErrorMessageType.EMPTY_FIELDS
//                return@launch
//            }
//
//            if (!validateEmailPassword.validateEmail(email)) {
//                snackbarMessage.value = ErrorMessageType.EMAIL_INCORRECT
//                return@launch
//            }
//
//            if (!validateEmailPassword.validatePassword(password)) {
//                snackbarMessage.value = ErrorMessageType.PASSWORD_INCORRECT
//                return@launch
//            }
//
//            when (val signInResult = signInUser.execute(email, password)) {
//                is Result.Success -> {
//                    UserAuthRepository.accessToken = signInResult.data.accessToken
//                    UserAuthRepository.refreshToken = signInResult.data.refreshToken
//
////                    when (firebaseAuthHandler.checkIsLoggedIn()) {
////                        is Result.Success -> Unit
////                        is Result.Error -> firebaseAuthHandler.signIn(email, password)
////                    }
//
//                    localUserDataStoring.updateLocalAccountData(email)
//
//                    userData.password = password
//
//                    createFingerprint()
//                }
//                is Result.Error -> snackbarMessage.value = ErrorMessageType.PASSWORD_INCORRECT_MESSAGE
//            }
//        }
//    }

    private fun createFingerprint() {
        val cipher = storePassword.provideEncryptionCipher()
        when {
            cipher.second != null -> {
                createFingerCipher.value = cipher.second
            }
            cipher.first != null -> {
                customMessage.value = cipher.first!!
            }
            else -> snackbarMessage.value = ErrorMessageType.NO_FINGERPRINT
        }
    }

    fun checkValidPassword(password: String) = validateEmailPassword.validatePassword(password)

    fun encryptPassword(cipher: Cipher?) {
        viewModelScope.launch {
            val encryptionResult = storePassword.encryptPassword(
                userData.password,
                cipher ?: return@launch
            )
            when (encryptionResult) {
                is Result.Success -> {
                    fingerPrintCreated.value = true
                    customMessage.value = "Fingerprint created"
                }
                is Result.Error -> customMessage.value = "Fingerprint creating error"
            }
        }
    }

}