package io.sevenb.terminal.ui.screens.trading

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import io.sevenb.terminal.R
import io.sevenb.terminal.databinding.ItemTradingListShimmerBinding

class TradingLoadListAdapter(var list: MutableList<Int>) : RecyclerView.Adapter<TradingLoadListAdapter.TradingLoadViewHolder>() {

    override fun getItemCount(): Int {
        return list.size
    }

    class TradingLoadViewHolder(binding: ItemTradingListShimmerBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val sflMarkets = binding.sflMarkets
        fun bind() {

        }
    }

    override fun onBindViewHolder(holder: TradingLoadViewHolder, position: Int) {
        holder.sflMarkets.startShimmerAnimation()
        holder.bind()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TradingLoadViewHolder {
        val binding: ItemTradingListShimmerBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_trading_list_shimmer,
            parent,
            false
        )

        return TradingLoadViewHolder(binding)
    }
}
