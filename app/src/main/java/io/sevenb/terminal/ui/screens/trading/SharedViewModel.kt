package io.sevenb.terminal.ui.screens.trading

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.sevenb.terminal.data.model.broker_api.rates.MarketRate
import io.sevenb.terminal.data.model.enum_model.HistoryType
import io.sevenb.terminal.ui.screens.order.OrderBottomSheetFragment
import io.sevenb.terminal.ui.screens.select_currency.BaseSelectItem
import javax.inject.Inject

class SharedViewModel @Inject constructor() : ViewModel() {

    val currentEmail = MutableLiveData<String>()
    val orderTicker = MutableLiveData<Pair<String, OrderBottomSheetFragment.Companion.OrderSide>>()
    val selectCurrencyList = MutableLiveData<List<BaseSelectItem>>()
    val selectCurrencyFiatList = MutableLiveData<List<BaseSelectItem>>()
    val selectCurrencyDepositList = MutableLiveData<List<BaseSelectItem>>()

    val confirmPhoneNumber = MutableLiveData<String>()

    val selectedCurrency = MutableLiveData<BaseSelectItem?>()
    val selectedCurrencyFiat = MutableLiveData<BaseSelectItem?>()
    val selectedCurrencyDeposit = MutableLiveData<BaseSelectItem?>()

    val historyFilter = MutableLiveData<MutableList<HistoryType>>()

    var updateAssets = false
    val updateAssetsAfterTransaction = MutableLiveData<Boolean>()

    val shouldUpdateBalance = MutableLiveData<Boolean?>()

    var ratesList = mutableMapOf<String, MarketRate?>()
}