package io.sevenb.terminal.ui.extension

import android.graphics.Color
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import io.sevenb.terminal.R

fun View.visible(value: Boolean) {
    this.isVisible = value
}

fun View.setTransparentBackground() {
    this.setBackgroundColor(Color.TRANSPARENT)
}

fun View.newBackground(backgroundId: Int?) {
    if (backgroundId == null) {
        setTransparentBackground()
        return
    }

    val context = this.context
    context?.let { this.background = ContextCompat.getDrawable(context, backgroundId) }
}

fun View.setErrorBackground() {
    try {
        this.newBackground(R.drawable.bg_red_stroke_eight)
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

fun View.setDefaultBackground() {
    try {
        this.newBackground(R.drawable.bg_white_stroke_eight)
    } catch (e: Exception) {
        e.printStackTrace()
    }
}