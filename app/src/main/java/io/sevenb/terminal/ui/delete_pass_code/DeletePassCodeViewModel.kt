package io.sevenb.terminal.ui.delete_pass_code

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.repository.auth.UserAuthRepository
import io.sevenb.terminal.domain.usecase.auth.LocalUserDataStoring
import io.sevenb.terminal.domain.usecase.auth.SignInUser
import io.sevenb.terminal.domain.usecase.auth.StoreRefreshToken
import io.sevenb.terminal.domain.usecase.firebase_auth.FirebaseAuthHandler
import io.sevenb.terminal.domain.usecase.safety.AccountSafetyStoring
import io.sevenb.terminal.domain.usecase.safety.PassCodeStoring
import io.sevenb.terminal.domain.usecase.validate.CheckCredentials
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import javax.inject.Inject

class DeletePassCodeViewModel @Inject constructor(
    private val checkCredentials: CheckCredentials,
    private val signInUser: SignInUser,
    private val localUserDataStoring: LocalUserDataStoring,
    private val storeRefreshToken: StoreRefreshToken,
    private val safetyStoring: AccountSafetyStoring,
    private val passCodeStoring: PassCodeStoring,
    private val firebaseAuthHandler: FirebaseAuthHandler
) : ViewModel() {

    val errorString = MutableLiveData<String>()
    val removingDone = MutableLiveData<Boolean>()
    val showProgress = MutableLiveData<Boolean>()

    fun validateFields(email: String, password: String) =
        checkCredentials.checkCredentials(email, password)

    fun signInUser(email: String, password: String) {
        viewModelScope.launch {
            when (val signInResult = signInUser.execute(email, password)) {
                is Result.Success -> {

//                    when (firebaseAuthHandler.checkIsLoggedIn()) {
//                        is Result.Success -> Unit
//                        is Result.Error -> firebaseAuthHandler.signIn(email, password)
//                    }

//                    UserAuthRepository.accessToken = signInResult.data.accessToken
//                    UserAuthRepository.refreshToken = signInResult.data.refreshToken

                    if (email.isNotEmpty()) localUserDataStoring.updateLocalAccountData(email)

                    processRemovingDone()
//                    storeRefreshToken(signInResult.data.refreshToken)
                }
                is Result.Error -> {
                    signInResult.exception.message?.let {
                        when {
                            it.contains("Incorrect login params") -> {
                                errorString.value = "Incorrect password"
                            }
                            else -> errorString.value = "Server error: code 0"
                        }
                    }
                }
            }
        }
    }

    fun showProgress(value: Boolean) {
        showProgress.value = value
    }

    private fun processRemovingDone() {
        viewModelScope.launch {
            val disablePassCodeResultAsync =
                async { safetyStoring.storePassCodeEnabled(false) }
            val disableFingerprintResultAsync =
                async { safetyStoring.storeFingerprintEnabled(false) }
            val removePassCodeResultAsync =
                async { passCodeStoring.storePassCode("") }
            val clearBiometryResultAsync =
                async { localUserDataStoring.clearBiometry() }

            localUserDataStoring.restoreLocalAccountData()

            removingDone.value =
                disablePassCodeResultAsync.await() is Result.Success &&
                        disableFingerprintResultAsync.await() is Result.Success &&
                        removePassCodeResultAsync.await() is Result.Success &&
                        clearBiometryResultAsync.await() is Result.Success
        }
    }
//    private fun storeRefreshToken(refreshToken: String) {
//        viewModelScope.launch {
//            when (val storeResult = storeRefreshToken.execute(refreshToken)) {
//                is Result.Success -> {
//                    val disablePassCodeResultAsync =
//                        async { safetyStoring.storePassCodeEnabled(false) }
//                    val disableFingerprintResultAsync =
//                        async { safetyStoring.storeFingerprintEnabled(false) }
//                    val removePassCodeResultAsync =
//                        async { passCodeStoring.storePassCode("") }
//                    val clearBiometryResultAsync =
//                        async { localUserDataStoring.clearBiometry() }
//
//                    localUserDataStoring.restoreLocalAccountData()
//
//                    removingDone.value =
//                        disablePassCodeResultAsync.await() is Result.Success &&
//                                disableFingerprintResultAsync.await() is Result.Success &&
//                                removePassCodeResultAsync.await() is Result.Success &&
//                                clearBiometryResultAsync.await() is Result.Success
//                }
//                is Result.Error -> {
//                    errorString.value = storeResult.exception.message
//                }
//            }
//        }
}
