package io.sevenb.terminal.ui.screens.auth.new_auth

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import io.sevenb.terminal.R
import io.sevenb.terminal.data.model.enum_model.ErrorMessageType
import io.sevenb.terminal.data.model.enums.*
import io.sevenb.terminal.data.model.enums.auth.NewAuthStates
import io.sevenb.terminal.data.model.enums.auth.PasswordRestoringStates
import io.sevenb.terminal.data.model.enums.auth.RegistrationStates
import io.sevenb.terminal.data.model.enums.auth.TfaState
import io.sevenb.terminal.data.model.enums.pass_code.PassCodeStates
import io.sevenb.terminal.domain.usecase.new_auth.HandleCaptcha
import io.sevenb.terminal.domain.usecase.new_auth.HandleFieldFocus
import io.sevenb.terminal.domain.usecase.new_auth.HandleFingerprint
import io.sevenb.terminal.ui.base.BaseFragment
import io.sevenb.terminal.ui.extension.*
import io.sevenb.terminal.ui.screens.auth.SharedAuthViewModel
import io.sevenb.terminal.ui.screens.auth.login_state.LoginFlow
import io.sevenb.terminal.ui.screens.auth.login_state.LoginViewModel
import io.sevenb.terminal.ui.screens.auth.password_resore_state.PasswordRestoringFlow
import io.sevenb.terminal.ui.screens.auth.password_resore_state.PasswordRestoringViewModel
import io.sevenb.terminal.ui.screens.auth.registration_state.RegistrationFlow
import io.sevenb.terminal.ui.screens.auth.registration_state.RegistrationViewModel
import io.sevenb.terminal.ui.screens.safety.SafetyViewModel
import io.sevenb.terminal.ui.screens.safety.passcode_state.PassCodeFlow
import io.sevenb.terminal.ui.screens.safety.passcode_state.PassCodeViewModel
import io.sevenb.terminal.ui.screens.safety.tfa.TfaFlow
import io.sevenb.terminal.ui.screens.safety.tfa.TfaViewModel
import kotlinx.android.synthetic.main.activity_app.*
import kotlinx.android.synthetic.main.fragment_auth_login.*
import kotlinx.android.synthetic.main.fragment_auth_new.*
import kotlinx.android.synthetic.main.fragment_auth_registration.*
import kotlinx.android.synthetic.main.fragment_auth_restore_password.*
import kotlinx.android.synthetic.main.fragment_auth_verification_code.*
import kotlinx.android.synthetic.main.fragment_change_password.*
import kotlinx.android.synthetic.main.fragment_loader.*
import kotlinx.android.synthetic.main.fragment_pass_code.*
import kotlinx.android.synthetic.main.fragment_progress.*
import kotlinx.android.synthetic.main.fragment_tfa.*
import org.json.JSONObject
import timber.log.Timber
import javax.inject.Inject

class NewAuthFragment : BaseFragment() {
    private lateinit var newAuthSharedViewModel: NewAuthSharedViewModel

    @Inject
    lateinit var handleCaptcha: HandleCaptcha

    @Inject
    lateinit var handleFingerprint: HandleFingerprint

    @Inject
    lateinit var loginFlow: LoginFlow
    private lateinit var loginViewModel: LoginViewModel

    @Inject
    lateinit var registrationFlow: RegistrationFlow
    private lateinit var registrationViewModel: RegistrationViewModel

    @Inject
    lateinit var passwordRestoringFlow: PasswordRestoringFlow
    private lateinit var passwordRestoringViewModel: PasswordRestoringViewModel

//    @Inject
//    lateinit var mPasswordChangeFragment: ChangePasswordFragment
//    private lateinit var passwordChangeViewModel: ChangePasswordViewModel

    @Inject
    lateinit var handleFieldFocus: HandleFieldFocus

    @Inject
    lateinit var passCodeFlow: PassCodeFlow
    private lateinit var passCodeViewModel: PassCodeViewModel

    @Inject
    lateinit var tfaFlow: TfaFlow
    private lateinit var tfaViewModel: TfaViewModel
    private lateinit var safetyViewModel: SafetyViewModel

    private val sharedAuthViewModel: SharedAuthViewModel by activityViewModels { viewModelFactory }

    override fun layoutId() = R.layout.fragment_auth_new

    override fun onSearchTapListener() {}

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        newAuthSharedViewModel = viewModelProvider(viewModelFactory)
        loginViewModel = viewModelProvider(viewModelFactory)
        registrationViewModel = viewModelProvider(viewModelFactory)
        passwordRestoringViewModel = viewModelProvider(viewModelFactory)
//        passwordChangeViewModel = viewModelProvider(viewModelFactory)
        passCodeViewModel = viewModelProvider(viewModelFactory)
        safetyViewModel = viewModelProvider(viewModelFactory)
        tfaViewModel = viewModelProvider(viewModelFactory)

        addObservers()
        initCaptcha()
        initFieldFocus()
        hideKeyboard(requireContext(), requireView())
        subscribeToSharedViewModel()
    }


    private fun initFieldFocus() {
        handleFieldFocus.init(container_auth)
    }

    private fun subscribeToSharedViewModel() {
        newAuthSharedViewModel.apply {
            newState.observe(viewLifecycleOwner, { processAuthState(it) })
            errorMessage.observe(viewLifecycleOwner, { processErrorMessage(it) })
            errorString.observe(viewLifecycleOwner, { processErrorString(it) })
            showProgress.observe(viewLifecycleOwner, { processProgressScreen(it) })
            focusField.observe(viewLifecycleOwner, { processFocusField(it) })
            startCaptchaFlow.observe(viewLifecycleOwner, { processStartCaptchaFlow() })
            geeTestResult.observe(viewLifecycleOwner, { processGeeTestResult(it) })
            focusSmsCodeField.observe(viewLifecycleOwner, { processFocusSmsCodeField() })
            showCaptcha.observe(viewLifecycleOwner, { processShowCaptcha(it) })
            verificationCodeVisibility.observe(viewLifecycleOwner, { processSmsCodeVisibility(it) })
            showLoader.observe(viewLifecycleOwner, { processShowLoader(it) })
            creatingPassCodeRequired.observe(viewLifecycleOwner, { processPassCodeCreation(it) })
//            creatingTfaRequired.observe(viewLifecycleOwner, { processTfaState() })
            closeCaptcha.observe(viewLifecycleOwner, { processCloseCaptcha(it) })
        }

        passCodeViewModel.apply {
            authDone.observe(
                viewLifecycleOwner, { newAuthSharedViewModel.setNewState(NewAuthStates.DONE_AUTH) })
            showLoader.observe(viewLifecycleOwner, { processShowLoader(Pair(true, it)) })
//            requestTfa.observe(viewLifecycleOwner, { processTfa(it) })
        }

        tfaViewModel.apply {
//            authDone.observe(
//                viewLifecycleOwner, { newAuthSharedViewModel.setNewState(NewAuthStates.DONE_AUTH) })
            showLoader.observe(viewLifecycleOwner, {// processShowLoader(Pair(true, it))
            })
//            requestTfa.observe(viewLifecycleOwner, { processTfa(it) })
        }

        sharedAuthViewModel.getGlobalStateFromActivity.observe(
            viewLifecycleOwner, { processStateFromActivity(it) })
    }

    private fun processCloseCaptcha(value: Boolean?) {
        value?.let { if (it) newAuthSharedViewModel.showProgress(false) }
    }

    private fun processStateFromActivity(state: CommonState?) {
        when (state) {
            is NewAuthStates -> newAuthSharedViewModel.setNewState(state)
            is RegistrationStates -> processRegistrationState(state)
            is PasswordRestoringStates -> processPasswordRestoreState(state)
        }
    }

    private fun processPassCodeCreation(userData: Pair<String, String>) {
        passCodeFlow.apply {
            initLifecycleOwner(viewLifecycleOwner, this@NewAuthFragment, handleFingerprint)
            initViews(
                containerPassCode,
                passCodeViewModel,
                safetyViewModel,
                PassCodeStates.CREATE_PASS_CODE,
                userData.first,
                userData.second
            )
        }
        setInvisibleViews(
            container_tfa,
            container_sign_in,
            container_sms_code,
            container_loader,
            container_registration,
            container_progress,
            container_restore_password
        )
        containerPassCode.visible(true)
    }

    private fun processTfaState(state: TfaState?) {
        tfaFlow.apply {
            initLifecycleOwner(viewLifecycleOwner, this@NewAuthFragment)
            initViews(
                container_tfa,
                tfaViewModel,
                newAuthSharedViewModel,
                null,
                handleFieldFocus,
                state,
            )
        }
        setInvisibleViews(
            container_sign_in,
            container_sms_code,
            container_loader,
            container_registration,
            container_progress,
            container_restore_password,
            containerPassCode,
        )
        container_tfa.visible(true)
    }

    private fun processShowLoader(pair: Pair<Boolean, String?>?) {
        pair?.let {
            if (it.first) {
                setInvisibleViews(
                    container_tfa,
                    container_sign_in,
                    container_sms_code,
                    container_registration,
                    container_progress,
                    container_restore_password,
                    containerPassCode
                )
                tv_message.text = it.second
            } else {
                tv_message.text = ""
            }
            container_loader.visible(it.first)
        }
    }

    private fun processSmsCodeVisibility(value: Boolean?) {
        value?.let {
            setGoneViews(containerPassCode)
            setViewsVisibility(value, container_sms_code)
        }
    }

    private fun processShowCaptcha(jsonObject: JSONObject?) {
        jsonObject?.let { handleCaptcha.showCaptcha(it) }
    }

    private fun processFocusSmsCodeField() {
        handleFieldFocus.focusField(input_verification_code)
    }

    private fun processGeeTestResult(geeTestResult: GeeTestResult?) {
        geeTestResult?.let { handleCaptcha.geeTestResult(it) }
    }

    private fun processStartCaptchaFlow() {
        handleCaptcha.startCaptchaFlow()
    }

    private fun processErrorString(message: String?) {
        message?.let { handleFieldFocus.showSnackBar(it) }
    }

    private fun processFocusField(focusableField: FocusableFields?) {
        Timber.d("FOCUS $focusableField")
        focusableField?.let {
            val view = when (focusableField) {
                FocusableFields.LOGIN_EMAIL -> input_email_sign_in
                FocusableFields.LOGIN_PASSWORD -> input_password_sign_in
                FocusableFields.REGISTRATION_EMAIL -> input_email_registration
                FocusableFields.REGISTRATION_PASSWORD -> input_password_registration
                FocusableFields.REGISTRATION_VERIFICATION_CODE -> input_verification_code
                FocusableFields.RESTORE_EMAIL -> input_email_restore
                FocusableFields.RESTORE_PASSWORD -> input_password_restore
                FocusableFields.RESTORE_VERIFICATION_CODE -> input_verification_code
                FocusableFields.CONTINUES_REGISTRATION_VERIFICATION_CODE -> input_verification_code
            }

            handleFieldFocus.apply {
                focusField(view)
            }
        }
    }

    private fun processErrorMessage(errorMessage: ErrorMessageType?) {
        newAuthSharedViewModel.showProgress(false)

        val emailVew = when (newAuthSharedViewModel.state) {
            NewAuthStates.REGISTRATION -> input_email_registration
            NewAuthStates.LOGIN -> input_email_sign_in
            NewAuthStates.PASSWORD_RESTORING -> input_email_restore
            NewAuthStates.CONTINUES_REGISTRATION -> input_verification_code
            NewAuthStates.CONTINUES_PASSWORD_RESTORING -> input_verification_code
            NewAuthStates.PASSWORD_CHANGE -> input_verification_code
            NewAuthStates.DONE_AUTH -> null
            NewAuthStates.SETTINGS -> null
            NewAuthStates.TFA_VERIFICATION -> null
            NewAuthStates.TFA_CREATE -> null
            NewAuthStates.CONTINUES_PASSWORD_CHANGE -> null
            NewAuthStates.CHANGE_PASSWORD_DONE -> null
        }

        val passwordView = when (newAuthSharedViewModel.state) {
            NewAuthStates.REGISTRATION -> input_password_registration
            NewAuthStates.LOGIN -> input_password_sign_in
            NewAuthStates.PASSWORD_RESTORING -> input_password_restore
            NewAuthStates.CONTINUES_REGISTRATION -> input_verification_code
            NewAuthStates.DONE_AUTH -> null
            NewAuthStates.CONTINUES_PASSWORD_RESTORING -> input_verification_code
            NewAuthStates.TFA_VERIFICATION -> null
            NewAuthStates.TFA_CREATE -> null
            NewAuthStates.SETTINGS -> null
            NewAuthStates.PASSWORD_CHANGE -> null
            NewAuthStates.CONTINUES_PASSWORD_CHANGE -> input_password_change
            NewAuthStates.CHANGE_PASSWORD_DONE -> TODO()
        }

        var errorText = getString(R.string.error_something_went_wrong)
        var focusView: EditText? = null

        when (errorMessage) {
            ErrorMessageType.EMPTY_FIELDS ->
                errorText = getString(R.string.error_empty_field)
            ErrorMessageType.EMPTY_EMAIL -> {
                errorText = getString(R.string.error_empty_email)
                focusView = emailVew
            }
            ErrorMessageType.LOGIN_EMPTY_EMAIL -> {
                errorText = getString(R.string.error_empty_email)
                focusView = emailVew
            }
            ErrorMessageType.SIGN_UP_EMPTY_EMAIL -> {
                errorText = getString(R.string.error_empty_email)
                focusView = emailVew
            }
            ErrorMessageType.EMPTY_PASSWORD -> {
                errorText = getString(R.string.error_empty_password)
                focusView = passwordView
            }
            ErrorMessageType.SIGN_UP_EMPTY_PASSWORD -> {
                errorText = getString(R.string.error_empty_password)
                focusView = passwordView
            }
            ErrorMessageType.LOGIN_EMPTY_PASSWORD -> {
                errorText = getString(R.string.error_empty_password)
                focusView = passwordView
            }
            ErrorMessageType.EMAIL_INCORRECT -> {
                errorText = getString(R.string.error_login_email)
                focusView = emailVew
            }
            ErrorMessageType.PASSWORD_INCORRECT -> {
                errorText = getString(R.string.error_login_requirements)
                focusView = passwordView
            }
            ErrorMessageType.PASSWORD_INCORRECT_MESSAGE -> {
                errorText = getString(R.string.error_login_password)
                focusView = passwordView
            }
            ErrorMessageType.USER_ALREADY_EXISTS -> {
                errorText = getString(R.string.error_user_already_exists)
            }
            ErrorMessageType.LOGIN_ERROR -> {
                errorText = getString(R.string.error_login_params_incorrect)
            }
            ErrorMessageType.CONFIRMATION_REQUIRED -> {
                errorText = getString(R.string.error_confirmation_required)
            }
            ErrorMessageType.CAPTCHA_TIMEOUT -> {
                errorText = getString(R.string.error_captcha_timeout)
            }
            ErrorMessageType.CAPTCHA_LOST_CONNECTION -> {
                errorText = getString(R.string.error_captcha_connection)
            }
            //TODO REFACTOR and SPLIT ERRORS
            ErrorMessageType.CAPTCHA_GET_PARSE_JSON -> {
                errorText = getString(R.string.error_captcha_common, errorMessage.ordinal)
            }
            ErrorMessageType.CAPTCHA_POST_PARSE_JSON -> {
                errorText = getString(R.string.error_captcha_common, errorMessage.ordinal)
            }
            ErrorMessageType.CAPTCHA_TOKEN -> {
                errorText = getString(R.string.error_captcha_common, errorMessage.ordinal)
            }
            ErrorMessageType.CAPTCHA_BAD_PARAMS -> {
                errorText = getString(R.string.error_captcha_common, errorMessage.ordinal)
            }
            ErrorMessageType.CAPTCHA_TOO_MANY_ATTEMPTS -> {
                errorText = getString(R.string.error_captcha_common, errorMessage.ordinal)
            }
            ErrorMessageType.CAPTCHA_RESEND -> {
                errorText = getString(R.string.error_captcha_common, errorMessage.ordinal)
            }
            ErrorMessageType.CAPTCHA_CONFIRM -> {
                errorText = getString(R.string.error_captcha_common, errorMessage.ordinal)
            }
//            ErrorMessageType.LOGIN_BAD_PARAMS -> TODO()
//            ErrorMessageType.RESTORE_EMPTY_EMAIL -> TODO()
//            ErrorMessageType.RESTORE_EMPTY_PASSWORD -> TODO()
//            ErrorMessageType.VERIFY_ACCOUNT -> TODO()
//            ErrorMessageType.MARKET_NOT_AVAILABLE -> TODO()
//            ErrorMessageType.WITHDRAWAL_DAILY_LIMIT -> TODO()
//            ErrorMessageType.NO_FINGERPRINT -> TODO()
//            ErrorMessageType.EMPTY_MESSAGE -> TODO()
//            ErrorMessageType.CHECK_PASSED -> TODO()
//            ErrorMessageType.LOG_OUT -> TODO()
//            ErrorMessageType.LOGIN_REQUEST -> TODO()
//            ErrorMessageType.FIREBASE_TOKEN -> TODO()
            else -> {
                errorText = getString(R.string.error_auth_common, errorMessage?.ordinal ?: 0)
            }
        }
        handleFieldFocus.showSnackBar(errorText, focusView)
    }

    private fun processProgressScreen(value: Boolean?) {
        value?.let { container_progress.isVisible = it }
    }

    private fun processAuthState(authAuthStates: NewAuthStates?) {
        hideKeyboard(requireContext(), requireView())
        authAuthStates?.let {
//            if (it != NewAuthStates.VERIFICATION_CODE)
            sharedAuthViewModel.setGlobalStateToActivity.value = it

            when (it) {
                NewAuthStates.REGISTRATION -> processRegistrationState()
                NewAuthStates.CONTINUES_REGISTRATION -> processContinuesRegistrationState()

                NewAuthStates.PASSWORD_RESTORING -> processPasswordRestoreState()
                NewAuthStates.CONTINUES_PASSWORD_RESTORING -> processContinuesPasswordRestoringState()
//                NewAuthStates.VERIFICATION_CODE -> processSmsCodeState()
                NewAuthStates.DONE_AUTH -> navigateToPortfolio()
                NewAuthStates.CHANGE_PASSWORD_DONE -> navigateToSettings()
                NewAuthStates.SETTINGS -> navigateToSettings()

                NewAuthStates.LOGIN -> processLoginState()
                NewAuthStates.TFA_VERIFICATION -> processTfaState(TfaState.VERIFICATION_ONLY)
                NewAuthStates.TFA_CREATE -> processTfaState(TfaState.INFORMATION)

//                NewAuthStates.PASSWORD_CHANGE -> processChangePasswordState()
//                NewAuthStates.CONTINUES_PASSWORD_CHANGE -> processContinuesPasswordChange()
            }
        }
    }

    private fun processContinuesRegistrationState() {
        removeFocusField()
        processRegistrationState()
    }

    private fun processContinuesPasswordRestoringState() {
        removeFocusField()
        processPasswordRestoreState()
    }

//    private fun processContinuesPasswordChange() {
//        removeFocusField()
//        processChangePasswordState()
//    }

//    private fun processSmsCodeState() {
//        removeFocusField()
//    }

//    private fun processChangePasswordState(state: PasswordChangeStates? = null) {
//        input_password_change.setDefaultBackground()
//        container_password_change.setDefaultBackground()
//
//        removeFocusField()
//        setInvisibleViews(
//            container_sign_in,
//            container_sms_code,
//            container_loader,
//            container_registration,
//            containerProgress,
//            containerPassCode,
//            container_restore_password
//        )
//        container_change_password.visible(true)
//
//        mPasswordChangeFragment.apply {
//            initLifecycleOwner(viewLifecycleOwner, this@NewAuthFragment, handleFingerprint)
//            initViews(
//                container_change_password,
//                newAuthSharedViewModel,
//                passwordChangeViewModel,
////                container_sms_code,
//                sharedAuthViewModel,
//                state
//            )
//        }
//    }

    private fun processPasswordRestoreState(state: PasswordRestoringStates? = null) {
        input_email_restore.setDefaultBackground()
        container_restore_password_input_password.setDefaultBackground()

        removeFocusField()
        setInvisibleViews(
            container_tfa,
            container_sign_in,
            container_sms_code,
            container_loader,
            container_registration,
            container_progress,
            containerPassCode
        )
        container_restore_password.visible(true)

        passwordRestoringFlow.apply {
            initLifecycleOwner(viewLifecycleOwner, this@NewAuthFragment, handleFingerprint)
            initViews(
                container_restore_password,
                newAuthSharedViewModel,
                passwordRestoringViewModel,
                container_sms_code,
                sharedAuthViewModel,
                state
            )
        }
    }

    private fun processRegistrationState(state: RegistrationStates? = null) {
        input_email_registration.setDefaultBackground()
        container_password.setDefaultBackground()

        removeFocusField()
        setInvisibleViews(
            container_tfa,
            container_sign_in,
            container_sms_code,
            container_loader,
            container_restore_password,
            container_progress,
            containerPassCode
        )
        container_registration.visible(true)

        registrationFlow.apply {
            initLifecycleOwner(viewLifecycleOwner, this@NewAuthFragment, handleFingerprint)
            initViews(
                container_registration,
                newAuthSharedViewModel,
                registrationViewModel,
                container_sms_code,
                sharedAuthViewModel,
                state
            )
        }
    }

    private fun processLoginState() {
        input_email_sign_in.setDefaultBackground()
        container_password_sign_in.setDefaultBackground()

        removeFocusField()
        setInvisibleViews(
            container_tfa,
            container_registration,
            container_sms_code,
            container_loader,
            container_restore_password,
            container_progress,
            containerPassCode
        )
        container_sign_in.visible(true)
        tfaFlow.setNullsToLiveData()
        loginFlow.apply {
            initLifecycleOwner(viewLifecycleOwner, this@NewAuthFragment, handleFingerprint)
            initViews(
                container_sign_in, newAuthSharedViewModel, loginViewModel, sharedAuthViewModel
            )
        }
    }

    private fun initCaptcha() {
        handleCaptcha.initCaptchaConfig(requireActivity(), newAuthSharedViewModel)
    }

    private fun addObservers() {
        lifecycle.addObserver(handleCaptcha)
    }

    private fun removeFocusField() {
        handleFieldFocus.removeFocusField()
    }

    private fun navigateToPortfolio() {
        if (findNavController().currentDestination?.id != R.id.newAuthFragment) return

        val direction = NewAuthFragmentDirections.actionNewAuthFragmentToPortfolioFragment()
        findNavController().navigate(direction)
    }

    private fun navigateToSettings() {
        if (findNavController().currentDestination?.id != R.id.newAuthFragment) return

        val direction = NewAuthFragmentDirections.actionNewAuthFragmentToSettingsFragment()
        findNavController().navigate(direction)
    }

    override fun onPause() {
        super.onPause()
        hideKeyboard(requireActivity())
    }

    override fun onStart() {
        super.onStart()
        hideKeyboard(requireContext(), requireView())
    }

    override fun onResume() {
        super.onResume()
        hideKeyboard(requireContext(), requireView())
    }
}
