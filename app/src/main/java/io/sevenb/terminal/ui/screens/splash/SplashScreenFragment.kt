package io.sevenb.terminal.ui.screens.splash

import android.os.Bundle
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import io.sevenb.terminal.R
import io.sevenb.terminal.data.model.enums.SafetyStates
import io.sevenb.terminal.ui.base.BaseFragment
import io.sevenb.terminal.ui.extension.viewModelProvider
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.Main
import timber.log.Timber

class SplashScreenFragment : BaseFragment() {
    private lateinit var splashScreenViewModel: SplashScreenViewModel

    override fun layoutId() = R.layout.fragment_splash

    //    override fun isFullScreen() = true
    override fun statusBarColor() = R.color.colorAccent

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        splashScreenViewModel = viewModelProvider(viewModelFactory)

        subscribeUI()
    }

    private fun subscribeUI() {
        splashScreenViewModel.apply {
            isLoggedOut.observe(viewLifecycleOwner, { processIsLoggedOut(it) })
            unlockMethod.observe(viewLifecycleOwner, { processUnlockMethod(it) })
        }
    }

    private fun processUnlockMethod(state: SafetyStates?) {
        state?.let {
            if (it != SafetyStates.LOGGED_OUT) {
                launchSafetyScreen(it)
            } else {
                launchAuthFragment()
            }
        }
    }

    private fun processIsLoggedOut(value: Int?) {
        value?.let {
            when (it) {
                0 -> splashScreenViewModel.getUnlockMethod()
                else -> launchAuthFragment()
            }
        }
    }

    private fun launchSafetyScreen(state: SafetyStates) {
        if (findNavController().currentDestination?.id == R.id.splashScreenFragment) {
            val direction =
                SplashScreenFragmentDirections.actionSplashScreenFragmentToSafetyFragment(state)
            findNavController().navigate(direction)
        }
    }

    private fun launchAuthFragment() {
        lifecycleScope.launch {
            delay(1500)
            if (findNavController().currentDestination?.id == R.id.splashScreenFragment) {
                val direction =
                    SplashScreenFragmentDirections.actionSplashScreenFragmentToNewAuthFragment()
                findNavController().navigate(direction)
            }
        }
    }

    override fun onSearchTapListener() {}

}