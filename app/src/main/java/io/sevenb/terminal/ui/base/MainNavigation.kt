package io.sevenb.terminal.ui.base

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.*
import androidx.annotation.ColorRes
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProvider
import dagger.android.support.DaggerFragment
import io.sevenb.terminal.AppActivity
import io.sevenb.terminal.R
import io.sevenb.terminal.ui.extension.hideKeyboard
import io.sevenb.terminal.ui.extension.setFullScreen
import io.sevenb.terminal.ui.extension.setStatusBarColor
import kotlinx.android.synthetic.main.activity_app.*
import javax.inject.Inject


/**
 * To be implemented by components that host top-level navigation destinations.
 */
interface NavigationHost {

    /** Called by MainNavigationFragment to setup it's toolbar with the navigation controller. */
    fun registerToolbarWithNavigation()
}

/**
 * To be implemented by main navigation destinations shown by a [NavigationHost].
 */
interface NavigationDestination {

    /** Called by the host when the user interacts with it. */
    fun onUserInteraction() {}
}

/**
 * Fragment representing a main navigation destination. This class handles wiring up the [Toolbar]
 * navigation icon if the fragment is attached to a [NavigationHost].
 */
abstract class BaseFragment : DaggerFragment(), NavigationDestination {

    private var navigationHost: NavigationHost? = null

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    abstract fun layoutId(): Int
    open fun toolbarTitleRes(): Int? = null
    open fun toolbarHasBackArrow() = false
    open fun hasBottomNavigation() = false
    open fun hasToolbarSearch() = true
    open fun getSoftInputMode() = WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN

    //    open fun isFullScreen(): Boolean = false
    open fun statusBarColor(): Int = R.color.colorPrimaryDark
    abstract fun onSearchTapListener()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is NavigationHost) {
            navigationHost = context
        }

//        setSoftInputMode(getSoftInputMode())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setStatusBarColor(statusBarColor())
//       setFullScreen(isFullScreen())
        return inflater.inflate(layoutId(), container, false)
    }

    override fun onResume() {
        super.onResume()
        hideKeyboard(requireContext(), requireView())
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initToolbar()
        isBottomNavigationVisible(hasBottomNavigation())
        hideKeyboard(requireContext(), requireView())
    }

    private fun isBottomNavigationVisible(isVisible: Boolean) {
        (activity as AppActivity)
            .bottomNavView.visibility = if (isVisible) View.VISIBLE else View.GONE
    }

    private fun initToolbar() {
        if (toolbarTitleRes() != null) {
            (activity as AppActivity).appToolbar.visibility = View.VISIBLE
        } else {
            (activity as AppActivity).appToolbar.visibility = View.GONE
        }

        (activity as AppActivity).initToolbar(
            toolbarTitleRes(),
            toolbarHasBackArrow(),
            hasToolbarSearch()
        )

        (activity as AppActivity).appToolbar.ivOpenSearch.setOnClickListener {
            onSearchTapListener()
        }
    }

    override fun onDetach() {
        super.onDetach()
        navigationHost = null
    }
}
