package io.sevenb.terminal.ui.screens.create_fingerprint

import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.EditText
import android.widget.ImageView
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import io.sevenb.terminal.R
import io.sevenb.terminal.data.model.broker_api.account.AccountInfo
import io.sevenb.terminal.data.model.enum_model.ErrorMessageType
import io.sevenb.terminal.data.model.portfolio.LocalAccountData
import io.sevenb.terminal.device.biometric.BiometricPromptUtils
import io.sevenb.terminal.ui.base.BaseFragment
import io.sevenb.terminal.ui.extension.*
import kotlinx.android.synthetic.main.fragment_auth_login.*
import kotlinx.android.synthetic.main.fragment_create_fingerprint.*
import timber.log.Timber
import javax.crypto.Cipher


class CreateFingerprintFragment : BaseFragment() {

    private lateinit var createFingerprintViewModel: CreateFingerprintViewModel

    override fun layoutId() = R.layout.fragment_create_fingerprint
    override fun hasToolbarSearch() = false

    private var passVisibility = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        createFingerprintViewModel = viewModelProvider(viewModelFactory)

        initView()
        subscribeUi()
        checkFingerprintsRegistered()
    }

    private fun initView() {
//        input_password_sign_in.doOnTextChanged { _, _, _, _ ->
//            container_password_sign_in.setDefaultBackground()
//        }

        welcome_title.text = getString(R.string.title_create_fingerprint)
        button_next_sign_in.text = getString(R.string.action_create_fingerprint)
        ll_action_not_account.isInvisible = true

        close_screen.isVisible = true
        close_screen.setOnClickListener { finishCreating() }

        label_input_email.visibility = View.GONE
//        input_email_sign_in.visibility = View.GONE
        ll_action_forgot_password.visibility = View.GONE

        button_next_sign_in.setOnClickListener {
            createFingerprintViewModel.accountInfo()
        }

//        input_password_sign_in.onActionDoneListener { createFingerprintViewModel.accountInfo() }

        img_pass_visibility_sign_in.setOnClickListener {
            passVisibility = !passVisibility
//            input_password_sign_in.transformationMethod =
//                if (passVisibility) HideReturnsTransformationMethod.getInstance() else
//                    PasswordTransformationMethod.getInstance()
//            input_password_sign_in.setSelection(input_password_sign_in.text.length)
            changeEyeImage(img_pass_visibility_sign_in)
        }

//        input_password_sign_in.focusField(requireContext())
    }

    private fun changeEyeImage(imageView: ImageView) {
        imageView.background =
            if (passVisibility) ContextCompat.getDrawable(requireContext(), R.drawable.ic_password)
            else ContextCompat.getDrawable(requireContext(), R.drawable.ic_password_hidden)
    }

    override fun getSoftInputMode() = WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE

    override fun onSearchTapListener() {
        Timber.d("onSearchTapListener ")
    }

    private fun subscribeUi() {
        createFingerprintViewModel.fingerPrintCreated.observe(
            viewLifecycleOwner,
            { finishCreating() })
        createFingerprintViewModel.createFingerCipher.observe(viewLifecycleOwner, {
            createFingerprintDialog(it)
        })
        createFingerprintViewModel.snackbarMessage.observe(viewLifecycleOwner, { showMessage(it) })
        createFingerprintViewModel.customMessage.observe(viewLifecycleOwner, { showSnackbar(it) })
        createFingerprintViewModel.accountInfo.observe(viewLifecycleOwner, { accountInfo(it) })
        createFingerprintViewModel.localAccountDataResult.observe(
            viewLifecycleOwner,
            { localAccountData(it) })
        createFingerprintViewModel.passwordExist.observe(viewLifecycleOwner, { passwordExist() })
    }

    private fun checkPasswordField(view: EditText, container: ConstraintLayout): Boolean {
        val password = view.getString()
        if (password.isEmpty()) {
            incorrectPassword(container, ErrorMessageType.EMPTY_PASSWORD)
            return false
        } else if (!createFingerprintViewModel.checkValidPassword(password)) {
            incorrectPassword(container, ErrorMessageType.PASSWORD_INCORRECT_MESSAGE)
            return false
        } else container.setDefaultBackground()

        return true
    }

    private fun incorrectPassword(view: View, message: ErrorMessageType) {
        showMessage(message)
        view.setErrorBackground()
    }

    private fun passwordExist() {
        tv_fingerprint_hint.isVisible = true
        welcome_title.text = getString(R.string.title_created_fingerprint)
        button_next_sign_in.text = getString(R.string.action_create_new_fingerprint)
        val constraintSet = ConstraintSet()
        constraintSet.clone(cl_top_elements)
        constraintSet.connect(
            R.id.container_password_sign_in,
            ConstraintSet.TOP,
            R.id.tv_fingerprint_hint,
            ConstraintSet.BOTTOM,
            51.toDp(requireContext())
        )
        constraintSet.applyTo(cl_top_elements)
    }

    private fun accountInfo(accountInfo: AccountInfo) {
        signInUser(accountInfo.email)
    }

    private fun localAccountData(localAccountData: LocalAccountData) {
        signInUser(localAccountData.email)
    }

    private fun signInUser(email: String) {
//        val password = input_password_sign_in.text.toString()

//        if (!checkPasswordField(input_password_sign_in, container_password_sign_in)) return

//        createFingerprintViewModel.signInUser(email, password)
    }

    private fun checkFingerprintsRegistered() {
        if (BiometricManager.from(requireContext())
                .canAuthenticate() != BiometricManager.BIOMETRIC_SUCCESS
        ) {
            showMessage(ErrorMessageType.NO_FINGERPRINT)
        }
    }

    private fun createFingerprintDialog(cipher: Cipher) {
        if (BiometricManager.from(requireContext())
                .canAuthenticate() == BiometricManager.BIOMETRIC_SUCCESS
        ) {
            val biometricPrompt =
                BiometricPromptUtils.createBiometricPrompt(
                    this,
                    ::encryptPassword,
                    ::biometricError
                )
            val promptInfo = BiometricPromptUtils.createLoginPromptInfo(this)
            biometricPrompt.authenticate(promptInfo, BiometricPrompt.CryptoObject(cipher))
        } else {
            showSnackbar(getString(R.string.biometric_error))
        }
    }

    private fun encryptPassword(authResult: BiometricPrompt.AuthenticationResult) {
        authResult.cryptoObject?.cipher.let { createFingerprintViewModel.encryptPassword(it) }
    }

    private fun biometricError(errorCode: Int) {
        showSnackbar(getString(R.string.error_biometric_unsuccessful))
        Timber.e("biometricError code=$errorCode")
    }

    private fun finishCreating() {
        findNavController().popBackStack(R.id.settingsFragment, false)
    }

    private fun showMessage(errorMessageType: ErrorMessageType) {
        when (errorMessageType) {
            ErrorMessageType.EMPTY_FIELDS -> showSnackbar(getString(R.string.error_empty_field))
            ErrorMessageType.EMAIL_INCORRECT -> showSnackbar(getString(R.string.error_login_email))
            ErrorMessageType.PASSWORD_INCORRECT -> showSnackbar(getString(R.string.error_login_requirements))
            ErrorMessageType.USER_ALREADY_EXISTS -> showSnackbar(getString(R.string.error_user_already_exists))
            ErrorMessageType.LOGIN_ERROR -> showSnackbar(getString(R.string.error_login_params_incorrect))
            ErrorMessageType.NO_FINGERPRINT -> showSnackbar(getString(R.string.biometric_error))
            ErrorMessageType.PASSWORD_INCORRECT_MESSAGE -> showSnackbar(getString(R.string.error_login_password))
            ErrorMessageType.EMPTY_PASSWORD -> showSnackbar(getString(R.string.error_empty_password))
        }
    }

    private fun showSnackbar(message: String) {
        Snackbar.make(container_create_fingerprint, message, Snackbar.LENGTH_SHORT).show()
    }

}