package io.sevenb.terminal.ui.extension

inline fun <reified T : Any> List<*>.typeIs() = all { it is T }