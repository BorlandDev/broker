package io.sevenb.terminal.ui.screens.safety.passcode_state

import android.animation.Animator
import android.animation.ObjectAnimator
import android.view.View
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import io.sevenb.terminal.R
import io.sevenb.terminal.data.model.enums.pass_code.PassCodeStates
import io.sevenb.terminal.data.model.enums.pass_code.PassCodeSubStates
import io.sevenb.terminal.domain.usecase.new_auth.HandleFingerprint
import io.sevenb.terminal.ui.base.BaseLifecycleObserver
import io.sevenb.terminal.ui.extension.hideKeyboard
import io.sevenb.terminal.ui.extension.newBackground
import io.sevenb.terminal.ui.extension.removeLast
import io.sevenb.terminal.ui.screens.safety.SafetyViewModel
import io.sevenb.terminal.ui.screens.settings.SharedSettingToActivityViewModel
import io.sevenb.terminal.utils.Logger
import kotlinx.android.synthetic.main.fragment_pass_code.view.*
import javax.crypto.Cipher

class PassCodeFlow : BaseLifecycleObserver() {
    private lateinit var passCodeViewModel: PassCodeViewModel
    private lateinit var safetyViewModel: SafetyViewModel
    private lateinit var sharedSettingsToActivityViewModel: SharedSettingToActivityViewModel

    private lateinit var view: View
    private lateinit var lifecycleOwner: LifecycleOwner
    override lateinit var fragment: Fragment

    private lateinit var handleFingerprint: HandleFingerprint

    private var fromSettings = false

    fun initLifecycleOwner(
        lifecycleOwner: LifecycleOwner,
        fragment: Fragment,
        handleFingerprint: HandleFingerprint
    ) {
        this.lifecycleOwner = lifecycleOwner
        this.fragment = fragment
        this.handleFingerprint = handleFingerprint
        handleFingerprint.init(fragment)
    }

    fun initSharedSettingsToActivityViewModel(sharedSettingsToActivityViewModel: SharedSettingToActivityViewModel) {
        this.sharedSettingsToActivityViewModel = sharedSettingsToActivityViewModel
    }

    fun initViews(
        view: View,
        passCodeViewModel: PassCodeViewModel,
        safetyViewModel: SafetyViewModel,
        state: PassCodeStates,
        email: String,
        password: String,
        fromSettings: Boolean = false
    ) {
        this.passCodeViewModel = passCodeViewModel
        this.safetyViewModel = safetyViewModel
        this.view = view
        this.fromSettings = fromSettings
        passCodeViewModel.email = email
        passCodeViewModel.password = password
        passCodeViewModel.errorCount = 0
        passCodeViewModel.biometricEnabled = false

        if (!passCodeViewModel.isSubscribed)
            subscribeUI()

        passCodeViewModel.setNewCurrentState(state)

        handleButtonClicks()
        hideKeyboard(view.context, view)
    }

    private fun handleButtonClicks() {
        view.apply {
            tvNumber1.setOnClickListener { processNumberClicked("1") }
            tvNumber2.setOnClickListener { processNumberClicked("2") }
            tvNumber3.setOnClickListener { processNumberClicked("3") }
            tvNumber4.setOnClickListener { processNumberClicked("4") }
            tvNumber5.setOnClickListener { processNumberClicked("5") }
            tvNumber6.setOnClickListener { processNumberClicked("6") }
            tvNumber7.setOnClickListener { processNumberClicked("7") }
            tvNumber8.setOnClickListener { processNumberClicked("8") }
            tvNumber9.setOnClickListener { processNumberClicked("9") }
            tvNumber0.setOnClickListener { processNumberClicked("0") }
            logOut.setOnClickListener { processChangeButtonClicked() }
            clearBiometric.setOnClickListener { processFingerprintButtonClicked() }
        }
    }

    private fun subscribeUI() {
        passCodeViewModel.apply {
            isSubscribed = true
            currentState.observe(lifecycleOwner, { processNewCurrentState(it) })
            changeBiometricToBackspace.observe(
                lifecycleOwner, { processChangeBiometricToBackspace(it) })
            currentSubState.observe(lifecycleOwner, { processCurrentSubState(it) })
            enteredNumbers.observe(lifecycleOwner, { processEnteredNumbers(it) })
            passCodeStored.observe(lifecycleOwner, { processPassCodeStored(it) })
            decryptBiometricCipher.observe(lifecycleOwner, { processFingerprintExistence(it) })
            errorString.observe(lifecycleOwner, { showError(it) })
            createFingerprint.observe(lifecycleOwner, { processCreateFingerprint(it) })
            showLoader.observe(lifecycleOwner, { processShowLoader(it) })
        }
    }

    private fun processPassCodeStored(value: Boolean?) {
        value?.let {
            if (it)
                if (fromSettings) {
                    sharedSettingsToActivityViewModel.apply {
                        createPassCodeFromSettings.value = Pair(false, null)
                        message.value = "PIN created"
                    }
                } else passCodeViewModel.createFingerprint()
        }
    }

    private fun processShowLoader(message: String?) {
        message?.let { safetyViewModel.showLoader.value = it }
    }

    private fun showError(message: String?) {
        message?.let { }
    }

    private fun processFingerprintExistence(cipher: Cipher?) {
        cipher?.let {
            handleFingerprint.decryptFingerprintDialog(
                it,
                { passCodeViewModel.setNewCurrentState(PassCodeStates.ENTER_PASS_CODE) },
                ::decryptPasswordFromStorage,
                ::biometricErrorSignInWithFingerprint
            )
        }
    }

    private fun decryptPasswordFromStorage(authResult: BiometricPrompt.AuthenticationResult) {
        if (handleFingerprint.isBiometricSuccess()) {
            authResult.cryptoObject?.cipher.let {
                passCodeViewModel.authDone()
            }
        } else validateShowBiometricError()
    }

    private fun validateShowBiometricError() {
        when (handleFingerprint.getBiometricAuthenticateAbility()) {
            BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE ->
                showError("No biometric features available on this device.")
            BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE ->
                showError("Biometric features are currently unavailable.")
        }
    }

    private fun biometricErrorSignInWithFingerprint(errorCode: Int) {
        if (errorCode == 7 || errorCode == 10 || errorCode == 13) {
            passCodeViewModel.setNewCurrentState(PassCodeStates.ENTER_PASS_CODE_BOTH_METHODS)
        }
    }

    private fun processEnteredNumbers(value: Int?) {
        value?.let {
            resetPickers()
            when (it) {
                0 -> resetPickers()
                1 -> view.ivFirstNumber.newBackground(R.drawable.ic_passcode_entered_symbol)
                2 ->
                    view.apply {
                        ivFirstNumber.newBackground(R.drawable.ic_passcode_entered_symbol)
                        ivSecondNumber.newBackground(R.drawable.ic_passcode_entered_symbol)
                    }
                3 -> view.apply {
                    ivFirstNumber.newBackground(R.drawable.ic_passcode_entered_symbol)
                    ivSecondNumber.newBackground(R.drawable.ic_passcode_entered_symbol)
                    ivThirdNumber.newBackground(R.drawable.ic_passcode_entered_symbol)
                }
                4 -> view.apply {
                    ivFirstNumber.newBackground(R.drawable.ic_passcode_entered_symbol)
                    ivSecondNumber.newBackground(R.drawable.ic_passcode_entered_symbol)
                    ivThirdNumber.newBackground(R.drawable.ic_passcode_entered_symbol)
                    ivFourthNumber.newBackground(R.drawable.ic_passcode_entered_symbol)
                }
                else -> {
                }
            }
        }
    }

    private fun resetPickers() {
        view.apply {
            ivFirstNumber.newBackground(R.drawable.ic_passcode_not_entered_symbol)
            ivSecondNumber.newBackground(R.drawable.ic_passcode_not_entered_symbol)
            ivThirdNumber.newBackground(R.drawable.ic_passcode_not_entered_symbol)
            ivFourthNumber.newBackground(R.drawable.ic_passcode_not_entered_symbol)
        }
    }

    private fun processCurrentSubState(state: PassCodeSubStates?) {
        state?.let {
            when (it) {
                PassCodeSubStates.LOG_OUT -> logOut()
                PassCodeSubStates.SKIPPED -> processSkipped()
                PassCodeSubStates.PASS_CODE_ENTERING_ERROR -> processEnteringError()
                PassCodeSubStates.PASS_CODE_ENTERING_PASSED -> processPassCodeEntered()
                PassCodeSubStates.PASS_CODE_ENTERING_FAILED -> processEnteringFail()
                PassCodeSubStates.PASS_CODE_CONFIRMING_ERROR -> processConfirmingError()
                PassCodeSubStates.PASS_CODE_CONFIRMING_FAILED -> processConfirmingFail()
                PassCodeSubStates.PASS_CODE_CONFIRMING_PASSED -> passCodeCreated()
            }
        }
    }

    private fun processPassCodeEntered() {
        if (fromSettings) {
            sharedSettingsToActivityViewModel.enterPassCodeFromSettings.value = Pair(false, null)
        } else {
            passCodeViewModel.authDone()
        }
    }

    private fun processSkipped() = signInWithNotify()
    private fun processEnteringFail() = logOut()
    private fun processConfirmingFail() = signInWithNotify()

    private fun processEnteringError() {
        passCodeViewModel.apply {
            errorCount++
            if (errorCount >= 5) {
                if (passCodeViewModel.biometricEnabled) showBiometric()
                else setNewSubState(PassCodeSubStates.PASS_CODE_ENTERING_FAILED)
            } else shakeAnimation()
        }
    }

    private fun processConfirmingError() {
        passCodeViewModel.apply {
            errorCount++
            if (errorCount >= 5) setNewSubState(PassCodeSubStates.PASS_CODE_CONFIRMING_FAILED)
            else shakeAnimation()
        }
    }

    fun shakeAnimation() {
        val objectAnimator = ObjectAnimator
            .ofFloat(
                view.containerPassDots,
                "translationX", 0F, 25F, -25F, 25F, -25F, 15F, -15F, 6F, -6F, 0F
            )
            .setDuration(500)
        objectAnimator.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator?) {}
            override fun onAnimationEnd(animation: Animator?) {
                passCodeViewModel.apply {
                    when (localState) {
                        PassCodeStates.CREATE_PASS_CODE -> {
                        }
                        PassCodeStates.CONFIRM_PASS_CODE -> {
                            resetCreatingPassCode()
                            setNewCurrentState(PassCodeStates.CREATE_PASS_CODE)
                        }
                        PassCodeStates.ENTER_PASS_CODE, PassCodeStates.ENTER_PASS_CODE_BOTH_METHODS ->
                            enterPassCode = ""
                    }
                }
            }

            override fun onAnimationCancel(animation: Animator?) {}
            override fun onAnimationRepeat(animation: Animator?) {}

        })
        objectAnimator.start()
    }

    private fun passCodeCreated() {
        passCodeViewModel.storePassCode()
    }

    private fun processChangeBiometricToBackspace(value: Boolean?) {
        val checkBiometricExistence = checkBiometricHardwareExistence()
        value?.let {
            view.clearBiometric.newBackground(
                if (it) R.drawable.ic_backspace else {
                    if (checkBiometricExistence) R.drawable.ic_fingerprint
                    else R.drawable.ic_backspace
                }
            )
        }
    }

    private fun processNewCurrentState(state: PassCodeStates?) {
        when (state) {
            PassCodeStates.CREATE_PASS_CODE -> {
                setNewTitle(getString(R.string.title_create_pass_code))
                setChangeButtonTitle(getString(R.string.label_cancel))

                resetCreatingPassCode()

                setFingerprintButton(false)
                view.logOut.setOnClickListener { passCodeViewModel.passCodeCancelled() }
            }
            PassCodeStates.CONFIRM_PASS_CODE -> {
                setNewTitle(getString(R.string.title_confirm_pass_code))
                setChangeButtonTitle(getString(R.string.action_button_back))

                resetPickers()

                setFingerprintButton(false)
                view.logOut.setOnClickListener { passCodeViewModel.setNewCurrentState(PassCodeStates.CREATE_PASS_CODE) }
            }
            PassCodeStates.ENTER_PASS_CODE, PassCodeStates.ENTER_PASS_CODE_BOTH_METHODS -> {
                resetPickers()
                passCodeViewModel.enterPassCode = ""
                setNewTitle(getString(R.string.title_enter_pass_code))
                setChangeButtonTitle(getString(R.string.title_log_out))

                if (state != PassCodeStates.ENTER_PASS_CODE) {
                    passCodeViewModel.biometricEnabled = true
                    showBiometric()
                }

                view.logOut.visibility =
                    if (fromSettings) View.INVISIBLE else View.VISIBLE

                setFingerprintButton(state != PassCodeStates.ENTER_PASS_CODE && !fromSettings)
                view.logOut.setOnClickListener {
                    safetyViewModel.logOut()
//                    safetyViewModel.navigateTo(NewAuthStates.LOGIN)
                }
            }
        }
    }

    private fun resetCreatingPassCode() {
        passCodeViewModel.apply {
            createPassCode = ""
            confirmPassCode = ""
        }
    }

    private fun setNewTitle(title: String) {
        view.tvEnterPassTitle.text = title
    }

    private fun setChangeButtonTitle(title: String) {
        view.logOut.text = title
    }

    private fun setFingerprintButton(value: Boolean) {
        view.clearBiometric.newBackground(
            if (value) R.drawable.ic_fingerprint else R.drawable.ic_backspace
        )
    }

    private fun createPassCode(passCode: String) {
        passCodeViewModel.apply {
            if (passCode == "_") createPassCode = createPassCode.removeLast()
            else createPassCode += passCode
        }
    }

    private fun confirmPassCode(passCode: String) {
        passCodeViewModel.apply {
            if (passCode == "_") confirmPassCode = confirmPassCode.removeLast()
            else confirmPassCode += passCode
        }
    }

    private fun enterPassCode(passCode: String) {
        passCodeViewModel.apply {
            if (passCode == "_") enterPassCode = enterPassCode.removeLast()
            else enterPassCode += passCode
        }
    }

    private fun processNumberClicked(number: String) {
        when (passCodeViewModel.localState) {
            PassCodeStates.ENTER_PASS_CODE, PassCodeStates.ENTER_PASS_CODE_BOTH_METHODS ->
                enterPassCode(number)
            PassCodeStates.CONFIRM_PASS_CODE -> confirmPassCode(number)
            PassCodeStates.CREATE_PASS_CODE -> createPassCode(number)
        }
    }

    private fun processFingerprintButtonClicked() {
        when (passCodeViewModel.localState) {
            PassCodeStates.ENTER_PASS_CODE, PassCodeStates.ENTER_PASS_CODE_BOTH_METHODS -> {
                if (passCodeViewModel.enterPassCode.isNotEmpty()) enterPassCode("_")
                else {
                    if (checkBiometricHardwareExistence()) showBiometric()
                    else enterPassCode("_")
                }
            }
            PassCodeStates.CONFIRM_PASS_CODE -> confirmPassCode("_")
            PassCodeStates.CREATE_PASS_CODE -> createPassCode("_")
        }
    }

    private fun showBiometric() {
        if (passCodeViewModel.localState == PassCodeStates.ENTER_PASS_CODE_BOTH_METHODS) {
            if (handleFingerprint.isBiometricSuccess()) {
                passCodeViewModel.checkFingerprintExistence()
            } else passCodeViewModel.authDone()
        }
    }

    private fun processChangeButtonClicked() {
        when (passCodeViewModel.localState) {
            PassCodeStates.ENTER_PASS_CODE, PassCodeStates.ENTER_PASS_CODE_BOTH_METHODS -> {
                if (fromSettings) {
                    sharedSettingsToActivityViewModel.enterPassCodeFromSettings.value =
                        Pair(null, null)
                } else passCodeViewModel.setNewSubState(PassCodeSubStates.LOG_OUT)
            }
            PassCodeStates.CONFIRM_PASS_CODE -> passCodeViewModel.setNewCurrentState(PassCodeStates.CREATE_PASS_CODE)
            PassCodeStates.CREATE_PASS_CODE -> {
                if (fromSettings) {
                    sharedSettingsToActivityViewModel.createPassCodeFromSettings.value =
                        Pair(null, null)
                } else passCodeViewModel.setNewSubState(PassCodeSubStates.SKIPPED)
            }
        }
    }

    private fun processCreateFingerprint(cipher: Cipher?) {
        cipher?.let {
            handleFingerprint.createFingerprintDialog(
                it,
                { signIn() },
                ::encryptPassword,
                ::biometricErrorCreateFingerprint,
                "Skip"
            )
        }
    }

    private fun signIn() {
        passCodeViewModel.apply {
            Logger.sendLog(
                "PassCodeFlow",
                "signIn",
            )
            signInAfterAccountCreated(email, password)
        }
    }

    private fun signInWithNotify() {
        if (fromSettings) {
            sharedSettingsToActivityViewModel.apply {
                createPassCodeFromSettings.value = Pair(null, null)
                message.value = "PIN not created"
            }
        } else {
            popupInformation()
            passCodeViewModel.apply {
                Logger.sendLog(
                    "PassCodeFlow",
                    "signInWithNotify",
                )
                signInAfterAccountCreated(email, password)
            }
        }
    }

    private fun popupInformation() {
        MaterialAlertDialogBuilder(fragment.requireContext()).apply {
            val isBiometricExist = checkBiometricHardwareExistence()
            setMessage(getString(if (isBiometricExist) R.string.message_activate_fingerprint_pin else R.string.message_u_can_activate_pin))
            setPositiveButton(android.R.string.ok) { dialog, _ -> dialog.dismiss() }
        }.create().show()
    }

    private fun logOut() {
        if (fromSettings) {
            sharedSettingsToActivityViewModel.apply {
                enterPassCodeFromSettings.value = Pair(null, null)
                message.value = "Unable to create fingerprint"
            }
        } else safetyViewModel.logOut()
    }

    private fun encryptPassword(authResult: BiometricPrompt.AuthenticationResult) {
        if (handleFingerprint.isBiometricSuccess()) {
            authResult.cryptoObject?.cipher.let {
                passCodeViewModel.encryptPassword(it)
            }
        } else validateShowBiometricError()
    }

    private fun biometricErrorCreateFingerprint(errorCode: Int) {
        if (errorCode == 10 || errorCode == 7 || errorCode == 13) {
            passCodeViewModel.fingerprintCancelled()
            if (errorCode == 7) passCodeViewModel.biometricEnabled = false
        }
    }

    fun setNullsToLiveData() {
        passCodeViewModel.apply {
            currentState.value = null
            currentSubState.value = null
            passCodeStored.value = null
            changeBiometricToBackspace.value = null
            enteredNumbers.value = null
            errorString.value = null
            decryptBiometricCipher.value = null
            createFingerprint.value = null
            showLoader.value = null
        }
    }

    private fun checkBiometricHardwareExistence(): Boolean {
        return when (handleFingerprint.getBiometricAuthenticateAbility()) {
            BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE -> false
            BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> false
            BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> false
            BiometricManager.BIOMETRIC_SUCCESS -> true
            else -> false
        }
    }

}
