package io.sevenb.terminal.ui.screens.select_currency

import androidx.lifecycle.ViewModel
import io.sevenb.terminal.domain.usecase.sort.SortCurrenciesHandler
import javax.inject.Inject

class SelectCurrencyViewModel @Inject constructor(
    private val sortCurrencyHandler: SortCurrenciesHandler
) : ViewModel() {

    fun sortCurrency(list: List<BaseSelectItem>) = sortCurrencyHandler.sortByTicker(list)

    fun makeSingleBTCTicker(list: List<BaseSelectItem>) =
        sortCurrencyHandler.makeSingleBTCTicker(list)

    fun filterBnbBscNetwork(list: List<BaseSelectItem>) =
        sortCurrencyHandler.filterBnbBscNetwork(list)
}