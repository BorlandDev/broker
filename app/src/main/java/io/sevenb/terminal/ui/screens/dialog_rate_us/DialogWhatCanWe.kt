package io.sevenb.terminal.ui.screens.dialog_rate_us

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import io.sevenb.terminal.R
import io.sevenb.terminal.databinding.DialogWhatCanWeBinding
import io.sevenb.terminal.utils.Logger


class DialogWhatCanWe : DialogFragment() {
    private var _binding: DialogWhatCanWeBinding? = null
    private val binding get() = _binding!!

    private var listenerConfirm: ((Boolean) -> Unit)? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = DialogWhatCanWeBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.tvConfirm.setOnClickListener {
            Logger.sendLogFeedback(binding.etMessage.text.toString())
            listenerConfirm?.invoke(true)
            dismiss()
        }

        binding.tvStepBack.setOnClickListener {
            listenerConfirm?.invoke(false)
            dismiss()
        }

        binding.tvCancel.setOnClickListener {
            dismiss()
        }
    }

    fun initListenerConfirm(data: (Boolean) -> Unit) {
        listenerConfirm = data
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    companion object {
        fun getInstance(): DialogWhatCanWe {
            val fragment = DialogWhatCanWe()
            fragment.setStyle(STYLE_NO_FRAME, R.style.RateUsDialog)
            return fragment
        }
    }


}