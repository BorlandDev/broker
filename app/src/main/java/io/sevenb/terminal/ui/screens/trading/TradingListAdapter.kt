package io.sevenb.terminal.ui.screens.trading

import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.CandleEntry
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.ChartTouchListener
import com.github.mikephil.charting.listener.OnChartGestureListener
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import io.sevenb.terminal.R
import io.sevenb.terminal.data.model.trading.TradeChartParams
import io.sevenb.terminal.data.model.trading.TradeChartRange
import io.sevenb.terminal.device.utils.DateTimeUtil.dateFromTimestamp
import io.sevenb.terminal.device.utils.DateTimeUtil.timeFromTimestamp
import io.sevenb.terminal.ui.extension.autoNotify
import io.sevenb.terminal.ui.extension.typeIs
import io.sevenb.terminal.ui.screens.order.OrderBottomSheetFragment
import io.sevenb.terminal.ui.view.CustomMarker
import kotlinx.android.synthetic.main.item_trading_list_expand.view.*
import kotlinx.android.synthetic.main.item_trading_list_main.view.*
import kotlinx.coroutines.runBlocking
import timber.log.Timber
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*
import kotlin.math.absoluteValue
import kotlin.properties.Delegates

class TradingListAdapter(
    private val openTrade: (Pair<String, OrderBottomSheetFragment.Companion.OrderSide>) -> Unit,
    private val updateItemChartByPosition: (TradeChartParams) -> Unit,
    private val listScrollEnabled: (Boolean) -> Unit,
    private val processFavorite: (TradingWalletItem) -> Unit,
    private val processAlert: (TradingWalletItem) -> Unit,
    private val scrollToPosition: (Int) -> Unit,
    private val updateExpandSelectedRange: (Int, TradeChartRange) -> Unit = { _, _ -> },
    private val updateCandleChartData: (TradeChartParams) -> Unit = {},
    private val updateExpandedCandleChartData: (TradeChartParams) -> Unit = {}
) : RecyclerView.Adapter<TradingItemViewHolder>() {

    var baseItemsList: MutableList<BaseTradingListItem> by Delegates.observable(mutableListOf()) { prop, oldList, newList ->
        autoNotify(oldList, newList) { o, n ->
            (o as TradingWalletItem).ticker == (n as TradingWalletItem).ticker
        }
    }
    private var expandedPosition = -1
    var baseCurrencyTicker = ""

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TradingItemViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_trading_list, parent, false)

        initLineChart(view)

        return TradingItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: TradingItemViewHolder, position: Int) {
        TODO("Not yet implemented")
    }

    override fun onBindViewHolder(
        holder: TradingItemViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        val item = baseItemsList[position]

        if (payloads.isEmpty()) {
            item.isExpanded = position == expandedPosition
            holder.itemView.trading_item_main.setOnClickListener {
                if (position == expandedPosition) {
                    item.updateExpandedStateChannel?.offer(Pair(item.ticker, false))
                    expandedPosition = -1
                    item.isExpanded = false
                    notifyItemChanged(position)
                } else {
                    item.updateExpandedStateChannel?.offer(Pair(item.ticker, true))
                    val previousExpanded = expandedPosition
                    expandedPosition = position
                    item.isExpanded = true
                    if (previousExpanded >= 0) notifyItemChanged(previousExpanded)
                    notifyItemChanged(position)
                    scrollToPosition(position)
                }
            }

            holder.bind(
                item,
                openTrade,
                baseCurrencyTicker,
                updateItemChartByPosition,
                processFavorite,
                processAlert,
                position,
                updateExpandSelectedRange,
                updateCandleChartData,
                updateExpandedCandleChartData
            )
        } else {
            val firstPayload = payloads[0] as? List<Any> ?: listOf()
            if (firstPayload.isNotEmpty()) {
                when (val firstItem = firstPayload[0]) {
                    is List<*> -> {
                        when {
                            firstItem.isNullOrEmpty() -> {
                            }
                            firstItem.typeIs<Entry>() -> {
                                val tradeChartParams =
                                    firstPayload[1] as? TradeChartParams ?: TradeChartParams(
                                        0, TradeChartRange.DAY
                                    )

                                holder.bindChartData(
                                    item, firstItem as List<Entry>, tradeChartParams.tradeChartRange
                                )
                            }
                            firstItem.typeIs<CandleEntry>() -> {
                                holder.candleDataSetBytEntries(
                                    item, firstItem as List<CandleEntry>
                                )
                            }
                        }
                    }
                    is String -> {
                        holder.bindPrice(item, firstItem)
                    }
                }
            }
        }
    }

    fun submitList(list: List<BaseTradingListItem>) {
        baseItemsList.clear()
        baseItemsList = list.toMutableList()
        notifyDataSetChanged()
    }

    private fun initLineChart(itemView: View) {
        itemView.item_line_chart.run {
            description.isEnabled = false
            isDoubleTapToZoomEnabled = false
            setTouchEnabled(false)
            setDrawGridBackground(false)

            isDragEnabled = false
            setScaleEnabled(false)
            isHighlightPerTapEnabled = false
            isHighlightPerDragEnabled = false

            xAxis.isEnabled = false
            xAxis.setDrawGridLines(false)

            axisRight.isEnabled = false
            legend.isEnabled = false

            setBackgroundColor(Color.WHITE)
            axisLeft.setDrawLabels(false)
            axisLeft.setDrawAxisLine(false)
            axisLeft.setDrawGridLines(false)
        }

        itemView.candle_stick_chart.run {
            setViewPortOffsets(50f, 0f, 120f, 25f)
            setBackgroundColor(Color.WHITE)
            description.isEnabled = false
            setDrawGridBackground(false)
            isDragEnabled = true
            isHighlightPerTapEnabled = true
            isHighlightPerDragEnabled = false

            setPinchZoom(true)
            isScaleXEnabled = true

            isAutoScaleMinMaxEnabled = true

            xAxis.isEnabled = true
            xAxis.setDrawGridLines(false)

            isDoubleTapToZoomEnabled = false
            maxVisibleCount

            axisLeft.isEnabled = false
            axisRight.isEnabled = true
            legend.isEnabled = false

            axisRight.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART)
            axisRight.textColor = Color.rgb(197, 197, 209)
            axisRight.gridColor = Color.rgb(240, 240, 245)
            axisRight.gridLineWidth = 1.2f
            axisRight.valueFormatter
            axisRight.setDrawAxisLine(false)

            onChartGestureListener = object : OnChartGestureListener {
                override fun onChartGestureStart(
                    me: MotionEvent?,
                    lastPerformedGesture: ChartTouchListener.ChartGesture?
                ) {
                    listScrollEnabled(false)
                }

                override fun onChartGestureEnd(
                    me: MotionEvent?,
                    lastPerformedGesture: ChartTouchListener.ChartGesture?
                ) {
                    itemView.expanded_line_chart.isHighlightPerDragEnabled = false
                    itemView.expanded_line_chart.highlightValue(0.0f, -1)
                    itemView.picked_amount.text = ""
                    itemView.picked_date.text = ""
                    listScrollEnabled(true)
                }

                override fun onChartFling(
                    me1: MotionEvent?,
                    me2: MotionEvent?,
                    velocityX: Float,
                    velocityY: Float
                ) {
                    Timber.d("onChartFling")
                }

                override fun onChartSingleTapped(me: MotionEvent?) {
                    Timber.d("onChartSingleTapped")
                }

                override fun onChartScale(me: MotionEvent?, scaleX: Float, scaleY: Float) {
                    Timber.d("onChartScale")
                }

                override fun onChartDoubleTapped(me: MotionEvent?) {
                    Timber.d("onChartDoubleTapped")
                }

                override fun onChartTranslate(me: MotionEvent?, dX: Float, dY: Float) {
                    Timber.d("onChartTranslate")
                }

                override fun onChartLongPressed(me: MotionEvent?) {
                    itemView.expanded_line_chart.isHighlightPerDragEnabled = true
                }
            }
        }

        itemView.expanded_line_chart.run {
            setViewPortOffsets(50f, 0f, 120f, 25f)
            setBackgroundColor(Color.WHITE)
            description.isEnabled = false
            isDoubleTapToZoomEnabled = false
            setDrawGridBackground(false)
            isDragEnabled = true
            setScaleEnabled(false)
            isHighlightPerTapEnabled = true
            isHighlightPerDragEnabled = false

            xAxis.isEnabled = true
            xAxis.setDrawGridLines(false)

            axisLeft.isEnabled = false
            axisRight.isEnabled = true
            legend.isEnabled = false

            axisRight.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART)
            axisRight.textColor = Color.rgb(197, 197, 209)
            axisRight.gridColor = Color.rgb(240, 240, 245)
            axisRight.gridLineWidth = 1.2f
            axisRight.setDrawAxisLine(false)
            marker = CustomMarker(itemView.context, R.layout.view_custom_marker)

            setOnChartValueSelectedListener(object : OnChartValueSelectedListener {
                override fun onNothingSelected() {
                    Timber.d("onNothingSelected")
                }

                override fun onValueSelected(e: Entry?, h: Highlight?) {
                    Timber.d("onValueSelected x=${e?.x} y=${e?.y}")

                    val date = dateFromTimestamp(e?.x?.toLong() ?: 0L)
                    val time = timeFromTimestamp(e?.x?.toLong() ?: 0L)
                    itemView.picked_date.text = "%s %s".format(date, time)
                    listScrollEnabled(false)
                }
            })

            onChartGestureListener = object : OnChartGestureListener {
                override fun onChartGestureStart(
                    me: MotionEvent?,
                    lastPerformedGesture: ChartTouchListener.ChartGesture?
                ) {
                }

                override fun onChartGestureEnd(
                    me: MotionEvent?,
                    lastPerformedGesture: ChartTouchListener.ChartGesture?
                ) {
                    itemView.expanded_line_chart.isHighlightPerDragEnabled = false
                    itemView.picked_amount.text = ""
                    itemView.picked_date.text = ""
                    listScrollEnabled(true)
                }

                override fun onChartFling(
                    me1: MotionEvent?,
                    me2: MotionEvent?,
                    velocityX: Float,
                    velocityY: Float
                ) {
                    Timber.d("onChartFling")
                }

                override fun onChartSingleTapped(me: MotionEvent?) {
                    Timber.d("onChartSingleTapped")
                }

                override fun onChartScale(me: MotionEvent?, scaleX: Float, scaleY: Float) {
                    Timber.d("onChartScale")
                }

                override fun onChartDoubleTapped(me: MotionEvent?) {
                    Timber.d("onChartDoubleTapped")
                }

                override fun onChartTranslate(me: MotionEvent?, dX: Float, dY: Float) {
                    Timber.d("onChartTranslate")
                }

                override fun onChartLongPressed(me: MotionEvent?) {
                    itemView.expanded_line_chart.isHighlightPerDragEnabled = true
                }
            }
        }
    }

    override fun getItemCount(): Int = baseItemsList.size

    companion object {
        fun formatCryptoAmount(amount: Float?, marketPrice: Boolean? = null): String {
            amount ?: return ""
            if (amount == 0.0f) return "0.0"

            val absAmount = amount.absoluteValue

            var divider = when {
                absAmount > 1000 -> ""
                absAmount >= 1 -> ".####"
                else -> ".########"
            }

            if (marketPrice != null && marketPrice) divider = ".#########"

            val symbols = DecimalFormatSymbols(Locale.ENGLISH)
            symbols.groupingSeparator = ','
//            val pattern = "#,###$divider"

            val symbolsa = DecimalFormatSymbols.getInstance()
            symbolsa.groupingSeparator = ','
            val formatter = DecimalFormat("###,###,###$divider", symbols)

//            val df = DecimalFormat(pattern, symbols)
            var res = formatter.format(amount)
            if(res[0] == '0')
                res.replaceFirst("0", "0.")
            return res
        }
    }

}
