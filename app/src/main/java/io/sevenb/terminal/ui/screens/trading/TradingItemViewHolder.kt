package io.sevenb.terminal.ui.screens.trading

import android.graphics.drawable.Drawable
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.github.mikephil.charting.data.CandleEntry
import com.github.mikephil.charting.data.Entry
import io.sevenb.terminal.R
import io.sevenb.terminal.data.model.trading.TradeChartParams
import io.sevenb.terminal.data.model.trading.TradeChartRange
import io.sevenb.terminal.ui.extension.load
import io.sevenb.terminal.ui.screens.order.OrderBottomSheetFragment
import io.sevenb.terminal.ui.screens.order.OrderViewModel
import io.sevenb.terminal.ui.screens.portfolio.AssetItem
import kotlinx.android.synthetic.main.item_trading_list_expand.view.*
import kotlinx.android.synthetic.main.item_trading_list_main.view.*

class TradingItemViewHolder(
    itemView: View
) : RecyclerView.ViewHolder(itemView) {

    fun bind(
        baseTradingListItem: BaseTradingListItem,
        openTrade: (Pair<String, OrderBottomSheetFragment.Companion.OrderSide>) -> Unit,
        baseCurrencyTicker: String,
        updateItemChartByPosition: (TradeChartParams) -> Unit,
        processFavorite: (TradingWalletItem) -> Unit,
        processAlert: (TradingWalletItem) -> Unit,
        position: Int,
        updateExpandSelectedRange: (Int, TradeChartRange) -> Unit,
        updateCandleChartData: (TradeChartParams) -> Unit,
        updateExpandedCandleChartData: (TradeChartParams) -> Unit
    ) {
        baseTradingListItem.bind(
            itemView,
            openTrade,
            baseCurrencyTicker,
            updateItemChartByPosition,
            position,
            updateExpandSelectedRange,
            updateCandleChartData,
            updateExpandedCandleChartData
        )

        when (baseTradingListItem) {
            is FavoriteTradeItem -> {
                itemView.ll_alert.isVisible = false
                itemView.ll_favorite.isVisible = false
            }
            is AssetItem -> {
                itemView.ll_alert.isVisible = false
                itemView.ll_favorite.isVisible = false
            }
            is TradingWalletItem -> {
                itemView.ll_alert.isVisible = true
                itemView.ll_favorite.isVisible = true

                val favoriteDrawable: Drawable? = if (baseTradingListItem.isFavorite) {
                    ContextCompat.getDrawable(
                        itemView.context,
                        R.drawable.ic_favorite_filled_star
                    )
                } else {
                    ContextCompat.getDrawable(itemView.context, R.drawable.ic_favorite_star)
                }

                itemView.img_icon_favorites.background = favoriteDrawable
                itemView.ll_favorite.setOnClickListener {
                    processFavorite(baseTradingListItem)
                }
                itemView.ll_alert.setOnClickListener {
                    processAlert(baseTradingListItem)
                }

            }

        }

    }

    fun bindChartData(
        baseTradingListItem: BaseTradingListItem,
        listEntries: List<Entry>,
        tradeChartRange: TradeChartRange
    ) {

        when (baseTradingListItem) {
            is TradingWalletItem -> {
                baseTradingListItem.bindChartData(
                    itemView,
                    listEntries,
                    tradeChartRange
                )
            }
        }

    }

    fun candleDataSetBytEntries(
        baseTradingListItem: BaseTradingListItem,
        listOfEntries: List<CandleEntry?>?
    ) {
        when (baseTradingListItem) {
            is TradingWalletItem -> {
                baseTradingListItem.candleDataSetBytEntries(
                    listOfEntries,
                    itemView
                )
            }
        }
    }

    fun bindPrice(
        baseTradingListItem: BaseTradingListItem,
        price: String
    ) {

        when (baseTradingListItem) {
            is TradingWalletItem -> {
                baseTradingListItem.bindPrice(
                    itemView,
                    price
                )
            }
        }

    }

}
