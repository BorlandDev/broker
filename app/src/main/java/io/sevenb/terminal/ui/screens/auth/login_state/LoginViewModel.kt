package io.sevenb.terminal.ui.screens.auth.login_state

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.sevenb.terminal.data.model.auth.ContinuesRegistrationObj
import io.sevenb.terminal.data.model.auth.UserData
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.enum_model.ErrorMessageType
import io.sevenb.terminal.data.model.enums.auth.NewAuthStates
import io.sevenb.terminal.domain.usecase.auth.*
import io.sevenb.terminal.domain.usecase.safety.AESCrypt
import io.sevenb.terminal.domain.usecase.safety.AccountSafetyStoring
import io.sevenb.terminal.domain.usecase.validate.CheckCredentials
import io.sevenb.terminal.utils.Logger
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.io.File
import javax.crypto.Cipher
import javax.inject.Inject

class LoginViewModel @Inject constructor(
    private val checkCredentials: CheckCredentials,
    private val signInUser: SignInUser,
    private val localUserDataStoring: LocalUserDataStoring,
    private val fingerPrintRestore: FingerPrintRestore,
    private val parseErrorMessage: ParseErrorMessage,
    private val storeFirstLaunch: StoreFirstLaunch,
    private val logOutUser: LogOutUser,
    private val safetyStoring: AccountSafetyStoring,
    private val aesCrypt: AESCrypt,
) : ViewModel() {

    val loginState = MutableLiveData<NewAuthStates>()
    val errorString = MutableLiveData<String>()
    val errorMessage = MutableLiveData<ErrorMessageType>()
    val decryptBiometricCipher = MutableLiveData<Cipher>()
    val encryptBiometricCipher = MutableLiveData<Cipher>()
    val focusEmail = MutableLiveData<Unit>()
    val continuesRegistrationObj = MutableLiveData<ContinuesRegistrationObj?>()
    val popupLocalAccountExists = MutableLiveData<Boolean>()
    val allLocalAccountDataRemoved = MutableLiveData<Unit>()
    val createPassCode = MutableLiveData<Boolean?>()

    var isFirstLaunch = false
    var localState = NewAuthStates.LOGIN
    var savedEmail = ""

    val userData = UserData()

    init {
        restoreFirstLaunch()
    }

    fun validateFields(email: String, password: String) =
        checkCredentials.checkCredentials(email, password)

    fun signInUser(email: String, password: String) {
        if (isItAnotherAccount(email)) {
            popupLocalAccountExists.value = true
            return
        }

        viewModelScope.launch {
            when (val signInResult = signInUser.execute(email, password)) {
                is Result.Success -> {
                    userData.email = email
                    userData.password = password

                    localUserDataStoring.apply {
                        updateLocalAccountData(email)
                        val encryptedPassword = aesCrypt.encrypt(password)
                        encryptedPassword?.let { storePassword(it) }
                    }

                    if (signInResult.data) {
                        changeCurrentState(NewAuthStates.TFA_VERIFICATION)
                    } else {
                        if (isFirstLaunch) {
                            changeCurrentState(NewAuthStates.TFA_CREATE)
                        } else {
                            authDone()
                        }
                    }
                }
                is Result.Error -> {
                    Logger.sendLog(
                        "LoginViewModel",
                        "signInUser",
                        signInResult.exception
                    )
                    signInResult.exception.message?.let {
                        when {
                            it.contains("INVALID_REQUEST_PARAMS") ||
                                    it.contains("bad_params") -> {
                                errorMessage.value = ErrorMessageType.LOGIN_ERROR
                            }
                            it.contains("unconfirmed_user") -> {
                                val continuesRegistration =
                                    ContinuesRegistrationObj(email, signInResult.message, password)

                                continuesRegistrationObj.value = continuesRegistration
                                this@LoginViewModel.errorMessage.value =
                                    ErrorMessageType.CONFIRMATION_REQUIRED
                            }
                            else -> {
                                errorString.value = "Server error: code 0"
                                return@launch
                            }
                        }

                    }
                    errorString.value = "Server error: code 0"
                }
            }
        }
    }

    fun authDone() {
        viewModelScope.launch {
            safetyStoring.storeLoggedOut(0)
            changeCurrentState(NewAuthStates.DONE_AUTH)
        }
    }

    fun checkFingerprintExistence() {
        viewModelScope.launch {
            val cipher = fingerPrintRestore.provideDecryptionCipher()
                ?: //                focusEmail.value = Unit
                return@launch
            try {
                decryptBiometricCipher.value = cipher
            } catch (e: IllegalStateException) {
            }
        }
    }

    fun decryptPassword(cipher: Cipher?) {
        viewModelScope.launch {
            val password = fingerPrintRestore.decryptPassword(cipher ?: return@launch)
            if (password != null) {
                if (password.isNotEmpty())
                    signInUser(savedEmail, password)
                else {
                    val clearBiometricAsync = async { localUserDataStoring.clearBiometry() }
                    val storeFirstLaunchAsync = async { storeFirstLaunch.setFirstLaunch(true) }
                    decryptPasswordError(clearBiometricAsync.await(), storeFirstLaunchAsync.await())
                }
            } else {
                errorString.value = "Login error: code 01"
            }
        }
    }

    private fun decryptPasswordError(await: Result<Unit>, await1: Result<Unit>) {
        errorString.value =
            "You may have changed your fingerprint. Current fingerprint in this app will be removed"
    }

    private fun restoreFirstLaunch() {
        runBlocking {
            val result = storeFirstLaunch.getFirstLaunch()
            if (result is Result.Success) {
                isFirstLaunch = result.data
            }
        }
    }

    fun changeCurrentState(state: NewAuthStates) {
        localState = state
        loginState.value = state
    }

    private fun isItAnotherAccount(signInEmail: String): Boolean {
        if (savedEmail.isEmpty()) return false
        return savedEmail != signInEmail
    }

    fun clearAllLocalAccountData(avatarFilePath: String) {
        viewModelScope.launch {
//            val logOutUserResult = logOutUser.execute()
            logOutUser.execute() //TODO CHECK IF NECESSARY
            val localDataResult = localUserDataStoring.deleteAllLocalUserData()

            val avatarFile: File? = File(avatarFilePath)
            avatarFile?.delete()

            savedEmail = ""

            if (localDataResult is Result.Success) {
                allLocalAccountDataRemoved.value = Unit
            } else {
                errorString.value = "Clear data error: 0"
            }
        }
    }

}