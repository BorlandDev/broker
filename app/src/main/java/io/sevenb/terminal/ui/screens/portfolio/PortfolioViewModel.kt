package io.sevenb.terminal.ui.screens.portfolio

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.github.mikephil.charting.data.Entry
import io.sevenb.terminal.BuildConfig
import io.sevenb.terminal.data.datasource.database.model.CurrencyEntity
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.enum_model.HistoryType
import io.sevenb.terminal.data.model.enums.PortfolioTabs
import io.sevenb.terminal.data.model.order.OrderItem
import io.sevenb.terminal.data.model.trading.CandleChartParams
import io.sevenb.terminal.data.model.trading.TradeChartParams
import io.sevenb.terminal.data.model.trading.TradeChartRange
import io.sevenb.terminal.data.repository.auth.PreferenceRepository
import io.sevenb.terminal.data.repository.auth.UserAuthRepository
import io.sevenb.terminal.data.repository.database.DbRepository
import io.sevenb.terminal.domain.usecase.auth.LocalUserDataStoring
import io.sevenb.terminal.domain.usecase.auth.StoreInfoConfirmationState
import io.sevenb.terminal.domain.usecase.charts.AccountData
import io.sevenb.terminal.domain.usecase.charts.AccountData.Companion.getBaseCurrencyTicker
import io.sevenb.terminal.domain.usecase.charts.CandlesData
import io.sevenb.terminal.domain.usecase.charts.ChartData
import io.sevenb.terminal.domain.usecase.charts.ChartDataHandler
import io.sevenb.terminal.domain.usecase.deposit.ClipboardCopy
import io.sevenb.terminal.domain.usecase.historyfilter.HistoryFilterProcessing
import io.sevenb.terminal.domain.usecase.order.MarketRate
import io.sevenb.terminal.domain.usecase.order.MarketsData
import io.sevenb.terminal.domain.usecase.order.OrderOperations
import io.sevenb.terminal.domain.usecase.processing_opened_orders.ProcessingOpenedOrder
import io.sevenb.terminal.ui.screens.trading.TradingWalletItem
import io.sevenb.terminal.utils.ConnectionStateMonitor
import io.sevenb.terminal.utils.Logger
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject
import android.util.Log


class PortfolioViewModel @Inject constructor(
    private val accountData: AccountData,
    private val orderOperations: OrderOperations,
    private val localUserDataStoring: LocalUserDataStoring,
    private val marketRate: MarketRate,
    private val chartData: ChartData,
    private val storeInfoConfirmationState: StoreInfoConfirmationState,
    private val marketsData: MarketsData,
    private val clipboardCopy: ClipboardCopy,
    private val candlesData: CandlesData,
    private val historyFilerProcessing: HistoryFilterProcessing,
    private val chartDataHandler: ChartDataHandler,
    private val processingOpenedOrder: ProcessingOpenedOrder,
    val dbRepository: DbRepository
) : ViewModel() {

    val totalHolding = MutableLiveData<String>()
    val lockedBalance = MutableLiveData<String>()
    val availableBalance = MutableLiveData<String>()
    val balancesList = MutableLiveData<List<AssetItem>>()
    val orderOpened = MutableLiveData<List<OrderItem>>()
    val cancelOrder = MutableLiveData<OrderItem>()
    val historyListItems = MutableLiveData<List<BaseHistoryItem>>()
    val snackbarMessage = MutableLiveData<String>()
    val showInfoDialog = MutableLiveData<Boolean>()
    val totalInBaseCurrency = MutableLiveData<String>()
    val chartDataResult = MutableLiveData<Pair<TradeChartParams, List<Entry>>>()
    val currencies = MutableLiveData<CurrencyEntity>()
//    val showRateUs = MutableLiveData<Boolean>()

    val updateCandleChartData = MutableLiveData<Int>()

    var tab = PortfolioTabs.ASSETS

    val updateAssets = MutableLiveData<Unit>()
    val rate = MutableLiveData<Float>()

    private var showTimeError = 0L

    init {
        subscribeUi()

        updateData()
        viewModelScope.launch {
            accountData.updateFlow.collect {
                println()
            }
        }
//        showRateUs(false)
    }

    private fun updateData() {
        accountData.cancelUpdateAssetsFlowJobJob()
        accountData.updateAssetsFlowJob = viewModelScope.launch {
            accountData.updateAssetsFlow.collect {
                val currentTime = System.currentTimeMillis()
                if (currentTime - it >= 5000) {
                    val accountBalances = async { accountBalanceList() }
                    val marketData = async { initMarketsData() }
//
                    updateAssetList(accountBalances.await(), marketData.await(), currentTime)
//                    initMarketsData()
                }
            }
        }
    }

//    fun showRateUs(fromBack: Boolean) {
//        viewModelScope.launch {
//            val isInAppReviewEnabled =
//                localUserDataStoring.isInAppReviewEnabled() as? Result.Success
//                    ?: return@launch
//
//            if (fromBack) {
//                showRateUs.postValue(isInAppReviewEnabled.data)
//            } else {
//                val isShowRateUsOnPortfolio =
//                    localUserDataStoring.isShowRateUsOnPortfolio() as? Result.Success
//                        ?: return@launch
//                if (isShowRateUsOnPortfolio.data) {
//                    showRateUs.postValue(isInAppReviewEnabled.data)
//                }
//            }
//        }
//    }

//    fun disableRateUs() {
//        viewModelScope.launch {
//            localUserDataStoring.disableRateUs()
//        }
//    }

    private fun updateAssetList(unit: Unit, secUnit: Unit, currentTime: Long) {
        accountData.updateAssetList(currentTime)
    }

    private fun initMarketsData() {
        viewModelScope.launch {
            marketsData.cacheFirstMarkets()
        }
    }

    @SuppressLint("LogNotTimber")
    private fun subscribeUi() {
        accountData.cancelPortfolioTotalBalanceJob()
        accountData.portfolioTotalBalanceFlowJob = viewModelScope.launch {
            accountData.totalBalanceFlow.collect {
                totalHolding.value = it.totalHoldingBalance
                totalInBaseCurrency.value = it.totalInBaseCurrency
                marketRate()
                lockedBalance.value = it.onOrdersBalance
                availableBalance.value = it.availableBalance
                localUserDataStoring.updateLocalAccountData(
                    totalHoldingBalance = it.totalHoldingBalance,
                    availableBalance = it.availableBalance,
                    onOrdersBalance = it.onOrdersBalance
                )
            }
        }

        accountData.cancelBalancesListFlowJobJob()
        accountData.balancesListFlowJob = viewModelScope.launch {
            accountData.balancesListFlow.collect { listAsset ->
//                if (!accountData.emptyBalancesAfterLogOut)
                balancesList.value = listAsset
            }
        }

        accountData.cancelHistoryFlowJobJob()
        accountData.historyFlowJob = viewModelScope.launch {
            accountData.historyListFlow.collect {
                historyListItems.value = it
            }
        }
    }

    fun makeRequestsByFilter(historyTypes: MutableList<HistoryType>, list: List<CurrencyEntity>) {
        viewModelScope.launch {
            val filteredHistoryData = historyFilerProcessing.getFilteredHistory(historyTypes, list)

            if (filteredHistoryData.second.isEmpty())
                accountData.updateHistoryList(filteredHistoryData.first.toMutableList())

            if (filteredHistoryData.second.isNotEmpty()) {
                when {
                    !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                    else -> {
                        var errorString = ""
                        filteredHistoryData.second.map { errorString += "$it\n" }
                        snackbarMessage.value = errorString
                    }
                }
            }
        }
    }

    private fun showNoInternetConnection() {
        val currentTime = System.currentTimeMillis()
        if (currentTime - showTimeError >= 1500) {
            snackbarMessage.value = ConnectionStateMonitor.noInternetError
            showTimeError = currentTime
        }
    }

    fun getPositionByTicker(ticker: String, list: List<TradingWalletItem>) =
        accountData.getPositionByTicker(ticker, list)

    fun updateCandleChartData(tradeChartParams: TradeChartParams) {
        viewModelScope.launch {
            val symbol = "${tradeChartParams.ticker}${getBaseCurrencyTicker()}"
            val candleChartParams =
                CandleChartParams(
                    tradeChartParams.listPosition,
                    tradeChartParams.tradeChartRange,
                    symbol = symbol
                )

            val listCandles = candlesData.requestCandleData(candleChartParams)

            accountData.updateCandleChartData(tradeChartParams.listPosition, listCandles)
            updateCandleChartData.value = tradeChartParams.listPosition
        }
    }

    fun updateItemChartData(ticker: String, tradeChartParams: TradeChartParams) {
        viewModelScope.launch {
            when (val currenciesResult =
                chartData.requestChartData(
                    ticker,
                    PreferenceRepository.baseCurrencyTicker,
                    tradeChartParams
                )) {
                is Result.Success -> {
                    if (tradeChartParams.tradeChartRange == TradeChartRange.DAY) {
                        accountData.updateChartParamsByTicker(
                            tradeChartParams.ticker,
                            currenciesResult.data
                        )
                        accountData.updateExpandedChartParamsByTicker(
                            tradeChartParams.ticker,
                            currenciesResult.data
                        )
                    } else {
                        accountData.updateExpandedChartParamsByTicker(
                            tradeChartParams.ticker,
                            currenciesResult.data
                        )
                    }
                    chartDataResult.value = Pair(tradeChartParams, currenciesResult.data)
                }
                is Result.Error -> {
                    when {
                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                        else -> snackbarMessage.value = currenciesResult.exception.message
                    }
                }
            }
        }
    }

    fun accountBalanceList() {
        viewModelScope.launch {
            when (val balancesResult = accountData.mapAccountAssets()) {
                is Result.Success -> {
                    if (UserAuthRepository.accessToken.isEmpty()) {
                        offerBalanceFlow(emptyList())
                    } else {
                        updateAssets.value = Unit
                    }
                }
                is Result.Error -> {
                    Logger.sendLog(
                        "PortfolioViewModel",
                        "accountBalanceList",
                        balancesResult.exception
                    )
                    when {
                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                        else -> snackbarMessage.value =
                            if (balancesResult.message.isEmpty()) balancesResult.exception.message
                            else balancesResult.message
                    }
                }
            }
        }
    }

    private fun offerBalanceFlow(balances: List<AssetItem>) {
        accountData.balancesListChannel.offer(balances)
    }

    fun marketRate() {
        viewModelScope.launch {
            when (val rateResult =
                marketRate.execute(AccountData.BTC, getBaseCurrencyTicker())) {
                is Result.Success -> {
                    val marketRateResult = rateResult.data
                    rate.value = marketRateResult.rate ?: 0.0f
                }
                is Result.Error -> {
                    when {
                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                        else -> snackbarMessage.value = rateResult.exception.message
                    }
                }
            }
        }
    }

    fun orderOpened(list: List<CurrencyEntity>) {
        viewModelScope.launch {
            when (val orderOpenedResult = orderOperations.orderOpened()) {
                is Result.Success -> {
                    when (val processingResult =
                        processingOpenedOrder.processOrders(orderOpenedResult.data, list)) {
                        is Result.Success -> orderOpened.value = processingResult.data
                        is Result.Error -> println()
                    }
//                    orderOpened.value = orderOpenedResult.data
                }
                is Result.Error -> {
                    when {
                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                        else -> snackbarMessage.value = "Can't get order list"
                    }
                }
            }
        }
    }

    fun cancelOrder(orderItem: OrderItem) {
        viewModelScope.launch {
            when (val cancelOrderResult = orderOperations.cancelOrder(orderItem)) {
                is Result.Success -> {
                    accountBalanceList()
                    cancelOrder.value = cancelOrderResult.data
                }
                is Result.Error -> {
                    when {
                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                        else -> snackbarMessage.value = cancelOrderResult.exception.message
                    }
                }
            }
        }
    }

    fun tryToShowInfoConfirmation() {
        viewModelScope.launch {
            val result = storeInfoConfirmationState.restore()
            if (result is Result.Success) {
                val isShowDialog = result.data
                if (isShowDialog) showInfoDialog.value = true
            }
        }
    }

    fun storeInfoConfirmationDialogState() {
        viewModelScope.launch {
            storeInfoConfirmationState.execute()
        }
    }

//    private fun totalHolding() {
//        viewModelScope.launch {
//            accountData.totalHolding()
//            if (accountData.accountApi.listOfBalances.isEmpty()) {
//                offerBalanceFlow(listOf())
//            }
//        }
//    }

    fun copyToClipboard(text: String) {
        viewModelScope.launch {
            clipboardCopy.copyToClipboard(text)
        }
    }

//    fun resetRateUs() {
//        viewModelScope.launch {
//            localUserDataStoring.resetLastRateUs()
//        }
//    }

    fun setLastRateTime() {
        viewModelScope.launch {
            localUserDataStoring.setLastRateTime(null)
        }
    }

    fun setDontAskVersion() {
        viewModelScope.launch {
            localUserDataStoring.setDontAskVersion(BuildConfig.VERSION_CODE)
        }
    }

    fun setAskLaterTime() {
        viewModelScope.launch {
            localUserDataStoring.setAskLaterTime(Date().time.toString())
        }
    }

//    fun showInAppReview() : Boolean{
//        viewModelScope.launch {
//          return true
//        }
//    }
}