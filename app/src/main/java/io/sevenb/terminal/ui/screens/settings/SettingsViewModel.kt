package io.sevenb.terminal.ui.screens.settings

import android.graphics.Bitmap
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.sevenb.terminal.data.model.broker_api.account.UserSettings
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.messages.NotificationsSettingsRequest
import io.sevenb.terminal.data.model.portfolio.LocalAccountData
import io.sevenb.terminal.data.repository.auth.UserAuthRepository
import io.sevenb.terminal.domain.usecase.auth.LocalUserDataStoring
import io.sevenb.terminal.domain.usecase.auth.LogOutUser
import io.sevenb.terminal.domain.usecase.auth.StorePassword
import io.sevenb.terminal.domain.usecase.charts.AccountData
import io.sevenb.terminal.domain.usecase.charts.AccountData.Companion.isLoggedOut
import io.sevenb.terminal.domain.usecase.notification_handler.NotificationHandler
import io.sevenb.terminal.domain.usecase.safety.AESCrypt
import io.sevenb.terminal.domain.usecase.safety.AccountSafetyStoring
import io.sevenb.terminal.domain.usecase.safety.PassCodeStoring
import io.sevenb.terminal.domain.usecase.settings.StoreDefaultCurrency
import io.sevenb.terminal.domain.usecase.tfa.TfaHandler
import io.sevenb.terminal.domain.usecase.verification.VerificationData
import io.sevenb.terminal.domain.usecase.withdrawal.EstimatedDailyLimit
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.crypto.Cipher
import javax.inject.Inject

class SettingsViewModel @Inject constructor(
    private val accountData: AccountData,
    private val verificationData: VerificationData,
    private val logOutUser: LogOutUser,
    private val localUserDataStoring: LocalUserDataStoring,
    private val storeDefaultCurrency: StoreDefaultCurrency,
    private val estimatedDailyLimit: EstimatedDailyLimit,
    private val safetyStoring: AccountSafetyStoring,
    private val aesCrypt: AESCrypt,
    private val passCodeStoring: PassCodeStoring,
    private val storePassword: StorePassword,
    private val notificationHandler: NotificationHandler,
    private val tfaHandler: TfaHandler
) : ViewModel() {

    val accountInfo = MutableLiveData<LocalAccountData>()
    val localAccountDataResult = MutableLiveData<LocalAccountData>()
    val dailyLimit = MutableLiveData<String>()
    val verificationToken = MutableLiveData<String>()
    val userLogOutDone = MutableLiveData<Boolean>()
    val userAvatarBitmap = MutableLiveData<Bitmap>()
    val snackbarMessage = MutableLiveData<String>()
    val biometricEnabled = MutableLiveData<Boolean?>()
    val passCodeEnabled = MutableLiveData<Boolean>()
    val passCodeCreatingRequired = MutableLiveData<Boolean?>()
    val fingerprintCreatingRequired = MutableLiveData<Boolean?>()
    val createFingerprint = MutableLiveData<Cipher?>()
    val unavailableCreateFingerprint = MutableLiveData<Boolean>()
    val navigateToRemovePassCode = MutableLiveData<Boolean?>()
    val requestPinApprove = MutableLiveData<Boolean?>()
    val showPopup = MutableLiveData<Boolean?>()
    val notificationsIsEnabled = MutableLiveData<Boolean>()

    val userSettings = MutableLiveData<UserSettings>()
    val showRateUs = MutableLiveData<Boolean>()

    var accountDataInfo: LocalAccountData? = null

    var dailyWithdrawalLimit: String? = null
    private var localBiometricEnabledValue = false
    private var localPinEnabledValue = false

    //    val mainNotifications = mutableSetOf<MainNotificationsPush>()
//    var enabledNotificationsList = mutableListOf<TemplateResponse>()
//    val enabledNotificationsListValue = MutableLiveData<MutableList<TemplateResponse>>()

    var tfa = false
    var email = ""
    var password = ""

    init {
        loadEmail()
        loadPinEnabled()
        loadBiometricEnabled()
        getDailyWithdrawalLimit()
        getAccountData()
        requestDailyLimitEstimated(AccountData.BTC)
        getUserSettings()
    }

    fun loadEmail() {
        email = localUserDataStoring.localAccountData.email
        viewModelScope.launch {
            when (val passwordResult = localUserDataStoring.restorePassword()) {
                is Result.Success -> {
                    val encryptedPassword = passwordResult.data
                    password = aesCrypt.decrypt(encryptedPassword)
                }
            }
        }
    }

    fun loadPinEnabled() {
        viewModelScope.launch {
            when (val pinEnabledResult = safetyStoring.restorePassCodeEnabled()) {
                is Result.Success -> pinValue(pinEnabledResult.data)
                is Result.Error -> {
                    pinValue(false)
                    snackbarMessage.value = "Pin is not stored"
                }
            }
        }
    }

    fun loadBiometricEnabled() {
        viewModelScope.launch {
            when (val biometricEnabledResult = safetyStoring.restoreFingerprintEnabled()) {
                is Result.Success -> biometricValue(biometricEnabledResult.data)
                is Result.Error -> {
                    biometricValue(false)
                    snackbarMessage.value = "Biometric is not stored"
                }
            }
        }
    }

    fun showRateUs() {
        viewModelScope.launch {
            val showInAppReview =
                localUserDataStoring.isInAppReviewEnabled() as? Result.Success
                    ?: return@launch
            showRateUs.postValue(showInAppReview.data)
        }
    }

//    fun disableRateUs() {
//        viewModelScope.launch {
//            localUserDataStoring.disableRateUs()
//        }
//    }
//
//    fun enableRateUs() {
//        viewModelScope.launch {
//            localUserDataStoring.enableRateUs()
//        }
//    }

    fun setLastRateTime() {
        viewModelScope.launch {
            localUserDataStoring.setLastRateTime(null)
        }
    }

    fun processEnablingFingerprint(value: Boolean) {
        if (value) {
            if (localPinEnabledValue) fingerprintCreatingRequired.value = true
            else showPopup.value = true
        } else {
            requestPinApprove.value = true
        }
    }

    fun removeFingerprint() {
        storeFingerprint(false)
        biometricValue(false)
    }

    private fun sendPassCodeCreatingValue(value: Boolean) {
        passCodeCreatingRequired.value = value
    }

    private fun storeFingerprint(value: Boolean) {
        viewModelScope.launch {
            safetyStoring.storeFingerprintEnabled(value)
        }
    }

    fun processPassCodeEnabling(value: Boolean?) {
        value?.let {
            if (it) sendPassCodeCreatingValue(it)
            else navigateToRemovePassCode.value = true
        }
    }

    fun biometricValue(value: Boolean?) {
        value?.let {
            localBiometricEnabledValue = value
            biometricEnabled.value = value
        }
    }

    fun pinValue(value: Boolean?) {
        value?.let {
            localPinEnabledValue = value
            passCodeEnabled.value = value
        }
    }

    private fun getAccountData() {
        viewModelScope.launch {
            accountData.accountInfoFlow.collect {
                accountInfo.value = it
                accountDataInfo = it
            }
        }
    }

//    fun resetRateUs() {
//        viewModelScope.launch {
//            localUserDataStoring.resetLastRateUs()
//        }
//    }

    private fun getDailyWithdrawalLimit() {
        viewModelScope.launch {
            accountData.dailyWithdrawalLimitFlow.collect {
                dailyWithdrawalLimit = it
                dailyLimit.value = it
            }
        }
    }

    fun accountInfo() {
        if (accountDataInfo == null) {
            viewModelScope.launch {
                when (val accountInfoResult = verificationData.accountInfo()) {
                    is Result.Success -> {
                        val data = LocalAccountData(
                            phone = "",
                            email = accountInfoResult.data.email,
                            status = accountInfoResult.data.status,
                            tfa = accountInfoResult.data.userSettings?.tfa
                        )
                        accountData.accountInfoChannel.offer(data)
//                        accountInfo.value = data
                    }
                    is Result.Error -> {
                        restoreLocalAccountData()
                        Timber.e("getting accountInfo error")
                    }
                }
            }
        }
    }

    fun getUserSettings() {
        viewModelScope.launch {
//            when (val userSettingsResult = notificationHandler.getUserSettings<UserSettings>()) {
            when (val userSettingsResult = notificationHandler.getUserSettings()) {
                is Result.Success -> {
                    val response = userSettingsResult.data.response // .data.user_settings
                    response?.let {
                        userSettings.value = it
                        notificationsIsEnabled.value = false
                        it.push.map {
                            if (it.is_enabled && it.template.template_type != null)
                                notificationsIsEnabled.value = it.is_enabled
                        }
                        tfa = it.tfa
                    }
                }
                is Result.Error -> snackbarMessage.value = userSettingsResult.message
            }
        }
    }

    fun toggleAllNotifications(value: Boolean) {
        viewModelScope.launch {
            userSettings.value?.let {

                val list = mutableListOf<NotificationsSettingsRequest>()
                list.add(NotificationsSettingsRequest(1, 1, value))
                list.add(NotificationsSettingsRequest(2, 1, value))
                list.add(NotificationsSettingsRequest(3, 1, value))


                when (val settingsResult = notificationHandler.put(list)) {
                    is Result.Success -> {
                        getUserSettings()
                        notificationsIsEnabled.value = value
                    }
                    is Result.Error -> {
                        snackbarMessage.value = settingsResult.message
                    }
                }
            }
        }
    }

//    fun toggleTfa(value: Boolean) {
//        viewModelScope.launch {
//            when (val enableResult = twoFaHandler.toggleTfa(value)) {
//                is Result.Success -> {
//                    requestUserSettings()
////                    if (enableResult.data.result) {
////                        enableTwoFa.value = value
////                    }
//                }
//                is Result.Error -> snackbarMessage.value = enableResult.message
//            }
//        }
//    }

//    fun restoreEnableTwoFa() {
//        viewModelScope.launch {
//            when (val restoreResult = tfaHandler.twoFaIsEnabled()) {
//                is Result.Success -> restoredTwoFa.value = restoreResult.data.result
//                is Result.Error -> snackbarMessage.value = restoreResult.message
//            }
//        }
//    }

    private fun restoreLocalAccountData() {
        viewModelScope.launch {
            when (val accountResult = localUserDataStoring.restoreLocalAccountData()) {
                is Result.Success -> {
//                    val accountData = accountResult.data
                    accountData.accountInfoChannel.offer(accountResult.data)
//                    accountInfo.value = accountData
                }
                is Result.Error -> Timber.e("localUserDataStoring.restoreLocalAccountData() error=${accountResult.exception}")
            }
        }
    }

    private fun requestDailyLimitEstimated(ticker: String) {
        if (dailyWithdrawalLimit == null) {
            viewModelScope.launch {
                if (UserAuthRepository.accessToken.isNotEmpty())
                    when (val dailyLimitResult = estimatedDailyLimit.execute(AccountData.BTC)) {
                        is Result.Success -> {
                            accountData.dailyWithdrawalLimitChannel.offer(dailyLimitResult.data)
                        }
                    }
            }
        }
    }

    fun getVerifications() {
        viewModelScope.launch {
            when (val getVerificationsResult = verificationData.gerVerifications()) {
                is Result.Success -> {
                    val listOfVerifications = getVerificationsResult.data
                    if (listOfVerifications.isNullOrEmpty()) {
                        createVerification()
                    } else {
                        requestVerification()
                    }
                }
                is Result.Error -> snackbarMessage.value = getVerificationsResult.exception.message
            }
        }
    }

    private fun createVerification() {
        viewModelScope.launch {
            when (val createVerificationResult = verificationData.createVerification()) {
                is Result.Success -> {
                    val requestVerificationToken = createVerificationResult.data.token
                    verificationToken.value = requestVerificationToken
                }
                is Result.Error -> {
                    // FIXME: verification creates on the server side when SumSub sends request to it's
                    //  webhook. So we need to check 400 code with "bad params" message to be sure that
                    //  verification was created and go to refresh token
                    val message = createVerificationResult.message
                    if (message.contains("{\"message\":\"Cannot request verification\",\"error\":\"bad_params\"}")) {
                        requestVerification()
                    } else {
                        snackbarMessage.value = createVerificationResult.exception.message
                    }
                }
            }
        }
    }

    private fun requestVerification() {
        viewModelScope.launch {
            when (val refreshTokenResult = verificationData.refreshToken()) {
                is Result.Success -> {
                    val requestVerificationToken = refreshTokenResult.data.token
                    verificationToken.value = requestVerificationToken
                }
                is Result.Error -> snackbarMessage.value = refreshTokenResult.exception.message
            }
        }
    }

    suspend fun refreshVerificationToken(): String? {
        return when (val verificationResult = verificationData.refreshToken()) {
            is Result.Success -> {

                val refreshedToken = verificationResult.data.token

                localUserDataStoring.updateLocalAccountData(
                    verificationToken = refreshedToken
                )

                refreshedToken
            }
            is Result.Error -> null
        }
    }

    fun accountLogOut() {
        viewModelScope.launch {
            val logOutUserResult = logOutUser.execute()

            if (logOutUserResult is Result.Success) {
                accountData.apply {

//                    clearDb()
                    cancelBalancesListFlowJobJob()

//                    localUserDataStoring.enableRateUs()
//                    localUserDataStoring.resetLastRateUs()
                    localUserDataStoring.setLastRateTime("0")

                    accountApi.apply {
                        localAccountData.totalInBaseCurrency = ""
                        localAccountData.totalHoldingBalance = ""
                        localAccountData.availableBalance = ""
                        localAccountData.onOrdersBalance = ""
                        localAccountData.balance = null
                        listOfBalances = listOf()
                    }
                    tradingItems = mutableListOf()

                    totalBalanceChannel = BroadcastChannel(Channel.CONFLATED)
                    totalBalanceFlow = accountData.totalBalanceChannel.asFlow()

                    balancesListChannel = BroadcastChannel(Channel.CONFLATED)
                    balancesListFlow = accountData.balancesListChannel.asFlow()
//                    emptyBalancesAfterLogOut = true

                    historyListChannel = BroadcastChannel(Channel.CONFLATED)
                    historyListFlow = accountData.historyListChannel.asFlow()

                    isLoggedOut = true

                    balancesListChannel.offer(emptyList())

                    accountInfoChannel = BroadcastChannel(Channel.CONFLATED)
                    accountInfoFlow = accountData.accountInfoChannel.asFlow()
                }

                userLogOutDone.value = true
            } else {
                snackbarMessage.value = "Log out error"
            }
        }
    }

    fun storeDefaultCurrency(ticker: String) {
        requestDailyLimitEstimated(ticker)
        viewModelScope.launch {
            storeDefaultCurrency.execute(ticker)
        }
    }

    fun createFingerprint() {
        val cipherPair = storePassword.provideEncryptionCipher()

        if (cipherPair.first != null && !storePassword.isBiometricSuccess()) {
            unavailableCreateFingerprint.value = true
        } else
            createFingerprint.value = cipherPair.second
    }

    fun encryptPassword(cipher: Cipher?) {
        viewModelScope.launch {
            if (password.isEmpty()) return@launch

            val encryptionResult = storePassword.encryptPassword(
                password, cipher ?: return@launch
            )
            when (encryptionResult) {
                is Result.Success -> {
                    safetyStoring.storeFingerprintEnabled(true)
                    biometricValue(true)
                }
                is Result.Error -> snackbarMessage.value = "Fingerprint creating error"
            }
        }
    }

//    private fun getNotificationResponse(type: NotificationsTypeFromBack): MainNotificationsPush {
//        var tempPush: MainNotificationsPush? = null
//        mainNotifications.forEach {
//            if (it.template.template_type == type) {
//                tempPush = it
//            }
////            tempResponse = when (it.template.template_type) {
////                NotificationsTypeFromBack.WITHDRAW_COMPLETE -> it
////                NotificationsTypeFromBack.DEPOSIT_COMPLETE -> it
////                NotificationsTypeFromBack.ORDER_COMPLETE -> it
////            }
//        }
//        return tempPush!!
//    }

}
