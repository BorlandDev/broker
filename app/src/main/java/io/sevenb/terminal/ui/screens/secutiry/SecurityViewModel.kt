package io.sevenb.terminal.ui.screens.secutiry

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.tfa.TfaResponse
import io.sevenb.terminal.domain.usecase.auth.LocalUserDataStoring
import io.sevenb.terminal.domain.usecase.auth.StorePassword
import io.sevenb.terminal.domain.usecase.safety.AESCrypt
import io.sevenb.terminal.domain.usecase.safety.AccountSafetyStoring
import io.sevenb.terminal.domain.usecase.tfa.TfaHandler
import kotlinx.coroutines.launch
import javax.crypto.Cipher
import javax.inject.Inject

class SecurityViewModel @Inject constructor(
    private val localUserDataStoring: LocalUserDataStoring,
    private val aesCrypt: AESCrypt,
    private val safetyStoring: AccountSafetyStoring,
    private val storePassword: StorePassword,
    private val tfaHandler: TfaHandler
) : ViewModel() {

    val snackbarMessage = MutableLiveData<String>()
    val biometricEnabled = MutableLiveData<Boolean?>()
    val passCodeEnabled = MutableLiveData<Boolean>()
    val passCodeCreatingRequired = MutableLiveData<Boolean?>()
    val fingerprintCreatingRequired = MutableLiveData<Boolean?>()
    val createFingerprint = MutableLiveData<Cipher?>()
    val unavailableCreateFingerprint = MutableLiveData<Boolean>()
    val navigateToRemovePassCode = MutableLiveData<Boolean?>()
    val requestPinApprove = MutableLiveData<Boolean?>()
    val showPopup = MutableLiveData<Boolean?>()

    val tfaEnabled = MutableLiveData<TfaResponse>()

    private var localBiometricEnabledValue = false
    private var localPinEnabledValue = false

    var tfa = false
    var email = ""
    var password = ""

    init {
        loadEmail()
        loadPinEnabled()
        loadBiometricEnabled()
    }

    fun loadEmail() {
        email = localUserDataStoring.localAccountData.email
        viewModelScope.launch {
            when (val passwordResult = localUserDataStoring.restorePassword()) {
                is Result.Success -> {
                    val encryptedPassword = passwordResult.data
                    password = aesCrypt.decrypt(encryptedPassword)
                }
            }
        }
    }

    fun loadPinEnabled() {
        viewModelScope.launch {
            when (val pinEnabledResult = safetyStoring.restorePassCodeEnabled()) {
                is Result.Success -> pinValue(pinEnabledResult.data)
                is Result.Error -> {
                    pinValue(false)
                    snackbarMessage.value = "Pin is not stored"
                }
            }
        }
    }

    fun loadBiometricEnabled() {
        viewModelScope.launch {
            when (val biometricEnabledResult = safetyStoring.restoreFingerprintEnabled()) {
                is Result.Success -> biometricValue(biometricEnabledResult.data)
                is Result.Error -> {
                    biometricValue(false)
                    snackbarMessage.value = "Biometric is not stored"
                }
            }
        }
    }

    fun processEnablingFingerprint(value: Boolean) {
        if (value) {
            if (localPinEnabledValue) fingerprintCreatingRequired.value = true
            else showPopup.value = true
        } else {
            requestPinApprove.value = true
        }
    }

    fun removeFingerprint() {
        storeFingerprint(false)
        biometricValue(false)
    }

    private fun sendPassCodeCreatingValue(value: Boolean) {
        passCodeCreatingRequired.value = value
    }

    private fun storeFingerprint(value: Boolean) {
        viewModelScope.launch {
            safetyStoring.storeFingerprintEnabled(value)
        }
    }

    fun processPassCodeEnabling(value: Boolean?) {
        value?.let {
            if (it) sendPassCodeCreatingValue(it)
            else navigateToRemovePassCode.value = true
        }
    }

    fun biometricValue(value: Boolean?) {
        value?.let {
            localBiometricEnabledValue = value
            biometricEnabled.value = value
        }
    }

    fun pinValue(value: Boolean?) {
        value?.let {
            localPinEnabledValue = value
            passCodeEnabled.value = value
        }
    }

    fun createFingerprint() {
        val cipherPair = storePassword.provideEncryptionCipher()

        if (cipherPair.first != null && !storePassword.isBiometricSuccess()) {
            unavailableCreateFingerprint.value = true
        } else
            createFingerprint.value = cipherPair.second
    }

    fun encryptPassword(cipher: Cipher?) {
        viewModelScope.launch {
            if (password.isEmpty()) return@launch

            val encryptionResult = storePassword.encryptPassword(
                password, cipher ?: return@launch
            )
            when (encryptionResult) {
                is Result.Success -> {
                    safetyStoring.storeFingerprintEnabled(true)
                    biometricValue(true)
                }
                is Result.Error -> snackbarMessage.value = "Fingerprint creating error"
            }
        }
    }

    fun toggleTfa() {
        viewModelScope.launch {
            when (val tfaResult = tfaHandler.toggleTfa()) {
                is Result.Success -> {
                    tfaEnabled.value = tfaResult.data
                }
                is Result.Error -> snackbarMessage.value = tfaResult.message
            }
        }
    }
}