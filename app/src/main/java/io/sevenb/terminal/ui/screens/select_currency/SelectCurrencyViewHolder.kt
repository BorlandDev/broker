package io.sevenb.terminal.ui.screens.select_currency

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import io.sevenb.terminal.data.model.settings.DefaultCurrencyItem
import io.sevenb.terminal.data.model.withdrawal.WithdrawItem
import io.sevenb.terminal.ui.screens.portfolio.AssetItem
import io.sevenb.terminal.ui.screens.trading.FavoriteTradeItem
import io.sevenb.terminal.ui.screens.trading.TradingWalletItem

class SelectCurrencyViewHolder(
    itemView: View
) : RecyclerView.ViewHolder(itemView) {

    fun bind(baseTradingListItem: Any, baseCurrency: String, itemSelected: (BaseSelectItem) -> Unit) {
        when(baseTradingListItem) {
            is DepositCurrencyItem -> {
                baseTradingListItem.bind(itemView, baseCurrency, itemSelected)
            }
            is WithdrawItem -> {
                baseTradingListItem.bind(itemView, baseCurrency, itemSelected)
            }
            is TradingWalletItem -> {
                baseTradingListItem.bind(itemView, baseCurrency, itemSelected)
            }
            is FavoriteTradeItem -> {
                baseTradingListItem.bind(itemView, baseCurrency, itemSelected)
            }
            is AssetItem -> {
                baseTradingListItem.bind(itemView, baseCurrency, itemSelected)
            }
            is DefaultCurrencyItem -> {
                baseTradingListItem.bind(itemView, baseCurrency, itemSelected)
            }
        }

    }

}
