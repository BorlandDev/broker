package io.sevenb.terminal.ui.extension

//import androidx.window.layout.WindowMetricsCalculator
import android.graphics.Insets
import android.graphics.Point
import android.view.WindowInsets
import android.view.WindowManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

fun BottomSheetDialogFragment.getHeight(): Int {
//    val windowMetrics =
//        WindowMetricsCalculator.getOrCreate().computeCurrentWindowMetrics(requireActivity())
//    val currentBounds = windowMetrics.bounds // E.g. [0 0 1350 1800]
//    val height = currentBounds.height()

//    return height
    return 0
}


fun BottomSheetDialogFragment.getWidth(): Int {
//    val windowMetrics =
//        WindowMetricsCalculator.getOrCreate().computeCurrentWindowMetrics(requireActivity())
//    val currentBounds = windowMetrics.bounds // E.g. [0 0 1350 1800]
//    val width = currentBounds.width()

    return 0
}

fun WindowManager.currentWindowMetricsPoint(): Point {
    return if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.R) {
        val windowInsets = currentWindowMetrics.windowInsets
        var insets: Insets = windowInsets.getInsets(WindowInsets.Type.navigationBars())
        windowInsets.displayCutout?.run {
            insets = Insets.max(
                insets,
                Insets.of(safeInsetLeft, safeInsetTop, safeInsetRight, safeInsetBottom)
            )
        }
        val insetsWidth = insets.right + insets.left
        val insetsHeight = insets.top + insets.bottom
        Point(
            currentWindowMetrics.bounds.width() - insetsWidth,
            currentWindowMetrics.bounds.height() - insetsHeight
        )
    } else {
        Point().apply {
            defaultDisplay.getSize(this)
        }
    }
}