package io.sevenb.terminal.ui.extension

import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.utils.Logger
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

suspend fun FirebaseAuth.login(email: String, password: String): Result<Boolean> =
    suspendCoroutine { continuation ->
        this.signInWithEmailAndPassword(email, password).addOnCompleteListener { task ->
            processTask(task, continuation)
        }
    }

suspend fun FirebaseAuth.register(email: String, password: String): Result<Boolean> =
    suspendCoroutine { continuation ->
        this.createUserWithEmailAndPassword(email, password).addOnCompleteListener { task ->
            processTask(task, continuation)
        }
    }

private fun FirebaseAuth.processTask(
    task: Task<AuthResult>,
    continuation: Continuation<Result<Boolean>>
) {
    if (task.isSuccessful) {
        Logger.sendLog(
            "FirebaseAuth",
            "processTask",
            "task.isSuccessful = true"
        )
        val user = this.currentUser
        continuation.resume(Result.Success(user != null))
    } else {
        val exception = task.exception
        Logger.sendLog(
            "FirebaseAuth",
            "processTask",
            exception
        )
        exception?.let { continuation.resume(Result.Error(it)) }
    }
}
