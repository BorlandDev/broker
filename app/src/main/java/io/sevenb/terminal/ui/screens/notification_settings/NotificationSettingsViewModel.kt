package io.sevenb.terminal.ui.screens.notification_settings

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.enums.notifications.NotificationsType
import io.sevenb.terminal.data.model.enums.notifications.NotificationsTypeFromBack
import io.sevenb.terminal.data.model.messages.MainNotificationsPush
import io.sevenb.terminal.data.model.messages.NotificationsSettingsRequest
import io.sevenb.terminal.domain.usecase.notification_handler.NotificationHandler
import kotlinx.coroutines.launch
import javax.inject.Inject

class NotificationSettingsViewModel @Inject constructor(
    private val notificationHandler: NotificationHandler
) : ViewModel() {

    lateinit var listMainNotificationsPush: MutableList<MainNotificationsPush>

    val enableOrderNotifications = MutableLiveData<Boolean?>()
    val enableWithdrawNotifications = MutableLiveData<Boolean?>()
    val enableDepositNotifications = MutableLiveData<Boolean?>()
    val popBackStack = MutableLiveData<Boolean?>()

    val errorMessage = MutableLiveData<String?>()

    var orderEnabled = false
    var depositEnabled = false
    var withdrawalEnabled = false

    fun enableNotification(value: Boolean, pNotificationPush: MainNotificationsPush) {
        viewModelScope.launch {
            pNotificationPush.apply {
                when (notificationHandler.put(
                    listOf(NotificationsSettingsRequest(template.id, text_type.id, value))
                )) {
                    is Result.Success -> {
                        val enable = when (template.template_type) {
                            NotificationsTypeFromBack.WITHDRAW_COMPLETE -> enableWithdrawNotifications
                            NotificationsTypeFromBack.DEPOSIT_COMPLETE -> enableDepositNotifications
                            NotificationsTypeFromBack.ORDER_COMPLETE -> enableOrderNotifications
                        }
                        enable.value = value
                    }
                    is Result.Error -> {
                        println()
                    }
                }
            }
        }
    }

    fun enableOrderNotifications(value: Boolean) {
        viewModelScope.launch {
            when (val enableResult =
                notificationHandler.storeEnabledNotifications(NotificationsType.ORDER)) {
                is Result.Success -> {
                    orderEnabled = value
                    checkNotificationsEnabling()
                    if (enableResult.data.result) enableOrderNotifications.value = value
                }
                is Result.Error -> errorMessage.value = enableResult.message
            }
        }
    }

    fun enableWithdrawalNotifications(value: Boolean) {
        viewModelScope.launch {
            when (val enableResult =
                notificationHandler.storeEnabledNotifications(NotificationsType.WITHDRAW)) {
                is Result.Success -> {
                    withdrawalEnabled = value
                    checkNotificationsEnabling()
                    if (enableResult.data.result) enableWithdrawNotifications.value = value
                }
                is Result.Error -> errorMessage.value = enableResult.message
            }
        }
    }

    fun enableDepositNotifications(value: Boolean) {
        viewModelScope.launch {
            when (val enableResult =
                notificationHandler.storeEnabledNotifications(NotificationsType.DEPOSIT)) {
                is Result.Success -> {
                    depositEnabled = value
                    checkNotificationsEnabling()
                    if (enableResult.data.result) enableDepositNotifications.value = value
                }
                is Result.Error -> errorMessage.value = enableResult.message
            }
        }
    }

    private fun checkNotificationsEnabling() {
        popBackStack.value = !orderEnabled && !withdrawalEnabled && !depositEnabled
    }
}