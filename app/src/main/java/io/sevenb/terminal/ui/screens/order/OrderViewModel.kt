package io.sevenb.terminal.ui.screens.order

import android.os.Build
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.sevenb.terminal.BuildConfig
import io.sevenb.terminal.data.datasource.PreferenceStorage
import io.sevenb.terminal.data.model.broker_api.markets.Market
import io.sevenb.terminal.data.model.broker_api.rates.MarketRateParams
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.enum_model.*
import io.sevenb.terminal.data.model.enums.OrderFieldPosition
import io.sevenb.terminal.data.model.filter.Filters
import io.sevenb.terminal.data.model.order.BidAskResponse
import io.sevenb.terminal.data.model.order.OrderItem
import io.sevenb.terminal.data.model.order.OrderParams
import io.sevenb.terminal.data.model.order.ReceivedRate
import io.sevenb.terminal.data.model.withdrawal.WithdrawItem
import io.sevenb.terminal.data.repository.auth.UserAuthRepository
import io.sevenb.terminal.domain.usecase.auth.LocalUserDataStoring
import io.sevenb.terminal.domain.usecase.charts.AccountData
import io.sevenb.terminal.domain.usecase.charts.AccountData.Companion.getBaseCurrencyTicker
import io.sevenb.terminal.domain.usecase.deposit.ClipboardCopy
import io.sevenb.terminal.domain.usecase.order.*
import io.sevenb.terminal.domain.usecase.sort.SortCurrenciesHandler
import io.sevenb.terminal.domain.usecase.trading.TradingCurrencies
import io.sevenb.terminal.ui.extension.*
import io.sevenb.terminal.ui.screens.select_currency.DepositCurrencyItem
import io.sevenb.terminal.ui.screens.trading.TradingListAdapter.Companion.formatCryptoAmount
import io.sevenb.terminal.utils.ConnectionStateMonitor
import io.sevenb.terminal.utils.Logger
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.java_websocket.client.WebSocketClient
import org.java_websocket.handshake.ServerHandshake
import timber.log.Timber
import java.lang.reflect.Type
import java.math.RoundingMode
import java.net.URI
import java.text.DecimalFormat
import javax.inject.Inject
import javax.net.ssl.*

class OrderViewModel @Inject constructor(
    private val orderOperations: OrderOperations,
    private val marketRate: MarketRate,
    private val localUserDataStoring: LocalUserDataStoring,
    private val parseFloatFromString: ParseFloatFromString,
    val accountData: AccountData,
    private val tradingCurrencies: TradingCurrencies,
    private val marketsData: MarketsData,
    private val clipboardCopy: ClipboardCopy,
    private val orderAvailability: OrderAvailability,
    private val sortCurrenciesHandler: SortCurrenciesHandler,
    private val sharedPefs: PreferenceStorage
) : ViewModel() {

    val orderResult = MutableLiveData<OrderItem>()
    val marketEstimateResult = MutableLiveData<String>()
    val reverseMarketEstimateResult = MutableLiveData<String>()
    val estimatedCostResult = MutableLiveData<String>()
    val rateResultString = MutableLiveData<String>()
    val brokerCommission = MutableLiveData<String>()
    val orderProcessing = MutableLiveData<Boolean>()
    val marketUnavailableMessage = MutableLiveData<Boolean>()
    val emptyBalanceResult = MutableLiveData<Boolean>()
    val snackbarMessage = MutableLiveData<ErrorMessageType>()
    val customErrorMessage = MutableLiveData<String>()
    val listOfBuyCoins = MutableLiveData<List<DepositCurrencyItem>>()
    val accountCoins = MutableLiveData<List<WithdrawItem>>()
    val availableBalance = MutableLiveData<String>()
    val showRateUs = MutableLiveData<Boolean>()

    val takeProfitValues = MutableLiveData<Pair<String, String>>().apply { value = Pair("0", "0") }
    val stopLossValues = MutableLiveData<Pair<String, String>>().apply { value = Pair("0", "0") }
    val fieldMinimal = MutableLiveData<OrderFieldError>()

    val market = MutableLiveData<Pair<Market?, Boolean>?>()
    val listDepositCoins = MutableLiveData<List<DepositCurrencyItem>>()

    private var localLimitType = LimitType.TAKE_PROFIT
    var rate: Float = 0.0f
    private var estimatedCostAmount: Float = 0.0f
    private var takeProfitPercent: Int = 0
    private var stopLossPercent: Int = 0

    var fromCurrency: String = ""

    var isFirstStart = true

    private var showTimeError = 0L

    val limitOrderMessage = MutableLiveData<String>()
    val byBuySellEstimate = MutableLiveData<Float>()
    val receiveForEstimate = MutableLiveData<Float>()
    val marketDataIsReady = MutableLiveData<Int>()

    private var filterMap = mutableMapOf<BinanceFilter, Filters>()

    var buySellRate = 0.0f
    var receiveForRate = 0.0f

    lateinit var reverseMarket: Market

    var localPricePrecision: Int? = 0

    var firstMarketBoolean = true
    var firstMarket: Market? = null
    var isDirectOrder = false

    var errorFieldIsShowing = false
    val receivedRates = MutableLiveData<ReceivedRate>()

    private lateinit var webSocketClient: WebSocketClient
    private lateinit var directSymbol: String

    private val gson = Gson()
    private val type: Type = genericType<BidAskResponse>()

    val bid = MutableLiveData<String>()
    val ask = MutableLiveData<String>()
    val showPercentPriceError = MutableLiveData<String>()
    val showInsufficientBalanceError = MutableLiveData<String>()
    val pricePrecision = MutableLiveData<Int>()
    val ratesList =
        MutableLiveData<MutableMap<String, io.sevenb.terminal.data.model.broker_api.rates.MarketRate?>>()

    var secondTickerPrecision = 0
    var pricePrecisionForNonDirectPair = 0

    var bidMillis = 0L
    var askMillis = 0L

    private val cc = ConflatedBroadcastChannel<String?>()
    var subscribed = false

    var multiplierUp: Float? = null
    var multiplierDown: Float? = null

    init {
        val currentTime = System.currentTimeMillis()

        bidMillis = currentTime
        askMillis = currentTime

        viewModelScope.launch {
            withContext(IO) {
                cc.consumeEach {
                    if (it != null && !it.contains("Request success")) {
                        val time = System.currentTimeMillis()
                        if (askMillis + 300 <= time) {
                            askMillis = time
                            setUpBidAsk(it)
                        }
                    }
                }
            }
        }
    }

    private suspend fun getFilters(symbol: String) {
        when (val filtersResult = marketsData.getFilters(symbol)) {
            is Result.Success -> {
                println()
                directSymbol = symbol.toLowerCase()

                if (subscribed)
                    unsubscribe()
                initWebSocket()

                filtersResult.data.filters?.forEach {
                    it.filterType?.let { filter -> filterMap[filter] = it }
                }

                multiplierUp =
                    filterMap[BinanceFilter.PERCENT_PRICE]?.let { it.multiplierUp?.getFloatValue() }
                multiplierDown =
                    filterMap[BinanceFilter.PERCENT_PRICE]?.let { it.multiplierDown?.getFloatValue() }

                val step = getPrecision()
                localPricePrecision = getPricePrecision()
                localPricePrecision?.let {
                    withContext(Main) {
                        pricePrecision.value = if (isDirectOrder) it else step?.let { step }
                    }
                    secondTickerPrecision = it
                }

                step?.let {
                    withContext(Main) { marketDataIsReady.value = it }
                    pricePrecisionForNonDirectPair = it
                }
            }
            is Result.Error -> {
                Logger.sendLog("OrderViewModel", "getFilters", filtersResult.exception)
                println()
                filterMap = mutableMapOf()
                withContext(Main) {
                    customErrorMessage.value = filtersResult.message
                }
            }
        }
    }

    fun depositList() {
        viewModelScope.launch {
            when (val depositListResult = tradingCurrencies.mappedDepositList()) {
                is Result.Success -> {
                    val sortedDepositItems =
                        sortCurrenciesHandler.sortByTicker(depositListResult.data)
                    val currencies = findMarketList(fromCurrency)
                    val finalDepositList = mutableListOf<DepositCurrencyItem>()

                    listDepositCoins.value = if (currencies != null) {
                        sortedDepositItems.forEach { depositItem ->
                            currencies[depositItem.ticker]?.let { finalDepositList.add(depositItem) }
                        }

                        sortCurrenciesHandler.filterBnbBscNetwork(finalDepositList)
                    } else sortedDepositItems
                }
                is Result.Error -> {
                    Logger.sendLog("OrderViewModel", "depositList", depositListResult.exception)
                    when {
                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                        else -> customErrorMessage.value = depositListResult.exception.message
                    }
                }
            }
        }
    }

    fun showRateUs(alwaysShow: Boolean) {
        viewModelScope.launch {
            val showInAppReview =
                localUserDataStoring.isInAppReviewEnabled() as? Result.Success
                    ?: return@launch
                showRateUs.postValue(showInAppReview.data)
        }
    }

    private fun showNoInternetConnection() {
        val currentTime = System.currentTimeMillis()
        if (currentTime - showTimeError >= 1500) {
            customErrorMessage.value = ConnectionStateMonitor.noInternetError
            showTimeError = currentTime
        }
    }

    private suspend fun findMarketList(from: String): MutableMap<String, String>? {
        return withContext(Dispatchers.IO) {
            when (val marketsResult = marketsData.cacheFirstMarkets()) {
                is Result.Success -> {
                    val markets = marketsResult.data

                    val currencies: MutableMap<String, String> = mutableMapOf()

                    markets.forEach {
                        if (it.baseAsset == from)
                            currencies[it.quoteAsset] = it.quoteAsset
                        else if (it.quoteAsset == from) currencies[it.baseAsset] = it.baseAsset
                    }

                    currencies
                }
                is Result.Error -> {
                    Logger.sendLog("OrderViewModel", "findMarketList", marketsResult.exception)
                    when {
                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                        else -> {
                            withContext(Main) {
                                customErrorMessage.value = marketsResult.exception.message
                            }
                        }
                    }
                    null
                }
                else -> null
            }
        }
    }

    fun findMarkets(from: String, to: String) {
        viewModelScope.launch {
            findMarketWithStepSize(from, to)
        }
    }

    fun setLastRateTime() {
        viewModelScope.launch {
            localUserDataStoring.setLastRateTime(null)
        }
    }

    fun requestAccountCoins() {
        viewModelScope.launch {
            when (val withdrawalListResult = accountData.mapWithdrawalList()) {
                is Result.Success -> {
                    val listOfAccountCoins = sortCurrenciesHandler
                        .filterBnbBscNetwork(sortCurrenciesHandler.sortByTicker(withdrawalListResult.data))
                    accountCoins.value = listOfAccountCoins
                    requestRates(listOfAccountCoins)
                }
                is Result.Error -> {
                    Logger.sendLog("OrderViewModel", "requestAccountCoins", withdrawalListResult.exception)
                    when {
                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                        else -> customErrorMessage.value = withdrawalListResult.exception.message
                    }
                }
            }
        }
    }

    private suspend fun requestRates(listOfAccountCoins: List<WithdrawItem>) {
        val baseTicker = getBaseCurrencyTicker()
        val requestParams = mutableListOf<MarketRateParams>()

        listOfAccountCoins.forEach {
            requestParams.add(MarketRateParams(it.ticker, baseTicker))
        }

        when (val priceResult = accountData.getPrices(requestParams)) {
            is Result.Success -> {
                val mapOfRates =
                    mutableMapOf<String, io.sevenb.terminal.data.model.broker_api.rates.MarketRate?>()
                priceResult.data.forEachIndexed { index, marketRate ->
                    mapOfRates[listOfAccountCoins[index].ticker] = marketRate
                }
                ratesList.value = mapOfRates
            }
            is Result.Error -> {
                Logger.sendLog("OrderViewModel", "requestRates", priceResult.exception)

                ratesList.value = mutableMapOf()
            }
        }
    }

    fun requestSellBalance(sellTicker: String) {
        viewModelScope.launch {
            when (val balanceByTickerResult = accountData.balanceByTicker(sellTicker)) {
                is Result.Success -> availableBalance.value = balanceByTickerResult.data
            }
        }
    }

    fun orderPost(
        from: String,
        to: String,
        orderSide: OrderBottomSheetFragment.Companion.OrderSide,
        orderType: OrderType,
        amount: String,
        price: String = "",
        thirdPrice: String = ""
    ) {
        viewModelScope.launch {
            orderProcessing.value = true

            if (orderAvailability.isBalanceNotAvailable(from, to, orderSide)) {
                emptyBalanceResult.value = true
                orderProcessing.value = false
                return@launch
            }

            if (amount.isEmpty()) {
                snackbarMessage.value = ErrorMessageType.EMPTY_FIELDS
                orderProcessing.value = false
                return@launch
            }

            if (orderType == OrderType.LIMIT &&
                price.isEmpty()
            ) {
                orderProcessing.value = false
                return@launch
            }

            val symbolMarket = findMarket(from, to)

            if (symbolMarket == null) {
                snackbarMessage.value = ErrorMessageType.MARKET_NOT_AVAILABLE
                orderProcessing.value = false
                return@launch
            }

            var priceForLimitOrder = price

            // apply orderSide on right market and revert order side on market [to/from]
            val marketSide = if (from == symbolMarket.baseAsset) {
                orderSide
            } else {
                if (orderSide == OrderBottomSheetFragment.Companion.OrderSide.BUY)
                    OrderBottomSheetFragment.Companion.OrderSide.SELL
                else
                    OrderBottomSheetFragment.Companion.OrderSide.BUY
            }

            var newAmount = amount
            val orderParams: OrderParams
            when (orderType) {
                OrderType.LIMIT -> {
                    var stopPrice: Float? = null
                    val priceFloat = parseFloatFromString.execute(price)
                    var limitOrderType = orderType

                    if (takeProfitPercent > 0) {
                        limitOrderType = OrderType.TAKE_PROFIT_LIMIT
                        stopPrice = priceFloat.times(takeProfitPercent).div(100) + priceFloat
                    } else if (stopLossPercent > 0) {
                        limitOrderType = OrderType.STOP_LOSS_LIMIT
                        stopPrice = priceFloat - priceFloat.times(stopLossPercent).div(100)
                    }

                    market.value?.let {
                        val estimatedRate = price.toFloatOrNull() ?: 0.0f
                        if (!it.second) {
                            localPricePrecision?.let { precision ->
                                priceForLimitOrder =
                                    roundFloat(1 / estimatedRate, precision)
                            }

                            newAmount = if (::reverseMarket.isInitialized) {
                                getPrecision()?.let { precision ->
                                    roundFloat((thirdPrice.toFloatOrNull() ?: 0.0f), precision)
                                }.toString()
                            } else {
                                getPrecision()?.let { precision ->
                                    roundFloat(
                                        (thirdPrice.toFloatOrNull() ?: 0.0f), precision
                                    )
                                } ?: ""
                            }
                        } else {
                            priceForLimitOrder = estimatedRate.toString()
                        }
                    }

                    orderParams = OrderParams(
                        symbolMarket.symbol,
                        marketSide.toString(),
                        limitOrderType.toString(),
                        timeLimitType = TimeInForceType.GTC.toString(),
                        price = priceForLimitOrder.getPlainDecimalString(),
                        stopPrice = stopPrice?.round(8)
                    )
                }
                else -> {
                    orderParams = OrderParams(
                        symbolMarket.symbol,
                        marketSide.toString(),
                        orderType.toString(),
                    )
                }
            }

            // 1. type of amount parameter for MARKET order type:
            // - to buy/sell DGB on the market DGBBTC than you need to pass parameter "amount"
            // - to buy/sell DGB on the market BTCDGB than you need to pass parameter "quoteAmount"
            // 2. for LIMIT type always "amount"
            when {
                orderType == OrderType.LIMIT -> orderParams.amount = newAmount
                from == symbolMarket.baseAsset -> orderParams.amount = amount
                else -> orderParams.quoteAmount = amount
            }

            filterMap[BinanceFilter.MIN_NOTIONAL]?.let {
                errorFieldIsShowing = if (isDirectOrder) {
                    if (thirdPrice.getFloatValue() < it.minNotional.getFloatValue()) { // third field is less than min notional
                        processMinNotional(it.minNotional, to, OrderFieldError.THIRD_FIELD)
                        return@launch
                    } else false
                } else {
                    if (amount.getFloatValue() < it.minNotional.getFloatValue()) { // first field is less than min notional
                        processMinNotional(it.minNotional, from, OrderFieldError.FIRST_FIELD)
                        return@launch
                    } else false
                }
            }

            val percentPrice = when (orderType) {
                OrderType.LIMIT -> priceForLimitOrder.getFloatValue()
                OrderType.MARKET -> price.getFloatValue()
                else -> 1f
            }

            if (multiplierUp != null && multiplierDown != null) {
                val mDown = rate * multiplierDown!!
                val mUp = rate * multiplierUp!!
                if (isDirectOrder) {
                    if (percentPrice > rate * multiplierDown!! && rate * multiplierUp!! > percentPrice)
                    else {
                        showPercentPriceError.value =
                            "The price must be in the range from ${formatBidAskPrecision(mDown.toPlainString())}" +
                                    " to ${formatBidAskPrecision(mUp.toPlainString())}"
                        return@launch
                    }
                } else {
                    if (price.getFloatValue() > rate * multiplierDown!! && rate * multiplierUp!! > price.getFloatValue())
                    else {
                        showPercentPriceError.value =
                            "The price must be in the range from ${formatBidAskPrecision(mDown.toPlainString())} " +
                                    "to ${formatBidAskPrecision(mUp.toPlainString())}"
                        return@launch
                    }
                }
            }

            when (val accountDataResult = orderOperations.orderPost(orderParams)) {
                is Result.Success -> {
                    val orderItem = accountDataResult.data
                    orderProcessing.value = false
                    orderResult.value = orderItem
                }
                is Result.Error -> {
                    Logger.sendLog("OrderViewModel", "orderPost", accountDataResult.exception)

                    orderProcessing.value = false
                    when {
                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                        else -> {
                            val insufficientBalance =
                                "Account has insufficient balance for requested action."
                            if (accountDataResult.message.contains(insufficientBalance)) {
                                showInsufficientBalanceError.value = insufficientBalance
                                delay(2500)
                                showInsufficientBalanceError.value = ""
                            } else {
                                customErrorMessage.value =
                                    handleErrorMessage(accountDataResult.message)
                                limitOrderMessage.value = "Error: ${accountDataResult.message}"
                            }
                        }
                    }
                }
            }
        }
    }

    private fun processMinNotional(
        minNotional: String?,
        ticker: String,
        errorField: OrderFieldError
    ) {
        val minValue = removeZerosBehind(minNotional)
        customErrorMessage.value =
            "$ticker amount is less than minimal $minValue"
        fieldMinimal.value = errorField
        errorFieldIsShowing = true
        orderProcessing.value = false
    }

    private suspend fun findMarket(from: String, to: String): Market? {
        return withContext(Dispatchers.IO) {
            when (val marketsResult = marketsData.cacheFirstMarkets()) {
                is Result.Success -> {
                    val markets = marketsResult.data
                    val symbolMarket = markets.firstOrNull {
                        (it.baseAsset == from && it.quoteAsset == to) || (it.baseAsset == to && it.quoteAsset == from)
                    }
                    symbolMarket
                }
                is Result.Error -> {
                    Logger.sendLog("OrderViewModel", "findMarket", marketsResult.exception)
                    when {
                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                        else -> customErrorMessage.value = marketsResult.exception.message
                    }
                    null
                }
            }
        }
    }

    fun makeEstimate(from: String, to: String, fromField: OrderFieldPosition) {
        viewModelScope.launch {
            val actualTo = when (to) {
                "USD" -> AccountData.USDT
                "XEC" -> "BCHA"
                else -> to
            }
            marketUnavailableMessage.value = false

            if (market.value == null) {
                findMarketWithStepSize(from, actualTo)
            }

            val baseCurrency = getBaseCurrencyTicker()
            val rateResult = if (isDirectOrder) {
                val listOfParams = listOf(
                    MarketRateParams(from, actualTo),
                    MarketRateParams(from, baseCurrency),
                    MarketRateParams(actualTo, baseCurrency)
                )
                accountData.getPrices(listOfParams)
            } else {
                val listOfParams = listOf(
                    MarketRateParams(actualTo, from),
                    MarketRateParams(from, baseCurrency),
                    MarketRateParams(actualTo, baseCurrency)
                )
                accountData.getPrices(listOfParams)
            }

            when (rateResult) {
                is Result.Success -> {
                    val marketRateResult = rateResult.data
                    if (marketRateResult[0]?.rate != null) {
                        rate = if (!isDirectOrder) {
                            1 / marketRateResult[0]?.rate!!
                        } else
                            marketRateResult[0]?.rate!!

                        marketRateResult[1]?.rate?.let {
                            buySellRate = it
                        }
                        marketRateResult[2]?.rate?.let {
                            receiveForRate = it
                        }

                        // Rate
                        val rateString = if (isDirectOrder) {
                            if (localPricePrecision != null)
                                roundFloat(rate, localPricePrecision!!)
                            else formatCryptoAmount(rate, true)
                        } else {
                            roundFloat(rate, pricePrecisionForNonDirectPair)
                        }

                        rateResultString.value = rateString

                        receivedRates.value = ReceivedRate(
                            marketRateResult[0]?.rate ?: 0.0f,
                            buySellRate,
                            receiveForRate,
                            fromField
                        )
                    }
                }
                is Result.Error -> {
                    Logger.sendLog("OrderViewModel", "makeEstimate", rateResult.exception)
                    when {
                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                        else -> customErrorMessage.value = rateResult.exception.message
                    }
                }
            }
        }
    }

    private suspend fun findMarketWithStepSize(from: String, to: String) {
        return withContext(Dispatchers.IO) {
            when (val marketsResult = marketsData.cacheFirstMarkets()) {
                is Result.Success -> {
                    val actualFrom = if (from == "XEC") "BCHA" else from
                    val actualTo = if (to == "XEC") "BCHA" else to
                    val markets = marketsResult.data

                    val isOrderSide: Boolean

                    var symbolMarket = markets.firstOrNull {
                        (it.baseAsset == actualFrom && it.quoteAsset == actualTo)
                    }

                    markets.firstOrNull {
                        (it.baseAsset == actualTo && it.quoteAsset == actualFrom)
                    }?.let { reverseMarket = it }

                    if (symbolMarket == null) {
                        symbolMarket = markets.firstOrNull {
                            it.baseAsset == actualTo && it.quoteAsset == actualFrom
                        }
                        isOrderSide = false
                        initFirstMarket(symbolMarket, isOrderSide)
                        getFilters("$actualTo$actualFrom")
                    } else {
                        isOrderSide = true
                        initFirstMarket(symbolMarket, isOrderSide)
                        getFilters("$actualFrom$actualTo")
                    }

                    withContext(Main) {
                        market.value = Pair(symbolMarket, isOrderSide)
                    }
                }
                is Result.Error -> {
                    Logger.sendLog("OrderViewModel", "findMarketWithStepSize", marketsResult.exception)
                    when {
                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                        else -> customErrorMessage.postValue(marketsResult.exception.message)
                    }
                }
            }
        }
    }

    private fun initFirstMarket(market: Market?, isDirectOrder: Boolean) {
        if (firstMarketBoolean) {
            firstMarket = market
            this.isDirectOrder = isDirectOrder
        }

        firstMarketBoolean = false
    }

    fun marketEstimate(from: String, to: String, amountString: String) {
        viewModelScope.launch {
            val actualTo = if (to == "USD") AccountData.USDT else to
            marketUnavailableMessage.value = false
            val formattedString = amountString.replace(",", "")
            val amount = parseFloatFromString.execute(formattedString)

            if (market.value == null) {
                findMarketWithStepSize(from, actualTo)
            }

            val baseCurrency = getBaseCurrencyTicker()
            val rateResult = if (market.value!!.second) {
                val listOfParams = listOf(
                    MarketRateParams(from, actualTo),
                    MarketRateParams(from, baseCurrency),
                    MarketRateParams(actualTo, baseCurrency)
                )
//                getFilters("$from$actualTo")
                accountData.getPrices(listOfParams)
            } else {
                val listOfParams = listOf(
                    MarketRateParams(actualTo, from),
                    MarketRateParams(from, baseCurrency),
                    MarketRateParams(actualTo, baseCurrency)
                )
//                getFilters("$actualTo$from")
                accountData.getPrices(listOfParams)
            }

            when (rateResult) {
                is Result.Success -> {
                    val marketRateResult = rateResult.data
                    if (marketRateResult[0]?.rate != null) {
                        rate = if (!market.value!!.second) {
                            1 / marketRateResult[0]?.rate!!
                        } else
                            marketRateResult[0]?.rate!!

                        if (rate == 0.0f) {
                            marketUnavailableMessage.value = true
                            marketEstimateResult.value = ""
                            rateResultString.value = ""

                            return@launch
                        }

                        val estimated = amount.toBigDecimal() * rate.toBigDecimal()
                        val marketEstimatedString = roundFloat(estimated.toFloat())

                        marketRateResult[1]?.rate?.let {
                            buySellRate = it
                            byBuySellEstimate.value = it
                        }
                        marketRateResult[2]?.rate?.let {
                            receiveForRate = it
                            receiveForEstimate.value = it
                        }

                        marketEstimateResult.value = marketEstimatedString.replace(",", ".")

                        // Rate
                        val rateString = if (localPricePrecision != null)
                            roundFloat(rate, localPricePrecision!!)
                        else formatCryptoAmount(rate, true)

                        if (rateString == "0") {
                            println()
                        }
                        rateResultString.value = rateString
                    }
                }
                is Result.Error -> {
                    Logger.sendLog("OrderViewModel", "marketEstimate", rateResult.exception)
                    when {
                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                        else -> customErrorMessage.value = rateResult.exception.message
                    }
                }
            }
        }
    }

    fun reverseMarketEstimate(from: String, to: String, amountString: String) {
        viewModelScope.launch {
            marketUnavailableMessage.value = false
            val formattedString = amountString.replace(",", "")
            val amount = parseFloatFromString.execute(formattedString)

            if (market.value == null) {
                findMarketWithStepSize(from, to)
            }

            val baseCurrency = getBaseCurrencyTicker()
            val rateResult = if (market.value!!.second) {
                val listOfParams = listOf(
                    MarketRateParams(from, to),
                    MarketRateParams(from, baseCurrency),
                    MarketRateParams(to, baseCurrency)
                )
                accountData.getPrices(listOfParams)
            } else {
                val listOfParams = listOf(
                    MarketRateParams(to, from),
                    MarketRateParams(from, baseCurrency),
                    MarketRateParams(to, baseCurrency)
                )
                accountData.getPrices(listOfParams)
            }

            when (rateResult) {
                is Result.Success -> {
                    val marketRateResult = rateResult.data
                    if (marketRateResult[0]?.rate != null) {
                        rate = if (!market.value!!.second) {
                            1 / marketRateResult[0]?.rate!!
                        } else
                            marketRateResult[0]?.rate!!

                        if (rate == 0.0f) {
                            marketUnavailableMessage.value = true
                            reverseMarketEstimateResult.value = ""
                            rateResultString.value = ""

                            return@launch
                        }

                        val estimatedString = amount / rate
                        val precision = getPrecision()

                        val reverseMarketEstimate = if (market.value!!.second && precision != null)
                            roundFloat(estimatedString, precision)
                        else roundFloat(estimatedString)

                        marketRateResult[1]?.rate?.let {
                            buySellRate = it
                            byBuySellEstimate.value = it
                        }
                        marketRateResult[2]?.rate?.let {
                            receiveForRate = it
                            receiveForEstimate.value = it
                        }
                        reverseMarketEstimateResult.value = reverseMarketEstimate.replace(",", ".")

                        // Rate
                        val rateString = if (localPricePrecision != null)
                            roundFloat(rate, localPricePrecision!!)
                        else formatCryptoAmount(rate, true)

                        if (rateString == "0") {
                            println()
                        }
                        rateResultString.value = rateString
                    }
                }
                is Result.Error -> {
                    Logger.sendLog("OrderViewModel", "reverseMarketEstimate", rateResult.exception)
                    when {
                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                        else -> customErrorMessage.value = rateResult.exception.message
                    }
                }
            }
        }
    }

    fun limitOrderMarketPrice(from: String, to: String, amountString: String) {
        viewModelScope.launch {
            val amount = parseFloatFromString.execute(amountString)

            if (amount <= 0) {
                Timber.d("limitOrderMarketPrice amount <= 0")
                return@launch
            }

            when (val rateResult = marketRate.execute(from, to)) {
                is Result.Success -> {
                    val marketRateResult = rateResult.data
                    if (marketRateResult.rate != null) {
                        rate = marketRateResult.rate!!

                        // Rate
                        val rateString = formatCryptoAmount(rate, true)
                        rateResultString.value = rateString
                    }
                }
                is Result.Error -> {
                    Logger.sendLog("OrderViewModel", "limitOrderMarketPrice", rateResult.exception)

                    when {
                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                        else -> customErrorMessage.value = rateResult.exception.message
                    }
                }
            }
        }
    }

    fun estimatedCost(amountString: String, priceString: String) {
        viewModelScope.launch {
            val amount = parseFloatFromString.execute(amountString)
            val price = parseFloatFromString.execute(priceString)

            if (amount <= 0 || price <= 0) {
                Timber.d("estimatedCost amount <= 0 || price <= 0")
                return@launch
            }

            estimatedCostAmount = amount.times(price)
            val commission = estimatedCostAmount.times(0.001f) // 0.01 %
            val commissionString = roundFloat(commission)
            brokerCommission.value = commissionString
            estimatedCostResult.value = roundFloat(estimatedCostAmount + commission)
        }
    }

    fun takeProfitStopLossChanger(limitType: LimitType, isPlusButton: Boolean) {
        localLimitType = limitType
        when (limitType) {
            LimitType.TAKE_PROFIT -> {
                if (isPlusButton) {
                    if (takeProfitPercent == MAX_PERCENT) return
                    takeProfitPercent += 5
                } else {
                    if (takeProfitPercent == MIN_PERCENT) return
                    takeProfitPercent -= 5
                }
                takeProfitValues.value =
                    Pair(takeProfitPercent.toString(), estimatedPercentString(takeProfitPercent))
                stopLossValues.value =
                    Pair("0", "0")
                stopLossPercent = 0
            }
            LimitType.STOP_LOSS -> {
                if (isPlusButton) {
                    if (stopLossPercent == MAX_PERCENT) return
                    stopLossPercent += 5
                } else {
                    if (stopLossPercent == MIN_PERCENT) return
                    stopLossPercent -= 5
                }
                stopLossValues.value =
                    Pair(stopLossPercent.toString(), estimatedPercentString(stopLossPercent))
                takeProfitValues.value =
                    Pair("0", "0")
                takeProfitPercent = 0
            }
        }
    }

    fun updateTakeStopAmounts() {
        stopLossValues.value =
            Pair(stopLossPercent.toString(), estimatedPercentString(stopLossPercent))
        takeProfitValues.value =
            Pair(takeProfitPercent.toString(), estimatedPercentString(takeProfitPercent))
    }

    private fun quoteAmount(amountString: String): Float {
        val amount = amountString.toFloatOrNull() ?: 0.0f
        return amount * (1 + takeProfitPercent / 100)
    }

    private fun stopPrice(): Float {
        return rate * stopLossPercent / 100
    }

    private fun estimatedPercentString(percent: Int): String {
        val amount = estimatedCostAmount.times(percent).div(100)
        return amount.round(6)
    }

    private fun handleErrorMessage(error: String?): String {
        if (error?.contains("Account has insufficient balance for requested action.") == true)
            return "Account has insufficient balance for requested action."

        if (error?.contains("MIN_NOTIONAL") == true)
            return "Quantity is too low to be a valid order."

        if (error?.contains("PRICE_FILTER") == true)
            return "Price is too low or too high."

        if (error?.contains("PERCENT_PRICE") == true) {
            return when (localLimitType) {
                LimitType.TAKE_PROFIT -> "Invalid Take-Profit range"
                LimitType.STOP_LOSS -> "Invalid Stop-Loss range"
            }
        }

        return error ?: ""
    }

    fun getPrecision(): Int? {
        return filterMap[BinanceFilter.LOT_SIZE]?.let {
            val stringTickFirst = it.stepSize?.split(".")?.get(0)
            val stringTickSecond = it.stepSize?.split(".")?.get(1)
            var precision: Int?
            if (stringTickFirst != "1") {
                precision = 0
                stringTickSecond?.let { str ->
                    loop@ for (char in str) {
                        precision = precision?.plus(1)
                        if (char.toInt() == 49) break@loop
                    }
                }
            } else precision = 0
            return if (precision == -1) null else precision
        }
    }

    private fun removeZerosBehind(value: String?): String? {
        return value?.let {
            val reversed = value.reversed()
            var reversedCopy = ""

            var countOfZeros = -1
            loop@ for (char in reversed) {
                if (char.toInt() != 48) break@loop
                countOfZeros++
            }
            reversed.forEachIndexed { index, c ->
                if (index > countOfZeros) reversedCopy += c
            }
            reversedCopy.reversed()
        }
    }

    private fun getPricePrecision(): Int? {
        return filterMap[BinanceFilter.PRICE_FILTER]?.let {
            val stringTickFirst = it.tickSize?.split(".")?.get(0)
            val stringTickSecond = it.tickSize?.split(".")?.get(1)
            var precision: Int?
            if (stringTickFirst != "1") {
                precision = 0
                stringTickSecond?.let { str ->
                    loop@ for (char in str) {
                        precision = precision?.plus(1)
                        if (char.toInt() == 49) break@loop
                    }
                }
            } else precision = 0
            return if (precision == -1) null else precision
        }
    }

    fun checkIsInPercents(value: String): Boolean {
        return if (availableBalance.value != null) {
            val amount = availableBalance.value!!.replace(",", "").toFloatOrNull() ?: 0.0f
            val percented25 = amount * 25 / 100
            val percented50 = amount * 50 / 100
            val percented100 = amount * 100 / 100
            value == percented25.toString() || value == percented50.toString() || value == percented100.toString()
        } else false
    }

    fun checkIsAllZeros(stringToCheck: String): String {
        var value = true
        stringToCheck.forEach {
            if (it.toInt() != 48 && it.toInt() != 46) value = false
        }

        return if (value) "0" else stringToCheck
    }

    private fun initWebSocket() {
        val brokerApiURI = URI(WEB_SOCKET_URL + UserAuthRepository.accessToken)
        createWebSocketClient(brokerApiURI)
    }

    private fun createWebSocketClient(uri: URI?) {
        viewModelScope.launch {
            webSocketClient = object : WebSocketClient(uri) {

                override fun onOpen(handshakedata: ServerHandshake?) {
                    subscribe()
                    verifyHostname(uri?.host)
                }

                override fun onMessage(message: String?) {
                    cc.offer(message)
                }

                override fun onClose(code: Int, reason: String?, remote: Boolean) {
                    unsubscribe()
                }

                override fun onError(ex: Exception?) {
                    subscribed = false
                }

                override fun onSetSSLParameters(sslParameters: SSLParameters?) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        super.onSetSSLParameters(sslParameters)
                    }
                }

            }

            val socketFactory: SSLSocketFactory = SSLSocketFactory.getDefault() as SSLSocketFactory
            webSocketClient.setSocketFactory(socketFactory)
            webSocketClient.connect()
        }
    }

    fun subscribe() {
        try {
            if (!subscribed && ::directSymbol.isInitialized) {
                if (webSocketClient.isOpen) {
                    subscribed = true
                    webSocketClient.send(
                        "{\n" +
                                " \"event\": \"subscribe\",\n" +
                                " \"stream\": \"$directSymbol@bookTicker\"\n" +
                                "}"
                    )
                } else {
                    initWebSocket()
                }
            }
        } catch (e: Exception) {

        }
    }

    fun unsubscribe() {
        try {
            subscribed = false
            webSocketClient.send(
                "{\n" +
                        " \"event\": \"unsubscribe\",\n" +
                        " \"stream\": \"$directSymbol@bookTicker\"\n" +
                        "}"
            )
            webSocketClient.close()
        } catch (e: Exception) {

        }
    }

    fun verifyHostname(hostname: String? = "") {
        webSocketClient.socket?.let {
            val hv: HostnameVerifier = HttpsURLConnection.getDefaultHostnameVerifier()
            val socket: SSLSocket = it as SSLSocket
            val session: SSLSession = socket.session

            if (!hv.verify(hostname, session)) {
                val exception =
                    SSLHandshakeException("Expected ${hostname}, found ${session.peerPrincipal}")
                Logger.sendLog(
                    "OrderViewModel",
                    "createWebSocketClient",
                    exception
                )
                throw exception
            }
        }
    }

    private suspend fun setUpBidAsk(message: String?) {
        message?.let { text ->
            val measurements: BidAskResponse = gson.fromJson(text, type)

            measurements.data?.data?.bidPrice?.let {
                withContext(Main) {
                    if (isDirectOrder) {
                        bid.value = formatBidAsk(it)
                    } else {
                        ask.value = formatBidAsk(it)
                    }
                }
            }
            measurements.data?.data?.askPrice?.let {
                withContext(Main) {
                    if (isDirectOrder) {
                        ask.value = formatBidAsk(it)
                    } else {
                        bid.value = formatBidAsk(it)
                    }
                }
            }
        }
    }

    private fun formatBidAsk(value: String): String {
        val pricePrecision =
            (if (isDirectOrder) localPricePrecision else pricePrecisionForNonDirectPair)
                ?: return ""

        val actualValue = if (!isDirectOrder) {
            val estimatedBidAsk = 1 / value.getFloatValue()
            estimatedBidAsk.toString()
        } else value

        val intPart = actualValue.getIntDigits()
        return if (pricePrecision == 0) intPart
        else {
            var newDecimalPart = ""
            val decimals = actualValue.getDecimalDigits()
            loop@ for ((index, char) in decimals.withIndex()) {
                if (index < pricePrecision) newDecimalPart += char
                else break@loop
            }
            "$intPart.$newDecimalPart"
        }
    }

    fun formatBidAskPrecision(value: String): String {
        val integers = value.getIntDigits()

        val pricePrecision =
            (if (isDirectOrder) localPricePrecision else pricePrecisionForNonDirectPair)
                ?: return ""

        return if (pricePrecision > 0) {
            var decimal = value.getDecimalDigits()
            if (decimal.length < pricePrecision) {
                val dP = pricePrecision - decimal.length
                for (i in 0 until dP) {
                    decimal += "0"
                }
                "$integers.$decimal"
            } else value
        } else {
            integers
        }
    }

    fun getCountSuccessOrder(): Int {
        return sharedPefs.countSuccessOrders
    }

    fun setCountSuccessOrder() {
        sharedPefs.countSuccessOrders += 1
    }


    companion object {
        private const val MIN_PERCENT = 0
        private const val MAX_PERCENT = 100
        private val WEB_SOCKET_URL =
            "wss://websocket-proxy-service${if (BuildConfig.FLAVOR == "prod") "" else ".staging"}.sevenb.io?apiKey="

        enum class LimitType {
            TAKE_PROFIT, STOP_LOSS
        }

        fun roundFloat(amount: Float, signs: Int = 8, estimateInBase: Boolean = false): String {
            return roundOffDecimal(amount, signs, estimateInBase)
        }

        private fun roundOffDecimal(number: Float, signs: Int, estimateInBase: Boolean): String {
            var decimalPrecision = ""

            for (i in 0 until signs) {
                decimalPrecision += "#"
            }

            val df = DecimalFormat("#${if (signs == 0) "" else ".$decimalPrecision"}")
            df.roundingMode = RoundingMode.FLOOR

            val roundedString = df.format(number)

            if (roundedString.containsComma()) {

            }

            return if (estimateInBase) {
                if (roundedString.containsDot()) {
                    val intDigits = roundedString.getIntDigits()
                    var decimalDigits = roundedString.getDecimalDigits()
                    if (decimalDigits.length < 2) {
                        while (decimalDigits.length < 2) {
                            decimalDigits += "0"
                        }
                        "$intDigits.$decimalDigits"
                    } else roundedString
                } else {
                    "$roundedString.00"
                }
            } else roundedString
        }

        fun roundDouble(amount: Double, signs: Int = 8, estimateInBase: Boolean = false): String {
            return roundOffDecimal(amount, signs, estimateInBase)
        }

        private fun roundOffDecimal(number: Double, signs: Int, estimateInBase: Boolean): String {
            var decimalPrecision = ""

            for (i in 0 until signs) {
                decimalPrecision += "#"
            }

            val df = DecimalFormat("#${if (signs == 0) "" else ".$decimalPrecision"}")
            df.roundingMode = RoundingMode.FLOOR

            val formattedString = df.format(number)
            val roundedString = if (formattedString.containsComma()) {
                formattedString.replace(",", ".")
            } else formattedString

            return if (estimateInBase) {
                if (roundedString.containsDot()) {
                    val intDigits = roundedString.getIntDigits()
                    var decimalDigits = roundedString.getDecimalDigits()
                    if (decimalDigits.length < 2) {
                        while (decimalDigits.length < 2) {
                            decimalDigits += "0"
                        }
                        "$intDigits.$decimalDigits"
                    } else roundedString
                } else {
                    "$roundedString.00"
                }
            } else roundedString
        }
    }
}

inline fun <reified T> genericType(): Type = object : TypeToken<T>() {}.type