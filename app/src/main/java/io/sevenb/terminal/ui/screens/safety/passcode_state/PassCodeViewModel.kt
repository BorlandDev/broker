package io.sevenb.terminal.ui.screens.safety.passcode_state

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.enums.pass_code.PassCodeStates
import io.sevenb.terminal.data.model.enums.pass_code.PassCodeSubStates
import io.sevenb.terminal.domain.usecase.auth.*
import io.sevenb.terminal.domain.usecase.firebase_auth.FirebaseAuthHandler
import io.sevenb.terminal.domain.usecase.safety.AESCrypt
import io.sevenb.terminal.domain.usecase.safety.AccountSafetyStoring
import io.sevenb.terminal.domain.usecase.safety.PassCodeStoring
import io.sevenb.terminal.ui.extension.removeLast
import io.sevenb.terminal.utils.Logger
import kotlinx.coroutines.*
import java.io.IOException
import javax.crypto.Cipher
import javax.inject.Inject
import kotlin.properties.Delegates

class PassCodeViewModel @Inject constructor(
    private val passCodeStoring: PassCodeStoring,
    private val safetyStoring: AccountSafetyStoring,
    private val localUserDataStoring: LocalUserDataStoring,
    private val fingerPrintRestore: FingerPrintRestore,
    private val storeFirstLaunch: StoreFirstLaunch,
    private val logOutUser: LogOutUser,
    private val storePassword: StorePassword,
    private val signInUser: SignInUser,
    private val storeRefreshToken: StoreRefreshToken,
    private val aesCrypt: AESCrypt,
    private val firebaseAuthHandler: FirebaseAuthHandler
) : ViewModel() {

    val currentState = MutableLiveData<PassCodeStates?>()
    val currentSubState = MutableLiveData<PassCodeSubStates?>()
    val passCodeStored = MutableLiveData<Boolean?>()
    val changeBiometricToBackspace = MutableLiveData<Boolean?>()
    val enteredNumbers = MutableLiveData<Int?>()
    val errorString = MutableLiveData<String?>()
    val decryptBiometricCipher = MutableLiveData<Cipher?>()

    //    var fingerPrintDone = MutableLiveData<Unit>()
    val createFingerprint = MutableLiveData<Cipher?>()
    val authDone = MutableLiveData<Unit>()
    val showLoader = MutableLiveData<String?>()

//    val requestTfa = MutableLiveData<Pair<Boolean, String>>()

    var localState = PassCodeStates.ENTER_PASS_CODE
    var localSubState = PassCodeSubStates.PASS_CODE_ENTERING_FAILED

    var savedPassCode = ""
    lateinit var email: String
    lateinit var password: String

    var errorCount = 0
    var biometricEnabled = false

    var isSubscribed = false

    var createPassCode: String by Delegates.observable("") { _, _, newValue ->
        if (newValue == "_") {
            createPassCode = createPassCode.removeLast()
        } else {
            val length = createPassCode.length
            if (length == 4) setNewCurrentState(PassCodeStates.CONFIRM_PASS_CODE)
            else enteredNumbers.value = createPassCode.length
        }
    }

    var confirmPassCode: String by Delegates.observable("") { _, _, newValue ->
        if (newValue == "_") {
            confirmPassCode = confirmPassCode.removeLast()
        } else {
            val length = confirmPassCode.length
            if (length == 4) {
                if (confirmPassCode == createPassCode) {
                    setNewSubState(PassCodeSubStates.PASS_CODE_CONFIRMING_PASSED)
                } else setNewSubState(PassCodeSubStates.PASS_CODE_CONFIRMING_ERROR)
            }
            enteredNumbers.value = confirmPassCode.length
        }
    }

    var enterPassCode: String by Delegates.observable("") { _, _, newValue ->
        val length = enterPassCode.length
        if (newValue == "_") {
            enterPassCode = enterPassCode.removeLast()
        } else {
            if (length == 4 && savedPassCode.isNotEmpty()) {
                if (enterPassCode == savedPassCode) {
                    setNewSubState(PassCodeSubStates.PASS_CODE_ENTERING_PASSED)
                } else setNewSubState(PassCodeSubStates.PASS_CODE_ENTERING_ERROR)
            }
        }

        changeBiometricToBackspace.value = length >= 1
        enteredNumbers.value = enterPassCode.length
    }

    private suspend fun getDecryptedPass(): String {
        val encryptedPass =
            when (val passRes = localUserDataStoring.restorePassword()) {
                is Result.Success -> passRes.data
                is Result.Error -> ""
            }
        return aesCrypt.decrypt(encryptedPass)
    }

    fun storePassCode() {
        viewModelScope.launch {
            if (confirmPassCode.isNotEmpty() && confirmPassCode == createPassCode) {
                val storePassCodeAsync = async { passCodeStoring.storePassCode(confirmPassCode) }
                val passCodeEnabledAsync = async { safetyStoring.storePassCodeEnabled(true) }

                passCodeStored.value =
                    storePassCodeAsync.await() is Result.Success && passCodeEnabledAsync.await() is Result.Success
            }
        }
    }

    fun passCodeCancelled() {
        viewModelScope.launch {
            val disablePassCodeResultAsync =
                async { safetyStoring.storePassCodeEnabled(false) }
            val disableFingerprintResultAsync =
                async { safetyStoring.storeFingerprintEnabled(false) }
            val removePassCodeResultAsync =
                async { passCodeStoring.storePassCode("") }
            val clearBiometryResultAsync =
                async { localUserDataStoring.clearBiometry() }

            if (
                disablePassCodeResultAsync.await() is Result.Success &&
                disableFingerprintResultAsync.await() is Result.Success &&
                removePassCodeResultAsync.await() is Result.Success &&
                clearBiometryResultAsync.await() is Result.Success
            )
                setNewSubState(PassCodeSubStates.SKIPPED)
        }
    }

    fun fingerprintCancelled() {
        viewModelScope.launch {
            val disableFingerprintResultAsync =
                async { safetyStoring.storeFingerprintEnabled(false) }
            val clearBiometryResultAsync =
                async { localUserDataStoring.clearBiometry() }

            if (disableFingerprintResultAsync.await() is Result.Success && clearBiometryResultAsync.await() is Result.Success) {
                signInAfterAccountCreated(email, password)
            }
        }
    }

    private fun restorePassCode() {
        viewModelScope.launch {
            val restorePassCodeResult = passCodeStoring.restorePassCode()

            if (restorePassCodeResult is Result.Success) {
                savedPassCode = restorePassCodeResult.data
                currentState.value = localState
            }
        }
    }

    fun setNewCurrentState(state: PassCodeStates) {
        localState = state
        if (state == PassCodeStates.ENTER_PASS_CODE || state == PassCodeStates.ENTER_PASS_CODE_BOTH_METHODS)
            restorePassCode()
        else currentState.value = state
    }

    fun setNewSubState(state: PassCodeSubStates) {
        localSubState = state
        currentSubState.value = state
    }

    fun checkFingerprintExistence() {
        viewModelScope.launch {
            val cipher = fingerPrintRestore.provideDecryptionCipher() ?: return@launch
            try {
                decryptBiometricCipher.value = cipher
            } catch (e: IllegalStateException) {
            }
        }
    }

    fun decryptPassword(cipher: Cipher?) {
        viewModelScope.launch {
            val password = fingerPrintRestore.decryptPassword(cipher ?: return@launch)
            if (password != null) {
                if (password.isNotEmpty())
                    authDone()
                else {
                    val clearBiometricAsync = async { localUserDataStoring.clearBiometry() }
                    val storeFirstLaunchAsync = async { storeFirstLaunch.setFirstLaunch(true) }
                    decryptPasswordError(clearBiometricAsync.await(), storeFirstLaunchAsync.await())
                }
            } else {
                errorString.value = "Login error: code 01"
            }
        }
    }

    fun createFingerprint() {
        val cipherPair = storePassword.provideEncryptionCipher()
        if (cipherPair.first != null && !storePassword.isBiometricSuccess()) {
            signInAfterAccountCreated(email, password)
        } else
            createFingerprint.value = cipherPair.second
    }

    private fun decryptPasswordError(await: Result<Unit>, await1: Result<Unit>) {
        errorString.value =
            "You may have changed your fingerprint. Current fingerprint in this app will be removed"
    }

    fun encryptPassword(cipher: Cipher?) {
        viewModelScope.launch {
            if (password.isNullOrEmpty()) return@launch

            val encryptionResult = storePassword.encryptPassword(
                password, cipher ?: return@launch
            )
            when (encryptionResult) {
                is Result.Success -> {
                    safetyStoring.storeFingerprintEnabled(true)
                    signInAfterAccountCreated(email, password)
                    errorString.value = "Fingerprint created"
                }
                is Result.Error -> {
                    val exception = IOException("Fingerprint creating error")
                    Logger.sendLog(
                        "PassCodeViewModel",
                        "encryptPassword",
                        exception
                    )
                    errorString.value = exception.message
                }
            }
        }
    }

    fun authDone() {
        viewModelScope.launch {
            val storeLogOutAsync = async { safetyStoring.storeLoggedOut(0) }
            val restoreLocalDataAsync = async { localUserDataStoring.restoreLocalAccountData() }

            if (storeLogOutAsync.await() is Result.Success && restoreLocalDataAsync.await() is Result.Success)
                authDone.value = Unit
        }
    }

    private fun signInUser(email: String, password: String) {
        viewModelScope.launch {
            when (val signInResult = signInUser.execute(email, password)) {
                is Result.Success -> {
                    localUserDataStoring.apply {
                        updateLocalAccountData(email)
                        val encryptedPassword = aesCrypt.encrypt(password)
                        encryptedPassword?.let { storePassword(it) }
                    }

                    authDone()
                }
                is Result.Error -> {
                    signInResult.exception.message?.let {
                        val errorLogin = if (email.isEmpty()) "0" else "1"
                        val errorPass = if (password.isEmpty()) "0" else "1"
                        val exception = IOException("Server error: code $errorLogin$errorPass")
                        Logger.sendLog(
                            "PassCodeViewModel",
                            "signInUser",
                            exception
                        )
                        setNewSubState(PassCodeSubStates.LOG_OUT)
                        errorString.value = exception.message
                    }
                }
            }
        }
    }


    fun signInAfterAccountCreated(email: String, password: String) {
        viewModelScope.launch {
            showLoader.value = "Please wait..."
            withContext(Dispatchers.IO) {
                delay(3 * 1000L)
                signInUser(email, password)
            }
        }
    }
//
//    private fun storeRefreshToken(refreshToken: String) {
//        viewModelScope.launch {
//            when (val storeResult = storeRefreshToken.execute(refreshToken)) {
//                is Result.Success -> {
//                    authDone()
//                }
//                is Result.Error -> {
//                    errorString.value = storeResult.exception.message
//                }
//            }
//        }
//    }
}