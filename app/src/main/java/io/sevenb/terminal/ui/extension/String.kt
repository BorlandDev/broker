package io.sevenb.terminal.ui.extension

fun String?.getValue(): Float {
    return try {
        this?.getFloatValue() ?: 0.0f
    } catch (e: Exception) {
        0.0f
    }
}

fun String?.containsDot(): Boolean {
    return try {
        this?.contains(".") ?: false
    } catch (e: Exception) {
        false
    }
}

fun String?.containsComma(): Boolean {
    return try {
        this?.contains(",") ?: false
    } catch (e: Exception) {
        false
    }
}

fun String?.getIntDigits(): String {
    if (this == null) return ""

    return try {
        if (this.containsDot()) {
            this.split(".")[0]
        } else this
    } catch (e: Exception) {
        this
    }
}

fun String?.getDecimalDigits(): String {
    if (this == null) return ""

    return try {
        if (this.containsDot()) {
            this.split(".")[1]
        } else this
    } catch (e: Exception) {
        this
    }
}

fun String.removeLast(): String {
    return if (this.isNotEmpty()) this.substring(0, this.length - 1)
    else this
}