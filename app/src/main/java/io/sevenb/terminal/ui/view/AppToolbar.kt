package io.sevenb.terminal.ui.view

import android.content.Context
import android.util.AttributeSet
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import io.sevenb.terminal.R

class AppToolbar : LinearLayout {

    lateinit var toolbar: Toolbar
    lateinit var clSearch: ConstraintLayout
    lateinit var ivOpenSearch: ImageView

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context) : super(context) {
        init(context)
    }

    private fun init(context: Context) {
        inflate(context, R.layout.toolbar_app, this)
        toolbar = this.findViewById(R.id.toolbar)
        clSearch = this.findViewById(R.id.cl_search)
        ivOpenSearch = this.findViewById(R.id.open_search)

        this.orientation = VERTICAL
    }

}