package io.sevenb.terminal.ui.screens.notification_settings

import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import io.sevenb.terminal.R
import io.sevenb.terminal.data.model.enums.notifications.NotificationsTypeFromBack
import io.sevenb.terminal.data.model.messages.MainNotificationsPush
import io.sevenb.terminal.ui.base.BaseFragment
import io.sevenb.terminal.ui.extension.viewModelProvider
import kotlinx.android.synthetic.main.fragment_notifications_settings.*

class NotificationsSettingsFragment : BaseFragment() {
    private val sharedNotificationsToSettings: SharedNotificationsToSettingsViewModel by activityViewModels { viewModelFactory }
    private lateinit var notificationsViewModel: NotificationSettingsViewModel

    //    private val args: NotificationsSettingsFragmentArgs by navArgs()
    private var listOfNotifications = mutableListOf<NotificationsTypeFromBack>()

    override fun layoutId() = R.layout.fragment_notifications_settings
    override fun onSearchTapListener() {}

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        notificationsViewModel = viewModelProvider(viewModelFactory)

        subscribeUI()
    }

    private fun subscribeUI() {
        viewLifecycleOwner.let { owner ->
            notificationsViewModel.apply {
                enableOrderNotifications.observe(owner, { processEnableOrder(it) })
                enableWithdrawNotifications.observe(owner, { processEnableWithdrawal(it) })
                enableDepositNotifications.observe(owner, { processEnableDeposit(it) })
                errorMessage.observe(owner, { processErrorMessage(it) })
            }

            sharedNotificationsToSettings.apply {
                mainNotificationsList.observe(owner, { enableNotifications(it) })
//                enabledNotificationsOnBack.observe(owner, { showEnabledNotifications(it) })
            }
        }

    }

    private fun processErrorMessage(message: String?) {
        println()
    }

    private fun processEnableDeposit(value: Boolean?) {
        value?.let {
            smNotificationDepositEnabling.isChecked = it
            if (it) listOfNotifications.add(NotificationsTypeFromBack.DEPOSIT_COMPLETE)
            else listOfNotifications.remove(NotificationsTypeFromBack.DEPOSIT_COMPLETE)
            checkNotificationsList()
        }
    }

    private fun processEnableWithdrawal(value: Boolean?) {
        value?.let {
            smNotificationWithdrawalEnabling.isChecked = it
            if (it) listOfNotifications.add(NotificationsTypeFromBack.WITHDRAW_COMPLETE)
            else listOfNotifications.remove(NotificationsTypeFromBack.WITHDRAW_COMPLETE)
            checkNotificationsList()
        }
    }

    private fun processEnableOrder(value: Boolean?) {
        value?.let {
            smNotificationOrderEnabling.isChecked = it
            if (it) listOfNotifications.add(NotificationsTypeFromBack.ORDER_COMPLETE)
            else listOfNotifications.remove(NotificationsTypeFromBack.ORDER_COMPLETE)
            checkNotificationsList()
        }
    }

    private fun checkNotificationsList() {
        sharedNotificationsToSettings.enableNotifications.value =
            listOfNotifications.toMutableList()

        if (listOfNotifications.isEmpty()) navigateBack()
    }

    private fun initViews() {
        ivNotificationSettingsBack.setOnClickListener { navigateBack() }

        notificationsViewModel.apply {
            val withdrawItem =
                listMainNotificationsPush.firstOrNull { it.template.template_type == NotificationsTypeFromBack.WITHDRAW_COMPLETE }
            val depositItem =
                listMainNotificationsPush.firstOrNull { it.template.template_type == NotificationsTypeFromBack.DEPOSIT_COMPLETE }
            val orderItem =
                listMainNotificationsPush.firstOrNull { it.template.template_type == NotificationsTypeFromBack.ORDER_COMPLETE }

            val deposit =
                if (depositItem != null) listMainNotificationsPush[listMainNotificationsPush.indexOf(
                    depositItem
                )]
                else depositItem

            val withdraw =
                if (withdrawItem != null) listMainNotificationsPush[listMainNotificationsPush.indexOf(
                    withdrawItem
                )]
                else withdrawItem

            val order =
                if (orderItem != null) listMainNotificationsPush[listMainNotificationsPush.indexOf(
                    orderItem
                )]
                else orderItem

            clNotificationOrder.setOnClickListener {
                if (order != null) enableNotification(!smNotificationOrderEnabling.isChecked, order)
            }
            vNotificationOrderEnabling.setOnClickListener {
                if (order != null) enableNotification(!smNotificationOrderEnabling.isChecked, order)
            }

            clNotificationWithdrawal.setOnClickListener {
                if (withdraw != null)
                    enableNotification(!smNotificationWithdrawalEnabling.isChecked, withdraw)
            }
            vNotificationWithdrawalEnabling.setOnClickListener {
                if (withdraw != null)
                    enableNotification(!smNotificationWithdrawalEnabling.isChecked, withdraw)
            }

            clNotificationDeposit.setOnClickListener {
                if (deposit != null)
                    enableNotification(!smNotificationDepositEnabling.isChecked, deposit)
            }
            vNotificationDepositEnabling.setOnClickListener {
                if (deposit != null)
                    enableNotification(!smNotificationDepositEnabling.isChecked, deposit)
            }
        }
    }

    private fun enableNotifications(listOfNotifications: List<MainNotificationsPush>) {
        notificationsViewModel.listMainNotificationsPush = listOfNotifications.toMutableList()
        val listOfEnabledNotifications = mutableListOf<NotificationsTypeFromBack>()
//        val defaultNotificationList = mutableListOf(
//            NotificationsTypeFromBack.WITHDRAW_COMPLETE,
//            NotificationsTypeFromBack.DEPOSIT_COMPLETE,
//            NotificationsTypeFromBack.ORDER_COMPLETE
//        )

        listOfNotifications.forEach {
            if (it.is_enabled)
                this.listOfNotifications.add(it.template.template_type)
            if (it.template.template_type != null)
                when (it.template.template_type) {
                    NotificationsTypeFromBack.WITHDRAW_COMPLETE -> {
                        listOfEnabledNotifications.add(NotificationsTypeFromBack.WITHDRAW_COMPLETE)
                        smNotificationWithdrawalEnabling.isChecked = it.is_enabled
                    }
                    NotificationsTypeFromBack.DEPOSIT_COMPLETE -> {
                        listOfEnabledNotifications.add(NotificationsTypeFromBack.DEPOSIT_COMPLETE)
                        smNotificationDepositEnabling.isChecked = it.is_enabled
                    }
                    NotificationsTypeFromBack.ORDER_COMPLETE -> {
                        listOfEnabledNotifications.add(NotificationsTypeFromBack.ORDER_COMPLETE)
                        smNotificationOrderEnabling.isChecked = it.is_enabled
                    }
                }
        }

//        defaultNotificationList.forEach {
//            if (!listOfEnabledNotifications.contains(it)) {
//                when (it) {
//                    NotificationsTypeFromBack.WITHDRAW_COMPLETE ->
//                        clNotificationWithdrawal.isVisible = false
//                    NotificationsTypeFromBack.DEPOSIT_COMPLETE ->
//                        clNotificationDeposit.isVisible = false
//                    NotificationsTypeFromBack.ORDER_COMPLETE ->
//                        clNotificationOrder.isVisible = false
//                }
//            }
//        }

        initViews()
    }

    private fun navigateBack() {
        findNavController().popBackStack()
    }
}