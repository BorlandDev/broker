package io.sevenb.terminal.ui.screens.withdraw

import android.app.Activity
import android.content.Intent
import android.graphics.Point
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.RelativeSizeSpan
import android.view.Display
import android.view.View
import android.widget.TextView
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.updateLayoutParams
import androidx.core.widget.doOnTextChanged
import androidx.navigation.fragment.findNavController
//import androidx.window.layout.WindowMetricsCalculator
import com.google.android.material.snackbar.Snackbar
import io.sevenb.terminal.R
import io.sevenb.terminal.data.model.broker_api.rates.MarketRate
import io.sevenb.terminal.data.model.enum_model.ErrorMessageType
import io.sevenb.terminal.data.model.withdrawal.WithdrawItem
import io.sevenb.terminal.data.model.withdrawal.WithdrawalHistoryItem
import io.sevenb.terminal.data.repository.auth.PreferenceRepository
import io.sevenb.terminal.device.utils.DateTimeUtil
import io.sevenb.terminal.device.utils.DateTimeUtil.parseToTimestamp
import io.sevenb.terminal.domain.usecase.charts.AccountData
import io.sevenb.terminal.ui.base.BaseBottomSheetFragment
import io.sevenb.terminal.ui.extension.*
import io.sevenb.terminal.ui.screens.order.OrderViewModel.Companion.roundDouble
import io.sevenb.terminal.ui.screens.scan_qr.DecoderActivity
import io.sevenb.terminal.ui.screens.scan_qr.DecoderActivity.Companion.QR_CODE_RESULT
import io.sevenb.terminal.ui.screens.select_currency.BaseSelectItem
import io.sevenb.terminal.ui.screens.trading.SharedViewModel
import io.sevenb.terminal.ui.screens.trading.TradingListAdapter.Companion.formatCryptoAmount
import kotlinx.android.synthetic.main.fragment_loader.*
import kotlinx.android.synthetic.main.fragment_withdraw.*
import kotlinx.android.synthetic.main.fragment_withdraw_confirm.*
import kotlinx.android.synthetic.main.fragment_withdraw_main.*
import kotlinx.android.synthetic.main.fragment_withdraw_success.*
import java.math.BigDecimal


/**
 * Created by samosudovd on 24/01/2020.
 */
class WithdrawBottomSheetFragment : BaseBottomSheetFragment() {

    private lateinit var withdrawViewModel: WithdrawViewModel
    private lateinit var sharedViewModel: SharedViewModel

    private var withdrawItem: WithdrawItem? = null
    private var localWithdrawalList: List<WithdrawItem> = mutableListOf()

    override fun layoutId() = R.layout.fragment_withdraw
    override fun containerId() = R.id.container_withdrawal_sized

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        withdrawViewModel = viewModelProvider(viewModelFactory)
        sharedViewModel = activity?.run {
            viewModelProvider(viewModelFactory)
        } ?: throw Exception("Invalid Activity")
        initView()
        subscribeUi()
    }

    private fun initView() {
        startShimmer(true)
        close_screen.setOnClickListener { dismiss() }

        et_withdraw.doOnTextChanged { text, _, _, _ ->
            cl_withdraw_container.setDefaultBackground()
            estimatedWithdrawal(text.toString())
        }

        input_withdraw_address.doOnTextChanged { _, _, _, _ ->
            cl_withdraw_address.setDefaultBackground()
        }

        input_withdraw_extra_id.doOnTextChanged { _, _, _, _ ->
            cl_withdraw_extra_id.setDefaultBackground()
        }

        cl_withdraw_input.setOnClickListener {
            et_withdraw.focusField(requireContext())
        }

        ll_resend_code_withdraw.setOnClickListener {
            withdrawViewModel.makeWithdrawalRequest()
        }

        cl_withdraw.setOnClickListener {
            sharedViewModel.selectCurrencyList.value = localWithdrawalList
            currencySelection()
        }
        iv_qr_scanner.setOnClickListener { openQrScanner() }

        label_twenty_five_percent_withdraw.setOnClickListener {
            percentButtons(25)
            selectPercents(it)
        }
        label_fifty_percent_withdraw.setOnClickListener {
            percentButtons(50)
            selectPercents(it)
        }
        label_hundred_percent_withdraw.setOnClickListener {
            percentButtons(100)
            selectPercents(it)
        }

        val display: Display? = activity?.windowManager?.defaultDisplay
        val size = Point()
        display?.getSize(size)
        val screenHeight: Int = size.y

        bg_swipe.updateLayoutParams { height = screenHeight }
        bg_swipe_dismiss_withdraw_success.updateLayoutParams { height = screenHeight }

        (container_withdrawal_market as MotionLayout).setTransitionListener(object :
            MotionLayout.TransitionListener {
            override fun onTransitionStarted(p0: MotionLayout?, p1: Int, p2: Int) {}

            override fun onTransitionChange(p0: MotionLayout?, p1: Int, p2: Int, p3: Float) {}

            override fun onTransitionCompleted(p0: MotionLayout?, p1: Int) {
                if (p0?.endState == p0?.currentState) {
                    if (!validateFields()) return

                    withdrawViewModel.withdrawalRequest(
                        withdrawItem ?: return,
                        et_withdraw.getString(),
                        input_withdraw_address.getString(),
                        input_withdraw_extra_id.getString(),
                    )
                } else swipeAnimationToStart()
            }

            override fun onTransitionTrigger(p0: MotionLayout?, p1: Int, p2: Boolean, p3: Float) {}
        })

        (container_withdrawal_success as MotionLayout).setTransitionListener(object :
            MotionLayout.TransitionListener {
            override fun onTransitionStarted(p0: MotionLayout?, p1: Int, p2: Int) {}

            override fun onTransitionChange(p0: MotionLayout?, p1: Int, p2: Int, p3: Float) {}

            override fun onTransitionCompleted(p0: MotionLayout?, p1: Int) {
                if (p0?.endState == p0?.currentState)
                    dismiss()
            }

            override fun onTransitionTrigger(p0: MotionLayout?, p1: Int, p2: Boolean, p3: Float) {}
        })

        button_verify_code.setOnClickListener {
            withdrawalConfirm()
        }

        button_copy_hash.setOnClickListener {
            copyHash()
        }

        input_verification_code.onActionDoneListener {
            withdrawalConfirm()
        }
    }

    private fun validateFields(): Boolean {
        val withdrawIsEmpty = et_withdraw.getString().isEmpty()
        val walletAddressIsEmpty = input_withdraw_address.getString().isEmpty()
        val extraIdIsEmpty = if (withdrawItem?.hasMemo == true) {
            input_withdraw_extra_id.getString().isEmpty()
        } else false

        when {
            withdrawIsEmpty && walletAddressIsEmpty && extraIdIsEmpty -> {
                showSnackbar(getString(R.string.error_empty_field))
                cl_withdraw_container.setErrorBackground()
                cl_withdraw_address.setErrorBackground()
                cl_withdraw_extra_id.setErrorBackground()
            }
            withdrawIsEmpty && walletAddressIsEmpty && withdrawItem?.hasMemo == false -> {
                showSnackbar(getString(R.string.error_empty_field))
                cl_withdraw_container.setErrorBackground()
                cl_withdraw_address.setErrorBackground()
            }
            withdrawIsEmpty -> {
                showSnackbar(getString(R.string.message_empty_withdraw_amount))
                cl_withdraw_container.setErrorBackground()
            }
            walletAddressIsEmpty -> {
                showSnackbar(getString(R.string.message_empty_wallet_address))
                cl_withdraw_address.setErrorBackground()
            }
            withdrawItem?.hasMemo == true && extraIdIsEmpty -> {
                showSnackbar(getString(R.string.message_empty_extra_id))
                cl_withdraw_extra_id.setErrorBackground()
            }
        }

        return !withdrawIsEmpty && !walletAddressIsEmpty && !extraIdIsEmpty
    }

    private fun withdrawalConfirm() {
        showProgressScreen(true, getString(R.string.message_confirming_withdrawal_request))
        withdrawViewModel.withdrawalConfirm(input_verification_code.text.toString())
    }

    private fun copyHash() {
        if (withdrawViewModel.hash != null) {
            withdrawViewModel.copyToClipboard(withdrawViewModel.hash!!)
            showSnackbar(getString(R.string.message_hash_copied))
        }
    }

    private fun selectPercents(view: View) {
        defaultViews()
        view.newBackground(R.drawable.bg_blue_stroke_four)
        (view as TextView).newTextColor(R.color.colorSelectPercent)
    }

    private fun defaultViews() {
        setDefaultPercent(label_twenty_five_percent_withdraw)
        setDefaultPercent(label_fifty_percent_withdraw)
        setDefaultPercent(label_hundred_percent_withdraw)
    }

    private fun setDefaultPercent(textView: TextView) {
        textView.newBackground(R.drawable.bg_white_four)
        textView.newTextColor(R.color.colorTextMain)
    }

    private fun swipeAnimationToStart() {
        (container_withdrawal_market as MotionLayout).transitionToStart()
    }

    private fun subscribeUi() {
        withdrawViewModel.apply {
            listOfWithdrawal.observe(viewLifecycleOwner, { withdrawalList(it) })
            withdrawalCreated.observe(viewLifecycleOwner, { withdrawalCreated(it) })
            withdrawalResult.observe(viewLifecycleOwner, { withdrawalResult(it) })
            dailyLimit.observe(viewLifecycleOwner, { dailyLimitEstimated(it) })
            snackbarMessage.observe(viewLifecycleOwner, { showMessage(it) })
            customErrorMessage.observe(viewLifecycleOwner, { showSnackbar(it) })
            zeroBalance.observe(viewLifecycleOwner, { isZeroBalance(it) })
            timerValue.observe(viewLifecycleOwner, { timerValue(it) })
            showResendButton.observe(viewLifecycleOwner, { showResendButton(it) })
            rateToBaseCurrency.observe(viewLifecycleOwner, { processEstimatedInDollars(it) })
            ratesList.observe(viewLifecycleOwner, { processWithdrawRates(it) })
            usdtWithdraw.observe(viewLifecycleOwner, { processUsdtWithdraw(it) })
        }

        sharedViewModel.selectedCurrency.observe(viewLifecycleOwner, { tickerSelection(it) })
    }

    private fun processEstimatedInDollars(rate: Float?) {
        rate?.let {
            val estimatedValue = it * et_withdraw.getDoubleValue()
            val estimated = roundDouble(estimatedValue, 2, true)
            tv_estimated_et_withdraw.text = "$$estimated"
            startAmountEtWithdrawal(false)
        }
    }

    private fun processUsdtWithdraw(value: Boolean?) {
        value?.let {
            if (it) {
                val estimatedValue = et_withdraw.getDoubleValue()
                val estimated = roundDouble(estimatedValue, 2, true)
                tv_estimated_et_withdraw.text = "$$estimated"
                startAmountEtWithdrawal(false)
            }
        }
    }

    private fun processWithdrawRates(ratesList: MutableMap<String, MarketRate?>?) {
        if (!ratesList.isNullOrEmpty()) {
            sharedViewModel.ratesList = ratesList
        }
    }

    private fun isZeroBalance(value: Boolean) {
        enableView(!value)
        swipeToConfirmLabelState(!value)
        if (value) withdrawViewModel.requestDailyLimitEstimated(AccountData.BTC)
    }

    private fun swipeToConfirmLabelState(value: Boolean) {
        val color = if (value) R.color.colorSelectPercent else R.color.colorRed
        val textId = if (value) R.string.order_swipe_confirm_title else R.string.label_zero_balance
        swipe_to_confirm_title.isAllCaps = value
        swipe_to_confirm_title.text = getString(textId)
        swipe_to_confirm_title.newTextColor(color)
    }

    private fun enableView(value: Boolean) {
        setEnabledViews(
            value,
            iv_qr_scanner,
            label_twenty_five_percent_withdraw,
            label_fifty_percent_withdraw,
            label_hundred_percent_withdraw,
            button_verify_code,
            cl_withdraw_input,
            et_withdraw,
            cl_withdraw,
            picker_ticker,
            ticker_to,
            cl_withdraw_container,
            input_withdraw_address,
            input_withdraw_extra_id
        )
        swipe_arrow.visibility = if (value) View.VISIBLE else View.INVISIBLE
        swipe_to_confirm.background =
            if (value) ResourcesCompat.getDrawable(
                resources,
                R.drawable.bg_swipe_rounded,
                null
            ) else null
        swipe_to_confirm_title.visibility = if (value) View.VISIBLE else View.INVISIBLE
        (container_withdrawal_market as MotionLayout).getTransition(R.id.withdraw_transition)
            .setEnable(value)
        bg_swipe.visible(value)
    }

    private fun currencySelection() {
        if (findNavController().currentDestination?.id != R.id.withdrawBottomSheetFragment) return

        val direction =
            WithdrawBottomSheetFragmentDirections.actionWithdrawBottomSheetFragmentToSelectCurrencyFragment()
        findNavController().navigate(direction)
    }

    private fun withdrawalList(list: List<WithdrawItem>) {
        localWithdrawalList = list
        tickerSelection(list.firstOrNull() ?: return)
    }

    private fun withdrawalCreated(value: Boolean) {
        val message = getString(R.string.message_creating_withdrawal_request)
        if (value) confirmationScreen()
        else swipeAnimationToStart()

        showProgressScreen(!value, message)
    }

    private fun showProgressScreen(value: Boolean, message: String = "") {
        container_loader.visible(value)
        tv_message.text = message
    }

    private fun confirmationScreen() {
        setInvisibleViews(container_withdrawal_market)
        setVisibleViews(cl_confirmation)
        email_code_sent_to.text = getString(R.string.label_registration_email)
        swipeAnimationToStart()
        input_verification_code.focusField(requireContext())
    }

    private fun timerValue(value: String) {
        timer_resend_code_withdraw.text = "0:%s".format(value)
    }

    private fun showResendButton(isShow: Boolean) {
        setViewsVisibility(!isShow, ll_resend_timer_withdraw)
        setViewsVisibility(isShow, ll_resend_code_withdraw)
    }

    private fun withdrawalResult(withdrawalHistoryItem: Pair<String, WithdrawalHistoryItem>) {
        setGoneViews(container_withdrawal_market, cl_confirmation)
        setVisibleViews(container_withdrawal_success)
        showProgressScreen(false)

        if (withdrawalHistoryItem.second.createdAt.isNotEmpty()) {
            val timestamp = parseToTimestamp(withdrawalHistoryItem.second.createdAt)
            val date = DateTimeUtil.dateFromTimestamp(timestamp)
            val time = DateTimeUtil.timeFromTimestamp(timestamp)
            withdrawal_date_created.text = "Created at %s %s".format(date, time)
        }

        val withdrawAmount = formatCryptoAmount(withdrawalHistoryItem.second.amount)
        withdrawal_amount_ticker.text = ticker_to.text
        withdrawal_amount_base_ticker.text = PreferenceRepository.baseCurrencyTicker
        withdrawal_amount.text = withdrawAmount
        if (withdrawViewModel.rate != null) {
            val estimated = "${
                withdrawAmount.toFloatOrNull()?.times(withdrawViewModel.rate!!)?.let {
                    roundDouble(it.toDouble(), 2, true)
                }
            }"
            withdrawal_amount_base.text = estimated
        }

        sharedViewModel.shouldUpdateBalance.value = true
        withdrawal_id_title.text = pendingTransaction
    }

    private fun tickerSelection(baseSelectItem: BaseSelectItem?) {
        baseSelectItem?.let {
            if (baseSelectItem !is WithdrawItem) return
            sharedViewModel.selectedCurrency.value = null

            startDailyLimitWithdrawal(true)
            startAmountEtWithdrawal(true)

            withdrawViewModel.requestDailyLimitEstimated(baseSelectItem.ticker)
            withdrawViewModel.findMarket(baseSelectItem.ticker)

            withdrawItem = baseSelectItem
            ticker_to.text = baseSelectItem.ticker
            withdrawViewModel.marketRate(baseSelectItem.ticker)
            et_withdraw.setText(baseSelectItem.amount)
            available_amount.text = "%s %s".format(baseSelectItem.amount, baseSelectItem.ticker)
            estimated_withdrawal_fee.text = "%s %s".format(
                BigDecimal(baseSelectItem.withdrawFee.toString()),
                baseSelectItem.ticker
            )
            val hintString = "%s (%s %s) %s".format(
                baseSelectItem.ticker,
                baseSelectItem.network,
                "network",
                "address"
            )
            val span = SpannableString(hintString)
            span.setSpan(
                RelativeSizeSpan(0.8f),
                0,
                hintString.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            input_withdraw_address.hint = span

            baseSelectItem.amount?.let { estimatedWithdrawal(it) }
            if (withdrawItem?.hasMemo == true) {
                setVisibleViews(cl_withdraw_extra_id, title_withdraw_extra_id)
            } else {
                setGoneViews(cl_withdraw_extra_id, title_withdraw_extra_id)
            }

            startShimmer(false)
        }
    }

    private fun dailyLimitEstimated(estimated: String) {
        startDailyLimitWithdrawal(false)
        amount_daily_limit.text = estimated
    }

    private fun estimatedWithdrawal(amount: String) {
        val amountFloat = amount.toFloatOrNull()
        var estimated = amountFloat?.minus(withdrawItem?.withdrawFee ?: 0.0f) ?: 0.0f
        if (estimated < 0.0f) estimated = 0.0f
        estimated_withdrawal_amount.text =
            "%s %s".format(formatCryptoAmount(estimated), withdrawItem?.ticker ?: "")
        withdrawViewModel.makeEstimate(ticker_to.text.toString())
    }

    private fun openQrScanner() {
        val intent = Intent(activity, DecoderActivity::class.java)
        startActivityForResult(intent, QR_CODE_REQUEST_ADDRESS)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode != Activity.RESULT_OK) return

        val result = data?.getStringExtra(QR_CODE_RESULT)
        if (result.isNullOrEmpty()) return

        when (requestCode) {
            QR_CODE_REQUEST_ADDRESS -> input_withdraw_address.setText(result)
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun percentButtons(percent: Int) {
        val amount = withdrawItem?.amount?.toFloatOrNull() ?: 0.0f
        val percented = amount * percent / 100
        et_withdraw.tryText(formatCryptoAmount(percented))
    }

    private fun showMessage(errorMessageType: ErrorMessageType) {
        when (errorMessageType) {
            ErrorMessageType.EMPTY_FIELDS -> showSnackbar(getString(R.string.error_empty_field))
            ErrorMessageType.VERIFY_ACCOUNT -> showSnackbar(getString(R.string.error_not_verified))
            ErrorMessageType.WITHDRAWAL_DAILY_LIMIT -> showSnackbar(getString(R.string.error_withdrawal_daily_limit))
        }
    }

    private fun showSnackbar(message: String) {
        Snackbar.make(dialog?.window?.decorView ?: return, message, Snackbar.LENGTH_SHORT).show()
        swipeAnimationToStart()
        showProgressScreen(false)
    }

    private fun startShimmer(value: Boolean) {
        if (value) {
            startDailyLimitWithdrawal(value)
            startAmountEtWithdrawal(value)

            sfl_amount_tv.startShimmerAnimation()
            sfl_estimated_withdrawal.startShimmerAnimation()
            sfl_withdrawal_fee.startShimmerAnimation()

            setInvisibleViews(cl_estimated_withdrawal)
            setInvisibleViews(cl_fee_withdrawal)
        } else {
            sfl_amount_tv.stopShimmerAnimation()
            sfl_estimated_withdrawal.stopShimmerAnimation()
            sfl_withdrawal_fee.stopShimmerAnimation()

            setVisibleViews(cl_estimated_withdrawal)
            setVisibleViews(cl_fee_withdrawal)
        }

        sfl_amount_tv.visible(value)
        sfl_estimated_withdrawal.visible(value)
        sfl_withdrawal_fee.visible(value)
    }

    private fun startDailyLimitWithdrawal(value: Boolean) {
        if (value) {
            sfl_daily_withdrawal_limit.startShimmerAnimation()
            setInvisibleViews(cl_withdrawal_daily_limit)
        } else {
            sfl_daily_withdrawal_limit.stopShimmerAnimation()
            setVisibleViews(cl_withdrawal_daily_limit)
        }

        sfl_daily_withdrawal_limit.visible(value)
    }

    private fun startAmountEtWithdrawal(value: Boolean) {
        if (value) {
            sfl_amount_et.startShimmerAnimation()
            setInvisibleViews(cl_withdraw_container)
        } else {
            sfl_amount_et.stopShimmerAnimation()
            setVisibleViews(cl_withdraw_container)
        }

        sfl_amount_et.visible(value)
    }

    override fun onPause() {
        startShimmer(false)
        startDailyLimitWithdrawal(false)
        startAmountEtWithdrawal(false)
        super.onPause()
    }

    override fun onDestroy() {
        sharedViewModel.ratesList = mutableMapOf()
        super.onDestroy()
    }

    companion object {
        private const val QR_CODE_REQUEST_ADDRESS = 0

        fun shortHash(hash: String, length: Int = 20): String {
            if (hash.length <= length) return hash
            return "${hash.take(length / 2)}...${hash.takeLast(length / 2)}"
        }

        private const val pendingTransaction = "The transaction is being processed.\n" +
                "\n" +
                "You can check its status in the History"
    }

}