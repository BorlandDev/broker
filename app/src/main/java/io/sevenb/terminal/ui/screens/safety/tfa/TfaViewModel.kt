package io.sevenb.terminal.ui.screens.safety.tfa

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.enum_model.ErrorMessageType
import io.sevenb.terminal.data.model.enums.CommonState
import io.sevenb.terminal.data.model.enums.FocusableFields
import io.sevenb.terminal.data.model.enums.auth.NewAuthStates
import io.sevenb.terminal.data.model.enums.auth.TfaState
import io.sevenb.terminal.data.model.tfa.TfaVerification
import io.sevenb.terminal.domain.usecase.auth.LocalUserDataStoring
import io.sevenb.terminal.domain.usecase.deposit.ClipboardCopy
import io.sevenb.terminal.domain.usecase.safety.AESCrypt
import io.sevenb.terminal.domain.usecase.tfa.QrGenerator
import io.sevenb.terminal.domain.usecase.tfa.TfaHandler
import io.sevenb.terminal.domain.usecase.validate.CheckCredentials
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class TfaViewModel @Inject constructor(
    private val localUserDataStoring: LocalUserDataStoring,
    private val clipboardCopy: ClipboardCopy,
    private val qrGenerator: QrGenerator,
    private val tfaHandler: TfaHandler,
    private val aesCrypt: AESCrypt,
    private val checkCredentials: CheckCredentials

) : ViewModel() {

    val setGlobalStateToActivity = MutableLiveData<CommonState?>()
    val getGlobalStateFromActivity = MutableLiveData<CommonState?>()

    val newState = MutableLiveData<TfaState>()

    val showProgress = MutableLiveData<Boolean>()
    val errorString = MutableLiveData<String>()
    val focusField = MutableLiveData<FocusableFields?>()
    val showLoader = MutableLiveData<Pair<Boolean, String>>()

    //    val authDone = MutableLiveData<Unit>()
    val createPassCode = MutableLiveData<Pair<String, String>>()

    var isSubscribed = false

    lateinit var email: String
    lateinit var password: String
    lateinit var secretKey: String

    var state: TfaState = TfaState.INFORMATION

    fun loadEmail() {
        viewModelScope.launch {
            val restoreLocalDataAsync = async { localUserDataStoring.restoreLocalAccountData() }
            if (restoreLocalDataAsync.await() is Result.Success) {
                email = localUserDataStoring.localAccountData.email
                when (val passwordResult = localUserDataStoring.restorePassword()) {
                    is Result.Success -> {
                        val encryptedPassword = passwordResult.data
                        password = aesCrypt.decrypt(encryptedPassword)
                    }
                }
            }
        }
    }

    fun setNewState(newState: TfaState) {
        state = newState
        this.newState.value = newState
    }

    fun copyToClipboard() = clipboardCopy.copyToClipboard(secretKey)

    fun pasteIsEnabled() = clipboardCopy.pasteIsEnabled()

    fun textToPaste() = clipboardCopy.textToPaste()

    fun getQrCodeBitmap() = qrGenerator.getQrCodeBitmap(
        TfaHandler.GOOGLE_AUTHENTICATOR_FORMAT.format(
            "Stash Tech Solutions Limited",
            secretKey,
            "7b - crypto broker",
        )
    )

    fun showProgress(value: Boolean) {
        showProgress.value = value
    }

    fun focusField(focusableField: FocusableFields?) {
        focusField.value = focusableField
    }

    fun showLoader(value: Boolean, title: String = "") {
        showLoader.value = Pair(value, title)
    }

    fun verify(code: String) {
        viewModelScope.launch {
            when (val verifyResult = tfaHandler.verify(code, email, password)) {
                is Result.Success -> {
                    when (newState.value) {
                        TfaState.VERIFICATION_ONLY -> {
                            createPassCode.value = Pair(email, password)
                        }
                        TfaState.VERIFICATION -> {
                            setNewState(TfaState.IS_ON)
                        }
                    }
                }
                is Result.Error -> {
                    errorString.value = verifyResult.exception.message
                }
            }
        }
    }

    fun toggle() {
        viewModelScope.launch {
            when (val verifyResult = tfaHandler.toggleTfa()) {
                is Result.Success -> {
                    secretKey = verifyResult.data.secret
                    setNewState(TfaState.SECRET)
                }
                is Result.Error -> errorString.value = verifyResult.message
            }
        }
    }

    fun validateCode(code: String) =
        checkCredentials.checkVerificationCode(code, ErrorMessageType.TFA_VERIFICATION)

}
