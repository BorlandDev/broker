package io.sevenb.terminal.ui.screens.alert.fragment

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import io.sevenb.terminal.AppActivity
import io.sevenb.terminal.R
import io.sevenb.terminal.ui.base.BaseBottomSheetFragment
import io.sevenb.terminal.ui.base.adapter.TabFragmentAdapter
import kotlinx.android.synthetic.main.activity_app.*
import kotlinx.android.synthetic.main.fragment_price_alert.*

class PriceAlertFragment : BaseBottomSheetFragment() {

    override fun layoutId() = R.layout.fragment_price_alert
    override fun containerId() = R.id.container_price_alert
    var name = ""
    var price = ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = TabFragmentAdapter(childFragmentManager)

        name = arguments?.getString("name").toString()
        price = arguments?.getString("price").toString()

        adapter.addFragment(PriceFragment.newInstance(), "Price")
        adapter.addFragment(PercentFragment.newInstance(), "Percent")

        vp_main_deposit.adapter = adapter
        tl_test.setupWithViewPager(vp_main_deposit)

        close_screen.setOnClickListener {
            dismiss()
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog

        dialog.setOnShowListener {
            val bottomSheet =
                (it as BottomSheetDialog).findViewById<View>(R.id.design_bottom_sheet) as FrameLayout?
            val behavior = BottomSheetBehavior.from(bottomSheet!!)
            behavior.state = BottomSheetBehavior.STATE_EXPANDED

            behavior.addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
                override fun onStateChanged(bottomSheet: View, newState: Int) {
                    if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                        behavior.state = BottomSheetBehavior.STATE_EXPANDED
                    }
                }

                override fun onSlide(bottomSheet: View, slideOffset: Float) {}
            })
        }

        return dialog
    }

    override fun onDestroy() {
        super.onDestroy()
        (activity as AppActivity).appToolbar.visibility = View.VISIBLE

        (activity as AppActivity).initToolbar(
            R.string.title_trading_tab,
            false,
            false
        )
        (activity as AppActivity).bottomNavView.visibility = View.VISIBLE
    }
}