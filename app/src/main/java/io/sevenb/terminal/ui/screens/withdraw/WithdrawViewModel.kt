package io.sevenb.terminal.ui.screens.withdraw

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.sevenb.terminal.data.model.broker_api.markets.Market
import io.sevenb.terminal.data.model.broker_api.rates.MarketRateParams
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.enum_model.BinanceFilter
import io.sevenb.terminal.data.model.enum_model.ErrorMessageType
import io.sevenb.terminal.data.model.filter.Filters
import io.sevenb.terminal.data.model.withdrawal.ConfirmWithdrawalParams
import io.sevenb.terminal.data.model.withdrawal.WithdrawItem
import io.sevenb.terminal.data.model.withdrawal.WithdrawalHistoryItem
import io.sevenb.terminal.data.model.withdrawal.WithdrawalRequest
import io.sevenb.terminal.data.repository.transactionmask.TransactionExplorerMaskRepository
import io.sevenb.terminal.device.utils.DateTimeUtil
import io.sevenb.terminal.domain.usecase.auth.StartResendTimer
import io.sevenb.terminal.domain.usecase.charts.AccountData
import io.sevenb.terminal.domain.usecase.charts.AccountData.Companion.getBaseCurrencyTicker
import io.sevenb.terminal.domain.usecase.deposit.ClipboardCopy
import io.sevenb.terminal.domain.usecase.order.MarketsData
import io.sevenb.terminal.domain.usecase.sort.SortCurrenciesHandler
import io.sevenb.terminal.domain.usecase.verification.VerificationData
import io.sevenb.terminal.domain.usecase.verification.VerificationData.Companion.ACCOUNT_STATUS_VERIFIED
import io.sevenb.terminal.domain.usecase.withdrawal.EstimatedDailyLimit
import io.sevenb.terminal.domain.usecase.withdrawal.RequestWithdrawal
import io.sevenb.terminal.domain.usecase.withdrawal.RequestWithdrawalHash
import io.sevenb.terminal.domain.usecase.withdrawal.WithdrawalConfirm
import io.sevenb.terminal.ui.screens.order.OrderViewModel
import io.sevenb.terminal.utils.ConnectionStateMonitor
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class WithdrawViewModel @Inject constructor(
    private val accountData: AccountData,
    private val requestWithdrawal: RequestWithdrawal,
    private val verificationData: VerificationData,
    private val withdrawalConfirm: WithdrawalConfirm,
    private val estimatedDailyLimit: EstimatedDailyLimit,
    private val transactionExplorerMaskRepository: TransactionExplorerMaskRepository,
    private val clipboardCopy: ClipboardCopy,
    private val startResendTimer: StartResendTimer,
    private val requestWithdrawalHash: RequestWithdrawalHash,
    private val marketsData: MarketsData,
    private val sortCurrenciesHandler: SortCurrenciesHandler
) : ViewModel() {

    val listOfWithdrawal = MutableLiveData<List<WithdrawItem>>()
    val withdrawalCreated = MutableLiveData<Boolean>()
    val withdrawalResult = MutableLiveData<Pair<String, WithdrawalHistoryItem>>()
    val dailyLimit = MutableLiveData<String>()
    val snackbarMessage = MutableLiveData<ErrorMessageType>()
    val customErrorMessage = MutableLiveData<String>()
    val zeroBalance = MutableLiveData<Boolean>()
    val showResendButton = MutableLiveData<Boolean>()
    val timerValue = MutableLiveData<String>()
    val usdtWithdraw = MutableLiveData<Boolean?>()

    var tempWithdrawalRequest: WithdrawalRequest? = null

    var hash: String? = null

    var showTimeError = 0L

    private lateinit var withdrawal: WithdrawalHistoryItem
    var rate: Float? = null

    val market = MutableLiveData<Pair<Market?, Boolean>?>()
    val marketDataIsReady = MutableLiveData<Int>()
    val estimateInBase = MutableLiveData<String>()
    val rateToBaseCurrency = MutableLiveData<Float>()
    val ratesList =
        MutableLiveData<MutableMap<String, io.sevenb.terminal.data.model.broker_api.rates.MarketRate?>>()

    var firstMarketBoolean = true
    var isDirectOrder = false

    var firstMarket: Market? = null

    private var filterMap = mutableMapOf<BinanceFilter, Filters>()

    init {
        withdrawalList()

        subscribeUi()
    }

    fun marketRate(from: String) {
        viewModelScope.launch {
            val marketRate = estimatedDailyLimit.marketApi.marketRate(
                from, getBaseCurrencyTicker()
            )
            when (marketRate) {
                is Result.Success -> {
                    rate = marketRate.data.rate
                }
                is Result.Error -> {
                    when {
                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                        else -> customErrorMessage.value = marketRate.message
                    }
                }
            }
        }
    }

    private fun showNoInternetConnection() {
        val currentTime = System.currentTimeMillis()
        if (currentTime - showTimeError >= 1500) {
            customErrorMessage.value = ConnectionStateMonitor.noInternetError
            showTimeError = currentTime
        }
    }

    private fun withdrawalList() {
        viewModelScope.launch {
            accountData.totalBalanceFlow.collect {
                val balance = it.availableBalance.toFloatOrNull()
                if (balance != null && balance == 0.0f) {
                    zeroBalance.value = true
                } else {
                    zeroBalance.value = false
                    when (val withdrawalListResult = accountData.mapWithdrawalList(true)) {
                        is Result.Success -> {
                            val listToWithdraw = filterWithdrawItems(withdrawalListResult.data)
                            listOfWithdrawal.value = listToWithdraw
                            requestRates(listToWithdraw)
                        }

                        is Result.Error -> {
                            when {
                                !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                                else -> customErrorMessage.value =
                                    withdrawalListResult.exception.message
                            }
                        }
                    }
                }
            }
        }
    }

    private suspend fun requestRates(listOfAccountCoins: List<WithdrawItem>) {
        val baseTicker = getBaseCurrencyTicker()
        val requestParams = mutableListOf<MarketRateParams>()

        listOfAccountCoins.forEach {
            requestParams.add(MarketRateParams(it.ticker, baseTicker))
        }

        when (val priceResult = accountData.getPrices(requestParams)) {
            is Result.Success -> {
                val mapOfRates =
                    mutableMapOf<String, io.sevenb.terminal.data.model.broker_api.rates.MarketRate?>()
                priceResult.data.forEachIndexed { index, marketRate ->
                    mapOfRates[listOfAccountCoins[index].ticker] = marketRate
                }
                ratesList.value = mapOfRates
            }
            is Result.Error -> {
                ratesList.value = mutableMapOf()
            }
        }
    }

    private fun filterWithdrawItems(list: List<WithdrawItem>): List<WithdrawItem> {
        val filteredBnbBscList = sortCurrenciesHandler.filterBnbBscNetwork(list)

        return sortCurrenciesHandler.makeSingleBTCTicker(filteredBnbBscList)
    }

    fun withdrawalRequest(
        withdrawItem: WithdrawItem,
        amount: String,
        address: String,
        extraId: String
    ) {
        withdrawalCreated.value = false
        val amountFloat = amount.toFloatOrNull()
        if (amountFloat == null || amountFloat < 0.0f) {
            snackbarMessage.value = ErrorMessageType.EMPTY_FIELDS
            return
        }

        if (address.trim().isEmpty()) {
            snackbarMessage.value = ErrorMessageType.EMPTY_FIELDS
            return
        }

        val withdrawalRequest = WithdrawalRequest(
            ticker = withdrawItem.ticker,
            network = withdrawItem.network,
            address = address,
            extraId = extraId,
            amount = amountFloat
        )

        makeWithdrawalRequest(withdrawalRequest)
    }

    fun makeEstimate(from: String, to: String = "USDT") {
        viewModelScope.launch {
            if (from == "USDT") {
                usdtWithdraw.value = true
                return@launch
            } else usdtWithdraw.value = false

            val actualTo = if (to == "USD") AccountData.USDT else to

            if (market.value == null) {
                findMarketWithStepSize(from, actualTo)
            }

            val rateResult = if (isDirectOrder) {
                val listOfParams = listOf(MarketRateParams(from, actualTo))
                accountData.getPrices(listOfParams)
            } else {
                val listOfParams = listOf(MarketRateParams(actualTo, from))
                accountData.getPrices(listOfParams)
            }

            when (rateResult) {
                is Result.Success -> {
                    val marketRateResult = rateResult.data
                    if (marketRateResult[0]?.rate != null) {
                        rate = if (!isDirectOrder) {
                            1 / marketRateResult[0]?.rate!!
                        } else
                            marketRateResult[0]?.rate!!

                        rate?.let {
                            rateToBaseCurrency.value = it
                            estimateInBase.value =
                                OrderViewModel.roundDouble(it.toDouble(), 2, true)
                        }

                    }
                }
                is Result.Error -> {
                    when {
                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                        else -> customErrorMessage.value = rateResult.exception.message
                    }
                }
            }
        }
    }

    fun findMarket(from: String, to: String = "USDT") {
        viewModelScope.launch {
            if (from != "USDT") findMarketWithStepSize(from, to)
        }
    }

    private suspend fun findMarketWithStepSize(from: String, to: String) {
        return withContext(Dispatchers.IO) {
            when (val marketsResult = marketsData.cacheFirstMarkets()) {
                is Result.Success -> {
                    val markets = marketsResult.data

                    val isOrderSide: Boolean

                    var symbolMarket = markets.firstOrNull {
                        (it.baseAsset == from && it.quoteAsset == to)
                    }

                    if (symbolMarket == null) {
                        symbolMarket = markets.firstOrNull {
                            it.baseAsset == to && it.quoteAsset == from
                        }
                        isOrderSide = false
                        initFirstMarket(symbolMarket, isOrderSide)
                        getFilters("$to$from")
                    } else {
                        isOrderSide = true
                        initFirstMarket(symbolMarket, isOrderSide)
                        getFilters("$from$to")
                    }

                    withContext(Dispatchers.Main) {
                        market.value = Pair(symbolMarket, isOrderSide)
                    }
                }
                is Result.Error -> {
                    when {
                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                        else -> customErrorMessage.postValue(marketsResult.exception.message)
                    }
                }
            }
        }
    }

    private fun initFirstMarket(market: Market?, isDirectOrder: Boolean) {
        if (firstMarketBoolean) {
            firstMarket = market
            this.isDirectOrder = isDirectOrder
        }

        firstMarketBoolean = false
    }

    fun makeWithdrawalRequest(withdrawalRequest: WithdrawalRequest? = tempWithdrawalRequest) {
        if (withdrawalRequest == null) return

        viewModelScope.launch {
            when (val requestWithdrawalResult = requestWithdrawal.execute(withdrawalRequest)) {
                is Result.Success -> {
                    withdrawal = requestWithdrawalResult.data
                    withdrawalCreated.value = true
                    try {
                        startResendTimer.execute(this)
                        showResendButton.value = false
                    } catch (e: Exception) {

                    }
                }
                is Result.Error -> {
                    when {
                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                        requestWithdrawalResult.message.contains("withdrawal_limit_exceeded") -> {
                            snackbarMessage.value = ErrorMessageType.WITHDRAWAL_DAILY_LIMIT
                        }
                        else -> {
                            val errorMessage = requestWithdrawalResult.message
                            customErrorMessage.value =
                                if (errorMessage.contains("Withdrawal amount is less then minimal")) {
                                    "Withdrawal amount is less than minimal"
                                } else errorMessage
                        }
                    }
                }
            }
        }
    }

    private fun subscribeUi() {
        viewModelScope.launch {
            startResendTimer.timerValueFlow.collect {
                timerValue.value = DateTimeUtil.formatSeconds(it.toLong())
                if (it == 0) showResendButton.value = true
            }
        }
    }

    fun withdrawalConfirm(code: String) {
        viewModelScope.launch {
            val confirmWithdrawalParams = ConfirmWithdrawalParams(withdrawal.withdrawalId, code)
            when (val withdrawalConfirmResult =
                withdrawalConfirm.execute(confirmWithdrawalParams)) {
                is Result.Success -> getTransactionExplorer(withdrawal)
                is Result.Error -> {
                    when {
                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                        else -> customErrorMessage.value =
                            withdrawalConfirmResult.exception.message
                    }
                }
            }
        }
    }

    fun requestDailyLimitEstimated(ticker: String) {
        viewModelScope.launch {
            when (val dailyLimitResult = estimatedDailyLimit.execute(ticker)) {
                is Result.Success -> {
                    dailyLimit.value = dailyLimitResult.data
                }
                is Result.Error -> {
                    when (val limit = estimatedDailyLimit.execute(AccountData.BTC)) {
                        is Result.Success -> dailyLimit.value = limit.data
                        is Result.Error -> dailyLimit.value = ""
                    }
                }
            }
        }
    }

    private suspend fun isAccountVerified(): Boolean {
        return when (val accountStatusResult = verificationData.accountStatus()) {
            is Result.Success -> {
                val listOfAccountStatuses = accountStatusResult.data
                listOfAccountStatuses.status == ACCOUNT_STATUS_VERIFIED
            }
            is Result.Error -> false
        }
    }

    private suspend fun withdrawalHash(id: String) = requestWithdrawalHash.execute(id)

    private fun getTransactionExplorer(historyItem: WithdrawalHistoryItem) {
        viewModelScope.launch {
            when (val response =
                transactionExplorerMaskRepository.getTransactionMask(historyItem.ticker.toLowerCase())) {
                is Result.Success -> {
                    withdrawal = historyItem
//                    withdrawalCreated.value = true
                    withdrawalResult.value =
                        Pair(response.data.transactionExplorerMask, withdrawal)
                }
                is Result.Error -> {
                    when {
                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                        else -> customErrorMessage.value = response.exception.message
                    }
                }
            }
        }
    }

    fun copyToClipboard(text: String) {
        viewModelScope.launch {
            clipboardCopy.copyToClipboard(text)
        }
    }

    private suspend fun getFilters(symbol: String) {
        when (val filtersResult = marketsData.getFilters(symbol)) {
            is Result.Success -> {
                println()

                filtersResult.data.filters?.forEach {
                    it.filterType?.let { filter -> filterMap[filter] = it }
                }

                val step = getPrecision()

                step?.let {
                    withContext(Dispatchers.Main) { marketDataIsReady.value = it }
                }
            }
            is Result.Error -> {
                filterMap = mutableMapOf()
                withContext(Dispatchers.Main) {
                    customErrorMessage.value = filtersResult.message
                }
            }
        }
    }

    private fun getPrecision(): Int? {
        return filterMap[BinanceFilter.LOT_SIZE]?.let {
            val stringTickFirst = it.stepSize?.split(".")?.get(0)
            val stringTickSecond = it.stepSize?.split(".")?.get(1)
            var precision: Int?
            if (stringTickFirst != "1") {
                precision = 0
                stringTickSecond?.let { str ->
                    loop@ for (char in str) {
                        precision = precision?.plus(1)
                        if (char.toInt() == 49) break@loop
                    }
                }
            } else precision = 0
            return if (precision == -1) null else precision
        }
    }
}