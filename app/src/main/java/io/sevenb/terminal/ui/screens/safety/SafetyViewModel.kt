package io.sevenb.terminal.ui.screens.safety

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.enums.auth.NewAuthStates
import io.sevenb.terminal.data.model.enums.SafetyStates
import io.sevenb.terminal.data.repository.auth.UserAuthRepository
import io.sevenb.terminal.domain.usecase.auth.LocalUserDataStoring
import io.sevenb.terminal.domain.usecase.auth.LogOutUser
import io.sevenb.terminal.domain.usecase.auth.RestoreRefreshToken
import kotlinx.coroutines.launch
import javax.inject.Inject

class SafetyViewModel @Inject constructor(
    private val restoreRefreshToken: RestoreRefreshToken,
    private val localUserDataStoring: LocalUserDataStoring,
    private val logOutUser: LogOutUser
) : ViewModel() {

    val authMethod = MutableLiveData<SafetyStates>()
    val authConfirmed = MutableLiveData<Boolean>()
    val processNavigation = MutableLiveData<NewAuthStates>()
    val showLoader = MutableLiveData<String>()
    val showPopupMenu = MutableLiveData<Pair<Boolean, String>>()
    val logOut = MutableLiveData<Boolean>()

    var availableVariant = SafetyStates.NOT_LOCKED

    var fromLocked: Boolean? = null

    fun setNewAuthMethod(state: SafetyStates) {
        availableVariant = state
        authMethod.value = state
    }

    fun restoreRefreshToken() {
        viewModelScope.launch {
            when (val restoreRefreshTokenResult = restoreRefreshToken.execute()) {
                is Result.Success -> {
                    UserAuthRepository.accessToken = restoreRefreshTokenResult.data
                    localUserDataStoring.restoreLocalAccountData()
                    authConfirmed.value = true
                }
                is Result.Error -> {
                    authConfirmed.value = false
                }
            }
        }
    }

    fun navigateTo(state: NewAuthStates) {
        if (state != NewAuthStates.LOGIN) {
            processNavigation.value = state
        } else {
            logOut.value = true
            viewModelScope.launch {
                val logOutUserResult = logOutUser.execute()

                if (logOutUserResult is Result.Success) {

                }
                processNavigation.value = state
            }
        }
    }

    fun logOut() {
        logOut.value = true
        viewModelScope.launch {
            logOutUser.execute()
        }
    }
}