package io.sevenb.terminal.ui.view

import android.content.Context
import android.util.AttributeSet
import com.google.android.material.switchmaterial.SwitchMaterial

class CustomSwitch @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : SwitchMaterial(context, attrs, defStyleAttr) {

    override fun performClick(): Boolean {
        return false
    }
}