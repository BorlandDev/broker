package io.sevenb.terminal.ui.screens.safety.tfa

import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.fragment.findNavController
import io.sevenb.terminal.R
import io.sevenb.terminal.data.model.enum_model.ErrorMessageType
import io.sevenb.terminal.data.model.enums.auth.TfaState
import io.sevenb.terminal.domain.usecase.new_auth.HandleFieldFocus
import io.sevenb.terminal.exceptions.NewAuthException
import io.sevenb.terminal.ui.base.BaseLifecycleObserver
import io.sevenb.terminal.ui.extension.getString
import io.sevenb.terminal.ui.extension.onActionDoneListener
import io.sevenb.terminal.ui.extension.tryText
import io.sevenb.terminal.ui.extension.visible
import io.sevenb.terminal.ui.screens.auth.new_auth.NewAuthSharedViewModel
import io.sevenb.terminal.ui.screens.settings.SharedSettingToActivityViewModel
import io.sevenb.terminal.utils.VibrationUtils
import kotlinx.android.synthetic.main.fragment_tfa_done.view.*
import kotlinx.android.synthetic.main.fragment_tfa_information.view.*
import kotlinx.android.synthetic.main.fragment_tfa_secret.view.*
import kotlinx.android.synthetic.main.fragment_tfa_verification_code.view.*

class TfaFlow : BaseLifecycleObserver() {

    private var tfaViewModel: TfaViewModel? = null
    private var newAuthSharedViewModel: NewAuthSharedViewModel? = null
    private var sharedSettingsToActivityViewModel: SharedSettingToActivityViewModel? = null

    private lateinit var handleFieldFocus: HandleFieldFocus

    private lateinit var view: View
    private lateinit var lifecycleOwner: LifecycleOwner
    override lateinit var fragment: Fragment

    private var fromSettings = false

    fun initLifecycleOwner(
        lifecycleOwner: LifecycleOwner,
        fragment: Fragment,
    ) {
        this.lifecycleOwner = lifecycleOwner
        this.fragment = fragment
    }

    fun initViews(
        view: View,
        tfaViewModel: TfaViewModel,
        newAuthSharedViewModel: NewAuthSharedViewModel?,
        sharedSettingsToActivityViewModel: SharedSettingToActivityViewModel?,
        handleFieldFocus: HandleFieldFocus,
        state: TfaState?,
        fromSettings: Boolean = false
    ) {
        this.view = view
        this.tfaViewModel = tfaViewModel
        this.newAuthSharedViewModel = newAuthSharedViewModel
        this.sharedSettingsToActivityViewModel = sharedSettingsToActivityViewModel

        this.handleFieldFocus = handleFieldFocus
        this.fromSettings = fromSettings

        if (!tfaViewModel.isSubscribed)
            subscribeUI()

        view.apply {
            if (state == TfaState.VERIFICATION_ONLY) {
                ivTfaVerificationBack.visibility = View.INVISIBLE
                tvTfaVerificationSkip.visibility = View.INVISIBLE
                tvTfaVerificationTitle.text = getString(R.string.title_tfa)
            } else {
                ivTfaVerificationBack.visibility = View.VISIBLE
                tvTfaVerificationSkip.visibility = View.VISIBLE
                tvTfaVerificationTitle.text = getString(R.string.title_tfa_enter_verification_code)
            }

            ivTfaInformationBack.visibility = if (fromSettings) View.VISIBLE else View.INVISIBLE
            tvTfaInformationSkip.visibility = if (fromSettings) View.INVISIBLE else View.VISIBLE
            tvTfaSecretSkip.text =
                getString(if (fromSettings) R.string.label_cancel else R.string.label_skip)
            tvTfaVerificationSkip.text =
                getString(if (fromSettings) R.string.label_cancel else R.string.label_skip)
        }

        this.tfaViewModel?.apply {
            state?.let { state ->
                setNewState(state)
            }
            loadEmail()
        }

        initFieldFocus()
    }

    private fun initFieldFocus() {
        handleFieldFocus.init(fragment.requireView())
    }

    private fun subscribeUI() {
        tfaViewModel?.apply {
            isSubscribed = true
            createPassCode.observe(lifecycleOwner, { processCreatePassCode(it) })
            newState.observe(lifecycleOwner, { processCurrentState(it) })
            errorString.observe(lifecycleOwner, { processErrorString(it) })
        }
    }

    private fun processCreatePassCode(pair: Pair<String, String>?) {
        pair?.let {
            newAuthSharedViewModel?.apply {
                requirePassCodeCreation(true, it.first, it.second)
            }
        }
    }

    private fun processCurrentState(tfaState: TfaState?) {
        tfaState?.let {
            tfaViewModel?.setGlobalStateToActivity?.value = it
//            newAuthSharedViewModel.setSubStateToActivity.value = it

//            sharedAuthViewModel.setGlobalStateToActivity.value = it

            when (it) {
                TfaState.INFORMATION -> processInformationState()
                TfaState.SECRET -> processSecretState()
                TfaState.VERIFICATION,
                TfaState.VERIFICATION_ONLY -> processVerificationState()
                TfaState.IS_ON -> processIsOnState()
            }
        }
    }

    private fun processInformationState() {
        view.apply {
            setInvisibleViews(
                clTfaSecret,
                clTfaVerification,
                clTfaDone
            )
            clTfaInformation.visible(true)

            ivTfaInformationBack.setOnClickListener {
                if (fromSettings) {
                    fragment.findNavController().popBackStack()
                }
            }
            btnTfaInformationContinue.setOnClickListener {
                btnTfaInformationContinue.isEnabled = false
                pbInformationContinue.visibility = View.VISIBLE
                tfaViewModel?.toggle()
            }
            tvTfaInformationSkip.setOnClickListener {
                tfaViewModel?.apply {
                    processCreatePassCode(Pair(email, password))
                }
            }
        }
    }

    private fun processSecretState() {
        view.apply {
            setInvisibleViews(
                clTfaInformation,
                clTfaVerification,
                clTfaDone
            )
            clTfaSecret.visible(true)
            btnTfaInformationContinue.isEnabled = true
            pbInformationContinue.visibility = View.GONE

            pbVerificationContinue.visibility = View.GONE
            btnTfaVerificationContinue.isEnabled = true
            etVerificationCode.isEnabled = true

            ivTfaSecretBack.setOnClickListener {
                tfaViewModel?.setNewState(TfaState.INFORMATION)
            }
            btnTfaSecretContinue.setOnClickListener {
                tfaViewModel?.setNewState(TfaState.VERIFICATION)
            }
            llSecret.setOnLongClickListener {
                tfaViewModel?.copyToClipboard()
                VibrationUtils.vibrate(fragment.requireContext())
                handleFieldFocus.showSnackBar("Secret key copied!")
                true
            }
            llSecret.setOnClickListener {
                tfaViewModel?.copyToClipboard()
                VibrationUtils.vibrate(fragment.requireContext())
                handleFieldFocus.showSnackBar("Secret key copied!")
            }
            tvTfaSecretSkip.setOnClickListener {
                tfaViewModel?.apply {
                    if (fromSettings) {
                        fragment.findNavController().popBackStack()
                    } else {
                        processCreatePassCode(Pair(email, password))
                    }
                }
            }

            tfaViewModel?.apply {
                tvSecret.text = secretKey
                ivTfaQr.setImageBitmap(getQrCodeBitmap())
            }
        }
    }

    private fun processVerificationState() {
        view.apply {
            setInvisibleViews(
                clTfaInformation,
                clTfaSecret,
                clTfaDone
            )
            clTfaVerification.visible(true)

            btnTfaVerificationContinue.setOnClickListener {
                verifyCode()
            }
            etVerificationCode.onActionDoneListener { verifyCode() }
            ivTfaVerificationBack.setOnClickListener {
                tfaViewModel?.setNewState(TfaState.SECRET)
            }
            tvTfaVerificationPaste.setOnClickListener {
                tfaViewModel?.apply {
                    etVerificationCode.tryText(textToPaste())
                }
            }
            tvTfaVerificationSkip.setOnClickListener {
                tfaViewModel?.apply {
                    if (fromSettings) {
                        fragment.findNavController().popBackStack()
                    } else {
                        processCreatePassCode(Pair(email, password))
                    }
                }
            }
            handleFieldFocus.focusField(etVerificationCode)
        }
    }

    private fun processIsOnState() {
        view.apply {
            setInvisibleViews(
                clTfaInformation,
                clTfaSecret,
                clTfaVerification
            )
            clTfaDone.visible(true)
            btnTfaDoneContinue.setOnClickListener {
                if (fromSettings) {
                    sharedSettingsToActivityViewModel?.let {
                        it.tfaWasChanged.value = true
                    }
                    fragment.findNavController().popBackStack()
                } else {
                    tfaViewModel?.apply {
                        processCreatePassCode(Pair(email, password))
                    }
                }
            }
        }
    }

    private fun processErrorString(message: String?) {
        view.apply {
            pbVerificationContinue.visibility = View.GONE
            pbInformationContinue.visibility = View.GONE
            btnTfaInformationContinue.isEnabled = true
            btnTfaVerificationContinue.isEnabled = true
            etVerificationCode.isEnabled = true
        }
        message?.let {
            handleFieldFocus.showSnackBar(message)
        }
    }

    private fun verifyCode() {
        tfaViewModel?.apply {
            val code = view.etVerificationCode.getString()
            val messageType = validateCode(code)
            if (messageType == ErrorMessageType.CHECK_PASSED) {
                view.apply {
                    pbVerificationContinue.visibility = View.VISIBLE
                    btnTfaVerificationContinue.isEnabled = false
                    etVerificationCode.isEnabled = false
                }
                verify(code)
            } else {
                processErrorString(NewAuthException(messageType).message)
            }
        }
    }

    fun setNullsToLiveData() {
        tfaViewModel?.apply {
            newState.value = null
            showProgress.value = null
            errorString.value = null
            focusField.value = null
            showLoader.value = null
            createPassCode.value = null
        }
    }

}