package io.sevenb.terminal.ui.screens.auth.registration_state

import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import io.sevenb.terminal.R
import io.sevenb.terminal.data.model.auth.ContinuesRegistrationObj
import io.sevenb.terminal.data.model.auth.UserCaptcha
import io.sevenb.terminal.data.model.auth.UserData
import io.sevenb.terminal.data.model.enum_model.ErrorMessageType
import io.sevenb.terminal.data.model.enums.FocusableFields
import io.sevenb.terminal.data.model.enums.GeeTestResult
import io.sevenb.terminal.data.model.enums.auth.NewAuthStates
import io.sevenb.terminal.data.model.enums.auth.RegistrationStates
import io.sevenb.terminal.domain.usecase.new_auth.HandleFingerprint
import io.sevenb.terminal.ui.extension.*
import io.sevenb.terminal.ui.screens.auth.SharedAuthViewModel
import io.sevenb.terminal.ui.screens.auth.new_auth.NewAuthSharedViewModel
import io.sevenb.terminal.ui.screens.settings.SettingsFragment
import io.sevenb.terminal.utils.Logger
import kotlinx.android.synthetic.main.fragment_auth_registration.view.*
import kotlinx.android.synthetic.main.fragment_auth_verification_code.view.*
import kotlinx.android.synthetic.main.view_auth_password_hint.view.*

class RegistrationFlow : LifecycleObserver {

    private lateinit var lifecycleOwner: LifecycleOwner
    private lateinit var fragment: Fragment
    private lateinit var handleFingerprint: HandleFingerprint

    private lateinit var view: View
    private lateinit var containerSmsCode: View
    private lateinit var newAuthSharedViewModel: NewAuthSharedViewModel
    private lateinit var registrationViewModel: RegistrationViewModel
    private lateinit var authSharedViewModel: SharedAuthViewModel

    private var passVisibility = false
    private var isFirstFocus = true
    private var isSubscribed = false
    private var isRegistrationCodeEnabled = true;

    fun initLifecycleOwner(
        lifecycleOwner: LifecycleOwner,
        fragment: Fragment,
        handleFingerprint: HandleFingerprint
    ) {
        this.lifecycleOwner = lifecycleOwner
        this.fragment = fragment
        this.handleFingerprint = handleFingerprint
        handleFingerprint.init(fragment)
    }

    fun initViews(
        view: View,
        newAuthSharedViewModel: NewAuthSharedViewModel,
        registrationViewModel: RegistrationViewModel,
        containerSms: View,
        authSharedViewModel: SharedAuthViewModel,
        state: RegistrationStates?
    ) {
        this.view = view
        this.newAuthSharedViewModel = newAuthSharedViewModel
        this.registrationViewModel = registrationViewModel
        this.authSharedViewModel = authSharedViewModel
        containerSmsCode = containerSms
        passVisibility = false
        isFirstFocus = true

        registrationViewModel.apply {
            showResendButton.value = false
//            if (state != RegistrationStates.REGISTRATION_VERIFICATION_CODE)
//                processShowSmsCode(false)
            subscribeUI()
        }

        if (!isSubscribed)
            subscribeUI()

        view.apply {
            input_email_registration.doOnTextChanged { _, _, _, _ -> showEmailError(false) }
            input_password_registration.doOnTextChanged { _, _, _, _ -> showPasswordError(false) }

            button_create_account_registration.setOnClickListener { signUpUser() }
            input_password_registration.onActionDoneListener { signUpUser() }

            ll_action_have_account_email.setOnClickListener {
                processRegistrationState(NewAuthStates.LOGIN)
            }

            setPasswordVisibilityChanger(input_password_registration, img_pass_visibility)
            setPasswordChangeListener(input_password_registration)
            setPasswordHints(input_password_registration)
        }

        containerSmsCode.apply {
            input_verification_code.tryText("")
        }

        state?.let {
            if (it == RegistrationStates.REGISTRATION_DATA) {
                registrationViewModel.setNewState(NewAuthStates.REGISTRATION)
            }
            registrationViewModel.changeCurrentState(it)
        }
    }

    private fun processRegistrationState(registrationState: NewAuthStates?) {
        registrationState?.let { newAuthSharedViewModel.setNewState(it) }
    }

    private fun timerValue(value: String) {
        containerSmsCode.timer_resend_code.text = "0:%s".format(value)
    }

    private fun showResendButton(isShow: Boolean) {
        containerSmsCode.apply {
            setViewsVisibility(!isShow, ll_resend_timer)
            setViewsVisibility(isShow, ll_resend_code)
        }
    }

    private fun subscribeUI() {
        isSubscribed = true
        lifecycleOwner.let { owner ->
            newAuthSharedViewModel.apply {
                userCaptcha.observe(owner, { processUserCaptcha(it) })
                newState.observe(owner, { processNewState(it) })
                continueRegistration.observe(owner, { processContinueRegistration(it) })
                emailFromStorage.observe(owner, { processEmailFromStorage(it) })
                closeCaptcha.observe(owner, { println() })
            }

            registrationViewModel.apply {
                geeTestResult.observe(owner, { processGeeTestResult(it) })
                errorString.observe(owner, { processErrorString(it) })
                errorMessage.observe(owner, { processErrorMessage(it) })
                showResendButton.observe(owner, { showResendButton(it) })
                timerValue.observe(owner, { timerValue(it) })
//                showSmsCode.observe(owner, { processShowVerificationCode(it) })
                currentState.observe(owner, { processCurrentState(it) })
                allLocalAccountDataRemoved.observe(owner, { processLocalAccountDataRemoved() })
                popupLocalAccountExists.observe(owner, { popupRemoveLocalAccount() })
//                creatingBiometricCipher.observe(owner, { processFingerprintCreation(it) })
//                passCodeCreationRequired.observe(owner, { processPassCodeCreation(it) })
                tfaRequired.observe(owner, {
                    processRegistrationState(NewAuthStates.TFA_CREATE)
//                    processPassCodeCreation(it)
                })
                userExistsResult.observe(owner, { processUserExist(it) })
                showLoader.observe(owner, { processShowLoader(it) })
            }
        }

    }

    private fun processUserExist(value: Boolean?) {
        value?.let { if (it) newAuthSharedViewModel.setNewState(NewAuthStates.LOGIN) }
    }

    private fun processShowLoader(message: String?) {
        message?.let { newAuthSharedViewModel.showLoader.value = Pair(true, it) }
    }

    private fun processEmailFromStorage(email: String?) {
        email?.let { registrationViewModel.emailFromStorage = it }
    }

    private fun processLocalAccountDataRemoved() = signUpUser()

    private fun processCurrentState(state: RegistrationStates?) {
        state?.let {
            authSharedViewModel.setSubStateToActivity.value = it
            when (it) {
                RegistrationStates.REGISTRATION_DATA -> {
                    containerSmsCode.input_verification_code.tryText("")
                    processShowVerificationCode(false)
                    newAuthSharedViewModel.focusField(FocusableFields.REGISTRATION_EMAIL)
                }
                RegistrationStates.REGISTRATION_VERIFICATION_CODE -> {
                    processShowVerificationCode(true)
                    newAuthSharedViewModel.focusField(FocusableFields.REGISTRATION_VERIFICATION_CODE)
                    newAuthSharedViewModel.userCaptcha.value = null

                    containerSmsCode.apply {
                        button_verify_code.setOnClickListener {
                            if (isRegistrationCodeEnabled) {
                                isRegistrationCodeEnabled = false
                                sendConfirmationCode()
                            }
                        }
                        input_verification_code.onActionDoneListener {
                            if (isRegistrationCodeEnabled) {
                                isRegistrationCodeEnabled = false
                                sendConfirmationCode()
                            }
                        }
                        action_change_email.setOnClickListener {
                            registrationViewModel.apply {
                                setNewState(NewAuthStates.REGISTRATION)
                                changeCurrentState(RegistrationStates.REGISTRATION_DATA)
                            }
                        }
                        ll_resend_code.setOnClickListener { registrationViewModel.resendRegistrationCode() }

                        email_code_sent_to.text = registrationViewModel.userData.email
                        sms_code_back_button.visible(true)
                        sms_code_back_button.setOnClickListener {
                            registrationViewModel.apply {
                                setNewState(NewAuthStates.REGISTRATION)
                                changeCurrentState(RegistrationStates.REGISTRATION_DATA)
                            }
                        }
                    }
                }
            }
        }
    }

    private fun processShowVerificationCode(value: Boolean?) {
        value?.let { newAuthSharedViewModel.showVerificationCode(it) }
    }

    private fun processContinueRegistration(obj: ContinuesRegistrationObj?) {
        obj?.let {
            registrationViewModel.apply {
                userData.email = obj.email
                userData.password = obj.password
                newAuthSharedViewModel.apply {
                    showProgress(false)
                    setNewState(NewAuthStates.CONTINUES_REGISTRATION)
                }
                setNewRegistrationToken(obj.token)
                requestOrResendRegistrationCode()
            }
        }
    }

    private fun processNewState(state: NewAuthStates?) {
        if (state == NewAuthStates.CONTINUES_REGISTRATION || state == NewAuthStates.REGISTRATION) {
            registrationViewModel.localAuthState = state

            if (state == NewAuthStates.CONTINUES_REGISTRATION) {
                registrationViewModel.changeCurrentState(RegistrationStates.REGISTRATION_VERIFICATION_CODE)
            } else if (state == NewAuthStates.REGISTRATION) {
                registrationViewModel.changeCurrentState(RegistrationStates.REGISTRATION_DATA)
            }
        }
        if (state == NewAuthStates.DONE_AUTH) {
            isSubscribed = false
        }
    }

    private fun processErrorMessage(errorMessageType: ErrorMessageType?) {
        errorMessageType?.let { newAuthSharedViewModel.showErrorMessage(it) }
    }

    private fun processErrorString(message: String?) {
        isRegistrationCodeEnabled = true
        message?.let { newAuthSharedViewModel.showError(it) }
    }

    private fun processGeeTestResult(geeTestResult: GeeTestResult?) {
        geeTestResult?.let {
            newAuthSharedViewModel.apply {
                if (geeTestResult != GeeTestResult.SUCCESS) {
                    showProgress(false)
                }
                setGeeTestResult(it)
                registrationViewModel.userData = userData
            }
        }
    }

    private fun processUserCaptcha(userCaptcha: UserCaptcha?) {
        userCaptcha?.let {
            registrationViewModel.apply {
//                if (newAuthSharedViewModel.stateWithoutSMS == NewAuthStates.REGISTRATION ||
//                    newAuthSharedViewModel.stateWithoutSMS == NewAuthStates.CONTINUES_REGISTRATION
//                ) {
                if (newAuthSharedViewModel.state == NewAuthStates.REGISTRATION ||
                    newAuthSharedViewModel.state == NewAuthStates.CONTINUES_REGISTRATION
                ) {
                    userData = newAuthSharedViewModel.userData
                    requestOrResendRegistrationCode(it)
                }
            }
        }
    }

    private fun showPasswordError(value: Boolean) {
        view.container_password.apply {
            if (value) setErrorBackground()
            else setDefaultBackground()
        }
    }

    private fun showEmailError(value: Boolean) {
        view.input_email_registration.apply {
            if (value) setErrorBackground()
            else setDefaultBackground()
        }
    }

    private fun signUpUser() {
        val email = view.input_email_registration.getString()
        val password = view.input_password_registration.getString()

        if (!registrationViewModel.isItAnotherAccount(email)) {
            newAuthSharedViewModel.showProgress(true)

            containerSmsCode.email_code_sent_to.text = email
            val messageType = registrationViewModel.validateFields(email, password)

            if (messageType == ErrorMessageType.CHECK_PASSED) {
                Logger.init(email)
                Logger.sendLog("RegistrationFlow", "signUpUser")
                newAuthSharedViewModel.startCaptchaFlow(UserData(email, password))
            } else {
                processRegistrationError(messageType)
                processErrorMessage(messageType)
            }
        }
    }

    private fun processRegistrationError(messageType: ErrorMessageType) {
        newAuthSharedViewModel.showProgress(false)
        when (messageType) {
            ErrorMessageType.EMPTY_FIELDS -> {
                showEmailError(true)
                showPasswordError(true)
            }
            ErrorMessageType.EMPTY_EMAIL, ErrorMessageType.EMAIL_INCORRECT ->
                showEmailError(true)
            ErrorMessageType.EMPTY_PASSWORD, ErrorMessageType.PASSWORD_INCORRECT ->
                showPasswordError(true)
            else -> {
            }
        }
    }

    private fun changeEyeImage(imageView: ImageView) {
        imageView.newBackground(if (passVisibility) R.drawable.ic_password else R.drawable.ic_password_hidden)
    }

    private fun setImage(imageView: ImageView, value: Boolean) =
        if (value) setEnteredImage(imageView) else setNotEnteredImage(imageView)

    private fun setEnteredImage(imageView: ImageView) =
        imageView.newBackground(R.drawable.ic_entered)

    private fun setNotEnteredImage(imageView: ImageView) =
        imageView.newBackground(R.drawable.ic_not_entered)

    private fun setPasswordHints(view: EditText) {
        view.doOnTextChanged { text, _, _, _ ->
            text?.let {
                val response = registrationViewModel.checkPassword(it.toString())

                this.view.apply {
                    setImage(img_pass_length, response.lengthPasses)
                    setImage(img_numbers_contain, response.numbersPasses)
                    setImage(img_lowercase, response.lowercasePasses)
                    setImage(img_uppercase, response.uppercasePasses)
                }
            }
        }
    }

    private fun setPasswordChangeListener(view: EditText) {
        view.onFocusChangeListener =
            View.OnFocusChangeListener { _, hasFocus ->
                if (isFirstFocus) {
                    isFirstFocus = false
                    if (hasFocus) {
                        this.view.apply {
                            setNotEnteredImage(img_pass_length)
                            setNotEnteredImage(img_numbers_contain)
                            setNotEnteredImage(img_uppercase)
                            setNotEnteredImage(img_lowercase)
                        }
                    }
                }
            }
    }

    private fun sendConfirmationCode() {
        registrationViewModel.confirmCodeRegistration(containerSmsCode.input_verification_code.getString())
    }

    private fun setPasswordVisibilityChanger(view: EditText, image: ImageView) {
        image.setOnClickListener {
            passVisibility = !passVisibility
            view.transformationMethod =
                if (passVisibility) HideReturnsTransformationMethod.getInstance() else
                    PasswordTransformationMethod.getInstance()
            view.setSelection(view.text.length)
            changeEyeImage(image)
        }
    }

    private fun popupRemoveLocalAccount() {
        MaterialAlertDialogBuilder(fragment.requireContext()).apply {
            setMessage(fragment.getString(R.string.message_clear_local_account_data))
            setNegativeButton(android.R.string.cancel) { dialog, _ ->
                dialog.dismiss()
            }
            setPositiveButton(android.R.string.ok) { dialog, _ ->
                registrationViewModel.clearAllLocalAccountData(
                    fragment.requireContext().cacheDir.path +
                            SettingsFragment.AVATAR_FILE_PATH
                )
                dialog.dismiss()
            }
        }.create().show()
    }
}