package io.sevenb.terminal.ui.screens.deposit

import android.Manifest
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.view.View
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.snackbar.Snackbar
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import io.sevenb.terminal.R
import io.sevenb.terminal.data.model.broker_api.account.DepositAddress
import io.sevenb.terminal.data.model.trading.TradeChartRange
import io.sevenb.terminal.domain.usecase.charts.AccountData
import io.sevenb.terminal.ui.base.BaseFragment
import io.sevenb.terminal.ui.extension.viewModelProvider
import io.sevenb.terminal.ui.screens.select_currency.BaseSelectItem
import io.sevenb.terminal.ui.screens.select_currency.DepositCurrencyItem
import io.sevenb.terminal.ui.screens.trading.SharedViewModel
import io.sevenb.terminal.ui.screens.trading.TradingWalletItem
import kotlinx.android.synthetic.main.fragment_fiat.*
import java.io.File
import java.io.File.separator
import java.io.FileOutputStream
import java.io.IOException


class FiatFragment : BaseFragment() {

    private lateinit var depositViewModel: DepositViewModel
    private lateinit var sharedViewModel: SharedViewModel

    private var localDepositList: MutableList<DepositCurrencyItem> = mutableListOf()
    private var localFiatList: MutableList<DepositCurrencyItem> = mutableListOf()

    override fun layoutId() = R.layout.fragment_fiat
    override fun onSearchTapListener() {
    }

//    override fun containerId() = R.id.container_deposit_sized

    public var currentNetwork: String? = null

    companion object{
        fun newInstance() = FiatFragment()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        depositViewModel = viewModelProvider(viewModelFactory)
        sharedViewModel = activity?.run {
            viewModelProvider(viewModelFactory)
        } ?: throw Exception("Invalid Activity")
        initView()
        subscribeUi()
    }

    private fun initView() {
        cl_deposit_ticker_fiat1.setOnClickListener {
            sharedViewModel.selectCurrencyDepositList.value = localDepositList.toList()
            currencySelection()
        }
        cl_deposit_ticker_fiat2.setOnClickListener {
            sharedViewModel.selectCurrencyFiatList.value = localFiatList
            currencySelectionFiat()
        }
        btnCopy_fiat.setOnClickListener { copyAddress() }
        deposit_address_fiat.setOnClickListener { copyAddress() }

        btnCopyExtra_fiat.setOnClickListener { copyExtra() }
        deposit_extra_address_using_fiat.setOnClickListener { copyExtra() }


        btnShare_fiat.setOnClickListener {
            val sendIntent: Intent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(
                    Intent.EXTRA_TEXT,
                    "${tv_deposit_ticker_fiat1.text} deposit address: ${deposit_address_fiat.text} ${if (currentNetwork != null) "in $currentNetwork network" else ""}"
                )
                type = "text/plain"
            }

            val shareIntent = Intent.createChooser(sendIntent, null)
            startActivity(shareIntent)
        }

        btnShareExtra_fiat.setOnClickListener {
            val sendIntent: Intent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(
                    Intent.EXTRA_TEXT,
                    "${tv_deposit_ticker_fiat1.text} deposit extra Id: ${deposit_extra_address_fiat.text}"
                )
                type = "text/plain"
            }

            val shareIntent = Intent.createChooser(sendIntent, null)
            startActivity(shareIntent)
        }

        container_deposit_main.setTransitionListener(object :
            MotionLayout.TransitionListener {
            override fun onTransitionStarted(p0: MotionLayout?, p1: Int, p2: Int) {}

            override fun onTransitionChange(p0: MotionLayout?, p1: Int, p2: Int, p3: Float) {}

            override fun onTransitionCompleted(p0: MotionLayout?, p1: Int) {
                if (p0?.endState == p0?.currentState) {
                    swipeToBottom()
                    (requireParentFragment() as DepositViewPagerFragment).openWebView(
                        "https://changenow.io/exchange?from=${
                            tv_deposit_ticker_fiat2.text.toString().toLowerCase()
                        }&to=${
                            if(currentNetwork != "USDT ETH"){if(tv_deposit_ticker_fiat1.text.toString() == "USDT") "usdterc20" else tv_deposit_ticker_fiat1.text.toString().toLowerCase()}else "USDTERC".toLowerCase() + "100"
                        }&recipientAddress=${deposit_address_fiat.text}&fiatMode=true&amount=100${if(deposit_extra_address_using_fiat.isVisible)"&recipientExtraId=${deposit_extra_address_fiat.text}" else ""}&link_id=e2881e6d60156a"
                    )
                }
            }

            override fun onTransitionTrigger(p0: MotionLayout?, p1: Int, p2: Boolean, p3: Float) {}
        })
    }

    fun swipeToBottom(){
        container_deposit_main.transitionToStart()
    }

    private fun screenShot(view: View): Bitmap? {
        val bitmap = Bitmap.createBitmap(
            view.width,
            view.height, Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        view.draw(canvas)
        return bitmap
    }


    private fun saveImageToGallery(context: Context, bmp: Bitmap): Boolean {
        val storePath =
            Environment.getExternalStorageDirectory().absolutePath + separator + "dearxy"
        val appDir = File(storePath)
        if (!appDir.exists()) {
            appDir.mkdir()
        }
        val fileName = System.currentTimeMillis().toString() + ".jpg"
        val file = File(appDir, fileName)
        try {
            val fos = FileOutputStream(file)
            val isSuccess = bmp.compress(Bitmap.CompressFormat.JPEG, 60, fos)
            fos.flush()
            fos.close()

            val uri = Uri.fromFile(file)
            context.sendBroadcast(Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri))
            return isSuccess
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return false
    }

    private fun subscribeUi() {
        depositViewModel.listDepositCoins.observe(viewLifecycleOwner, { depositList(it) })
        depositViewModel.addressDeposit.observe(viewLifecycleOwner, { setupDepositAddress(it) })
        depositViewModel.snackbarMessage.observe(viewLifecycleOwner, { showSnackbar(it) })

        sharedViewModel.selectedCurrencyDeposit.observe(viewLifecycleOwner, {
            if (!localDepositList.isNullOrEmpty())
                tickerSelection(it)
        })
        sharedViewModel.selectedCurrencyFiat.observe(viewLifecycleOwner, {
            if (!localFiatList.isNullOrEmpty())
                tickerSelectionFiat(it)
        })
    }

    private fun currencySelection() {
        if (findNavController().currentDestination?.id != R.id.depositViewPager) return

        val direction =
            DepositViewPagerFragmentDirections.actionDepositViewPagerToSelectCurrencyDepositFragment()
        findNavController().navigate(direction)
    }

    private fun currencySelectionFiat() {
        if (findNavController().currentDestination?.id != R.id.depositViewPager) return

        val direction =
            DepositViewPagerFragmentDirections.actionDepositViewPagerToSelectCurrencyFiatFragment()
        findNavController().navigate(direction)
    }

    private fun setupDepositAddress(address: DepositAddress) {
        val content = SpannableString(address.address)
        content.setSpan(UnderlineSpan(), 0, content.length, 0)
        deposit_address_fiat.setText(content)
        address.extraId.let {
            if (it.isNotEmpty()) {
                val content = SpannableString(it)
                content.setSpan(UnderlineSpan(), 0, content.length, 0)
                deposit_extra_address_fiat.text = content
                deposit_extra_address_using_fiat.visibility = View.VISIBLE
                deposit_extra_address_fiat.text = content
            } else {
                deposit_extra_address_using_fiat.visibility = View.GONE
                deposit_extra_address_fiat.text = ""
            }
        }
    }

    private fun copyAddress() {
        depositViewModel.copyToClipboard(deposit_address_fiat.text.toString())
        showSnackbar(getString(R.string.message_address_copied))
    }

    private fun copyExtra() {
        depositViewModel.copyToClipboard(deposit_extra_address_fiat.text.toString())
        showSnackbar(getString(R.string.message_extra_copied))
    }

    private fun depositList(list: List<DepositCurrencyItem>) {

        list.map {
            when(it.ticker){
                "BTC" -> localDepositList.add(it)
                "ETH" -> if(it.name != "Bitcoin Cash" && it.name != "ARBITRUM" && it.name != "BNB" && it.name != "Polkadot" && it.name != "USD Coin" && it.name != "TRON")localDepositList.add(it) else ""
                "USDT" -> if(it.network == "ETH" && it.name != "ETH") localDepositList.add(it) else ""
                "XRP" -> localDepositList.add(it)
                "LTC" -> localDepositList.add(it)
                "BCH" -> if(it.network != "ETH") localDepositList.add(it) else ""
                "ADA" -> localDepositList.add(it)
                "BNB" -> if(it.network != "ETH") localDepositList.add(it) else ""
                "DOT" -> if(it.network != "ETH") localDepositList.add(it) else ""
                "XVG" -> localDepositList.add(it)
                "LINK" -> localDepositList.add(it)
                "USDC" -> if(it.network != "ETH") localDepositList.add(it) else ""
                "XLM" -> localDepositList.add(it)
                "XMR" -> localDepositList.add(it)
                "XEM" -> localDepositList.add(it)
                "TRX" -> if(it.network != "ETH") localDepositList.add(it) else ""
                "Dash" -> localDepositList.add(it)
                "AAVE" -> localDepositList.add(it)
                "ZIL" -> localDepositList.add(it)
                "ZEC" -> localDepositList.add(it)
                "SHIB" -> localDepositList.add(it)
                "DOGE" -> localDepositList.add(it)
                "HBAR" -> localDepositList.add(it)
                "THETA" -> localDepositList.add(it)
                else -> ""
            }
        }
        localFiatList.add(DepositCurrencyItem(network = "Dollar", ticker = "USD", amount = "", change = 0.0f, cmcIconId = 11111, currentRange = TradeChartRange.DAY, estimated = "", isExpanded = false, name = "", price = 0.0f, updateExpandedStateChannel = null))
        localFiatList.add(DepositCurrencyItem(network = "Euro", ticker = "EUR", amount = "", change = 0.0f, cmcIconId = 11112, currentRange = TradeChartRange.DAY, estimated = "", isExpanded = false, name = "", price = 0.0f, updateExpandedStateChannel = null))
        localFiatList.add(DepositCurrencyItem(network = "British Pound Sterling", ticker = "GBP", amount = "", change = 0.0f, cmcIconId = 11113, currentRange = TradeChartRange.DAY, estimated = "", isExpanded = false, name = "", price = 0.0f, updateExpandedStateChannel = null))
        localFiatList.add(DepositCurrencyItem(network = "Australian Dollar", ticker = "AUD", amount = "", change = 0.0f, cmcIconId = 11114, currentRange = TradeChartRange.DAY, estimated = "", isExpanded = false, name = "", price = 0.0f, updateExpandedStateChannel = null))
        localFiatList.add(DepositCurrencyItem(network = "Canadian Dollar", ticker = "CAD", amount = "", change = 0.0f, cmcIconId = 11115, currentRange = TradeChartRange.DAY, estimated = "", isExpanded = false, name = "", price = 0.0f, updateExpandedStateChannel = null))
        localFiatList.add(DepositCurrencyItem(network = "Russian Ruble", ticker = "RUB", amount = "", change = 0.0f, cmcIconId = 11116, currentRange = TradeChartRange.DAY, estimated = "", isExpanded = false, name = "", price = 0.0f, updateExpandedStateChannel = null))
        localFiatList.add(DepositCurrencyItem(network = "Indian Rupees", ticker = "INR", amount = "", change = 0.0f, cmcIconId = 11117, currentRange = TradeChartRange.DAY, estimated = "", isExpanded = false, name = "", price = 0.0f, updateExpandedStateChannel = null))
        tickerSelection(localDepositList.firstOrNull() ?: return)
        tickerSelectionFiat(localFiatList.firstOrNull() ?: return)
    }

    private fun tickerSelection(baseSelectItem: BaseSelectItem?) {
        if (baseSelectItem !is DepositCurrencyItem) return
        sharedViewModel.selectedCurrencyDeposit.value = null

        loadTicker(baseSelectItem.cmcIconId)
        tv_deposit_ticker_fiat1.text = baseSelectItem.ticker

        resetAddressFields()
        depositViewModel.requestDepositAddress(baseSelectItem)
    }

    private fun tickerSelectionFiat(baseSelectItem: BaseSelectItem?) {
        if (baseSelectItem !is DepositCurrencyItem) return
        sharedViewModel.selectedCurrencyFiat.value = null

        currentNetwork = baseSelectItem.ticker
            "%s".format(baseSelectItem.name)
        loadTickerFiat(baseSelectItem.cmcIconId)
        tv_deposit_ticker_fiat2.text = baseSelectItem.ticker
        depositViewModel.requestDepositAddress(baseSelectItem)
    }

    private fun resetAddressFields() {
        deposit_address_fiat.text = ""
        deposit_extra_address_fiat.text = ""
    }

    private fun showSnackbar(message: String) {
        if (!message.contains(AccountData.ERROR_CODE_500))
            Snackbar.make(container_deposit_main.rootView, message, Snackbar.LENGTH_SHORT)
                .show()
    }

    private fun showSnackbardd(message: String) {
        val snak =
            Snackbar.make(container_deposit_main.rootView, message, Snackbar.LENGTH_LONG)
        snak.setAction("Yes") { requestReadPermissions() }
        snak.show()
    }

    private fun requestReadPermissions() {
        Dexter.withActivity(requireActivity())
            .withPermissions(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
                        screenShot(container_deposit_main)?.let { it1 ->
                            val result = saveImageToGallery(
                                requireContext(),
                                it1
                            )
                            showSnackbar(if (result) "Saved" else "Oops, something goes wrong. Try later")
                        }
                    }

                    if (report.isAnyPermissionPermanentlyDenied) {
                        showSnackbar("In order to save the image, give the application the required permission")
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest>,
                    token: PermissionToken
                ) {
                    token.continuePermissionRequest()
                }

            }).withErrorListener {
                showSnackbar("Oops, something goes wrong")
            }
            .onSameThread()
            .check()
    }

    override fun onPause() {
        enableAnimation(true)

        super.onPause()
    }

    private fun loadTicker(iconId: Int) {
        val requestOptions = RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)
        Glide.with(requireContext())
            .load(TradingWalletItem.CMC_ICON_URL.format(iconId))
            .apply(requestOptions)
            .into(iv_deposit_currency_fiat1)
    }

    private fun loadTickerFiat(iconId: Int) {
        val requestOptions = RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)
        val image = when(iconId){
            11111 -> R.drawable.ic_united_states_of_america_svgrepo_com
            11112 -> R.drawable.ic_european_union_svgrepo_com
            11113 -> R.drawable.ic_united_kingdom_svgrepo_com
            11114 -> R.drawable.ic_australia_svgrepo_com
            11115 -> R.drawable.ic_canada_svgrepo_com
            11116 -> R.drawable.ic_russia_svgrepo_com
            else -> R.drawable.ic_india_svgrepo_com
        }
        Glide.with(requireContext())
            .load(requireContext().getDrawable(image))
            .apply(requestOptions)
            .into(iv_deposit_currency_fiat2)
    }

    private fun enableAnimation(value: Boolean) {
        container_deposit_main.getTransition(R.id.deposit_transition).setEnable(value)
    }
}