package io.sevenb.terminal.ui.base

import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleObserver
import io.sevenb.terminal.ui.extension.setInvisibleViews
import io.sevenb.terminal.ui.extension.setVisibleViews

abstract class BaseLifecycleObserver : LifecycleObserver {

    abstract var fragment: Fragment

    fun getString(resId: Int) = fragment.getString(resId)

    fun setInvisibleViews(vararg views: View?) = fragment.setInvisibleViews(*views)

    fun setVisibleViews(vararg views: View?) = fragment.setVisibleViews(*views)
}