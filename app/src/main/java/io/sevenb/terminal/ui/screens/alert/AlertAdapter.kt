package io.sevenb.terminal.ui.screens.alert

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import io.sevenb.terminal.R
import io.sevenb.terminal.data.model.broker_api.PriceAlertGet
import io.sevenb.terminal.ui.screens.trading.TradingWalletItem
import kotlinx.android.synthetic.main.item_alert_list_main.view.*
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

class AlertAdapter (var list: MutableList<PriceAlertGet>,var tradingList: List<TradingWalletItem>, val callback: Callback)
    : RecyclerView.Adapter<AlertAdapter.AlertHolder>() {

    interface Callback {
        fun onItemClicked(orders: PriceAlertGet)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlertHolder {
        return AlertHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_alert_list_main, parent, false)
        )
    }

    fun setMovieListItems(movieList: MutableList<PriceAlertGet>, tradingList :List<TradingWalletItem>) {
        this.list = movieList
        this.tradingList = tradingList
        notifyDataSetChanged()
    }
    fun addMovieListItems(movieList: List<PriceAlertGet>) {
        this.list.addAll(movieList)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: AlertHolder, position: Int) {
        with(holder){
            deleteItem.setOnClickListener{
                callback.onItemClicked(list[position])
            }

            tvTicker.text = list[position].baseAsset
            for(item in tradingList){
                if(item.ticker == list[position].baseAsset){
                    tvName.text = item.name
                    Glide.with(itemView.context)
                        .load(TradingWalletItem.CMC_ICON_URL.format(item.cmcIconId))
                        .apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                        .into(imgIcon)
                }
            }


            if(list[position].side == "RISE"){
                if(list[position].goalType == "VALUE_REACH" || list[position].goalType == "VALUE_CHANGE"){
                    priceAlert.text = "Price above ${doubleToStringNoDecimal(list[position].goalValue)}"
                }else{
                    priceAlert.text = "Price change over ${list[position].goalChange}%"
                }
            }else{
                if(list[position].goalType == "VALUE_REACH" || list[position].goalType == "VALUE_CHANGE"){
                    priceAlert.text = "Price below ${doubleToStringNoDecimal(list[position].goalValue)}"
                }else{
                    priceAlert.text = "Price change under ${list[position].goalChange}%"
                }
            }
        }
    }

    fun doubleToStringNoDecimal(d: String): String? {
        val formatter: DecimalFormat = NumberFormat.getInstance(Locale.US) as DecimalFormat
        formatter.applyPattern("#,###.##")
        return formatter.format(d.toDouble())
    }

    class AlertHolder (view: View) : RecyclerView.ViewHolder(view) {
        val imgIcon = view.img_icon
        val tvTicker = view.tv_ticker
        val tvName = view.tv_name
        val priceAlert = view.priceAlert
        val deleteItem = view.delete_item
    }
}