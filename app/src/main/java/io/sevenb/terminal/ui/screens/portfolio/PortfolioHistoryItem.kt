package io.sevenb.terminal.ui.screens.portfolio

import android.annotation.SuppressLint
import android.os.Parcelable
import android.view.View
import android.widget.ImageView
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import io.sevenb.terminal.R
import io.sevenb.terminal.data.model.enum_model.HistoryType
import io.sevenb.terminal.device.utils.DateTimeUtil
import io.sevenb.terminal.device.utils.DateTimeUtil.parseToTimestamp
import io.sevenb.terminal.domain.usecase.charts.AccountData
import io.sevenb.terminal.ui.extension.newTextColor
import io.sevenb.terminal.ui.extension.visible
import io.sevenb.terminal.ui.screens.order.OrderViewModel.Companion.roundDouble
import io.sevenb.terminal.ui.screens.trading.TradingWalletItem
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.item_history.view.*
import java.math.BigDecimal

@Parcelize
class PortfolioHistoryItem(
    val id: String = "",
    val symbol: String = "",
    val amount: String = "",
    override val createdAt: String = "",
    val type: HistoryType,
    val status: String = "",
    var firstTicker: String? = "",
    var secondTicker: String? = "",
    private var firstTickerIconId: Int = 0,
    private var secondTickerIconId: Int = 0,
    var cmcIconId: Int = 0,
    var estimatedInBase: Float? = null,
    var address: String? = null,
    var commision: Double? = null,
    var originalAmount: Double? = null,
    var commisison: Double? = null,
    val cummulativeQuoteAmount: Double? = null
) : BaseHistoryItem, Parcelable {

    override fun bind(itemView: View, copyHash: (String) -> Unit, onClickHistory: (PortfolioHistoryItem) -> Unit) {
        when (type) {
            HistoryType.DEPOSIT -> depositItem(itemView, copyHash, onClickHistory)
            HistoryType.ORDERS -> orderItem(itemView, onClickHistory)
            HistoryType.WITHDRAW -> withdrawItem(itemView, copyHash, onClickHistory)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun depositItem(itemView: View, copyHash: (String) -> Unit, onClickHistory: (PortfolioHistoryItem) -> Unit) {
        itemView.iv_ticker.visible(true)
        itemView.cl_tickers.visibility = View.INVISIBLE

        loadImage(itemView, cmcIconId, itemView.iv_ticker)

        itemView.tv_symbol.text = symbol
        val amountString = "+ ${BigDecimal(amount)} $symbol"
        itemView.tv_ticker_pnl.text = amountString
        itemView.tv_ticker_pnl.newTextColor(R.color.colorGreen)

        itemView.setOnClickListener {
            onClickHistory.invoke(this)
        }

        if (createdAt.isNotEmpty()) {
            try {
                itemView.tv_transaction_date.text = "%s".format(formatDate())
            } catch (e: Exception) {

            }
        }

        val estimatedString =
            "${AccountData.getBaseCurrencyCharacter()}${
                estimatedInBase?.let { roundDouble(it.toDouble(), 2, true) }
            }"
        itemView.tv_estimated_money.text = "≈ $estimatedString"

        itemView.tv_status.text = status
//        itemView.setOnClickListener {
//            if (id.isNotEmpty())
//                copyHash(id)
//        }
    }

    @SuppressLint("SetTextI18n")
    private fun withdrawItem(itemView: View, copyHash: (String) -> Unit, onClickHistory: (PortfolioHistoryItem) -> Unit) {
        itemView.iv_ticker.visible(true)
        itemView.cl_tickers.visibility = View.INVISIBLE
        itemView.setOnClickListener {
            onClickHistory.invoke(this)
        }

        loadImage(itemView, cmcIconId, itemView.iv_ticker)

        itemView.tv_symbol.text = symbol
        val amountString = "- ${BigDecimal(amount)} $symbol"
        itemView.tv_ticker_pnl.text = amountString
        itemView.tv_ticker_pnl.newTextColor(R.color.colorRed)

        if (createdAt.isNotEmpty()) {
            try {
                itemView.tv_transaction_date.text = "%s".format(formatDate())
            } catch (e: Exception) {

            }
        }

        val estimatedString =
            "${AccountData.getBaseCurrencyCharacter()}${
                estimatedInBase?.let { roundDouble(it.toDouble(), 2, true) }
            }"
        itemView.tv_estimated_money.text = "≈ $estimatedString"
        itemView.tv_status.text = status

//            itemView.setOnClickListener {
//                if (id.isNotEmpty())
//                    copyHash(id)
//            }
    }

    @SuppressLint("SetTextI18n")
    private fun orderItem(itemView: View, onClickHistory: (PortfolioHistoryItem) -> Unit) {
        itemView.cl_tickers.visible(true)
        itemView.iv_ticker.visibility = View.INVISIBLE
        itemView.setOnClickListener {
            onClickHistory.invoke(this)
        }

        firstTicker = if (firstTicker == "BCHA") "XEC" else firstTicker
        secondTicker = if (secondTicker == "BCHA") "XEC" else secondTicker

        loadImage(itemView, firstTickerIconId, itemView.iv_first_ticker)
        loadImage(itemView, secondTickerIconId, itemView.iv_second_ticker)

        if (createdAt.isNotEmpty()) {
            try {
                itemView.tv_transaction_date.text = "%s".format(formatDate())
            } catch (e: Exception) {

            }
        }

        val estimatedString =
            "${AccountData.getBaseCurrencyCharacter()}${
                estimatedInBase?.let { roundDouble(it.toDouble(), 2, true) }
            }"
        itemView.tv_estimated_money.text = "≈ $estimatedString"

        val character = getCharacter(id)
        val amountString = "$character ${BigDecimal(amount)} $firstTicker"
        itemView.tv_ticker_pnl.setTextColor(getColor(checkColor(id), itemView))
        itemView.tv_ticker_pnl.text = amountString

        val symbolFormattedString = "$firstTicker/$secondTicker"
        itemView.tv_symbol.text = symbolFormattedString
        itemView.tv_status.text = status
    }

    private fun getCharacter(value: String): String =
        when (value.toLowerCase()) {
            buy -> "+"
            sell -> "-"
            else -> ""
        }


    private fun checkColor(value: String): Int =
        when (value.toLowerCase()) {
            buy -> R.color.colorGreen
            sell -> R.color.colorRed
            else -> -1
        }


    private fun getColor(colorId: Int, itemView: View): Int {
        return ContextCompat.getColor(itemView.context, colorId)
    }

    private fun loadImage(itemView: View, cmcIconId: Int = 0, target: ImageView) {
        val requestOptions = RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)
        if (cmcIconId == 7686) {
            Glide.with(itemView.context)
                .load(R.drawable.ic_e_cash_logo)
                .apply(requestOptions)
                .into(target)
        } else Glide.with(itemView.context)
            .load(TradingWalletItem.CMC_ICON_URL.format(cmcIconId))
            .apply(requestOptions)
            .into(target)
    }

    private fun removeDayNight(date: String): String =
        when {
            date.contains("AM") -> date.replace(" AM", "")
            date.contains("PM") -> date.replace(" PM", "")
            else -> ""
        }

    private fun formatDate(): String {
        val timestamp = parseToTimestamp(createdAt)
        val date = DateTimeUtil.dateTimeFromTimestamp(timestamp)
        val dateArray = date.split(" ")
        return "${dateArray[1]} ${dateArray[0]}, ${dateArray[2]} ${dateArray[3]}"
    }


    companion object {
        private const val buy = "buy"
        private const val sell = "sell"
    }
}
