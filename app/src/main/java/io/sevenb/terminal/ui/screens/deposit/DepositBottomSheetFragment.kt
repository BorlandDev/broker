package io.sevenb.terminal.ui.screens.deposit

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Canvas
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.view.View
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.snackbar.Snackbar
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import io.sevenb.terminal.R
import io.sevenb.terminal.data.model.broker_api.account.DepositAddress
import io.sevenb.terminal.domain.usecase.charts.AccountData
import io.sevenb.terminal.ui.base.BaseBottomSheetFragment
import io.sevenb.terminal.ui.base.BaseFragment
import io.sevenb.terminal.ui.extension.viewModelProvider
import io.sevenb.terminal.ui.extension.visible
import io.sevenb.terminal.ui.screens.select_currency.BaseSelectItem
import io.sevenb.terminal.ui.screens.select_currency.DepositCurrencyItem
import io.sevenb.terminal.ui.screens.trading.SharedViewModel
import io.sevenb.terminal.ui.screens.trading.TradingWalletItem
import kotlinx.android.synthetic.main.fragment_deposit_main.*
import java.io.File
import java.io.File.separator
import java.io.FileOutputStream
import java.io.IOException


/**
 * Created by samosudovd on 24/01/2020.
 */
class DepositBottomSheetFragment : BaseFragment() {

    private lateinit var depositViewModel: DepositViewModel
    private lateinit var sharedViewModel: SharedViewModel

    private var localDepositList: List<DepositCurrencyItem> = mutableListOf()

    override fun layoutId() = R.layout.fragment_deposit
    override fun onSearchTapListener() {
    }

//    override fun containerId() = R.id.container_deposit_sized

    private var currentNetwork: String? = null
    private var currentBitmap: Bitmap? = null

    companion object {
        fun newInstance() = DepositBottomSheetFragment()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        depositViewModel = viewModelProvider(viewModelFactory)
        sharedViewModel = activity?.run {
            viewModelProvider(viewModelFactory)
        } ?: throw Exception("Invalid Activity")
        initView()
        subscribeUi()
    }

    private fun initView() {
        startShimmer(true)

        cl_deposit_ticker.setOnClickListener {
            sharedViewModel.selectCurrencyList.value = localDepositList
            currencySelection()
        }
        btnCopy.setOnClickListener { copyAddress() }
        deposit_address.setOnClickListener {
            copyAddress()
        }
        btnCopyExtra.setOnClickListener { copyExtra() }
        deposit_extra_address_using.setOnClickListener { copyExtra() }
        deposit_extra_address.setOnClickListener { copyExtra() }

        iv_qr_code.setOnClickListener {
            copyAddress()
        }

        iv_qr_code.setOnLongClickListener {
            showSnackbardd("Save image to gallery?")
            true
        }

        btnShare.setOnClickListener {
            val sendIntent: Intent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(
                    Intent.EXTRA_TEXT,
                    "${tv_deposit_ticker.text} deposit address: ${deposit_address.text} ${if (currentNetwork != null) "in $currentNetwork network" else ""}"
                )
                type = "text/plain"
            }

            val shareIntent = Intent.createChooser(sendIntent, null)
            startActivity(shareIntent)
        }

        btnShareExtra.setOnClickListener {
            val sendIntent: Intent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(
                    Intent.EXTRA_TEXT,
                    "${tv_deposit_ticker.text} deposit extra Id: ${deposit_extra_address.text}"
                )
                type = "text/plain"
            }

            val shareIntent = Intent.createChooser(sendIntent, null)
            startActivity(shareIntent)
        }

        container_deposit_main.setTransitionListener(object :
            MotionLayout.TransitionListener {
            override fun onTransitionStarted(p0: MotionLayout?, p1: Int, p2: Int) {}

            override fun onTransitionChange(p0: MotionLayout?, p1: Int, p2: Int, p3: Float) {}

            override fun onTransitionCompleted(p0: MotionLayout?, p1: Int) {
                if (p0?.endState == p0?.currentState)
                    (requireParentFragment() as BaseBottomSheetFragment).dismiss()
            }

            override fun onTransitionTrigger(p0: MotionLayout?, p1: Int, p2: Boolean, p3: Float) {}
        })
    }

    private fun screenShot(view: View): Bitmap? {
        val bitmap = Bitmap.createBitmap(
            view.width,
            view.height, Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        view.draw(canvas)
        return bitmap
    }


    private fun saveImageToGallery(context: Context, bmp: Bitmap): Boolean {
        val storePath =
            Environment.getExternalStorageDirectory().absolutePath + separator + "dearxy"
        val appDir = File(storePath)
        if (!appDir.exists()) {
            appDir.mkdir()
        }
        val fileName = System.currentTimeMillis().toString() + ".jpg"
        val file = File(appDir, fileName)
        try {
            val fos = FileOutputStream(file)
            val isSuccess = bmp.compress(Bitmap.CompressFormat.JPEG, 60, fos)
            fos.flush()
            fos.close()

            val uri = Uri.fromFile(file)
            context.sendBroadcast(Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri))
            return isSuccess
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return false
    }

    private fun subscribeUi() {
        depositViewModel.listDepositCoins.observe(viewLifecycleOwner, { depositList(it) })
        depositViewModel.addressDeposit.observe(viewLifecycleOwner, { setupDepositAddress(it) })
        depositViewModel.qrCodeBitmap.observe(viewLifecycleOwner, { qrCodeBitmap(it) })
        depositViewModel.snackbarMessage.observe(viewLifecycleOwner, { showSnackbar(it) })

        sharedViewModel.selectedCurrency.observe(viewLifecycleOwner, {
            if (!localDepositList.isNullOrEmpty())
                tickerSelection(it)
        })
    }

    private fun currencySelection() {
        if (findNavController().currentDestination?.id != R.id.depositViewPager) return

        val direction =
            DepositViewPagerFragmentDirections.actionDepositViewPagerToSelectCurrencyFragment()
        findNavController().navigate(direction)
    }

    private fun setupDepositAddress(address: DepositAddress) {
        val content = SpannableString(address.address)
        content.setSpan(UnderlineSpan(), 0, content.length, 0)
        deposit_address.text = content
        address.extraId.let {
            if (it.isNotEmpty()) {
                val content = SpannableString(it)
                content.setSpan(UnderlineSpan(), 0, content.length, 0)
                deposit_extra_address.text = content
                deposit_extra_address_using.visibility = View.VISIBLE
                deposit_extra_address.text = content
            } else {
                deposit_extra_address_using.visibility = View.GONE
                deposit_extra_address.text = ""
            }
        }
    }

    private fun qrCodeBitmap(bitmap: Bitmap?) {
        bitmap?.let {
            startShimmer(false)
        }
        currentBitmap = bitmap
        iv_qr_code.setImageBitmap(bitmap)
    }

    private fun copyAddress() {
        depositViewModel.copyToClipboard(deposit_address.text.toString())
        showSnackbar(getString(R.string.message_address_copied))
    }

    private fun copyExtra() {
        depositViewModel.copyToClipboard(deposit_extra_address.text.toString())
        showSnackbar(getString(R.string.message_extra_copied))
    }

    private fun depositList(list: List<DepositCurrencyItem>) {
        localDepositList = list
        tickerSelection(list.firstOrNull() ?: return)
    }

    private fun tickerSelection(baseSelectItem: BaseSelectItem?) {
        if (baseSelectItem !is DepositCurrencyItem) return
        sharedViewModel.selectedCurrency.value = null

        startShimmer(true)
        loadTicker(baseSelectItem.cmcIconId)
        tv_deposit_ticker.text = baseSelectItem.ticker
        val tickerText = if (baseSelectItem.ticker != baseSelectItem.network) {
            currentNetwork = baseSelectItem.network
            "%s (%s %s)".format(baseSelectItem.name, baseSelectItem.network, "network")
        } else {
            currentNetwork = baseSelectItem.ticker
            "%s".format(baseSelectItem.name)
        }

        resetAddressFields()
        depositViewModel.requestDepositAddress(baseSelectItem)
    }

    private fun resetAddressFields() {
        deposit_address.text = ""
        qrCodeBitmap(null)
    }

    private fun showSnackbar(message: String) {
        if (!message.contains(AccountData.ERROR_CODE_500))
            Snackbar.make(requireView(), message, Snackbar.LENGTH_LONG)
                .apply {
                    view.elevation = Resources.getSystem().displayMetrics.density * 16f
                }
                .show()
    }

    private fun showSnackbardd(message: String) {
        val snak =
            Snackbar.make(requireView(), message, Snackbar.LENGTH_LONG)
                .apply {
                    view.elevation = Resources.getSystem().displayMetrics.density * 16f
                }
        snak.setAction("Yes") { requestReadPermissions() }
        snak.show()
    }

    private fun requestReadPermissions() {
        Dexter.withActivity(requireActivity())
            .withPermissions(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
                        screenShot(container_deposit_main)?.let { it1 ->
                            val result = saveImageToGallery(
                                requireContext(),
                                it1
                            )
                            showSnackbar(if (result) "Saved" else "Oops, something goes wrong. Try later")
                        }
                    }

                    if (report.isAnyPermissionPermanentlyDenied) {
                        showSnackbar("In order to save the image, give the application the required permission")
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest>,
                    token: PermissionToken
                ) {
                    token.continuePermissionRequest()
                }

            }).withErrorListener {
                showSnackbar("Oops, something goes wrong")
            }
            .onSameThread()
            .check()
    }

    override fun onPause() {
        sgl_deposit.stopShimmerAnimation()
        sgl_deposit.isVisible = false
        cl_container_deposit_main.visible(true)
        enableAnimation(true)

        super.onPause()
    }

    private fun loadTicker(iconId: Int) {
        val requestOptions = RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)
        Glide.with(requireContext())
            .load(TradingWalletItem.CMC_ICON_URL.format(iconId))
            .apply(requestOptions)
            .into(iv_deposit_currency)
    }

    private fun enableAnimation(value: Boolean) {
        container_deposit_main.getTransition(R.id.deposit_transition).setEnable(value)
    }

    private fun startShimmer(value: Boolean) {
        sgl_deposit.visible(value)
        cl_container_deposit_main.visible(!value)
        if (value) sgl_deposit.startShimmerAnimation()
        else sgl_deposit.stopShimmerAnimation()
        enableAnimation(!value)
    }
}
