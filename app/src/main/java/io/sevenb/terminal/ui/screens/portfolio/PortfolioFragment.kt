package io.sevenb.terminal.ui.screens.portfolio

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.mikephil.charting.data.Entry
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.checkbox.MaterialCheckBox
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import io.sevenb.terminal.R
import io.sevenb.terminal.data.datasource.database.model.CurrencyEntity
import io.sevenb.terminal.data.model.enum_model.HistoryType.*
import io.sevenb.terminal.data.model.enums.PortfolioTabs
import io.sevenb.terminal.data.model.order.OrderItem
import io.sevenb.terminal.data.model.trading.TradeChartParams
import io.sevenb.terminal.data.model.trading.TradeChartRange
import io.sevenb.terminal.data.repository.auth.PreferenceRepository
import io.sevenb.terminal.domain.usecase.charts.AccountData
import io.sevenb.terminal.ui.base.BaseFragment
import io.sevenb.terminal.ui.extension.*
import io.sevenb.terminal.ui.screens.order.OrderBottomSheetFragment
import io.sevenb.terminal.ui.screens.select_currency.BaseSelectItem
import io.sevenb.terminal.ui.screens.settings.SettingsFragment
import io.sevenb.terminal.ui.screens.trading.SharedViewModel
import io.sevenb.terminal.ui.screens.trading.TradingListAdapter
import kotlinx.android.synthetic.main.fragment_portfolio.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class PortfolioFragment : BaseFragment() {

    private lateinit var portfolioViewModel: PortfolioViewModel
    private lateinit var sharedViewModel: SharedViewModel

    private val assetsAdapter = TradingListAdapter(
        ::orderScreen,
        ::updateItemChartByPosition,
        ::assetListScrollEnabling,
        { },
        { },
        ::scrollToListPosition,
        updateCandleChartData = ::updateCandleChartData
    )
    private val openedOrdersAdapter = OpenedOrdersAdapter(::cancelOrder)
    private val portfolioHistoryAdapter = PortfolioHistoryAdapter(::copyHash, ::onClickHistory)

    private var localListAssets: List<AssetItem> = mutableListOf()
    private var localListHistory: List<BaseHistoryItem> = mutableListOf()
    private var localListOrders: List<OrderItem> = mutableListOf()

    private var onHistoryClicked = false
    private var onHistoryRequested = false
    private var savedCurrencies: MutableList<CurrencyEntity>? = mutableListOf()

    private val listOfWithdraw: MutableList<CurrencyEntity>? = mutableListOf()

    override fun layoutId() = R.layout.fragment_portfolio
    override fun hasBottomNavigation() = true
    override fun toolbarTitleRes() = R.string.title_portfolio_tab
    override fun hasToolbarSearch() = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        portfolioViewModel = viewModelProvider(viewModelFactory)
        sharedViewModel = activity?.run {
            viewModelProvider(viewModelFactory)
        } ?: throw Exception("Invalid Activity")
        handleArgs()
        initView()
        subscribeUi()
    }

    private fun handleArgs() {
        val isShowPopup = arguments?.getBoolean("isShowPopup") ?: false
        if (isShowPopup) portfolioViewModel.tryToShowInfoConfirmation()
    }

    override fun onSearchTapListener() {
        sharedViewModel.selectCurrencyList.value = localListAssets
        currencySelection()
    }

    private fun initView() {
        showProgressBar(true)
        label_assets_tab.setOnClickListener {
            portfolioViewModel.tab = PortfolioTabs.ASSETS
            startShimmer(false)
            onHistoryClicked = false
            onHistoryRequested = false
            resetTabPicker()

            label_assets_tab.newBackground(R.drawable.bg_tab_title_selected)
            label_assets_tab.newTextColor(R.color.colorTextMain)

            asset_list.visible(true)
            setGoneViews(order_list, history_list, history_filters, container_placeholder)

            if (!localListAssets.isNullOrEmpty())
                assetsAdapter.submitList(localListAssets)
            else
                portfolioViewModel.accountBalanceList()

//            assetList(localListAssets)
            showProgressBar(localListAssets.isEmpty())
            if (swipe_to_refresh.isRefreshing) swipe_to_refresh.isRefreshing = false
        }
        title_orders_tab.setOnClickListener {
            portfolioViewModel.tab = PortfolioTabs.ORDERS
            startShimmer(false)
            onHistoryClicked = false
            onHistoryRequested = false
            resetTabPicker()
            title_orders_tab.newBackground(R.drawable.bg_tab_title_selected)
            title_orders_tab.newTextColor(R.color.colorTextMain)

            order_list.visible(true)
            setGoneViews(asset_list, history_list, history_filters, container_placeholder)

            savedCurrencies?.let { portfolioViewModel.orderOpened(it) }
            showProgressBar(openedOrdersAdapter.baseItemsList.isEmpty())
            if (!localListOrders.isNullOrEmpty()) openedOrdersAdapter.submitList(localListOrders)
            if (swipe_to_refresh.isRefreshing) swipe_to_refresh.isRefreshing = false
        }
        title_history_tab.setOnClickListener {
            portfolioViewModel.tab = PortfolioTabs.HISTORY
            onHistoryClicked = true
            resetTabPicker()

            title_history_tab.newBackground(R.drawable.bg_tab_title_selected)
            title_history_tab.newTextColor(R.color.colorTextMain)

            setVisibleViews(history_list, history_filters)
            setGoneViews(asset_list, order_list, container_placeholder)

            if (sharedViewModel.historyFilter.value == null) {
                if (!onHistoryRequested && !savedCurrencies.isNullOrEmpty()) {
                    onHistoryRequested = true
                    portfolioViewModel.makeRequestsByFilter(
                        mutableListOf(
                            DEPOSIT, WITHDRAW, ORDERS
                        ), savedCurrencies!!
                    )
                }
            }

            sharedViewModel.historyFilter.value?.let {
                if (!onHistoryRequested && !savedCurrencies.isNullOrEmpty()) {
                    onHistoryRequested = true
                    portfolioViewModel.makeRequestsByFilter(it, savedCurrencies!!)
                }
            }

            if (!localListHistory.isNullOrEmpty()) portfolioHistoryAdapter.submitList(
                localListHistory
            ) else startShimmer(true)
            if (swipe_to_refresh.isRefreshing) swipe_to_refresh.isRefreshing = false
        }

        history_filters.setOnClickListener { historyTypeFragment() }

        initLists()

        button_deposit.setOnClickListener { depositScreen() }
        button_withdraw.setOnClickListener { withdrawScreen() }

        open_search_portfolio.setOnClickListener {
            sharedViewModel.selectCurrencyList.value = localListAssets
            currencySelection()
        }

        if (SettingsFragment.hasUpdated) portfolioViewModel.marketRate()

        swipe_to_refresh.setOnRefreshListener {
            when (portfolioViewModel.tab) {
                PortfolioTabs.ASSETS -> portfolioViewModel.accountBalanceList()
                PortfolioTabs.HISTORY -> {
                    sharedViewModel.historyFilter.value?.let {
                        if (!savedCurrencies.isNullOrEmpty()) {
                            onHistoryRequested = true
                            portfolioViewModel.makeRequestsByFilter(it, savedCurrencies!!)
                        }
                    }
                        ?: if (!savedCurrencies.isNullOrEmpty()) {
                            onHistoryRequested = true
                            portfolioViewModel.makeRequestsByFilter(
                                mutableListOf(DEPOSIT, WITHDRAW, ORDERS), savedCurrencies!!
                            )
                        }

                    showProgressBar(sharedViewModel.historyFilter.value == null && localListHistory.isNullOrEmpty())
                }
                PortfolioTabs.ORDERS -> {
                    savedCurrencies?.let { portfolioViewModel.orderOpened(it) }

                    showProgressBar(portfolioHistoryAdapter.baseItemsList.isEmpty())
                }
            }
        }
        swipe_to_refresh.setColorSchemeColors(
            ContextCompat.getColor(
                requireContext(),
                R.color.colorSelectPercent
            )
        )
    }

    private fun resetTabPicker() {
        label_assets_tab.setBackgroundResource(0)
        title_orders_tab.setBackgroundResource(0)
        title_history_tab.setBackgroundResource(0)

        label_assets_tab.newTextColor(R.color.colorTextSecond)
        title_orders_tab.newTextColor(R.color.colorTextSecond)
        title_history_tab.newTextColor(R.color.colorTextSecond)
    }

    private fun initLists() {
        asset_list.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(
                context, LinearLayoutManager.VERTICAL, false
            )
            adapter = assetsAdapter
        }
        order_list.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(
                context, LinearLayoutManager.VERTICAL, false
            )
            adapter = openedOrdersAdapter
        }
        history_list.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(
                context, LinearLayoutManager.VERTICAL, false
            )
            adapter = portfolioHistoryAdapter
        }
    }

    private fun subscribeUi() {
        portfolioViewModel.apply {
            totalHolding.observe(viewLifecycleOwner, { updateTotalSum(it) })
            lockedBalance.observe(viewLifecycleOwner, { updateLockedSum(it) })
            availableBalance.observe(viewLifecycleOwner, { updateAvailableSum(it) })
            balancesList.observe(viewLifecycleOwner, { assetList(it) })
            orderOpened.observe(viewLifecycleOwner, { orderOpenedList(it) })
            cancelOrder.observe(viewLifecycleOwner, { cancelOrderResult(it) })
            historyListItems.observe(viewLifecycleOwner, { historyItem(it) })
            snackbarMessage.observe(viewLifecycleOwner, { showSnackBarMessage(it) })
            rate.observe(viewLifecycleOwner, { baseCurrencyAvailableSum(it) })
            showInfoDialog.observe(viewLifecycleOwner, { showInfoConfirmationDialog() })
            totalInBaseCurrency.observe(viewLifecycleOwner, { baseCurrencyAmount(it) })
            chartDataResult.observe(viewLifecycleOwner, { chartDataResult(it) })
            updateAssets.observe(viewLifecycleOwner, { sharedViewModel.updateAssets = false })
            dbRepository.getAll().observe(viewLifecycleOwner, { processCurrencies(it) })
//            showRateUs.observe(viewLifecycleOwner, { dialogDoYouLike(it) })
        }

        sharedViewModel.apply {
            selectedCurrency.observe(viewLifecycleOwner, { selectedSearchItem(it) })
            updateAssetsAfterTransaction.observe(
                viewLifecycleOwner, { processUpdateAssetsAfterTransaction(it) })
            historyFilter.observe(
                viewLifecycleOwner,
                {
                    if (!savedCurrencies.isNullOrEmpty()) {
                        onHistoryRequested = true
                        startShimmer(true)
                        updatePlaceholderImage(visibility = false)
                        swipe_to_refresh.isRefreshing = true
                        portfolioViewModel.makeRequestsByFilter(it, savedCurrencies!!)
                    }
                })
            shouldUpdateBalance.observe(
                viewLifecycleOwner,
                { processUpdateAssetsAfterTransaction(it) })
        }
    }

//    private fun dialogDoYouLike(isInAppReviewEnabled: Boolean) {
//        val dialog = DialogDoYouLike.getInstance(isInAppReviewEnabled)
//        dialog.show(parentFragmentManager, "")
//        dialog.initStateListener {
//            when (it) {
//                RateUsStates.LEAVE_IT ->{
////                    portfolioViewModel.setDontAskVersion()
//                }
//                RateUsStates.ASK_LATER -> {
////                    portfolioViewModel.setAskLaterTime()
////                    portfolioViewModel.resetRateUs()
////                    portfolioViewModel.disableRateUs()
//                }
//                RateUsStates.NO -> {
////                    portfolioViewModel.resetRateUs()
//                    dialogWhatCanWe()
//                }
//                RateUsStates.YES -> {
////                    portfolioViewModel.enableRateUs()
////                    portfolioViewModel.resetRateUs()
//                }
//                RateUsStates.YES_GOOGLE_PLAY -> {
////                    portfolioViewModel.resetRateUs()
//                    portfolioViewModel.setLastRateTime()
//                }
//            }
//        }
//    }

//    private fun dialogWhatCanWe() {
//        val dialog = DialogWhatCanWe.getInstance()
//        dialog.show(parentFragmentManager, "")
//        dialog.initListenerConfirm {
//            if (it) {
////                portfolioViewModel.resetRateUs()
//                dialogThankYou()
//            } else {
//                portfolioViewModel.showRateUs(true)
//            }
//        }
//    }

//    private fun dialogThankYou() {
//        val dialog = DialogThankYou.getInstance()
//        dialog.show(parentFragmentManager, "")
//    }

    private fun processCurrencies(list: List<CurrencyEntity>?) {
        savedCurrencies = list?.toMutableList()
        list?.let {
            if (onHistoryClicked) {
                val historyFilter = sharedViewModel.historyFilter.value

                onHistoryRequested = true

                if (historyFilter == null) {
                    portfolioViewModel.makeRequestsByFilter(
                        mutableListOf(DEPOSIT, WITHDRAW, ORDERS), it
                    )
                } else {
                    portfolioViewModel.makeRequestsByFilter(historyFilter, it)
                }
            }
        }
    }

    private fun processUpdateAssetsAfterTransaction(value: Boolean?) {
        if (value == true) {
            portfolioViewModel.accountBalanceList()
            sharedViewModel.apply {
                updateAssetsAfterTransaction.value = false
                shouldUpdateBalance.value = false
            }
        }
    }

    private fun baseCurrencyAmount(amount: String) {
        val amountWithCharacter = "${AccountData.getBaseCurrencyCharacter()} $amount"
        total_sum_base_currency.text = amountWithCharacter
    }

    private fun baseCurrencyAvailableSum(rate: Float) {
        if (sharedViewModel.updateAssets) {
            total_sum_base_currency.text = ""
            ticker_total_sum_base_currency.text = PreferenceRepository.baseCurrencyTicker
        }
    }

    private fun updateTotalSum(totalSum: String) {
        if (!sharedViewModel.updateAssets) {
            total_sum.text = totalSum
            ticker_total_sum.text = AccountData.BTC
        } else {
            total_sum.text = ""
        }
        swipe_to_refresh.isRefreshing = false
    }

    private fun updatePlaceholderImage(
        placeholderImageId: Int? = null,
        placeholderText: String? = null,
        visibility: Boolean
    ) {
        if (visibility) {
            img_placeholder.setImageDrawable(placeholderImageId?.let { requireContext().getDrawable(it) })
            tv_placeholder.text = placeholderText
            container_placeholder.visible(true)
        } else container_placeholder.visible(false)
    }

    private fun updateLockedSum(lockedSum: String) {
        sum_on_orders.text = lockedSum
    }

    private fun updateAvailableSum(availableSum: String) {
        sum_available.text = availableSum
    }

    private fun assetList(listAssets: List<AssetItem>) {
        if (!sharedViewModel.updateAssets) {
            showSearchIcon(listAssets.size)
            localListAssets = listAssets
        }

        if (portfolioViewModel.tab == PortfolioTabs.ASSETS) {
            showProgressBar(false)
            assetsAdapter.submitList(listAssets)


            if (listAssets.isEmpty()) updatePlaceholderImage(
                R.drawable.ic_assets_placeholder,
                assetsPlaceholderText,
                true
            ) else updatePlaceholderImage(visibility = false)
        }
    }

    private fun showSearchIcon(size: Int) {
        open_search_portfolio.visible(size > 5)
    }

    private fun showProgressBar(value: Boolean) {
        progress_portfolio.visible(value)
    }

    private fun copyHash(hash: String) {
        portfolioViewModel.copyToClipboard(hash)
        showSnackBarMessage(getString(R.string.message_hash_copied))
    }

    private fun onClickHistory(item: PortfolioHistoryItem) {
        Log.e("!!!", "click - $item")
        if (SettingsFragment.hasUpdated) portfolioViewModel.marketRate()
        when(item.type) {
            ORDERS -> {
                val direction =
                    PortfolioFragmentDirections.actionPortfolioFragmentToShowStatusBottomSheetFragment(item)
                findNavController().navigate(direction)
            }
            WITHDRAW -> {
                val direction =
                    PortfolioFragmentDirections.actionPortfolioFragmentToShowWithdrawDepositeBottomSheetFragment(item)
                findNavController().navigate(direction)
            }
            DEPOSIT -> {
                val direction =
                    PortfolioFragmentDirections.actionPortfolioFragmentToShowStatusDepositeBottomSheetFragment(item)
                findNavController().navigate(direction)
            }
        }

    }

    private fun updateCandleChartData(tradeChartParams: TradeChartParams) {
        portfolioViewModel.updateCandleChartData(tradeChartParams)
    }

    private fun orderOpenedList(listOrders: List<OrderItem>) {
        localListOrders = listOrders

        if (portfolioViewModel.tab == PortfolioTabs.ORDERS) {
            showProgressBar(false)
            openedOrdersAdapter.submitList(listOrders)
            swipe_to_refresh.isRefreshing = false
            if (listOrders.isEmpty()) updatePlaceholderImage(
                R.drawable.ic_orders_placeholder,
                ordersPlaceholderText,
                true
            ) else updatePlaceholderImage(visibility = false)
        }
    }

    private fun cancelOrder(baseOrderListItem: BaseOrderListItem) {
        if (baseOrderListItem !is OrderItem) return

        portfolioViewModel.cancelOrder(baseOrderListItem)
    }

    private fun cancelOrderResult(orderItem: OrderItem) {
        val message = getString(R.string.message_order_canceled).format(orderItem.orderId)
        showSnackBarMessage(message)
        runBlocking {
            delay(1000)
            savedCurrencies?.let { portfolioViewModel.orderOpened(it) }
        }
    }

    private fun historyItem(listHistoryItems: List<BaseHistoryItem>) {
        localListHistory = listHistoryItems

        if (!listHistoryItems.isNullOrEmpty()) {
            onHistoryRequested = false
        }

        showProgressBar(false)
        if (portfolioViewModel.tab == PortfolioTabs.HISTORY) {
            portfolioHistoryAdapter.submitList(listHistoryItems)
            swipe_to_refresh.isRefreshing = false
            startShimmer(false)
            if (listHistoryItems.isEmpty()) updatePlaceholderImage(
                R.drawable.ic_history_placeholder, historyPlaceholderText, true
            ) else updatePlaceholderImage(visibility = false)
        }
    }

    private fun depositScreen() {
        if (findNavController().currentDestination?.id != R.id.portfolioFragment) return

        val direction =
            PortfolioFragmentDirections.actionPortfolioFragmentToDepositViewPager()
        findNavController().navigate(direction)
    }

    private fun withdrawScreen() {
        if (findNavController().currentDestination?.id != R.id.portfolioFragment) return

        val direction =
            PortfolioFragmentDirections.actionPortfolioFragmentToWithdrawBottomSheetFragment()
        findNavController().navigate(direction)
    }

    private fun currencySelection() {
        if (findNavController().currentDestination?.id != R.id.portfolioFragment) return

        val direction =
            PortfolioFragmentDirections.actionPortfolioFragmentToSelectCurrencyFragment()
        findNavController().navigate(direction)
    }

    private fun historyTypeFragment() {
        if (findNavController().currentDestination?.id != R.id.portfolioFragment) return

        val direction =
            PortfolioFragmentDirections.actionPortfolioFragmentToHistoryFilterBottomSheetFragment()
        findNavController().navigate(direction)
    }

    private fun orderScreen(tickerAndType: Pair<String, OrderBottomSheetFragment.Companion.OrderSide>) {
        if (findNavController().currentDestination?.id != R.id.portfolioFragment) return

        sharedViewModel.orderTicker.value = tickerAndType
        val direction =
            PortfolioFragmentDirections.actionPortfolioFragmentToOrderBottomSheetFragment2()
        findNavController().navigate(direction)
    }

    private fun updateItemChartByPosition(tradeChartParams: TradeChartParams) {
        try {
            val item = localListAssets[tradeChartParams.listPosition]
            portfolioViewModel.updateItemChartData(item.ticker, tradeChartParams)
        } catch (e: Exception) {
        }
    }

    private fun chartDataResult(pairResult: Pair<TradeChartParams, List<Entry>>) {
        val tradeChartParams = pairResult.first
        val listEntries = pairResult.second

        portfolioViewModel.getPositionByTicker(tradeChartParams.ticker, localListAssets)?.let {
            if (tradeChartParams.tradeChartRange == TradeChartRange.DAY) {
                localListAssets[it].chartEntries = listEntries
                localListAssets[it].expandedChartEntries = listEntries
            } else {
                localListAssets[it].expandedChartEntries = listEntries
            }
        }

        val payload = mutableListOf<Any>()
        payload.add(listEntries)
        payload.add(tradeChartParams)

        assetsAdapter.notifyItemChanged(tradeChartParams.listPosition, payload)
    }

    private fun assetListScrollEnabling(isEnabled: Boolean) {
        try {
            swipe_to_refresh.isEnabled = isEnabled
            asset_list.suppressLayout(!isEnabled)
        } catch (e: IllegalStateException) {
        }
    }

    private fun selectedSearchItem(baseSelectItem: BaseSelectItem?) {
        if (baseSelectItem !is AssetItem) return
        sharedViewModel.selectedCurrency.value = null

        val index = localListAssets.indexOf(baseSelectItem)
        scrollToListPosition(index)
    }

    private fun scrollToListPosition(position: Int) {
        asset_list.scrollToPosition(position)
    }

    private fun showInfoConfirmationDialog() {
        MaterialAlertDialogBuilder(requireContext(), R.style.AlertDialogTheme).apply {
            val infoConfirmationDialogView = requireActivity().layoutInflater.inflate(
                R.layout.dialog_info_confirmation, null
            )

            setCancelable(false)

            val dontShowCheckBox =
                infoConfirmationDialogView.findViewById<MaterialCheckBox>(R.id.dont_show_checkbox)

            setView(infoConfirmationDialogView)

            setPositiveButton(android.R.string.ok) { dialog, _ ->
                if (dontShowCheckBox.isChecked) portfolioViewModel.storeInfoConfirmationDialogState()
                dialog.dismiss()
            }
        }.create().show()
    }

    private fun showSnackBarMessage(message: String) {
        val bottomNavView: BottomNavigationView? = activity?.findViewById(R.id.bottomNavView)
        bottomNavView?.let {
            if (!message.contains(AccountData.ERROR_CODE_500))
                Snackbar.make(bottomNavView, message, Snackbar.LENGTH_SHORT).apply {
                    anchorView = bottomNavView
                }.show()
        }
    }

    private fun startShimmer(value: Boolean) {
        if (portfolioViewModel.tab == PortfolioTabs.HISTORY) {
            history_list.visible(!value)
            if (value) sgl_history.startShimmerAnimation()
            else sgl_history.stopShimmerAnimation()
        }
        sgl_history.visible(value)
    }

    override fun onPause() {
        startShimmer(false)
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        lifecycleScope.launch {
            delay(300)
            hideKeyboard(requireActivity())
        }
    }

    companion object {
        private const val assetsPlaceholderText =
            "Your assets will be shown here.\n Top up your balance to begin."
        private const val ordersPlaceholderText = "No open orders \nfor now."
        private const val historyPlaceholderText =
            "Your deposit, withdrawal and \ntrading actions will be listed here."
    }
}