package io.sevenb.terminal.ui.extension


import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.view.View
import android.view.WindowInsets
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import androidx.core.content.PermissionChecker
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import io.sevenb.terminal.ui.base.BaseFragment

inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> FragmentTransaction) =
    beginTransaction().func().commit()

inline fun <reified VM : ViewModel> Fragment.viewModelProvider(
    provider: ViewModelProvider.Factory
) =
    ViewModelProvider(this, provider).get(VM::class.java)

fun BaseFragment.close() = parentFragmentManager.popBackStack()

val BaseFragment.appContext: Context get() = activity?.applicationContext!!

fun hideKeyboard(activity: Activity?) {
    activity?.let {
        val imm = it.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view: View? = it.currentFocus
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}

fun hideKeyboard(context: Context, view: View) {
    val imm = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}

fun setGoneViews(vararg views: View?) = views.forEach { it?.isVisible = false }

fun Fragment.setVisibleViews(vararg views: View?) = views.forEach { it?.isVisible = true }

fun setEnabledViews(value: Boolean, vararg views: View?) = views.forEach { it?.isEnabled = value }

fun setViewsVisibility(value: Boolean, vararg views: View?) =
    views.forEach { it?.isVisible = value }

fun Fragment.setInvisibleViews(vararg views: View?) = views.forEach {
    it?.visibility = View.INVISIBLE
    it?.clearFocus()
}

fun batchSameBackgrounds(backgroundResourceId: Int, vararg views: View?) =
    views.forEach { view -> view?.newBackground(backgroundResourceId) }

fun Fragment.checkPermission(permissionName: String): Boolean {
    return if (Build.VERSION.SDK_INT >= 23) {
        val granted =
            ContextCompat.checkSelfPermission(requireContext(), permissionName)
        granted == PackageManager.PERMISSION_GRANTED
    } else {
        val granted =
            PermissionChecker.checkSelfPermission(requireContext(), permissionName)
        granted == PermissionChecker.PERMISSION_GRANTED
    }
}

fun setFocusListeners(vararg views: EditText?) {
    views.forEach {
        if (it != null) {
            it.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
                if (hasFocus) {
                    it.cursorToLastIfSelected()
                }
            }
        }
    }
}

@Suppress("DEPRECATION")
fun Fragment.setFullScreen(isFullScreen: Boolean) {
    if (isFullScreen) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            requireActivity().window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            requireActivity().window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
            )
        }
    } else {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            requireActivity().window.insetsController?.show(WindowInsets.Type.statusBars())
        } else {
            requireActivity().window.clearFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN
            )
        }
    }
}

fun Fragment.setStatusBarColor(@ColorRes colorId: Int) {
    requireActivity().window.statusBarColor = ContextCompat.getColor(requireContext(), colorId)
}

