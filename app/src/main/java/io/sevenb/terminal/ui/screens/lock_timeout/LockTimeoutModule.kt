package io.sevenb.terminal.ui.screens.lock_timeout

import androidx.lifecycle.LifecycleCoroutineScope
import io.sevenb.terminal.R
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.domain.usecase.safety.AccountSafetyStoring
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import javax.inject.Inject

class LockTimeoutModule @Inject constructor(
    private val safetyStoring: AccountSafetyStoring
) {

    private var lifecycleScope: LifecycleCoroutineScope? = null
    private var sharedLockModel: SharedLockModel? = null

    fun init(
        lifecycleScope: LifecycleCoroutineScope,
        sharedLockModel: SharedLockModel
    ) {
        this.lifecycleScope = lifecycleScope
        this.sharedLockModel = sharedLockModel
    }

    suspend fun restoreLockData(): Int? {
        return withContext(Main) {
            val timeRes = safetyStoring.restorePauseTime()
            val isLocked = safetyStoring.restoreTimeoutEnabled()
            if (timeRes is Result.Success && isLocked is Result.Success && isLocked.data) {
                val time = timeRes.data
                if (time.isNotEmpty()) {

                    try {
                        val longTime = time.toLong()

                        val minuteAsync = async { safetyStoring.restoreMinuteLock() }
                        val fiveMinutesAsync = async { safetyStoring.restoreFiveMinutesLock() }
                        val halfHourAsync = async { safetyStoring.restoreHalfHourLock() }
                        val hourAsync = async { safetyStoring.restoreHourLock() }
                        val rebootAsync = async { safetyStoring.restoreRebootLock() }
                        val rebootResult = async { safetyStoring.restoreIsRebooted() }

                        return@withContext awaitData(
                            minuteAsync.await(),
                            fiveMinutesAsync.await(),
                            halfHourAsync.await(),
                            hourAsync.await(),
                            rebootAsync.await(),
                            longTime,
                            rebootResult.await()
                        )

                    } catch (e: Exception) {
                        return@withContext null
                    }
                } else null
            } else null
        }
    }

    private fun awaitData(
        minRes: Result<Boolean>,
        fiveMinRes: Result<Boolean>,
        halfRes: Result<Boolean>,
        hourRes: Result<Boolean>,
        rebootRes: Result<Boolean>,
        longTime: Long,
        rebootResult: Result<Boolean>
    ): Int? {
        return if (minRes is Result.Success &&
            fiveMinRes is Result.Success &&
            halfRes is Result.Success &&
            hourRes is Result.Success &&
            rebootRes is Result.Success &&
            rebootResult is Result.Success
        ) {
            val _1 = minRes.data
            val _5 = fiveMinRes.data
            val _30 = halfRes.data
            val _60 = hourRes.data
            val _0 = rebootRes.data

            val currentTime = System.currentTimeMillis()

            when {
                _1 -> {
                    if (currentTime - longTime >= 1000 * 60) R.id.safetyFragment
                    else R.id.splashScreenFragment
                }
                _5 -> {
                    if (currentTime - longTime >= 1000 * 5 * 60) R.id.safetyFragment
                    else R.id.splashScreenFragment
                }
                _30 -> {
                    if (currentTime - longTime >= 1000 * 30 * 60) R.id.safetyFragment
                    else R.id.splashScreenFragment
                }
                _60 -> {
                    if (currentTime - longTime >= 1000 * 60 * 60) R.id.safetyFragment
                    else R.id.splashScreenFragment
                }
                _0 -> {
                    if (rebootResult.data) {
                        runBlocking { safetyStoring.storeIsRebooted(false) }
                        R.id.safetyFragment
                    } else R.id.splashScreenFragment
                }
                else -> null
            }
        } else null
    }

    fun removeData() {
        lifecycleScope = null
        sharedLockModel = null
    }
}