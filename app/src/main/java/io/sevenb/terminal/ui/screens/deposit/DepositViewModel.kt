package io.sevenb.terminal.ui.screens.deposit

import android.graphics.Bitmap
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.sevenb.terminal.data.model.broker_api.account.DepositAddress
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.domain.usecase.deposit.ClipboardCopy
import io.sevenb.terminal.domain.usecase.deposit.QrCodeBuilder
import io.sevenb.terminal.domain.usecase.deposit.RequestDepositAddress
import io.sevenb.terminal.domain.usecase.sort.SortCurrenciesHandler
import io.sevenb.terminal.domain.usecase.trading.TradingCurrencies
import io.sevenb.terminal.ui.screens.select_currency.DepositCurrencyItem
import io.sevenb.terminal.utils.ConnectionStateMonitor
import kotlinx.coroutines.launch
import javax.inject.Inject

class DepositViewModel @Inject constructor(
    private val tradingCurrencies: TradingCurrencies,
    private val requestDepositAddress: RequestDepositAddress,
    private val qrCodeBuilder: QrCodeBuilder,
    private val clipboardCopy: ClipboardCopy,
    private val sortCurrenciesHandler: SortCurrenciesHandler
) : ViewModel() {

    val listDepositCoins = MutableLiveData<List<DepositCurrencyItem>>()
    val addressDeposit = MutableLiveData<DepositAddress>()
    val qrCodeBitmap = MutableLiveData<Bitmap>()
    val snackbarMessage = MutableLiveData<String>()

    private var showTimeError = 0L

    init {
        depositList()
    }

    private fun depositList() {
        viewModelScope.launch {
            when (val depositListResult = tradingCurrencies.mappedDepositList()) {
                is Result.Success -> listDepositCoins.value =
                    sortDepositList(depositListResult.data)
                is Result.Error -> {
                    when {
                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                        else -> snackbarMessage.value = depositListResult.exception.message
                    }
                }
            }
        }
    }

    private fun showNoInternetConnection() {
        val currentTime = System.currentTimeMillis()
        if (currentTime - showTimeError >= 1500) {
            snackbarMessage.value = ConnectionStateMonitor.noInternetError
            showTimeError = currentTime
        }
    }

    private fun sortDepositList(currencyItems: List<DepositCurrencyItem>): List<DepositCurrencyItem> {
        val btcFiltered: MutableList<DepositCurrencyItem> =
            sortCurrenciesHandler.makeSingleBTCTicker(currencyItems)

        return sortCurrenciesHandler.sortByTicker(btcFiltered)
    }

    fun requestDepositAddress(currency: DepositCurrencyItem) {
        viewModelScope.launch {
            when (val depositAddressResult = requestDepositAddress.execute(currency)) {
                is Result.Success -> {
                    val address = depositAddressResult.data
                    addressDeposit.value = address
                    qrCodeFromAddress(address.address)
                }
                is Result.Error -> {
                    when {
                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                        /*else -> snackbarMessage.value = depositAddressResult.exception.message*/
                    }
                }
            }
        }
    }

    private fun qrCodeFromAddress(address: String) {
        viewModelScope.launch {
            when (val qrCodeResult = qrCodeBuilder.execute(address)) {
                is Result.Success -> qrCodeBitmap.value = qrCodeResult.data
                is Result.Error -> {
                    when {
                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
                        else -> snackbarMessage.value = qrCodeResult.exception.message
                    }
                }
            }
        }
    }

    fun copyToClipboard(text: String) {
        viewModelScope.launch {
            clipboardCopy.copyToClipboard(text)
        }
    }

}