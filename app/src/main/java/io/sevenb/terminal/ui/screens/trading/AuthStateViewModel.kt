package io.sevenb.terminal.ui.screens.trading

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.sevenb.terminal.data.model.enums.AuthState
import javax.inject.Inject

class AuthStateViewModel @Inject constructor() : ViewModel() {

    val stackState = MutableLiveData<AuthState>()

    val emitToActivityRestoreState = MutableLiveData<AuthState>()
    val emitToFragmentRestoreState = MutableLiveData<AuthState>()

    val emitActivityRegistration = MutableLiveData<AuthState>()
    val emitFragmentRegistration = MutableLiveData<AuthState>()
}