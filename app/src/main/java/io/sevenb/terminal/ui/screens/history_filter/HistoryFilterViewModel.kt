package io.sevenb.terminal.ui.screens.history_filter

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.sevenb.terminal.data.model.enum_model.HistoryType
import javax.inject.Inject

class HistoryFilterViewModel @Inject constructor() : ViewModel() {

    val historyType = MutableLiveData<HistoryType>()

    fun selectHistoryType(historyType: HistoryType) {
        this.historyType.value = historyType
    }

}