package io.sevenb.terminal.ui.screens.alert.fragment

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.lifecycle.ViewModel
import com.google.android.material.snackbar.Snackbar
import io.sevenb.terminal.R
import io.sevenb.terminal.data.model.broker_api.GoalType
import io.sevenb.terminal.data.model.broker_api.Side
import io.sevenb.terminal.domain.usecase.charts.AccountData
import io.sevenb.terminal.ui.base.BaseFragment
import io.sevenb.terminal.ui.extension.viewModelProvider
import io.sevenb.terminal.ui.screens.alert.viewModel.PercentViewModel
import io.sevenb.terminal.ui.screens.alert.viewModel.PriceViewModel
import io.sevenb.terminal.ui.screens.auth.change_password.ChangePasswordViewModel
import kotlinx.android.synthetic.main.fragment_fiat.*
import kotlinx.android.synthetic.main.fragment_percent.*
import kotlinx.android.synthetic.main.fragment_price.*
import kotlinx.android.synthetic.main.fragment_price.container_fragment_price
import kotlinx.android.synthetic.main.fragment_price.course
import kotlinx.android.synthetic.main.fragment_price.currency
import kotlinx.android.synthetic.main.fragment_price.email_switch
import kotlinx.android.synthetic.main.fragment_price.progress_bar
import kotlinx.android.synthetic.main.fragment_price.push_switch

class PriceFragment() : BaseFragment() {
    private lateinit var percentViewModel: PriceViewModel

    override fun layoutId() = R.layout.fragment_price
    override fun onSearchTapListener() {

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        percentViewModel = viewModelProvider(viewModelFactory)

        course.text = "≈ ${(parentFragment as PriceAlertFragment).price}"

        currency.text = "${(parentFragment as PriceAlertFragment).name}/${AccountData.getBaseCurrencyTicker()}"

        percentViewModel.error.observe(viewLifecycleOwner,{
            progress_bar.visibility = View.GONE
            swipeToBottom()

            showSnackbar(it)
        })

        percentViewModel.progress.observe(viewLifecycleOwner, {
            if(it == "false") {
                swipeToBottom()
                progress_bar.visibility = View.GONE
                showSnackbar("Alert saved")
                (parentFragment as PriceAlertFragment).dismiss()
            }
        })

        percentViewModel.progressStart.observe(viewLifecycleOwner,{
            if(it == "true" )
                progress_bar.visibility = View.VISIBLE
        })

        container_fragment_price.setTransitionListener(object :
            MotionLayout.TransitionListener {
            override fun onTransitionStarted(p0: MotionLayout?, p1: Int, p2: Int) {}

            override fun onTransitionChange(p0: MotionLayout?, p1: Int, p2: Int, p3: Float) {}

            override fun onTransitionCompleted(p0: MotionLayout?, p1: Int) {
                if (p0?.endState == p0?.currentState) {
                    percentViewModel = viewModelProvider(viewModelFactory)
                    if(price_above_edit_text.text.toString() != "" && price_above_edit_text.text.toString().replace(",", "") != ""  && price_above_edit_text.text.toString().replace(".", "") != ""  && price_above_edit_text.text.toString()[0] != ',' && price_above_edit_text.text.toString()[0] != '.')
                        percentViewModel.priceAlertCreate((parentFragment as PriceAlertFragment).name!!, AccountData.getBaseCurrencyTicker(), push_switch.isChecked, email_switch.isChecked, price_above_edit_text.text.toString(), Side.RISE, GoalType.VALUE_REACH)
                    if(price_below_edit_text.text.toString() != "" && price_below_edit_text.text.toString().replace(",", "") != ""  && price_below_edit_text.text.toString().replace(".", "") != "" && price_below_edit_text.text.toString()[0] != ',' && price_below_edit_text.text.toString()[0] != '.')
                        percentViewModel.priceAlertCreate((parentFragment as PriceAlertFragment).name!!, AccountData.getBaseCurrencyTicker(), push_switch.isChecked, email_switch.isChecked, price_below_edit_text.text.toString(), Side.FALL, GoalType.VALUE_REACH)
                }
            }

            override fun onTransitionTrigger(p0: MotionLayout?, p1: Int, p2: Boolean, p3: Float) {}
        })
    }

    private fun showSnackbar(message: String) {
        if (!message.contains(AccountData.ERROR_CODE_500))
            Snackbar.make(container_fragment_price.rootView, message, Snackbar.LENGTH_SHORT)
                .show()
    }

    fun swipeToBottom(){
        container_fragment_price.transitionToStart()
    }

    override fun onPause() {
        enableAnimation(true)

        super.onPause()
    }

    private fun enableAnimation(value: Boolean) {
        container_fragment_price.getTransition(R.id.deposit_transition).setEnable(value)
    }

    companion object {
        fun newInstance() = PriceFragment()
    }
}