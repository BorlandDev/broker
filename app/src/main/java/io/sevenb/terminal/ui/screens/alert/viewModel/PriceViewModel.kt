package io.sevenb.terminal.ui.screens.alert.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.sevenb.terminal.data.model.broker_api.CreatePriceAlert
import io.sevenb.terminal.data.model.broker_api.GoalType
import io.sevenb.terminal.data.model.broker_api.PriceAlert
import io.sevenb.terminal.data.model.broker_api.Side
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.portfolio.LocalAccountData
import io.sevenb.terminal.domain.usecase.auth.LocalUserDataStoring
import io.sevenb.terminal.domain.usecase.charts.AccountData
import io.sevenb.terminal.domain.usecase.notification_handler.NotificationHandler
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class PriceViewModel @Inject constructor(
    private val notificationHandler: NotificationHandler,
    private val localUserDataStoring: LocalUserDataStoring
) :
    ViewModel() {

    var progress: MutableLiveData<String> = MutableLiveData<String>()
    var progressStart: MutableLiveData<String> = MutableLiveData<String>()
    var error: MutableLiveData<String> = MutableLiveData<String>()

    fun priceAlertInfo() {
        viewModelScope.launch {
            when (val priceAlertInfoResult = notificationHandler.getPriceAlert()) {
                is Result.Success -> {
//                    lvPriceAlertInfo.value = priceAlertInfoResult.data
                }
                is Result.Error -> {
                    Timber.e("getting accountInfo error")
                }
            }
        }

    }

    fun priceAlertCreate(baseAsset: String, fiatCurrency: String, checkEmail: Boolean, checkPush: Boolean, priceBelow: String, side: Side, goalType: GoalType) {

        viewModelScope.launch {
            progressStart.value = "true"
            when (val priceAlertInfoResult =
                notificationHandler.createPriceAlert(
                    PriceAlert(
                        baseAsset,
                        localUserDataStoring.localAccountData.email,
                        fiatCurrency,
                        priceBelow,
                        goalType,
                        checkEmail,
                        checkPush,
                        side
                    )
                )) {
                is Result.Success -> {
                    progress.value = "false"
                }
                is Result.Error -> {
                    error.value = "error save"
                    Timber.e("getting accountInfo error")
                }
            }
        }

    }
}