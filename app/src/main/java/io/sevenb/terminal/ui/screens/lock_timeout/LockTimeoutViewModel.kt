package io.sevenb.terminal.ui.screens.lock_timeout

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.domain.usecase.safety.AccountSafetyStoring
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import javax.inject.Inject

class LockTimeoutViewModel @Inject constructor(
    private val safetyStoring: AccountSafetyStoring
) : ViewModel() {

    val minuteLock = MutableLiveData<Boolean?>()
    val fiveMinutesLock = MutableLiveData<Boolean?>()
    val halfHourLock = MutableLiveData<Boolean?>()
    val hourLock = MutableLiveData<Boolean?>()
    val rebootLock = MutableLiveData<Boolean?>()

    private var minuteBool = false
    private var fiveMinutesBool = false
    private var halfHourBool = false
    private var hourBool = false
    private var rebootBool = false

    init {
        viewModelScope.launch {
            val minuteAsync = async { safetyStoring.restoreMinuteLock() }
            val fiveMinutesAsync = async { safetyStoring.restoreFiveMinutesLock() }
            val halfHourAsync = async { safetyStoring.restoreHalfHourLock() }
            val hourAsync = async { safetyStoring.restoreHourLock() }
            val rebootAsync = async { safetyStoring.restoreRebootLock() }

            awaitData(
                minuteAsync.await(),
                fiveMinutesAsync.await(),
                halfHourAsync.await(),
                hourAsync.await(),
                rebootAsync.await()
            )
        }
    }

    private fun awaitData(
        minRes: Result<Boolean>,
        fiveMinRes: Result<Boolean>,
        halfRes: Result<Boolean>,
        hourRes: Result<Boolean>,
        rebootRes: Result<Boolean>,
    ) {
        if (minRes is Result.Success &&
            fiveMinRes is Result.Success &&
            halfRes is Result.Success &&
            hourRes is Result.Success &&
            rebootRes is Result.Success
        ) {
            when {
                minRes.data -> setMinuteLock(minRes.data)
                fiveMinRes.data -> setFiveMinutesLock(fiveMinRes.data)
                halfRes.data -> setHalfHourLock(halfRes.data)
                hourRes.data -> setHourLock(hourRes.data)
                rebootRes.data -> setRebootLock(rebootRes.data)
            }
        }
    }

    fun setMinuteLock(value: Boolean?) {
        viewModelScope.launch {
            if (value != null) {
                safetyStoring.removeAllLocks()
                val res = safetyStoring.storeMinuteLock(value)
                if (res is Result.Success) {
                    minuteBool = value
                    resetPickers()
                    minuteLock.value = value
                }
            } else {
                minuteLock.value = value
            }
        }
    }

    fun setFiveMinutesLock(value: Boolean?) {
        viewModelScope.launch {
            if (value != null) {
                safetyStoring.removeAllLocks()
                val res = safetyStoring.storeFiveMinutesLock(value)
                if (res is Result.Success) {
                    fiveMinutesBool = value
                    resetPickers()
                    fiveMinutesLock.value = value
                }
            } else {
                fiveMinutesLock.value = value
            }
        }
    }

    fun setHalfHourLock(value: Boolean?) {
        viewModelScope.launch {
            if (value != null) {
                safetyStoring.removeAllLocks()
                val res = safetyStoring.storeHalfHourLock(value)
                if (res is Result.Success) {
                    halfHourBool = value
                    resetPickers()
                    halfHourLock.value = value
                }
            } else {
                halfHourLock.value = value
            }
        }
    }

    fun setHourLock(value: Boolean?) {
        viewModelScope.launch {
            if (value != null) {
                safetyStoring.removeAllLocks()
                val res = safetyStoring.storeHourLock(value)
                if (res is Result.Success) {
                    hourBool = value
                    resetPickers()
                    hourLock.value = value
                }
            } else {
                hourLock.value = value
            }
        }
    }

    fun setRebootLock(value: Boolean?) {
        viewModelScope.launch {
            if (value != null) {
                safetyStoring.removeAllLocks()
                val res = safetyStoring.storeRebootLock(value)
                if (res is Result.Success) {
                    rebootBool = value
                    resetPickers()
                    rebootLock.value = value
                }
            } else {
                rebootLock.value = value
            }
        }
    }

    private fun resetPickers() {
        selectTimeoutState()
        minuteLock.value = false
        fiveMinutesLock.value = false
        halfHourLock.value = false
        hourLock.value = false
        rebootLock.value = false

        minuteBool = false
        fiveMinutesBool = false
        halfHourBool = false
        hourBool = false
        rebootBool = false
    }

    private fun selectTimeoutState() {
        when {
            minuteBool || fiveMinutesBool || halfHourBool || hourBool || rebootBool -> storeTimeoutEnabled()
            else -> storeTimeoutDisabled()
        }
    }

    private fun storeTimeoutEnabled() {
        viewModelScope.launch { safetyStoring.storeTimeoutEnabled(true) }
    }

    private fun storeTimeoutDisabled() {
        viewModelScope.launch { safetyStoring.storeTimeoutEnabled(false) }
    }

}