package io.sevenb.terminal.ui.screens.select_currency

interface BaseCurrencyItem {
    var ticker: String
}