package io.sevenb.terminal.ui.screens.trading

import com.github.mikephil.charting.data.Entry
import io.sevenb.terminal.data.model.trading.TradeChartRange
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ConflatedBroadcastChannel


/**
 * Created by samosudovd on 07/12/2020.
 */

class FavoriteTradeItem(
    name: String = "",
    ticker: String = "",
    cmcIconId: Int = 0,
    price: Float? = null,
    change: Float = 0.0f,
    isSellAvailable: Boolean = false,
    chartEntries: List<Entry>? = null,
    expandedChartEntries: List<Entry>? = null,
    override var network: String? = "",
    lastChartUpdate: Long? = null,
    override val updateTimeChannel: ConflatedBroadcastChannel<Pair<String, Long>>,
    override val updateChartRangeChannel: ConflatedBroadcastChannel<Pair<String, TradeChartRange>>,
    override var currentRange: TradeChartRange?,
    override var isExpanded: Boolean?,
    override var updateExpandedStateChannel: Channel<Pair<String, Boolean>>?
) : TradingWalletItem(
    name,
    ticker,
    cmcIconId,
    price,
    change,
    isSellAvailable,
    chartEntries,
    expandedChartEntries,
    lastChartUpdate = lastChartUpdate,
    updateTimeChannel = updateTimeChannel,
    updateChartRangeChannel = updateChartRangeChannel,
    currentRange = currentRange,
    isExpanded = isExpanded,
    updateExpandedStateChannel = updateExpandedStateChannel
)
