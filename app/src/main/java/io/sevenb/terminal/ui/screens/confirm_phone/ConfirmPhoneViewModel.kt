package io.sevenb.terminal.ui.screens.confirm_phone

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.sevenb.terminal.data.model.broker_api.ConfirmPhoneResponse
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.repository.auth.UserAuthRepository
import io.sevenb.terminal.device.utils.DateTimeUtil
import io.sevenb.terminal.domain.usecase.auth.ResendConfirmationCode
import io.sevenb.terminal.domain.usecase.auth.StartResendTimer
import io.sevenb.terminal.domain.usecase.auth.StoreRefreshToken
import io.sevenb.terminal.domain.usecase.auth.UpdateRefreshToken
import io.sevenb.terminal.domain.usecase.settings.ConfirmPhone
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class ConfirmPhoneViewModel @Inject constructor(
    private val confirmPhone: ConfirmPhone,
    private val startResendTimer: StartResendTimer,
    private val resendConfirmationCode: ResendConfirmationCode,
    private val updateRefreshToken: UpdateRefreshToken,
    private val storeStoreRefreshToken: StoreRefreshToken
) : ViewModel() {

    val phoneConfirmed = MutableLiveData<Boolean>()
    val timerValue = MutableLiveData<String>()
    val showResendButton = MutableLiveData<Boolean>()
    val snackbarMessage = MutableLiveData<String>()

    private lateinit var confirmPhoneResponse: ConfirmPhoneResponse

    init {
        subscribeUi()
    }

    private fun subscribeUi() {
        viewModelScope.launch {
            startResendTimer.timerValueFlow.collect {
                timerValue.value = DateTimeUtil.formatSeconds(it.toLong())
                if (it == 0) showResendButton.value = true
            }
        }
    }

    fun requestConfirmPhoneCode() {
        viewModelScope.launch {
            when (val confirmPhoneCodeResult = confirmPhone.requestConfirmPhoneCode()) {
                is Result.Success -> {
                    confirmPhoneResponse = confirmPhoneCodeResult.data

                    startResendTimer.execute(this)
                    showResendButton.value = false
                    Timber.d("requestConfirmPhoneCode: confirmation code requested")
                }
                is Result.Error -> snackbarMessage.value = "Confirmation code error: 0"
            }
        }
    }

    fun resendConfirmationCode() {
        viewModelScope.launch {
            if (::confirmPhoneResponse.isInitialized) {
                when (val result = resendConfirmationCode.execute(confirmPhoneResponse.token)) {
                    is Result.Success -> {
                        confirmPhoneResponse.token = result.data.token
                        startResendTimer.execute(this)
                        showResendButton.value = false
                    }
                    is Result.Error -> {
                        snackbarMessage.value = "Confirmation code error: 2"
                    }
                }
            }
        }
    }

    fun confirmPhone(confirmationCode: String) {
        viewModelScope.launch {
            if (confirmationCode.isEmpty()) {
                snackbarMessage.value = "Please fill in all fields."
                return@launch
            }

            if (::confirmPhoneResponse.isInitialized) {
                when (confirmPhone.confirmPhoneWithCode(confirmPhoneResponse.token, confirmationCode)) {
                    is Result.Success -> {
                        updateRefreshToken()
                    }
                    is Result.Error -> snackbarMessage.value = "Confirmation code error: 1"
                }
            } else {
                requestConfirmPhoneCode()
            }
        }
    }

    private fun updateRefreshToken() {
        viewModelScope.launch {
            val refreshToken = UserAuthRepository.refreshToken
            when (val updateRefreshTokenResult = updateRefreshToken.execute(refreshToken)) {
                is Result.Success -> {
                    UserAuthRepository.accessToken = updateRefreshTokenResult.data.accessToken
                    UserAuthRepository.refreshToken = updateRefreshTokenResult.data.refreshToken

                    storeStoreRefreshToken.execute(UserAuthRepository.refreshToken)

                    phoneConfirmed.value = true
                }
                is Result.Error -> snackbarMessage.value = "Confirmation code error: 3"
            }
        }
    }

}