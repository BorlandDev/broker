package io.sevenb.terminal.ui.extension

import android.graphics.drawable.Drawable
import android.widget.ImageView
import coil.Coil
import coil.ImageLoader
import coil.imageLoader
import coil.loadAny
import coil.request.Disposable
import coil.request.ImageRequest

//
//inline fun ImageView.load(
//    any: Any?,
//    imageLoader: ImageLoader = Coil.imageLoader(this.context),
//    builder: LoadRequestBuilder.() -> Unit = {}
//): RequestDisposable {
//    return if (any is Drawable) {
//        imageLoader.load(context, any) {
//            target(this@load)
//            builder()
//        }
//    } else {
//        imageLoader.load(context, any as String) {
//            target(this@load)
//            builder()
//        }
//    }
//}

@JvmSynthetic
inline fun ImageView.load(
    any: Any?,
    imageLoader: ImageLoader = context.imageLoader,
    builder: ImageRequest.Builder.() -> Unit = {}
): Disposable {
    return loadAny(any, imageLoader, builder)
}