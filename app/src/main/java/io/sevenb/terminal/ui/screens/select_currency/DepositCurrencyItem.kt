package io.sevenb.terminal.ui.screens.select_currency

import android.view.View
import coil.load
import io.sevenb.terminal.R
import io.sevenb.terminal.data.model.trading.TradeChartRange
import io.sevenb.terminal.ui.screens.trading.TradingWalletItem.Companion.CMC_ICON_URL
import kotlinx.android.synthetic.main.item_select_list.view.*
import kotlinx.coroutines.channels.Channel


/**
 * Created by samosudovd on 07/06/2018.
 */

class DepositCurrencyItem(
    var name: String = "",
    override var ticker: String = "",
    private val price: Float = 0.0f,
    private val change: Float = 0.0f,
    override var network: String?,
    val cmcIconId: Int = 0,
    override var isExpanded: Boolean?,
    override var updateExpandedStateChannel: Channel<Pair<String, Boolean>>?,
    override var currentRange: TradeChartRange?,
    override var estimated: String = "",
    override var amount: String? = ""
) : BaseSelectItem {

    /**
     * Bind method by common search list adapter
     * @see io.sevenb.terminal.ui.screens.select_currency.SelectCurrencyAdapter
     */
    override fun bind(
        itemView: View,
        baseCurrency: String,
        itemSelected: (BaseSelectItem) -> Unit
    ) {

        if(ticker == "USD" || ticker == "EUR" || ticker == "GBP" || ticker == "AUD" || ticker == "CAD" || ticker == "RUB" || ticker == "INR"){
            val image = when(cmcIconId){
                11111 -> R.drawable.ic_united_states_of_america_svgrepo_com
                11112 -> R.drawable.ic_european_union_svgrepo_com
                11113 -> R.drawable.ic_united_kingdom_svgrepo_com
                11114 -> R.drawable.ic_australia_svgrepo_com
                11115 -> R.drawable.ic_canada_svgrepo_com
                11116 -> R.drawable.ic_russia_svgrepo_com
                else -> R.drawable.ic_india_svgrepo_com
            }
            itemView.img_icon.setImageDrawable(itemView.context.getDrawable(image))
        }
        else if (cmcIconId == 7686) {
            itemView.img_icon.load(R.drawable.ic_e_cash_logo) {
                placeholder(R.drawable.ic_coin_placeholder)
                error(R.drawable.ic_coin_placeholder)
            }
        } else itemView.img_icon.load(CMC_ICON_URL.format(cmcIconId)) {
            placeholder(R.drawable.ic_coin_placeholder)
            error(R.drawable.ic_coin_placeholder)
        }

        ticker = if (ticker == "BCHA") "XEC" else ticker
        name = if (name == "Bitcoin Cash ABC") "eCash" else name
        network = if (network == "BCHA") "XEC" else network

        itemView.tv_ticker.text = ticker
        itemView.tv_name.text = if (ticker == network) {
            name
        } else if(ticker == "USD" || ticker == "EUR" || ticker == "GBP" || ticker == "AUD" || ticker == "CAD" || ticker == "RUB" || ticker == "INR"){
            network
        }else "%s (%s %s)".format(name, network, "network")
        itemView.tv_estimated.text = "%s %s".format(price.toString(), baseCurrency)
        itemView.setOnClickListener { itemSelected(this) }
    }

}
