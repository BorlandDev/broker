//package io.sevenb.terminal.ui.screens.auth
//
//import androidx.biometric.BiometricManager
//import androidx.lifecycle.MutableLiveData
//import androidx.lifecycle.ViewModel
//import androidx.lifecycle.viewModelScope
//import com.google.gson.Gson
//import io.sevenb.terminal.data.model.auth.UserCaptcha
//import io.sevenb.terminal.data.model.auth.UserData
//import io.sevenb.terminal.data.model.broker_api.RequestTokenResponse
//import io.sevenb.terminal.data.model.core.Result
//import io.sevenb.terminal.data.model.enum_model.ErrorMessageType
//import io.sevenb.terminal.data.model.enums.AuthState
//import io.sevenb.terminal.data.model.enums.GeeTestResult
//import io.sevenb.terminal.data.repository.auth.UserAuthRepository
//import io.sevenb.terminal.device.utils.DateTimeUtil.formatSeconds
//import io.sevenb.terminal.domain.usecase.auth.*
//import io.sevenb.terminal.utils.ConnectionStateMonitor
//import kotlinx.coroutines.Dispatchers
//import kotlinx.coroutines.delay
//import kotlinx.coroutines.flow.collect
//import kotlinx.coroutines.launch
//import kotlinx.coroutines.withContext
//import org.json.JSONException
//import org.json.JSONObject
//import timber.log.Timber
//import java.io.File
//import java.util.regex.Pattern
//import javax.crypto.Cipher
//import javax.inject.Inject
//
//class AuthViewModel @Inject constructor(
//    private val requestSmsCode: RequestSmsCode,
//    private val startResendTimer: StartResendTimer,
//    private val sendConfirmationCode: SendConfirmationCode,
//    private val resendConfirmationCode: ResendConfirmationCode,
//    private val setNewPassword: SetNewPassword,
//    private val signInUser: SignInUser,
//    private val restoreRefreshToken: RestoreRefreshToken,
//    private val storeRefreshToken: StoreRefreshToken,
//    private val fingerPrintRestore: FingerPrintRestore,
//    private val storePassword: StorePassword,
//    private val captchaDataInterop: CaptchaDataInterop,
//    private val logOutUser: LogOutUser,
//    private val localUserDataStoring: LocalUserDataStoring,
//    private val parseErrorMessage: ParseErrorMessage,
//    private val validateEmailPassword: ValidateEmailPassword,
//    private val storeFirstLaunch: StoreFirstLaunch
//) : ViewModel() {
//
//    val authState = MutableLiveData<AuthState>().apply { value = AuthState.NEW }
//    val authDone = MutableLiveData<Boolean>()
//    val popupLocalAccountExists = MutableLiveData<Boolean>()
//    val startCaptchaFlow = MutableLiveData<Boolean>()
//    val timerValue = MutableLiveData<String>()
//    val showResendButton = MutableLiveData<Boolean>()
//    val snackbarMessage = MutableLiveData<ErrorMessageType>()
//    val customErrorMessage = MutableLiveData<String>()
//    val showCaptcha = MutableLiveData<JSONObject?>()
//    val geeTestResult = MutableLiveData<GeeTestResult>()
//    val userExistsResult = MutableLiveData<Boolean>()
//    val creatingBiometricCipher = MutableLiveData<Cipher?>()
//    val decryptBiometricCipher = MutableLiveData<Cipher>()
//    val allLocalAccountDataRemoved = MutableLiveData<Boolean>()
//    val sendMessageScreen = MutableLiveData<Unit>()
//    val restoredEmail = MutableLiveData<String>()
//    val isFirstLaunch = MutableLiveData<Boolean>()
//    var currentState: AuthState = AuthState.NEW
//    val focusEmail = MutableLiveData<Unit>()
//
//    var userData = UserData()
//    private lateinit var requestToken: RequestTokenResponse
//    private lateinit var restorePasswordToken: RequestTokenResponse
//    private lateinit var restoreConfirmedToken: String
//
//    var currentStateBackStack = MutableLiveData<AuthState>()
//
//    private var showTimeError = 0L
//
//    private lateinit var savedEmail: String
//    var tempState = AuthState.NEW
//    private var tempPassword: String? = null
//
//    init {
//        userExistence()
//        restoreLocalAccountData()
//
//        subscribeUi()
//    }
//
//    private fun userExistence() {
//        viewModelScope.launch {
//            when (val result = restoreRefreshToken.execute()) {
//                is Result.Success -> {
//                    if (result.data.isNotEmpty()) {
//                        authState.value = AuthState.SIGN_IN
//                        currentStateBackStack.value = AuthState.SIGN_IN
//                    } else {
//                        if (localUserDataStoring.localAccountData.email.isNotEmpty()) {
//                            authState.value = AuthState.SIGN_IN
//                            currentStateBackStack.value = AuthState.SIGN_IN
//                        } else {
//                            authState.value = AuthState.NEW
//                            currentStateBackStack.value = AuthState.NEW
//                        }
//                    }
//                }
//                else -> Timber.e("userExistence error")
//            }
//        }
//    }
//
//    fun saveEmail(email: String) {
//        savedEmail = email
//    }
//
//    fun getSavedEmail() = if (::savedEmail.isInitialized) savedEmail else ""
//
//    private fun showNoInternetConnection() {
//        val currentTime = System.currentTimeMillis()
//        if (currentTime - showTimeError >= 1500) {
//            customErrorMessage.value = ConnectionStateMonitor.noInternetError
//            showTimeError = currentTime
//        }
//    }
//
//    private fun restoreLocalAccountData() {
//        viewModelScope.launch {
//            when (val result = localUserDataStoring.restoreLocalAccountData()) {
//                is Result.Success -> {
//                    val localAccountData = result.data
//                    userData.email = localAccountData.email
//                    restoredEmail.value = localAccountData.email
//                }
//            }
//        }
//    }
//
//    private fun subscribeUi() {
//        viewModelScope.launch {
//            startResendTimer.timerValueFlow.collect {
//                timerValue.value = formatSeconds(it.toLong())
//                if (it == 0) showResendButton.value = true
//            }
//        }
//    }
//
//    fun getBiometricAuthenticateAbility() = storePassword.getBiometricAuthenticateAbility()
//
//    fun isBiometricSuccess() =
//        getBiometricAuthenticateAbility() == BiometricManager.BIOMETRIC_SUCCESS
//
//    fun isFingerPrintExist() {
//        viewModelScope.launch {
//            val cipher = fingerPrintRestore.provideDecryptionCipher()
//            if (cipher == null) {
//                focusEmail.value = Unit
//                return@launch
//            }
//            try {
//                decryptBiometricCipher.value = cipher
//            } catch (e: IllegalStateException) {
//            }
//        }
//    }
//
//    fun decryptPassword(cipher: Cipher?) {
//        viewModelScope.launch {
//            val password = fingerPrintRestore.decryptPassword(cipher ?: return@launch)
//            if (password != null) {
//                if (password.isNotEmpty())
//                    signInUser(userData.email, password)
//                else {
//                    localUserDataStoring.clearBiometry()
//                    storeFirstLaunch.setFirstLaunch(true)
//                    customErrorMessage.value =
//                        "You may have changed your fingerprint. Current fingerprint in this app will be removed"
//                }
//            } else {
//                customErrorMessage.value = "Login error: code 01"
//            }
//        }
//    }
//
//    fun createFingerprint() {
//        val cipherPair = storePassword.provideEncryptionCipher()
//
//        if (cipherPair.first != null && !isBiometricSuccess()) {
//            customErrorMessage.value = cipherPair.first
//        }
//        creatingBiometricCipher.value = cipherPair.second
//    }
//
//    fun encryptPassword(cipher: Cipher?, password: String = "") {
//        viewModelScope.launch {
//            if (password.isNotEmpty()) tempPassword = password
//            val encryptionResult = storePassword.encryptPassword(
//                if (tempState == AuthState.SIGN_IN) password else userData.password,
//                cipher ?: return@launch
//            )
//            when (encryptionResult) {
//                is Result.Success -> {
//                    if (isFirstLaunch.value == true) {
//                        if (authState.value == AuthState.SMS_CODE)
//                            signInAfterAccountCreated()
//                        else storeFirstLaunch()
//
//                    } else signInAfterAccountCreated()
//                    customErrorMessage.value = "Fingerprint created"
//                }
//                is Result.Error -> customErrorMessage.value = "Fingerprint creating error"
//            }
//        }
//    }
//
//    suspend fun restoreEncryptedPassword(): Result<String> {
//        return storePassword.restoreEncryptedPassword()
//    }
//
//    fun encryptPasswordLoginState(password: String, cipher: Cipher?) {
//        viewModelScope.launch {
//            val encryptionResult = storePassword.encryptPassword(
//                password,
//                cipher ?: return@launch
//            )
//            when (encryptionResult) {
//                is Result.Success -> {
//                    if (isFirstLaunch.value == true) {
//                        storeFirstLaunch()
//                        authDone.value = true
//                    }
//                    customErrorMessage.value = "Fingerprint created"
//                }
//                is Result.Error -> customErrorMessage.value = "Fingerprint creating error"
//            }
//        }
//    }
//
//    fun validateEmailPassword(email: String, password: String) {
//        viewModelScope.launch {
//            if (isLocalAccountExists()) {
//                popupLocalAccountExists.value = true
//                return@launch
//            }
//
//            if (!validateEmailPassword.validateEmail(email)) {
//                snackbarMessage.value = ErrorMessageType.EMAIL_INCORRECT
//                return@launch
//            }
//
//            if (!validateEmailPassword.validatePassword(password)) {
//                snackbarMessage.value = ErrorMessageType.PASSWORD_INCORRECT
//                return@launch
//            }
//
//            userData.email = email
//            userData.password = password
//            startCaptchaFlow.value = true
//        }
//    }
//
//    fun captcha(param: Int, result: String) {
//        if (param == 0) {
//            viewModelScope.launch {
//                when (val resultJsonObject = captchaDataInterop.getCaptchaData()) {
//                    is Result.Success -> {
//                        var jsonObject: JSONObject? = null
//                        try {
//                            jsonObject = JSONObject(Gson().toJson(resultJsonObject.data))
//                        } catch (e: JSONException) {
//                            e.printStackTrace()
//                            Timber.e("captchaDataInterop.getCaptchaData() parsing error")
//                        }
//
//                        showCaptcha.value = jsonObject
//                    }
//                    is Result.Error -> {
//                        when {
//                            !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
//                            else -> customErrorMessage.value = "Server error: code 3"
//                        }
//                    }
//                }
//            }
//        } else if (param == 1) {
//            viewModelScope.launch {
//                try {
//                    val jsonObject = JSONObject(result)
//                    val challenge = jsonObject.getString(CaptchaDataInterop.GEETEST_CHALLENGE)
//                    val seccode = jsonObject.getString(CaptchaDataInterop.GEETEST_SECCODE)
//                    val validate = jsonObject.getString(CaptchaDataInterop.GEETEST_VALIDATE)
//
//                    userData.captcha = UserCaptcha(challenge, seccode, validate)
//                    if (authState.value == AuthState.ACCOUNT_DETAILS) {
//                        geeTestResult.value = GeeTestResult.SUCCESS
//                        smsCodeRestorePassword()
//                    } else {
//                        smsCodeRequest()
//                    }
//                } catch (e: JSONException) {
//                    customErrorMessage.value = e.message
//                    Timber.e("postCaptchaData: error - ${e.message}")
//                }
//            }
//        }
//    }
//
//    private fun smsCodeRestorePassword() {
//        viewModelScope.launch {
//            startResendTimer.execute(this)
//            showResendButton.value = false
//            when (val result = requestSmsCode.executeRestorePassword(userData.email)) {
//                is Result.Success -> {
//                    restorePasswordToken = result.data
//                    authState.value = AuthState.SMS_CODE
//                    currentStateBackStack.value = AuthState.SMS_CODE
//                }
//                is Result.Error -> {
//                    when {
//                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
//                        result.message.contains("user_exists") -> {
//                            geeTestResult.value = GeeTestResult.USER_EXISTS
//                            snackbarMessage.value = ErrorMessageType.USER_ALREADY_EXISTS
////                            userExistsResult.value = true
//                        }
//                        result.message.contains("bad_params") -> {
//                            geeTestResult.value = GeeTestResult.USER_EXISTS
//                            val errorMessage = parseErrorMessage.execute(result.message)
//                            customErrorMessage.value = errorMessage?.message
//                        }
//                        else -> {
//                            geeTestResult.value = GeeTestResult.FAIL
//                            customErrorMessage.value = "Server error: code 2"
//                        }
//                    }
//                }
//            }
//        }
//    }
//
//    private fun smsCodeRequest() {
//        viewModelScope.launch {
//            startResendTimer.execute(this)
//
//            when (val result = requestSmsCode.execute(userData)) {
//                is Result.Success -> {
//                    geeTestResult.value = GeeTestResult.SUCCESS
//                    requestToken = result.data
//                    authState.value = AuthState.SMS_CODE
//                    currentStateBackStack.value = AuthState.SMS_CODE_RESTORE_PASSWORD
//                }
//                is Result.Error -> {
//                    when {
//                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
//                        result.message.contains("user_exists") -> {
//                            geeTestResult.value = GeeTestResult.USER_EXISTS
//                            snackbarMessage.value = ErrorMessageType.USER_ALREADY_EXISTS
//                            userExistsResult.value = true
//                        }
//                        result.message.contains("bad_params") -> {
//                            geeTestResult.value = GeeTestResult.USER_EXISTS
//                            val errorMessage = parseErrorMessage.execute(result.message)
//                            customErrorMessage.value = errorMessage?.error
//                        }
//                        result.message.contains("unconfirmed_user") -> {
//                            geeTestResult.value = GeeTestResult.USER_EXISTS
//                            snackbarMessage.value = ErrorMessageType.USER_ALREADY_EXISTS
//                            userExistsResult.value = true
//                        }
//                        else -> {
//                            geeTestResult.value = GeeTestResult.FAIL
//                            customErrorMessage.value = "Server error: code 2"
//                        }
//                    }
//                }
//            }
//        }
//    }
//
//    private fun resendConfirmationCode() {
//        val token = if (::requestToken.isInitialized) requestToken.token
//        else restorePasswordToken.token
//
//        val email = if (::savedEmail.isInitialized) savedEmail else ""
//        viewModelScope.launch {
//            when (val result = resendConfirmationCode.execute(token, email)) {
//                is Result.Success -> {
//                    requestToken = result.data
//                    startResendTimer.execute(this)
//                    showResendButton.value = false
//                }
//                is Result.Error -> {
//                    when {
//                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
//                        else -> customErrorMessage.value = "Server error: code 4"
//                    }
//                }
//            }
//        }
//    }
//
//    fun resendCodeNewLogic() {
//        if (::requestToken.isInitialized)
//            resendConfirmationCode()
//        else if (::restorePasswordToken.isInitialized)
//            smsCodeRestorePassword()
//    }
//
//    fun confirmationCode(
//        confirmationCode: String
//    ) {
//        if (::requestToken.isInitialized)
//            registrationConfirmationCode(confirmationCode)
//
//        if (::restorePasswordToken.isInitialized)
//            restoreConfirmationCode(confirmationCode)
//    }
//
//    private fun restoreConfirmationCode(confirmationCode: String) {
//        viewModelScope.launch {
//            when (val responseToken = sendConfirmationCode.executeVerifyCode(
//                userData.email,
//                confirmationCode,
//                restorePasswordToken.token
//            )) {
//                is Result.Success -> {
//                    localUserDataStoring.updateLocalAccountData(
//                        userData.email
//                    )
//                    restoreConfirmedToken = responseToken.data.token
//                    authState.value = AuthState.CREATING_PASSWORD
//                }
//                is Result.Error -> {
//                    when {
//                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
//                        else -> customErrorMessage.value = "Incorrect confirmation code"
//                    }
//                }
//            }
//        }
//    }
//
//    private fun createNewPassword() {
//        viewModelScope.launch {
//            if (::restoreConfirmedToken.isInitialized)
//                when (val setNewPassword =
//                    setNewPassword.execute(
//                        userData.email,
//                        userData.password,
//                        restoreConfirmedToken
//                    )) {
//                    is Result.Success -> {
//                        if (setNewPassword.data.result)
//                            createFingerprint()
//                    }
//                    is Result.Error -> {
//                        when {
//                            !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
//                            else -> customErrorMessage.value = "Server error: code 1"
//                        }
//                    }
//                }
//        }
//    }
//
//    private fun registrationConfirmationCode(confirmationCode: String) {
//        viewModelScope.launch {
//            when (sendConfirmationCode.execute(requestToken.token, confirmationCode)) {
//                is Result.Success -> {
//                    localUserDataStoring.updateLocalAccountData(userData.email)
//                    createFingerprint()
//                }
//                is Result.Error -> {
//                    when {
//                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
//                        else -> customErrorMessage.value = "Incorrect code"
//                    }
//                }
//            }
//        }
//    }
//
//    fun signInUser(email: String, password: String) {
//        viewModelScope.launch {
//            if (email.isEmpty() && password.isEmpty()) {
//                snackbarMessage.value = ErrorMessageType.EMPTY_FIELDS
//                return@launch
//            } else if (email.isEmpty()) {
//                snackbarMessage.value = ErrorMessageType.LOGIN_EMPTY_EMAIL
//                return@launch
//            } else if (password.isEmpty()) {
//                snackbarMessage.value = ErrorMessageType.LOGIN_EMPTY_PASSWORD
//                return@launch
//            }
//
//            if (!checkValidEmail(email)) {
//                snackbarMessage.value = ErrorMessageType.EMAIL_INCORRECT
//                return@launch
//            }
//
//            if (!checkValidPassword(password)) {
//                snackbarMessage.value = ErrorMessageType.PASSWORD_INCORRECT
//                return@launch
//            }
//
//            if (isItAnotherAccount(email)) {
//                popupLocalAccountExists.value = true
//                return@launch
//            }
//
//            when (val signInResult = signInUser.execute(email, password)) {
//                is Result.Success -> {
//                    UserAuthRepository.accessToken = signInResult.data.accessToken
//                    UserAuthRepository.refreshToken = signInResult.data.refreshToken
//
//                    localUserDataStoring.updateLocalAccountData(email)
//
//                    storeRefreshToken(signInResult.data.refreshToken)
//                }
//                is Result.Error -> {
//                    when {
//                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
//                        signInResult.message.contains("Incorrect login params") -> {
//                            snackbarMessage.value = ErrorMessageType.LOGIN_ERROR
//                        }
//                        signInResult.message.contains("unconfirmed_user") -> {
//                            val errorMessage = parseErrorMessage.exec(signInResult.message)
//                            errorMessage?.extra?.let {
//                                requestToken = RequestTokenResponse(it.accessToken)
//                                savedEmail = email
//                                resendConfirmationCode()
//                                authState.value = AuthState.SMS_CODE
//                            }
//                            snackbarMessage.value = ErrorMessageType.CONFIRMATION_REQUIRED
//                        }
//                        else -> customErrorMessage.value = "Server error: code 0"
//                    }
//                }
//            }
//        }
//    }
//
//    fun validateEmail(email: String) {
//        if (!validateEmailPassword.validateEmail(email)) {
//            snackbarMessage.value = ErrorMessageType.EMAIL_INCORRECT
//            return
//        }
//
//        startCaptchaFlow.value = true
//        userData.email = email
//    }
//
//    fun validatePassword(password: String) {
//        if (!validateEmailPassword.validatePassword(password)) {
//            snackbarMessage.value = ErrorMessageType.PASSWORD_INCORRECT
//            return
//        }
//
//        userData.password = password
//        createNewPassword()
//    }
//
//    /**
//     * So an account creating takes 2-3 seconds on a server, we need to show
//     * special progress screen in this case
//     */
//    fun signInAfterAccountCreated() {
//        viewModelScope.launch {
//            withContext(Dispatchers.IO) {
//
//                try {
//                    fingerPrintRestore.deleteStoredPassword()
//                } catch (e: Exception) {
//                }
//
//                delay(3 * 1000L)
//                if (tempState == AuthState.SIGN_IN) {
//                    tempPassword?.let {
//                        signInUser(savedEmail, it)
//                    }
//                } else
//                    signInUser(userData.email, userData.password)
//            }
//        }
//    }
//
//    private fun isLocalAccountExists() = userData.email.isNotEmpty()
//    private fun isItAnotherAccount(signInEmail: String): Boolean {
//        if (userData.email.isEmpty()) return false
//        return userData.email != signInEmail
//    }
//
//    private fun storeRefreshToken(refreshToken: String) {
//        viewModelScope.launch {
//            when (val storeResult = storeRefreshToken.execute(refreshToken)) {
//                is Result.Success -> {
//                    val cipher =
//                        if (isBiometricSuccess()) fingerPrintRestore.provideDecryptionCipher()
//                        else null
//
//                    if (currentState == AuthState.SIGN_IN && cipher == null) {
//                        if (isFirstLaunch.value != false) restoreFirstLaunch()
//                        else authDone.value = true
//                    } else authDone.value = true
//                }
//                is Result.Error -> {
//                    when {
//                        !ConnectionStateMonitor.connectionEnabled -> showNoInternetConnection()
//                        else -> customErrorMessage.value = storeResult.exception.message
//                    }
//                }
//            }
//        }
//    }
//
//    fun clearAllLocalAccountData(avatarFilePath: String) {
//        viewModelScope.launch {
//            val logOutUserResult = logOutUser.execute()
//            val localDataResult = localUserDataStoring.deleteAllLocalUserData()
//
//            val avatarFile: File? = File(avatarFilePath)
//            avatarFile?.delete()
//
//            userData = UserData()
//
//            if (logOutUserResult is Result.Success && localDataResult is Result.Success) {
//                allLocalAccountDataRemoved.value = true
//            } else {
//                customErrorMessage.value = "Clear data error: 0"
//            }
//        }
//    }
//
//    fun checkState() {
//        if (::requestToken.isInitialized) {
//            authState.value = AuthState.NEW
//            currentStateBackStack.value = AuthState.NEW
//        } else {
//            authState.value = AuthState.ACCOUNT_DETAILS
//            currentStateBackStack.value = AuthState.ACCOUNT_DETAILS
//        }
//    }
//
//    private fun storeFirstLaunch() {
//        viewModelScope.launch {
//            storeFirstLaunch.setFirstLaunch()
//        }
//    }
//
//    private fun restoreFirstLaunch() {
//        viewModelScope.launch {
//            val result = storeFirstLaunch.getFirstLaunch()
//            if (result is Result.Success) {
//                isFirstLaunch.value = result.data
//            }
//        }
//    }
//
//    fun checkValidEmail(email: String) = validateEmailPassword.validateEmail(email)
//
//    fun checkValidPassword(password: String) = validateEmailPassword.validatePassword(password)
//
//    fun testDeleteFingerprint() {
//        viewModelScope.launch {
//            fingerPrintRestore.deleteStoredPassword()
//        }
//    }
//
//    fun stringContainsNumber(s: String): Boolean {
//        return Pattern.compile("[0-9]").matcher(s).find()
//    }
//
//    fun stringContainsLowercase(s: String): Boolean {
//        return Pattern.compile("(?=.*[a-z])").matcher(s).find()
//    }
//
//    fun stringContainsUppercase(s: String): Boolean {
//        return Pattern.compile("(?=.*[A-Z])").matcher(s).find()
//    }
//
//    override fun onCleared() {
//        currentStateBackStack.value = AuthState.ENTERED
//        super.onCleared()
//    }
//}