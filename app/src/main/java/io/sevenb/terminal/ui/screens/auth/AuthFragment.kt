//package io.sevenb.terminal.ui.screens.auth
//
//import android.os.Bundle
//import android.text.method.HideReturnsTransformationMethod
//import android.text.method.PasswordTransformationMethod
//import android.view.View
//import android.view.WindowManager
//import android.widget.EditText
//import android.widget.ImageView
//import androidx.biometric.BiometricManager
//import androidx.biometric.BiometricManager.BIOMETRIC_SUCCESS
//import androidx.biometric.BiometricPrompt
//import androidx.constraintlayout.widget.ConstraintLayout
//import androidx.core.widget.doOnTextChanged
//import androidx.navigation.fragment.findNavController
//import com.geetest.sdk.GT3ConfigBean
//import com.geetest.sdk.GT3ErrorBean
//import com.geetest.sdk.GT3GeetestUtils
//import com.geetest.sdk.GT3Listener
//import com.geetest.sdk.utils.GT3ServiceNode
//import com.google.android.material.dialog.MaterialAlertDialogBuilder
//import com.google.android.material.snackbar.Snackbar
//import io.sevenb.terminal.AppActivity
//import io.sevenb.terminal.R
//import io.sevenb.terminal.data.model.core.Result
//import io.sevenb.terminal.data.model.enum_model.ErrorMessageType
//import io.sevenb.terminal.data.model.enums.AuthState
//import io.sevenb.terminal.data.model.enums.GeeTestResult
//import io.sevenb.terminal.device.biometric.BiometricPromptUtils
//import io.sevenb.terminal.ui.base.BaseFragment
//import io.sevenb.terminal.ui.extension.*
//import io.sevenb.terminal.ui.screens.settings.SettingsFragment
//import io.sevenb.terminal.ui.screens.trading.AuthStateViewModel
//import kotlinx.android.synthetic.main.fragment_auth.*
//import kotlinx.android.synthetic.main.fragment_auth_login.*
//import kotlinx.android.synthetic.main.fragment_auth_password_hint.*
//import kotlinx.android.synthetic.main.fragment_auth_restore_password.*
//import kotlinx.android.synthetic.main.fragment_auth_sign_up.*
//import kotlinx.android.synthetic.main.fragment_auth_sms_code.*
//import kotlinx.android.synthetic.main.fragment_loader.*
//import kotlinx.coroutines.CoroutineScope
//import kotlinx.coroutines.Dispatchers.Main
//import kotlinx.coroutines.delay
//import kotlinx.coroutines.launch
//import kotlinx.coroutines.runBlocking
//import org.json.JSONObject
//import timber.log.Timber
//import javax.crypto.Cipher
//
//
//class AuthFragment : BaseFragment() {
//    private lateinit var authViewModel: AuthViewModel
//    private lateinit var authStateViewModel: AuthStateViewModel
//
//    private lateinit var gT3GeetestUtils: GT3GeetestUtils
//    private lateinit var gt3ConfigBean: GT3ConfigBean
//
//    private var passVisibility = false
//
//    private var localAuthState: AuthState = AuthState.NEW
//
//    private var isFirstFocus: Boolean = true
//    private var loginIsFocused = false
//    private var restorePassword = false
//
//    override fun layoutId() = R.layout.fragment_auth
//    override fun hasBottomNavigation() = false
//
//    var first = true
//
//    var snackbarCurrentlyShowing = false
//
//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//        authViewModel = viewModelProvider(viewModelFactory)
//        authStateViewModel = viewModelProvider(viewModelFactory)
//
//        initCaptchaConfig()
//
//        initView()
//        subscribeUi()
//    }
//
//    override fun onSearchTapListener() {
//        Timber.d("onSearchTapListener ")
//    }
//
//    private fun initView() {
//        button_send_code.setOnClickListener { createAccount() }
//
//        button_send_code_restore.setOnClickListener {
//            validateEmailOnRestore()
//        }
//
//        sms_code_back_button.visible(true)
//        sms_code_back_button.setOnClickListener {
//            authViewModel.checkState()
//        }
//
//        button_verify_code.setOnClickListener {
//            val confirmationCode = input_sms_code.getString()
//            authViewModel.confirmationCode(confirmationCode)
//        }
//
//        ll_resend_code.setOnClickListener {
//            authViewModel.resendCodeNewLogic()
//        }
//
//        action_change_email.setOnClickListener {
//            when (authStateViewModel.stackState.value) {
//                AuthState.REGISTRATION_STATE -> {
//                    emitToActivity(AuthState.NEW)
//                    authViewModel.authState.value = AuthState.NEW
//                }
//                AuthState.PASSWORD_RESTORE_STATE -> {
//                    emitToActivity(AuthState.ACCOUNT_DETAILS)
//                    authViewModel.authState.value = AuthState.ACCOUNT_DETAILS
//                }
//            }
//        }
//
//        ll_action_have_account_email.setOnClickListener {
//            emitToActivity(AuthState.SIGN_IN)
//            authViewModel.authState.value = AuthState.SIGN_IN
//        }
//
//        ll_action_not_account.setOnClickListener {
//            emitToActivity(AuthState.NEW)
//            authViewModel.authState.value = AuthState.NEW
//        }
//
//        ll_action_forgot_password.setOnClickListener {
//            emitToActivity(AuthState.ACCOUNT_DETAILS)
//            authViewModel.authState.value = AuthState.ACCOUNT_DETAILS
//        }
//
//        button_next_sign_in.setOnClickListener { login() }
//
//        buttton_create_fingerprint.setOnLongClickListener {
//            authViewModel.testDeleteFingerprint()
//            true
//        }
//
//        img_pass_visibility.setOnClickListener {
//            passVisibility = !passVisibility
//            input_password_sign_up.transformationMethod =
//                if (passVisibility) HideReturnsTransformationMethod.getInstance() else
//                    PasswordTransformationMethod.getInstance()
//            input_password_sign_up.text?.let { it1 -> input_password_sign_up.setSelection(it1.length) }
//            changeEyeImage(img_pass_visibility)
//        }
//
//        addTextListenerPass(
//            input_password_sign_up,
//            img_pass_length,
//            img_numbers_contain,
//            img_uppercase,
//            img_lowercase
//        )
//
//        input_password_sign_up.onFocusChangeListener =
//            View.OnFocusChangeListener { _, hasFocus ->
//                if (isFirstFocus) {
//                    isFirstFocus = false
//                    if (hasFocus) {
//                        setNotEnteredImage(img_pass_length)
//                        setNotEnteredImage(img_numbers_contain)
//                        setNotEnteredImage(img_uppercase)
//                        setNotEnteredImage(img_lowercase)
//                    }
//                }
//            }
//
//        input_password_sign_in.onActionDoneListener { login() }
//        input_email_restore.onActionDoneListener { validateEmailOnRestore() }
//        input_password_sign_up.onActionDoneListener { createAccount() }
//        input_sms_code.onActionDoneListener {
//            authViewModel.confirmationCode(input_sms_code.getString())
//        }
//
//        input_email_sign_in.doOnTextChanged { _, _, _, _ ->
//            input_email_sign_in.setDefaultBackground()
//        }
//        input_password_sign_in.doOnTextChanged { _, _, _, _ ->
//            container_password_sign_in.setDefaultBackground()
//        }
//        input_email.doOnTextChanged { _, _, _, _ ->
//            input_email.setDefaultBackground()
//        }
//        input_password_sign_up.doOnTextChanged { _, _, _, _ ->
//            container_password.setDefaultBackground()
//        }
//
//        input_email_restore.doOnTextChanged { _, _, _, _ ->
//            input_email_restore.setDefaultBackground()
//        }
//        input_password_restore.doOnTextChanged { _, _, _, _ ->
//            container_restore_password_input_password.setDefaultBackground()
//        }
//    }
//
//    private fun setEnteredImage(imageView: ImageView) =
//        imageView.newBackground(R.drawable.ic_entered)
//
//    private fun setNotEnteredImage(imageView: ImageView) =
//        imageView.newBackground(R.drawable.ic_not_entered)
//
//    private fun createAccount() {
//        val email = input_email.getString()
//        val password = input_password_sign_up.getString()
//
//        if (email.isEmpty() && password.isEmpty()) {
//            input_email.setErrorBackground()
//            container_password.setErrorBackground()
//            showMessage(ErrorMessageType.EMPTY_FIELDS)
//            return
//        }
//
//        if (!checkEmailField(input_email)) return
//        if (!checkPasswordField(input_password_sign_up, container_password)) return
//
//        authViewModel.saveEmail(email)
//        authViewModel.tempState = AuthState.NEW
//        authViewModel.validateEmailPassword(email, password)
//    }
//
//    private fun login() {
//        showProgressBarSignUp(true)
//        val email = input_email_sign_in.getString()
//        val password = input_password_sign_in.getString()
//
//        authViewModel.saveEmail(email)
//        authViewModel.tempState = AuthState.SIGN_IN
//
//        if (email.isEmpty() && password.isEmpty()) {
//            input_email_sign_in.setErrorBackground()
//            container_password_sign_in.setErrorBackground()
//            showMessage(ErrorMessageType.EMPTY_FIELDS)
//            return
//        }
//
//        if (!checkEmailField(input_email_sign_in)) return
//        if (!checkPasswordField(input_password_sign_in, container_password_sign_in)) return
//
//        authViewModel.signInUser(email, password)
//        hideKeyboard(requireActivity())
//    }
//
//    private fun checkEmailField(view: EditText): Boolean {
//        val email = view.getString()
//        if (email.isEmpty()) {
//            incorrectEmail(view, ErrorMessageType.EMPTY_EMAIL)
//            return false
//        } else if (!authViewModel.checkValidEmail(email)) {
//            incorrectEmail(view, ErrorMessageType.EMAIL_INCORRECT)
//            return false
//        } else view.setDefaultBackground()
//
//        return true
//    }
//
//    private fun checkPasswordField(view: EditText, container: ConstraintLayout): Boolean {
//        val password = view.getString()
//        if (password.isEmpty()) {
//            incorrectPassword(container, ErrorMessageType.EMPTY_PASSWORD)
//            return false
//        } else if (!authViewModel.checkValidPassword(password)) {
//            val messageType = when (authViewModel.tempState) {
//                AuthState.NEW -> ErrorMessageType.PASSWORD_INCORRECT
//                AuthState.SIGN_IN -> ErrorMessageType.PASSWORD_INCORRECT_MESSAGE
//                AuthState.ACCOUNT_DETAILS -> ErrorMessageType.PASSWORD_INCORRECT
//                else -> ErrorMessageType.EMPTY_MESSAGE
//            }
//            incorrectPassword(container, messageType)
//            return false
//        } else container.setDefaultBackground()
//
//        return true
//    }
//
//    private fun incorrectEmail(view: View, message: ErrorMessageType) {
//        showMessage(message)
//        view.setErrorBackground()
//    }
//
//    private fun incorrectPassword(view: View, message: ErrorMessageType) {
//        showMessage(message)
//        view.setErrorBackground()
//    }
//
//    override fun getSoftInputMode() = WindowManager.LayoutParams.SOFT_INPUT_STATE_UNSPECIFIED
//
//    private fun subscribeUi() {
//        authViewModel.authState.observe(viewLifecycleOwner, { viewState(it) })
//        authViewModel.authDone.observe(viewLifecycleOwner, { signingInDone() })
//        authViewModel.popupLocalAccountExists.observe(
//            viewLifecycleOwner,
//            { popupRemoveLocalAccount() })
//        authViewModel.startCaptchaFlow.observe(viewLifecycleOwner, { startCaptchaFlow() })
//        authViewModel.timerValue.observe(viewLifecycleOwner, { timerValue(it) })
//        authViewModel.showResendButton.observe(viewLifecycleOwner, { showResendButton(it) })
//        authViewModel.snackbarMessage.observe(viewLifecycleOwner, { showMessage(it) })
//        authViewModel.customErrorMessage.observe(viewLifecycleOwner, { showSnackbar(it) })
//        authViewModel.showCaptcha.observe(viewLifecycleOwner, { showCaptcha(it) })
//        authViewModel.geeTestResult.observe(viewLifecycleOwner, { catchGeeTestResult(it) })
//        authViewModel.userExistsResult.observe(viewLifecycleOwner, { userExistsResult() })
//        authViewModel.creatingBiometricCipher.observe(viewLifecycleOwner, {
//            createFingerprintDialog(it)
//        })
//        authViewModel.decryptBiometricCipher.observe(viewLifecycleOwner, {
//            decryptFingerprintDialog(it)
//        })
//        authViewModel.allLocalAccountDataRemoved.observe(
//            viewLifecycleOwner,
//            { localAccountDataRemoved() })
//        authViewModel.restoredEmail.observe(viewLifecycleOwner, { restoreEmail(it) })
//        authViewModel.sendMessageScreen.observe(viewLifecycleOwner, { sendMessage() })
//        authViewModel.isFirstLaunch.observe(viewLifecycleOwner, { processFirstLaunch(it) })
//        authViewModel.currentStateBackStack.observe(viewLifecycleOwner, { emitToActivity(it) })
//        authViewModel.focusEmail.observe(viewLifecycleOwner, { focusLoginField() })
//
//        authStateViewModel.emitToFragmentRestoreState.observe(
//            viewLifecycleOwner,
//            { processRemoveBackStack(it) })
//    }
//
//    private fun validateEmailOnRestore() {
//        val email = input_email_restore.getString()
//
//        authViewModel.tempState = AuthState.ACCOUNT_DETAILS
//        if (!checkEmailField(input_email_restore)) return
//
//        authViewModel.validateEmail(email)
//        authViewModel.saveEmail(email)
//        email_code_sent_to.text = email
//    }
//
//    private fun emitToActivity(authState: AuthState) {
//        authStateViewModel.emitToActivityRestoreState.value = authState
//    }
//
//    private fun setCurrentStackState(authState: AuthState) {
//        authStateViewModel.stackState.value = authState
//    }
//
//    private fun processRemoveBackStack(authState: AuthState) {
//        when (authState) {
//            AuthState.SMS_CODE_RESTORE_PASSWORD -> {
//                emitToActivity(AuthState.SMS_CODE_RESTORE_PASSWORD)
//                viewState(AuthState.SMS_CODE)
//            }
//            AuthState.ACCOUNT_DETAILS -> {
//                emitToActivity(AuthState.ACCOUNT_DETAILS)
//                viewState(AuthState.ACCOUNT_DETAILS)
//            }
//            AuthState.SIGN_IN -> {
//                emitToActivity(AuthState.SIGN_IN)
//                viewState(AuthState.SIGN_IN)
//            }
//            AuthState.NEW -> {
//                emitToActivityRegistration(AuthState.NEW)
//                viewState(AuthState.NEW)
//            }
//            AuthState.SMS_CODE -> {
//                emitToActivityRegistration(AuthState.SMS_CODE)
//                viewState(AuthState.SMS_CODE)
//            }
//        }
//    }
//
//    private fun emitToActivityRegistration(authState: AuthState) {
//        authStateViewModel.emitActivityRegistration.value = authState
//    }
//
//    private fun processFirstLaunch(value: Boolean?) {
//        value?.let {
//            if (value) {
//                authViewModel.createFingerprint()
//            } else {
//                val email = authViewModel.userData.email
//                val password = when {
//                    authViewModel.userData.password.isNotEmpty() -> authViewModel.userData.password
//                    localAuthState == AuthState.SIGN_IN -> input_password_sign_in.getString()
//                    localAuthState == AuthState.NEW -> input_password_sign_up.getString()
//                    else -> ""
//                }
//                authViewModel.signInUser(email, password)
//            }
//        }
//    }
//
//    private fun restoreEmail(email: String) {
//        if (localAuthState == AuthState.SIGN_IN) {
//            input_email_sign_in.setText(email)
//            if (BiometricManager.from(requireContext())
//                    .canAuthenticate() != BIOMETRIC_SUCCESS
//            ) {
//                focusLoginField()
//            } else {
//                runBlocking {
//                    val result = authViewModel.restoreEncryptedPassword()
//                    if (result is Result.Success) {
//                        if (result.data.isEmpty()) focusLoginField()
//                    }
//                }
//            }
//        }
//    }
//
//    private fun focusLoginField() {
//        if (!snackbarCurrentlyShowing)
//            input_email_sign_in.focusField(requireContext())
//        loginIsFocused = true
//    }
//
//    private fun createNewPassState() {
//        emitToActivity(AuthState.CREATING_PASSWORD)
//
//        setGoneViews(label_input_email_restore, input_email_restore, container_sms_code)
//        setVisibleViews(
//            label_input_password_restore,
//            container_restore_password,
//            container_restore_password_input_password,
//            container_restore_hint
//        )
//
//        restore_password_header.text = getString(R.string.label_enter_new_password)
//        button_send_code_restore.text = getString(R.string.action_create_fingerprint)
//        button_send_code_restore.setOnClickListener {
//            validatePasswordOnRestore()
//        }
//
//        input_password_restore.onActionDoneListener { validatePasswordOnRestore() }
//        input_password_restore.focusField(requireContext())
//
//        passVisibility = false
//        isFirstFocus = true
//
//        img_pass_visibility_restore_password.setOnClickListener {
//            passVisibility = !passVisibility
//            input_password_restore.transformationMethod =
//                if (passVisibility) HideReturnsTransformationMethod.getInstance() else
//                    PasswordTransformationMethod.getInstance()
//            input_password_restore.setSelection(input_password_restore.text.length)
//            changeEyeImage(img_pass_visibility_restore_password)
//        }
//
//        input_password_restore.onFocusChangeListener =
//            View.OnFocusChangeListener { _, hasFocus ->
//                if (isFirstFocus) {
//                    isFirstFocus = false
//                    if (hasFocus) {
//                        setNotEnteredImage(img_pass_length_restore)
//                        setNotEnteredImage(img_numbers_contain_restore)
//                        setNotEnteredImage(img_uppercase_restore)
//                        setNotEnteredImage(img_lowercase_restore)
//                    }
//                }
//            }
//
//        addTextListenerPass(
//            input_password_restore,
//            img_pass_length_restore,
//            img_numbers_contain_restore,
//            img_lowercase_restore,
//            img_uppercase_restore
//        )
//    }
//
//    private fun validatePasswordOnRestore() {
//        if (!checkPasswordField(
//                input_password_restore,
//                container_restore_password_input_password
//            )
//        ) return
//
//        authViewModel.validatePassword(input_password_restore.getString())
//    }
//
//    private fun addTextListenerPass(
//        editText: EditText,
//        imageLength: ImageView,
//        imageNumbers: ImageView,
//        imageLowercase: ImageView,
//        imageUppercase: ImageView
//    ) {
//        editText.doOnTextChanged { text, _, _, _ ->
//            if (text != null) {
//                if (text.length < 8) setNotEnteredImage(imageLength)
//                else setEnteredImage(imageLength)
//
//                if (authViewModel.stringContainsNumber(text.toString()))
//                    setEnteredImage(imageNumbers)
//                else setNotEnteredImage(imageNumbers)
//
//                if (authViewModel.stringContainsLowercase(text.toString()))
//                    setEnteredImage(imageLowercase)
//                else setNotEnteredImage(imageLowercase)
//
//                if (authViewModel.stringContainsUppercase(text.toString()))
//                    setEnteredImage(imageUppercase)
//                else setNotEnteredImage(imageUppercase)
//            }
//        }
//    }
//
//    private fun sendMessage() {
//        container_restore_password.visible(false)
//        container_sms_code.visible(true)
//    }
//
//    private fun changeEyeImage(imageView: ImageView) {
//        imageView.newBackground(if (passVisibility) R.drawable.ic_password else R.drawable.ic_password_hidden)
//    }
//
//    private fun startCaptchaFlow() {
//        gT3GeetestUtils.startCustomFlow()
//    }
//
//    private fun catchGeeTestResult(geeTestResult: GeeTestResult) {
//        when (geeTestResult) {
//            GeeTestResult.SUCCESS -> gT3GeetestUtils.showSuccessDialog()
//            GeeTestResult.ERROR -> gT3GeetestUtils.showFailedDialog()
//            GeeTestResult.USER_EXISTS -> gT3GeetestUtils.dismissGeetestDialog()
//        }
//    }
//
//    private fun viewState(authState: AuthState) {
//        localAuthState = authState
//
//        when (authState) {
//            AuthState.NEW -> {
//                input_email.setDefaultBackground()
//                container_password.setDefaultBackground()
//
//                hideKeyboard(requireActivity())
//                setCurrentStackState(AuthState.REGISTRATION_STATE)
//                loginIsFocused = false
//                passVisibility = false
//                restorePassword = false
//                container_registration.visible(true)
//                setGoneViews(
//                    container_sms_code,
//                    container_sign_in,
//                    container_loader,
//                    container_restore_password,
//                    iv_restore_password_back
//                )
//                input_email.isFocusableInTouchMode = true
//                input_sms_code.setText("")
//
//                input_email.focusField(requireContext())
//                authViewModel.currentState = AuthState.NEW
//                emitToActivity(AuthState.NEW)
//                emitToActivityRegistration(AuthState.NEW)
//            }
//            AuthState.SMS_CODE -> {
//                hideKeyboard(requireActivity())
//                loginIsFocused = false
//                setGoneViews(
//                    container_registration,
//                    container_sign_in,
//                    container_loader,
//                    container_restore_password,
//                    iv_restore_password_back
//                )
//                container_sms_code.visible(true)
//
////                if (authViewModel.tempState == AuthState.ACCOUNT_DETAILS) {
////                    setGoneViews(ll_resend_code, ll_resend_timer)
////                }
//
//                email_code_sent_to.text = authViewModel.getSavedEmail()
//
//                emitToActivityRegistration(AuthState.SMS_CODE)
//            }
//            AuthState.SIGN_IN -> {
//                setCurrentStackState(AuthState.LOGIN_STATE)
//
//                input_email_sign_in.setDefaultBackground()
//                container_password_sign_in.setDefaultBackground()
//
//                passVisibility = false
//                setGoneViews(
//                    container_registration,
//                    container_sms_code,
//                    container_loader,
//                    container_restore_password,
//                    iv_restore_password_back
//                )
//                container_sign_in.visible(true)
//                input_sms_code.setText("")
//
//                img_pass_visibility_sign_in.setOnClickListener {
//                    passVisibility = !passVisibility
//                    input_password_sign_in.transformationMethod =
//                        if (passVisibility) HideReturnsTransformationMethod.getInstance() else
//                            PasswordTransformationMethod.getInstance()
//                    input_password_sign_in.setSelection(input_password_sign_in.text.length)
//                    changeEyeImage(img_pass_visibility_sign_in)
//                }
//
//                authViewModel.currentState = AuthState.SIGN_IN
//                emitToActivity(AuthState.SIGN_IN)
//                checkFingerprintsRegistered()
//            }
//            AuthState.CREATING_ACCOUNT -> {
//                loginIsFocused = false
//                passVisibility = false
//                setGoneViews(
//                    container_registration,
//                    container_sms_code,
//                    container_sign_in,
//                    container_restore_password,
//                    iv_restore_password_back
//                )
//                try {
//                    container_loader.visible(true)
//                } catch (e: Exception) {
//                }
//                val message = if (restorePassword) getString(R.string.message_changing_password)
//                else getString(R.string.message_creating_account)
//                tv_message.text = message
//            }
//            AuthState.ACCOUNT_DETAILS -> {
//                input_email_restore.setDefaultBackground()
//                container_restore_password_input_password.setDefaultBackground()
//                hideKeyboard(requireActivity())
//                setCurrentStackState(AuthState.PASSWORD_RESTORE_STATE)
//                loginIsFocused = false
//                passVisibility = false
//                restorePassword = true
//                accountDetailsState()
//                if (!snackbarCurrentlyShowing) {
//                    input_email_restore.focusField(requireContext())
//                }
//                authViewModel.currentState = AuthState.ACCOUNT_DETAILS
//                emitToActivity(AuthState.ACCOUNT_DETAILS)
//            }
//            AuthState.CREATING_PASSWORD -> {
//                createNewPassState()
//            }
//        }
//    }
//
//    private fun accountDetailsState() {
//        setVisibleViews(
//            container_restore_password,
//            iv_restore_password_back,
//            label_input_email_restore,
//            input_email_restore,
//            iv_restore_password_back
//        )
//        setGoneViews(
//            label_input_password_restore,
//            container_restore_password_input_password,
//            container_restore_hint,
//            container_registration,
//            container_sms_code,
//            container_sign_in,
//            container_loader
//        )
//
//        iv_restore_password_back.setOnClickListener {
//            activity?.onBackPressed()
//        }
//
//        restore_password_header.text = getString(R.string.label_reset_password)
//        button_send_code_restore.text = getString(R.string.action_auth_next)
//        button_send_code_restore.setOnClickListener {
//            validateEmailOnRestore()
//        }
//    }
//
//    private fun checkFingerprintsRegistered() {
//        if (authViewModel.isBiometricSuccess()) {
//            authViewModel.isFingerPrintExist()
//        } else {
//            validateShowBiometricError()
//
//            if (!snackbarCurrentlyShowing)
//                focusLoginField()
//        }
//    }
//
//    private fun validateShowBiometricError() {
//        when (authViewModel.getBiometricAuthenticateAbility()) {
//            BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE ->
//                showSnackbar("No biometric features available on this device.")
//            BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE ->
//                showSnackbar("Biometric features are currently unavailable.")
//        }
//    }
//
//    private fun timerValue(value: String) {
//        timer_resend_code.text = "0:%s".format(value)
//    }
//
//    private fun showResendButton(isShow: Boolean) {
//        setViewsVisibility(!isShow, ll_resend_timer)
//        setViewsVisibility(isShow, ll_resend_code)
//    }
//
//    private fun showMessage(errorMessageType: ErrorMessageType) {
//        when (errorMessageType) {
//            ErrorMessageType.EMPTY_FIELDS -> showSnackbar(getString(R.string.error_empty_field))
//            ErrorMessageType.EMPTY_EMAIL -> showSnackbar(getString(R.string.error_empty_email))
//            ErrorMessageType.EMPTY_PASSWORD -> showSnackbar(getString(R.string.error_empty_password))
//            ErrorMessageType.EMAIL_INCORRECT -> showSnackbar(getString(R.string.log_in_email_incorrect))
//            ErrorMessageType.PASSWORD_INCORRECT -> showSnackbar(getString(R.string.log_in_password_incorrect))
//            ErrorMessageType.PASSWORD_INCORRECT_MESSAGE -> showSnackbar(getString(R.string.log_in_incorrect_password))
//            ErrorMessageType.USER_ALREADY_EXISTS -> showSnackbar(getString(R.string.error_user_already_exists))
//            ErrorMessageType.LOGIN_ERROR -> showSnackbar(getString(R.string.error_login_params_incorrect))
//            ErrorMessageType.CONFIRMATION_REQUIRED -> showSnackbar(getString(R.string.error_confirmation_required))
//        }
//    }
//
//    private fun showProgressBarSignUp(value: Boolean) {
//        progress_sign_in.visible(value)
//
//        if (localAuthState == AuthState.SIGN_IN)
//            container_sign_in.visible(!value)
//    }
//
//    private fun userExistsResult() {
//        emitToActivity(AuthState.SIGN_IN)
//        viewState(AuthState.SIGN_IN)
//        input_email_sign_in.tryText(authViewModel.getSavedEmail())
//    }
//
//    private fun showSnackbar(message: String) {
//        snackbarCurrentlyShowing = true
//        hideKeyboard(requireActivity())
//        if (message != "Fingerprint created")
//            Snackbar.make(container_auth, message, Snackbar.LENGTH_LONG).show()
//        showProgressBarSignUp(false)
//        CoroutineScope(Main).launch {
//
//            delay(3000)
//            snackbarCurrentlyShowing = false
//
//            focusFieldAfterError(message)
//        }
//    }
//
//    private fun focusFieldAfterError(message: String) {
//        when (localAuthState) {
//            AuthState.NEW -> {
//                if (message.contains(EMAIL))
//                    focusRegistrationEmailField()
//                else if (message.contains(PASSWORD))
//                    focusRegistrationPasswordField()
//            }
//            AuthState.SIGN_IN -> {
//                when {
//                    message.contains(EMAIL) -> focusLoginEmailField()
//                    message.contains(PASSWORD) -> focusLoginPasswordField()
//                    message.contains(USER_EXIST) -> focusLoginEmailField()
//                }
//            }
//            AuthState.SMS_CODE -> {
//                input_sms_code.focusField(requireContext())
//            }
//            AuthState.SMS_CODE_RESTORE_PASSWORD -> {
//                input_sms_code.focusField(requireContext())
//            }
//            AuthState.ACCOUNT_DETAILS -> {
//                input_email_restore.focusField(requireContext())
//            }
//            AuthState.CREATING_PASSWORD -> {
//                input_password_restore.focusField(requireContext())
//            }
//        }
//    }
//
//    private fun focusRegistrationEmailField() = input_email.focusField(requireContext())
//    private fun focusRegistrationPasswordField() =
//        input_password_sign_up.focusField(requireContext())
//
//    private fun focusLoginEmailField() = input_email_sign_in.focusField(requireContext())
//    private fun focusLoginPasswordField() = input_password_sign_in.focusField(requireContext())
//
//    private fun signingInDone() {
//        tradingFragment()
//        initZendesk()
//    }
//
//    private fun initZendesk() {
//        context?.let {
//            if (authViewModel.userData.email.isNotEmpty()) {
//                AppActivity.user_email = authViewModel.userData.email
//            }
//        }
//    }
//
//    private fun tradingFragment() {
//        if (findNavController().currentDestination?.id != R.id.authFragment) return
//
////        val direction = AuthFragmentDirections.actionAuthFragmentToPortfolioFragment(true)
////        findNavController().navigate(direction)
//    }
//
//    private fun initCaptchaConfig() {
//        gT3GeetestUtils = GT3GeetestUtils(activity)
//        gt3ConfigBean = GT3ConfigBean()
//        gt3ConfigBean.pattern = 1
//        gt3ConfigBean.isUnCanceledOnTouchKeyCodeBack = true
//        gt3ConfigBean.isCanceledOnTouchOutside = true
//        gt3ConfigBean.lang = null
//        gt3ConfigBean.timeout = 10000
//        gt3ConfigBean.webviewTimeout = 10000
//        gt3ConfigBean.gt3ServiceNode = GT3ServiceNode.NODE_CHINA
//        gt3ConfigBean.isCanceledOnTouchOutside = false
//
//        gt3ConfigBean.listener = object : GT3Listener() {
//            override fun onSuccess(p0: String?) {
//                if (!snackbarCurrentlyShowing)
//                    input_sms_code.focusField(requireContext())
//                Timber.d("gt3ConfigBean onSuccess $p0")
//            }
//
//            override fun onFailed(errorBean: GT3ErrorBean?) {
//                Timber.d("gt3ConfigBean onFailed ${errorBean.toString()}")
//            }
//
//            override fun onReceiveCaptchaCode(p0: Int) {
//                Timber.d("gt3ConfigBean onReceiveCaptchaCode $p0")
//            }
//
//            override fun onStatistics(p0: String?) {
//                Timber.d("gt3ConfigBean onStatistics $p0")
//            }
//
//            override fun onClosed(p0: Int) {}
//            override fun onButtonClick() {
//                authViewModel.captcha(0, "")
//            }
//
//            override fun onDialogReady(p0: String?) {
//                Timber.d("gt3ConfigBean onDialogReady $p0")
//                super.onDialogReady(p0)
//            }
//
//            override fun onDialogResult(result: String?) {
//                Timber.d("gt3ConfigBean onDialogResult $result")
//                result ?: return
//
//                authViewModel.captcha(1, result)
//            }
//        }
//
//        gT3GeetestUtils.init(gt3ConfigBean)
//    }
//
//    private fun showCaptcha(jsonobj: JSONObject?) {
//        gt3ConfigBean.setApi1Json(jsonobj)
//        gT3GeetestUtils.getGeetest()
//    }
//
//    private fun decryptFingerprintDialog(cipher: Cipher) {
//        if (authViewModel.isBiometricSuccess()) {
//            val biometricPrompt =
//                BiometricPromptUtils.createBiometricPrompt(
//                    this,
//                    ::decryptPasswordFromStorage,
//                    ::biometricErrorSigningIn
//                )
//            val promptInfo = BiometricPromptUtils.createLoginPromptInfo(this)
//            biometricPrompt.authenticate(promptInfo, BiometricPrompt.CryptoObject(cipher))
//        } else {
//            val email = authViewModel.userData.email
//            val password = when {
//                authViewModel.userData.password.isNotEmpty() -> authViewModel.userData.password
//                localAuthState == AuthState.SIGN_IN -> input_password_sign_in.text.toString()
//                localAuthState == AuthState.NEW -> input_password_sign_up.text.toString()
//                else -> ""
//            }
//            authViewModel.signInUser(email, password)
//        }
//    }
//
//    private fun decryptPasswordFromStorage(authResult: BiometricPrompt.AuthenticationResult) {
//        if (authViewModel.isBiometricSuccess()) {
//            authResult.cryptoObject?.cipher.let {
//                showProgressBarSignUp(true)
//                authViewModel.decryptPassword(it)
//            }
//        } else validateShowBiometricError()
//    }
//
//    private fun createFingerprintDialog(cipher: Cipher?) {
//        if (authViewModel.isBiometricSuccess()) {
//            cipher?.let {
//                val biometricPrompt =
//                    BiometricPromptUtils.createBiometricPrompt(
//                        this,
//                        ::encryptPassword,
//                        ::biometricErrorCreating
//                    )
//                val promptInfo = BiometricPromptUtils.createLoginPromptInfo(this)
//                biometricPrompt.authenticate(promptInfo, BiometricPrompt.CryptoObject(it))
//            }
//        } else {
//            if (authViewModel.currentState == AuthState.SIGN_IN) {
//                authViewModel.authDone.value = true
//            } else signInUserAfterAccountCreated()
//        }
//    }
//
//    private fun encryptPassword(authResult: BiometricPrompt.AuthenticationResult) {
//        if (authViewModel.currentState == AuthState.SIGN_IN) {
//            when (authViewModel.tempState) {
//                AuthState.SIGN_IN ->
//                    if (authViewModel.isFirstLaunch.value == true) {
//                        authResult.cryptoObject?.cipher.let {
//                            authViewModel.encryptPasswordLoginState(
//                                input_password_sign_in.text.toString(), it
//                            )
//                        }
//                    } else {
//                        if (authViewModel.isBiometricSuccess())
//                            authResult.cryptoObject?.cipher.let {
//                                viewState(AuthState.CREATING_ACCOUNT)
//                                authViewModel.encryptPassword(
//                                    it, input_password_sign_in.getString()
//                                )
//                            }
//                        else validateShowBiometricError()
//                    }
//            }
//        } else
//            authResult.cryptoObject?.cipher.let {
//                viewState(AuthState.CREATING_ACCOUNT)
//                authViewModel.encryptPassword(it)
//            }
//    }
//
//    private fun biometricErrorSigningIn(errorCode: Int) {
//        CoroutineScope(Main).launch {
//            delay(500)
//            if (authViewModel.currentState == AuthState.SIGN_IN) {
//                if (!loginIsFocused)
//                    focusLoginField()
//            }
//        }
//        Timber.e("biometricErrorSigningIn code=$errorCode")
//    }
//
//    private fun biometricErrorCreating(errorCode: Int) {
//        if (authViewModel.currentState == AuthState.SIGN_IN) {
//            if (errorCode == 10 || errorCode == 7) {
//                popupDeclineFingerprint()
//            }
//        } else if (authViewModel.tempState == AuthState.ACCOUNT_DETAILS) {
//            if (errorCode == 10 || errorCode == 7) {
//                signInUserAfterAccountCreated()
//            }
//        } else if (authViewModel.tempState == AuthState.NEW) {
//            if (errorCode == 10 || errorCode == 7) {
//                signInUserAfterAccountCreated()
//            }
//        } else {
//            signInUserAfterAccountCreated()
//            Timber.e("biometricErrorCreating code=$errorCode")
//        }
//    }
//
//    private fun popupDeclineFingerprint() {
//        MaterialAlertDialogBuilder(requireContext()).apply {
//            setMessage(getString(R.string.message_decline_fingerprint))
//            setPositiveButton(android.R.string.ok) { dialog, _ ->
//                authViewModel.authDone.value = true
//                dialog.dismiss()
//            }
//        }.create().show()
//    }
//
//    private fun popupRemoveLocalAccount() {
//        MaterialAlertDialogBuilder(requireContext()).apply {
//            setMessage(getString(R.string.message_clear_local_account_data))
//            setNegativeButton(android.R.string.cancel) { dialog, _ ->
//                dialog.dismiss()
//                showProgressBarSignUp(false)
//            }
//            setPositiveButton(android.R.string.ok) { dialog, _ ->
//                authViewModel.clearAllLocalAccountData(requireContext().cacheDir.path + SettingsFragment.AVATAR_FILE_PATH)
//                dialog.dismiss()
//            }
//        }.create().show()
//    }
//
//    private fun localAccountDataRemoved() {
//        when (localAuthState) {
//            AuthState.NEW -> createAccount()
//            AuthState.SIGN_IN -> login()
//        }
//    }
//
//    private fun signInUserAfterAccountCreated() {
//        emitToActivity(AuthState.CREATING_ACCOUNT)
//        viewState(AuthState.CREATING_ACCOUNT)
//        authViewModel.signInAfterAccountCreated()
//    }
//
//    override fun onDestroy() {
//        super.onDestroy()
//        hideKeyboard(requireActivity())
//        if (::gT3GeetestUtils.isInitialized) gT3GeetestUtils.destory()
//    }
//
//    override fun onDestroyView() {
//        hideKeyboard(requireActivity())
//        super.onDestroyView()
//    }
//
//    override fun onDetach() {
//        hideKeyboard(requireActivity())
//        super.onDetach()
//    }
//
//    companion object {
//        private const val EMAIL = "email"
//        private const val PASSWORD = "password"
//        private const val USER_EXIST = "exist"
//    }
//}