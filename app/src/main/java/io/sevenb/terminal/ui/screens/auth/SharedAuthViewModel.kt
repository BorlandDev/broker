package io.sevenb.terminal.ui.screens.auth

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.sevenb.terminal.data.model.enums.CommonState
import javax.inject.Inject

class SharedAuthViewModel @Inject constructor() : ViewModel() {
    val setGlobalStateToActivity = MutableLiveData<CommonState?>()
    val getGlobalStateFromActivity = MutableLiveData<CommonState?>()

    val setSubStateToActivity = MutableLiveData<CommonState?>()
    val getSubStateFromActivity = MutableLiveData<CommonState?>()
}