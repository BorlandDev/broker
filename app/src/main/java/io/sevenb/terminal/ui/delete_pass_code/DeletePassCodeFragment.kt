package io.sevenb.terminal.ui.delete_pass_code

import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import io.sevenb.terminal.R
import io.sevenb.terminal.data.model.enum_model.ErrorMessageType
import io.sevenb.terminal.domain.usecase.new_auth.HandleFieldFocus
import io.sevenb.terminal.ui.base.BaseFragment
import io.sevenb.terminal.ui.extension.*
import io.sevenb.terminal.ui.screens.settings.NotifySettingsViewModel
import io.sevenb.terminal.ui.screens.settings.SharedSettingToActivityViewModel
import kotlinx.android.synthetic.main.fragment_delete_pass_code.*
import javax.inject.Inject

class DeletePassCodeFragment : BaseFragment() {

    private lateinit var deletePassCodeViewModel: DeletePassCodeViewModel
    private val notifySettingsViewModel: NotifySettingsViewModel by activityViewModels { viewModelFactory }
    private val navArgs: DeletePassCodeFragmentArgs by navArgs()
    private val sharedSettingToActivityViewModel: SharedSettingToActivityViewModel
            by activityViewModels { viewModelFactory }

    private var passVisibility = false

    @Inject
    lateinit var handleFieldFocus: HandleFieldFocus

    override fun layoutId() = R.layout.fragment_delete_pass_code

    override fun onSearchTapListener() {}

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        deletePassCodeViewModel = viewModelProvider(viewModelFactory)

        subscribeUI()
        initViews()
    }

    private fun initViews() {
        initFieldFocusHandler()
        input_password_delete_pass_code.apply {
            doOnTextChanged { _, _, _, _ ->
                processPasswordError(false)
            }
            onActionDoneListener { checkPassword() }
        }

        button_delete_pass_code.setOnClickListener { checkPassword() }
        setPasswordVisibilityChanger(
            input_password_delete_pass_code, img_pass_visibility_delete_pass_code
        )
    }

    private fun initFieldFocusHandler() {
        handleFieldFocus.apply {
            init(clDeletePassCodeContainer)
            focusField(input_password_delete_pass_code)
        }
    }

    private fun subscribeUI() {
        deletePassCodeViewModel.apply {
            errorString.observe(viewLifecycleOwner, { processErrorString(it) })
            removingDone.observe(viewLifecycleOwner, { processRemovingDone(it) })
            showProgress.observe(viewLifecycleOwner, { processShowProgress(it) })
        }
    }

    private fun processShowProgress(value: Boolean?) {
        value?.let { showProgress(it) }
    }

    private fun showProgress(value: Boolean) {
        fragmentProgress.visible(value)
        button_delete_pass_code.visible(!value)
    }

    private fun processRemovingDone(value: Boolean?) {
        value?.let {
            if (it) {
                findNavController().popBackStack()
                sharedSettingToActivityViewModel.message.value = "PIN was removed"
                notifySettingsViewModel.notifyActToUpdateSefetyData.value = true
            } else showProgress(false)
        }
    }

    private fun processErrorString(message: String) =
        handleFieldFocus.showSnackBar(message, input_password_delete_pass_code)

    fun checkPassword() {
        navArgs.email?.let {
            val password = input_password_delete_pass_code.getString()
            val messageType = deletePassCodeViewModel.validateFields(it, password)

            if (messageType == ErrorMessageType.CHECK_PASSED) {
                deletePassCodeViewModel.signInUser(it, password)
            } else {
                processPasswordError(true)
                processErrorMessage(messageType)
            }
        }
    }

    private fun processErrorMessage(errorMessage: ErrorMessageType?) {
        deletePassCodeViewModel.showProgress(false)

        when (errorMessage) {
            ErrorMessageType.EMPTY_PASSWORD, ErrorMessageType.EMPTY_FIELDS ->
                handleFieldFocus.showSnackBar(
                    getString(R.string.error_empty_password), input_password_delete_pass_code
                )
            ErrorMessageType.PASSWORD_INCORRECT ->
                handleFieldFocus.showSnackBar(
                    getString(R.string.error_login_requirements), input_password_delete_pass_code
                )
        }
    }

    private fun processPasswordError(value: Boolean) {
        container_delete_pass_code_input_password.apply {
            if (value) setErrorBackground() else setDefaultBackground()
        }
    }

    private fun setPasswordVisibilityChanger(view: EditText, image: ImageView) {
        image.setOnClickListener {
            passVisibility = !passVisibility
            view.transformationMethod =
                if (passVisibility) HideReturnsTransformationMethod.getInstance() else
                    PasswordTransformationMethod.getInstance()
            view.setSelection(view.text.length)
            changeEyeImage(image)
        }
    }

    private fun changeEyeImage(imageView: ImageView) {
        imageView.newBackground(if (passVisibility) R.drawable.ic_password else R.drawable.ic_password_hidden)
    }
}