package io.sevenb.terminal.ui.screens.select_currency

import android.view.View
import io.sevenb.terminal.data.model.trading.TradeChartRange
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ConflatedBroadcastChannel

interface BaseSelectItem: BaseCurrencyItem {

    var network: String?
    var isExpanded: Boolean?
    var currentRange: TradeChartRange?
    var updateExpandedStateChannel: Channel<Pair<String, Boolean>>?
    var estimated: String
    var amount: String?

    fun bind(itemView: View, baseCurrency: String, itemSelected: (BaseSelectItem) -> Unit)

}