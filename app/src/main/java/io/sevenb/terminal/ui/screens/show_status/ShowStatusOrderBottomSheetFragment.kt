package io.sevenb.terminal.ui.screens.show_status

import android.os.Bundle
import android.util.Log
import android.view.Display
import android.view.View
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.core.view.updateLayoutParams
import io.sevenb.terminal.R
import io.sevenb.terminal.device.utils.DateTimeUtil
import io.sevenb.terminal.ui.base.BaseBottomSheetFragment
import io.sevenb.terminal.ui.extension.getString
import io.sevenb.terminal.ui.extension.toPlainString
import io.sevenb.terminal.ui.screens.portfolio.PortfolioHistoryItem
import kotlinx.android.synthetic.main.fragment_show_status_deposite.*
import kotlinx.android.synthetic.main.fragment_show_status_order.*
import kotlinx.android.synthetic.main.fragment_show_status_order.close_screen
import kotlinx.android.synthetic.main.fragment_show_status_order.first_amount
import kotlinx.android.synthetic.main.fragment_show_status_order.first_amount_ticker
import kotlinx.android.synthetic.main.fragment_show_status_order.iv_success
import kotlinx.android.synthetic.main.fragment_show_status_order.market_date_created
import kotlinx.android.synthetic.main.fragment_show_status_order.second_amount
import kotlinx.android.synthetic.main.fragment_show_status_order.second_amount_ticker
import kotlinx.android.synthetic.main.fragment_show_status_order.success_title

class ShowStatusOrderBottomSheetFragment : BaseBottomSheetFragment() {
    override fun layoutId() = R.layout.fragment_show_status_order
    override fun containerId() = R.id.container_show_status_order

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val data = arguments?.getParcelable<PortfolioHistoryItem>("content")
        Log.e("!!!", "data - $data")

        when(data?.status.toString()) {
            "Not confirmed" -> {
                iv_success.setImageResource(R.drawable.ic_failed)
            }
            "Pending" -> {
                iv_success.setImageResource(R.drawable.ic_in_proccess)

            }
            "Processing" -> {
                iv_success.setImageResource(R.drawable.ic_in_proccess)


            }
            "Finished" -> {
                iv_success.setImageResource(R.drawable.ic_success_blue)

            }
            "Failed" -> {
                iv_success.setImageResource(R.drawable.ic_failed)

            }
            "Refunded" -> {
                iv_success.setImageResource(R.drawable.ic_in_proccess)

            }
            else -> {
                iv_success.setImageResource(R.drawable.ic_in_proccess)
                success_title.text = "Processing"
            }
        }

        success_title.text = data?.status

        val timestamp = DateTimeUtil.parseToTimestamp(data?.createdAt ?: "")
        val date = DateTimeUtil.dateFromTimestamp(timestamp)
        val time = DateTimeUtil.timeFromTimestamp(timestamp)
        market_date_created.text = "Created at %s %s".format(date, time)

        commision.text = data?.commisison.toString()

        first_amount_ticker.text = if(data?.id == "buy") {
            if (data?.firstTicker == "BCHA") "XEC" else data?.firstTicker
        } else {
            if (data?.secondTicker == "BCHA") "XEC" else data?.secondTicker
        }
        second_amount_ticker.text = if(data?.id == "buy") {
            if (data?.secondTicker == "BCHA") "XEC" else data?.secondTicker
        } else {
            if (data?.firstTicker == "BCHA") "XEC" else data?.firstTicker
        }

        first_amount.text = if(data?.id == "buy") {
            data?.cummulativeQuoteAmount?.toPlainString()
        } else {
            data?.amount.toString()
        }

        second_amount.text = if(data?.id == "buy") {
            data?.amount.toString()
        } else {
            data?.cummulativeQuoteAmount?.toPlainString()
        }
        commision_sign.text = if (data?.secondTicker == "BCHA") "XEC" else data?.secondTicker


        inner_container_order.setTransitionListener(object :
            MotionLayout.TransitionListener {
            override fun onTransitionStarted(p0: MotionLayout?, p1: Int, p2: Int) {}

            override fun onTransitionChange(p0: MotionLayout?, p1: Int, p2: Int, p3: Float) {}

            override fun onTransitionCompleted(p0: MotionLayout?, p1: Int) {
                dismiss()
            }

            override fun onTransitionTrigger(p0: MotionLayout?, p1: Int, p2: Boolean, p3: Float) {}
        })
        close_screen.setOnClickListener {
            dismiss()
        }
    }

    private fun swipeAnimationToStart() {
        inner_container_order.transitionToStart()
    }

    private fun enableAnimation(value: Boolean) {

    }

    private fun formatDate(createdAt: String): String {
        val timestamp = DateTimeUtil.parseToTimestamp(createdAt)
        val date = DateTimeUtil.dateFromTimestamp(timestamp)
        val dateArray = date.split(" ")
        return "${dateArray[1]} ${dateArray[0]}, ${dateArray[2]}"
    }
}