package io.sevenb.terminal.ui.screens.splash

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.sevenb.terminal.data.model.core.Result
import io.sevenb.terminal.data.model.enums.SafetyStates
import io.sevenb.terminal.data.repository.auth.UserAuthRepository
import io.sevenb.terminal.domain.usecase.auth.LocalUserDataStoring
import io.sevenb.terminal.domain.usecase.auth.ProcessTokens
import io.sevenb.terminal.domain.usecase.auth.RestoreRefreshToken
import io.sevenb.terminal.domain.usecase.auth.SignInUser
import io.sevenb.terminal.domain.usecase.charts.AccountData
import io.sevenb.terminal.domain.usecase.firebase_auth.FirebaseAuthHandler
import io.sevenb.terminal.domain.usecase.order.MarketRate
import io.sevenb.terminal.domain.usecase.safety.AESCrypt
import io.sevenb.terminal.domain.usecase.safety.AccountSafetyStoring
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class SplashScreenViewModel @Inject constructor(
    private val restoreRefreshToken: RestoreRefreshToken,
    private val accountSafetyStoring: AccountSafetyStoring,
    private val accountData: AccountData,
    private val marketRate: MarketRate,
    private val signInUser: SignInUser,
    private val processTokens: ProcessTokens,
    private val localUserDataStoring: LocalUserDataStoring,
    private val firebaseAuthHandler: FirebaseAuthHandler,
    private val aesCrypt: AESCrypt
) : ViewModel() {

    val isLoggedOut = MutableLiveData<Int>()
    val unlockMethod = MutableLiveData<SafetyStates>()

    init {
        checkIsLoggedOut()
    }

    private fun checkIsLoggedOut() {
        viewModelScope.launch {
            val loggedOutResult = accountSafetyStoring.restoreLoggedOut()
            if (loggedOutResult is Result.Success) {
                isLoggedOut.value = loggedOutResult.data
            } else {
                isLoggedOut.value = -1
            }
        }
    }

    fun getUnlockMethod() {
        viewModelScope.launch {
            when (val verifyToken = processTokens.verifyToken()) {
                is Result.Success -> {
                    if (verifyToken.data.result) {
                        val refreshTokenAsync = async { restoreRefreshToken.execute() }
                        val fingerprintAvailable =
                            async { accountSafetyStoring.restoreFingerprintEnabled() }
                        val passCodeAvailable =
                            async { accountSafetyStoring.restorePassCodeEnabled() }

                        selectUnlockMethod(
                            fingerprintAvailable.await(),
                            passCodeAvailable.await(),
                            refreshTokenAsync.await()
                        )
                    } else {
                        removeSafetyData()
                        unlockMethod.value = SafetyStates.LOGGED_OUT
                    }
                }
                is Result.Error -> {
                    removeSafetyData()
                    unlockMethod.value = SafetyStates.LOGGED_OUT
                }
            }
        }
    }

    suspend fun loadAccountAssets() {
        withContext(IO) {
            when (val restoreRefreshTokenResult = restoreRefreshToken.execute()) {
                is Result.Success -> {
                    UserAuthRepository.accessToken = restoreRefreshTokenResult.data
                    accountData.apply {
                        mapAccountAssets(true)
                        updateAssetList(System.currentTimeMillis())
                        viewModelScope.launch { primaryLoadFromDB() }
                    }
                }
                else -> {
                }
            }
        }
    }

    private suspend fun selectUnlockMethod(
        fingerprintEnabled: Result<Boolean>,
        passCodeEnabled: Result<Boolean>,
        refreshToken: Result<String>
    ) {
        val fingerprint =
            if (fingerprintEnabled is Result.Success) fingerprintEnabled.data else false

        val passCode = if (passCodeEnabled is Result.Success) passCodeEnabled.data else false
        val token = refreshToken is Result.Success && refreshToken.data.isNotEmpty()

        withContext(Main) {
            unlockMethod.value = when {
                !token -> {
                    removeSafetyData()
                    SafetyStates.LOGGED_OUT
                }
                fingerprint && passCode -> {
                    viewModelScope.launch { loadAccountAssets() }
                    SafetyStates.BOTH_VARIANTS_AVAILABLE
                }
                fingerprint -> SafetyStates.FINGERPRINT_AVAILABLE
                passCode -> {
                    viewModelScope.launch { loadAccountAssets() }
                    SafetyStates.PASS_CODE_AVAILABLE
                }
                else -> {
                    loadAccountAssets()
                    SafetyStates.NOT_LOCKED
                }
            }
        }
    }

//    fun loginInFb() {
//        viewModelScope.launch {
//            when (firebaseAuthHandler.checkIsLoggedIn()) {
//                is Result.Success -> {
//                    when (val fbTokenResult = firebaseAuthHandler.receiveToken()) {
//                        is Result.Success -> signInUser.sendMessageToken(fbTokenResult.data)
//                        is Result.Error -> {
//                        }
//                    }
//                }
//                is Result.Error -> {
//                    val pass = getDecryptedPass()
//                    localUserDataStoring.restoreLocalAccountData()
//
//                    val email = localUserDataStoring.localAccountData.email
//
//                    when (firebaseAuthHandler.signUp(email, pass)) {
//                        is Result.Success -> {
//                            println()
//                        }
//                        is Result.Error -> {
//                            println()
//                        }
//                    }
//
//                    when (firebaseAuthHandler.signIn(email, pass)) {
//                        is Result.Success -> {
//                            when (val fbTokenResult = firebaseAuthHandler.receiveToken()) {
//                                is Result.Success -> signInUser.sendMessageToken(fbTokenResult.data)
//                                is Result.Error -> {
//                                }
//                            }
//                        }
//                        is Result.Error -> {
//                        }
//                    }
//                }
//            }
//        }
//    }

    private suspend fun getDecryptedPass(): String {
        val encryptedPass =
            when (val passRes = localUserDataStoring.restorePassword()) {
                is Result.Success -> passRes.data
                is Result.Error -> ""
            }
        return aesCrypt.decrypt(encryptedPass)
    }

    private fun removeSafetyData() {
        viewModelScope.launch { accountSafetyStoring.logOut() }
    }
}