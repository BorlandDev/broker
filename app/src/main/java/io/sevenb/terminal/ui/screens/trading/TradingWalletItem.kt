package io.sevenb.terminal.ui.screens.trading

import android.graphics.Color
import android.graphics.Paint
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.ViewCompat
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.github.mikephil.charting.components.LimitLine
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.highlight.Highlight
import io.sevenb.terminal.R
import io.sevenb.terminal.data.model.enums.TradingWalletItemType
import io.sevenb.terminal.data.model.trading.TradeChartParams
import io.sevenb.terminal.data.model.trading.TradeChartRange
import io.sevenb.terminal.ui.extension.load
import io.sevenb.terminal.ui.extension.newTextColor
import io.sevenb.terminal.ui.screens.order.OrderBottomSheetFragment
import io.sevenb.terminal.ui.screens.order.OrderViewModel.Companion.roundFloat
import io.sevenb.terminal.ui.screens.select_currency.BaseSelectItem
import io.sevenb.terminal.ui.screens.trading.TradingListAdapter.Companion.formatCryptoAmount
import kotlinx.android.synthetic.main.item_select_list.view.*
import kotlinx.android.synthetic.main.item_trading_list_expand.view.*
import kotlinx.android.synthetic.main.item_trading_list_main.view.*
import kotlinx.android.synthetic.main.item_trading_list_main.view.img_icon
import kotlinx.android.synthetic.main.item_trading_list_main.view.tv_amount
import kotlinx.android.synthetic.main.item_trading_list_main.view.tv_amount_ticker
import kotlinx.android.synthetic.main.item_trading_list_main.view.tv_name
import kotlinx.android.synthetic.main.item_trading_list_main.view.tv_ticker
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.runBlocking
import timber.log.Timber


/**
 * Created by samosudovd on 07/06/2018.
 */

open class TradingWalletItem(
    var name: String = "",
    override var ticker: String = "",
    val cmcIconId: Int = 0,
    var price: Float? = null,
    private val change: Float = 0.0f,
    val isSellAvailable: Boolean = false,
    var chartEntries: List<Entry>? = null,
    var expandedChartEntries: List<Entry>? = null,
    var isFavorite: Boolean = false,
    var lastChartUpdate: Long? = null,
    override var currentRange: TradeChartRange? = null,
    var candleChartData: List<CandleEntry?>? = null,
    var tradingWalletItemType: TradingWalletItemType = TradingWalletItemType.NONE,
    var isDrawn: Boolean = false,
    override var network: String? = "",
    open val updateTimeChannel: ConflatedBroadcastChannel<Pair<String, Long>>,
    open val updateChartRangeChannel: ConflatedBroadcastChannel<Pair<String, TradeChartRange>>,
    override var updateExpandedStateChannel: Channel<Pair<String, Boolean>>?,
    override var isExpanded: Boolean? = false,
    override var estimated: String = "",
    override var amount: String? = ""
) : BaseTradingListItem, BaseSelectItem {

    override fun bind(
        itemView: View,
        openTrade: (Pair<String, OrderBottomSheetFragment.Companion.OrderSide>) -> Unit,
        baseCurrencyTicker: String,
        updateItemChartByPosition: (TradeChartParams) -> Unit,
        position: Int,
        updateExpandSelectedRange: (Int, TradeChartRange) -> Unit,
        updateCandleChartData: (TradeChartParams) -> Unit,
        updateExpandedCandleChartData: (TradeChartParams) -> Unit
    ) {
        if (isExpanded == false) {
            currentRange = TradeChartRange.DAY
            expandedChartEntries = chartEntries?.toList()
            currentRange?.let { updateRangeChannel(it) }
            updateExpandSelectedRange(position, currentRange!!)
        }

        name = if (name == "Bitcoin Cash ABC") "eCash" else name

        chartTypeListener(itemView, updateCandleChartData, updateItemChartByPosition, position)

        isExpanded?.let {
            itemView.trading_item_expand.isVisible = it
        }

        val requestOptions = RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)

        if (cmcIconId == 7686) {
            Glide.with(itemView.context)
                .load(R.drawable.ic_e_cash_logo)
                .apply(requestOptions)
                .into(itemView.img_icon)
        } else Glide.with(itemView.context)
            .load(CMC_ICON_URL.format(cmcIconId))
            .apply(requestOptions)
            .into(itemView.img_icon)

        itemView.tv_ticker.text = if (ticker == "BCHA") "XEC" else ticker
        itemView.tv_name.text = name
        itemView.tv_amount.text = formatCryptoAmount(price)
        itemView.tv_amount_ticker.text = baseCurrencyTicker
        itemView.tv_change.text = change.toString()

        datePickerClickListeners(
            itemView,
            updateItemChartByPosition,
            position,
            updateCandleChartData,
            updateExpandSelectedRange
        )

        itemView.button_buy.setOnClickListener {
            openTrade.invoke(Pair(ticker, OrderBottomSheetFragment.Companion.OrderSide.BUY))
        }

        itemView.button_sell.isVisible = isSellAvailable

        val params = itemView.button_buy.layoutParams as ViewGroup.MarginLayoutParams
        params.marginEnd =
            if (isSellAvailable) itemView.resources.getDimensionPixelSize(R.dimen.margin_twelve)
            else itemView.resources.getDimensionPixelSize(R.dimen.margin_sixteen)
        itemView.button_buy.layoutParams = params

        itemView.button_sell.setOnClickListener {
            openTrade.invoke(Pair(ticker, OrderBottomSheetFragment.Companion.OrderSide.SELL))
        }

        when {
            lastChartUpdate == null -> {
                val tradeParam = TradeChartParams(position, TradeChartRange.DAY)
                if (baseCurrencyTicker.isEmpty()) {
                    if (!chartEntries.isNullOrEmpty())
                        bindChartData(itemView, chartEntries!!, TradeChartRange.DAY)
                    else {
                        tradeParam.ticker = ticker
                        updateItemChartByPosition(tradeParam)
                        updateCandleChartData(tradeParam)
                        updateTimeChannel()
                    }
                } else {
                    tradeParam.ticker = ticker
                    updateItemChartByPosition(tradeParam)
                    updateCandleChartData(tradeParam)
                    updateTimeChannel()
                }
            }
            System.currentTimeMillis() - lastChartUpdate!! >= 30_000 -> {
                val tradeParam = TradeChartParams(position, TradeChartRange.DAY)

                if (chartEntries.isNullOrEmpty()) {
                    tradeParam.ticker = ticker
                    updateItemChartByPosition(tradeParam)
                } else {
                    bindChartData(itemView, chartEntries!!, TradeChartRange.DAY)
                }

                if (isExpanded == false) {
                    tradeParam.ticker = ticker
                    expandedChartEntries = chartEntries?.toList()
                    updateRangeChannel(TradeChartRange.DAY)
                    updateExpandSelectedRange(position, TradeChartRange.DAY)
                    updateItemChartByPosition(tradeParam)
                }
                updateTimeChannel()
                if (isExpanded == true) {
                    when (tradingWalletItemType) {
                        TradingWalletItemType.CHART -> {
                            val expandedLineDataSet =
                                lineDataSetByEntries(expandedChartEntries, itemView)
                            itemView.expanded_line_chart.data =
                                LineData(listOf(expandedLineDataSet))
                            itemView.expanded_line_chart.invalidate()
                            setLimitLime(itemView, expandedChartEntries)
                            itemView.expanded_line_chart.invalidate()
                        }
//                        TradingWalletItemType.CANDLE -> {
//                            candleDataSetBytEntries(candleChartData, itemView)
//                        }
                    }
                }
            }
            else -> {
                if (chartEntries.isNullOrEmpty()) {
                    val tradeParam = TradeChartParams(position, TradeChartRange.DAY)
                    tradeParam.ticker = ticker
                    updateItemChartByPosition(tradeParam)
                } else {
                    if (!chartEntries.isNullOrEmpty())
                        bindChartData(itemView, chartEntries!!, TradeChartRange.DAY)
                    if (isExpanded == false) {
                        expandedChartEntries = chartEntries?.toList()
                        updateRangeChannel(TradeChartRange.DAY)
                        updateExpandSelectedRange(position, TradeChartRange.DAY)
                    }
                }
            }
        }

        priceChange(itemView, expandedChartEntries)
    }

    private fun updateTimeChannel() = runBlocking {
        updateTimeChannel.offer(Pair(ticker, System.currentTimeMillis()))
    }

    private fun updateRangeChannel(currentRange: TradeChartRange) = runBlocking {
        updateChartRangeChannel.offer(Pair(ticker, currentRange))
    }

    private fun chartTypeListener(
        itemView: View,
        updateCandleChartData: (TradeChartParams) -> Unit,
        updateItemChartByPosition: (TradeChartParams) -> Unit,
        position: Int
    ) {
        itemView.tv_chart_type.setOnClickListener {
            reverseChartType(itemView, updateCandleChartData, updateItemChartByPosition, position)
        }
    }

    private fun reverseChartType(
        itemView: View,
        updateCandleChartData: (TradeChartParams) -> Unit,
        updateItemChartByPosition: (TradeChartParams) -> Unit,
        position: Int
    ) {
        val tradeParam = TradeChartParams(position, currentRange!!)
        tradeParam.ticker = ticker

        tradingWalletItemType = when (tradingWalletItemType) {
            TradingWalletItemType.CHART -> {
                itemView.expanded_line_chart.visibility = View.INVISIBLE
                itemView.candle_stick_chart.visibility = View.VISIBLE
                itemView.tv_chart_type.text =
                    itemView.context.getString(R.string.trading_item_show_chart)
                TradingWalletItemType.CANDLE
            }
            TradingWalletItemType.CANDLE -> {
                itemView.expanded_line_chart.visibility = View.VISIBLE
                itemView.candle_stick_chart.visibility = View.INVISIBLE
                itemView.tv_chart_type.text =
                    itemView.context.getString(R.string.trading_item_show_candles)
                TradingWalletItemType.CHART
            }
            else -> TradingWalletItemType.NONE
        }
        isDrawn = false
        updateCandleChartData(tradeParam)
        updateItemChartByPosition(tradeParam)
    }

    private fun setLimitLime(itemView: View, expandedChartEntries: List<Entry>?) {
        itemView.expanded_line_chart.run {
            val first = expandedChartEntries?.firstOrNull()?.y ?: 0.0f
            val last = expandedChartEntries?.lastOrNull()?.y ?: 0.0f
            val isPositive = last > first

            if (!expandedChartEntries.isNullOrEmpty()) {
                val yLvl = expandedChartEntries.last().y
                yLvl.let {
                    val line = LimitLine(it).apply {
                        lineWidth = 1.5f
                        lineColor = if (isPositive) LINE_POSITIVE else LINE_NEGATIVE
                        enableDashedLine(10f, 10f, 0f)
                        labelPosition = LimitLine.LimitLabelPosition.RIGHT_TOP
                    }
                    axisRight.removeAllLimitLines()
                    axisRight.addLimitLine(line)
                }
            }
        }
    }

    private fun priceChange(itemView: View, list: List<Entry>?) {
        val first = list?.firstOrNull()?.y ?: 0.0f
        val last = list?.lastOrNull()?.y ?: 0.0f

        if (first == 0.0f) {
            itemView.tv_change.text = "0.0%"
            return
        }

        val delta = last - first
        val change = delta.div(first).times(100)
        val changeRoundedString = roundFloat(change, 1)

        var sign = ""
        if (change >= 0.0f) {
            itemView.tv_change.newTextColor(R.color.colorGreen)
            sign = "+"
        } else {
            itemView.tv_change.newTextColor(R.color.colorRed)
        }

        itemView.tv_change.text = "%s%s%s".format(sign, changeRoundedString, "%")
    }

    override fun bindChartData(
        itemView: View,
        listEntries: List<Entry>,
        tradeChartRange: TradeChartRange,
        update: Boolean
    ) {
        if (tradeChartRange == TradeChartRange.DAY) {
            val lineDataSet = lineDataSetByEntries(listEntries, itemView)
            if (listEntries.isEmpty()) {
                bindChartPlaceholder(itemView)
            } else {
                if (listEntries.first().y == 0.0f && listEntries.last().y == 0.0f) {
                    bindChartPlaceholder(itemView)
                } else {
                    itemView.item_line_chart.data = LineData(listOf(lineDataSet))
                    itemView.item_line_chart.invalidate()

                    if (isExpanded == false) itemView.item_line_chart.isVisible = true
                    else if (isExpanded == true) itemView.item_line_chart.isVisible = false

                    itemView.item_chart_placeholder.isVisible = false
                }
            }
        }

        priceChange(itemView, listEntries)
        if (isExpanded == true) {
            if (update) {
                val entries = if (currentRange == TradeChartRange.DAY) listEntries
                else expandedChartEntries

                val expandedLineDataSet = lineDataSetByEntries(entries, itemView)
                itemView.expanded_line_chart.data = LineData(listOf(expandedLineDataSet))
                itemView.expanded_line_chart.invalidate()
                setLimitLime(itemView, entries)
                itemView.expanded_line_chart.invalidate()

//                candleDataSetBytEntries(candleChartData, itemView)
            }
        }
    }

    private fun bindChartPlaceholder(itemView: View) {
        itemView.item_line_chart.isVisible = false
        itemView.item_chart_placeholder.isVisible = true
    }

    override fun bindPrice(itemView: View, price: String) {
        Timber.d("bindPrice price=$price")
        itemView.tv_amount.text = price
    }

    private fun datePickerClickListeners(
        itemView: View,
        updateItemChartByPosition: (TradeChartParams) -> Unit,
        position: Int,
        updateCandleChartData: (TradeChartParams) -> Unit,
        updateExpandSelectedRange: (Int, TradeChartRange) -> Unit
    ) {
        setDefaultPickerBackground(itemView)
        restorePickerBackground(itemView)

        itemView.tv_period_one_day.setOnClickListener {
            val chartParams = TradeChartParams(position, TradeChartRange.DAY)
            chartParams.ticker = ticker
            processPickerClick(
                it as TextView,
                itemView,
                chartParams,
                updateItemChartByPosition,
                updateCandleChartData,
                updateExpandSelectedRange
            )
        }
        itemView.tv_period_7days.setOnClickListener {
            val chartParams = TradeChartParams(position, TradeChartRange.WEEK)
            chartParams.ticker = ticker
            processPickerClick(
                it as TextView,
                itemView,
                chartParams,
                updateItemChartByPosition,
                updateCandleChartData,
                updateExpandSelectedRange
            )
        }
        itemView.tv_period_30days.setOnClickListener {
            val chartParams = TradeChartParams(position, TradeChartRange.MONTH)
            chartParams.ticker = ticker
            processPickerClick(
                it as TextView,
                itemView,
                chartParams,
                updateItemChartByPosition,
                updateCandleChartData,
                updateExpandSelectedRange
            )
        }
        itemView.tv_period_90days.setOnClickListener {
            val chartParams = TradeChartParams(position, TradeChartRange.THREE_MONTH)
            chartParams.ticker = ticker
            processPickerClick(
                it as TextView,
                itemView,
                chartParams,
                updateItemChartByPosition,
                updateCandleChartData,
                updateExpandSelectedRange
            )
        }
        itemView.tv_period_year.setOnClickListener {
            val chartParams = TradeChartParams(position, TradeChartRange.YEAR)
            chartParams.ticker = ticker
            processPickerClick(
                it as TextView,
                itemView,
                chartParams,
                updateItemChartByPosition,
                updateCandleChartData,
                updateExpandSelectedRange
            )
        }
        itemView.tv_period_all.setOnClickListener {
            val chartParams = TradeChartParams(position, TradeChartRange.ALL)
            chartParams.ticker = ticker
            processPickerClick(
                it as TextView,
                itemView,
                chartParams,
                updateItemChartByPosition,
                updateCandleChartData,
                updateExpandSelectedRange
            )
        }
    }

    private fun processPickerClick(
        view: TextView,
        itemView: View,
        tradeChartParams: TradeChartParams,
        updateItemChartByPosition: (TradeChartParams) -> Unit,
        updateCandleChartData: (TradeChartParams) -> Unit,
        updateExpandSelectedRange: (Int, TradeChartRange) -> Unit
    ) {
        updateItemChartByPosition(tradeChartParams)
//            updateCandleChartData(tradeChartParams)
        resetDatePicker(itemView)
        view.newTextColor(R.color.colorTextMain)
        updateRangeChannel(tradeChartParams.tradeChartRange)
        updateExpandSelectedRange(tradeChartParams.listPosition, tradeChartParams.tradeChartRange)
        setDefaultPickerBackground(itemView)
        view.background = setChartButtonPressed(itemView)
        view.typeface = getTypeFace(itemView)
        isDrawn = false
        itemView.candle_stick_chart.fitScreen()
        setElevation(view)
    }

    private fun getColor(itemView: View, colorId: Int) =
        ContextCompat.getColor(itemView.context, colorId)

    private fun restorePickerBackground(itemView: View) {
        val textColor = R.color.colorTextMain
        resetDatePicker(itemView)
        when (currentRange) {
            TradeChartRange.DAY -> {
                restorePicker(itemView.tv_period_one_day, itemView, textColor)
            }
            TradeChartRange.WEEK -> {
                restorePicker(itemView.tv_period_7days, itemView, textColor)
            }
            TradeChartRange.MONTH -> {
                restorePicker(itemView.tv_period_30days, itemView, textColor)
            }
            TradeChartRange.THREE_MONTH -> {
                restorePicker(itemView.tv_period_90days, itemView, textColor)
            }
            TradeChartRange.YEAR -> {
                restorePicker(itemView.tv_period_year, itemView, textColor)
            }
            TradeChartRange.ALL -> {
                restorePicker(itemView.tv_period_all, itemView, textColor)
            }
        }
    }

    private fun restorePicker(view: TextView, itemView: View, textColor: Int) {
        view.background = setChartButtonPressed(itemView)
        view.setTextColor(getColor(itemView, textColor))
        view.typeface = getTypeFace(itemView)
        setElevation(view)
    }

    private fun getTypeFace(itemView: View) =
        ResourcesCompat.getFont(itemView.context, R.font.inter_semi_bold)

    private fun getDefaultTypeFace(itemView: View) =
        ResourcesCompat.getFont(itemView.context, R.font.inter_medium)

    private fun setChartButtonPressed(itemView: View) =
        ContextCompat.getDrawable(itemView.context, R.drawable.ic_charts_buttons)

    private fun setElevation(view: TextView) {
        ViewCompat.setElevation(view, 5F)
    }

    private fun setDefaultPickerBackground(itemView: View) {
        itemView.tv_period_one_day.background = null
        itemView.tv_period_7days.background = null
        itemView.tv_period_30days.background = null
        itemView.tv_period_90days.background = null
        itemView.tv_period_year.background = null
        itemView.tv_period_all.background = null
    }

    private fun resetDatePicker(itemView: View) {
        val textColor = R.color.colorTextSecond
        itemView.tv_period_one_day.setTextColor(getColor(itemView, textColor))
        itemView.tv_period_one_day.typeface = getDefaultTypeFace(itemView)

        itemView.tv_period_7days.setTextColor(getColor(itemView, textColor))
        itemView.tv_period_7days.typeface = getDefaultTypeFace(itemView)

        itemView.tv_period_30days.setTextColor(getColor(itemView, textColor))
        itemView.tv_period_30days.typeface = getDefaultTypeFace(itemView)

        itemView.tv_period_90days.setTextColor(getColor(itemView, textColor))
        itemView.tv_period_90days.typeface = getDefaultTypeFace(itemView)

        itemView.tv_period_year.setTextColor(getColor(itemView, textColor))
        itemView.tv_period_year.typeface = getDefaultTypeFace(itemView)

        itemView.tv_period_all.setTextColor(getColor(itemView, textColor))
        itemView.tv_period_all.typeface = getDefaultTypeFace(itemView)
    }

    private fun lineDataSetByEntries(
        list: List<Entry>?,
        itemView: View
    ): LineDataSet {
        val lineDataSet = LineDataSet(list, "itemDataSet")
        val first = list?.firstOrNull()?.y ?: 0.0f
        val last = list?.lastOrNull()?.y ?: 0.0f
        val isPositive = last > first

        lineDataSet.run {
            mode = LineDataSet.Mode.LINEAR
            cubicIntensity = 0.05f
            setDrawCircles(false)
            setDrawIcons(false)
            setDrawFilled(false)
            setDrawValues(false)
            color = if (isPositive) LINE_POSITIVE else LINE_NEGATIVE
            lineWidth = 1.5f
            isHighlightEnabled = true

            highLightColor = COLOR_DARK_LINE
            setDrawHorizontalHighlightIndicator(true)
            setDrawVerticalHighlightIndicator(true)

            val highlightedValues = itemView.expanded_line_chart.highlighted

            if (highlightedValues.isNullOrEmpty()) {
                try {
                    if (!expandedChartEntries.isNullOrEmpty())
                        if (expandedChartEntries?.last() != null) {
                            if (itemView.expanded_line_chart != null) {
                                val highlight1 = Highlight(
                                    expandedChartEntries!!.last().x,
                                    expandedChartEntries!!.last().y,
                                    0
                                )
                                itemView.expanded_line_chart.highlightValue(highlight1, true)
                                itemView.expanded_line_chart.invalidate()
                            }
                        }
                } catch (e: NullPointerException) {
                }
            } else {
                itemView.expanded_line_chart.highlightValue(highlightedValues[0])
                itemView.expanded_line_chart.invalidate()
            }
        }

        return lineDataSet
    }

    fun candleDataSetBytEntries(
        list: List<CandleEntry?>?,
        itemView: View
    ) {
        if (!isDrawn) {
            if (!list.isNullOrEmpty()) {
                isDrawn = true
                val candleDataSet = CandleDataSet(list, "DataSet 1").apply {
                    color = Color.rgb(80, 80, 80)
                    shadowColorSameAsCandle = true
                    decreasingColor = getColor(itemView, R.color.colorRed)
                    decreasingPaintStyle = Paint.Style.FILL
                    increasingColor = getColor(itemView, R.color.colorGreen)
                    increasingPaintStyle = Paint.Style.FILL
                    neutralColor = Color.LTGRAY
                    setDrawValues(false)
                    barSpace = 1f
                }

                val data = CandleData(candleDataSet)

                itemView.candle_stick_chart.data = data
                itemView.candle_stick_chart.setVisibleXRangeMaximum(500F)
                itemView.candle_stick_chart.moveViewToX(Integer.MAX_VALUE.toFloat())

                itemView.candle_stick_chart.invalidate()
            }
        }
    }

    /**
     * Bind method by common search list adapter
     * @see io.sevenb.terminal.ui.screens.select_currency.SelectCurrencyAdapter
     */
    override fun bind(
        itemView: View,
        baseCurrency: String,
        itemSelected: (BaseSelectItem) -> Unit
    ) {
        if (cmcIconId == 7686) {
            itemView.img_icon.load(R.drawable.ic_e_cash_logo) {
                placeholder(R.drawable.ic_coin_placeholder)
                error(R.drawable.ic_coin_placeholder)
            }
        } else itemView.img_icon.load(CMC_ICON_URL.format(cmcIconId)) {
            placeholder(R.drawable.ic_coin_placeholder)
            error(R.drawable.ic_coin_placeholder)
        }
        itemView.tv_ticker.text = "%s".format(if (ticker == "BCHA") "XEC" else ticker)
        itemView.tv_name.text = if (name == "Bitcoin Cash ABC") "eCash" else name
        itemView.tv_estimated.text = "%s %s".format(price?.toString(), baseCurrency)
        itemView.setOnClickListener { itemSelected(this) }
    }

    companion object {
        const val CMC_ICON_URL = "https://s2.coinmarketcap.com/static/img/coins/64x64/%s.png"
        val COLOR_DARK_LINE = Color.rgb(53, 53, 76)
        val LINE_POSITIVE = Color.rgb(46, 189, 133)
        val LINE_NEGATIVE = Color.rgb(255, 89, 33)
    }

}
