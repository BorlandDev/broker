package io.sevenb.terminal.device.utils

import io.sevenb.terminal.device.utils.DateTimeUtil.parseToTimestamp
import io.sevenb.terminal.device.utils.DateTimeUtil.parseToTimestampWithFormat
import junit.framework.Assert.assertEquals
import org.junit.Test

class DateTimeUtilTest {

    @Test
    fun parseIsoDate() {

        val firstLong = parseToTimestampWithFormat("2020-03-05T10:34:18.000+0000")
        println("firstLong: $firstLong")
        assertEquals(firstLong, 1583404458L)

        val secondLong = parseToTimestamp("2020-11-17T14:00:56.000Z")
        assertEquals(secondLong, 1605621656L)
        println("dateFormatter1 long: $secondLong")
    }
}