/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.android.vending.licensing;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.text.format.DateUtils;
import android.util.Log;

import java.util.concurrent.TimeUnit;

public class MonthlyStrictPolicy implements Policy {

    private static final String PREFS_FILE = "com.android.vending.licensing.DailyPolicy";
    private static final String PREF_LAST_RESPONSE = "lastResponse";
    private static final String PREF_VALIDITY_TIMESTAMP = "validityTimestamp";

    private static final long EXPIRED_TIME_IN_MILLIS = TimeUnit.DAYS.toMillis(30);

    private int mLastResponse;
    private boolean mServerResponse;

    private long mValidityTimestamp;

    private PreferenceObfuscator mPreferences;

    private Context mContext;

    private static final String TAG = "Policy";

    public MonthlyStrictPolicy() {
        // Set default policy. This will force the application to check the policy on launch.
        mLastResponse = Policy.RETRY;
    }

    public MonthlyStrictPolicy(Context context, Obfuscator obfuscator) {
        // Import old values
        mContext = context;
        SharedPreferences sp = context.getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE);
        mPreferences = new PreferenceObfuscator(sp, obfuscator);
        mLastResponse = Integer.parseInt(
                mPreferences.getString(PREF_LAST_RESPONSE, Integer.toString(Policy.RETRY)));

        mValidityTimestamp = Long.parseLong(mPreferences.getString(PREF_VALIDITY_TIMESTAMP, String.valueOf(0)));
        if (mValidityTimestamp == 0) {
            setValidityTimestamp(mLastResponse == LICENSED ? EXPIRED_TIME_IN_MILLIS : 0);
        }
    }

    public void processServerResponse(int response, ResponseData rawData) {
        mServerResponse = true;
        setLastResponse(response);

        if (response == Policy.LICENSED) {
            setValidityTimestamp(EXPIRED_TIME_IN_MILLIS);
        } else if (response == Policy.NOT_LICENSED) {
            setValidityTimestamp(0);
        }
    }

    public void setLastResponse(int lastResponse) {
        this.mLastResponse = lastResponse;
        mPreferences.putString(PREF_LAST_RESPONSE, Integer.toString(lastResponse));
        mPreferences.commit();
    }

    public void setValidityTimestamp(long validityTimestamp) {
        this.mValidityTimestamp = System.currentTimeMillis() + validityTimestamp;
        mPreferences.putString(PREF_VALIDITY_TIMESTAMP, Long.toString(mValidityTimestamp));
        mPreferences.commit();
    }

    public boolean allowAccess() {
        long ts = System.currentTimeMillis();

        boolean result;
        if (isNetworkAvailable(mContext) && !mServerResponse) {
            Log.e(TAG, "launch with internet " + DateUtils.formatElapsedTime(mValidityTimestamp - System.currentTimeMillis()));
            return false;
        } else {
            result = mLastResponse != Policy.NOT_LICENSED && ts <= mValidityTimestamp;
            Log.e(TAG, "launch with internet " + DateUtils.formatElapsedTime(mValidityTimestamp - System.currentTimeMillis()));
            Log.e(TAG, "allowAccess result " + result);
        }

        return result;
    }

    private boolean isNetworkAvailable(final Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

}
