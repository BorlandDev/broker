/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.android.vending.licensing;

import android.content.Context;
import android.content.SharedPreferences;

public class StrictPolicy implements Policy {

    private static final String PREFS_FILE = "com.android.vending.licensing.DailyPolicy";
    private static final String PREF_LAST_RESPONSE = "lastResponse";

    private int mLastResponse;

    private PreferenceObfuscator mPreferences;

    private Context mContext;

    public StrictPolicy() {
        // Set default policy. This will force the application to check the policy on launch.
        mLastResponse = Policy.RETRY;
    }

    public StrictPolicy(Context context, Obfuscator obfuscator) {
        // Import old values
        mContext = context;
        SharedPreferences sp = context.getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE);
        mPreferences = new PreferenceObfuscator(sp, obfuscator);
        mLastResponse = Integer.parseInt(
                mPreferences.getString(PREF_LAST_RESPONSE, Integer.toString(Policy.RETRY)));
    }

    public void processServerResponse(int response, ResponseData rawData) {
        setLastResponse(response);
    }

    public void setLastResponse(int lastResponse) {
        this.mLastResponse = lastResponse;
        mPreferences.putString(PREF_LAST_RESPONSE, Integer.toString(lastResponse));
        mPreferences.commit();
    }

    public boolean allowAccess() {
        return mLastResponse == Policy.LICENSED;
    }

}
