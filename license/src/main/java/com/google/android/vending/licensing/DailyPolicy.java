/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.android.vending.licensing;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.text.format.DateUtils;
import android.util.Log;

public class DailyPolicy implements Policy {

    private static final String PREFS_FILE = "com.android.vending.licensing.DailyPolicy";
    private static final String PREF_LAST_RESPONSE = "lastResponse";
    private static final String PREF_VALIDITY_TIMESTAMP = "validityTimestamp";

    private static final long DAY_IN_MILLIS = 60 * 60 * 24 * 1000;

    private int mLastResponse;

    private long mValidityTimestamp;

    private PreferenceObfuscator mPreferences;

    private Context mContext;

    public DailyPolicy() {
        // Set default policy. This will force the application to check the policy on launch.
        mLastResponse = Policy.RETRY;
    }

    public DailyPolicy(Context context, Obfuscator obfuscator) {
        // Import old values
        mContext = context;
        SharedPreferences sp = context.getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE);
        mPreferences = new PreferenceObfuscator(sp, obfuscator);
        mLastResponse = Integer.parseInt(
                mPreferences.getString(PREF_LAST_RESPONSE, Integer.toString(Policy.RETRY)));

        mValidityTimestamp = Long.parseLong(mPreferences.getString(PREF_VALIDITY_TIMESTAMP,
                String.valueOf(DAY_IN_MILLIS)));
    }

    public void processServerResponse(int response, ResponseData rawData) {
        setLastResponse(response);

        if (response == Policy.LICENSED) {
            setValidityTimestamp(DAY_IN_MILLIS);
        } else if(response == Policy.NOT_LICENSED){
            setValidityTimestamp(0);
        }
    }

    public void setLastResponse(int lastResponse) {
        this.mLastResponse = lastResponse;
        mPreferences.putString(PREF_LAST_RESPONSE, Integer.toString(lastResponse));
        mPreferences.commit();
    }

    public void setValidityTimestamp(long validityTimestamp) {
        this.mValidityTimestamp = System.currentTimeMillis() + validityTimestamp;
        mPreferences.putString(PREF_VALIDITY_TIMESTAMP,  Long.toString(mValidityTimestamp));
        mPreferences.commit();
    }

    public boolean allowAccess() {
        Log.d("allowAccess", "allowAccess");
        long ts = System.currentTimeMillis();

        if (isNetworkAvailable(mContext)) {
            boolean result = mLastResponse == Policy.LICENSED && ts <= mValidityTimestamp;
            Log.d("allowAccess", "result: " + result + " validityTimestamp" + DateUtils.formatElapsedTime(mValidityTimestamp - ts));
            return result;
        } else if (mLastResponse == Policy.LICENSED) {
            return true;
        }

        return false;
    }

    private boolean isNetworkAvailable(final Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

}
